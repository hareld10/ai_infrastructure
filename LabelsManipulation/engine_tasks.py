from multiprocessing import Process

import numpy as np
import pandas as pd
from tqdm import tqdm
import os
import sys, pathlib
module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, module_path)
sys.path.insert(0, "/workspace/AI/Harel/AI_Infrastructure/")
from DataPipeline.arg_parser import parser
from DataPipeline.ensamble import Ensemble
from DataPipeline.src.GetEngines import get_engines
from DataPipeline.shared_utils import read_label, is_docker
if is_docker():
    from DataPipeline.dataProcessing_args import Pipeline
import json
import requests


class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


def add_delta_clusters_file(engine):
    df = pd.DataFrame()
    for idx in tqdm(range(len(engine))):
        p_delta = engine.get_processed_delta(idx=idx)
        indices = np.unique(p_delta["cluster_id"].values.astype(np.int)).tolist()
        row = {"delta_indices": indices}
        df = df.append({**engine.get_ids_dict(idx), **row}, ignore_index=True)
    df.to_csv(engine.drive_context.aux_path + "/delta_clusters.csv")
    return


def generate_engine(engine, cmd="--radar4d --camExport --rgb --lidar --skip-lidar --radar"):
    cmd = "--processed-delta --camExport --rgb --skip-rgb --lidar --skip-lidar "

    args = parser.parse_args(cmd.split())
    args.src = [engine.get_drive_name()]
    args.lidarSrc = engine.drive_context.base_lidar_src
    args.radarSrc = engine.drive_context.base_radar_src
    args.cameraSrc = engine.drive_context.base_camera_src
    args.dest = engine.drive_context.base_destination
    data_pipeline = Pipeline(args, src_index=0)

    p = Process(target=data_pipeline.run, args=(0,))
    p.start()
    p.join()
    pass


def ensemble_engine(engine):
    ensambler = Ensemble()
    ensambler.ensemble_from_dir(path_context=engine.drive_context, df=engine.df, force=True)


def segformer_engine(engine, force=False):
    for engine_idx in tqdm(range(len(engine))):
        label_path_soft = f"{engine.drive_context.camera_labels}/camera_segformer_soft/camera_segformer_soft_{engine.get_camera_id(engine_idx)}.npy"
        label_path_hard = f"{engine.drive_context.camera_labels}/camera_segformer_hard/camera_segformer_hard_{engine.get_camera_id(engine_idx)}.npy"

        if os.path.exists(label_path_soft) and os.path.exists(label_path_hard) and not force:
            continue

        os.makedirs(os.path.split(label_path_soft)[0], exist_ok=True)
        os.makedirs(os.path.split(label_path_hard)[0], exist_ok=True)

        im_path = engine.get_image(engine_idx, path_only=True)
        if engine_idx % 1000 == 0:
            print(engine_idx, im_path)
        packet = {"img": im_path,
                  "save_soft": label_path_soft,
                  "save_hard": label_path_hard,
                  }
        to_send = json.dumps(packet, cls=NumpyEncoder)
        ret = requests.post(f'http://localhost:1024/predict_and_save', json=to_send)
        try:
            if ret.json()["status"] != 0:
                print("some error happened")
        except Exception as e:
            print("Failed to read json")

def main():
    from multiprocessing import Pool
    engines = get_engines(wf="92", val_train="all", randomize=False, rect=True)
    engines = [engines[0]]
    for engine in engines:
        ensemble_engine(engine)

    # engines = get_engines(wf="36", val_train="all", randomize=False, rect=False)
    # print(engines)

    # with Pool(3) as p:
    #     p.map(segformer_engine, engines)
    #
    # for engine in engines:
    #     segformer_engine(engine)


if __name__ == '__main__':
    main()
