import sys
from pathlib import Path

from OpenSource.WisenseEngine import WisenseEngine
from wiseTypes.classes_dims import Road

sys.path.insert(0, "/home/amper/AI/Meir/radar_perception/")
import plotly.express as px
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pickle
import cv2
from tqdm import tqdm
import warnings
from shapely.geometry import Polygon, Point

warnings.filterwarnings("ignore")

sys.path.insert(0, "/home/amper/AI/Harel/AI_Infrastructure/")  # For non-docker
sys.path.insert(0, "/workspace/AI/Harel/AI_Infrastructure/")  # For docker
from DataPipeline.src.GetEngines import get_engines
from wiseTypes.Label3D import Label3D
import matplotlib

plt.style.use('dark_background')
plt_font = 18
font = {'size': plt_font}
matplotlib.rc('font', **font)
plt.rcParams["figure.figsize"] = (15, 8)

# free_space_segmentation_path = r'predictions/free_space_segmentation_pred_highway.p'
#
# with open(free_space_segmentation_path, 'rb') as f:
#     free_space_seg_pred = pickle.load(f)


def pred_grid_to_df(pred_grid, r_bins, az_bins, threshold=0.5, upsample_res=(50, 50)):
    pred_r_grid, pred_az_grid = np.where(cv2.resize(pred_grid, upsample_res) > threshold)
    pred_r, pred_az = np.linspace(r_bins[0], r_bins[-1], upsample_res[0])[pred_r_grid], \
                      np.linspace(az_bins[0], az_bins[-1], upsample_res[1])[pred_az_grid]
    pred_x = pred_r * np.sin(np.deg2rad(pred_az))
    pred_y = pred_r * np.cos(np.deg2rad(pred_az))
    return pd.DataFrame({'x': pred_x, 'y': pred_y, 'r': pred_r, 'az': pred_az})


def get_seg(idx):
    r_bins = free_space_seg_pred['dataset'].grid_bins['r']
    az_bins = free_space_seg_pred['dataset'].grid_bins['Az']
    pred_grid = free_space_seg_pred['predictions'][idx]
    return pred_grid_to_df(pred_grid, r_bins, az_bins)


def get_seg_filtered(idx, cloud_df):
    cloud_df = cloud_df.copy()
    r_bins = free_space_seg_pred['dataset'].grid_bins['r']
    az_bins = free_space_seg_pred['dataset'].grid_bins['Az']
    pred_grid = free_space_seg_pred['predictions'][idx]

    cloud_df['frames_offset'] = 0
    cloud_df_ground, bins_cols = free_space_seg_pred['dataset'].add_indices_to_cloud(cloud_df[cloud_df.z.abs() < 0.5])
    pred_grid = pred_grid.copy()
    pred_grid[cloud_df_ground.r_indices, cloud_df_ground.Az_indices] = 0

    return pred_grid_to_df(pred_grid, r_bins, az_bins)


# obj_det_path = r'predictions/3dvd_pred_highway.p'
# with open(obj_det_path, 'rb') as f:
#     obj_det_pred = pickle.load(f)


def get_3dvd(idx):
    pred = obj_det_pred['predictions'][idx]
    numel = len(pred['pred_scores'])
    bboxs = []
    for score, bbox_raw in zip(pred['pred_scores'], pred['pred_boxes']):
        bboxs.append(Label3D('vehicle', score, *bbox_raw[:-1], bbox_raw[-1] + np.pi / 2))
    return bboxs


idx_start = 389
output_len = 50
mode = ' '


def draw_engine_idx(engine: WisenseEngine, idx, obj_det_results=None, save_path=None):
    max_y = 80
    if obj_det_results is None:
        obj_det_results = engine.get_ensemble_3d(idx)

    pt_size = 3
    # cloud_df = engine.get_processed_delta(idx)
    cloud_df = engine.get_lidar_df(idx)
    cloud_df = cloud_df[cloud_df.z >= 0]
    cloud_df = cloud_df[cloud_df.y <= max_y]
    cloud_df = cloud_df[cloud_df.x.abs() <= 20]

    cloud_df = pd.concat([cloud_df, pd.DataFrame(
        dict(x=[-30, 30], y=[0, max_y+10], z=[-10, 10])
    )])

    # seg_df = get_seg_filtered(idx, cloud_df)
    seg_df = engine.get_road_surface(idx)

    road_seg_points = np.array([Point(p) for p in seg_df[['x', 'y']].values], dtype=np.object_)
    free_space_mask = np.ones(len(road_seg_points)).astype(np.bool_)
    im = engine.get_image(idx)

    polygons = []

    for obj in obj_det_results:
        if obj.score > 0.1:
            obj.heading += np.pi / 2
            box_points = obj.get_box_points()
            polygons.append(Polygon(box_points).buffer(0.2))

    if 'vx' in cloud_df.columns:
        cloud_df['vel'] = (cloud_df['vx'] ** 2 + cloud_df['vy'] ** 2 + cloud_df['vz'] ** 2) ** 0.5
        for c_id, cluster_df in cloud_df[cloud_df.vel > 0].groupby('cluster_id'):
            if cluster_df.shape[0] < 3:
                cluster_df = pd.concat([cluster_df, cluster_df, cluster_df])
            polygons.append(Polygon(cluster_df[['x', 'y']].to_numpy()).convex_hull.buffer(0.2))

    if 1:
        if mode == 'filter':
            delta_points = np.array([Point(p) for p in cloud_df[['x', 'y']].values], dtype=np.object_)
            delta_mask = np.zeros_like(delta_points).astype(bool)
            for i, point in enumerate(delta_points):
                if any([polygon.contains(point) for polygon in polygons]):
                    delta_mask[i] = True
            delta_mask |= (cloud_df.vel > 0).values.flatten()
            cloud_df = cloud_df.iloc[delta_mask]
            # f = px.
        else:
            # Remove free space point inside objects
            for i, point in enumerate(road_seg_points):
                if any([polygon.contains(point) for polygon in polygons]):
                    free_space_mask[i] = False

            seg_df = seg_df[free_space_mask]

            seg_df['sz'] = 1
            seg_df['z'] = -0.3
            # seg_df['color'] = str(Road.color)

            # seg_df = pd.DataFrame(columns=["x", "y", "z"])
            # seg_df['sz'] = 1
            # seg_df['z'] = -0.3
            f = px.scatter_3d(seg_df, 'x', 'y', 'z', height=1000, size='sz', color=[f"rgb{Road.color}" for _ in range(len(seg_df))])

    # f['layout']['yaxis']['autorange'] = "reversed"
    f.add_scatter3d(x=cloud_df.x.values, y=cloud_df.y.values, z=cloud_df.z.values, mode='markers',
                    marker=dict(size=pt_size, color=cloud_df.z.values, colorscale='RdYlBu_r'))

    if mode != 'filter':
        for obj in obj_det_results:
            if obj.ty > max_y:
                continue
            obj.heading += np.pi / 2
            box_3d = np.vstack(
                [obj.get_box_points()[:-1, :], obj.get_box_points()[:-1, :] + np.array([0, 0, obj.height])])
            f.add_mesh3d(
                # 8 vertices of a cube
                x=box_3d[:, 0],
                y=box_3d[:, 1],
                z=box_3d[:, 2],

                i=[7, 0, 0, 0, 4, 4, 6, 6, 4, 0, 3, 2],
                j=[3, 4, 1, 2, 5, 6, 5, 2, 0, 1, 6, 3],
                k=[0, 7, 2, 3, 6, 7, 1, 1, 5, 5, 7, 6],
                opacity=0.3,
                color=f"rgb{obj.obj_color}",
                flatshading=True
            )
            # f.add_scatter3d(x=lidar_df.x.values, y=lidar_df.y.values, z = lidar_df.z.values, mode='markers',
        #                   marker=dict(size=2))

    f.update_layout(scene_aspectmode='data', template='plotly_dark')
    factor = 1
    camera = dict(
        eye=dict(x=-0.27 / factor, y=-1.5 / factor, z=1 / factor)
    )
    # factor = 1.2
    # camera = dict(
    #     eye=dict(x=-0.27 / factor, y=-1.5 / factor, z=0.1 / factor)
    # )

    f.update_layout(scene_camera=camera)
    f.update_layout(scene=dict(
        zaxis=dict(range=[-5, 12]),
        xaxis=dict(range=[-25, 25]),
        yaxis=dict(range=[3, 50]),

    ))

    f.write_image(save_path, width=1500)
