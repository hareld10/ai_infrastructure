import os
import glob
from pathlib import Path
from tqdm import tqdm
import pandas as pd
import numpy as np
from matplotlib import gridspec, colors
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import PercentFormatter
import cv2

from LabelsManipulation.plotly_figure import draw_engine_idx

plt.style.use('dark_background')
import sys

module_path = os.path.abspath('')
sys.path.insert(0, module_path)
sys.path.insert(0, module_path + "/../")
from DataPipeline.src.GetEngines import get_engines, get_index
from OpenSource.WisenseEngine import WisenseEngine
from Evaluation.evaluation_utils import make_video_from_dir, draw
from Calibrator.calibrator_utils import process_lidar_to_image_on_ax_df
from DataPipeline.pipeline4 import LC, CC
from DataPipeline.pipeline4_utils import get_dir_size
from multiprocessing import Pool
import imageio

# engines = get_engines(wf="198", df="sync_data_10_20_50.csv", rect=True, drive_str="Highway")
engines = get_engines(wf="198", df="filtered_sync_data_lr10_cr10_lc10_gr50.csv", rect=True, drive_str="210720")
engine = engines[0]
# engine.update_df_range(755, 900)
# engine.sample_df(20)
# engine.df = pd.concat([engine.df.iloc[300:380], engine.df.iloc[680:755]]).reset_index(drop=True)
# engine.df = pd.concat([engine.df.iloc[690:755], engine.df.iloc[1876:1976]]).reset_index(drop=True)

plot_delta = 0
plot_cc = 0
plot_lc = 0
plot_ensemble = 1
plot_ssn = 0
v_d = "/workspace/HDD/Harel/figures/exp_7/"
s_p = "/workspace/HDD/Harel/videos/"
os.makedirs(v_d, exist_ok=True)
os.makedirs(s_p, exist_ok=True)

def draw_index(idx):
    fig_path = f"{v_d}/{engine.get_id(idx)}.png"

    plotly_mode = True
    max_r = 150
    n_cols = sum([plot_delta, plot_cc, plot_lc, plot_ensemble, plot_ssn])
    fig = plt.figure(figsize=(20, 15))
    gs = gridspec.GridSpec(ncols=n_cols, nrows=3)

    # GT v2.0
    ax = fig.add_subplot(gs[0, 0])
    img = engine.get_image(idx)
    labels2d = engine.get_2d_label(idx, kind="yolor")
    seg = engine.get_segmentation_label(idx, kind="segformer")
    img = cv2.addWeighted(seg.get_img(colors=True), 0.4, img.copy(), 0.6, 0)

    ax.imshow(draw(img, labels2d))
    ax.axis(False)
    ax.set_title("Camera | GT v2.0")

    # GT v1.0
    # ax = fig.add_subplot(gs[0, 1])
    # img = engine.get_image(idx)
    # labels2d = engine.get_2d_label(idx, kind="edet")
    # seg = engine.get_segmentation_label(idx, kind="deeplab")
    # img = cv2.addWeighted(seg.get_img(colors=True), 0.4, img.copy(), 0.6, 0)
    #
    # ax.imshow(draw(img, labels2d))
    # ax.axis(False)
    # ax.set_title("Camera | GT v1.0")
    #
    pc = engine.get_lidar_df(idx)
    pc = pc[pc.r <= max_r].reset_index(drop=True)
    pc = pc[pc.z <= 4].reset_index(drop=True)

    #     process_lidar_to_image_on_ax_df(pc, img, ax=ax, points_size=1)
    #     ax.imshow(img)
    #     ax.axis(False)
    #     ax.set_title("Camera | cmap=range")

    cmap = "cool"
    alpha = 0.5
    vmin, vmax = -3, 3
    #     cm = plt.get_cmap(cmap)
    cNorm = colors.Normalize(vmin=vmin, vmax=vmax)
    #     scalarMap = cmx.ScalarMappable(norm=cNorm)

    cur_col = 0
    s_row = 1
    if plot_delta:
        ax = fig.add_subplot(gs[s_row:, cur_col])
        delta = engine.get_processed_delta(idx)
        if type(delta) != int:
            s = ax.scatter(delta.x, delta.y, c=delta.z, s=10, cmap=cmap, norm=cNorm, alpha=alpha)
            #             delta.plot(x="x", y="y", c="cluster_id", ax=ax, s=10, kind="scatter", alpha=alpha, cmap="jet", colorbar=False, xlabel="x[m]", ylabel="y[m]")
            labels = engine.get_ensemble_3d(idx, as_obj=True)
            for l in labels:
                if l.cls == "pedestrian":
                    l.width += 1
                    l.length += 1
            [x.plot(ax=ax, bound="r-bbox", annotation_font_size=0) for x in labels]
        ax.set_xlim(-30, 30)
        ax.set_ylim(3, 130)
        ax.set_xlabel("x [m]")
        ax.set_ylabel("y [m]")
        ax.set_title("PCP top-view")
        cur_col += 1

    if plot_lc:
        ax = fig.add_subplot(gs[s_row:, cur_col])
        pc.plot(x="x", y="y", c="z", ax=ax, cmap=cmap, s=10, kind="scatter", alpha=alpha, vmin=vmin, vmax=vmax)
        ax.set_xlim(-30, 30)
        ax.set_ylim(3, 130)
        ax.set_title("Lidar top-view | LC | cmap=z")
        labels = engine.get_lidar_3dssd(idx, as_obj=True)
        [x.plot(ax=ax, bound="r-bbox", annotation_font_size=0) for x in labels]
        cur_col += 1

    if plot_ensemble:
        ax = fig.add_subplot(gs[s_row:, cur_col])
        labels = engine.get_ensemble_3d(idx, as_obj=True)
        for l in labels:
            if l.cls == "pedestrian":
                l.width += 1
                l.length += 1

        if plotly_mode:
            plotly_fig_path = f"{v_d}/{engine.get_id(idx)}_ee_plotly.png"
            draw_engine_idx(engine, idx=idx, obj_det_results=labels, save_path=plotly_fig_path)
            ax.imshow(cv2.imread(plotly_fig_path)[..., ::-1])
            ax.axis(False)
        else:

            s = ax.scatter(pc.x, pc.y, c=pc.z, s=10, cmap=cmap, norm=cNorm, alpha=alpha)
            #         cb = plt.colorbar(s, fraction=0.036, pad=0.04)
            #         cb.set_label('z [m]')
            ax.set_xlim(-30, 30)
            ax.set_ylim(3, 130)

            ax.set_xlabel("x [m]")
            ax.set_ylabel("y [m]")
            [x.plot(ax=ax, bound="r-bbox", annotation_font_size=0) for x in labels]
        ax.set_title("Lidar top-view | GT v2.0")
        cur_col += 1

    if plot_cc:
        ax = fig.add_subplot(gs[s_row:, cur_col])
        labels = engine.get_lidar_camera_fusion_labels(idx, as_obj=True)
        for l in labels:
            if l.cls == "pedestrian":
                l.width += 1
                l.length += 1
        if plotly_mode:
            plotly_fig_path = f"{v_d}/{engine.get_id(idx)}_cc_plotly.png"
            draw_engine_idx(engine, idx=idx, obj_det_results=labels, save_path=plotly_fig_path)
            ax.imshow(cv2.imread(plotly_fig_path)[..., ::-1])
            ax.axis(False)
        else:
            s = ax.scatter(pc.x, pc.y, c=pc.z, s=10, cmap=cmap, norm=cNorm, alpha=alpha)
            cb = plt.colorbar(s, fraction=0.036, pad=0.04)
            cb.set_label('z [m]')
            ax.set_xlim(-30, 30)
            ax.set_ylim(3, 130)
            ax.set_xlabel("x [m]")
            ax.set_ylabel("y [m]")
            [x.plot(ax=ax, bound="r-bbox", annotation_font_size=0) for x in labels]

        ax.set_title("Lidar top-view | GT v1.0")
        cur_col += 1

    if plot_ssn:
        ax = fig.add_subplot(gs[s_row:, cur_col])
        pc.plot(x="x", y="y", c="z", ax=ax, cmap=cmap, s=10, kind="scatter", alpha=alpha, vmin=vmin, vmax=vmax)
        ax.set_xlim(-30, 30)
        ax.set_ylim(3, 130)
        ax.set_title("Lidar top-view | SSN | cmap=z")
        labels_ssn = engine.get_lidar_ssn(idx, as_obj=True)
        [x.plot(ax=ax, bound="r-bbox", annotation_font_size=0) for x in labels_ssn]
        cur_col += 1

    title = f"{str(engine.start_idx + idx)} ({idx})"
    plt.suptitle("GT and processed-point-cloud\nWise 4 | WF=198\n" + str(title))
    plt.tight_layout()

    # logo = imageio.imread("/home/amper/AI/Harel/AI_Infrastructure/Plotting/image/square png_white logo.png")
    # ax = plt.axes([0.10, 0.7, 0.17, 0.17], frameon=True)
    # ax.imshow(logo)
    # ax.axis(False)

    plt.savefig(fig_path)
    plt.close()


with Pool(3) as p:
    print(p.map(draw_index, range(len(engine))))

engine.df["timestampRadar"] = sorted(engine.df.timestamp_radar)
make_video_from_dir(df=engine.df, images_dir=v_d, save_path=s_p, frame_rate=3)
