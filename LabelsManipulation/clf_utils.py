import argparse
import os
import pathlib
import sys

import numpy as np
from pprint import pprint
import time, requests, socket

module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, module_path)
sys.path.insert(0, module_path + "/../")
import pandas as pd

from DataPipeline.runner import run_docker_template


def roty(t):
    """ Rotation about the y-axis. """
    c = np.cos(t)
    s = np.sin(t)
    return np.array([[c, 0, s], [0, 1, 0], [-s, 0, c]])


def rotz(t):
    """ Rotation about the z-axis. """
    c = np.cos(t)
    s = np.sin(t)
    return np.array([[c, -s, 0], [s, c, 0], [0, 0, 1]])


def compute_box_3d(point):
    center = point[:3]
    l, h, w = point[3:6]
    # rot_mat = rotz(point[6] - np.pi / 2)
    rot_mat = rotz(point[6])
    p1 = center + np.dot(rot_mat, [w / 2, -l / 2, h / 2])
    p2 = center + np.dot(rot_mat, [w / 2, l / 2, h / 2])
    p3 = center + np.dot(rot_mat, [-w / 2, l / 2, h / 2])
    p4 = center + np.dot(rot_mat, [-w / 2, -l / 2, h / 2])
    return np.vstack((p1, p2, p3, p4, p1))

def compute_box_3d_ordered(point):
    center = point[:3]
    l, h, w = point[3:6]
    rot_mat = rotz(point[6] - np.pi / 2)
    p1 = center + np.dot(rot_mat, [-w / 2, l / 2, h / 2])
    p2 = center + np.dot(rot_mat, [w / 2, l / 2, h / 2])
    p3 = center + np.dot(rot_mat, [+w / 2, -l / 2, h / 2])
    p4 = center + np.dot(rot_mat, [-w / 2, -l / 2, h / 2])
    return np.vstack((p1, p2, p3, p4, p1))

def get_free_tcp_port():
    tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcp.bind(('', 0))
    addr, port = tcp.getsockname()
    tcp.close()
    time.sleep(0.1)
    return port


def run_size_estimation_docker(available_port=77, d="-d", script_flag="", gpu=0):
    if available_port is None:
        available_port = get_free_tcp_port()
    container_name = f'size_estimation_3dssd_{available_port}'
    if gpu is None:
        gpu = os.getpid() % 2
    se_docker_cmd = f"nvidia-docker run  -it -d -v /home/amper/:/workspace/ -v /media/amper/:/media/amper/ -v " \
                    f"/mnt/:/mnt/ --env=\"DISPLAY\"  -p {available_port}:{available_port} -v " \
                    f"/tmp/.X11-unix:/tmp/.X11-unix --shm-size=64g --ulimit memlock=-1 --ulimit stack=61708864 " \
                    f" --env LD_LIBRARY_PATH=/usr/local/lib/python3.6/dist-packages/spconv:/usr/local/nvidia/lib:/usr/local/nvidia/lib64"

    print("Size Estimation Docker over port", available_port, container_name)
    script_cmd = f"python3 /workspace/AI/Harel/Lidar/3DSSD-pytorch-openPCDet/tools/infer_service.py --port {available_port} --gpu {gpu} {script_flag}"
    time.sleep(np.random.randint(5))
    run_docker_template(image="1a7e495386cb", docker_image_cmd=se_docker_cmd, container_name=container_name, exec_cmd=script_cmd, d=d)
    for i in range(5):
        print("wait", i, container_name)
        response = requests.post(f'http://localhost:{available_port}/alive', json=pd.DataFrame().to_json())
        res_code = int(response.status_code)
        if res_code < 400:
            print("Success docker loading", container_name)
            break
        else:
            time.sleep(5)
    return available_port, container_name


def non_max_suppression_fast(boxes, overlapThresh):
    # if there are no boxes, return an empty list
    # boxes = []
    # for elem in frame_data:
    #     boxes.append([elem["bbox"][0], elem["bbox"][1], elem["bbox"][2], elem["bbox"][3]])

    boxes = np.array(boxes)
    if len(boxes) == 0:
        return []
    # if the bounding boxes integers, convert them to floats --
    # this is important since we'll be doing a bunch of divisions

    if boxes.dtype.kind == "i":
        boxes = boxes.astype("float")

    # initialize the list of picked indexes
    pick = []
    # grab the coordinates of the bounding boxes
    x1 = boxes[:, 0]
    y1 = boxes[:, 1]
    x2 = boxes[:, 2]
    y2 = boxes[:, 3]
    # compute the area of the bounding boxes and sort the bounding
    # boxes by the bottom-right y-coordinate of the bounding box
    area = (x2 - x1 + 1) * (y2 - y1 + 1)
    idxs = np.argsort(y2)
    # keep looping while some indexes still remain in the indexes
    # list
    while len(idxs) > 0:
        # grab the last index in the indexes list and add the
        # index value to the list of picked indexes
        last = len(idxs) - 1
        i = idxs[last]
        pick.append(i)
        # find the largest (x, y) coordinates for the start of
        # the bounding box and the smallest (x, y) coordinates
        # for the end of the bounding box
        xx1 = np.maximum(x1[i], x1[idxs[:last]])
        yy1 = np.maximum(y1[i], y1[idxs[:last]])
        xx2 = np.minimum(x2[i], x2[idxs[:last]])
        yy2 = np.minimum(y2[i], y2[idxs[:last]])
        # compute the width and height of the bounding box
        w = np.maximum(0, xx2 - xx1 + 1)
        h = np.maximum(0, yy2 - yy1 + 1)
        # compute the ratio of overlap
        overlap = (w * h) / area[idxs[:last]]
        # delete all indexes from the index list that have
        idxs = np.delete(idxs, np.concatenate(([last],
                                               np.where(overlap > overlapThresh)[0])))
    # return only the bounding boxes that were picked using the
    # integer data type

    # build back frame data

    return pick

if __name__ == '__main__':
    # python3 clf_utils.py --d "" --ckpt_dir ../output/kitti_models/3DSSD_openPCDet/3DSSD_size_estimation_None_uniform_0.1_z_epochs_80/ckpt/ --ckpt checkpoint_epoch_76.pth --flags "--uniform 0.1 --z" --port 82
    parser = argparse.ArgumentParser(description='Evaluation arguments')
    parser.add_argument('--d', type=str, default="-d", help='')
    parser.add_argument('--port', type=int, default=None, help='')
    parser.add_argument('--se', action='store_true', default=False, help="path to specific dataframe")
    parser.add_argument('--ssd3d', action='store_true', default=False, help="path to specific dataframe")
    parser.add_argument('--ssd3d_z', action='store_true', default=False, help="path to specific dataframe")
    parser.add_argument('--ssd3d_b', action='store_true', default=False, help="path to specific dataframe")

    parser.add_argument('--ckpt', type=str, default="", help='')
    parser.add_argument('--ckpt_dir', type=str, default="", help='')
    parser.add_argument('--flags', type=str, default="", help='')
    parser.add_argument('--gpu', type=str, default=0, help='')

    parser.add_argument('--all', action='store_true', default=False, help="path to specific dataframe")
    args = parser.parse_args()

    if args.all:
        args.d = "-d"

    if args.ckpt:
        if args.port is None:
            print("Must define port")
            exit()
        flags = f"--ckpt_dir {args.ckpt_dir} --ckpt {args.ckpt} {args.flags} --cfg_file ./cfgs/kitti_models/3DSSD_openPCDet.yaml"
        run_size_estimation_docker(available_port=args.port, d=args.d, script_flag=flags, gpu=args.gpu)

    if args.se or args.all:
        args.port = 77
        run_size_estimation_docker(available_port=args.port, d=args.d, gpu=args.gpu)

    if args.ssd3d or args.all:
        args.port = 78
        flags = "--ckpt_dir ../output/kitti_models/3DSSD_openPCDet/3DSSD_uniform_0.15_epochs_90/ckpt/ " \
                "--ckpt uniform_0.15_checkpoint_epoch_90.pth --uniform 0.15 --cfg_file ./cfgs/kitti_models/3DSSD_openPCDet.yaml"

        run_size_estimation_docker(available_port=args.port, d=args.d, script_flag=flags, gpu=args.gpu)

    if args.ssd3d_z or args.all:
        args.port = 79
        # flags = "--ckpt_dir ../output/kitti_models/3DSSD_openPCDet/3DSSD_uniform_0.3_z_epochs_40/ckpt/ " \
        #         "--ckpt checkpoint_epoch_40.pth --uniform 0.15 --z --cfg_file ./cfgs/kitti_models/3DSSD_openPCDet.yaml"

        flags = "--ckpt_dir ../output/kitti_models/3DSSD_openPCDet/3DSSD_uniform_0.15_z_epochs_40/ckpt/ " \
                "--ckpt checkpoint_epoch_40.pth --uniform 0.15 --z --cfg_file ./cfgs/kitti_models/3DSSD_openPCDet.yaml"
        run_size_estimation_docker(available_port=args.port, d=args.d, script_flag=flags, gpu=args.gpu)

    if args.ssd3d_b or args.all:
        args.port = 81
        flags = "--ckpt_dir ../output/kitti_models/3DSSD_openPCDet/3DSSD_epochs_90/ckpt/ " \
                "--ckpt checkpoint_epoch_90.pth --cfg_file ./cfgs/kitti_models/3DSSD_openPCDet.yaml"

        run_size_estimation_docker(available_port=args.port, d=args.d, script_flag=flags, gpu=args.gpu)

    if args.port is not None:
        run_size_estimation_docker(available_port=args.port, d=args.d, gpu=args.gpu)