import argparse
import pathlib
import sys

import requests
from mpl_toolkits.axes_grid1 import make_axes_locatable

module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, module_path)
sys.path.insert(0, module_path + "/../")
from wiseTypes.Label import Label
from DataPipeline.runner import run_docker_template
from LabelsManipulation.clf_utils import compute_box_3d, get_free_tcp_port, run_size_estimation_docker, non_max_suppression_fast
from LabelsManipulation.lidar_size_estimation.model_wrapper import SizeEstimator
from wiseTypes.Cluster import Cluster
from Plotting.wise_plotter import add_rcs, plot_rgb, cut_pc_by_lims
from matplotlib import gridspec, colors
from Calibrator.calibrator_utils import plot_pc_simple, process_lidar_to_image_on_ax_simple, \
    process_lidar_to_image_on_ax, process_lidar_to_image_on_ax_df, get_3d_ax, plot_pc_2d_df, save_obj
# from DataPipeline.src.GetEngines import get_engines
from wiseTypes.Label2D import Label2D
from wiseTypes.Types import DebugTypes, MetaDataTypes
from wiseTypes.classes_dims import Road
from tqdm import tqdm
import numpy as np
import matplotlib.pyplot as plt
import os
from pprint import pprint
import pandas as pd
import time
import warnings
import random
import h5py
from timeit import default_timer as timer
import imageio

module_path = str(pathlib.Path(__file__).parent.absolute())
warnings.filterwarnings('ignore')
from sklearn.cluster import KMeans
from scipy.spatial import distance
import scipy as sp
import scipy.interpolate
import matplotlib.patches as mpatches
from sklearn.cluster import DBSCAN
from DataPipeline.shared_utils import PrintException, toRAzEl, pickle_write, read_label, find_open_ports
from Evaluation.evaluation_utils import make_video_from_dir, draw, cmap_clusters
from OpenSource.WisenseEngine import WisenseEngine
from glob import glob
from DataPipeline.src.GetEngines import get_from_ts, get_engines
import matplotlib
from multiprocessing import Pool
import cv2
import socket
from multiprocessing.dummy import Pool as ThreadPool

KMEANS = "kmeans"
DB_SCAN = "dbscan"
video_dir = module_path + "/lidar_camera_fusion/"
os.makedirs(video_dir, exist_ok=True)
plt.style.use('dark_background')
plt_font = 18
font = {'size': plt_font}
matplotlib.rc('font', **font)


class CameraLidarFusion:
    def __init__(self, data_engine, config="v2", debug=False, thin=False, se_docker=None, load_docker=False):
        # Algorithm Params
        self.data_engine = data_engine
        if type(config) == str:
            self.config_version = config
            self.config = read_label("%s/configs/%s_%s.json" % (module_path, type(self).__name__, config))
        elif type(config) == dict:
            self.config_version = config["version"]
            self.config = config
        else:
            print("Error, got invalid config", config)
        # print("CameraLidarFusion: version", self.config_version)
        self.size_estimation = self.config["size_estimation"]
        self.container_name, self.available_port = -1, -1
        if thin:
            self.size_estimation = False
        if se_docker is not None:
            self.available_port = se_docker
        else:
            self.available_port = get_free_tcp_port()

        self.cut_factors = self.config["bb_extraction"]["cut_factors"]
        self.min_num_points_per_bb = self.config["bb_extraction"]["m_min_points_per_bb"]
        self.min_points_per_cluster = self.config["bb_extraction"]["m_min_points_per_cluster"]

        self.alg = self.config["clustering"]["type"]

        # Association Params
        self.associate_with_delta_cluster = self.config["association"]["active"]
        self.m_max_distance_from_cluster_center = self.config["association"]["m_max_distance_from_cluster_center"]
        self.max_diameter_per_object = self.config["association"]["max_diameter_per_object"]
        self.min_iou = self.config["association"]["m_min_iou"]
        self.m_max_2d_cog_distance = self.config["association"]["m_max_2d_cog_distance"]

        # DBSCAN Params
        self.dbscan_epsilon = self.config["clustering"]["dbscan_epsilon"]
        self.dbscan_min_points = self.config["clustering"]["dbscan_min_points"]

        # K-Means Params
        self.num_clusters = 4
        self.radius_around_cluster = 4

        # Plotting Param
        self.point_size = 1
        self.display_instances = False
        self.display_frame = False
        self.plot_every = 100
        self.debug = debug
        return

    def load_docker(self):
        print("size estimation mode\nloading docker")
        try:
            _, self.container_name = run_size_estimation_docker(available_port=self.available_port)
        except Exception as e:
            self.available_port = 77
            print("Can't get docker, using default docker on port 77")

    def get_current_config(self):
        return self.config

    @classmethod
    def get_config(cls, config):
        return read_label("%s/configs/%s_%s.json" % (module_path, cls.__name__, config))

    def get_name(self):
        return "%s_%s" % (type(self).__name__, self.config_version)

    def update_points_in_box(self, label, lidar_df):
        bb_median_range = np.nan

        for cut_factor in self.cut_factors:
            if cut_factor != 1:
                b_x1, b_x2 = label.x1 + (label.x2 - label.x1) // cut_factor, label.x2 - (label.x2 - label.x1) // cut_factor
                b_y1, b_y2 = label.y1 + (label.y2 - label.y1) // cut_factor, label.y2 - (label.y2 - label.y1) // cut_factor
            else:
                b_x1, b_x2, b_y1, b_y2 = label.x1, label.x2, label.y1, label.y2
            left = np.logical_and(lidar_df["x_pixel"] >= b_x1, lidar_df["x_pixel"] <= b_x2)
            right = np.logical_and(lidar_df["y_pixel"] >= b_y1, lidar_df["y_pixel"] <= b_y2)
            mask_points_of_bb = np.logical_and(left, right)

            rngs = lidar_df.loc[mask_points_of_bb, "r_lidar_camera"]
            if len(rngs) < self.min_num_points_per_bb and cut_factor != self.cut_factors[-1]:
                continue
            bb_median_range = np.median(rngs)
            if not np.isnan(bb_median_range):
                break
        if np.isnan(bb_median_range):
            label.write_data["fail_cause"] = "Insufficient points in bounding-box"
            return False

        label.meta_data["bb_median_range_to_obj"] = float(bb_median_range)
        label.meta_data["bb_com"] = [int(b_x1), int(b_x2), int(b_y1), int(b_y2)]
        # cluster_df = lidar_df[mask_points_of_bb].median()
        # label.meta_data["3d_cluster_center"] = np.array([cluster_df["x"], cluster_df["y"], cluster_df["z"]])
        return True

    @staticmethod
    def get_pts_from_pc(label, lidar_df):
        left = np.logical_and(lidar_df["x_pixel"] > label.x1, lidar_df["x_pixel"] < label.x2)
        right = np.logical_and(lidar_df["y_pixel"] > label.y1, lidar_df["y_pixel"] < label.y2)
        mask_points_of_bb = np.logical_and(left, right)
        pts_extracted_from_pc = lidar_df[mask_points_of_bb]
        return pts_extracted_from_pc

    def get_clustered_points(self, points_in_lidar_space):
        coord_keys = ["orig_x", "orig_y", "orig_z"]
        cur_points = None
        if self.alg == KMEANS:
            num_points = points_in_lidar_space.shape[0]
            if num_points > 450:
                self.num_clusters = 10
            elif num_points > 350:
                self.num_clusters = 9
            elif num_points > 250:
                self.num_clusters = 8
            elif num_points > 200:
                self.num_clusters = 7
            elif num_points < 100:
                self.num_clusters = 5
            elif num_points < 50:
                self.num_clusters = 4
            else:
                self.num_clusters = 6

            kmeans = KMeans(n_clusters=self.num_clusters, random_state=0).fit(points_in_lidar_space[coord_keys])
            preds = kmeans.update(points_in_lidar_space[coord_keys])
            unique, counts = np.unique(preds, return_counts=True)
            main_cluster_idx = unique[counts.argmax()]
            cur_points = points_in_lidar_space[preds == main_cluster_idx]
            # cluster_center = kmeans.cluster_centers_[main_cluster_idx]
            # cur_points["dist_from_cluster_center"] = np.linalg.norm(cur_points[coord_keys] - cluster_center, axis=-1)
            # cur_points = cur_points[cur_points["dist_from_cluster_center"] < self.radius_around_cluster]
            # if len(cur_points) > 100:
            #     cur_points = cur_points.sort_values(by=["dist_from_cluster_center"], ascending=False)
            #     cur_points = cur_points[:int(len(cur_points) * 0.9)]

        elif self.alg == DB_SCAN:
            db = DBSCAN(eps=self.dbscan_epsilon, min_samples=self.dbscan_min_points)
            X = points_in_lidar_space[coord_keys]
            db_res = db.fit(X)
            labels = db_res.labels_
            core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
            core_samples_mask[db.core_sample_indices_] = True
            unique_labels = set(labels)

            max_idx = max(unique_labels, key=lambda x: (labels == x).sum())
            max_points_cluster_mask = (labels == max_idx)
            cur_points = points_in_lidar_space[max_points_cluster_mask & core_samples_mask]

        return cur_points, np.mean(cur_points[coord_keys], axis=0)

    def add_info_to_label(self, label, df_pts_extracted_from_pc):
        if len(df_pts_extracted_from_pc) < self.min_points_per_cluster:
            label.write_data["fail_cause"] = "Insufficient points in cluster"
            return

        cluster_points, cluster_center = self.get_clustered_points(df_pts_extracted_from_pc)
        label.debug_data[DebugTypes.pts_from_bb] = df_pts_extracted_from_pc
        label.debug_data[DebugTypes.predicted_cluster_from_bb] = cluster_points

        if cluster_points.shape[0] == 0:
            if self.debug: print("Couldn't find cluster")
            label.write_data["fail_cause"] = "Failed to get main cluster (DBSCAN)"
            return

        projected_center = self.data_engine.project_lidar_to_radar(pts=np.array([cluster_center]), filter_fov_flag=False)
        # print(projected_center.shape)
        if projected_center.shape[0] == 0:  # center not in radar space
            label.write_data["fail_cause"] = "Center not in Radar FOV"
            return

        label.meta_data["projected_3d_center_clustering"] = projected_center[0][:3].tolist()
        label.meta_data["3d_center_clustering"] = cluster_center.tolist()
        label.meta_data["3d_heading"] = 0  # In Image Space
        label.meta_data["3d_dimensions"] = {"length": 0, "width": 0, "height": 0}

        label.meta_data["prediction_score"] = float(0)
        # label.score = 0

        # size_estimation
        if not self.size_estimation:
            return

        to_send = cluster_points[["orig_x", "orig_y", "orig_z", "r"]].to_json()
        # print("len(cluster_points)", len(cluster_points))
        if self.available_port != -1:
            if label.cls == "pedestrian":
                ped_box = list(cluster_center.tolist())
                ped_box.extend([0.7, 1.9, 0.7, 0])
                label.write_data["size_estimation"] = ped_box
                box_points = self.data_engine.project_lidar_to_radar(compute_box_3d(ped_box))[:, :3]
                label.write_data["size_estimation_projected_points"] = box_points.tolist()
                # label.write_data["size_estimation_score"] = 0.9
                # label.meta_data["prediction_score"] = 0.9
                # label.score = label.meta_data["prediction_score"]

            else:
                ret = requests.post(f'http://localhost:{self.available_port}/predict', json=to_send)
                pred_dicts = ret.json()["pred_dicts"]
                for k, v in pred_dicts.items():
                    pred_dicts[k] = np.asarray(v)
                if len(pred_dicts["pred_scores"]) > 0:
                    index = pred_dicts["pred_scores"].argmax()
                    obj = pred_dicts["pred_boxes"][index]
                    label.write_data["size_estimation"] = obj.tolist()
                    b = compute_box_3d(obj)
                    box_points = self.data_engine.project_lidar_to_radar(b)[:, :3]
                    label.write_data["size_estimation_projected_points"] = box_points.tolist()
                    label.write_data["size_estimation_score"] = pred_dicts["pred_scores"][index]
                    label.meta_data["prediction_score"] = float(pred_dicts["pred_scores"][index])
                    label.score = label.meta_data["prediction_score"]
        else:
            print("add_info_to_label: size estimation disabled")
        return

    def associate(self, delta_clusters, label, write=True):
        label.write_data["fail_cause_association"] = ""

        ass = {"sum": [0, 0, 0, 0], "val": [-1, -1, -1, -1]}

        if "3d_center_clustering" not in label.meta_data:
            # if label.write_data["fail_cause"] == "":
            label.write_data["fail_cause_association"] = "couldnt find reason"
            return False

        if DebugTypes.predicted_cluster_from_bb not in label.debug_data:
            label.write_data["fail_cause"] = "predicted_cluster_from_bb not in label data"
            return

        l_cluster = Cluster(cluster_df=label.debug_data[DebugTypes.predicted_cluster_from_bb], power_key=None)
        label.write_data["num_points_in_lidar_cluster"] = len(l_cluster)
        if len(l_cluster) == 0:
            label.write_data["fail_cause_association"] = "zero points in lidar_cluster"
            print("zero points in lidar cluster")
            return

        cluster_center = label.meta_data["projected_3d_center_clustering"]
        sorted_clusters = sorted(delta_clusters, key=lambda x: np.linalg.norm(delta_clusters[x].get_3d_center() - cluster_center))

        if len(sorted_clusters) < 1:
            label.write_data["fail_cause_association"] = "No Delta clusters found"
            return None

        match_idx = None
        sorted_cluster: Cluster = delta_clusters[sorted_clusters[0]]

        # Distance From Center
        distance_from_center = np.linalg.norm(sorted_cluster.get_3d_center() - cluster_center)

        # typical dimensions
        if self.max_diameter_per_object:
            # Typical Dimensions
            if label.obj.m_max_cluster_diameter <= sorted_cluster.max_diameter:
                if self.debug: print("Couldn't Associate Diameter: cluster=", sorted_cluster.max_diameter, " desired=",
                                     label.obj.m_max_cluster_diameter)
                label.write_data["fail_cause_association"] = "Cluster max diameter"
                label.write_data["m_max_cluster_diameter"] = str(sorted_cluster.max_diameter)
                label.write_data["m_max_cluster_diameter_vel"] = str(sorted_cluster.get_vel(as_speed=True))
                return None

        # 2d iou distance
        best_iou_idx, best_iou_val = l_cluster.get_best_iou(list(delta_clusters.values()))
        ass["val"][0] = best_iou_val
        if best_iou_val >= self.min_iou:
            match_idx = best_iou_idx
            ass["sum"][0] = 1

        best_cross_dist_idx, best_cross_dist_val = l_cluster.get_best_cross_distance(list(delta_clusters.values()))
        ass["val"][1] = best_cross_dist_val
        if best_cross_dist_val <= self.m_max_2d_cog_distance:
            match_idx = best_cross_dist_idx
            ass["sum"][1] = 1

        se_iou_val = -1
        if "size_estimation_projected_points" in label.write_data:
            points = np.asarray(label.write_data["size_estimation_projected_points"])
            try:
                se_cluster = Cluster(cluster_df=pd.DataFrame(data={"x": points[:, 0], "y": points[:, 1], "z": points[:, 2], }),
                                     power_key=None)
                se_idx, se_iou_val = se_cluster.get_best_iou(list(delta_clusters.values()))
                ass["val"][2] = se_iou_val
                if se_iou_val >= self.min_iou:
                    match_idx = se_idx
                    ass["sum"][2] = 1
            except Exception as e:
                pass
                # print("Exception", label.write_data["size_estimation_projected_points"])
                # print("points.shape", points.shape)

        # 3d com distance
        ass["val"][3] = distance_from_center
        if distance_from_center <= self.m_max_distance_from_cluster_center:
            if self.debug: print("Couldn't Associate Distance: cluster=",
                                 np.linalg.norm(sorted_cluster.get_3d_center() - cluster_center), " desired=",
                                 self.m_max_distance_from_cluster_center)
            match_idx = sorted_cluster.cluster_id
            ass["sum"][3] = 1

        label.write_data["association_meta"] = ass

        if match_idx is None:
            label.write_data["fail_cause_association"] = "Spatial association fail"
            label.write_data["best_iou_val"] = best_iou_val
            label.write_data["best_size_estimation_iou_val"] = se_iou_val
            label.write_data["best_3d_cog_distance"] = best_cross_dist_val
            label.write_data["best_3d_com_distance"] = distance_from_center
            return None

        if write:
            label.meta_data[MetaDataTypes.associate_cluster_id.name] = int(match_idx)
            label.debug_data[DebugTypes.delta_cluster.name] = delta_clusters[match_idx]
            label.associate_cluster = label.meta_data[MetaDataTypes.associate_cluster_id.name]

        return match_idx

    def run_algorithm(self, labels_2d, lidar_df, delta_clusters=None, path=None):
        # Get Classes From 2D Label
        for label in labels_2d:
            # Extract points from bounding box
            # if not self.update_points_in_box(label=label, lidar_df=lidar_df): continue

            df_pts_extracted_from_pc = CameraLidarFusion.get_pts_from_pc(label=label, lidar_df=lidar_df)

            self.add_info_to_label(label=label, df_pts_extracted_from_pc=df_pts_extracted_from_pc)

            if self.associate_with_delta_cluster:
                self.associate(delta_clusters, label)

        return labels_2d

    def run_sub_cluster_label(self, label_2d, delta_clusters):
        if MetaDataTypes.associate_cluster_id.name in label_2d.meta_data: return False
        if DebugTypes.predicted_cluster_from_bb not in label_2d.debug_data: return False
        flag = False
        new_cluster, sub_clusters_index = None, None
        l_cluster = Cluster(cluster_df=label_2d.debug_data[DebugTypes.predicted_cluster_from_bb], power_key=None)
        # Check on which cluster we want to run sub-clusters algorithm
        for cluster_idx, cluster in delta_clusters.items():
            tmp_cluster = cluster.get_bb_intersected_points(l_cluster)
            if len(tmp_cluster) > 0:
                if not tmp_cluster.is_static():
                    continue
                new_cluster = tmp_cluster
                sub_clusters_index = cluster_idx
                break

        if new_cluster is not None:
            sub_clusters = delta_clusters[sub_clusters_index].get_sub_clusters()
            match_idx = self.associate(delta_clusters=sub_clusters, label=label_2d)
            if match_idx is not None:
                flag = True
                label_2d.meta_data[MetaDataTypes.sub_cluster_id.name] = int(match_idx)
                label_2d.meta_data[MetaDataTypes.associate_cluster_id.name] = int(sub_clusters_index)
        return flag

    def run_sub_clusters(self, labels_2d, delta_clusters):
        flag = False
        for label_2d in labels_2d:
            flag = flag or self.run_sub_cluster_label(label_2d, delta_clusters)
        return flag

    def run_engine_idx(self, engine, engine_idx, force=True, write=True, labels_2d=None):
        # if not force:
        #     pred_path = f"{engine.get_predictions_dir()}/{self.get_name()}/{engine.get_id(engine_idx)}npy"
        #     os.makedirs(os.path.split(pred_path)[0], exist_ok=True)
        #     if os.path.exists(pred_path):
        #         return np.load(pred_path, allow_pickle=True)
        if labels_2d is None:
            labels_2d = engine.get_2d_label(engine_idx, kind="yolor", force_obj=True)
        processed_delta = engine.get_processed_delta(engine_idx)
        lidar_df = engine.get_lidar_df(engine_idx)
        lidar_df = lidar_df.apply(pd.to_numeric, errors='coerce')
        if type(processed_delta) != int:
            delta_clusters = engine.get_clusters(p_delta=processed_delta)
        else:
            delta_clusters = {}

        # lidar_camera_df = lidar_df[lidar_df['x_lidar_camera'].notna()]
        lidar_camera_df = lidar_df[lidar_df['x_pixel'].notna()]

        labels_2d = self.run_algorithm(labels_2d=labels_2d, path=None, lidar_df=lidar_camera_df, delta_clusters=delta_clusters)
        for x in labels_2d:
            x.write_data["engine_idx"] = engine_idx
            x.write_data["engine_name"] = engine.get_name()

        labels_3d = [x.to_label_3d() for x in labels_2d]
        labels_3d = [x for x in labels_3d if x]
        for x in labels_3d:
            x.engine = engine

        # if write and engine.get_predictions_dir() is not None:
        #     os.makedirs(os.path.split(pred_path)[0], exist_ok=True)
        #     print(labels_3d)
        #     np.save(pred_path, labels_3d)
        return labels_3d

    def plot(self, engine, idx, im, labels_2d, lidar_df, delta_clusters, processed_delta):
        plt.close()
        gs = gridspec.GridSpec(ncols=4, nrows=3, height_ratios=[2, 2, 1])
        fig = plt.figure(figsize=(20, 12))
        plt.tight_layout(pad=2)

        # OD Camera
        orig_im_ax = fig.add_subplot(gs[0, :2])
        plot_rgb(ax=orig_im_ax, im=im, engine=engine, labels_2d=labels_2d)

        # Lidar Camera
        im_ax = fig.add_subplot(gs[1, :2])
        process_lidar_to_image_on_ax_df(display_df=lidar_df[lidar_df["x_pixel"].notna()], im=im, ax=im_ax,
                                        points_size=1)
        im_to_plot = plot_rgb(ax=im_ax, im=im, engine=engine)
        im_ax.set_title("Lidar | cmap=range [m]", pad=5)

        # orig_im_ax = fig.add_subplot(gs[2, :2])
        # plot_rgb(ax=orig_im_ax, im=engine.get_segmentation_label(idx).get_img(colors=True), engine=engine, labels_2d=labels_2d)
        # orig_im_ax.set_title("Camera | Segmentation", pad=5)

        # Lidar
        ax_3d_lidar = get_3d_ax(fig=fig, loc=gs[:2, -2], x_left=-45, x_right=45, ax3d=False)
        ax_3d_lidar.set_title("Lidar | Scatter | cmap=elevation [m]", pad=20)
        plot_lidar = lidar_df[lidar_df["x"].notna()]
        cNorm = colors.Normalize(vmin=plot_lidar["z"].min(), vmax=plot_lidar["z"].max())
        scat = ax_3d_lidar.scatter(plot_lidar["x"], plot_lidar["y"], c=plot_lidar["z"], cmap="jet", s=1, norm=cNorm,
                                   alpha=0.6)
        plt.colorbar(scat, ax=ax_3d_lidar, fraction=0.026, orientation='vertical')
        [x.plot(ax_3d_lidar) for x in labels_2d]

        legend_ax = fig.add_subplot(gs[2, 2])
        legend_ax.axis("off")
        handles = Label2D.get_legend_patches(labels_2d)
        plt.legend(handles=handles, loc='center', prop={'size': 18})

        legend_ax = fig.add_subplot(gs[2, 3])
        legend_ax.axis("off")
        handles = Label2D.get_legend_patches(labels_2d)
        plt.legend(handles=handles, loc='center', prop={'size': 18})

        # Delta Clusters
        delta_ax = get_3d_ax(fig=fig, loc=gs[:2, -1], x_left=-45, x_right=45, ax3d=False)
        delta_ax.set_title("Delta | cmap=id", pad=20)
        # delta_ax.set_title("Delta | cmap=elevation [m]", pad=30)
        # cNorm = colors.Normalize(vmin=processed_delta["z"].min(), vmax=processed_delta["z"].max())
        # scat = delta_ax.scatter(processed_delta['x'], processed_delta['y'], c=processed_delta["z"], cmap="winter", norm=cNorm, s=self.point_size, alpha=0.6)
        # plt.colorbar(scat, ax=delta_ax, fraction=0.026, orientation='vertical')
        for cluster_idx, cluster in delta_clusters.items():
            cluster_df = cluster.cluster_df
            cluster_color = cmap_clusters(int(cluster.cluster_id))
            delta_ax.scatter(cluster_df['x'], cluster_df['y'], color=cluster_color, s=self.point_size)
            # cluster_center = cluster.get_3d_center()
            # delta_ax.annotate(str(cluster.cluster_id), (cluster_center[0], cluster_center[1]), color=cluster_color, fontsize=12)
        [x.plot(delta_ax) for x in labels_2d]
        # plt.legend(handles=handles, bbox_to_anchor=(0.5, -0.5), loc='lower center',  prop={'size': 18})

        plt.suptitle("Camera-Lidar Fusion | " + engine.get_id(idx=idx) + " | " + str(engine.start_idx + idx))
        plt.tight_layout(pad=2)

        return

    def plot_instances(self, engine, idx, im=None, labels_2d=None, lidar_df=None, delta_clusters=None,
                       suffix="", ensure_classes=None, specific_cluster=None,
                       title="", save_path=None,
                       fail_case=False, classification_video_mode=None, draw_anyway=False,
                       draw_all_labels_2d=False,
                       draw_all_labels_3d=False,
                       draw_all_labels_clf=False):
        if im is None:
            im = engine.get_image(idx)
        if labels_2d is None:
            labels_2d = engine.get_lidar_camera_fusion_labels(idx=idx, as_obj=True)
        if lidar_df is None:
            lidar_df = engine.get_lidar_df(idx)
            lidar_df = lidar_df.apply(pd.to_numeric, errors='coerce')
        if delta_clusters is None:
            processed_delta = engine.get_processed_delta(idx)
            delta_clusters = engine.get_clusters(p_delta=processed_delta)

        plt.close()
        gs = gridspec.GridSpec(ncols=4, nrows=3)
        fig = plt.figure(figsize=(24, 13))
        plt.tight_layout(pad=1)
        if specific_cluster is not None:
            delta_clusters[int(specific_cluster)].cluster_color = (1, 1, 1)
        # OD
        if classification_video_mode is None:
            od_im_ax = fig.add_subplot(gs[0, :2])

            if type(draw_all_labels_2d) == list:
                plot_rgb(ax=od_im_ax, im=im.copy(), engine=engine, labels_2d=draw_all_labels_2d)
            else:
                plot_rgb(ax=od_im_ax, im=im.copy(), engine=engine, labels_2d=labels_2d)
            lidar_im_ax = fig.add_subplot(gs[0, 2:4])
        else:
            lidar_im_ax = fig.add_subplot(gs[0, :2])
            od_im_ax = None

        lidar_im_ax_colormap = "range" # range
        cNorm = process_lidar_to_image_on_ax_df(display_df=lidar_df[lidar_df["x_pixel"].notna()], im=im.copy(),
                                                ax=lidar_im_ax, points_size=0.2, colormap=lidar_im_ax_colormap)
        plot_rgb(ax=lidar_im_ax, im=im.copy(), engine=engine)
        lidar_im_ax.set_title(f"Camera | cmap={lidar_im_ax_colormap} [m]", pad=20)

        if classification_video_mode is None:
            # Delta Scatter
            delta_ax = get_3d_ax(fig=fig, loc=gs[1:3, 3], x_left=-45, x_right=45, ax3d=False)
            for cluster_idx, cluster in delta_clusters.items():
                cluster_df = cluster.cluster_df
                delta_ax.scatter(cluster_df['x'], cluster_df['y'], color=cluster.cluster_color, s=self.point_size)
                cluster_center = cluster.get_3d_center()
                # delta_ax.annotate(str(cluster.cluster_id), (cluster_center[0], cluster_center[1]), color=cluster.cluster_color, fontsize=14)
                delta_ax.set_title("Delta | cmap=id", pad=30)

            # Lidar Scatter
            ax_3d_lidar = get_3d_ax(fig=fig, loc=gs[1:3, 2], x_left=-45, x_right=45, ax3d=False)
            ax_3d_lidar.set_title("Lidar | cmap=elevation [m]", pad=30)
            plot_lidar = lidar_df[lidar_df["x"].notna()]
            cNorm = colors.Normalize(vmin=plot_lidar["z"].min(), vmax=plot_lidar["z"].max())
            scat = ax_3d_lidar.scatter(plot_lidar["x"], plot_lidar["y"], c=plot_lidar["z"], cmap="jet", s=1, norm=cNorm)
            plt.colorbar(scat, ax=ax_3d_lidar, fraction=0.026, orientation='vertical')
        else:
            ax_3d_lidar = None
            delta_ax = get_3d_ax(fig=fig, loc=gs[:, 2:4], x_left=-45, x_right=45, ax3d=False)
            for cluster_id, cluster in delta_clusters.items():
                cluster.plot(ax=delta_ax)
            pred_df = classification_video_mode["pred_df"]
            cur_instances = pred_df[pred_df["timestamp_radar"].astype(str) == engine.get_id(idx)]
            # Clusters
            for row_idx, row in cur_instances[cur_instances["sub_cluster"] == -1].iterrows():
                row_cluster_id = int(row["cluster_id"])
                if row_cluster_id not in delta_clusters: continue
                delta_clusters[row_cluster_id].plot_bounding_box(ax=delta_ax, annotate=row["predicted_class"])

        """
        Plot All 3D Labels        
        """
        if type(draw_all_labels_clf) == list:
            [x.plot(ax=ax_3d_lidar, radar=True, bound="r-bbox", annotation_font_size=14) for x in draw_all_labels_clf if x.cls != "vehicle"]
            [x.plot(ax=delta_ax, radar=True, bound="r-bbox", annotation_font_size=14) for x in draw_all_labels_clf if x.cls != "vehicle"]
        if type(draw_all_labels_3d) == list:
            [x.plot(ax=ax_3d_lidar, radar=True, bound="r-bbox", annotation_font_size=14) for x in draw_all_labels_3d]
            [x.plot(ax=delta_ax, radar=True, bound="r-bbox", annotation_font_size=14) for x in draw_all_labels_3d]

        """
        Plot instance
        """
        found = False
        for label_idx, label_2d in enumerate(labels_2d):
            if draw_anyway:
                pass
            elif specific_cluster is not None:
                ass_flag = self.associate(delta_clusters, label_2d)
                if str(label_2d.associate_cluster) != str(specific_cluster):
                    continue
                else:
                    label_2d.meta_data[DebugTypes.delta_cluster.name] = delta_clusters[int(specific_cluster)]
                    label_2d.debug_data = label_2d.meta_data
                    self.update_points_in_box(label=label_2d, lidar_df=lidar_df)
                    df_pts_extracted_from_pc = CameraLidarFusion.get_pts_from_pc(label=label_2d, lidar_df=lidar_df)
                    self.add_info_to_label(label=label_2d, df_pts_extracted_from_pc=df_pts_extracted_from_pc)
            elif fail_case:
                if "projected_3d_center_clustering" not in label_2d.meta_data:
                    # Fail because not even 3d label (check later) not association
                    continue
            else:
                if DebugTypes.delta_cluster.name not in label_2d.debug_data:
                    print("Warning: DebugTypes.delta_cluster.name not in label_2d.debug_data")
                    # continue
            if ensure_classes is not None:
                if label_2d.cls not in ensure_classes:
                    continue

            found = (label_idx, label_2d)
            # OD
            x1, y1, x2, y2 = label_2d.get_coords(as_int=True)
            if od_im_ax is not None:
                od_im_ax.plot([x1, x2], [y1, y2], linewidth=4, color='yellow')
                od_im_ax.plot([x1, x2], [y2, y1], linewidth=4, color='yellow')

            # Full Image
            # todo change back
            lidar_im_ax.plot([x1, x2], [y1, y2], linewidth=4, color='yellow')
            lidar_im_ax.plot([x1, x2], [y2, y1], linewidth=4, color='yellow')

            # Cropped BBOX | Lidar
            bbs_only_im = plot_rgb(ax=None, im=im, engine=engine)
            if classification_video_mode is None:
                im_ax = fig.add_subplot(gs[1, 0])
                cropped_delta_ax = fig.add_subplot(gs[2, 0])
                im_ax.axis(False)
                if DebugTypes.predicted_cluster_from_bb in label_2d.debug_data:
                    process_lidar_to_image_on_ax_df(
                        display_df=label_2d.debug_data[DebugTypes.predicted_cluster_from_bb],
                        # lidar_df[lidar_df["x_pixel"].notna()],
                        # todo change back label_2d.debug_data[DebugTypes.predicted_cluster_from_bb],
                        im=bbs_only_im, ax=im_ax, points_size=16, specific_cNorm=cNorm,
                        fraction=0.02, pad=0.04)
                im_ax.imshow(im)
                im_ax.set_ylim(int(label_2d.y1), int(label_2d.y2))
                im_ax.set_xlim(int(label_2d.x1), int(label_2d.x2))
                im_ax.invert_yaxis()
                im_ax.set_anchor('C')
                im_ax.set_title("Lidar | cmap=range [m]")
            else:
                cropped_delta_ax = fig.add_subplot(gs[1:3, 0])

            # Cropped BBOX | Delta
            if DebugTypes.delta_cluster.name in label_2d.debug_data:
                cropped_delta_ax.axis(False)
                cluster = label_2d.debug_data[DebugTypes.delta_cluster.name].get_pts().apply(pd.to_numeric,
                                                                                             errors='coerce')
                process_lidar_to_image_on_ax_df(display_df=cluster, im=bbs_only_im, ax=cropped_delta_ax, points_size=16,
                                                specific_cNorm=cNorm)
                cropped_delta_ax.imshow(im)
                y_min = min(int(label_2d.y1), cluster["y_pixel"].min() - 10)
                y_max = max(int(label_2d.y2), cluster["y_pixel"].max() + 10)
                x_min = min(int(label_2d.x1), cluster["x_pixel"].min() - 10)
                x_max = max(int(label_2d.x2), cluster["x_pixel"].max() + 10)
                cropped_delta_ax.set_ylim(y_min, y_max)
                cropped_delta_ax.set_xlim(x_min, x_max)
                cropped_delta_ax.invert_yaxis()
                cropped_delta_ax.set_anchor('C')
                cropped_delta_ax.set_title("Delta | cmap=range [m]")

            cluster_center_legend_flag = False
            if "projected_3d_center_clustering" in label_2d.meta_data:
                cluster_center = label_2d.meta_data["projected_3d_center_clustering"]
                if ax_3d_lidar is not None:
                    ax_3d_lidar.add_patch(
                        plt.Circle((cluster_center[0], cluster_center[1]), 3, color='yellow', fill=False, zorder=0,
                                   linewidth=3))
                    plot_pc_2d_df(lidar_df=pd.DataFrame(
                        data={"x": [cluster_center[0]], "y": [cluster_center[1]], "z": [cluster_center[2]]}),
                        ax=ax_3d_lidar, s=self.point_size * 30, c="red",
                        elev=False, alpha=1,
                        label="cluster-center" if not cluster_center_legend_flag else "")
                delta_ax.add_patch(
                    plt.Circle((cluster_center[0], cluster_center[1]), 3, color='yellow', fill=False, zorder=0,
                               linewidth=3))

                # if MetaDataTypes.associate_cluster_id.name in label_2d.meta_data:
                #     ax_3d_lidar.annotate(str(label_2d.meta_data[MetaDataTypes.associate_cluster_id.name]), (cluster_center[0], cluster_center[1]),
                #                          color=cmap_clusters(int(label_2d.meta_data[MetaDataTypes.associate_cluster_id.name])), fontsize=12)

                lidar_color, delta_color, fft_color = "lightskyblue", "red", "orange"
                both_ax = fig.add_subplot(gs[1:3, 1])
                both_ax.set_xlabel('X [m]\n', labelpad=5)
                both_ax.set_ylabel('Y [m]', labelpad=5)

                l_cluster = None
                if DebugTypes.pts_from_bb in label_2d.debug_data:
                    sc3 = plot_pc_2d_df(lidar_df=label_2d.debug_data[DebugTypes.predicted_cluster_from_bb], ax=both_ax,
                                        s=self.point_size * 10, c=lidar_color, elev=False, alpha=0.7)
                    l_cluster = Cluster(cluster_df=label_2d.debug_data[DebugTypes.predicted_cluster_from_bb],
                                        power_key=None)
                    l_cluster.cluster_color = lidar_color
                    # l_cluster.plot_bounding_cross(ax=both_ax)

                    if self.size_estimation and 'size_estimation_projected_points' in label_2d.write_data:
                        print("label2dddd\n", label_2d)
                        print(type(label_2d), label_2d)
                        label_2d.plot(ax=both_ax, bound="r-bbox", annotation_font_size=0)
                        # box_points = np.asarray(label_2d.write_data['size_estimation_projected_points'])
                        # if len(box_points) > 0:
                        #     both_ax.plot(box_points[:, 0], box_points[:, 1], color="yellow")

                if DebugTypes.delta_cluster.name in label_2d.debug_data:
                    both_ax.set_title("Delta(red) | Lidar(blue) | BEV", pad=100)
                    cluster = label_2d.debug_data[DebugTypes.delta_cluster.name]
                    plot_delta = cluster.get_pts()
                    center = cluster.get_3d_center()
                    geo_center = cluster.get_3d_center(geometric=True)
                    # cluster.plot_bounding_cross(ax=both_ax)
                    cluster.plot_bounding_box(ax=both_ax, color="red")
                    cNorm = colors.Normalize(vmin=np.min(plot_delta["total_power"]),
                                             vmax=np.max(plot_delta["total_power"]))
                    scat = both_ax.scatter(plot_delta["x"], plot_delta["y"], c=plot_delta["total_power"], cmap="summer",
                                           s=self.point_size * 15, norm=cNorm)
                    # both_ax.scatter([center[0]], [center[1]], c="yellow", s=200, marker="X")
                    plt.colorbar(scat, ax=both_ax, fraction=0.026, orientation='vertical', label="dB")
                    both_ax.set_title("Delta| Lidar \nBEV | vel=" + str(
                        np.linalg.norm(cluster.get_vel()).round(2)) + "m/s", pad=90)
                    add_rcs(df=plot_delta, ax=both_ax)

                    if l_cluster is not None:
                        # title +=f"iou_2d={round(l_cluster.get_iou(cluster), 2)}"
                        try:
                            # label_2d.plot(ax=)
                            points = np.asarray(label_2d.write_data["size_estimation_projected_points"])
                            se_cluster = Cluster(cluster_df=pd.DataFrame(data={"x": points[:, 0], "y": points[:, 1], "z": points[:, 2], }),
                                                 power_key=None)
                            se_iou = se_cluster.get_iou(cluster)
                            # title += f" iou_2d_se={round(se_iou, 2)}"
                        except Exception as e:
                            pass
                elif fail_case:
                    both_ax, cropped_delta_ax = cropped_delta_ax, both_ax
                    pass

                    pad_around = 3
                    both_ax.set_xlim(cluster_center[0] - pad_around, cluster_center[0] + pad_around)
                    both_ax.set_ylim(cluster_center[1] - pad_around, cluster_center[1] + pad_around)
                    label_2d.associate_cluster = "Couldn't Associate"
                    # Print IOU,
                    best_iou_idx, best_iou_val = l_cluster.get_best_iou(list(delta_clusters.values()))
                    best_dist_idx, best_dist_val = l_cluster.get_best_distance(list(delta_clusters.values()))
                    # best_geo_distance_idx, best_geo_distance_val = l_cluster.get_best_distance(list(delta_clusters.values()), geometric=True)
                    best_cross_dist_idx, best_cross_dist_val = l_cluster.get_best_cross_distance(
                        list(delta_clusters.values()))

                    both_title = "2d-iou=%.2f (%d), 3d-COM-d=%.2f (%d)\n 2d-COG-d=%.2f (%d)" % (
                        best_iou_val, best_iou_idx,
                        best_dist_val, best_dist_idx,
                        best_cross_dist_val, best_cross_dist_idx,)  # , best_geo_distance_val, best_geo_distance_idx)
                    if best_iou_idx == best_cross_dist_idx == best_dist_idx:
                        both_title += ", dz=%.2f" % (
                                l_cluster.get_3d_center()[-1] - delta_clusters[best_dist_idx].get_3d_center()[-1])

                    both_ax.set_title(both_title, loc='left', fontsize=16)
                    both_ax.set_title("")

            # both_ax.clear()
            # tmp_df = label_2d.debug_data[DebugTypes.predicted_cluster_from_bb]
            # tmp_df.plot(x="y", y="z", c="x", cmap="turbo", kind="scatter", s=2, ax=both_ax)
            break

        if not found and specific_cluster is not None:
            cropped_delta_ax = fig.add_subplot(gs[2, 0])
            cropped_delta_ax.axis(False)
            cluster = delta_clusters[int(specific_cluster)].get_pts().apply(pd.to_numeric, errors='coerce')
            process_lidar_to_image_on_ax_df(display_df=cluster, im=im.copy(), ax=cropped_delta_ax, points_size=16,
                                            specific_cNorm=cNorm)
            cropped_delta_ax.imshow(im)
            y_min = min([cluster["y_pixel"].min() - 100])
            y_max = max([cluster["y_pixel"].max() + 100])
            x_min = min([cluster["x_pixel"].min() - 100])
            x_max = max([cluster["x_pixel"].max() + 100])
            cropped_delta_ax.set_ylim(y_min, y_max)
            cropped_delta_ax.set_xlim(x_min, x_max)
            cropped_delta_ax.invert_yaxis()
            cropped_delta_ax.set_anchor('C')
            cropped_delta_ax.set_title("Delta | cmap=range [m]")

            both_ax = fig.add_subplot(gs[1:3, 1])
            both_ax.set_xlabel('X [m]\n', labelpad=5)
            both_ax.set_ylabel('Y [m]', labelpad=5)
            both_ax.set_title("Delta(red) | Lidar(blue) | BEV", pad=100)

            cluster = delta_clusters[int(specific_cluster)]
            # cluster.plot_bounding_box(ax=delta_ax)
            # cluster.plot_bounding_box(ax=ax_3d_lidar)
            plot_delta = cluster.get_pts()
            center = cluster.get_3d_center()
            if ax_3d_lidar is not None:
                ax_3d_lidar.add_patch(
                    plt.Circle((center[0], center[1]), 3, color='yellow', fill=False, zorder=0, linewidth=3))
            delta_ax.add_patch(plt.Circle((center[0], center[1]), 3, color='yellow', fill=False, zorder=0, linewidth=3))

            cluster.plot_bounding_cross(ax=both_ax)
            cNorm = colors.Normalize(vmin=np.min(plot_delta["total_power"]), vmax=np.max(plot_delta["total_power"]))
            scat = both_ax.scatter(plot_delta["x"], plot_delta["y"], c=plot_delta["total_power"], cmap="summer",
                                   s=self.point_size * 15, norm=cNorm)
            both_ax.scatter([center[0]], [center[1]], c="yellow", s=200, marker="X")
            plt.colorbar(scat, ax=both_ax, fraction=0.026, orientation='vertical', label="dB")
            both_ax.set_title("Delta(green) \nBEV | vel=" + str(np.linalg.norm(cluster.get_vel()).round(2)) + "m/s",
                              pad=90)
            add_rcs(df=plot_delta, ax=both_ax)
        elif not found and specific_cluster is None:
            if not draw_anyway:
                plt.close()
                return
            # plt.close()
            # No Label and no specific cluster
            # return
        label_idx = "None"
        cluster_idx = "None"
        if not found:
            cls = "unclassified"
            if specific_cluster is not None:
                cluster_idx = specific_cluster
        else:
            label_idx, label_2d = found
            cls = label_2d.cls
            cluster_idx = label_2d.associate_cluster
        plt.suptitle(engine.get_drive_name() + " | " + engine.get_id(idx=idx) + " | " + str(engine.start_idx + idx)
                     + "\ncluster_id=" + str(cluster_idx) + ", label_id=" + str(
            label_idx) + " | " + cls + "\n" + engine.get_dt_title(idx) + "\n" + str(title))

        plt.tight_layout()

        if od_im_ax is not None:
            logo = imageio.imread("/home/amper/AI/Harel/AI_Infrastructure/Plotting/image/square png_white logo.png")
            ax = plt.axes([0.0, 0.85, 0.12, 0.12], frameon=True)
            ax.imshow(logo)
            ax.axis(False)

        if classification_video_mode is None:
            suffix = "_%s_%s" % (cls, suffix)

        if "empty" in suffix:
            suffix = ""

        if save_path is None:
            save_path = video_dir + engine.get_id(idx=idx) + str(suffix) + ".png"
        plt.savefig(save_path)
        print("saved", save_path)

    def __del__(self):
        if self.container_name != -1:
            os.system("docker kill " + self.container_name)
            os.system("docker rm " + self.container_name)


def gen_instance_df(engine, debug=False):
    # alg = CameraLidarFusion(data_engine=engine)
    desired_keys = ["cat", "x", "y", "z", "x_proj", "y_proj", "z_proj", "r", "az", "el", "vel", "cluster_id", "score"]
    instances_df = pd.DataFrame(columns=list(engine.df.iloc[0].keys()) + desired_keys)
    part_count = 1
    [os.remove(x) for x in glob(engine.drive_context.aux_path + "/instances_3d_df_part*")]
    for engine_idx in tqdm(range(len(engine))):
        # s2 = timer()
        try:
            delta_clusters = engine.get_clusters(engine.get_processed_delta(engine_idx))
            label_path = engine.get_lidar_camera_fusion_labels(engine_idx, path_only=True, as_obj=False)
            labels_2d = engine.get_lidar_camera_fusion_labels(engine_idx, as_obj=False)
            used_clusters = set()
            for label in labels_2d:
                if "3d_center_clustering" not in label: continue
                if "associate_cluster_id" not in label: continue
                row = engine.df.iloc[engine_idx].to_dict()
                x, y, z = label["3d_center_clustering"]
                x_proj, y_proj, z_proj = label["projected_3d_center_clustering"]
                r, az, el = toRAzEl(x_proj, y_proj, z_proj)
                cluster_id = label["associate_cluster_id"]
                used_clusters.add(cluster_id)
                # cluster_distance = label.meta_data[MetaDataTypes.associate_cluster_distance.name]
                score = label["score"]
                cat = label["cat"]
                vel = np.linalg.norm(delta_clusters[cluster_id].get_vel())
                scope = locals()
                d = dict(((k, eval(k, scope)) for k in desired_keys))
                to_append = {**d, **row}
                instances_df = instances_df.append(to_append, ignore_index=True)

            # Add unclassified
            for cluster_idx, cluster in delta_clusters.items():
                if cluster.cluster_id in used_clusters:
                    continue
                # Add unclassified
                cluster_id = cluster.cluster_id
                row = engine.df.iloc[engine_idx].to_dict()
                vel = np.linalg.norm(cluster.get_vel())
                cat = "unclassified"
                score = 0
                x, y, z, x_proj, y_proj, z_proj, r, az, el, sub_cluster_id = None, None, None, None, None, \
                                                                             None, None, None, None, None
                scope = locals()
                d = dict(((k, eval(k, scope)) for k in desired_keys))
                to_append = {**d, **row}
                instances_df = instances_df.append(to_append, ignore_index=True)

            # labels2save = [label.get_dict() for label in labels_2d]
            # if not debug: save_obj(obj=labels_2d, path=label_path)

        except Exception as e:
            print(e)
            PrintException()
            if debug:
                exit()
            continue

        # e2 = timer()
        # print("whole_frame", e2 - s2, "\n")
        if len(instances_df) > 10000:
            print("writing part", part_count)
            instances_df.to_csv(engine.drive_context.aux_path + "/instances_3d_df_part_" + str(part_count) + ".csv")
            part_count += 1
            instances_df = pd.DataFrame(columns=list(engine.df.iloc[0].keys()) + desired_keys)

    # Write last one
    instances_df.to_csv(engine.drive_context.aux_path + "/instances_3d_df_part_" + str(part_count) + ".csv")
    dfs = [x for x in glob(engine.drive_context.aux_path + "/instances_3d_df_part*")]
    instances_df = pd.concat([pd.read_csv(x) for x in dfs])
    instances_df = instances_df.reset_index(drop=True)
    instances_df = instances_df.drop_duplicates(subset=['timestampRadar', 'cluster_id']).reset_index(drop=True)
    instances_df = instances_df.reset_index(drop=True)
    instances_df = instances_df.drop(
        columns=["idRadar", "filenameRadar", "inFileIdxRadar", "timeInt", "idCamera", "filenameCamera",
                 "inFileIdxCamera"])
    instances_df.drop(instances_df.filter(regex="Unname"), axis=1, inplace=True)
    instances_df.reset_index(drop=True).to_csv(engine.drive_context.instances_3d_df)


def run_camera_lidar_fusion_labeling(engine, force, debug=False, video=False, ensure_classes=None):
    print("\n\n######### Camera-Lidar-Fusion Labeling ######### \n\n engiine=", engine)
    alg = CameraLidarFusion(data_engine=engine)
    os.makedirs("%s" % engine.drive_context.camera_to_lidar_clusters, exist_ok=True)
    for engine_idx in tqdm(range(len(engine))):
        try:
            label_path = engine.drive_context.camera_to_lidar + "/camera_to_lidar_" + engine.get_lidar_id(idx=engine_idx) + ".json"
            if (not debug) and (not force) and os.path.exists(label_path):
                continue
            labels_2d = engine.get_2d_label(engine_idx)
            if ensure_classes is not None:
                ped_flag = False
                for label in labels_2d:
                    if label.cls in ensure_classes:
                        ped_flag = True
                        break
                if not ped_flag:
                    continue

            # Data
            im = engine.get_image(engine_idx)
            lidar_df = engine.get_lidar_df(engine_idx)
            lidar_df = lidar_df.apply(pd.to_numeric, errors='coerce')
            processed_delta = engine.get_processed_delta(engine_idx)
            delta_clusters = engine.get_clusters(p_delta=processed_delta)
            lidar_camera_df = lidar_df[lidar_df['x_lidar_camera'].notna()]

            labels_2d = alg.run_algorithm(labels_2d=labels_2d, lidar_df=lidar_camera_df, delta_clusters=delta_clusters,
                                          path=engine.get_processed_delta(engine_idx, path_only=True))

            labels2save = [label.get_dict() for label in labels_2d]
            # for l in labels2save:
            #     for k, v in l.items():
            #         print(k, type(v), v)
            # print(labels2save)
            if not debug: save_obj(obj=labels2save, path=label_path)

            if not (debug or video or engine_idx % alg.plot_every == 0): continue
            if alg.display_instances:
                alg.plot_instances(engine=engine, idx=engine_idx, im=im.copy(),
                                   labels_2d=labels_2d, lidar_df=lidar_df, delta_clusters=delta_clusters,
                                   ensure_classes=ensure_classes)
            if alg.display_frame:
                alg.plot(engine=engine, idx=engine_idx, im=im, labels_2d=labels_2d, lidar_df=lidar_df,
                         delta_clusters=delta_clusters, processed_delta=processed_delta)
                if video:
                    plt.savefig(video_dir + engine.get_id(idx=engine_idx) + ".png")
                else:
                    try:
                        plt.savefig(
                            engine.drive_context.camera_to_lidar_images + engine.get_id(idx=engine_idx) + ".png")
                    except Exception as e:
                        continue

        except Exception as e:
            PrintException()
            if debug:
                exit()
            continue
    # if video: make_video_from_dir(df=engine.df, images_dir=video_dir, suffix="_" + str(time.time()).split(".")[0])
    del alg
    return


def foo(engine: WisenseEngine):
    """
    Dont sample engine!
    @param engine:
    @return:
    """
    if args.full or args.labels_only:  # 3D
        run_camera_lidar_fusion_labeling(engine=engine, force=args.force, debug=args.debug, video=False)
    if args.full or args.ass_only:  # Classification
        gen_instance_df(engine)


def run_ensemble(engine: WisenseEngine):
    for idx in tqdm(range(len(engine))):
        label_path = engine.get_ensemble_3d(idx=idx, as_obj=False, path_only=True)
        os.makedirs(os.path.split(label_path)[0], exist_ok=True)
        clf = engine.get_lidar_camera_fusion_labels(idx, True)
        ssd3d = engine.get_lidar_3dssd(idx, True)

        finals = []
        [finals.append(x) for x in ssd3d if x.score >= 0.4]
        [finals.append(x) for x in clf if x.score >= 0.4]

        for x in ssd3d:
            for y in clf:
                iou = Label.get_2d_iou(x, y)
                if iou > 0.25:
                    x.score = x.score * max(1, (x.score + y.score)/2)
                    finals.append(x)

        bboxes = np.array([x.get_bev_bbox() for x in finals])
        indices = np.array(non_max_suppression_fast(bboxes, 0.8))
        finals = np.asarray(finals)

        if len(indices) > 0:
            finals = finals[indices]
        labels2save = [x.get_dict() for x in finals]
        save_obj(obj=labels2save, path=label_path)


def gen_csv():
    print("Generating CSV's!")
    engines = get_engines(val_train="val", randomize=False, envelope_active=False)
    with Pool(6) as p:
        p.map(gen_instance_df, engines)


def draw_ts(base_path, ts, cluster_id, title, save_path=None, thin=False):
    """

    @param base_path: base path for drive etc: /workspace/data_backup7/76mWF/DRIVE_NAME/
    @param ts: radar timestamp
    @param cluster_id: cluster to draw
    @param title: added title etc: "prediction is Amazing"
    @param save_path: where to save, abs path
    @return:
    """
    # global engines
    # if engines is None:
    #     engines = get_engines(val_train="all", randomize=False, envelope_active=True)
    # engine, engine_idx = get_from_ts(engines=engines, ts=str(ts))
    engine = WisenseEngine(base_path_drive=base_path, base_name="Wisense", rect_mode=True, envelope_active=True,
                           df=None)

    engine_idx = engine.get_idx_from_ts(ts=ts)
    if engine_idx is None:
        print("Warning | ts not in drive path\n", "ts=", ts, "base_path=", base_path)
        return
    alg = CameraLidarFusion(data_engine=engine, thin=thin)
    alg.plot_instances(engine=engine, idx=engine_idx, im=None,
                       labels_2d=alg.run_engine_idx(engine, engine_idx), lidar_df=None, delta_clusters=None,
                       ensure_classes=None,
                       specific_cluster=cluster_id, title=title, save_path=save_path)
    plt.close()


def draw_examples():
    # engines = get_engines(val_train="all", envelope_active=True, randomize=True)
    engines = get_engines(envelope_active=True, randomize=True, df="instances_3d_df.csv")
    # engines = get_engines(val_train="val")
    # engines = np.random.permutation(engines+get_engines(val_train="val"))
    # engines = [get_engines(val_train="val")[-1]]

    # with Pool(3) as p:
    #     p.map(foo, engines)

    # "bicycle" or label.cls == "cyclist" or label.cls == "pedestrian"
    ensure_classes = {"bicycle": True, "cyclist": True}
    # ensure_classes = None
    for data_engine in engines:
        # data_engine.sample_df(200)
        # data_engine.df = data_engine.df[(data_engine.df["r"] > 30)]
        # data_engine.df = data_engine.df.reset_index(drop=True)
        # data_engine.sample_df(min(20, len(data_engine.df)))
        data_engine.sample_df(150)
        run_camera_lidar_fusion_labeling(engine=data_engine, force=True, debug=True, video=True,
                                         ensure_classes=ensure_classes)
        # try:
        #     associate_and_gen_instance_df(data_engine)
        # except Exception as e:
        #     PrintException()
        #     continue


def main():
    if False:
        _engines = get_engines(envelope_active=True, randomize=True, df="instances_3d_df.csv")
        classes = pd.unique(_engines[0].df["cat"])
        for class_idx, engine in enumerate(_engines):
            engine.df = engine.df[engine.df["cat"] == classes[class_idx % len(classes)]].reset_index(drop=True)
            engine.sample_df(1)
            idx = 0
            ts = engine.get_id(idx=idx)
            base_path = engine.base_path
            clusters = engine.get_clusters(engine.get_processed_delta(idx=idx))
            draw_ts(base_path=base_path, ts=ts, cluster_id=engine.df.loc[idx, "cluster_id"], title="randomized")
            # draw_ts(ts="1599042493847394369", cluster_id="9", title="Some title")
            # draw_ts(ts="1598876389493733238", cluster_id="25", title="Some title")
            # draw_ts(ts="1599042858566713735", cluster_id="8", title="Unclassified")
            # draw_ts(ts="1599042858566713735", cluster_id="6", title="Unclassified")


if __name__ == "__main__":
    # python3 camera_lidar_fusion.py --p 4 --engines all --full --force
    # python3 camera_lidar_fusion.py --p 4 --engines all --fusion --force
    # python3 camera_lidar_fusion.py --p 4 --engines all --labels-only --force

    parser = argparse.ArgumentParser(description='Evaluation arguments')
    parser.add_argument('--full', action='store_true', default=False, help="path to specific dataframe")
    parser.add_argument('--ass-only', action='store_true', default=False, help="path to specific dataframe")
    parser.add_argument('--labels-only', action='store_true', default=False, help="path to specific dataframe")
    parser.add_argument('--fusion', action='store_true', default=False, help="path to specific dataframe")

    parser.add_argument('--debug', action='store_true', default=False, help="path to specific dataframe")
    parser.add_argument('--force', action='store_true', default=False, help="path to specific dataframe")
    parser.add_argument('--p', default=1, type=int, help="specify GPU to use")
    parser.add_argument('--engines', type=str, required=True, help='specify a ckpt directory to be evaluated if needed')
    parser.add_argument('--drive', type=str, default=None, help='')
    args = parser.parse_args()

    engines = get_engines(wf="92", val_train=args.engines, randomize=False, envelope_active=False, drive_str=args.drive)
    if args.fusion:
        with Pool(args.p) as p:
            p.map(run_ensemble, engines)

    if args.full or args.labels_only or args.ass_only:
        with Pool(args.p) as p:
            p.map(foo, engines)
