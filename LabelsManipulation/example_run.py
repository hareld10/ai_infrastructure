from matplotlib import gridspec, colors

from Calibrator.calibrator_utils import plot_pc_simple, process_lidar_to_image_on_ax_simple, process_lidar_to_image_on_ax, get_3d_ax
from DataPipeline.src.GetEngines import get_engines
from wiseTypes.classes_dims import Road
from tqdm import tqdm
import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd
import time
from scipy.spatial import distance
import scipy as sp
import scipy.interpolate
from sklearn.cluster import DBSCAN
from DataPipeline.shared_utils import PrintException, toRAzEl, pickle_write, is_docker
from Evaluation.evaluation_utils import make_video_from_dir
from OpenSource.WisenseEngine import WisenseEngine
from scipy.interpolate import interp1d, interp2d
import matplotlib

# matplotlib.use('pdf')
plt.style.use('dark_background')


def example_run(engine):
    for engine_idx in tqdm(range(len(engine))):
        try:
            # Get Data
            im = engine.get_image(engine_idx)
            lidar_df = engine.get_lidar_df(engine_idx)
            lidar_df = lidar_df.apply(pd.to_numeric, errors='coerce')
            processed_delta = engine.get_processed_delta(engine_idx)
            processed_delta = engine.process_first_hit(processed_delta, az_bin_deg=0.1)
            delta_clusters = engine.get_clusters(p_delta=processed_delta)

            gs = gridspec.GridSpec(ncols=2, nrows=1)
            fig = plt.figure(figsize=(24, 13))
            plt.tight_layout(pad=2)
            im_ax = fig.add_subplot(gs[0, 0])
            im_ax.axis(False)
            im_ax.imshow(im)
            im_ax.set_title("Camera", pad=20)

            delta_ax = get_3d_ax(fig=fig, loc=gs[0, 1], x_left=-45, x_right=45, ax3d=False)
            for cluster_idx, cluster in delta_clusters.items():
                cluster_df = cluster.cluster_df
                cluster_color = cluster.cluster_color
                delta_ax.scatter(cluster_df['x'], cluster_df['y'], color=cluster_color, s=1)
                cluster_center = cluster.get_3d_center()
                delta_ax.annotate(str(cluster.cluster_id), (cluster_center[0], cluster_center[1]), color=cluster_color, fontsize=12)
                delta_ax.set_title("Delta | cmap=id", pad=30)

            plt.show()
            exit()

        except Exception as e:
            PrintException()
            exit()
            continue


if __name__ == '__main__':
    engines = np.random.permutation(get_engines())
    example_run(engines[0])
