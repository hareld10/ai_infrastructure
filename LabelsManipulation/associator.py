import numpy as np
import pandas as pd
from wiseTypes.Cluster import Cluster


class Associator:
    def __init__(self, engine, iou_val=0.05, cross_dist_val=0.6, distance_from_center=0.6):
        self.engine = engine
        self.iou_val = iou_val
        self.cross_dist_val = cross_dist_val
        self.distance_from_center = distance_from_center
        pass

    def associate(self, label, delta_clusters):
        match_idx = -1

        box_points = label.get_box_points()
        box_points[:, :3] = self.engine.project_lidar_to_radar(box_points, filter_fov_flag=False)[:, :3]

        cluster_center = np.asarray([label.tx, label.ty, label.tz])
        cluster_center = self.engine.project_lidar_to_radar(np.asarray([cluster_center]), filter_fov_flag=True)
        if cluster_center.shape[0] > 0:
            cluster_center = cluster_center[0][:3]
        else:
            return

        l_cluster = Cluster(cluster_df=pd.DataFrame(data={"x": box_points[:, 0], "y": box_points[:, 1], "z": box_points[:, 2]}), power_key=None)

        best_iou_idx, best_iou_val = l_cluster.get_best_iou(list(delta_clusters.values()))
        if best_iou_val >= self.iou_val:
            match_idx = best_iou_idx

        best_cross_dist_idx, best_cross_dist_val = l_cluster.get_best_cross_distance(list(delta_clusters.values()))
        if best_cross_dist_val <= self.cross_dist_val:
            match_idx = best_cross_dist_idx

        sorted_clusters = sorted(delta_clusters, key=lambda x: np.linalg.norm(delta_clusters[x].get_3d_center() - cluster_center))
        sorted_cluster: Cluster = delta_clusters[sorted_clusters[0]]

        distance_from_center = np.linalg.norm(sorted_cluster.get_3d_center() - cluster_center)
        if distance_from_center <= self.distance_from_center:
            match_idx = sorted_cluster.cluster_id

        label.associate_cluster = int(match_idx)
        label.annotate = str(match_idx)

        if match_idx == -1:
            label.write_data["fail_cause"] = "Spatial association fail"
            label.write_data["best_iou_val"] = best_iou_val
            label.write_data["best_3d_cog_distance"] = best_cross_dist_val
            label.write_data["best_3d_com_distance"] = distance_from_center


