import pathlib
import sys

from LabelsManipulation.lidar_size_estimation.pointnet2_utils import radar_to_lidar_coordinates, lidar_to_radar_coordinates

module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, module_path)

import pointnet2_cls_ssg as pointnet2
import torch
import numpy as np

base_dir = "/workspace/HDD/Harel/lidar/lidar_size_estimation/"


class SizeEstimator:
    def __init__(self):
        self.model = pointnet2.get_model().cuda(1)
        self.model.load_state_dict(torch.load(base_dir + "/pointnet2_veh_epoch_5_iter_3000.pth",map_location=lambda storage, loc: storage))
        pass

    def predict(self, model_input):
        if model_input.shape[0] <= 128:
            model_input = model_input[np.random.randint(model_input.shape[0], size=128), :]

        model_input = radar_to_lidar_coordinates(model_input)
        model_input = torch.from_numpy(np.asarray([model_input.T])).cuda(1).float()

        print("input shape", model_input.shape)
        with torch.no_grad():
            model_results = self.model.eval()(model_input)[0]
        tx, ty, tz, p_length, p_width, p_height, p_heading = model_results.cpu().numpy()[0]
        flip = lidar_to_radar_coordinates(np.asarray([[tx, ty, tz]]))
        ret_dict = {}
        ret_dict["tx"] = flip[0, 0]
        ret_dict["ty"] = flip[0, 1]
        ret_dict["tz"] = flip[0, 2]
        ret_dict["length"] = p_length
        ret_dict["width"] = p_width
        ret_dict["height"] = p_height
        ret_dict["heading"] = p_heading
        return ret_dict

    @staticmethod
    def plot(ax, ret_dict, color="green"):
        print("plotting", ret_dict)
        cx, cy = ret_dict["tx"], ret_dict["ty"]  # meta_data["tx"].numpy()[0], meta_data["ty"].numpy()[0]
        dx, dy = ret_dict["length"], ret_dict["width"]  # meta_data["sl_x"].numpy()[0], meta_data["sw_y"].numpy()[0]
        heading = ret_dict["heading"]  # meta_data["heading"].numpy()[0] + np.pi / 2
        c = np.cos(heading)
        s = np.sin(heading)
        rot_mat = [[c, -s], [s, c]]
        center = [cx, cy]
        p1 = center + np.dot(rot_mat, [dy / 2, dx / 2])
        p2 = center + np.dot(rot_mat, [dy / 2, -dx / 2])
        p3 = center + np.dot(rot_mat, [-dy / 2, -dx / 2])
        p4 = center + np.dot(rot_mat, [-dy / 2, dx / 2])
        pts = [p1, p2, p3, p4]
        pairs = [[0, 1], [1, 2], [2, 3], [3, 0]]
        for ind in pairs:
            ax.plot([pts[ind[0]][0], pts[ind[1]][0]], [pts[ind[0]][1], pts[ind[1]][1]], c=color)
