import numpy as np


def get_points(cx, cy, dx, dy, heading):
    c = np.cos(heading)
    s = np.sin(heading)
    rot_mat = [[c, -s], [s, c]]
    center = [cx, cy]
    p1 = center + np.dot(rot_mat, [dy / 2, dx / 2])
    p2 = center + np.dot(rot_mat, [dy / 2, -dx / 2])
    p3 = center + np.dot(rot_mat, [-dy / 2, -dx / 2])
    p4 = center + np.dot(rot_mat, [-dy / 2, dx / 2])
    pts = [p1, p2, p3, p4]
    return pts


def draw(cx, cy, dx, dy, heading, ax2plot, color):
    pts = get_points(cx, cy, dx, dy, heading)
    pairs = [[0, 1], [1, 2], [2, 3], [3, 0]]
    for ind in pairs:
        ax2plot.plot([pts[ind[0]][0], pts[ind[1]][0]], [pts[ind[0]][1], pts[ind[1]][1]], c=color)
    return




# val_data_loader = DataLoader(dataset=val_dataset, num_workers=8, batch_size=1, shuffle=True, drop_last=True)
# val_pbar = tqdm(iter(val_data_loader), leave=False, total=len(val_data_loader), ncols=150)
# val_loss = 0
# for val_iteration, val_batch in enumerate(val_pbar, 1):
#     model_input = val_batch['model_input'].cuda(gpu)
#     label = val_batch['label'].cuda(gpu)
#
#     p_length, p_width, p_heading = 0, 0, 0

# with torch.no_grad():
#     model_results = model.eval()(model_input)[0]
#     if loss_type == iou_3d:
#         model_result = model_results.unsqueeze(1)
#         label = label.unsqueeze(1)
#     # print("\nval= model_result", model_result.shape, "label", label.shape)
#     iou_loss, iou = loss(model_result, label)
#     mse_loss = mse_loss_func(model_result, label)
#     if loss_type == iou_3d:
#         loss_batch = 0.5 * torch.mean(iou_loss) + 0.5 * torch.mean(mse_loss)
#         tx, ty, tz, p_length, p_width, p_height, p_heading = model_results.cpu().numpy()[0]
#
#     val_loss = 0.9 * val_loss + 0.1 * loss_batch.item()
# tx, ty, tz,  = model_results.cpu().numpy()[0]

#     if val_iteration % plot_every_val == 0:
#         meta_data = val_batch["metadata"]
#         fig = plt.figure(figsize=(15, 8))
#         gs = gridspec.GridSpec(ncols=1, nrows=1)
#         ax = fig.add_subplot(gs[0, 0])
#         model_input = model_input.cpu().numpy()
#         scat = ax.scatter(model_input[:, 0], model_input[:, 1], s=3, cmap="jet")
#
#         draw(cx=meta_data["tx"].numpy()[0], cy=meta_data["ty"].numpy()[0],
#              dx=meta_data["sl_x"].numpy()[0], dy=meta_data["sw_y"].numpy()[0],
#              heading=meta_data["heading"].numpy()[0] + np.pi / 2, color="b", ax2plot=ax)
#         ax.scatter(meta_data["tx"], meta_data["ty"], s=6, color="b", label="label")
#         title = ""
#         title += "label= tx=%s, ty=%s, tz=%s l=%s w=%s h=%s heading=%s" % (meta_data["tx"].numpy().round(2)[0], meta_data["ty"].numpy().round(2)[0],
#                                                                            meta_data["tz"].numpy().round(2)[0],
#                                                                            meta_data["sl_x"].numpy().round(2)[0], meta_data["sw_y"].numpy().round(2)[0],
#                                                                            meta_data["sh"].numpy().round(2)[0], meta_data["heading"].numpy().round(2)[0])
#
#         if loss_type == iou_3d:
#             title += "\ntx=%s ty=%s tz=%s l=%s w=%s h=%s heading=%s" % (tx.round(2), ty.round(2), tz.round(2),
#                                                                         p_length.round(2), p_width.round(2), p_height.round(2), p_heading.round(2))
#         ax.set_title(title)
#         if p_heading is not None:
#             draw(cx=tx, cy=ty, dx=p_length, dy=p_width, heading=p_heading + np.pi / 2, color="r", ax2plot=ax)
#         ax.scatter(tx, ty, s=6, color="r", label="prediction")
#         ax.legend()
#         plt.suptitle("size estimation | frame %s" % meta_data["frameId"].numpy())
#         plt.savefig(module_path + "/results/epoch_%d_sample_%d.png" % (epoch, val_iteration))
#         plt.close()
#
# plotter.plot('loss', 'val', loss_type, epoch, val_loss)

# if False and iteration % 200 == 0:  # Model Input
#     meta_data = batch["metadata"]
#     fig = plt.figure(figsize=(10, 6))
#     gs = gridspec.GridSpec(ncols=1, nrows=1)
#     ax = fig.add_subplot(gs[0, 0])
#     draw_model_input = model_input.cpu().numpy()
#     scat = ax.scatter(draw_model_input[:, 0], draw_model_input[:, 1], s=3, cmap="jet", )
#     draw(cx=meta_data["tx"].numpy()[0], cy=meta_data["ty"].numpy()[0],
#          dx=meta_data["sl_x"].numpy()[0], dy=meta_data["sw_y"].numpy()[0],
#          heading=meta_data["heading"].numpy()[0] + np.pi / 2, color="b", ax2plot=ax)
#     ax.scatter(meta_data["tx"], meta_data["ty"], s=6, color="b", label="label")
#     ax.legend()
#     plt.title("size estimation | frame %s" % meta_data["frameId"].numpy())
#     plt.savefig(module_path + "/results/model_input_epoch_%d_sample_%d.png" % (epoch, iteration))
#     plt.close()
