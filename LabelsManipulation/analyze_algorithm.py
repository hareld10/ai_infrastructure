import argparse
import json
from copy import deepcopy
from pprint import pprint

from DataPipeline.src.GetEngines import get_engines, get_from_ts
from LabelsManipulation.associator import Associator
from LabelsManipulation.camera_lidar_fusion import CameraLidarFusion
import pandas as pd
from Evaluation.evaluation_utils import make_video_from_dir
from wiseTypes.Types import DebugTypes
import pathlib
from DataPipeline.pipeline4 import Ensemble3D
module_path = str(pathlib.Path(__file__).parent.absolute())
video_dir = module_path + "/lidar_camera_fusion/"
import time
import numpy as np
from tqdm import tqdm
from collections import defaultdict
import os

# python3 LabelsManipulation/analyze_algorithm.py --d highway --v --tid 143 --sample 1718-1873
# python3 LabelsManipulation/analyze_algorithm.py --v --d 200901_lidar_drive_1_W92_M15_RF1 --tid 2773 --sample 15028-15139


def analyze_engine(engine, track_mode=False, c_mode=None, classes=None, cur_track_id=None, mode=None):
    alg = CameraLidarFusion(data_engine=engine, debug=False, config="v2_full", se_docker=77)
    alg.size_estimation = True
    print("Ensure classes: ", classes)
    title = ""
    # c_mode = {"pred_df": pd.read_csv("/workspace/HDD/Meir/radar/radar_classification/%s.csv" % engine.get_drive_name())}
    plotter_frames = 0
    first = True if cur_track_id is None else False
    s = SingleAssociation()
    ass = Associator(engine=engine)
    ensemble = Ensemble3D(engine=engine)
    for engine_idx in tqdm(range(len(engine))):
        if classes is not None:
            labels_2d = engine.get_2d_label(engine_idx)
            flag = False
            for l in labels_2d:
                if l.cls in classes:
                    flag = True
                    break
            if not flag:
                continue

        im = engine.get_image(engine_idx)
        lidar_df = engine.get_lidar_df(engine_idx)
        lidar_df = lidar_df.apply(pd.to_numeric, errors='coerce')
        processed_delta = engine.get_processed_delta(engine_idx)
        delta_clusters = engine.get_clusters(p_delta=processed_delta)
        # labels_3d = engine.get_ensemble_3d(engine_idx, True)

        labels_3d = ensemble.run_engine_idx(engine=engine, engine_idx=engine_idx, kind="ssn")
        pprint(labels_3d)
        labels_2d = engine.get_2d_label(engine_idx, True)

        labels_3d, labels_2d = engine.filter_3d_by_2d(labels_3d, labels_2d)
        # [ass.associate(x, delta_clusters) for x in labels_3d]

        if track_mode:
            print("Current track id", cur_track_id)
            tracks_ids = [x.get_track_id() for x in
                          sorted(delta_clusters.values(), key=lambda x: np.linalg.norm(x.get_3d_center()))
                          if x.get_track_id() != -1]

            if len(tracks_ids) == 0:
                print("Skipping - No relevant track id found")
                continue
            if track_mode and cur_track_id is None or cur_track_id not in tracks_ids:
                if first:
                    cur_track_id = np.random.choice(tracks_ids)
                    first = False
                else:
                    print("End of current track_id", cur_track_id, "out of", tracks_ids)
                    return
                print("getting new_track_id", cur_track_id, "out of", tracks_ids)

        processes_labels_2d = alg.run_engine_idx(engine, engine_idx, force=True, write=False)
        for p_l2d in processes_labels_2d:
            p_l2d.heading += np.pi /2

        pass_processes_labels_2d = processes_labels_2d
        pass_labels_3d = labels_3d
        pass_labels_2d = labels_2d

        for p_l_idx, p_label in enumerate(processes_labels_2d):
            if track_mode:
                if DebugTypes.delta_cluster.name not in p_label.debug_data:
                    continue
                track_idx = p_label.debug_data[DebugTypes.delta_cluster.name].get_track_id()
                if track_idx != cur_track_id:
                    continue

            if mode == "draw_ass_only" or mode == "dao":
                if DebugTypes.delta_cluster.name not in p_label.debug_data:
                    continue

            if mode == "check_single_association" or mode == "csa":
                ans = s.check_single_association(p_label, p_l_idx)
                if ans is None:
                    continue
                title, p_l_idx = ans
                pass_processes_labels_2d = None
                pass_labels_3d = None
                pass_labels_2d = None

            print(p_label)
            alg.plot_instances(engine=engine, idx=engine_idx, im=im.copy(),
                               labels_2d=[p_label], lidar_df=lidar_df, delta_clusters=delta_clusters,
                               suffix="empty" if track_mode else f'_{p_l_idx}', title=title,
                               classification_video_mode=c_mode, ensure_classes=classes,
                               draw_anyway=True,
                               draw_all_labels_2d=pass_labels_2d,
                               draw_all_labels_3d=pass_labels_3d,
                               draw_all_labels_clf=pass_processes_labels_2d,
                               )
            plotter_frames += 1
            if track_mode:
                break

    print("\n\nplotted", plotter_frames, "out of", len(engine))


class SingleAssociation:
    def __init__(self):
        self.prefixes = defaultdict(int)

    def check_single_association(self, p_label, p_l_idx):
        # Check single association
        if "association_meta" not in p_label.write_data:
            return
        ass = p_label.write_data["association_meta"]
        s = sum(ass["sum"])
        if s != 1:
            return
        prefix = "".join([str(int(x)) for x in ass["sum"]])
        if self.prefixes[prefix] >= 20:
            return
        self.prefixes[prefix] += 1
        # print(prefix)
        # if prefix != "0001":
        #     return

        p_l_idx = f"_{p_l_idx}_{prefix}"
        title = str([round(x, 2) for x in ass["val"]])

        return title, p_l_idx


def find_tracking_seq(engine):
    print("In find tracks", engine.get_drive_name())
    tracks = defaultdict(int)
    tracks_frames = defaultdict(list)
    track_cache_name = f"{engine.drive_context.aux_path}/tracks_ids_{engine.start_idx}_{engine.end_idx}.json"
    if os.path.exists(track_cache_name):
        print("loading tracks from cache\n", track_cache_name)
        with open(track_cache_name, "r") as f:
            to_run = json.load(f)
            print("Loaded\n", to_run)
            return to_run

    for engine_idx in tqdm(range(len(engine))):
        processed_delta = engine.get_processed_delta(engine_idx)
        delta_clusters = engine.get_clusters(p_delta=processed_delta)

        id_to_trk_map = defaultdict(lambda: -1)
        for cluster_id, cluster in delta_clusters.items():
            trk_id = cluster.get_track_id()
            if trk_id != -1:
                id_to_trk_map[trk_id] = cluster.get_track_id()

        labels = engine.get_lidar_camera_fusion_labels(idx=engine_idx, as_obj=True)
        for label in labels:
            if label.associate_cluster not in delta_clusters:
                continue
            trk_id = delta_clusters[label.associate_cluster].get_track_id()
            tracks[trk_id] += 1
            tracks_frames[trk_id].append(engine_idx)

    tracks = dict(sorted(tracks.items(), key=lambda item: item[1], reverse=True))

    to_run = []
    for trk_idx, global_app in tracks.items():
        s = min(tracks_frames[trk_idx]) + engine.start_idx
        s_rel = min(tracks_frames[trk_idx])
        e = max(tracks_frames[trk_idx]) + engine.start_idx
        e_rel = max(tracks_frames[trk_idx])

        # if global_app <= 80:
        #     continue
        print(f"trk_id={trk_idx}, total={e - s}/{global_app}, s={s}, e={e}")
        to_run.append((float(trk_idx), float(s), float(e), float(s_rel), float(e_rel), float(global_app)))

    with open(track_cache_name, "w") as f:
        json.dump(to_run, f)
    return to_run

def main():
    print("Analyze algorithm!")
    parser = argparse.ArgumentParser(description='Evaluation arguments')
    # skip analyzing and got straight to video
    parser.add_argument('--skip', action='store_true', default=False, help="path to specific dataframe")
    # classification video mode
    parser.add_argument('--c', action='store_true', default=False, help="path to specific dataframe")
    # track mode
    parser.add_argument('--t', action='store_true', default=False, help="path to specific dataframe")
    # find tracks mode
    parser.add_argument('--find_tracks', action='store_true', default=False, help="path to specific dataframe")
    # track specific id
    parser.add_argument('--tid', default=None, type=int, help="track id")
    # video
    parser.add_argument('--v', action='store_true', default=False, help="path to specific dataframe")
    # drive str
    parser.add_argument('--d', type=str, default=None, help='')
    # sample df multi-choice
    parser.add_argument('--sample', default=None, type=str, help="specify GPU to use")
    # ensure classes
    parser.add_argument('--cls', default=None, type=str, help="specify GPU to use")
    # draw sepcific timestamp
    parser.add_argument('--ts', default=None, type=str, help="specify GPU to use")
    # mode draw_ass_only, check_single_association
    parser.add_argument('--m', default=None, type=str, help="specify GPU to use")

    parser.add_argument('--wf', default="92", type=str, help="specify GPU to use")
    parser.add_argument('--df', default=None, type=str, help="specify GPU to use")
    args = parser.parse_args()

    engines = get_engines(wf=args.wf, df=args.df, val_train="all", randomize=True, envelope_active=True, drive_str=args.d)
    print(engines)
    if args.ts is not None:
        engine, idx = get_from_ts(engines=engines, ts=args.ts)
        engine.update_df_range(idx, idx + 1)
        engines = [engine]

    if args.tid: args.t = True

    for engine in engines:
        if args.sample:
            if "-" in args.sample:
                s, e = args.sample.split("-")
                engine.update_df_range(int(s), int(e))
            elif "r" in args.sample:
                engine.sample_df_range(int(args.sample[1:]))
            else:
                engine.sample_df(int(args.sample))

        if args.find_tracks:
            to_run = find_tracking_seq(engine)
            old_df = engine.df.copy()
            if True:
                for trk_id, s, e, s_rel, e_rel,global_app in to_run:
                    engine.df = old_df.copy()
                    if trk_id == -1: #or global_app <= 140:
                        print("not good enough")
                        continue
                    else:
                        print("find_tracks: Going on ", trk_id, s, e)
                    engine.update_df_range(s_rel, e_rel)
                    print("len engine", engine)
                    analyze_engine(engine, track_mode=True, cur_track_id=trk_id)
                    make_video_from_dir(df=engine.df, images_dir=video_dir, suffix="_" + str(time.time()).split(".")[0], frame_rate=5)
                    print("Write video for track id", trk_id, engine.get_drive_name())
            continue

        if not args.skip: analyze_engine(engine, track_mode=args.t, cur_track_id=args.tid,
                                         classes=None if args.cls is None else args.cls.split(","), mode=args.m)
        if args.v:
            make_video_from_dir(df=engine.df, images_dir=video_dir, suffix="_" + str(time.time()).split(".")[0], frame_rate=4)


if __name__ == "__main__":
    main()
