from matplotlib import gridspec, colors

from Calibrator.calibrator_utils import plot_pc_simple, process_lidar_to_image_on_ax_simple, process_lidar_to_image_on_ax, process_lidar_to_image_on_ax_df
from DataPipeline.src.GetEngines import get_engines
from wiseTypes.classes_dims import Road
from tqdm import tqdm
import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd
import time
from scipy.spatial import distance
import scipy as sp
import scipy.interpolate
from sklearn.cluster import DBSCAN
from DataPipeline.shared_utils import PrintException, toRAzEl, pickle_write
from Evaluation.evaluation_utils import make_video_from_dir
from OpenSource.WisenseEngine import WisenseEngine
from scipy.interpolate import interp1d, interp2d
import matplotlib
from scipy.interpolate import griddata
from multiprocessing import Pool

# matplotlib.use('pdf')
plt.style.use('dark_background')
from wiseTypes.classes_dims import Road

kw_args = {"spatial_calibration_path": "/home/amper/AI/Harel/AI_Infrastructure/Calibrator/cali_200909_pitch_fix"}
font = {'size': 20}
matplotlib.rc('font', **font)


def infer_lidar_road(engine, force=True, plot_every=200, debug=False, video=False):
    print("\n\n######### Lidar Road Seg #########")
    for engine_idx in tqdm(range(len(engine))):
        try:
            label_path = engine.drive_context.lidar_road_seg_df + "/lidar_road_seg_df_" + engine.get_lidar_id(idx=engine_idx) + ".csv"
            os.makedirs(os.path.split(label_path)[0], exist_ok=True)
            if not force and os.path.exists(label_path): continue

            road_seg_df = engine.get_lidar_df(idx=engine_idx, filter_by_seg_class=Road(), keep_semantic=True)

            if not debug:
                road_seg_df.to_csv(label_path)

            if engine_idx % plot_every != 0: continue
            im = engine.get_image(engine_idx)
            seg = engine.get_segmentation_label(idx=engine_idx)

            plt.close()
            gs = gridspec.GridSpec(nrows=3, ncols=3)
            fig = plt.figure(figsize=(23, 15))

            # Display seg
            ax = fig.add_subplot(gs[0, 2])
            ax.imshow(im)
            ax.set_title("Camera Segmentation")
            ax.imshow(seg.get_img(colors=True), alpha=0.4)
            ax.axis("off")
            ax.set_title("Camera | Lidar | Heatmap=range [m]")
            # process_lidar_to_image_on_ax_simple(imgfov_pc_pixel, imgfov_pc_velo[:, s_idx:e_idx], im.copy(), ax, points_size=0.2)
            full_lidar = engine.get_lidar_df(idx=engine_idx)
            process_lidar_to_image_on_ax_df(full_lidar[full_lidar["x_pixel"].notna()], im=im.copy(), ax=ax, points_size=0.2)

            ax = fig.add_subplot(gs[:, :2])
            process_lidar_to_image_on_ax_df(road_seg_df, im=im.copy(), ax=ax, points_size=0.2)
            # process_lidar_to_image_on_ax_simple(imgfov_pc_pixel_road, imgfov_pc_velo_road[:, s_idx:e_idx], im.copy(), ax, points_size=1)
            ax.imshow(im)
            ax.set_title("Lidar Road | Projection | Heatmap=range[m]")
            ax.axis("off")

            # ax = fig.add_subplot(gs[1, 2])
            # process_lidar_to_image_on_ax_simple(imgfov_pc_pixel_road_not, imgfov_pc_velo_road_not[:, s_idx:e_idx], im.copy(), ax, points_size=1)
            # ax.imshow(im)
            # ax.set_title("Lidar Not Road | Projection | Heatmap=range[m]")
            # ax.axis("off")

            ax = fig.add_subplot(gs[1, 2])
            cNorm = colors.Normalize(vmin=road_seg_df["z"].min(), vmax=road_seg_df["z"].max())
            scat = ax.scatter(road_seg_df["x"], road_seg_df["y"], c=road_seg_df["z"], cmap="jet", s=4, norm=cNorm)
            plt.colorbar(scat, ax=ax, orientation='horizontal')
            ax.set_title("Lidar Road | Scatter | Heatmap=elevation [m]", pad=15)
            ax.set_xlim(-40, 40)
            ax.set_ylim(0, 100)

            ax = fig.add_subplot(gs[2, 2])
            cNorm = colors.Normalize(vmin=full_lidar["z"].min(), vmax=full_lidar["z"].max())
            scat = ax.scatter(full_lidar["x"], full_lidar["y"], c=full_lidar["z"], cmap="jet", s=4, norm=cNorm)
            plt.colorbar(scat, ax=ax, orientation='horizontal')
            ax.set_title("Lidar | Scatter | Heatmap=elevation [m]", pad=15)
            ax.set_xlim(-40, 40)
            ax.set_ylim(0, 100)

            plt.suptitle("Lidar Road Segmentation| " + engine.get_id(idx=engine_idx) + " | " + str(engine.start_idx + engine_idx))

            plt.tight_layout(pad=1)
            if debug:
                plt.savefig("./lidar_road_example/seg_" + engine.get_id(idx=engine_idx) + ".png")
                if not video:
                    # plt.show()
                    exit()
            else:
                os.makedirs(engine.drive_context.lidar_road_seg_df_images, exist_ok=True)
                plt.savefig(engine.drive_context.lidar_road_seg_df_images + engine.get_id(idx=engine_idx) + ".png")

        except Exception as e:
            PrintException()
            continue
    # make_video_from_dir(df=engine.df, images_dir="./lidar_road_example/", suffix="_" + str(start) + "_" + str(end))


def get_rays_df(df, az_bin_deg):
    display_df = df.copy()
    display_df = display_df.reset_index(drop=True)
    display_df["r"], display_df["Az"], display_df["El"] = toRAzEl(x=display_df["x"], y=display_df["y"], z=display_df["z"])

    az_index_values = np.digitize(display_df["Az"], bins=np.linspace(-90, 90, np.int(180 / az_bin_deg)))
    display_df["Az_index"] = az_index_values
    grouops_df = display_df.groupby("Az_index")["r"].nsmallest(1)
    if grouops_df.index.nlevels > 1:
        display_df = display_df.iloc[grouops_df.index.get_level_values(1)]
    else:
        display_df = display_df.iloc[grouops_df.index.get_level_values(0)]
    display_df = display_df.set_index("Az_index")
    display_df = display_df.reindex(pd.RangeIndex(0, 1800), fill_value="NaN")
    display_df = display_df.reset_index(drop=True)

    return display_df


def infer_lidar_road_delimiter(engine, force=True, plot_every=150, debug=False, video=False):
    print("\n\n######### Lidar Road Delimiter #########")
    # Algorithm Settings
    min_height, max_height = -2.5, 1.3
    dilate_factor = 10
    az_bin_deg = 0.1

    for engine_idx in tqdm(range(len(engine))):
        try:
            label_path = engine.drive_context.lidar_road_delimiter + "/lidar_road_delimiter_" + engine.get_lidar_id(idx=engine_idx) + ".csv"
            if not force and os.path.exists(label_path):
                continue

            # Get Data
            s_idx, e_idx = 3, 6

            im = engine.get_image(engine_idx)
            road_seg_df = engine.get_lidar_df(idx=engine_idx)
            road_seg_df = road_seg_df[(road_seg_df["orig_z"] >= min_height) & (road_seg_df["orig_z"] <= max_height)]
            road_seg_df = road_seg_df.reset_index(drop=True)

            road_seg_df_to_plot = road_seg_df.copy()

            # lidar, projected_lidar = engine.get_lidar(engine_idx)
            # lidar = lidar[np.logical_and(lidar[:, 2] >= min_height, lidar[:, 2] <= max_height)]
            # projected_lidar = projected_lidar[np.logical_and(projected_lidar[:, 2] >= min_height, projected_lidar[:, 2] <= max_height)]

            seg = engine.get_segmentation_label(idx=engine_idx)
            # imgfov_pc_pixel, imgfov_pc_velo = engine.process_lidar_on_image(lidar, im)
            # boolean_mask = seg.get_mask_of_boundary(imgfov_pc_pixel, cls=Road(), dilate_factor=dilate_factor)

            # imgfov_pc_pixel_road = imgfov_pc_pixel[:, boolean_mask]
            # imgfov_pc_velo_road = imgfov_pc_velo[boolean_mask]

            # projected_pc_velo_road = engine.project_lidar_to_radar(pts=imgfov_pc_velo_road[:, s_idx:e_idx])
            # road_seg_df = engine.transform_projected_lidar_to_df(pts=projected_pc_velo_road)

            boolean_mask = seg.get_mask_of_boundary(road_seg_df[["x_pixel", "y_pixel"]].T.to_numpy(), cls=Road(), dilate_factor=dilate_factor)
            road_seg_df = road_seg_df[boolean_mask]

            # road_seg_df["dist_from_center"] = np.linalg.norm(np.vstack([road_seg_df["x"], road_seg_df["y"]]), axis=0)
            # road_seg_df = road_seg_df.sort_values(by=["dist_from_center"]).reset_index(drop=True)
            road_seg_df = road_seg_df.reset_index(drop=True)

            # print("bef", np.unique(road_seg_df["confidence"]))
            interpolated_df_01 = get_rays_df(df=road_seg_df, az_bin_deg=az_bin_deg)
            interpolated_df_01 = interpolated_df_01.apply(pd.to_numeric, errors='coerce')
            interpolated_df_01 = interpolated_df_01.interpolate(method="linear", columns=[""])
            # print("aft", np.unique(road_seg_df["confidence"]))
            # smooth
            # window_size = 10
            # interpolated_df_01.loc[:, "rolling_var"] = interpolated_df_01["dist_from_center"].rolling(window_size).var()
            # interpolated_df_01.loc[:, "rolling_median"] = interpolated_df_01["dist_from_center"].rolling(window_size).median()
            # interpolated_df_01.loc[:, "median_flag"] = interpolated_df_01["dist_from_center"] < interpolated_df_01.loc[:, "rolling_median"]
            # interpolated_df_01.loc[:, "in_suspicious_var_range"] = (interpolated_df_01.loc[:, "rolling_var"] > 2) & (interpolated_df_01.loc[:, "rolling_var"] < 11)
            #
            # interpolated_df_01.to_csv("./interpolated_df_01.csv")
            # good_indices = np.logical_not(interpolated_df_01.loc[:, "in_suspicious_var_range"]) | (interpolated_df_01.loc[:, "in_suspicious_var_range"]) & interpolated_df_01.loc[:, "median_flag"] == True

            # interpolated_df_01_fixed[~good_indices] = "NaN"
            # interpolated_df_01_fixed = interpolated_df_01[good_indices]
            # interpolated_df_01_fixed = interpolated_df_01_fixed.apply(pd.to_numeric, errors='coerce')
            # interpolated_df_01_fixed = interpolated_df_01_fixed.interpolate(method="linear", columns=[""])
            # interpolated_df_01_fixed = interpolated_df_01_fixed.reset_index(drop=True)
            #
            # interpolated_df_01_fixed = interpolated_df_01_fixed.reset_index(drop=True)

            if not debug or True:
                # columns_to_drop = ["rolling_median", "rolling_var", "median_flag", "in_suspicious_var_range"]
                # interpolated_df_01_fixed = interpolated_df_01_fixed.drop_duplicates(subset=["x", "y", "z"], keep="first")
                # interpolated_df_01_fixed.drop(columns=columns_to_drop).to_csv(label_path)
                interpolated_df_01 = interpolated_df_01.reset_index(drop=True)
                interpolated_df_01 = interpolated_df_01.drop_duplicates(subset=["x", "y", "z"], keep="first")
                interpolated_df_01.to_csv(label_path)

            if engine_idx % plot_every != 0: continue
            interpolated_df_01 = interpolated_df_01.reset_index(drop=True)
            """
            Plotting
            """
            gs = gridspec.GridSpec(nrows=3, ncols=3)
            fig = plt.figure(figsize=(20, 12))

            # Display seg
            ax = fig.add_subplot(gs[0, 2])
            ax.imshow(im)
            ax.set_title("Camera Segmentation")
            ax.imshow(seg.get_img(colors=True), alpha=0.4)
            ax.axis("off")
            ax.set_title("Camera | Lidar | Heatmap=range [m]", pad=20)
            process_lidar_to_image_on_ax_df(road_seg_df_to_plot, im=im.copy(), ax=ax, points_size=0.2)
            # process_lidar_to_image_on_ax_simple(imgfov_pc_pixel, imgfov_pc_velo[:, s_idx:e_idx], im.copy(), ax, points_size=0.2)

            ax = fig.add_subplot(gs[1, 2])
            process_lidar_to_image_on_ax_df(road_seg_df, im=im.copy(), ax=ax, points_size=0.2)
            # process_lidar_to_image_on_ax_simple(imgfov_pc_pixel_road, imgfov_pc_velo_road[:, s_idx:e_idx], im.copy(), ax, points_size=1)
            ax.imshow(im)
            ax.set_title("Delimiter Proj | Heatmap=range [m]", pad=20)
            ax.axis("off")

            cNorm = colors.Normalize(vmin=road_seg_df["z"].min(), vmax=road_seg_df["z"].max())

            ax = fig.add_subplot(gs[2, 2])
            scat = ax.scatter(road_seg_df["x"], road_seg_df["y"], c=road_seg_df["z"], cmap="jet", s=4, norm=cNorm)
            plt.colorbar(scat, ax=ax, orientation='horizontal')
            ax.set_title("Delimiter | Heatmap=elevation [m]", pad=20)
            ax.set_xlim(-40, 40)
            ax.set_ylim(0, 100)

            # Plot
            # ax = fig.add_subplot(gs[1, 0])
            # scat = ax.scatter(projected_lidar[:, 0], projected_lidar[:, 1], c=projected_lidar[:, 2], cmap="jet", s=4, norm=cNorm)
            # plt.colorbar(scat, ax=ax, orientation='horizontal')
            # ax.set_title("Lidar | Scatter | Heatmap=elevation[m]")
            # ax.set_xlim(-40, 40)
            # ax.set_ylim(0, 100)

            plt.suptitle("Lidar Road Delimiter| " + engine.get_id(idx=engine_idx) + " | " + str(engine.start_idx + engine_idx))

            ax = fig.add_subplot(gs[:, :2])
            ax.scatter(interpolated_df_01["x"], interpolated_df_01["y"], c=interpolated_df_01["z"], cmap="jet", s=4, norm=cNorm)
            for ray_idx in range(len(interpolated_df_01)):
                plt.plot([0, interpolated_df_01.loc[ray_idx, "x"]], [0, interpolated_df_01.loc[ray_idx, "y"]], "r")
            ax.set_title("Delimiter | Linear-Interpolation | Az-Bin=0.1deg")
            ax.set_xlim(-40, 40)
            ax.set_ylim(0, 100)

            # ax = fig.add_subplot(gs[1, 1])
            # ax.scatter(interpolated_df_01_fixed["x"], interpolated_df_01_fixed["y"], c=interpolated_df_01_fixed["z"], cmap="jet", s=4, norm=cNorm)
            # for ray_idx in range(len(interpolated_df_01_fixed)):
            #     plt.plot([0, interpolated_df_01_fixed.loc[ray_idx, "x"]], [0, interpolated_df_01_fixed.loc[ray_idx, "y"]], "r")
            # ax.set_title("Fixed! Interpolated Az-Bin=0.1 deg | Scatter")
            # ax.set_xlim(-40, 40)
            # ax.set_ylim(0, 100)

            plt.tight_layout(pad=1)
            if debug:
                if video:
                    plt.savefig("./lidar_road_example/delimiter_" + engine.get_id(idx=engine_idx) + ".png")
                else:
                    plt.show()
                    exit()
            else:
                plt.savefig(engine.drive_context.lidar_road_delimiter_images + engine.get_id(idx=engine_idx) + ".png")
            plt.close()

        except Exception as e:
            PrintException()
            if debug:
                exit()
            continue
    if video: make_video_from_dir(df=engine.df, images_dir="./lidar_road_example/", suffix="_" + str(time.time()).split(".")[0])


def gen_depth_maps(engine, force=True, plot_every=200, debug=False):
    for engine_idx in tqdm(range(len(engine))):
        try:
            label_path = engine.get_depth_map(idx=engine_idx, path_only=True)
            os.makedirs(os.path.split(label_path)[0], exist_ok=True)
            if not force and os.path.exists(label_path): continue

            im = engine.get_image(engine_idx)
            seg = engine.get_segmentation_label(engine_idx)
            df = engine.get_lidar_df(engine_idx)
            df = df[df["x_pixel"].notna()].reset_index(drop=True)
            df = df[df["r_lidar_camera"].notna()].reset_index(drop=True)

            road_mask_bool = (seg.label == 8).astype(np.uint8)
            xi = np.arange(0, im.shape[1], 1)
            yi = np.arange(0, im.shape[0], 1)
            xi, yi = np.meshgrid(xi, yi)

            zi = griddata((df["x_pixel"], df["y_pixel"]), df["r_lidar_camera"], (xi, yi), method='linear')

            zi = np.nan_to_num(zi, nan=-1)
            zi = zi.astype(np.float32)

            if not debug:
                np.savez(label_path, zi)

            if engine_idx % plot_every != 0: continue

            plt.close()
            fig = plt.figure(figsize=(25, 14))
            gs = gridspec.GridSpec(ncols=3, nrows=2)
            ax = fig.add_subplot(gs[1, 0])
            ax.imshow(im)
            ax.imshow(seg.get_img(colors=True), alpha=0.4)
            ax.set_title("Camera")

            zi = np.nan_to_num(zi, 0)
            zi = zi.clip(0, 80)
            ax = fig.add_subplot(gs[:, 1:])
            scat = ax.imshow(zi * road_mask_bool, cmap="turbo")
            ax.set_title("Road Depth Map | cmap=r [m]")

            plt.colorbar(scat, ax=ax, fraction=0.026)

            ax = fig.add_subplot(gs[0, 0])
            plt_df = engine.get_lidar_df(engine_idx)
            plt_df = plt_df.apply(pd.to_numeric, errors='coerce')
            process_lidar_to_image_on_ax_df(display_df=plt_df, im=engine.get_image(engine_idx), ax=ax, points_size=2)
            ax.imshow(im)
            ax.set_title("Lidar Projection")
            plt.suptitle("Lidar Depth Map | " + str(engine.get_id(engine_idx)))
            os.makedirs(engine.drive_context.depth_map_images, exist_ok=True)
            plt.savefig(engine.drive_context.depth_map_images + str(engine.get_id(engine_idx)) + ".png")

        except Exception as e:
            PrintException()
            continue


def main():
    engines = get_engines(val_train="all", randomize=False)
    # data_engine1 = WisenseEngine(base_name="Wisense", base_path_drive="/media/amper/data_backup7/76mWF/200902_lidar_drive_highway_W92_M15_RF1/", rect_mode=True, **kw_args)
    # data_engine1.update_df_range(start_idx=1650, end_idx=2000, shuffle=False)
    # data_engine1.sample_df(50)
    #
    # data_engine4 = WisenseEngine(base_name="Wisense", base_path_drive="/media/amper/data_backup7/76mWF/200902_lidar_drive_highway_W92_M15_RF1/", rect_mode=True)
    # data_engine4.update_df_range(start_idx=1200, end_idx=1350, shuffle=False)
    # data_engine4.sample_df(50)
    #
    # data_engine2 = WisenseEngine(base_name="Wisense", base_path_drive="/media/amper/data_backup7/76mWF/200831_lidar_drive_1_urban_W92_M15_RF1/", rect_mode=True)
    # data_engine2.update_df_range(start_idx=500, end_idx=1500, shuffle=False)
    # data_engine2.sample_df(50)
    #
    # data_engine3 = WisenseEngine(base_name="Wisense", base_path_drive="/media/amper/data_backup7/76mWF/200831_lidar_drive_1_urban_W92_M15_RF1/", rect_mode=True)
    # data_engine3.update_df_range(start_idx=450, end_idx=600)
    # data_engine3.sample_df(50)

    with Pool(4) as p:
        print(p.map(gen_depth_maps, engines))
        # print(p.map(infer_lidar_road_delimiter, engines))

    # for data_engine in engines:  # , data_engine2]:
    # infer_lidar_road_delimiter(engine=data_engine, force=True, plot_every=1, debug=True, video=True)
    # infer_lidar_road(engine=data_engine, force=False, debug=False, video=False)


if __name__ == "__main__":
    main()
