import argparse
import json
import os
import pandas as pd
import glob
import os.path as osp
import json

import numpy as np
from PIL import Image
from tqdm import tqdm


def make_df_folders(trainDisc, trainWf, trainDatasets, base='/workspace/'):
    # Create training df
    dfTrain          = pd.DataFrame()

    for i,experiment in enumerate(trainDatasets):
        csvPath = base + trainDisc[i] + '/' + trainWf +'/' + experiment + '/filteredData.csv'
        dfExperiment            = pd.read_csv(csvPath)
        dfExperiment['Dataset'] = experiment
        dfExperiment['Disc']    = trainDisc[i]
        dfExperiment['WF']      = trainWf
        print('Loaded date:',experiment, len(dfExperiment), 'frames')
        dfTrain = dfTrain.append(dfExperiment)
    dfTrain = dfTrain.reset_index(drop=True)
    print('FinalLoaded date:',experiment, len(dfTrain), 'frames')
    return dfTrain