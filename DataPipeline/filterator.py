from tqdm import tqdm
import os
import pandas as pd
from glob import glob
import numpy as np
from DataPipeline.shared_utils import read_label


class Filterator:
    def __init__(self):
        return

    @staticmethod
    def run(context, args, df):
        drop_indices = []
        logger = []

        for idx in tqdm(range(len(df))):
            # Camera
            paths = []
            paths.append(context.camera_rgb + "/camera_rgb_" + str(df.iloc[idx]["timestampCamera"]) + ".png")
            paths.append(context.radar_sant_save_path + "/S_ANT_" + str(df.iloc[idx]["timestampRadar"]) + ".npy")
            # paths.append(context.camera_det_ensemble + "/camera_det_ensemble_" + str(df.loc[idx, "timestampCamera"]) + ".json")
            # paths.append(context.camera_det_EfficientDetD7 + "/camera_det_EfficientDetD7_" + str(df.iloc[idx]["timestampCamera"]) + ".json")
            # paths.append(context.camera_det_yoloV3 + "/camera_det_yoloV3_" + str(df.iloc[idx]["timestampCamera"]) + ".json")
            paths.append(context.camera_seg_class + "/camera_seg_class_" + str(df.iloc[idx]["timestampCamera"]) + ".npz")
            paths.append(context.camera_seg_soft + "/camera_seg_soft_" + str(df.iloc[idx]["timestampCamera"]) + ".npz")

            # if args.lidar:
            #     paths.append(context.lidar_points + "/lidar_pc_" + str(df.loc[idx, 'timestampLidar'])[:-4] + ".npz")
            #     paths.append(context.lidar_3d + "/lidar_3d_" + str(df.loc[idx, "timestampLidar"])[:-4] + ".json")
            #     paths.append(context.lidar_3d_pointrcnn + "/lidar_3d_pointrcnn_" + str(df.loc[idx, "timestampLidar"])[:-4] + ".json")
            #     paths.append(context.lidar_3d_ensemble + "/lidar_3d_ensemble_" + str(df.loc[idx, "timestampLidar"])[:-4] + ".npz")
            #     paths.append(context.lidar_seg + "/lidar_seg_" + str(df.loc[idx, "timestampLidar"])[:-4] + ".npz")
            #     # paths.append(context.camera_to_lidar + "/camera_to_lidar_" + str(df.loc[idx, "timestampLidar"])[:-4] + ".json")
            #
            # paths.append(context.cuda_4dfft + '/cuda_4dfft_' + str(df.loc[idx, "timestampRadar"]) + ".csv")
            # paths.append(context.metadata + "/metadata_cuda_4dfft_" + str(df.loc[idx, "timestampRadar"]) + ".json")
            #
            # paths.append(context.cuda_delta + '/cuda_delta_' + str(df.loc[idx, "timestampRadar"]) + ".csv")
            # meta_data_cuda = context.metadata + "/metadata_cuda_delta_" + str(df.loc[idx, "timestampRadar"]) + ".json"
            # paths.append(meta_data_cuda)
            # paths.append(context.cuda_processed_delta + '/cuda_processed_delta_' + str(df.loc[idx, "timestampRadar"]) + ".csv")

            # if os.path.exists(meta_data_cuda):
            #     label =  read_label(meta_data_cuda)
            #     if "platform_velocity" not in label:
            #         print("platform_velocity not in", idx)
            #         drop_indices.append(idx)
            #     else:
            #         if np.isnan(np.asarray(label["platform_velocity"])).sum()>0:
            #             print("Failed to find velocity estimation")
            #             drop_indices.append(idx)
            for p in paths:
                if not os.path.exists(p):
                    drop_indices.append(idx)
                    logger.append(str(idx) + "," + p + ",\n")

        with open(context.aux_path + "/missing_logger.csv", "w") as fp:
            fp.writelines(logger)

        filtered_data = df.drop(drop_indices).reset_index(drop=True)
        filtered_data.to_csv(context.aux_path + "/filtered_data_" + context.df_suffix, index=False)
        filtered_data.to_csv(context.aux_path + "/filtered_data.csv", index=False)
        print("Removed", len(set(drop_indices)), "Out of", len(df))
        return