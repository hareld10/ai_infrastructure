import logging
import pathlib
import sys
from multiprocessing.context import Process

import easydict

from DataPipeline.DNNs.EfficientDet.inference import infer_from_base_path
from DataPipeline.DNNs.PyTorch_YOLOv3.inference_pipeline import infer_from_df
from DataPipeline.DNNs.deeplab.inference_pipeline import deeplab_from_base_path
from DataPipeline.ensamble import Ensemble, all_ensemble_contexts
from DataPipeline.runner import run_docker
from WiseSDKAI import *
from DataPipeline.shared_utils import read_label, rectify
from DataPipeline.src.GetEngines import get_engines
from DataPipeline.syncer import EngineSyncer
from easydict import EasyDict as edict
import glob
from DataPipeline.wise_sdk_runner import SDKRunner
import time
import inspect

module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, module_path)
sys.path.insert(0, "/workspace/AI/Harel/AI_Infrastructure/")

log = logging.getLogger("application")
sh = logging.StreamHandler()
log.addHandler(sh)


class Pipeline:
    def __init__(self, engine, pipeline_config=None):
        self.engine = engine
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                            datefmt='%d-%m %H:%M',
                            filename=self.engine.drive_context.aux_path + 'logging.txt',
                            filemode='a')
        log.setLevel(logging.DEBUG)
        log.info("\n\nPipeline object created")
        if pipeline_config is None:
            self.pipeline_config = edict(read_label(module_path + "/configs/pipeline_config.json"))
            log.info("loaded default pipeline config len=%d", len(engine))
            log.debug(self.pipeline_config)

        # Objects
        self.syncer = EngineSyncer(engine=engine, pipeline_config=self.pipeline_config)
        self.ensembler = Ensemble(contexts=all_ensemble_contexts)
        self.df_data = None

        return

    def get_docker_args(self, mode=False):
        base_path = self.engine.base_path
        df = self.engine.df_name
        force = int(mode == 2)
        gpu = self.pipeline_config["gpu"]
        return base_path + " " + df + " " + str(force) + " " + str(gpu)

    def rectify_rgb(self):
        log.info("\n################ Rectify RGB ####################\n")
        if not self.check_if_do(self.engine.drive_context.camera_rgb_rectified_original, force_not_affecting=True):
            return

        for idx in tqdm(range(len(self.df_data))):
            rgbImPath = self.engine.drive_context.camera_rgb + "/camera_rgb_" + str(self.df_data.iloc[idx]["timestampCamera"]) + ".png"
            img = cv2.imread(rgbImPath)
            rec = rectify(img, K=self.engine.drive_context.K, D=self.engine.drive_context.D)
            rgbImPath = self.engine.drive_context.camera_rgb_rectified + "/camera_rgb_" + str(self.df_data.iloc[idx]["timestampCamera"]) + ".png"
            cv2.imwrite(rgbImPath, rec)
        return

    def preprocess(self):
        self.df_data = self.syncer.run()
        if self.pipeline_config.samples:
            if "-" in str(self.pipeline_config.samples):
                words = self.pipeline_config.samples.split("-")
                start, end = int(words[0]), int(words[1])
                log.info("Sampling Sequence! %d - %d", start, end)
                self.df_data = self.df_data[start:end].reset_index(drop=True)
            else:
                log.info("Sampling %d num samples out of %d", int(self.pipeline_config.samples), len(self.df_data))
                self.df_data = self.df_data.sample(int(self.pipeline_config.samples)).reset_index(drop=True)

            self.engine.drive_context.df_path = self.engine.drive_context.sample_df_path
            self.df_data.to_csv(self.engine.drive_context.sample_df_path, sep=',', index=False)
            self.engine.df_name = os.path.split(self.engine.drive_context.sample_df_path)[-1]

            logging.info("Write sample df %s", self.engine.drive_context.sample_df_path)
        log.info("Preprocessing finish: len(df_data)=%d", len(self.df_data))
        return

    def check_if_do(self, dir_name, force_not_affecting=False):
        if not force_not_affecting:
            if self.pipeline_config.samples:
                return True
        else:
            log.info("check_if_do: Force-Doesn't-Affect - %s", dir_name)
            return False

        count = len(glob.glob(dir_name + "/*"))
        len_data = len(self.df_data)
        if (len_data - count) < len_data * 0.05:
            log.info("check_if_do: Skipping - dir_name=%s, count=%d, len-df=%d", dir_name, count, len_data)
            return False
        else:
            log.info("check_if_do: Not Skipping - dir_name=%s, count=%d, len-df=%d", dir_name, count, len_data)
        return True

    def radar_and_camera(self):
        sdk = SDKRunner(context=self.engine.drive_context)

        if self.pipeline_config["camera"] == 2 or (self.pipeline_config["camera"] == 1 and self.check_if_do(self.engine.drive_context.camera_rgb)):
            sdk.open_camera_frames_by_folder(self.df_data, True, False, False, self.engine.drive_context)
            self.rectify_rgb()

        p_delta_mode = self.pipeline_config["radar"]["p_delta"]["do"]
        if p_delta_mode == 2 or (p_delta_mode == 1 and self.check_if_do(self.engine.drive_context.cuda_processed_delta)):  # CUDA Delta
            log.info("\n################ CUDA Processed Delta ####################\n")
            sdk.set_config(config_name=self.pipeline_config["radar"]["p_delta"]["config"])
            sdk.dsp(self.df_data, p_delta_mode > 1, pc_type=WiseSDK.DATA_TYPE_E.PROCESSED_POINT_CLOUD_DATA_TYPE, s_ant=False, by_range=False)

        radar_4d_fft_mode = self.pipeline_config["radar"]["4d_fft"]["do"]
        if radar_4d_fft_mode == 2 or (radar_4d_fft_mode == 1 and self.check_if_do(self.engine.drive_context.cuda_4dfft)):
            log.info("\n################ CUDA 4D FFT ####################\n")
            sdk.set_config(config_name=self.pipeline_config["radar"]["4d_fft"]["config"])
            sdk.dsp(self.df_data, radar_4d_fft_mode > 1, pc_type=WiseSDK.DATA_TYPE_E.POINT_CLOUD_DATA_TYPE, s_ant=False, by_range=False)

        log.info("Sleeping Before Shutting Down")
        time.sleep(3)

        sdk.WiseSDK_kill()

    def extract_data(self):
        # Radar and camera
        if self.pipeline_config["camera"] >= 1 or self.pipeline_config["radar"]["do"] >= 1:
            cam_and_radar_process = Process(target=self.radar_and_camera)
            cam_and_radar_process.start()
            cam_and_radar_process.join()
            cam_and_radar_process.terminate()
        return

    def gt_process(self):
        od_mode = self.pipeline_config["gt"]["object_detection"]
        if od_mode >= 1:
            if od_mode == 2 or self.check_if_do(self.engine.drive_context.camera_det_EfficientDetD7):
                log.info("Object Detection D7")
                logging.info("Docker args: %s", self.get_docker_args(mode=od_mode))
                script_location = inspect.getfile(infer_from_base_path).replace("/home/amper/", "/workspace/")
                run_docker(image="fdc69d2a5787", container_name="od_edet7", exec_cmd="python3 " + script_location + " " + self.get_docker_args(mode=od_mode))

            # if od_mode == 2 or self.check_if_do(self.engine.drive_context.camera_det_yoloV3):
            #     log.info("Object Detection Yolo")
            #     p4 = Process(target=infer_from_df, args=(self.engine.drive_context, self.df_data, od_mode == 2, self.pipeline_config["gpu"],))
            #     p4.start()
            #     p4.join()

        seg_mode = self.pipeline_config["gt"]["segmentation"]
        if seg_mode >= 1:
            if seg_mode == 2 or (self.check_if_do(self.engine.drive_context.camera_seg_class) or self.check_if_do(self.engine.drive_context.camera_seg_soft)):
                log.info("Segmentation")
                p5 = Process(target=deeplab_from_base_path,
                             args=(self.engine.drive_context, self.df_data, seg_mode == 2, self.pipeline_config["gpu"],))
                p5.start()
                p5.join()

        ensemble_2d_mode = self.pipeline_config["gt"]["ensemble_2d"]
        if ensemble_2d_mode >= 1:
            if ensemble_2d_mode == 2 or self.check_if_do(self.engine.drive_context.camera_det_ensemble):
                log.info("2D Ensemble")
                self.ensembler.ensemble_from_dir(path_context=self.engine.drive_context, df=self.df_data, force=ensemble_2d_mode == 2)

    def run(self):
        print(self.engine.get_drive_name())
        self.preprocess()

        self.extract_data()

        self.gt_process()
        pass


def run(engine):
    p = Pipeline(engine=engine)
    p.run()


if __name__ == '__main__':
    engines = get_engines(randomize=False)
    run(engine=engines[0])
    pass
