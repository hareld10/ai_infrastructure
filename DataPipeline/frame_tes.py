
import os
import sys, pathlib
import time
from pprint import pprint

from DataPipeline.shared_utils import find_data_size_properties

module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, module_path)
# import WiseSDK
import glob
WiseSDK.modulesInit()
# time.sleep(1)

# dsp_configuration = WiseSDK.DSPConfiguration().dict()
# print(dsp_configuration)
# server_id = WiseSDK.SERVER_ID_E.SERVER_ID_INTERNAL
# WiseSDK.setDSPParameters(dsp_configuration, server_id)

# path = sys.argv[1]
path = "/workspace/data_backup3/250mWF/200621_drive_2_W36_M8_RF1_ANT3/radarRawData/Radar_1592736007175342098.raw"
# path = "/workspace/data_backup3/64mWF/200203_drive_0_W60_M8_RF1/radarRawData/Radar_1580731629886947327.raw"
if not os.path.exists(path):
    print("WARNING, frame doesnt exist")
    exit(1)
else:
    print("\n\npath Exists\n\n", path)

# frames = sorted(glob.glob("/workspace/data_backup5/250mWF/200901_lidar_drive_2_W92_M12_RF1/radarRawData/*.raw"))

frame = WiseSDK.processFrame(path, 0, WiseSDK.DATA_TYPE_E.RANGE_DOPPLER_DATA_TYPE)
time.sleep(10)
Rframe = WiseSDK.processFrame(path, 0, WiseSDK.DATA_TYPE_E.RAW_DATA_TYPE)

# Get the values
arr = Rframe.getData()
print(arr.shape)
# Get dimension from header
num_samples, num_rx, num_tx, num_pulses = find_data_size_properties(Rframe)
frame = WiseSDK.processFrame(path, 0, WiseSDK.DATA_TYPE_E.RANGE_DOPPLER_DATA_TYPE)
time.sleep(1)

print(frame.getHeader().waveform_id, frame.getHeader().mode_id)
pprint(frame.getMetadata().dict())

time.sleep(1)
WiseSDK.modulesDestroy()
