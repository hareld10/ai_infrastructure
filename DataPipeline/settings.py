# settings.py
import json
import pathlib
from copy import copy
from threading import Thread, Lock
import os
import sys
import numpy as np

from Calibrator.projection import Camera
from Calibrator.calibrator_utils import loadParams
from DataPipeline.shared_utils import read_label, is_docker

module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, module_path)

class Context:
    def __init__(self, args, index=0, create_dirs=True, base_path="", spatial_calibration_path=None, envelope_file=None):
        self.visSavePath = ""
        self.cameraOdLabel = ""
        self.cameraSegLabel = ""
        self.radarSrc_path = ""
        self.cameraSrc_path = ""
        self.depthSrc_path = ""
        self.dst_path = ""
        self.cameraData_path = ""
        self.cameraDataDepth_path = ""
        self.cameraDataRgb_path = ""
        self.radarData_path = ""
        self.base_destination = ""
        self.pbar = ""
        self.radar_params = ""
        self.base_lidar_src, self.base_radar_src, self.base_camera_src = "", "", ""

        if spatial_calibration_path is None:
            # spatial_calibration_path = module_path + "/configs/calibration_params_lidar_radar_200909"
            spatial_calibration_path = module_path + "/configs/cali_200909_pitch_fix"
        with open(spatial_calibration_path, "r") as fp:
            self.spatial_calibration_params = json.load(fp)

        if envelope_file is None:
            self.envelope = read_label(module_path + "/configs/envelopes/default_envelope.json")
        else:
            self.envelope = read_label(module_path + "/configs/envelopes/" + envelope_file)

        self.K = np.zeros((3, 3), dtype=np.float32)
        self.K[0][0] = self.spatial_calibration_params["fx"]
        self.K[0][2] = self.spatial_calibration_params["px"]
        self.K[1][1] = self.spatial_calibration_params["fy"]
        self.K[1][2] = self.spatial_calibration_params["py"]
        self.K[2][2] = 1

        self.D = np.zeros((1, 5), dtype=np.float32)
        self.D[0][0] = self.spatial_calibration_params["k1"]
        self.D[0][1] = self.spatial_calibration_params["k2"]
        self.D[0][2] = self.spatial_calibration_params["p1"]
        self.D[0][3] = self.spatial_calibration_params["p2"]
        self.D[0][4] = self.spatial_calibration_params["k3"]

        self.camera_obj = Camera(K=self.K, D=self.D)
        self.lidar_obj = None
        self.args = args
        if args is not None:
            self.rectified_flag = args.rect_labels
            self.src_dir = args.src[index]
            self.cameraDataRaw_path = args.cameraSrc + "/" + self.src_dir + "/cameraRawData/"
            self.gpsDataRaw_path = args.gpsSrc + "/" + self.src_dir + "/gpsRawData/"
            self.base_camera_src = args.cameraSrc
            self.base_gps_src = args.gpsSrc
            self.radarDataRaw_path = args.radarSrc + "/" + self.src_dir + "/radarRawData/"
            self.base_radar_src = args.radarSrc
            self.depthDataRaw_path = args.depthSrc + "/" + self.src_dir + "/depthRawData/"
            self.lidar_src = args.lidarSrc + "/" + self.src_dir
            self.base_lidar_src = args.lidarSrc
            self.destination_path = args.dest + "/" + args.src[index]
            self.base_destination = args.dest
            self.lidar_raw_data = self.lidar_src + "/lidarRawData/"
            print("Current Destination Path", self.destination_path)

            self.radar_src = args.radarSrc + "/" + self.src_dir
            self.camera_src = args.cameraSrc + "/" + self.src_dir
            self.depth_src = args.depthSrc + "/" + self.src_dir
        else:
            self.destination_path = base_path
            # if is_docker():
            #     self.destination_path = self.destination_path.replace("/home/amper/", "/workspace/")

        self.aux_path = str(self.destination_path) + "/aux/"
        self.evaluation = self.destination_path + "/evaluation/"

        self.sample_df_path = self.aux_path + "/sample_df.csv"
        self.lidar_csv_path = self.aux_path + "/dfLidar.csv"
        self.instances_3d_df = self.aux_path + "/instances_3d_df.csv"
        self.instances_2d_df = self.aux_path + "/instances_2d_df.csv"

        self.df_suffix = "sync_data_radar_lidar.csv"
        self.df_path = self.aux_path + "/" + self.df_suffix

        self.camera_data = self.destination_path + "/camera_data/"
        self.gps_data = self.destination_path + "/gps_data/"

        self.camera_rgb_original = self.camera_data + "/camera_rgb/"
        self.camera_rgb = copy(self.camera_rgb_original)

        self.camera_rgb_rectified_original = self.camera_data + "/camera_rgb_rectified/"
        self.camera_rgb_rectified = copy(self.camera_rgb_rectified_original)

        self.camera_labels_regular = self.destination_path + "/camera_labels/"
        self.camera_labels_rectified_regular = self.destination_path + "/camera_labels/rectified_labels/"

        self.camera_labels = copy(self.camera_labels_regular)
        self.rectified_labels = copy(self.camera_labels_rectified_regular)
        self.rectified_labels_images = self.rectified_labels + "/images/"

        self.lidar_data = self.destination_path + "/lidar_data/"
        self.lidar_labels = self.destination_path + "/lidar_labels/"

        self.lidar_3d = self.lidar_labels + "/lidar_3d/"
        self.lidar_3dssd = self.lidar_labels + "/lidar_3dssd/"
        self.lidar_3d_pointrcnn = self.lidar_labels + "/lidar_3d_pointrcnn/"
        self.lidar_seg = self.lidar_labels + "/lidar_seg/"
        self.ensemble_3d = self.lidar_labels + "/ensemble_3d/"
        self.lidar_3d_ensemble = self.lidar_labels + "/lidar_3d_ensemble/"
        self.lidar_3d_ensemble_images = self.lidar_3d_ensemble + "/images/"
        self.camera_to_lidar = self.lidar_labels + "/camera_to_lidar/"
        self.camera_to_lidar_clusters = self.camera_to_lidar + "/clusters/"
        self.camera_to_lidar_images = self.camera_to_lidar + "/images/"
        self.camera_to_lidar_instances = self.camera_to_lidar_images
        self.lidar_road_seg = self.lidar_labels + "/lidar_road_seg/"

        self.depth_map = self.lidar_labels + "/depth_map/"
        self.depth_map_images = self.depth_map + "/images/"

        self.lidar_ssn = self.lidar_labels + "/lidar_ssn/"

        self.lidar_road_seg_df = self.lidar_labels + "/lidar_road_seg_df/"
        self.lidar_road_seg_df_images = self.lidar_road_seg_df + "/images/"
        self.not_lidar_road_seg = self.lidar_labels + "/not_lidar_road_seg/"
        self.lidar_road_seg_images = self.lidar_road_seg + "/images/"

        self.lidar_road_surface = self.lidar_labels + "/lidar_road_surface/"
        self.lidar_road_surface_images = self.lidar_road_surface + "/images/"

        self.lidar_road_delimiter = self.lidar_labels + "/lidar_road_delimiter/"
        self.lidar_road_delimiter_images = self.lidar_road_delimiter + "/images/"
        # self.lidar_road_delimiter_no_fix = self.lidar_labels + "/lidar_road_delimiter_no_fix/"
        # self.lidar_road_delimiter_no_fix_images = self.lidar_road_delimiter_no_fix + "/images/"

        self.radar_data = self.destination_path + "/radar_data/"
        self.mimo_raw_save_path = self.radar_data + "/mimo_raw/"
        self.ppa_raw_save_path = self.radar_data + "/ppa_raw/"
        self.radar_raw_save_path = self.radar_data + "/radar_raw/"
        self.radar_sant_save_path = self.radar_data + "/S_ANT/"
        self.metadata = self.destination_path + "/metadata/"
        self.cuda_4dfft = self.radar_data + "/cuda_4dfft/"
        self.radar4d_phase_amp = self.radar_data + "/radar4d_phase_amp/"
        self.cuda_delta = self.radar_data + "/cuda_delta/"
        self.cuda_processed_delta = self.radar_data + "/cuda_processed_delta/"

        self.validation = self.destination_path + "/validation/"
        self.lidar_points = self.lidar_data + "/lidar_pc/"

        self.camera_det_EfficientDetD7 = self.camera_labels + '/camera_det_EfficientDetD7/'
        self.camera_det_EfficientDetD7_images = self.camera_det_EfficientDetD7 + '/images/'
        self.camera_det_yoloV3 = self.camera_labels + '/camera_det_yoloV3/'
        self.camera_det_yoloV3_images = self.camera_det_yoloV3 + '/images/'
        self.camera_det_yolor = self.camera_labels + '/camera_det_yolor/'
        self.camera_det_yolor_images = self.camera_det_yolor + '/images/'
        self.camera_seg_class = self.camera_labels + '/camera_seg_class/'
        self.camera_seg_class_images = self.camera_seg_class + '/images/'
        self.camera_seg_soft = self.camera_labels + '/camera_seg_soft/'
        self.camera_seg_soft_images = self.camera_seg_soft + '/images/'

        self.camera_segformer_soft = f"{self.camera_labels}/camera_segformer_soft/"
        self.camera_segformer_class = f"{self.camera_labels}/camera_segformer_hard/"

        self.camera_det_ensemble = self.camera_labels + '/camera_det_ensemble/'
        self.camera_det_ensemble_images = self.camera_det_ensemble + '/images/'

        self.cur_rect = False

        self.camera_obj_path = f"{self.aux_path}/camera_obj"
        self.lidar_obj_path = f"{self.aux_path}/lidar_obj"

        if create_dirs:
            self.run_dirs_creation()

        return

    def set_directly(self, drive_name, camera_src, radar_src, depth_src, lidar_src, dest, gps_src):
        self.src_dir = drive_name
        self.base_radar_src = radar_src
        self.base_camera_src = camera_src
        self.base_lidar_src = lidar_src
        self.base_destination = dest
        self.cameraDataRaw_path = camera_src + "/" + self.src_dir + "/cameraRawData/"
        self.gpsDataRaw_path = gps_src + "/" + self.src_dir + "/gpsRawData/"
        self.radarDataRaw_path = radar_src + "/" + self.src_dir + "/radarRawData/"
        self.depthDataRaw_path = depth_src + "/" + self.src_dir + "/depthRawData/"
        self.lidar_src = lidar_src + "/" + self.src_dir
        self.lidar_raw_data = self.lidar_src + "/lidarRawData/"

        self.radar_src = radar_src + "/" + self.src_dir + "/"
        self.camera_src = camera_src + "/" + self.src_dir + "/"
        self.depth_src = depth_src + "/" + self.src_dir + "/"

    def run_dirs_creation(self):
        last_stat = self.cur_rect

        self.creat_dirs()
        self.set_rect_paths(True)
        self.creat_dirs()
        self.set_rect_paths(False)

        self.set_rect_paths(last_stat)


    def creat_dirs(self):
        os.makedirs(self.destination_path, exist_ok=True)
        os.makedirs(self.camera_data, exist_ok=True)
        os.makedirs(self.camera_rgb, exist_ok=True)
        os.makedirs(self.gps_data, exist_ok=True)
        os.makedirs(self.camera_rgb_rectified, exist_ok=True)
        os.makedirs(self.camera_labels, exist_ok=True)
        os.makedirs(self.camera_to_lidar_instances, exist_ok=True)
        os.makedirs(self.lidar_data, exist_ok=True)
        os.makedirs(self.lidar_labels, exist_ok=True)
        os.makedirs(self.lidar_3d_ensemble, exist_ok=True)
        os.makedirs(self.lidar_3dssd, exist_ok=True)
        os.makedirs(self.lidar_ssn, exist_ok=True)
        os.makedirs(self.lidar_3d_ensemble_images, exist_ok=True)
        os.makedirs(self.lidar_road_seg, exist_ok=True)
        os.makedirs(self.lidar_road_seg_df, exist_ok=True)
        os.makedirs(self.lidar_road_seg_df_images, exist_ok=True)
        os.makedirs(self.lidar_road_delimiter, exist_ok=True)
        os.makedirs(self.lidar_road_delimiter_images, exist_ok=True)
        os.makedirs(self.evaluation, exist_ok=True)
        # os.makedirs(self.lidar_road_delimiter_no_fix, exist_ok=True)
        # os.makedirs(self.lidar_road_delimiter_no_fix_images, exist_ok=True)
        os.makedirs(self.camera_segformer_class, exist_ok=True)
        os.makedirs(self.camera_segformer_soft, exist_ok=True)

        os.makedirs(self.not_lidar_road_seg, exist_ok=True)
        os.makedirs(self.lidar_road_seg_images, exist_ok=True)
        os.makedirs(self.lidar_3d, exist_ok=True)
        os.makedirs(self.camera_to_lidar, exist_ok=True)
        os.makedirs(self.camera_to_lidar_clusters, exist_ok=True)
        os.makedirs(self.camera_to_lidar_images, exist_ok=True)
        os.makedirs(self.radar_data, exist_ok=True)
        os.makedirs(self.mimo_raw_save_path, exist_ok=True)
        os.makedirs(self.ppa_raw_save_path, exist_ok=True)
        os.makedirs(self.radar_raw_save_path, exist_ok=True)
        os.makedirs(self.radar_sant_save_path, exist_ok=True)
        os.makedirs(self.radar4d_phase_amp, exist_ok=True)
        os.makedirs(self.metadata, exist_ok=True)
        os.makedirs(self.cuda_4dfft, exist_ok=True)
        os.makedirs(self.cuda_delta, exist_ok=True)
        os.makedirs(self.cuda_processed_delta, exist_ok=True)
        os.makedirs(self.validation, exist_ok=True)
        os.makedirs(self.lidar_points, exist_ok=True)
        if self.args is not None:
            os.makedirs(self.lidar_raw_data, exist_ok=True)
        os.makedirs(self.aux_path, exist_ok=True)
        os.makedirs(self.rectified_labels, exist_ok=True)
        os.makedirs(self.rectified_labels_images, exist_ok=True)
        os.makedirs(self.camera_det_EfficientDetD7, exist_ok=True)
        os.makedirs(self.camera_det_EfficientDetD7_images, exist_ok=True)
        os.makedirs(self.camera_det_yoloV3, exist_ok=True)
        os.makedirs(self.camera_det_yoloV3_images, exist_ok=True)
        os.makedirs(self.camera_det_yolor_images, exist_ok=True)
        os.makedirs(self.camera_det_yolor, exist_ok=True)
        os.makedirs(self.camera_seg_class, exist_ok=True)
        os.makedirs(self.camera_seg_class_images, exist_ok=True)
        os.makedirs(self.camera_seg_soft, exist_ok=True)
        os.makedirs(self.camera_seg_soft_images, exist_ok=True)
        os.makedirs(self.camera_det_ensemble, exist_ok=True)
        os.makedirs(self.camera_det_ensemble_images, exist_ok=True)
        return

    def set_rect_paths(self, rect=False):
        self.cur_rect = rect
        if rect:
            # print("Context: Working on rect mode!")
            self.camera_labels = self.camera_labels_rectified_regular
            self.camera_rgb = self.camera_rgb_rectified_original
        else:
            # print("Context: Working on non-rect mode!")
            self.camera_labels = self.camera_labels_regular
            self.camera_rgb = self.camera_rgb_original

        self.camera_det_EfficientDetD7 = self.camera_labels + '/camera_det_EfficientDetD7/'
        self.camera_det_EfficientDetD7_images = self.camera_det_EfficientDetD7 + '/images/'
        self.camera_det_yoloV3 = self.camera_labels + '/camera_det_yoloV3/'
        self.camera_det_yoloV3_images = self.camera_det_yoloV3 + '/images/'
        self.camera_seg_class = self.camera_labels + '/camera_seg_class/'
        self.camera_seg_class_images = self.camera_seg_class + '/images/'
        self.camera_seg_soft = self.camera_labels + '/camera_seg_soft/'
        self.camera_seg_soft_images = self.camera_seg_soft + '/images/'

        self.camera_segformer_soft = f"{self.camera_labels}/camera_segformer_soft/"
        self.camera_segformer_class = f"{self.camera_labels}/camera_segformer_hard/"

        self.camera_det_ensemble = self.camera_labels + '/camera_det_ensemble/'
        self.camera_det_ensemble_images = self.camera_det_ensemble + '/images/'
        self.camera_det_yolor = self.camera_labels + '/camera_det_yolor/'
        self.camera_det_yolor_images = self.camera_det_yolor + '/images/'
        return True


def init():
    global radarSrc_path
    global cameraSrc_path
    global depthSrc_path
    global dst_path
    global cameraData_path
    global cameraDataDepth_path
    global cameraDataRgb_path
    global radarData_path
    global mutex
    global pbar
    global radar_params
    global cameraSegLabel
    global cameraOdLabel
    global visSavePath

    visSavePath = ""
    cameraOdLabel = ""
    cameraSegLabel = ""
    radarSrc_path = ""
    cameraSrc_path = ""
    depthSrc_path = ""
    dst_path = ""
    cameraData_path = ""
    cameraDataDepth_path = ""
    cameraDataRgb_path = ""
    radarData_path = ""
    mutex = ""
    pbar = ""
    radar_params = ""
