import argparse
import sys

parser = argparse.ArgumentParser(description='Date pipeline arguments')

# Resources
parser.add_argument('--dest', type=str, help="path to destination")
parser.add_argument('--radarSrc', type=str, default="", help="path to radar raw files")
parser.add_argument('--cameraSrc', type=str, default="", help="path to camera raw files")
parser.add_argument('--depthSrc', type=str, default="", help="path to depth raw files")
parser.add_argument('--lidarSrc', type=str, default="", help="path to lidar raw files")

# parser.add_argument("-radar-path", type=str, default="", help="path to radar raw files")
# parser.add_argument('--camera-path', type=str, default="", help="path to camera raw files")
# parser.add_argument('--depth-path', type=str, default="", help="path to depth raw files")
# parser.add_argument('--lidar-path', type=str, default="", help="path to lidar raw files")

# Settings
parser.add_argument('--force', action='store_true', default=False, help="overwrite existing files")
parser.add_argument('--force-pc', action='store_true', default=False, help="path to lidar raw files")
parser.add_argument('--force-4d', action='store_true', default=False, help="path to lidar raw files")
parser.add_argument('--sync', type=int, default=10, help="sync threshold between sensors in millisecond")
parser.add_argument('--force-sync', action='store_true', default=False, help="ensure specific frame rate from if possible from synced data, e.g all synced drive is at \n ")
parser.add_argument('--samples',  type=str, default="", help="randomly sample SAMPLES frames")
parser.add_argument('--cameraOnly', action='store_true', default=False, help="path to lidar raw files")
parser.add_argument('--radarOnly', action='store_true', default=False, help="path to lidar raw files")
parser.add_argument('--flip', action='store_true', default=False, help="flip RGB camera image")
parser.add_argument('--delete-unused', action='store_true', default=False, help="delete unused raw files")
parser.add_argument('--val', action='store_true', default=False, help="output validation")
parser.add_argument('--video', type=str, default="", help="path to lidar raw files")
parser.add_argument('--df', type=str, default="", help="path to specific dataframe")

parser.add_argument('--update-filter', action='store_true', default=False, help="update filter data file")
parser.add_argument('--filterator', action='store_true', default=False, help="update filter data file")
# Lidar
parser.add_argument('--lidar', action='store_true', default=True, help="output lidar point cloud")
parser.add_argument('--skip-lidar', action='store_true', default=False, help="output lidar point cloud")
parser.add_argument('--skip-sant', action='store_true', default=False, help="output lidar point cloud")
# Camera
parser.add_argument('--camExport', action='store_true', default=False, help="path to specific dataframe")
parser.add_argument('--rgb', action='store_true', default=False, help="output RGB")
parser.add_argument('--depth', action='store_true', default=False, help="output depth")
parser.add_argument('--cameraPC', action='store_true', default=False, help="path to specific dataframe")

# Radar
parser.add_argument('--radar', action='store_true', default=False, help="output radar s_ant")
parser.add_argument('--radarPc', action='store_true', default=False, help="output radar delta")
parser.add_argument( '--radar4d', action='store_true', default=False, help="output radar 4dfft")
parser.add_argument( '--phase-amp', action='store_true', default=False, help="output radar 4dfft")
parser.add_argument('--processed-delta', action='store_true', default=False, help="output radar processed delta")
parser.add_argument('--by-range', action='store_true', default=False, help="output radar splitted by range")
# parser.add_argument('--radar-sant', action='store_true', default=False, help="output radar s_ant")
# parser.add_argument('--radar-delta', action='store_true', default=False, help="output radar delta")
# parser.add_argument('-radar-4dfft', action='store_true', default=False, help="output radar 4dfft")

# AI

# parser.add_argument('-od-2d', action='store_true', default=False, help="output 2D object detection")
# parser.add_argument('-od-3d', action='store_true', default=False, help="output 3D object detection")
# parser.add_argument('-seg-2d', action='store_true', default=False, help="output 2D segmentation")
# parser.add_argument('-seg-3d', action='store_true', default=False, help="output 3D segmentation")

parser.add_argument('--road-seg', action='store_true', default=False, help="output road segmentation")
parser.add_argument('--veh-seg', action='store_true', default=False, help="output vehicle segmentation")
parser.add_argument('--od', action='store_true', default=False, help="output object detection")
parser.add_argument('--seg', action='store_true', default=False, help="output segmentation")
parser.add_argument('--clf', action='store_true', default=False, help="output segmentation")
parser.add_argument('--lidar-road', action='store_true', default=False, help="output segmentation")
parser.add_argument('--lidar-road-del', action='store_true', default=False, help="output segmentation")
parser.add_argument('--ensemble-2d', action='store_true', default=False, help="output segmentation")
parser.add_argument('--pc3d', action='store_true', default=False, help="output 3d object detection")
parser.add_argument('--ensemble-3d', action='store_true', default=False, help="output 3d lidar ensemble")
parser.add_argument('--odYolo', action='store_true', default=False, help="path to specific dataframe")
parser.add_argument('--gpu', default=0, type=int, help="specify GPU to use")
parser.add_argument('--rect-labels', action='store_true', default=False, help="rectified labels object detection")

# Sources
parser.add_argument('--skip-rgb', action='store_true', default=False, help="output lidar point cloud")
parser.add_argument('--associate', action='store_true', default=False, help="output lidar point cloud")
parser.add_argument('--all-lidar', action='store_true', default=False, help="path to specific dataframe")
parser.add_argument('--rev', action='store_true', default=False, help="path to specific dataframe")
parser.add_argument('--single', type=str, default="", help="path to specific dataframe")
parser.add_argument('--skip', action='store_true', default=False, help="path to specific dataframe")
parser.add_argument('--csv-only', action='store_true', default=False, help="path to specific dataframe")
parser.add_argument('--all-zed', action='store_true', default=False, help="path to specific dataframe")
parser.add_argument('--radar-raw', action='store_true', default=False, help="path to specific dataframe")
parser.add_argument('-config', type=str, default="", help="path to specific dataframe")
parser.add_argument('--wf', type=str, required=False, help="names of dirs to process")
parser.add_argument('--debug', action='store_true', default=False, help="path to specific dataframe")
parser.add_argument('--src', nargs='+', type=str, required=False, help="names of dirs to process")
