import json
import os
import pickle
from pprint import pprint

import pandas as pd
import numpy as np
from psutil import process_iter
from signal import SIGTERM  # or SIGKILL
import sys, shutil
import glob, linecache, traceback
import subprocess

from tqdm import tqdm

HOME = os.path.expanduser('~')


def get_dir_size(Folderpath):
    size = 0
    for path, dirs, files in os.walk(Folderpath):
        for f in files:
            fp = os.path.join(path, f)
            size += os.path.getsize(fp)
    return size


def add_sdk_path():
    sdk_path = f"{HOME}/workspace/wise_sdk/SDK/python/"
    # if sdk_path not in sys.path:
    sys.path.insert(0, sdk_path)


class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


def kill_process_on_port(port):
    for proc in process_iter():
        for conns in proc.connections(kind='inet'):
            if conns.laddr.port == 8080:
                proc.send_signal(SIGTERM)  # or SIGKILL

def pickle_write(file_path, obj):
    """
    Serialize an object to a provided file_path
    """

    with open(file_path, 'wb') as file:
        pickle.dump(obj, file)

def save_obj(obj, path):
    with open(path, 'w') as fp:
        json.dump(obj, fp, indent=2)

def save_json(obj, path):
    with open(path, 'w') as fp:
        json.dump(obj, fp, indent=2)

def load_json(file_name):
    with open(file_name, "r") as fp:
        js = json.load(fp)
    return js


class BadFrames:
    def __init__(self, engine):
        self.engine = engine
        self.bad_frame_path = f"{engine.drive_context.aux_path}/bad_frame.csv"
        self.df = pd.DataFrame()
        if os.path.exists(self.bad_frame_path):
            self.df = pd.read_csv(self.bad_frame_path, index_col=0)
            print("loaded bad frame", engine, self.df)

    def add_ts(self, ts):
        try:
            engine_idx = self.engine.get_idx_from_ts(ts=ts, key=self.engine.ts_radar)
        except Exception as e:
            return
        self.df = self.df.append({"radar_ts": ts}, ignore_index=True)
        self.df.to_csv(self.bad_frame_path)

        print("added bad frame idx", ts)
        print("- len before", len(self.engine.df))
        self.engine.df.drop(engine_idx, inplace=True)
        self.engine.df.reset_index(drop=True).to_csv(self.engine.csv_path)
        print("+ len before", len(self.engine.df))


class FreeSpace:
    def __init__(self, engine, index_callback):
        self.engine = engine
        self.index_callback = index_callback
        self.base_data = "/media/amper/wis*"
        self.srcs = glob.glob(self.base_data)
        self.dirs = ["radarRawData", "cameraRawData", "gpsRawData", "lidarRawData"]

        self.destinations_raw = ["/workspace/data_backup10/wise4_raw"]
        self.destinations_genrated = ["/workspace/data_backup10/250mWF_wise4"]

    def find(self, d):
        for src in self.srcs:
            ret = glob.glob(f"{src}/*/{self.engine.get_drive_name()}/{d}/")
            if len(ret) == 1:
                return ret[0]

    def get_dirs_to_move(self):
        to_move = [self.find(d) for d in self.dirs]
        for a in to_move:
            if a is None:
                print("FreeSpace: some error getting sources from disks")
                return None

        return to_move

    def get_destination(self, srcs, free_size_needed):
        for dest in srcs:
            if shutil.disk_usage(dest).free > free_size_needed:
                return dest
        return None

    def copy_engine(self):
        df = self.index_callback()
        drive = self.engine.get_drive_name()

        for d in self.dirs:
            src = self.find(d)
            if not src:
                print("FreeSpace: copy_engine: Probably nothing attached")
                continue
            dest = df.loc[drive, f"{d}_amper"] + "/" + drive + "/"
            os.makedirs(dest, exist_ok=True)

            if src.endswith("/"):
                src = src[:-1]

            print("####")
            print("src", src)
            print("dest", dest)

            subprocess.call(["rsync", "-ahvr", src, dest])

    def register_engine(self):
        to_move = self.get_dirs_to_move()
        if not to_move:
            return False

        pprint(to_move)

        raw_size = sum([get_dir_size(x) for x in to_move])

        print("total raw size to move", raw_size / (10 ** 12), "TB")

        dest_raw = self.get_destination(srcs=self.destinations_raw, free_size_needed=raw_size)
        if not dest_raw:
            print("Can't find where to put raw files")
            return False

        dest_generated = self.get_destination(srcs=self.destinations_genrated, free_size_needed=raw_size / 4)
        if not dest_generated:
            print("Can't find where to put genrated files")
            return False

        df = self.index_callback()
        df_path = self.index_callback(path_only=True)

        drive = self.engine.get_drive_name()

        df.loc[drive, "suffix"] = "/"
        df.loc[drive, "val_train"] = "train"
        df.loc[drive, "camera_flip"] = 0
        df.loc[drive, "radarRawData_amper"] = dest_raw
        df.loc[drive, "lidarRawData_amper"] = dest_raw
        df.loc[drive, "gpsRawData_amper"] = dest_raw
        df.loc[drive, "cameraRawData_amper"] = dest_raw
        df.loc[drive, "amper_disc"] = dest_generated

        print("write to", df_path, "but not really")
        print(df.loc[drive])
        df.to_csv(df_path)
        print()


def apply_uniform(df, x_res, y_res, z_res, x_key="orig_x", y_key="orig_y", z_key="orig_z"):
    display_df = df.copy().reset_index(drop=True)
    x_min, x_max = display_df[x_key].min(), display_df[x_key].max()
    y_min, y_max = display_df[y_key].min(), display_df[y_key].max()
    z_min, z_max = display_df[z_key].min(), display_df[z_key].max()

    if z_res != 0:
        z_index_values = np.digitize(display_df[z_key], bins=np.linspace(z_min, z_max, np.int(np.abs(z_max - z_min) / z_res)))
        display_df[z_key] = z_min + (z_index_values * z_res)

    if x_res != 0:
        x_index_values = np.digitize(display_df[x_key], bins=np.linspace(x_min, x_max, np.int(np.abs(x_max - x_min) / x_res)))
        display_df[x_key] = x_min + (x_index_values * x_res)

    if y_res != 0:
        y_index_values = np.digitize(display_df[y_key], bins=np.linspace(y_min, y_max, np.int(np.abs(y_max - y_min) / y_res)))
        display_df[y_key] = y_min + (y_index_values * y_res)

    display_df = display_df.drop_duplicates(subset=[x_key, y_key, z_key])

    return display_df.reset_index(drop=True)

def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    if tb is not None:
        f = tb.tb_frame
        lineno = tb.tb_lineno
        filename = f.f_code.co_filename
        linecache.checkcache(filename)
        line = linecache.getline(filename, lineno, f.f_globals)
        tb = traceback.format_exc()
        print(tb)
        print('EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj))

add_sdk_path()

config_3_5T = {"FINE_AZP_OVS_FACT": 4,
               "FINE_ELP_OVS_FACT": 2,
               "FINE_CFAR_GLOBAL_THR_DB": 6,
               "FINE_CFAR_LOCAL_THR_DB": 10,
               'POSTFILT_MAPPER_FLAG': 0,
               "MAX_DETECTIONS": 20000,
               'AZ_STEER_MIN_DEG': -40.0,
               'AZ_STEER_MAX_DEG': 40.0,
               "RNG_MAX": 76.0,
               "RNG_MIN": 3.0,
               'FINE_DELTA_RNG_DEP_N_FILT': 1,
               }

calib_file_3_5T = "%s/DataPipeline/configs/cal.json" % os.getcwd()
