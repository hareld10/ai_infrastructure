import argparse
import sys
import os
from random import random

import pandas as pd
import numpy as np
from tqdm import tqdm
import settings
import ctypes
import cv2
from tqdm import tqdm
from threading import Thread, Lock
import json
import glob
import pickle
from skimage import exposure
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx
import time
from pathlib import Path

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure

from shared_utils import filter_fov_az, lidar_to_radar_coordinates


class Validator:
    def __init__(self, context, df_data):
        self.df_data = df_data
        self.context = context

        self.other_colors = Validator.gen_colors(109)
        self.colors = {"car": (0, 0, 255), "traffic light": (255, 0, 0), "truck": (135, 206, 250),
                       "bus": (100, 149, 237),
                       "pedestrian": (255, 165, 0), "person": (255, 165, 0), "bicycle": (170, 0, 255),
                       "motorcycle": (216, 0, 155), "forbidden_pedestrian": (238,130,238)}
        self.obj_list = ['person', 'bicycle', 'car', 'motorcycle', 'airplane', 'bus', 'train', 'truck', 'boat',
                         'traffic light',
                         'fire hydrant', '', 'stop sign', 'parking meter', 'bench', 'bird', 'cat', 'dog', 'horse',
                         'sheep',
                         'cow', 'elephant', 'bear', 'zebra', 'giraffe', '', 'backpack', 'umbrella', '', '', 'handbag',
                         'tie',
                         'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball', 'kite', 'baseball bat',
                         'baseball glove',
                         'skateboard', 'surfboard', 'tennis racket', 'bottle', '', 'wine glass', 'cup', 'fork', 'knife',
                         'spoon',
                         'bowl', 'banana', 'apple', 'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza',
                         'donut',
                         'cake', 'chair', 'couch', 'potted plant', 'bed', '', 'dining table', '', '', 'toilet', '',
                         'tv',
                         'laptop', 'mouse', 'remote', 'keyboard', 'cell phone', 'microwave', 'oven', 'toaster', 'sink',
                         'refrigerator', '', 'book', 'clock', 'vase', 'scissors', 'teddy bear', 'hair drier',
                         'toothbrush']
        return

    @staticmethod
    def gen_colors(n):
        ret = []
        r = int(random() * 256)
        g = int(random() * 256)
        b = int(random() * 256)
        step = 256 / n
        for i in range(n):
            r += step
            g += step
            b += step
            r = int(r) % 256
            g = int(g) % 256
            b = int(b) % 256
            ret.append((r, g, b))
        return ret

    def make_video(self):
        return

    def db(self, array):
        return 10 * np.log10(array)

    def get_nci(self, s_ant):
        nci = np.zeros([s_ant.shape[0], s_ant.shape[2]])
        for i in range(s_ant.shape[1]):
            nci += np.abs(s_ant[:, i, :])
        nci = np.fft.fftshift(nci, axes=(1,))
        cur_plot = self.db(nci).T
        h, w = cur_plot.shape[:2]

        ratio = max(int(w), int(h))
        cur_plot = cv2.resize(cur_plot, dsize=(int(ratio*3), int(ratio*2)), interpolation=cv2.INTER_NEAREST)
        return cur_plot

    def get_channels_array(self, s_ant):
        # Build channelsArray
        all_channels = np.abs(s_ant[:, 1])

        for i in range(s_ant.shape[1])[1:]:
            cur_channel = np.abs(s_ant[:, i])
            all_channels = np.concatenate([all_channels, cur_channel], axis=1)

        scale_y = 4
        all_channels = cv2.resize(all_channels, (all_channels.shape[1]*4, all_channels.shape[0]))
        all_channels = self.db(all_channels)

        return all_channels

    def get_channel_strength(self, s_ant):
        ch_idx, ch_strength, ch_color = [], [], []

        # Channel strength
        for i in range(s_ant.shape[1])[:]:
            cur_channel = np.sum(np.abs(s_ant[:, i]))
            ch_idx.append(i)
            ch_strength.append(cur_channel)

        # plt.cla()
        # plt.bar(ch_idx, self.db(ch_strength), align='center', alpha=0.5)
        return ch_idx, self.db(ch_strength)

    def validate_radar(self, idx):
        s_ant_path = self.context.radar_sant_save_path + '/S_ANT_' + str(self.df_data.loc[idx, 'timestampRadar']) + '.npy'
        if not os.path.exists(s_ant_path):
            print("Warning - S_ANT doesn't exist", s_ant_path)
            return

        s_ant = np.load(s_ant_path)  # (1021, 48, 116) complex64]
        # title = "S_ANT Shape = " + str(s_ant.shape)
        title = "Frame Validation " + self.context.src_dir + " " + str(self.df_data.loc[idx, 'timestampRadar'])
        # Build NCI
        nci = self.get_nci(s_ant)
        # title += ", NCI Shape " + str(nci.shape)

        channels_array = self.get_channels_array(s_ant)
        # title += ", Channels Shape " + str(channels_array.shape)

        channels_strength = self.get_channel_strength(s_ant)

        return [title, nci, channels_array, channels_strength]

    def get_od(self, idx, img):
        label_json_path = self.context.camera_det_ensemble + "/camera_det_ensemble_" + str(self.df_data.loc[idx, 'timestampCamera']) + ".json"
        if not os.path.exists(label_json_path):
            return img
        im = img.copy()
        with open(label_json_path) as label_json:
            frame_data = json.load(label_json)
            for cur_label in frame_data:
                obj = cur_label['cat']
                cat_id = cur_label['cat_id']
                score = cur_label['score']
                x1, y1, x2, y2 = cur_label['bbox']
                # color = self.other_colors[cat_id]
                if obj in self.colors:
                    color = self.colors[obj]
                else:
                    color = (0, 255, 0)
                cv2.rectangle(im, (x1, y1), (x2, y2), color, 2)
                cv2.putText(im, '{}, {:.3f}'.format(obj, score),
                            (x1, y1 + 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                            color, 1)

        return im

    def get_road_seg(self, idx, img):
        label_path = self.base_path + "/camera_road_seg/camera_road_seg_" + str(self.df_data.loc[idx, 'timestampCamera']) + ".npz"
        if not os.path.exists(label_path):
            return
        im = img.copy()

        probs = np.load(label_path)["arr_0"]
        cleanImg = im.copy()

        probsImg = np.stack([probs, probs, probs], axis=2)
        probsImg = (probsImg*255).astype(np.uint8)
        probsImg = cv2.resize(probsImg, (cleanImg.shape[1], cleanImg.shape[0]))
        probsImg = probsImg.astype(np.float32) / 255

        heatMap = np.uint8(exposure.rescale_intensity(np.copy(probs), out_range=(0, 255)))
        heatMap  = cv2.applyColorMap(heatMap, cv2.COLORMAP_JET)
        heatMap = cv2.resize(heatMap, (cleanImg.shape[1], cleanImg.shape[0]))
        heatMap  = cv2.addWeighted(heatMap, 0.4, cleanImg, 0.6, 0)
        heatMap  = np.where(probsImg>0.2,heatMap,cleanImg)

        return heatMap

    def get_seg(self, idx, img):
        label_path = self.context.camera_seg_class + "/camera_seg_class_" + str(self.df_data.loc[idx, 'timestampCamera']) + ".npz"
        if not os.path.exists(label_path):
            return img
        probs = np.load(label_path)["arr_0"]
        return probs

    def get_veh_seg(self, idx, img):
        label_path = self.base_path + "/camera_vehicle_seg/camera_vehicle_seg_" + str(
            self.df_data.loc[idx, 'timestampCamera']) + ".npz"
        if not os.path.exists(label_path):
            return
        im = img.copy()

        probs = np.load(label_path)["arr_0"]
        cleanImg = im.copy()

        probsImg = np.stack([probs, probs, probs], axis=2)
        probsImg = (probsImg * 255).astype(np.uint8)
        probsImg = cv2.resize(probsImg, (cleanImg.shape[1], cleanImg.shape[0]))
        probsImg = probsImg.astype(np.float32) / 255

        heatMap = np.uint8(exposure.rescale_intensity(np.copy(probs), out_range=(0, 255)))
        heatMap = cv2.applyColorMap(heatMap, cv2.COLORMAP_JET)
        heatMap = cv2.resize(heatMap, (cleanImg.shape[1], cleanImg.shape[0]))
        heatMap = cv2.addWeighted(heatMap, 0.4, cleanImg, 0.6, 0)
        heatMap = np.where(probsImg > 0.2, heatMap, cleanImg)

        return heatMap

    def validate_camera(self, idx):
        rgb_path = self.context.camera_rgb + "/camera_rgb_" + str(self.df_data.loc[idx, 'timestampCamera']) + ".png"
        if not os.path.exists(rgb_path):
            print("Warning, rgb_path doesn't exist", rgb_path)
            return

        im = cv2.cvtColor(cv2.imread(rgb_path), cv2.COLOR_BGR2RGB)
        original = im.copy()

        od_im = self.get_od(idx, im)
        seg_im = self.get_seg(idx, im)
        return [original, od_im, seg_im]

    def add_figure(self, im, title, ax):
        if im is None:
            return
        ax.set_title(title)
        ax.imshow(im)
        ax.set_xticks([])
        ax.set_yticks([])

    def validate_lidar(self, idx, fig):
        pc_path = self.context.lidar_points + "/lidar_pc_" + str(self.df_data.loc[idx, 'timestampLidar'])[:-4] + ".npz"
        if not os.path.exists(pc_path):
            return

        points_all = np.load(pc_path)["arr_0"]

        points_all = points_all[np.linalg.norm(points_all, axis=-1)<=70]

        # azim = 205
        size =1.3
        # Top View
        ax = fig.add_subplot(133, projection='3d')
        points_all = filter_fov_az(points_all, 80)
        points_all = lidar_to_radar_coordinates(points_all)

        ax.scatter(points_all[:, 0], points_all[:, 1], points_all[:, 2], c=np.linalg.norm(points_all, axis=1),
                   cmap="jet", s=size)

        ax.set_xlabel('  X')
        ax.set_ylabel('  Y')
        ax.set_zlabel('  Z')
        ax.set_title('Lidar Top View')
        ax.view_init(elev=90, azim=-90)

        # Top View
        # ax = fig.add_subplot(336, projection='3d')
        # ax.scatter(points_all[:, 0], points_all[:, 1], points_all[:, 2], c=np.linalg.norm(points_all, axis=1),
        #            cmap="jet", s=size)
        #
        # ax.set_xlabel('X Label')
        # ax.set_ylabel('Y Label')
        # ax.set_zlabel('Z Label')
        # ax.set_title('Front View - azim ' + str(azim))
        # ax.view_init(elev=0, azim=azim)
        #
        # # default
        # ax = fig.add_subplot(339, projection='3d')
        # ax.scatter(points_all[:, 0], points_all[:, 1], points_all[:, 2], c=np.linalg.norm(points_all, axis=1),
        #            cmap="jet", s=size)
        #
        # ax.invert_xaxis()
        # ax.set_title('Default View')
        # ax.set_xlabel('X Label')
        # ax.set_ylabel('Y Label')
        # ax.set_zlabel('Z Label')

        return

    def validate_lidar_old(self, idx, fig):
        pc_path = self.context.lidar_points + "/lidar_pc_" + str(self.df_data.loc[idx, 'timestampLidar'])[:-4] + ".npz"
        if not os.path.exists(pc_path):
            return

        points_all = np.load(pc_path)["arr_0"]

        points_all = points_all[np.linalg.norm(points_all, axis=-1)<=40]

        azim = 205
        size = 0.1
        # Top View
        ax = fig.add_subplot(433, projection='3d')
        ax.scatter(points_all[:, 0], points_all[:, 1], points_all[:, 2], c=np.linalg.norm(points_all, axis=1),
                   cmap="jet", s=size)

        ax.set_xlabel('X Label')
        ax.set_ylabel('Y Label')
        ax.set_zlabel('Z Label')
        ax.set_title('Top View - azim: ' + str(azim))
        ax.view_init(elev=90, azim=azim)

        # Top View
        ax = fig.add_subplot(436, projection='3d')
        ax.scatter(points_all[:, 0], points_all[:, 1], points_all[:, 2], c=np.linalg.norm(points_all, axis=1),
                   cmap="jet", s=size)

        ax.set_xlabel('X Label')
        ax.set_ylabel('Y Label')
        ax.set_zlabel('Z Label')
        ax.set_title('Front View - azim ' + str(azim))
        ax.view_init(elev=0, azim=azim)

        # default
        ax = fig.add_subplot(439, projection='3d')
        ax.scatter(points_all[:, 0], points_all[:, 1], points_all[:, 2], c=np.linalg.norm(points_all, axis=1),
                   cmap="jet", s=size)

        ax.invert_xaxis()
        ax.set_title('Default View')
        ax.set_xlabel('X Label')
        ax.set_ylabel('Y Label')
        ax.set_zlabel('Z Label')

        return

    def validate_lidar_debug(self, idx, fig):
        pc_path = self.context.lidar_points + "/lidar_pc_" + str(self.df_data.loc[idx, 'timestampLidar'])[:-4] + ".npz"
        if not os.path.exists(pc_path):
            return

        points_all = np.load(pc_path)["arr_0"]

        points_all = points_all[np.linalg.norm(points_all, axis=-1)<70]

        azim = np.random.randint(0, 180)
        # Top View
        plt_idx=1
        for azim in range(0, 360, 10):
            ax = fig.add_subplot(6, 6, plt_idx, projection='3d')
            ax.scatter(points_all[:, 0], points_all[:, 1], points_all[:, 2], c=np.linalg.norm(points_all, axis=1),
                       cmap="jet")

            ax.set_xlabel('X Label')
            ax.set_ylabel('Y Label')
            ax.set_zlabel('Z Label')
            ax.set_title('Top View - azim: ' + str(azim))
            ax.view_init(elev=90, azim=azim)
            plt_idx +=1

    def run(self, force=True):
        for idx in tqdm(range(len(self.df_data)), file=sys.stdout):
            final_validate_path = self.context.validation + "v_" + str(self.df_data.loc[idx, 'timestampRadar']) + ".png"
            if not force and os.path.exists(final_validate_path) and os.path.getsize(final_validate_path) > 800000:
                print("Skipping")
                return

            plt.cla()
            fig = plt.figure(figsize=(50, 25))

            radar_figures = self.validate_radar(idx)
            # self.validate_pc(idx, fig)
            camera_figures = self.validate_camera(idx)

            if radar_figures is not None:
                title, nci, channels_array, channels_strength = radar_figures
                fig.suptitle(title)

                # nci
                ax = fig.add_subplot(331)
                ax.set_title('NCI')
                ax.set_xlabel('Range')
                ax.set_ylabel('Doppler')
                ax.set_xticks([])
                ax.set_yticks([])
                ret = ax.imshow(nci, cmap="jet", origin="lower")
                plt.colorbar(ret, ax=ax, orientation='horizontal')

                ax = fig.add_subplot(334)
                ax.set_title('Virtual Array')
                ax.set_xticks([])
                ax.set_yticks([])

                ret = ax.imshow(channels_array, cmap="jet")
                plt.colorbar(ret, ax=ax, orientation='horizontal')

                ax = fig.add_subplot(337)
                ax.set_title('Channel Power')
                ax.set_xlabel('Channels')
                ax.set_ylabel('db')
                ax.bar(channels_strength[0], channels_strength[1], align='center', alpha=0.5)

            else:
                print("Warning! - Error in radar validation")

            if camera_figures is not None:
                original, od_im, seg_im = camera_figures
                self.add_figure(original, "RGB Image", fig.add_subplot(332))
                self.add_figure(od_im, "Object Detection", fig.add_subplot(335))
                self.add_figure(seg_im, "Semantic Segmentation", fig.add_subplot(338))
                # self.add_figure(veh_seg, "Vehicle Segmentation", fig.add_subplot(4,3, 11))

            lidar_figures = self.validate_lidar(idx, fig)
            plt.tight_layout()
            plt.savefig(final_validate_path)
            plt.close()

            # fig = plt.figure(figsize=(50, 50))
            # self.validate_lidar_debug(idx, fig)
            # plt.tight_layout()
            # plt.savefig(final_validate_path.replace("v_", "v_azimuth_check"))
            # plt.close()
        return

    def make_video_from_dir(self):
        fourcc = cv2.VideoWriter_fourcc(*'MP4V')
        flag = False
        for i in tqdm(sorted(glob.glob(self.context.validation + "/*.png"))):
            cur = cv2.imread(i)
            if not flag:
                height, width = cur.shape[:2]
                video = cv2.VideoWriter(os.path.join(self.context.validation, 'validation_video.mp4'), fourcc, 8, (width, height))
                flag = True
            video.write(cur)
        video.release()
        return


