import json
import os
import pickle
import sys, pathlib
import time

module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, module_path)
import WiseSDK
import glob

WiseSDK.modulesInit()
time.sleep(1)

configs_path = module_path + "/configs/"
config_name = sys.argv[1]

print("CONFIG NAME", config_name)
dsp_configuration = WiseSDK.DSPConfiguration().dict()

dsp_configuration["ALGM"]["RNG_MIN"] = 0
dsp_configuration["ALGM"]["RNG_MAX"] = 2000

dsp_configuration["ALGM"]["EL_STEER_MIN_DEG"] = -15.0
dsp_configuration["ALGM"]["EL_STEER_MAX_DEG"] = 15.0

dsp_configuration["ALGM"]["AZ_STEER_MIN_DEG"] = -40.0
dsp_configuration["ALGM"]["AZ_STEER_MAX_DEG"] = 40.0

dsp_configuration["ALGI"]["SRA_ELP_FFT_OVERSAMPLING_FACT"] = 2
dsp_configuration["ALGI"]["SRA_AZP_FFT_OVERSAMPLING_FACT"] = 4

dsp_configuration["ALGI"]["IMG_ALG_TYPE"] = 2

# 0 – Delta(default)
# 1 – Delta Plus
# 2 – FFT


def save_obj(obj, name):
    with open(configs_path + '/' + name + '.json', 'w') as fp:
        json.dump(obj, fp, indent=2)

    with open(configs_path + '/' + name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)


def load_obj(name):
    with open('data.json', 'r') as fp:
        data = json.load(fp)
    # with open('obj/' + name + '.pkl', 'rb') as f:
    #     return pickle.load(f)


save_obj(dsp_configuration, name=config_name)
print(dsp_configuration)
# server_id = WiseSDK.SERVER_ID_E.SERVER_ID_INTERNAL
# WiseSDK.setDSPParameters(dsp_configuration, server_id)
