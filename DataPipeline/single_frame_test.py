import WiseSDK
import os
import time
import pandas as pd
import numpy as np

def get_pc_from_frame(frame_to_process):
    header = frame_to_process.getHeader()
    data = frame_to_process.getData()
    num_of_fields = int(data.size / header.dims.size2)
    data_shape = [header.dims.size2, num_of_fields]
    ret_pc = pd.DataFrame(data=np.reshape(data, data_shape),
                          columns=["x", "y", "z", "r", "u_sin", "v_sin", "dop", "uncorrected_dop", "value_real", "power_im", "noise", "cluster_id", "x_global", "y_global", "trk_id", "vx", "vy",
                                   "vz",
                                   "trk_extrapolation_flag", "delta_peak_flag"])
    return ret_pc


# Init WiseSDK
WiseSDK.modulesInit()
time.sleep(2)
server_id = WiseSDK.SERVER_ID_E.SERVER_ID_INTERNAL

# Initial configuration
config_Start = {
    "ALGI": {
        "COARS_CFAR_THR_DB": 11.0,
        "FINE_CFAR_THR_DB": 6.0,
        "FINE_CFAR_LOCAL_THR_DB": 10.0,
        "IMG_ALG_TYPE": 0,
        "POSTFILT_CLUSTER_DRU_M": 2.5,
        "POSTFILT_CLUSTER_DRV_M": 3.5,
        "POSTFILT_CLUSTER_DR_M": 3.0,
        "POSTFILT_CLUSTER_DV_MS": 0.5,
        "POSTFILT_CLUSTER_MIN_POINTS": 3,
        "POSTFILT_CLUSTER_NBINS_MIN": 2.0,
        "POSTFILT_MAPPER_FLAG": 0,
        "POSTFILT_STATIC_VEL_THR_MS": 0.3,
        "SRA_AZP_FFT_OVERSAMPLING_FACT": 10,
        "SRA_ELP_FFT_OVERSAMPLING_FACT": 2,
        "VEL_EST_DIM": 3,
        "VEL_EST_N_POINTS_MIN": 10,
        "VEL_EST_VMAX_MS": 30.0,
        "VEL_EST_VMIN_MS": -1.0,
        "VEL_TRCK_INIT_TIME_SEC": 0.9,
        "VEL_TRCK_ON": 1,
        "VEL_TRCK_RESTART_TIME_SEC": 5.0
    },
    "ALGM": {
        "AZ_STEER_MAX_DEG": 40.0,
        "AZ_STEER_MIN_DEG": -40.0,
        "EL_STEER_MAX_DEG": 10.0,
        "EL_STEER_MIN_DEG": -10.0,
        "RCM_FLAG": 1,
        "RNG_MAX": 76.0,
        "RNG_MIN": 3.0,
        "MAX_DETECTIONS": 20000
    },
    "ALGMPR": {
        "MAPPER_RESET_OCCUPANCY_MAP": 0,
        "MIN_CORRELATION": 0.4
    },
    "ALGTRCK": {
        "LOST_MT_TRK_AGE": 0.75,
        "LOST_STAT_TRK_AGE": 0.75,
        "VEL_MAX_MS": 50.0
    }
}

# Set configuration
status = WiseSDK.setDSPParameters(config_Start, server_id)
time.sleep(1)
print("dsp parameters status", status)

# Set any puzzle raw radar file
raw_path = "/workspace/data_backup5/76mWF_Raw/200831_lidar_drive_1_urban_W92_M15_RF1/radarRawData/Radar_1598876694594845607.raw"

# Ensure raw file exists
if not os.path.exists(raw_path):
    print("Frame does not exist!!")
    exit()
else:
    print("Exists!")

# Process init frame (allocations)
frame = WiseSDK.processFrame(raw_path, 0, WiseSDK.DATA_TYPE_E.PROCESSED_POINT_CLOUD_DATA_TYPE)
time.sleep(8)

print("\n\n\nStart test\n\n\n")

# Get Delta point cloud
frame = WiseSDK.processFrame(raw_path, 0, WiseSDK.DATA_TYPE_E.PROCESSED_POINT_CLOUD_DATA_TYPE)
print("frame=", frame.getData().shape)
if frame.getHeader().dims.size1 > 0:
    # If process success - extract point cloud
    pc = get_pc_from_frame(frame)
    print("1st frame got", pc.shape)
else:
    print("1st Frame failed")

# Get 4D-FFT frame
# Change configuration settings in order to get 4DFFT
config_Start["ALGI"]["IMG_ALG_TYPE"] = 2
status = WiseSDK.setDSPParameters(config_Start, server_id)
frame = WiseSDK.processFrame(raw_path, 0, WiseSDK.DATA_TYPE_E.PROCESSED_POINT_CLOUD_DATA_TYPE)
if frame.getHeader().dims.size1 > 0:
    # If process success - extract point cloud
    pc = get_pc_from_frame(frame)
    print("2nd frame got", pc.shape)
else:
    print("2nd Frame failed")


# Associate Between Delta and FFT and match phase/amp

# Save Delta