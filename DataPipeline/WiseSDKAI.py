# Last change: 28.1.2020 by Kfir
import linecache
import os
import traceback
from copy import copy, deepcopy

import pandas as pd
import numpy as np
import os.path
import time
from IPython.display import clear_output
import sys, pathlib

from shared_utils import db

module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, module_path)
import WiseSDK
from pprint import pprint
import settings
import ctypes
import cv2
from tqdm import tqdm
from threading import Thread, Lock
import json
import glob
from importlib import reload
import pickle
import matplotlib

matplotlib.use('pdf')
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx
import time
from matplotlib import gridspec
from pathlib import Path


def saveRadarParams(frame, settings):
    radarParamsFile = {"rng_vec": [], "rng_bin": [], "dop_vec": [], "dop_bin": [], "az_vec": [], "az_bin": [],
                       "el_vec": [], "el_bin": []}

    radar_param = {}

    # if frame.getDataType() == WiseSDK.DATA_TYPE_E.POINT_CLOUD_DATA_TYPE:
    #     print("#"*20)
    #     print("frame.getAxisParams()")
    #     print(frame.getAxisParams())
    #     print("#" * 20)
    # r, a, e, d
    ax = frame.getAxisParams()

    headers = ["range", "az", "el", "doppler"]
    for i, name in enumerate(headers):
        radar_param[name] = [ax[i].min, ax[i].max, ax[i].num_bins]

    samples_num = (radar_param["range"][1] - radar_param["range"][0] + 1)
    doppler_num = (radar_param["doppler"][1] - radar_param["doppler"][0] + 1)
    azimuth_num = (radar_param["az"][1] - radar_param["az"][0] + 1)
    elevation_num = (radar_param["el"][1] - radar_param["el"][0] + 1) / radar_param["el"][2]

    radarParamsFile["rng_bin"] = (radar_param["range"][1] - radar_param["range"][0]) / radar_param["range"][2]
    radarParamsFile["rng_vec"] = np.linspace(radar_param["range"][0], radar_param["range"][1], radar_param["range"][2])

    radarParamsFile["dop_bin"] = (radar_param["doppler"][1] - radar_param["doppler"][0]) / radar_param["doppler"][2]
    radarParamsFile["dop_vec"] = np.linspace(radar_param["doppler"][0], radar_param["doppler"][1],
                                             radar_param["doppler"][2])

    radarParamsFile["az_bin"] = (radar_param["az"][1] - radar_param["az"][0]) / radar_param["az"][2]
    radarParamsFile["az_vec"] = np.linspace(radar_param["az"][0], radar_param["az"][1], radar_param["az"][2])

    radarParamsFile["el_bin"] = (radar_param["el"][1] - radar_param["el"][0]) / radar_param["el"][2]
    radarParamsFile["el_vec"] = np.linspace(radar_param["el"][0], radar_param["el"][1], radar_param["el"][2])

    #     print(samples_num)

    print("saved_radar param to", settings.dst_path)
    np.save(settings.dst_path + '/radarParam', radarParamsFile)

    return radarParamsFile


def setWorkspace(radarSrc, cameraSrc, dst_path, depthSrc=""):
    settings.init()
    settings.radarSrc_path = radarSrc
    settings.cameraSrc_path = cameraSrc
    settings.dst_path = dst_path
    settings.depthSrc_path = depthSrc
    settings.mutex = Lock()

    # Create folders
    os.makedirs(dst_path, exist_ok=True)
    #     os.makedirs(dst_path + "/depthCamera", exist_ok=True)
    os.makedirs(dst_path + "/rgbCamera", exist_ok=True)
    os.makedirs(dst_path + "/S_ANT", exist_ok=True)
    os.makedirs(dst_path + "/metadata", exist_ok=True)
    os.makedirs(dst_path + "/cuda_4dfft", exist_ok=True)
    os.makedirs(dst_path + "/cuda_delta", exist_ok=True)
    os.makedirs(dst_path + "/validation", exist_ok=True)
    os.makedirs(dst_path + "/pcRadar", exist_ok=True)

    # savePath = dst_path + '/cameraSegLabel/'
    # os.makedirs(savePath, exist_ok=True)

    # odPath = dst_path + '/cameraOdLabel/'
    # os.makedirs(odPath, exist_ok=True)

    # Set settings paths
    settings.visSavePath = dst_path + "/cameraPred"
    # settings.cameraOdLabel = odPath
    # settings.cameraSegLabel = savePath
    settings.cameraData_path = dst_path
    settings.cameraDataRgb_path = settings.cameraData_path + "/rgbCamera"
    settings.cameraDataDepth_path = settings.depthSrc_path + "/depthCamera"
    settings.cameraDataPC_path = settings.cameraData_path + "/pcCamera"
    settings.cameraDataRaw_path = "/cameraRawData/"
    settings.radarDataRaw_path = "/radarRawData/"
    settings.depthDataRaw_path = "/depthRawData/"

    context = settings.Context()
    context.radarSrc_path = radarSrc
    context.cameraSrc_path = cameraSrc
    context.dst_path = dst_path
    context.depthSrc_path = depthSrc
    context.visSavePath = dst_path + "/cameraPred"
    # context.cameraOdLabel = odPath
    # context.cameraSegLabel = savePath
    context.cameraData_path = dst_path
    context.cameraDataRgb_path = context.cameraData_path + "/rgbCamera"
    context.cameraDataDepth_path = context.depthSrc_path + "/depthCamera"
    context.cameraDataPC_path = context.cameraData_path + "/pcCamera"
    return context


def syncSensors(timeSync, match_time_flag=False, depth=False, pc=False, get_camera_only=False):
    # Time tolerance for sync
    timeSync = timeSync * np.power(10, 6)  # [ns]

    # Define folders
    cameraPath = settings.cameraSrc_path + settings.cameraDataRaw_path
    radarPath = settings.radarSrc_path + settings.radarDataRaw_path
    depthPath = settings.depthSrc_path + settings.depthDataRaw_path
    t1 = time.time()
    # Read camera CSV
    if not depth:
        if os.path.exists(settings.dst_path + '/dfCamera.csv'):
            print("Reading dfCamera")
            dfCamera = pd.read_csv(settings.dst_path + '/dfCamera.csv', index_col=None)
            dfCamera = dfCamera.drop(columns=['platformVelocity'])
            # dfCamera['timestampCamera'] = pd.to_numeric(dfCamera['timestampCamera']).astype('int64')
            print('# of camera frames:', dfCamera.shape[0])
        else:
            cameraCSVs = [f for f in os.listdir(cameraPath) if os.path.splitext(f)[-1] == '.csv']
            list_ = []
            for file_ in tqdm(cameraCSVs):
                try:
                    df = pd.read_csv(cameraPath + '/' + file_, index_col=None)
                    try:
                        df.__delitem__("csvVersion")
                    except:
                        pass
                    file_ = file_
                    file_ = file_.replace("_timestamp", "");
                    # file_ = file_.replace(".csv", ".svo")
                    file_ = file_.replace(".csv", ".raw")
                    df.loc[:, 'filenameCamera'] = file_
                    df.loc[:, 'inFileIdxCamera'] = np.arange(len(df))
                    df.loc[:, 'platformVelocity'] = ""
                except:
                    continue
                list_.append(df)
            dfCamera = pd.concat(list_, ignore_index=True)
            dfCamera = dfCamera.dropna(axis=1, how='all')
            print('# of camera frames:', dfCamera.shape[0])
            dfCamera.to_csv(settings.dst_path + '/dfCamera.csv', sep=',', index=False)

        if get_camera_only:
            dfCamera['timeInt'] = dfCamera['timestampCamera']
            dfCamera = dfCamera.sort_values(by='timestampCamera')
            dfCamera = dfCamera.reset_index(drop=True)
            dfCamera['timestampCamera'] = dfCamera['timestampCamera']
            print("Returning camera only")
            return dfCamera
    else:
        depthCsv = glob.glob(depthPath + "/*.csv")
        if len(depthCsv) != 1:
            print("depthPath ", depthPath)
            print("Too many or no depth_sync.csv", len(depthCsv))
        depthCsv = depthCsv[0]
        #         depthCsv = depthPath + "/depth_sync.csv"
        df = pd.read_csv(depthCsv, index_col=None)
        df = df.dropna(axis=1, how='all')

        col = {"timestampDepth": "timestampCamera", "filenameDepth": "filenameCamera", "idDepth": "idCamera"}
        df = df.astype('int64')
        df = df.rename(columns=col)
        df.insert(0, 'csvVersion', ',')
        df.insert(1, 'idCamera', ',')
        df.loc[:, 'idCamera'] = np.arange(len(df))
        df.loc[:, 'idCamera'] = np.arange(len(df))
        df.loc[:, 'platformVelocity'] = ""
        df.loc[:, 'filenameCamera'] = "rgb_depth.svo"
        df.loc[:, 'inFileIdxCamera'] = np.arange(len(df))
        dfCamera = df
        print('# of camera(depth) frames:', dfCamera.shape[0])

    # Read radar CSV
    if os.path.exists(settings.dst_path + '/dfRadar.csv'):
        print("Reading dfRadar")
        dfRadar = pd.read_csv(settings.dst_path + '/dfRadar.csv', index_col=None)
        print('# of radar frames:', dfRadar.shape[0])
    else:
        print("Generating dfRadar,", settings.dst_path + '/dfRadar.csv')
        dfRadar = pd.DataFrame()
        radarCSVs = sorted([f for f in os.listdir(radarPath) if os.path.splitext(f)[-1] == '.csv'])
        print("len(radarCSVs) ", len(radarCSVs))
        list_ = []
        for file_ in tqdm(radarCSVs):
            try:
                df = pd.read_csv(radarPath + '/' + file_, index_col=None)
                try:
                    df.__delitem__("csvVersion")
                except:
                    pass
                file_ = file_.replace(".csv", ".raw")
                df.loc[:, 'filenameRadar'] = file_
                df.loc[:, 'inFileIdxRadar'] = np.arange(len(df))

            except Exception as e:
                print("Exception ", str(e))
                continue
            list_.append(df)
        dfRadar = pd.concat(list_, ignore_index=True)
        dfRadar = dfRadar.dropna(axis=1, how='all')
        print('# of radar frames:', dfRadar.shape[0])
        dfRadar.to_csv(settings.dst_path + '/dfRadar.csv', sep=',', index=False)

    if (match_time_flag):
        if os.path.exists(cameraPath + '/SensorsOffset.sync'):
            dfOffsets = pd.read_csv(cameraPath + '/SensorsOffset.sync', index_col=None)
            dfCamera = match_time(dfOffsets, dfRadar, dfCamera)
            print("after sync len", len(dfCamera))
        else:
            print("No Sync File")

    # Sync image and radar df using timestamp
    dfCamera['timeInt'] = pd.to_numeric(dfCamera['timestampCamera'])  # TODO: look into why the type changed to object
    dfRadar['timeInt'] = dfRadar['timestampRadar']
    dfCamera = dfCamera.sort_values(by='timestampCamera')  # merge needs sorted values
    dfRadar = dfRadar.sort_values(by='timestampRadar')
    dfCamera = dfCamera.reset_index(drop=True)
    dfRadar = dfRadar.reset_index(drop=True)
    dfRadar['timestampRadar'] = dfRadar['timestampRadar'].astype(str)
    dfCamera['timestampCamera'] = dfCamera['timestampCamera'].astype(str)

    dfSync = pd.merge_asof(dfCamera, dfRadar, on='timeInt', direction='nearest',
                           tolerance=timeSync)  # there is a bug with merge_asof which changes the value sof dfRadar['timestampRadar'] if not str

    # Clean dfSync
    dfSync = dfSync.dropna()

    dfSync = dfSync.drop(['timeInt'], axis=1)

    dfSync = dfSync.rename(index=str, columns={"timestamp_x": "timestampCamera", "timestamp_y": "timestampRadar"})

    dfSync.timestampCamera = pd.to_numeric(dfSync.timestampCamera, errors='coerce', downcast='integer')
    dfSync.timestampRadar = pd.to_numeric(dfSync.timestampRadar, errors='coerce', downcast='integer')

    dfSync = dfSync.drop_duplicates('timestampCamera')
    dfSync = dfSync.drop_duplicates('timestampRadar')
    dfSync = dfSync.reset_index(drop=True)
    dfSync['idRadar'] = dfSync['idRadar'].astype(np.int64)

    dfSync.to_csv(settings.dst_path + '/syncData.csv', sep=',', index=False)
    dfSync.to_csv(settings.dst_path + '/filteredData.csv', sep=',', index=False)

    # Print stats
    print('# of Synced Meas:', len(dfSync.index))
    print('Time Sync:', int(timeSync / 1000000), 'ms')
    print('Processing time:', np.round(time.time() - t1, 1), 's')
    return dfSync, dfRadar, dfCamera


def deleteSyncedFrame(tsCamera):
    dfSync = pd.read_csv(settings.dst_path + '/filteredData.csv', index_col=None)
    dfSync = dfSync[dfSync.timestampCamera != tsCamera]
    dfSync.to_csv(settings.dst_path + '/filteredData.csv', sep=',', index=False)


def updateProgressBar():
    # settings.mutex.acquire()
    # try:
    #     settings.pbar.n = settings.pbar.n + 1  # check this
    #     settings.pbar.refresh()  # check this
    # except:
    #     pass
    # finally:
    #     settings.mutex.release()
    return


def updateProgressBar_old():
    settings.mutex.acquire()
    try:
        settings.pbar.n = settings.pbar.n + 1  # check this
        settings.pbar.refresh()  # check this
    except:
        pass
    finally:
        settings.mutex.release()


def getFrameFormats(rgb_flag, depth_flag, pc_flag):
    frame_formats = 0
    if (rgb_flag):
        frame_formats |= WiseSDK.CAMERA_FORMAT_TYPE_E.CAMERA_RGB_TYPE
    if (depth_flag):
        frame_formats |= WiseSDK.CAMERA_FORMAT_TYPE_E.CAMERA_DEPTH_TYPE
    if (pc_flag):
        frame_formats |= WiseSDK.CAMERA_FORMAT_TYPE_E.CAMERA_PC_TYPE

    return frame_formats


def getCameraSavePath(frame_format, frame_ts):
    if (frame_format == WiseSDK.CAMERA_FORMAT_TYPE_E.CAMERA_RGB_TYPE):
        return '/rgbCamera/' + "rgbCamera_" + str(frame_ts) + ".png"
    elif (frame_format == WiseSDK.CAMERA_FORMAT_TYPE_E.CAMERA_RGB_TYPE):
        return '/rgbCamera/' + "rgbCamera_" + str(frame_ts) + ".png"
    elif (frame_format == WiseSDK.CAMERA_FORMAT_TYPE_E.CAMERA_DEPTH_TYPE):
        return '/depthCamera/' + "depthCamera_" + str(frame_ts) + ".png"
    elif (frame_format == WiseSDK.CAMERA_FORMAT_TYPE_E.CAMERA_PC_TYPE):
        return '/pcCamera/' + "pcCamera_" + str(frame_ts) + ".xyz"
    else:
        return ""


def getRadarSavePath(data_type, frame_ts, img_alg_type):
    if data_type == WiseSDK.DATA_TYPE_E.RANGE_DOPPLER_DATA_TYPE:
        return '/S_ANT/S_ANT_' + str(frame_ts)
    elif img_alg_type == 2:
        return '/cuda_4dfft/cuda_4dfft_' + str(frame_ts)
    elif data_type == WiseSDK.DATA_TYPE_E.POINT_CLOUD_DATA_TYPE:
        return '/cuda_delta/cuda_delta_' + str(frame_ts)
    else:
        return ""


def getCameraFrame(timestamp, frame_format):
    status = -1

    src_path = settings.cameraSrc_path
    dst_path = settings.dst_path

    idx = dfSync.index[dfSync['timestampCamera'] == np.uint64(timestamp)].values
    try:
        dfRow = dfSync.iloc[idx[0]]
    except:
        print('index is out of boundaries!')

    cameraFilename = dfRow.loc['filenameCamera']
    cameraFrameIdx = ctypes.c_uint(int(dfRow.loc['inFileIdxCamera'])).value
    cameraFrameTs = ctypes.c_ulong(dfRow.loc['timestampCamera']).value
    suffix = getCameraSavePath(frame_format, str(cameraFrameTs))
    if (os.path.isfile(dst_path + getCameraFormatSavePath(frame_format, cameraFrameTs))):
        # print "Found it! was already extracted from .svo"
        return cv2.imread(dst_path + getCameraFormatSavePath(frame_format, cameraFrameTs))

    status = WiseSDK.saveCameraFrame(str(src_path + settings.cameraDataRaw_path + cameraFilename), dst_path,
                                     cameraFrameIdx, cameraFrameTs, frame_format)

    if (status != 0):
        return None

    return cv2.imread(dst_path + getCameraFormatSavePath(frame_format, cameraFrameTs))


def openCameraFramesByFolder(dfData, rgb_flag, depth_flag, pc_flag, context):
    # pbar = tqdm(range(len(dfData.index)))
    # pbar.update(0)
    # pbar.refresh()

    cameraFilenames = dfData.filenameCamera.unique()
    print("cameraFilenames", len(cameraFilenames))
    for cameraFilename in tqdm(cameraFilenames):
        timestamps = np.array(dfData.loc[dfData['filenameCamera'] == cameraFilename, 'timestampCamera']).astype(np.int64)
        indices = np.array(dfData.loc[dfData['filenameCamera'] == cameraFilename, 'inFileIdxCamera']).astype(np.int64)
        # print(str(settings.cameraSrc_path + settings.cameraDataRaw_path + str(cameraFilename)))
        status = WiseSDK.saveCameraFrame(
            str(context.cameraDataRaw_path + str(cameraFilename)), context.destination_path,
            indices, timestamps, getFrameFormats(rgb_flag, depth_flag, pc_flag), updateProgressBar)
    # print('extracted ' + str(settings.pbar.n) + ' out of ' + str(len(dfData.index)) + ' frames')
    return


def openCameraFramesByFolder_old(dfData, rgb_flag, depth_flag, pc_flag, settings):
    del settings.pbar
    settings.pbar = tqdm(range(len(dfData.index)))
    settings.pbar.update(0)
    settings.pbar.refresh()

    cameraFilenames = dfData.filenameCamera.unique()
    for cameraFilename in cameraFilenames:
        timestamps = np.array(dfData.loc[dfData['filenameCamera'] == cameraFilename, 'timestampCamera'])
        indices = np.array(dfData.loc[dfData['filenameCamera'] == cameraFilename, 'inFileIdxCamera'])
        # print(str(settings.cameraSrc_path + settings.cameraDataRaw_path + str(cameraFilename)))
        status = WiseSDK.saveCameraFrame(
            str(settings.cameraSrc_path + settings.cameraDataRaw_path + str(cameraFilename)), settings.dst_path,
            indices, timestamps, \
            getFrameFormats(rgb_flag, depth_flag, pc_flag), updateProgressBar)
    print('extracted ' + str(settings.pbar.n) + ' out of ' + str(len(dfData.index)) + ' frames')
    return


def getRadarFrame(timestamp, dfSync, settings):
    # src_path = settings.radarSrc_path
    # dst_path = settings.dst_path
    idx = dfSync.index[dfSync['timestampRadar'] == timestamp].values
    try:
        dfRow = dfSync.iloc[idx[0]]
    except:
        print('index is out of boundaries!')

    radarFilename = dfRow.loc['filenameRadar']
    radarFrameIdx = ctypes.c_uint(int(dfRow.loc['inFileIdxRadar'])).value
    radarPath = str(settings.radarSrc_path + radarFilename)
    # print("Cur Path: ", radarPath, " idx ", radarFrameIdx, "size", Path(radarPath).stat().st_size)
    if not os.path.exists(radarPath):
        print("warning - frame path doesn't exist")
        print("Cur Path: ", radarPath, " idx ", radarFrameIdx, "size", Path(radarPath).stat().st_size)
    frame = WiseSDK.getRadarFrame(radarPath, radarFrameIdx)

    if (frame.getHeader().dims.size1) == 0:
        print("Error! Frame was not found");

    return frame


def getRadarFrame_old(timestamp, dfSync):
    src_path = settings.radarSrc_path
    dst_path = settings.dst_path
    idx = dfSync.index[dfSync['timestampRadar'] == timestamp].values
    try:
        dfRow = dfSync.iloc[idx[0]]
    except:
        print('index is out of boundaries!')

    radarFilename = dfRow.loc['filenameRadar']
    radarFrameIdx = ctypes.c_uint(int(dfRow.loc['inFileIdxRadar'])).value
    radarPath = str(src_path + settings.radarDataRaw_path + radarFilename)
    # print("Cur Path: ", radarPath, " idx ", radarFrameIdx, "size", Path(radarPath).stat().st_size)
    if not os.path.exists(radarPath):
        print("warning - frame path doesn't exist")
        print("Cur Path: ", radarPath, " idx ", radarFrameIdx, "size", Path(radarPath).stat().st_size)
    frame = WiseSDK.getRadarFrame(radarPath, radarFrameIdx)

    if (frame.getHeader().dims.size1) == 0:
        print("Error! Frame was not found");

    return frame


def orderData(frame, settings):
    if (frame.getDataType() == WiseSDK.DATA_TYPE_E.POINT_CLOUD_DATA_TYPE):

        header = frame.getHeader()
        data = frame.getData()
        num_of_fields = int(data.size / header.dims.size2)
        data_shape = [header.dims.size2, num_of_fields]
        pc = pd.DataFrame(data=np.reshape(data, data_shape),
                          columns=["x", "y", "z", "r", "u_sin", "v_sin", "dop", "uncorrected_dop", "value_real", "power_im", "noise", "cluster_id", "x_global", "y_global", "trk_id", "vx", "vy", "vz",
                                   "trk_extrapolation_flag"])

        pc_metadata = frame.getMetadata()
        axis_params = pc_metadata.dict()['axis_params']
        # range, az, elevation. doppler

        range_bin = (axis_params[0][1] - axis_params[0][0]) / (axis_params[0][2] - 1)
        u_bin = (axis_params[1][1] - axis_params[1][0]) / (axis_params[1][2] - 1)
        v_bin = (axis_params[2][1] - axis_params[2][0]) / (axis_params[2][2] - 1)
        dop_bin = (axis_params[3][1] - axis_params[3][0]) / (axis_params[3][2] - 1)

        range_indices = np.round(((pc["r"].values - axis_params[0][0]) / range_bin)).astype(np.int)
        az_indices = np.round(((pc["u_sin"].values - axis_params[1][0]) / u_bin)).astype(np.int)
        el_indices = np.round(((pc["v_sin"].values - axis_params[2][0]) / v_bin)).astype(np.int)
        dop_indices = np.round((pc["dop"].values - axis_params[3][0]) / dop_bin).astype(np.int)

        pc["range_indices"] = range_indices
        pc["az_indices"] = az_indices
        pc["el_indices"] = el_indices
        pc["dop_indices"] = dop_indices

        # pc["intensity"] = db(np.abs(pc["value_real"].values*pc["value_real"].values + pc["power_im"].values*pc["power_im"].values))

        print("len - pc", len(pc))
        # n_detections = frame.getHeader().dims.size2
        # data = frame.getData().reshape((n_detections, 18))
        # pc = np.zeros((8, n_detections), dtype=np.float32)
        # pc[0] = data[:, 0]
        # pc[1] = data[:, 1]
        # pc[2] = data[:, 2]
        # pc[3] = data[:, 3]
        # pc[4] = data[:, 4]
        # pc[5] = data[:, 5]
        # pc[6] = data[:, 6]
        # pc[7] = data[:, 9]
        #
        # pc[0] = data[:, 0]
        # pc[1] = data[:, 1]
        # pc[2] = data[:, 2]
        # pc[3] = data[:, 3]
        # pc[4] = data[:, 4]
        # pc[5] = data[:, 5]
        # pc[6] = data[:, 6]
        # pc[7] = data[:, 9]

        # n_detections = frame.getHeader().dims.size2
        # print("frame.getHeader().element_size ", frame.getHeader().element_size)
        # print("frame.getData().shape", frame.getData().shape)
        # data = frame.getData().reshape((n_detections, 8))
        # print("data.dtype ", data.dtype, "data.shape", data.shape)
        # print("before data[0,:] before", data[:,0][:10])
        # print("before data[1,:] before", data[:,1][:10])
        # print("before data[2,:] before", data[:,2][:10])
        # print("before data[3,:] before", data[:,3][:10])

        # PointCloud = {"R": [], "U": [], "V": [], "vel": [], "intensity": []}
        # metadata = frame.getMetadata()

        # radar_param = saveRadarParams(frame, settings=settings)

        # n_detections = frame.getHeader().dims.size2
        # data = frame.getData().reshape((n_detections, 18))
        # pc = np.zeros((8, n_detections), dtype=np.float32)
        # pc[0] = data[:, 0]
        # pc[1] = data[:, 1]
        # pc[2] = data[:, 2]
        # pc[3] = data[:, 3]
        # pc[4] = data[:, 4]
        # pc[5] = data[:, 5]
        # pc[6] = data[:, 6]
        # pc[7] = data[:, 9]

        # PointCloud['R'] = radar_param["rng_vec"][0] + PointCloud['R'] * radar_param["rng_bin"]
        # PointCloud['U'] = np.sin(np.deg2rad(radar_param["az_vec"][0] + PointCloud['U'] * radar_param["az_bin"]))
        # PointCloud['V'] = np.sin(np.deg2rad(radar_param["el_vec"][0] + PointCloud['V'] * radar_param["el_bin"]))
        # PointCloud['vel'] = radar_param["dop_vec"][0] + PointCloud['vel'] * radar_param["dop_bin"]

        # print("PointCloud['R'][0] ", PointCloud['R'][:10])
        # print("PointCloud['U'][0] ", PointCloud['U'][:10])
        # print("PointCloud['V'][0] ", PointCloud['V'][:10])
        # print("PointCloud['vel'][0] ", PointCloud['vel'][:10])

        # PointCloud_Cartesian = {"x": [], "y": [], "z": [], "doppler": [], "intensity": []}
        # PointCloud_Cartesian["x"] = PointCloud['R'] * np.sqrt(
        #     1 - PointCloud['U'] * PointCloud['U'] - PointCloud['V'] * PointCloud['V'])
        # PointCloud_Cartesian["y"] = PointCloud['R'] * PointCloud['U']
        # PointCloud_Cartesian["z"] = PointCloud['R'] * PointCloud['V']
        # PointCloud_Cartesian["doppler"] = PointCloud['vel']
        # PointCloud_Cartesian["intensity"] = PointCloud['intensity']

        # print("PointCloud_Cartesian['x'][0] ", PointCloud_Cartesian["x"][:10])

        return pc
    elif (frame.getDataType() == WiseSDK.DATA_TYPE_E.RANGE_DOPPLER_DATA_TYPE):
        header = frame.getHeader()
        data = np.reshape(frame.getData(),
                          (header.dims.size1, header.dims.size2 * header.dims.size3, header.dims.size4), order='F')
        return data
    else:
        return None


def match_time(timestamps_df, radar_df, camera_df, key='timestampCamera'):
    print("In Match Time")
    radar_arr = radar_df.loc[:, 'timestampRadar'].to_numpy(dtype='int64')[1:]

    camera_arr = camera_df.loc[:, key].to_numpy(dtype='int64')[1:]
    timestamps_arr = timestamps_df.to_numpy(dtype='int64')[1:]

    final_camera_arr = np.copy(camera_arr)
    print("len(final_camera_arr", len(final_camera_arr))
    # Camera Original TimeStamps
    camera_ts = camera_arr[:].astype("int64")

    for index, val in enumerate(camera_arr[:-1]):
        # Get ts to fix from radar csv
        t_s = float(val)

        # find best index from matching timestamps
        best_index = np.argmin(np.abs((timestamps_arr[:, 0] - t_s)))

        # Get Camera timestamp
        matched_radar = timestamps_arr[best_index][1]
        matched_camera = timestamps_arr[best_index][0]

        # Get the offset
        offset = matched_camera - matched_radar

        final_camera_arr[index] -= offset
    #         print(offset, best_index, camera_df.iloc[index+1,1], final_camera_arr[index])

    camera_df.iloc[1:, 1] = final_camera_arr
    return camera_df


def save_obj(obj, path):
    with open(path, 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)


def load_obj(path):
    with open(path, 'rb') as f:
        return pickle.load(f)


def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    tb = traceback.format_exc()
    print(tb)
    print('EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj))


def save_radar_param(frame, settings, suffix=""):
    radar_param = {}
    ax = frame.getAxisParams()

    headers = ["range", "az", "el", "doppler"]
    for i, name in enumerate(headers):
        radar_param[name] = [ax[i].min, ax[i].max, ax[i].num_bins]

    pprint(radar_param)
    save_path = settings.dst_path + "/radar_param_" + str(suffix) + ".pkl"

    save_obj(radar_param, save_path)
    d = load_obj(save_path)

    print("Radar Param: ", suffix, d)
    return True


wrote_radar_params = False


def saveRadarFrame(frame, df, index, settings, img_alg_type=0, suffix=""):
    global wrote_radar_params
    if index == 0:
        if frame.getDataType() == WiseSDK.DATA_TYPE_E.POINT_CLOUD_DATA_TYPE and not wrote_radar_params:
            save_radar_param(frame, settings=settings, suffix="point_cloud")
            wrote_radar_params = True
        if frame.getDataType() == WiseSDK.DATA_TYPE_E.RANGE_DOPPLER_DATA_TYPE:
            save_radar_param(frame, settings=settings, suffix="range_dop")

    frame_timestamp = frame.getHeader().timestamp

    if frame.getDataType() == WiseSDK.DATA_TYPE_E.POINT_CLOUD_DATA_TYPE:
        PointCloud = orderData(frame, settings=settings)
        frame_path = settings.dst_path + getRadarSavePath(frame.getDataType(), frame_timestamp, img_alg_type=img_alg_type) + str(suffix) + ".csv"
        if img_alg_type == 2:
            PointCloud = PointCloud.drop(columns=["cluster_id", "x_global", "y_global", "trk_id", "vx", "vy", "vz", "trk_extrapolation_flag"])

        PointCloud.to_csv(frame_path, index=False)

    elif frame.getDataType() == WiseSDK.DATA_TYPE_E.RANGE_DOPPLER_DATA_TYPE:
        RD = orderData(frame, settings=settings)
        framePath = settings.dst_path + getRadarSavePath(frame.getDataType(), frame_timestamp, img_alg_type=img_alg_type)
        np.save(framePath, RD)
    else:
        print("saveRadarFrame: Error! data type " + str(frame.getDataType()) + " is not supported")


def syncRadarOnly():
    # Define folders
    radarPath = settings.radarSrc_path + settings.radarDataRaw_path

    t1 = time.time()

    # Read radar CSV
    dfRadar = pd.DataFrame()
    radarCSVs = [f for f in os.listdir(radarPath) if os.path.splitext(f)[-1] == '.csv']
    list_ = []
    for file_ in radarCSVs:
        try:
            df = pd.read_csv(radarPath + '/' + file_, index_col=None)
            try:
                df.__delitem__("csvVersion")
            except:
                pass
            file_ = file_.replace(".csv", ".raw")
            df.loc[:, 'filenameRadar'] = file_
            df.loc[:, 'inFileIdxRadar'] = np.arange(len(df))

        except:
            continue
        list_.append(df)
    dfRadar = pd.concat(list_, ignore_index=True)
    dfRadar = dfRadar.dropna(axis=1, how='all')
    print('# of radar frames:', dfRadar.shape[0])

    # Sync image and radar df using timestamp
    dfRadar['timeInt'] = dfRadar['timestampRadar']
    dfRadar = dfRadar.sort_values(by='timestampRadar')
    dfRadar = dfRadar.reset_index(drop=True)
    dfRadar['timestampRadar'] = dfRadar['timestampRadar']
    return dfRadar


def set_config(config_name, apply=True):
    # pass
    c_path = module_path + "/configs/" + config_name + ".json"
    with open(c_path, 'r') as fp:
        dsp_configuration = json.load(fp)
        # print("LOADED ", dsp_configuration)

    # with open(c_path, 'rb') as fp:
    #     dsp_configuration = pickle.load(fp)
    #     print("LOADED ", dsp_configuration)

    # print(dsp_configuration)
    server_id = WiseSDK.SERVER_ID_E.SERVER_ID_INTERNAL
    if apply:
        print("SETTING ", type(dsp_configuration), dsp_configuration)
        if WiseSDK.setDSPParameters(dsp_configuration, server_id) == -1:
            print("ERRORRRR in setting config")
            return
        print("SET CONFIGS SUCCESSFULLY")
        time.sleep(10)
    return dsp_configuration



def dsp(dfData, force=False, imaging_flag=False, s_ant=False, context=None, img_alg_type=0):

    for i in tqdm(range(len(dfData))):
        # TODO: re-arange for each data type (ANT ,ANT_DPS, pc)
        if i %5000 == 0:
            time.sleep(5)
        # if i == 0:
        #     print("sleeping")
        #     time.sleep(0)

        dsp_configuration = WiseSDK.DSPConfiguration().dict()
        #print("Default", dsp_configuration)
        dsp_configuration["ALGM"]["RNG_MIN"] = 0
        dsp_configuration["ALGM"]["RNG_MAX"] = 2000
        dsp_configuration["ALGI"]["COARS_CFAR_THR_DB"] = 10
        dsp_configuration["ALGI"]["FINE_CFAR_THR_DB"] = 6
        dsp_configuration["ALGM"]["EL_STEER_MIN_DEG"] = -15
        dsp_configuration["ALGM"]["EL_STEER_MAX_DEG"] = 15
        dsp_configuration["ALGM"]["AZ_STEER_MIN_DEG"] = -40
        dsp_configuration["ALGM"]["AZ_STEER_MAX_DEG"] = 40
        PAD_EL = 2
        PAD_AZ = 4
        if img_alg_type == 2:
            dsp_configuration["ALGM"]["RCM_FLAG"] = 0
        dsp_configuration["ALGI"]["IMG_ALG_TYPE"] = img_alg_type
        dsp_configuration["ALGI"]["SRA_ELP_FFT_OVERSAMPLING_FACT"] = PAD_EL
        dsp_configuration["ALGI"]["SRA_AZP_FFT_OVERSAMPLING_FACT"] = PAD_AZ

        # print("current config ", dsp_configuration)
        server_id = WiseSDK.SERVER_ID_E.SERVER_ID_INTERNAL
        WiseSDK.setDSPParameters(dsp_configuration, server_id)

        if i == 0:
            # print(dsp_configuration)
            time.sleep(8)
        else:
            time.sleep(0.02)

        try:
            radar_frame = getRadarFrame(dfData.loc[i, 'timestampRadar'], dfData, settings=context)

            if s_ant:
                if (radar_frame.getHeader().dims.size1 != 0):
                    # processed_frame = WiseSDK.processFrame(radar_frame, WiseSDK.DATA_TYPE_E.RANGE_DOPPLER_DATA_TYPE)
                    # processed_frame = WiseSDK.processFrame(radar_frame, WiseSDK.DATA_TYPE_E.RANGE_DOPPLER_DATA_TYPE)

                    processed_frame = WiseSDK.processFrame(radar_frame, WiseSDK.DATA_TYPE_E.RANGE_DOPPLER_DATA_TYPE)
                    if i == 0:
                        print("sleeping 3")
                        time.sleep(10)
                    # print(processed_frame.getHeader().waveform_id, processed_frame.getHeader().mode_id, processed_frame.getHeader().antenna_version, processed_frame.getHeader().rf_config_id)
                    # print(i)
                    # print(dfData.loc[i])
                    #
                    if (processed_frame.getHeader().dims.size1 > 0):
                        saveRadarFrame(processed_frame, dfData, i, settings=settings)
                        print("Writing S_ANT", i)

            if imaging_flag:
                if i == 0:
                    print("sleeping 2")
                    # point_cloud = WiseSDK.processFrame(radar_frame, WiseSDK.DATA_TYPE_E.POINT_CLOUD_DATA_TYPE)
                    time.sleep(1)

                if img_alg_type == 2:
                    if os.path.exists(settings.dst_path + "/cuda_4dfft/cuda_4dfft_" + str(dfData.loc[i, 'timestampRadar']) + ".csv") and not force:
                        continue
                elif img_alg_type == 0:
                    if os.path.exists(settings.dst_path + "/cuda_delta/cuda_delta_" + str(dfData.loc[i, 'timestampRadar']) + ".csv") and not force:
                        continue

                time.sleep(0.02)
                point_cloud = WiseSDK.processFrame(radar_frame, WiseSDK.DATA_TYPE_E.POINT_CLOUD_DATA_TYPE)
                if (point_cloud.getHeader().dims.size1 > 0):
                    saveRadarFrame(point_cloud, dfData, i, img_alg_type=img_alg_type, settings=settings)

                    metadata = point_cloud.getMetadata().dict()
                    # print(metadata)
                    name_map = {0: "cuda_delta", 2:"cuda_4dfft"}
                    with open(settings.dst_path + '/metadata/metadata_' + str(dfData.loc[i, 'timestampRadar']) + " " + str(name_map[img_alg_type]) + '.json', 'w') as fp:
                        json.dump(metadata, fp, sort_keys=True, indent=2)

                #####################
                # dsp_configuration = set_config("LIDAR", apply=False)
                # print(dsp_configuration)
                # for coarse in range(10, 20, 2):
                #     time.sleep(0.05)

                    # dsp_configuration = WiseSDK.DSPConfiguration().dict()
                    # print("Default", dsp_configuration)
                    # dsp_configuration["ALGM"]["RNG_MIN"] = 0
                    # dsp_configuration["ALGM"]["RNG_MAX"] = 2000
                    # dsp_configuration["ALGI"]["COARS_CFAR_THR_DB"] = coarse
                    # dsp_configuration["ALGI"]["FINE_CFAR_THR_DB"] = 6
                    # dsp_configuration["ALGM"]["EL_STEER_MIN_DEG"] = -15.0
                    # dsp_configuration["ALGM"]["EL_STEER_MAX_DEG"] = 15.0
                    # dsp_configuration["ALGM"]["AZ_STEER_MIN_DEG"] = -40.0
                    # dsp_configuration["ALGM"]["AZ_STEER_MAX_DEG"] = 40.0
                    # dsp_configuration["ALGI"]["IMG_ALG_TYPE"] = IMG_ALG_TYPE
                    # dsp_configuration["ALGI"]["SRA_ELP_FFT_OVERSAMPLING_FACT"] = PAD_EL
                    # dsp_configuration["ALGI"]["SRA_AZP_FFT_OVERSAMPLING_FACT"] = PAD_AZ
                    # WiseSDK.setDSPParameters(dsp_configuration, server_id)




                    # dsp_configuration = set_config("LIDAR", apply=False)
                    # dsp_configuration = WiseSDK.DSPConfiguration().dict()
                    # dsp_configuration["ALGI"]["COARS_CFAR_THR_DB"] = coarse
                    # print("Setting", dsp_configuration)
                    # if WiseSDK.setDSPParameters(dsp_configuration, server_id) == -1:
                    #     print("FAILED TO APPLY DSP CONFIGS")
                    # time.sleep(1)

                    # start = time.time()
                    # point_cloud = WiseSDK.processFrame(radar_frame, WiseSDK.DATA_TYPE_E.POINT_CLOUD_DATA_TYPE)
                    # # print("Processed", time.time() - start)
                    # start2 = time.time()
                    # saveRadarFrame(point_cloud, dfData, i, settings=settings, suffix="_coarse_cfar_" + str(coarse))
                    # print("Processed2", time.time() - start2)
                #####################

        except Exception as e:
            print("Error", e)
            PrintException()
            continue

    # WiseSDK_kill()
    print("Re-Saving dfData with MetaData")
    dfData.to_csv(settings.dst_path + '/syncData.csv', sep=',', index=False)
    return


def validateRadarOnly(dfData, idx, destinationPath, RD=True, force=False):
    def db(array):
        return 10 * np.log10(array)

    # plt.close()
    timestampRadar = str(dfData.loc[idx, 'timestampRadar'])

    os.makedirs(destinationPath + "/validation/", exist_ok=True)
    imSavePath = destinationPath + "/validation/" + "v_" + str(dfData.loc[idx, 'timestampRadar']) + ".png"
    if not force and os.path.exists(imSavePath) and os.path.getsize(imSavePath) > 800000:
        print("Skipping")
        return
    #     else:
    #         print("Size ", os.path.getsize(imSavePath), " force ", force, " exists ", os.path.exists(imSavePath))

    title = "Image " + str(dfData.loc[idx, 'timestampRadar']) + " Pred Includes:"

    plt.figure(figsize=(30, 15))
    if RD:
        sAntPath = destinationPath + '/S_ANT/S_ANT_' + str(dfData.loc[idx, 'timestampRadar']) + '.npy'
        if os.path.exists(sAntPath):
            S_ANT = np.load(sAntPath)  # (1021, 48, 116) complex64]
            title = "S_ANT Shape = " + str(S_ANT.shape)
            # Build NCI
            nci = np.zeros([S_ANT.shape[0], S_ANT.shape[2]])
            for i in range(S_ANT.shape[1]):
                nci += np.abs(S_ANT[:, i, :])

            title += ", NCI Shape " + str(nci.shape)
            plt.subplot(2, 2, 2, title=title)
            nci = np.fft.fftshift(nci, axes=(1,))
            curPlot = db(nci).T
            h, w = curPlot.shape[:2]

            curPlot = cv2.resize(curPlot, dsize=(int(h) * 3, w), interpolation=cv2.INTER_NEAREST)
            plt.imshow(cv2.resize(curPlot, (curPlot.shape[0] * 3, curPlot.shape[1])), cmap="jet", origin="lower")
            plt.colorbar(orientation='horizontal')

            # Build channelsArray
            allChannels = np.abs(S_ANT[:, 1])

            for i in range(S_ANT.shape[1])[1:]:
                curChannel = np.abs(S_ANT[:, i])
                allChannels = np.concatenate([allChannels, curChannel], axis=1)

            scale_y = 4
            allChannels = cv2.resize(allChannels, (allChannels.shape[1], allChannels.shape[0] * scale_y))

            plt.subplot(2, 2, 4)
            plt.imshow(db(allChannels), cmap="jet")
            plt.colorbar(orientation='horizontal')
            title += ", Channels Shape " + str(allChannels.shape)
            title += " RD, "

            im_path = destinationPath + "/camera_rgb/rgb_camera_" + str(dfData.loc[idx, 'timestampCamera']) + ".png"
            im_path_radar = destinationPath + "/camera_rgb/rgb_camera_" + str(
                dfData.loc[idx, 'timestampRadar']) + ".png"

            if not os.path.exists(im_path):
                print("Camera not exist")

            if not os.path.exists(im_path_radar):
                print("Radar not exist")

            plt.subplot(2, 2, 1)
            plt.imshow(cv2.cvtColor(cv2.imread(im_path), cv2.COLOR_BGR2RGB))

            ch_idx, ch_strength, ch_color = [], [], []

            # Channel strength
            for i in range(S_ANT.shape[1])[:]:
                curChannel = np.sum(np.abs(S_ANT[:, i]))
                ch_idx.append(i)
                ch_strength.append(curChannel)

            # jet = cm = plt.get_cmap('jet') 
            # cNorm  = colors.Normalize(vmin=np.min(np.array(ch_strength)), vmax=np.max(np.array(ch_strength)))
            # scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)

            # columns = np.arange(8)
            # rows = np.array(ch_strength).reshape((8, 6))
            # colors_cells = np.array([scalarMap.to_rgba(c)[:3] for c in ch_strength]).reshape((8, 6, 3))

            plt.subplot(2, 2, 3)
            y_pos = ch_idx
            performance = db(ch_strength)

            plt.bar(y_pos, performance, align='center', alpha=0.5)

            # plt.table(cellText=rows,colLabels=columns, cellColours=colors_cells, loc='center')
        else:
            print("Warning, S_ANT doesnt exists")

    plt.savefig(imSavePath)
    plt.close()


def WiseSDK_kill():
    time.sleep(1)
    WiseSDK.modulesDestroy()


def WiseSDK_init():
    time.sleep(1)
    reload(WiseSDK)
    time.sleep(1)
    WiseSDK.modulesInit()


def cleanData(dfData):
    missingFrames = []
    missingRadar = 0
    missingCamera = 0
    missingLabel = 0
    for idx in tqdm(range(len(dfData))):
        pathDepth = settings.dst_path + '/depthCamera/depthCamera_' + str(dfData.loc[idx, 'timestampCamera']) + '.png'
        pathRGB = settings.dst_path + '/rgbCamera/rgbCamera_' + str(dfData.loc[idx, 'timestampCamera']) + '.png'
        pathANT = settings.dst_path + '/ANTRadar_DPS/S_ANT_DPS_' + str(dfData.loc[idx, 'timestampRadar']) + '.npy'
        #     pathPC = basePath + '/radarTrainPC/pc_' + str(dfData.loc[idx,'timestampRadar']) + '.mat'
        #     pathSegLabel = basePath + '/cameraSegLabel/' + str(dfData.loc[idx,'timestampCamera'])+'.npz'
        #     pathOBLabel = basePath + '/cameraODLabel/label_' + str(dfData.loc[idx,'timestampCamera'])+'.csv'

        if not os.path.isfile(pathANT):  # or os.path.isfile(pathPC):
            missingRadar += 1
            missingFrames.append(idx)
        if not (os.path.isfile(pathDepth) or os.path.isfile(pathRGB)):
            missingCamera += 1
            missingFrames.append(idx)
    #     if not os.path.isfile(pathOBLabel) or os.path.isfile(pathSegLabel):
    #         missingLabel+=1
    #         missingFrames.append(idx)

    syncData = len(dfData)
    print('# of synced frames:', syncData)
    dfData.drop(dfData.index[missingFrames], inplace=True)
    dfData = dfData.reset_index(drop=True)
    dfData.to_csv(settings.dst_path + '/trainData.csv', sep=',', index=False)
    #     print('# of train frames:',len(dfData))
    #     print('# of missing frames:',syncData - len(dfData))
    print('# of missing rgb frames:', missingCamera)
    print('# of missing labels:', missingLabel)
    print('# of missing radar frames:', missingRadar)
