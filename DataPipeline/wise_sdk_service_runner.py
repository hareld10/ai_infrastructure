import WiseSDK
# from WiseSDK.lidar.LidarDirectory import LidarDirectory
import numpy as np
# WiseSDK.modulesInit()

sync_th = 20

base_dir = "/workspace/data_backup10/wise4/2_Urban_GT_WF_198/"
server_id = WiseSDK.SERVER_ID_E.SERVER_ID_INTERNAL

# radar_dir = WiseSDK.RadarDirectory(base_dir + 'radarRawData/')
# camera_dir = WiseSDK.CameraDirectory(base_dir + 'cameraRawData/')
# gps_dir = WiseSDK.GPSDirectory(base_dir + 'gpsRawData/')
# lidar_dir = LidarDirectory(base_dir + 'lidarRawData/')
#
# N = radar_dir.getNumFrames()
# for i in range(N):
#     radar_ts = radar_dir.getFrameTimestamp(i)
#     try:
#         camera = camera_dir.getSyncedFrames(radar_ts, sync_th)
#         gps = gps_dir.getSyncedFrames(radar_ts, sync_th)
#         lidar = lidar_dir.getSyncedFrames(radar_ts, sync_th)
#     except:
#         print("No synced frame")
#
nRadar, nCamera, nGPS = WiseSDK.getDirectoryNumFrames(base_dir)
print(f"nRadar={nRadar}, nCamera={nCamera}, nGPS={nGPS}")

idx = np.random.randint(nRadar)
print("idx=", idx)

# for/ try except
radar, camera, gps = WiseSDK.getSyncedFrames(base_dir, idx, sync_th)

# todo build csv
# timestamps of all sensors
# raw file names and indices

print(radar.getHeader().dict())


# todo set calibration
gen_type = [WiseSDK.DATA_TYPE_E.PROCESSED_POINT_CLOUD_DATA_TYPE]
point_cloud = WiseSDK.processFrame(radar, gen_type[0])

# timestamps
print(point_cloud)