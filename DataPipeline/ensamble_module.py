import numpy as np
import os
from tqdm import tqdm
import glob
import sys
import json
import os
import sys
import time
import datetime
import argparse
import matplotlib.pyplot as plt
import pandas as pd
from tqdm import tqdm
from PIL import Image
import cv2
import torch
relevant_class = {"person":True, "pedestrian":True}
nms_thresh = 0.4
yolo_thresh = 0.7
d7_thresh = 0.5
iou_thresh = 0.5

import matplotlib
font = {
        'weight' : 'bold',
        'size'   : 28}

matplotlib.rc('font', **font)


def read_label(path):
    with open(path, "r") as fp:
        frame_data = json.load(fp)
    return frame_data


def get_rel_classes(frame_data):
    ret = []
    irrelevant = []
    for i in range(len(frame_data)):
        if frame_data[i]["cat"] in relevant_class:
            ret.append(frame_data[i])
        else:
            irrelevant.append(frame_data[i])
    return ret, irrelevant


def non_max_suppression_fast(frame_data, overlapThresh):
    # if there are no boxes, return an empty list
    boxes = []
    for elem in frame_data:
        boxes.append([elem["bbox"][0], elem["bbox"][1], elem["bbox"][2], elem["bbox"][3]])

    boxes = np.array(boxes)
    if len(boxes) == 0:
        return []
    # if the bounding boxes integers, convert them to floats --
    # this is important since we'll be doing a bunch of divisions

    if boxes.dtype.kind == "i":
        boxes = boxes.astype("float")

    # initialize the list of picked indexes
    pick = []
    # grab the coordinates of the bounding boxes
    x1 = boxes[:, 0]
    y1 = boxes[:, 1]
    x2 = boxes[:, 2]
    y2 = boxes[:, 3]
    # compute the area of the bounding boxes and sort the bounding
    # boxes by the bottom-right y-coordinate of the bounding box
    area = (x2 - x1 + 1) * (y2 - y1 + 1)
    idxs = np.argsort(y2)
    # keep looping while some indexes still remain in the indexes
    # list
    while len(idxs) > 0:
        # grab the last index in the indexes list and add the
        # index value to the list of picked indexes
        last = len(idxs) - 1
        i = idxs[last]
        pick.append(i)
        # find the largest (x, y) coordinates for the start of
        # the bounding box and the smallest (x, y) coordinates
        # for the end of the bounding box
        xx1 = np.maximum(x1[i], x1[idxs[:last]])
        yy1 = np.maximum(y1[i], y1[idxs[:last]])
        xx2 = np.minimum(x2[i], x2[idxs[:last]])
        yy2 = np.minimum(y2[i], y2[idxs[:last]])
        # compute the width and height of the bounding box
        w = np.maximum(0, xx2 - xx1 + 1)
        h = np.maximum(0, yy2 - yy1 + 1)
        # compute the ratio of overlap
        overlap = (w * h) / area[idxs[:last]]
        # delete all indexes from the index list that have
        idxs = np.delete(idxs, np.concatenate(([last],
                                               np.where(overlap > overlapThresh)[0])))
    # return only the bounding boxes that were picked using the
    # integer data type

    # build back frame data

    return np.array(frame_data)[pick].tolist()


def get_iou(a, b, epsilon=1e-5):
    """ Given two boxes `a` and `b` defined as a list of four numbers:
            [x1,y1,x2,y2]
        where:
            x1,y1 represent the upper left corner
            x2,y2 represent the lower right corner
        It returns the Intersect of Union score for these two boxes.

    Args:
        a:          (list of 4 numbers) [x1,y1,x2,y2]
        b:          (list of 4 numbers) [x1,y1,x2,y2]
        epsilon:    (float) Small value to prevent division by zero

    Returns:
        (float) The Intersect of Union score.
    """
    # COORDINATES OF THE INTERSECTION BOX
    x1 = max(a[0], b[0])
    y1 = max(a[1], b[1])
    x2 = min(a[2], b[2])
    y2 = min(a[3], b[3])

    # AREA OF OVERLAP - Area where the boxes intersect
    width = (x2 - x1)
    height = (y2 - y1)
    # handle case where there is NO overlap
    if (width < 0) or (height < 0):
        return 0.0
    area_overlap = width * height

    # COMBINED AREA
    area_a = (a[2] - a[0]) * (a[3] - a[1])
    area_b = (b[2] - b[0]) * (b[3] - b[1])
    area_combined = area_a + area_b - area_overlap

    # RATIO OF AREA OF OVERLAP OVER COMBINED AREA
    iou = area_overlap / (area_combined + epsilon)
    return iou


def draw_pred_on_img(preds, img, color):
    for cur_label in preds:
        obj = cur_label['cat']
        cat_id = cur_label['cat_id']
        score = cur_label['score']
        x1, y1, x2, y2 = cur_label['bbox']
        cv2.rectangle(img, (x1, y1), (x2, y2), color, 2)
        cv2.putText(img, '{}, {:.3f}'.format(obj, score),
                    (x1, y1 + 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                    (color), 2)
    return img


def filter_single(preds, thresh):
    final_preds = []
    to_check = []
    for i in range(len(preds)):
        if preds[i]["score"] > thresh:
            final_preds.append(preds[i])
        else:
            to_check.append(preds[i])
    return final_preds, to_check


def filter_check(yolo_check, d7_check):
    after_filter = []
    for yolo_idx in range(len(yolo_check)):
        for d7_idx in range(len(d7_check)):
            cur_iou = get_iou(yolo_check[yolo_idx]["bbox"], d7_check[d7_idx]["bbox"])
            if cur_iou > iou_thresh:
                new_label = {}
                new_label['cat'] = "pedestrian"
                new_label['cat_id'] = "undefined"
                new_label['score'] = np.clip(yolo_check[yolo_idx]["score"] + d7_check[d7_idx]["score"], 0, 1)
                x1, y1, x2, y2 = yolo_check[yolo_idx]["bbox"]
                new_label['bbox'] = [int(x1), int(y1), int(x2), int(y2)]
                new_label['img_res'] = [940, 1824]
                after_filter.append(new_label)
    return after_filter


def filter_preds(yolo_preds, d7_preds):
    d7_final, d7_check = filter_single(d7_preds, d7_thresh)
    yolo_final, yolo_check = filter_single(yolo_preds, yolo_thresh)

    finals = d7_final + yolo_final
    checked = filter_check(yolo_check, d7_check)
    finals += checked

    # NMS
    finals = non_max_suppression_fast(finals, nms_thresh)
    return finals, len(checked)


def ensemble_from_dir(base_path, df, force=False):
    base_path_ensembles = base_path + "/camera_det_ensemble/"
    os.makedirs(base_path_ensembles, exist_ok=True)

    base_path_ensembles_images = base_path_ensembles + "/images/"
    os.makedirs(base_path_ensembles_images, exist_ok=True)

    for idx in tqdm(range(len(df))):
        rgbImPath = base_path + "/rgbCamera/rgbCamera_" + str(df.iloc[idx]["timestampCamera"]) + ".png"
        e_det = base_path + "/camera_det_EfficientDetD7/camera_det_" + str(
            df.iloc[idx]["timestampCamera"]) + ".json"
        yolo_det = base_path + "/camera_det_yoloV3/camera_det_yoloV3_" + str(
            df.iloc[idx]["timestampCamera"]) + ".json"

        if not os.path.exists(yolo_det) or not os.path.exists(e_det) :
            print("some labels dont exists")
            continue

        label_path = base_path_ensembles + "/camera_det_ensemble_" + str(df.iloc[idx]["timestampCamera"]) + ".json"
        if os.path.exists(label_path) and not force:
            continue

        yolo_label, other_classes_yolo = get_rel_classes(read_label(yolo_det))
        d7_label, other_classes_d7 = get_rel_classes(read_label(e_det))

        finals, count = filter_preds(yolo_label, d7_label)

        if finals is not None:
            finals += other_classes_d7
        else:
            finals = other_classes_d7

        for j in range(len(finals)):
            finals[j]['img_res'] = [940, 1824]
            if finals[j]['cat'] == "person":
                finals[j]['cat'] = "pedestrian"

        with open(label_path, 'w') as fp:
            json.dump(finals, fp, sort_keys=True, indent=4)

        if idx % 50 == 0:
            if not os.path.exists(rgbImPath):
                continue
            img = cv2.cvtColor(cv2.imread(rgbImPath), cv2.COLOR_BGR2RGB)
            img_yolo = draw_pred_on_img(yolo_label, img.copy(), color=(0, 255, 0))
            plt.figure(figsize=(50, 25))
            plt.subplot(2, 2, 1)
            plt.title("Yolo")
            plt.imshow(img_yolo)

            img_d7 = draw_pred_on_img(d7_label, img.copy(), (0, 0, 255))
            plt.subplot(2, 2, 2)
            plt.title("D7")
            plt.imshow(img_d7)

            img_both = draw_pred_on_img(d7_label, img_yolo, (0, 0, 255))
            plt.subplot(2, 2, 3)
            plt.title("both")
            plt.imshow(img_both)

            img_final = draw_pred_on_img(finals, img.copy(), (255, 0, 0))
            plt.subplot(2, 2, 4)
            plt.title("after filter")
            plt.imshow(img_final)

            plt.savefig(base_path_ensembles_images + "/" + str(df.iloc[idx]["timestampCamera"]) + "_" + str(count) + ".png")
            plt.close()