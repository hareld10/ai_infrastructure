import os
import time

from DataPipeline.pipeline4 import SegFormer
import glob

# dir_path = "/workspace/HDD/Meir/radar/depth_estimation/kitti_depth_validation/depth_val/val_selection_cropped/"
# fs = glob.glob(dir_path + "/image/*.png")
# soft_base = f"{dir_path}/soft_path/"
# hard_base = f"{dir_path}/hard_path/"
# os.makedirs(soft_base, exist_ok=True)
# os.makedirs(hard_base, exist_ok=True)
# print(len(fs))

with SegFormer(None) as e:
    time.sleep(120000000)
    # for x in fs:
    #     name = os.path.split(x)[-1]
    #     soft_path = f"{soft_base}/soft_{name[:-4]}"
    #     hard_path = f"{hard_base}/hard_{name[:-4]}"
    #     e.single(x, soft_path, hard_path)