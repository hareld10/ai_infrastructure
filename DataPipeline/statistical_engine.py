import pathlib

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import Figure
from matplotlib import gridspec
import sys

from LabelsManipulation.camera_lidar_fusion import CameraLidarFusion
from Plotting.wise_plotter import project_delta_clusters

module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, module_path)
sys.path.insert(0, "/workspace/AI/Harel/AI_Infrastructure/")
from DataPipeline.src.GetEngines import get_engines
from shared_utils import read_label
import os
from wiseTypes.classes_dims import Road
from tqdm import tqdm
import numpy as np
import matplotlib.pyplot as plt
import os
import json
from multiprocessing import Process, Manager
import pandas as pd
import time
from scipy.spatial import distance
import scipy as sp
import scipy.interpolate
from sklearn.cluster import DBSCAN
from DataPipeline.shared_utils import PrintException, toRAzEl, pickle_write
from Evaluation.evaluation_utils import make_video_from_dir, draw
from OpenSource.WisenseEngine import WisenseEngine
from scipy.interpolate import interp1d, interp2d
import matplotlib
from pprint import pprint

video_dir = module_path + "/../LabelsManipulation/lidar_camera_fusion/"

# matplotlib.use('pdf')
plt.style.use('dark_background')
from wiseTypes.classes_dims import Road

kw_args = {"spatial_calibration_path": "/home/amper/AI/Harel/AI_Infrastructure/Calibrator/cali_200909_pitch_fix"}
font = {'size': 20}
matplotlib.rc('font', **font)

# from wise_sdk_runner import SDKRunner


def oversampling_analysis(engines, debug=False):
    el_oversampling = [(2, 10)]
    # save_path = module_path + "/../Statistics/delta_results/"
    # if not os.path.exists(save_path):
    #     print("save path not exists")
    # alg = CameraLidarFusion(dat)

    # cur_engine = engines[0]
    for cur_engine in engines:
        cur_engine.sample_df(10)
        manager = Manager()
        frames_lst = manager.dict()
        for engine_idx in range(len(cur_engine)):
            frames_lst[engine_idx] = manager.list()

        for el_pad in el_oversampling:
            print("running config", el_pad)

            def generate(engine, frame_dict):
                sdk = SDKRunner(context=engine.drive_context)
                c_path = module_path + "/configs/170121_delta.json"
                with open(c_path, 'r') as fp:
                    cur_config = json.load(fp)
                cur_config["ALGI"]["SRA_ELP_FFT_OVERSAMPLING_FACT"] = int(el_pad[0])
                cur_config["ALGI"]["SRA_AZP_FFT_OVERSAMPLING_FACT"] = int(el_pad[1])
                # cur_config["ALGM"]["EL_STEER_MIN_DEG"] = int(-20)
                # cur_config["ALGM"]["EL_STEER_MAX_DEG"] = int(20)
                print(cur_config)
                sdk.set_config_dict(cur_config)

                for engine_idx in tqdm(range(len(engine))):
                    try:
                        radar_p_delta = sdk.generate_online(df=engine.df, idx=engine_idx)
                        if radar_p_delta is not None:
                            print(len(radar_p_delta))
                            if engine_idx in frame_dict:
                                frame_dict[engine_idx].append(radar_p_delta)
                            else:
                                frame_dict[engine_idx] = [radar_p_delta]
                        else:
                            time.sleep(0.1)
                            print("pc is None")
                    except Exception as e:
                        PrintException()
                        if debug:
                            exit()
                        continue

            p = Process(target=generate, args=(cur_engine, frames_lst,))
            p.start()
            p.join()
            # p.terminate()

            for engine_idx, pcs in frames_lst.items():
                print(len(pcs))

        alg = CameraLidarFusion(data_engine=cur_engine)
        for engine_idx, pcs in frames_lst.items():
            if len(pcs) < len(el_oversampling):
                print("Too Small", len(pcs))
                continue
            for plot_idx, processed_delta in enumerate(pcs):
                print(np.unique(processed_delta["el_indices"]))

                im = cur_engine.get_image(engine_idx)
                # lidar_df = cur_engine.get_lidar_df(engine_idx)
                # lidar_df = lidar_df.apply(pd.to_numeric, errors='coerce')
                delta_clusters = cur_engine.get_clusters(p_delta=processed_delta)
                plt.close()
                plt.figure(figsize=(20, 10))
                project_delta_clusters(plt.gca(), delta_clusters, im=im)
                plt.savefig(video_dir + "/delta_projection_" + str(cur_engine.get_id(engine_idx)) + ".png")
                # lidar_camera_df = lidar_df[lidar_df['x_lidar_camera'].notna()]
                # labels_2d = cur_engine.get_2d_label(engine_idx)
                # alg.run_algorithm(labels_2d=labels_2d, lidar_df=lidar_camera_df, delta_clusters=delta_clusters)
                # alg.plot_instances(engine=cur_engine, idx=engine_idx, im=im, labels_2d=labels_2d, lidar_df=lidar_df, delta_clusters=delta_clusters, prefix=str(cur_engine.get_id(engine_idx)) + "_el_" + str(el_oversampling[plot_idx][0]) + "_az_" + str(el_oversampling[plot_idx][1]) + "_")
    # print(frames_lst)


def evaluate_configs(engines, force, debug=False):
    configs_matrix = np.asarray([[[11, 6, 10], [20, 6, 10], [30, 6, 10], [40, 6, 10]],
                                 [[11, 6, 10], [11, 10, 10], [11, 20, 10], [11, 30, 10]],
                                 [[11, 6, 5], [11, 6, 10], [11, 6, 20], [11, 6, 40]]]).astype(int)

    pprint(configs_matrix)
    result_matrix = np.zeros((3, 4), dtype=object)
    save_path = module_path + "/../Statistics/delta_results/"
    if not os.path.exists(save_path):
        print("save path not exists")

    for config_idx, _ in np.ndenumerate(result_matrix):
        config = configs_matrix[config_idx]
        print("running config", config, "config_idx", config_idx)
        for _engine in engines:
            def run_engine(engine, lst):
                sdk = SDKRunner(context=engine.drive_context)
                c_path = module_path + "/configs/170121_delta.json"
                with open(c_path, 'r') as fp:
                    cur_config = json.load(fp)
                cur_config["ALGI"]["COARS_CFAR_THR_DB"] = int(config[0])
                cur_config["ALGI"]["FINE_CFAR_THR_DB"] = int(config[1])
                cur_config["ALGI"]["FINE_CFAR_LOCAL_THR_DB"] = int(config[2])
                sdk.set_config_dict(cur_config)

                for engine_idx in tqdm(range(len(engine))):
                    try:

                        pc = sdk.generate_online(df=engine.df, idx=engine_idx)
                        if pc is not None:
                            lst.append(len(pc))
                        else:
                            time.sleep(0.1)
                            print("pc is None")
                    except Exception as e:
                        PrintException()
                        if debug:
                            exit()
                        continue

            manager = Manager()
            frames_lst = manager.list()
            p = Process(target=run_engine, args=(_engine, frames_lst,))
            p.start()
            p.join()
            p.terminate()
            result_matrix[config_idx] = result_matrix[config_idx] + list(frames_lst)

    pprint(result_matrix)
    np.save(save_path + "/matrix_.npy", result_matrix)
    count = sum([len(engine) for engine in engines])
    np.save(save_path + "/matrix_" + str(count) + ".npy", result_matrix)
    np.save(save_path + "/config_matrix.npy", configs_matrix)


if __name__ == '__main__':
    oversampling_analysis(get_engines(val_train="all"), debug=True)
