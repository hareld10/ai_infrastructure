import time
from copy import deepcopy

import pandas as pd
import sys, pathlib

from matplotlib import gridspec
from tqdm import tqdm
import os
import numpy as np
import matplotlib.pyplot as plt

module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, module_path)

from shared_utils import find_data_size_properties
import shutil

module_path = "/workspace/workspace/wise_sdk/SDK/python/"
sys.path.insert(0, module_path)

# module_path = "//home/amper/workspace/wise_sdk/SDK/python/"
# sys.path.insert(0, module_path)
import WiseSDK


class RawReader:
    def __init__(self, init=True):
        self.df = None
        # self.df_path = "/workspace/HDD/new_lidar_drives/200831_lidar_drive_1_urban_W92_M15_RF1/"
        # self.df = pd.read_csv(self.df_path + "/sample_df.csv", index_col=None)
        # self.df["raw_src"] = "/data_backup5/250mWF/200831_lidar_drive_1_urban_W92_M15_RF1/"
        # print("len df", len(self.df))
        self.init = init
        if self.init:
            WiseSDK.modulesInit()
        return

    def set_df(self, df):
        self.df = df
        return

    def __del__(self):
        if self.init:
            WiseSDK.modulesDestroy()

    @staticmethod
    def get_raw_directly(radar_path, idx):
        """
        @param radar_path:
        @param idx:
        @return: Rx, Samples, Tx, Pulses
        """
        radar_frame = WiseSDK.getRadarFrame(radar_path, int(float(idx)))
        header = radar_frame.getHeader()
        if (header.dims.size1 == 0):
            return [-1, [], [], [], [], []]

        num_samples, num_rx, num_tx, num_pulses = find_data_size_properties(radar_frame)
        z_out = radar_frame.getData()
        # z_out = np.reshape(z, (num_samples, num_rx, num_pulses * num_tx), order='F')
        # Convert uint to signed int
        z_out = z_out / 4
        z_out = z_out * 16
        z_out = z_out.astype(np.short)
        z_out = z_out / 16
        return z_out.astype(np.complex)

    @staticmethod
    def get_ppa_mimo_raw(radar_path, sleep=False):
        # Process FRme
        if not os.path.exists(radar_path):
            print("get_ppa_mimo_raw: not exists", radar_path)
            return

        Rframe = WiseSDK.processFrame(radar_path, 0, WiseSDK.DATA_TYPE_E.RAW_DATA_TYPE)
        # SLeeping due to continues bug in cuda pipeling (ned time to allocate)
        if sleep:
            time.sleep(8)
        else:
            time.sleep(0.2)
        Rframe = WiseSDK.processFrame(radar_path, 0, WiseSDK.DATA_TYPE_E.RAW_DATA_TYPE)

        # Get the values
        arr = Rframe.getData()
        # Get dimension from header
        num_samples, num_rx, num_tx, num_pulses = find_data_size_properties(Rframe)
        axis1, axis2, axis3, axis4 = find_data_size_properties(Rframe)
        number_of_mimo_pulses_frame = WiseSDK.getRadarNumberOfMimoPulsesInPuzzle(Rframe.getHeader().waveform_id,
                                                                                 Rframe.getHeader().mode_id,
                                                                                 WiseSDK.AntennaVersion.ANTENNA_3_5,
                                                                                 Rframe.getHeader().rf_config_id,
                                                                                 )

        number_of_pulses_per_frame = WiseSDK.getPulsesNumber(Rframe.getHeader().waveform_id)
        if number_of_mimo_pulses_frame <= 0:
            mimo_raw = np.reshape(arr, (num_samples, num_rx, num_pulses * num_tx), order='F').real
            return mimo_raw, None

        # Calculate part of mimo from flatten array
        total_mimo = int(number_of_mimo_pulses_frame * num_samples * Rframe.getHeader().dims.size2)

        # print("total_mimo", total_mimo)

        # slice array and reshape
        mimo_raw = arr[:total_mimo]
        mimo_raw = np.reshape(mimo_raw, (num_samples, Rframe.getHeader().dims.size2, number_of_mimo_pulses_frame), order='F')

        # slice array and reshape
        ppa_raw = arr[total_mimo:]
        ppa_raw = np.reshape(ppa_raw, (num_samples, Rframe.getHeader().dims.size2, number_of_pulses_per_frame - number_of_mimo_pulses_frame), order='F')

        return mimo_raw, ppa_raw

    @staticmethod
    def get_ppa_mimo_raw_offline(radar_path, sleep=False):
        # Process FRme
        if not os.path.exists(radar_path):
            print("get_ppa_mimo_raw: not exists", radar_path)
            return

        arr = RawReader.get_raw_directly(radar_path, 0)
        # Rframe = WiseSDK.processFrame(radar_path, 0, WiseSDK.DATA_TYPE_E.RAW_DATA_TYPE)

        # Get the values
        # arr = Rframe.getData()

        # Get dimension from header
        num_samples, num_rx, num_tx, num_pulses = find_data_size_properties(Rframe)
        axis1, axis2, axis3, axis4 = find_data_size_properties(Rframe)
        number_of_mimo_pulses_frame = WiseSDK.getRadarNumberOfMimoPulsesInPuzzle(Rframe.getHeader().waveform_id,
                                                                                 Rframe.getHeader().mode_id,
                                                                                 WiseSDK.AntennaVersion.ANTENNA_3_5,
                                                                                 Rframe.getHeader().rf_config_id,
                                                                                 )

        number_of_pulses_per_frame = WiseSDK.getPulsesNumber(Rframe.getHeader().waveform_id)
        if number_of_mimo_pulses_frame <= 0:
            mimo_raw = np.reshape(arr, (num_samples, num_rx, num_pulses * num_tx), order='F').real
            return mimo_raw, None

        # Calculate part of mimo from flatten array
        total_mimo = int(number_of_mimo_pulses_frame * num_samples * Rframe.getHeader().dims.size2)

        # print("total_mimo", total_mimo)

        # slice array and reshape
        mimo_raw = arr[:total_mimo]
        mimo_raw = np.reshape(mimo_raw, (num_samples, Rframe.getHeader().dims.size2, number_of_mimo_pulses_frame),
                              order='F')

        # slice array and reshape
        ppa_raw = arr[total_mimo:]
        ppa_raw = np.reshape(ppa_raw, (
        num_samples, Rframe.getHeader().dims.size2, number_of_pulses_per_frame - number_of_mimo_pulses_frame),
                             order='F')

        return mimo_raw, ppa_raw

    def run(self, df, context, write=False):
        self.df = df
        for idx in tqdm(range(1, len(self.df))):
            # Getting Path of raw File
            radar_path = context.radarDataRaw_path + str(self.df.iloc[idx]["filenameRadar"])
            if not os.path.exists(radar_path):
                print("not exists", radar_path)
                continue

            sleep = True if idx == 1 else False
            mimo_raw, ppa_raw = self.get_ppa_mimo_raw(radar_path, sleep=sleep)

            # print("mimo", mimo_raw.shape, "ppa", ppa_raw.shape, ppa_raw.dtype)
            if write:
                # print("Writing!")
                if mimo_raw is not None:
                    os.makedirs(context.mimo_raw_save_path, exist_ok=True)
                    # print(ppa_raw.itemsize*ppa_raw.size)

                    np.save(context.mimo_raw_save_path + "/mimo_raw_" + str(self.df.iloc[idx]["timestampRadar"]), mimo_raw[:, 0, :])
                    # print("Saved!")
                # else:
                #     os.makedirs(context.ppa_raw_save_path, exist_ok=True)
                # np.save(context.mimo_raw_save_path + "/mimo_raw_" + str(self.df.iloc[idx]["timestampRadar"]), mimo_raw)
                # np.save(context.ppa_raw_save_path + "/ppa_raw_" + str(self.df.iloc[idx]["timestampRadar"]), ppa_raw)
        return




# r = RawReader()
# r.run()
#
# WiseSDK.modulesDestroy()
