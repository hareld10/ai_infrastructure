import pathlib
import os
import glob
from pathlib import Path
from tqdm import tqdm
import pandas as pd
import numpy as np
from DataPipeline.shared_utils import read_label
from wiseTypes.Types import EnvelopeTypes

module_path = str(pathlib.Path(__file__).parent.absolute())


def pc_envelope_decorator(func):
    def inner1(*args, **kwargs):
        pc_df = func(*args, **kwargs)
        envelope = args[0].drive_context.envelope
        if not envelope[EnvelopeTypes.ENVELOPE_ACTIVE.name]:
            return pc_df

        if type(pc_df) == str:
            return pc_df
        pc_df = pc_df[(pc_df["r"] >= envelope[EnvelopeTypes.RNG_MIN.name]) & (pc_df["r"] <= envelope[EnvelopeTypes.RNG_MAX.name])]
        pc_df = pc_df[(pc_df["Az"] >= envelope[EnvelopeTypes.AZ_STEER_MIN_DEG.name]) & (pc_df["Az"] <= envelope[EnvelopeTypes.AZ_STEER_MAX_DEG.name])]
        return pc_df.reset_index(drop=True)

    return inner1


def get_el_rgb_indices(engine):
    envelope = engine.drive_context.envelope
    if not envelope[EnvelopeTypes.ENVELOPE_ACTIVE.name]:
        return None, None
    el_max = int(engine.drive_context.camera_obj.Radar2Image(r=[200], az=[0], el=[envelope[EnvelopeTypes.EL_STEER_MIN_DEG.name]], D=engine.drive_context.D)[1])
    el_min = int(engine.drive_context.camera_obj.Radar2Image(r=[200], az=[0], el=[envelope[EnvelopeTypes.EL_STEER_MAX_DEG.name]], D=engine.drive_context.D)[1])
    return el_min, el_max

def rgb_envelope_decorator(func):
    def inner1(*args, **kwargs):
        rgb_im = func(*args, **kwargs)
        envelope = args[0].drive_context.envelope
        if not envelope[EnvelopeTypes.ENVELOPE_ACTIVE.name]:
            return rgb_im
        self = args[0]
        el_min, el_max = get_el_rgb_indices(engine=self)

        # el_max = int(self.drive_context.camera_obj.Radar2Image(r=[200], az=[0], el=[envelope[EnvelopeTypes.EL_STEER_MIN_DEG.name]], D=self.drive_context.D)[1])
        # el_min = int(self.drive_context.camera_obj.Radar2Image(r=[200], az=[0], el=[envelope[EnvelopeTypes.EL_STEER_MAX_DEG.name]], D=self.drive_context.D)[1])
        # az_min = int(self.drive_context.camera_obj.Radar2Image(r=[200], el=[0], az=[envelope[EnvelopeTypes.AZ_STEER_MIN_DEG.name]], D=self.drive_context.D)[0])
        # az_max = int(self.drive_context.camera_obj.Radar2Image(r=[200], el=[0], az=[envelope[EnvelopeTypes.AZ_STEER_MAX_DEG.name]], D=self.drive_context.D)[0])
        # new_img = np.zeros_like(rgb_im)
        # new_img[el_min:el_max, az_min:az_max] = rgb_im[el_min:el_max, az_min:az_max]

        new_img = np.zeros_like(rgb_im)
        new_img[el_min:el_max, :] = rgb_im[el_min:el_max, :]
        return new_img

    return inner1
