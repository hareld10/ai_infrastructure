import json
from copy import copy, deepcopy
from math import sqrt

import matplotlib
import torch
from random import random
import torch.utils.data as data
import numpy as np
import cv2
import glob
from matplotlib import pyplot as plt
import imutils
from tqdm import tqdm
from collections import defaultdict
import os
from skimage.draw import random_shapes

font = {'weight' : 'bold',
        'size'   : 22}

matplotlib.rc('font', **font)

class Images:
    def __init__(self):
        src = ["/mnt/hdd/200623/"]
        bank = []
        for i in src:
            bank += glob.glob(i + "*/rgbCamera/*.png")
        self.bank = np.random.permutation(bank)
        print("len_bank", len(bank))
        self.idx = 0

        return

    def get_ensemble_img(self):
        while True:
            self.idx += 1
            path = self.bank[self.idx]

            ts = os.path.split(path)[-1].split(".")[0].split("_")[-1]
            label_path = os.path.split(path)[0] + "/../camera_det_ensemble/camera_det_ensemble_" + ts + ".json"
            if os.path.exists(label_path):
                im = cv2.imread(path)
                with open(label_path) as label_json:
                    frame_data = json.load(label_json)

                    # cond
                    flag1, flag2 = False, False
                    for elem in frame_data:
                        if elem["cat"] == "person" or elem["cat"] == "pedestrian":
                            flag1 = True
                        if elem["cat"] == "motorcycle":
                            flag2 = True
                    if not (flag1 and flag2):
                        # print("NO person and motorcycle")
                        continue

            else:
                print("label don't exists")
                continue

            yield im, frame_data

    def get_seg_image(self):
        while True:
            self.idx += 1
            path = self.bank[self.idx]

            ts = os.path.split(path)[-1].split(".")[0].split("_")[-1]
            label_path = os.path.split(path)[0] + "/../camera_road_seg/camera_road_seg_" + ts + ".npz"
            if os.path.exists(label_path):
                im = cv2.cvtColor(cv2.imread(path), cv2.COLOR_BGR2RGB)
                label = np.load(label_path)["arr_0"]
                label = cv2.resize(label, (im.shape[1], im.shape[0]))
            else:
                print("label don't exists")
                continue

            yield im, label


class LabelChecker:
    def __init__(self):
        self.images_loader = Images()
        self.colors = {"car": (0, 0, 255), "traffic light": (255, 0, 0), "truck": (135, 206, 250),
                       "bus": (100, 149, 237),
                       "pedestrian": (255, 165, 0), "person": (255, 165, 0), "bicycle": (170, 0, 255),
                       "motorcycle": (216, 0, 155), "rider":(0,255,0)}
        self.other_colors = LabelChecker.gen_colors(109)
        self.output = "//home/amper/AI/Harel/Results/"
        return

    @staticmethod
    def gen_colors(n):
        ret = []
        r = int(random() * 256)
        g = int(random() * 256)
        b = int(random() * 256)
        step = 256 / n
        for i in range(n):
            r += step
            g += step
            b += step
            r = int(r) % 256
            g = int(g) % 256
            b = int(b) % 256
            ret.append((r, g, b))
        return ret

    def get_iou(self, a, b, epsilon=1e-5):
        """ Given two boxes `a` and `b` defined as a list of four numbers:
                [x1,y1,x2,y2]
            where:
                x1,y1 represent the upper left corner
                x2,y2 represent the lower right corner
            It returns the Intersect of Union score for these two boxes.

        Args:
            a:          (list of 4 numbers) [x1,y1,x2,y2]
            b:          (list of 4 numbers) [x1,y1,x2,y2]
            epsilon:    (float) Small value to prevent division by zero

        Returns:
            (float) The Intersect of Union score.
        """
        # COORDINATES OF THE INTERSECTION BOX
        x1 = max(a[0], b[0])
        y1 = max(a[1], b[1])
        x2 = min(a[2], b[2])
        y2 = min(a[3], b[3])

        # AREA OF OVERLAP - Area where the boxes intersect
        width = (x2 - x1)
        height = (y2 - y1)
        # handle case where there is NO overlap
        if (width < 0) or (height < 0):
            return 0.0
        area_overlap = width * height

        # COMBINED AREA
        area_a = (a[2] - a[0]) * (a[3] - a[1])
        area_b = (b[2] - b[0]) * (b[3] - b[1])
        area_combined = area_a + area_b - area_overlap

        # RATIO OF AREA OF OVERLAP OVER COMBINED AREA
        iou = area_overlap / (area_combined + epsilon)
        return iou

    def visualize(self, im, label):
        for cur_label in label:
            obj = cur_label['cat']
            cat_id = cur_label['cat_id']
            score = cur_label['score']
            x1, y1, x2, y2 = cur_label['bbox']
            if obj in self.colors:
                color = self.colors[obj]
            else:
                color = (0, 255, 0)
            cv2.rectangle(im, (x1, y1), (x2, y2), color, 2)
            cv2.putText(im, '{}, {:.3f}'.format(obj, score),
                        (x1, y1 + 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                        color, 1)

        return im

    def filter_intersection(self, label, iou_thresh):
        m_cycles = [x for x in label if x["cat"] == "motorcycle"]
        if len(m_cycles) == 0:
            return label

        num_riders = 0
        for idx, x in enumerate(label):
            max_iou = 0
            if x["cat"] == "person" or x["cat"] == "pedestrian":
                for motor in m_cycles:
                    max_iou = max(max_iou, self.get_iou(motor["bbox"], x["bbox"]))
            if max_iou > iou_thresh:
                label[idx]["cat"] = "rider"
                num_riders+=1
        return label, num_riders

    def run(self, num=1):
        for i in range(num):
            while True:
                plot = False
                im, label = next(self.images_loader.get_ensemble_img())
                for idx, t in enumerate([0.1, 0.15, 0.2, 1]):
                    filtered_label, num_removed = self.filter_intersection(deepcopy(label), iou_thresh=t)
                    if num_removed != 0:
                        plot = True
                        break
                if plot:
                    print("plotting")
                    plt.figure(figsize=(40, 30))
                    for idx, t in enumerate([0.1, 0.15, 0.2, 1]):
                        filtered_label, num_removed = self.filter_intersection(deepcopy(label), iou_thresh=t)
                        ret_im = self.visualize(im[..., ::-1].copy(), filtered_label)
                        plt.subplot(2, 2, idx + 1)
                        plt.title("Thresh:" + str(t) + " removed:" + str(num_removed))
                        plt.imshow(ret_im)
                    plt.tight_layout()
                    plt.savefig(self.output + "/" + str(i) + ".png")
                    plt.close()
                    break
                else:
                    print("Didnt remove")
            # cv2.imwrite(self.output + "/" + str(i) + ".png", ret_im)


a = LabelChecker()
a.run(100)
