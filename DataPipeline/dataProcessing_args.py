import ast
import shutil
import sys, pathlib
import torch

module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, module_path)
sys.path.insert(0, "/workspace/AI/Harel/AI_Infrastructure/")
from LabelsManipulation.camera_lidar_fusion import run_camera_lidar_fusion_labeling
from LabelsManipulation.lidar_road import infer_lidar_road, infer_lidar_road_delimiter
from OpenSource.WisenseEngine import WisenseEngine
# from DNNs.camera_road_seg.inference_pipeline import infer_road_seg_from_dir
# from DNNs.camera_vehicle_segmentation.inference_pipeline import infer_veh_seg_from_dir
from DNNs.PyTorch_YOLOv3.inference_pipeline import infer_from_df
from DNNs.deeplab.inference_pipeline import deeplab_from_base_path
from ensamble_module import ensemble_from_dir
from shared_utils import load_json, rectify

import settings
from DNNs.EfficientDet.inference import infer_from_base_path
# from DNNs.EfficientDet.inference_new import ModelWrapper
# from WiseSDKAI import *
import numpy as np
import glob
import time
from ensamble import Ensemble, PedContext, VehicleContext, MotorcycleContext, all_ensemble_contexts
from validation import Validator
import cv2
import os
from argparse import Namespace
import pandas as pd
from tqdm import tqdm
import sys
from multiprocessing import Process
import codecs, json
import argparse
import time
from pathlib import Path
import sys
import pathlib
from syncer import Syncer
from settings import Context
from lidar_reader import LidarReader
import zipfile
from pathlib import Path
from arg_parser import parser
from raw_reader import RawReader
# from wise_sdk_runner import SDKRunner
import subprocess
import ensemble_3d
import multiprocessing
from concurrent.futures import ThreadPoolExecutor
from filterator import Filterator


class Pipeline:
    def __init__(self, args, src_index=0):
        self.gpu = 1
        self.args = args
        self.segWeights = 'weights/CRDv2.6.1.unet4_best_model_190919.pth'
        self.force = False
        self.radarOnly = False
        self.cameraOnly = False
        self.motorcycles_classes = {"motorcycle": True, "motorbike": True, "bicycle": True}
        self.df_data = None
        self.cam_export, self.df_radar, self.df_camera = None, None, None,
        self.df_lidar = None
        self.src_dir = None
        self.index = 0
        self.context = Context(args, index=src_index)
        self.lidar_reader = LidarReader(args=args, calibration_params=self.context.spatial_calibration_params)
        self.syncer = Syncer(self.args, self.context, )
        self.ensambler = Ensemble(contexts=all_ensemble_contexts)
        self.LABEL_NAMES = np.asarray([
            "road", "sidewalk", "building", "wall", "fence", "pole", "traffic-light", "traffic-sign", "vegetation",
            "terrain", "sky",
            "person", "rider", "car", "truck", "bus", "train", "motorcycle", "bicycle",

        ])

    def set_args(self, new_args):
        print("Got new args", new_args)
        self.args = new_args
        self.context = Context(new_args, index=0)

    def delete_unused(self):
        print("#dfSync ", len(self.df_data))
        saved_frames_radar, saved_frames_camera = {}, {}
        for idx in range(len(self.df_data)):
            ts = self.df_data.loc[idx, 'filenameRadar'].split(".")[0]
            raw_to_remove = self.context.radar_src + self.df_data.loc[idx, 'filenameRadar']
            saved_frames_radar[ts] = True

            ts = self.df_data.loc[idx, 'filenameCamera'].split(".")[0]
            raw_to_remove = self.context.camera_src + self.df_data.loc[idx, 'filenameCamera']
            saved_frames_camera[ts] = True

        def run_over(delete=False):
            keep_count = 0
            del_count = 0

            radar_files = glob.glob(self.context.radar_src + "/*/Radar*.raw")
            print("radar_src", self.context.radar_src, " len glob ", len(radar_files))
            for f in radar_files:
                t_s = os.path.split(f)[-1].split(".")[0]
                if t_s in saved_frames_radar:
                    keep_count += 1
                else:
                    if delete:
                        os.remove(f)
                    time.sleep(0.0001)
                    del_count += 1
            print("Radar: keep_count ", keep_count)
            print("Radar del_count ", del_count)

            keep_count = 0
            del_count = 0
            camera_files = glob.glob(self.context.camera_src + "/*/Camera*.raw")
            print("camera_src", self.context.camera_src, " len glob ", len(camera_files))
            for f in camera_files:
                t_s = os.path.split(f)[-1].split(".")[0]
                if t_s in saved_frames_camera:
                    keep_count += 1
                else:
                    if delete:
                        os.remove(f)
                    time.sleep(0.0001)
                    del_count += 1
            print("Camera keep_count ", keep_count)
            print("Camera del_count ", del_count)

        run_over(delete=False)
        val = input("Are you sure you want to delete?? Yes/No")
        if val == "Yes":
            print("You Choose to Delete (Unavailable)")
            run_over(delete=True)
        else:
            print("Not deleting!")

    def keep_synced(self, force_sync=False):
        # check if done before
        csvs = len(glob.glob(self.context.radarDataRaw_path + "/*.csv"))
        raws = len(glob.glob(self.context.radarDataRaw_path + "/*.raw"))

        if raws == csvs:
            print("It seems synced to frame/rate never done before. csvs", csvs, "raws", raws)
        else:
            print("It seems df_data is synced, csvs", csvs, "raws", raws, " force_sync", force_sync)

        if not force_sync:
            print("Not forcing sync to specific FPS")
            print("final df ", len(self.df_data))
            return True

        print("forced syncing")
        # keep synced with 5 fps:
        radar_fs = 25
        f_s = 6

        synced_len = len(self.df_data)
        desired_len = int(f_s * len(self.df_radar) // radar_fs)

        steps = 0
        print("synced_len ", synced_len)
        print("desired_len ", desired_len)
        for i in range(1, 10):
            x = (synced_len - synced_len // i) - desired_len
            if x > 0:
                steps = i
                break

        if steps != 0:
            new_df = self.df_data.loc[::steps]
            self.df_data = self.df_data.drop(new_df.index).reset_index()
        self.df_data.to_csv(self.context.df_path, sep=',', index=False)
        print("final df ", len(self.df_data))
        return True

    def order_lidar(self):
        print("Don't forget source /opt/ros/melodic/setup.bash")
        for i in glob.glob(self.context.lidar_src + "/*.bag"):
            print("Moving")
            destination = os.path.split(i)[-1]
            shutil.move(i, self.context.lidar_raw_data + destination)

        if os.path.exists(self.context.lidar_csv_path):
            lidar_df = pd.read_csv(self.context.lidar_csv_path, index_col=None)
            print("Reading dfLidar ", len(lidar_df))
            return lidar_df

        print("Creating lidar df - UnBagging", self.context.lidar_src)
        search_path = self.context.lidar_raw_data + "/*.bag"
        files = glob.glob(search_path)
        print("found %d files in %s" % (len(files), search_path))
        for bag_file in files:
            print("BagFile", bag_file)
            if "orig" in bag_file:
                continue
            if os.path.split(bag_file)[-1].split(".")[0].split("_")[-1].isdigit():
                index_bag = "rosbag reindex " + bag_file
                subprocess.call(index_bag, shell=True)
            run_cmd = "python /workspace/AI/Harel/lidar_collection/gen_df_from_bag.py " + bag_file + " " + self.context.aux_path
            subprocess.call(run_cmd, shell=True)

        dfs = []
        for df_per_bag in glob.glob(self.context.aux_path + "/dflidarBag*"):
            dfs.append(pd.read_csv(df_per_bag, index_col=None))
        df_lidar = pd.concat(dfs, ignore_index=True)
        df_lidar = df_lidar.sort_values(by='timeInt')
        df_lidar = df_lidar.reset_index(drop=True)
        df_lidar.to_csv(self.context.lidar_csv_path, sep=',', index=False)

        return df_lidar

    def unbag_lidar(self):
        print("UnBagging", self.context.lidar_src)
        #

        for bag_file in glob.glob(self.context.lidar_raw_data + "/*.bag"):
            print("BagFile", bag_file)
            if "orig" in bag_file:
                continue
            run_cmd = "python /workspace/AI/Harel/lidar_collection/read_bag_np.py " + bag_file + " " + self.context.lidar_points + " " + self.context.df_path
            subprocess.call(run_cmd, shell=True)
        return

    def check_if_do(self, dir_name, force_not_affecting=False):
        if not force_not_affecting:
            if self.args.samples or self.args.force:
                return True
        else:
            print("check_if_do: Force-Doesn't-Affect - ", dir_name)
            return False

        count = len(glob.glob(dir_name + "/*"))
        len_data = len(self.df_data)
        if (len_data - count) < len_data * 0.05:
            print("check_if_do: Skipping - ", dir_name, count, "len-df", len_data)
            return False
        else:
            print("check_if_do: Not-Skipping - ", dir_name, count, "len-df", len_data)
        return True

    def preprocess(self):
        if self.args.force:
            print("Force!")
            self.force = True

        #################################################
        # Init workspace paramteres
        # self.context = setWorkspace(self.radar_src, self.camera_src, self.destination_path, depthSrc=self.depth_src)

        if self.args.depth:
            print("Depth mode - no rgb will be opened")
            self.cam_export = False
            if not self.args.depthSrc:
                print("You must specify depthSrc in depth mode")
                exit(1)

        if self.args.lidar:
            self.df_lidar = self.order_lidar()
            print("len df_lidar", len(self.df_lidar))

        if not self.args.df:
            self.df_data = self.syncer.run(src=self.src_dir, df_lidar=self.df_lidar, context=self.context)
            self.df_radar, self.df_camera = self.syncer.df_radar, self.syncer.df_camera

            if self.radarOnly:
                print("Radar Only")
                self.df_data = self.syncer.df_radar
            elif self.args.cameraOnly:
                print("Camera Only")
                self.df_data = self.syncer.df_camera
            else:
                self.keep_synced(force_sync=self.args.force_sync)
                if self.args.delete_unused:
                    self.delete_unused()
                    print("Deleted and Exit")
                    exit()
                    pass

        else:
            self.df_data = pd.read_csv(self.context.destination_path + "/" + self.args.df, index_col=False)

        if self.args.samples:
            if "-" in self.args.samples:
                words = self.args.samples.split("-")
                start, end = int(words[0]), int(words[1])
                print("Sampling Sequence! ", start, "-", end)
                self.df_data = self.df_data[start:end].reset_index(drop=True)
            else:
                print("Sampling ", int(self.args.samples), " num samples, original len ", len(self.df_data))
                self.df_data = self.df_data.sample(int(self.args.samples)).reset_index(drop=True)

            self.context.df_path = self.context.sample_df_path
            self.df_data.to_csv(self.context.sample_df_path, sep=',', index=False)

        if self.args.video:
            words = self.args.video.split("-")
            start = int(words[0])
            end = int(words[1])
            print("Generating Video Mode! ", start, "-", end)
            self.df_data = self.df_data[start:end]
            self.df_data = self.df_data.reset_index(drop=True)

        print('Dataset length:', len(self.df_data))
        self.check_free_space()

    def check_free_space(self):
        amount_needed = 0
        if self.args.radar:
            amount_needed += 16
        return

    def update_filtered_data(self):
        print("Updating Filtered Data")
        df = self.df_data.copy()
        df = df.astype({'timestampCamera': 'str'})
        new_df = df.copy()
        new_df['ped_count'] = 0
        new_df['veh_count'] = 0
        new_df['ped_3d_count'] = 0
        new_df['veh_3d_count'] = 0
        new_df['ped_on_road_count'] = 0
        new_df['min_score_ped'] = 0
        new_df['min_score_veh'] = 0
        new_df['max_score_ped'] = 0
        new_df['max_score_veh'] = 0

        for l_name in self.LABEL_NAMES:
            new_df[l_name + "_dist"] = 0

        err_count = 0
        for idx in tqdm(range(len(df))):
            # Get label
            try:
                label_path = self.context.camera_det_ensemble + "/camera_det_ensemble_" + str(df.loc[idx, "timestampCamera"]) + ".json"
                seg_label_path = self.context.camera_seg_class + "/camera_seg_class_" + str(df.iloc[idx]["timestampCamera"]) + ".npz"
                label_3d = self.context.lidar_3d + "/lidar_3d_" + str(df.loc[idx, "timestampLidar"])[:-4] + ".json"

                label_3d = load_json(label_3d)
                label_2d = load_json(label_path)

                seg_label = np.load(seg_label_path)["arr_0"]
            except Exception as e:
                err_count += 1
                PrintException()
                continue

            # update_label_dist
            label_distribution = [0] * 19
            for k in range(19):
                try:
                    x = df.loc[idx, "label_distribution"][k]
                except:
                    cur_heatmap = seg_label == k
                    label_distribution[k] = np.sum(cur_heatmap) / (seg_label.shape[0] * seg_label.shape[1])
                    x = np.sum(cur_heatmap) / (seg_label.shape[0] * seg_label.shape[1])

                new_df.loc[idx, self.LABEL_NAMES[k] + "_dist"] = x

            """
            3D Field
            """

            bb_points = np.zeros((len(label_3d["pred_boxes"]), 9), dtype=np.float32)
            for idx_label in range(len(label_3d["pred_boxes"])):
                bb_points[idx_label, :7] = label_3d["pred_boxes"][idx_label]
                bb_points[idx_label, 7] = label_3d["pred_labels"][idx_label]
                bb_points[idx_label, 8] = label_3d["pred_scores"][idx_label]
            points_to_plot = self.lidar_reader.fix_lidar_mat(bb_points)
            new_df.loc[idx, 'ped_3d_count'] = points_to_plot[points_to_plot[:, 7] == 2].shape[0]
            new_df.loc[idx, 'veh_3d_count'] = points_to_plot[points_to_plot[:, 7] == 1].shape[0]

            """
            Update Filter Data in 2D Field
            """
            # Pedstrian and vehicle Count
            min_score_ped = 1
            min_score_veh = 1
            max_score_ped = 0
            max_score_veh = 0

            peds, vehs, peds_on_road = 0, 0, 0

            for bb in label_2d:
                if bb['cat'] == 'pedestrian' or bb['cat'] == 'person':
                    peds += 1
                    min_score_ped = min(bb['score'], min_score_ped)
                    max_score_ped = max(bb['score'], max_score_ped)

                if bb['cat'] == 'ped_on_road':
                    peds_on_road += 1

                if bb['cat'] == 'car' or bb['cat'] == 'truck' or bb['cat'] == 'motorcycle' or bb['cat'] == 'bus' or bb['cat'] == 'motorbike':
                    vehs += 1
                    min_score_veh = min(bb['score'], min_score_veh)
                    max_score_veh = max(bb['score'], max_score_veh)

            if vehs == 0:
                max_score_veh = 0
            if peds == 0:
                max_score_ped = 0

            new_df.loc[idx, 'ped_count'] = peds
            new_df.loc[idx, 'veh_count'] = vehs
            new_df.loc[idx, 'ped_on_road_count'] = peds_on_road
            new_df.loc[idx, "min_score_ped"] = min_score_ped
            new_df.loc[idx, "min_score_veh"] = min_score_veh
            new_df.loc[idx, "max_score_ped"] = max_score_ped
            new_df.loc[idx, "max_score_veh"] = max_score_veh

        new_df.to_csv(self.context.df_path, index=False, sep=',')
        print("Saved new filtered Data", self.context.df_path)
        return

    def cam_export(self, rgb_flag):
        WiseSDK_init()
        openCameraFramesByFolder(self.df_data, rgb_flag, self.args.depth, self.args.cameraPC, self.context)
        WiseSDK_kill()
        return

    def radar_export(self):
        WiseSDK_init()
        dsp(self.df_data, self.args.force, imaging_flag=self.args.radarPc, s_ant=self.args.radar, settings=self.context)
        WiseSDK_kill()
        return

    def radar_and_camera(self, rgb_flag=True):
        import WiseSDK
        from wise_sdk_runner import SDKRunner
        sdk = SDKRunner(context=self.context, debug=self.args.debug, init=True)

        if self.args.radar_raw:
            raw_reader = RawReader()
            raw_reader.run(df=self.df_data, context=self.context)
        if self.args.camExport and self.check_if_do(self.context.camera_rgb) and not self.args.skip_rgb:
            sdk.open_camera_frames_by_folder(self.df_data, rgb_flag, self.args.depth, self.args.cameraPC, self.context, flip=self.args.flip)

        if self.args.radar and self.check_if_do(self.context.radar_sant_save_path):
            if not self.args.skip_sant:
                dsp_config = sdk.set_config(config_name="010321_sant")
                sdk.dsp(self.df_data, self.args.force, s_ant=True)

        if self.args.phase_amp:
            print("\n################ CUDA Phase-Amp ####################\n")
            sdk.gen_internal_type(dfData=self.df_data, pc_type="radar4d_phase_amp", force=self.args.force)

        if self.args.processed_delta:  # and self.check_if_do(self.context.cuda_processed_delta, force_not_affecting=True):  # CUDA Delta
            print("\n################ CUDA Processed Delta + classification association ####################\n")
            sdk.set_config(config_name="2503021_delta")
            sdk.dsp(self.df_data, self.args.force, pc_type=WiseSDK.DATA_TYPE_E.PROCESSED_POINT_CLOUD_DATA_TYPE, s_ant=self.args.radar)
            self.args.associate = True

        if self.args.radarPc and self.check_if_do(self.context.cuda_delta):  # CUDA Delta
            print("\n################ CUDA Delta ####################\n")
            sdk.set_config(config_name="170121_delta")
            sdk.dsp(self.df_data, self.args.force, pc_type=WiseSDK.DATA_TYPE_E.POINT_CLOUD_DATA_TYPE, s_ant=self.args.radar)

        if self.args.radar4d and self.check_if_do(self.context.cuda_4dfft):
            print("\n################ CUDA 4D FFT ####################\n")
            sdk.set_config(config_name="170121_fft")
            sdk.dsp(self.df_data, self.args.force, pc_type=WiseSDK.DATA_TYPE_E.POINT_CLOUD_DATA_TYPE, s_ant=False)
            # x = sdk.generate_online(df=self.df_data, idx=0, data_type=WiseSDK.DATA_TYPE_E.POINT_CLOUD_DATA_TYPE)
            # print(x.shape)

        # if not self.args.samples and (self.args.radarPc and self.args.radar4d):
        #     print("\n################ Velocity Correction ####################\n")
        #     print("\n################ Delta Velocity Correction ####################\n")
        #     sdk.set_config(config_name="170121_delta")
        #     sdk.apply_velocity_correction(self.df_data)
        #
        #     print("\n################ 4DFFT Velocity Correction ####################\n")
        #     sdk.set_config(config_name="170121_fft")
        #     sdk.apply_velocity_correction(self.df_data)

        print("Sleeping Before Shutting Down")
        time.sleep(3)

        sdk.WiseSDK_kill()

    def after_process(self):
        # if self.args.val:
        #     print("\n################ Validation Phase ####################\n")
        #     validator = Validator(self.context, self.df_data)
        #     validator.run()
        #     if self.args.video:
        #         validator.make_video_from_dir()

        if self.args.filterator:
            Filterator.run(self.context, args=self.args, df=self.df_data)
            return

        if self.args.update_filter:
            print("\n################ Updating Filter Data ####################\n")
            self.update_filtered_data()

        # Clean empty dirs:
        for root, dirs, files in os.walk(self.context.destination_path, topdown=False):
            for name in dirs:
                d_path = os.path.join(root, name)
                if os.path.isdir(d_path) and len(os.listdir(d_path)) == 0:
                    print("removing ", d_path, " as it's empty")
                    os.rmdir(d_path)

    def ai_process(self):
        # if self.args.road_seg:
        #     print("Road Segmentation")
        #     p1 = Process(target=infer_road_seg_from_dir,
        #                  args=(self.destination_path, self.df_data, self.force, self.args.gpu,))
        #     p1.start()
        # infer_road_seg_from_dir(self.destination_path, self.df_data, self.force)
        # torch.cuda.empty_cache()

        if self.args.od and self.check_if_do(self.context.camera_det_EfficientDetD7):
            print("Object Detection D7")
            p2 = Process(target=infer_from_base_path, args=(self.context, self.df_data, self.force, self.args.gpu,))
            p2.start()
            p2.join()
            # infer_from_base_path(self.destination_path, self.df_data, self.force, gpu=self.args.gpu)
            # torch.cuda.empty_cache()

        # if self.args.veh_seg:
        #     print("Veh Segmentation")
        #     p3 = Process(target=infer_veh_seg_from_dir,
        #                  args=(self.destination_path, self.df_data, self.force, self.args.gpu,))
        #     p3.start()
        # infer_veh_seg_from_dir(self.destination_path, self.df_data, self.force)
        # torch.cuda.empty_cache()

        if self.args.odYolo and self.check_if_do(self.context.camera_det_yoloV3):
            print("Object Detection Yolo")
            p4 = Process(target=infer_from_df, args=(self.context, self.df_data, self.force, self.args.gpu,))
            p4.start()
            # infer_from_df(self.destination_path, self.df_data, self.force, gpu=self.args.gpu)
            # torch.cuda.empty_cache()
            p4.join()

        if self.args.seg and (self.check_if_do(self.context.camera_seg_class) or self.check_if_do(self.context.camera_seg_soft)):
            p5 = Process(target=deeplab_from_base_path,
                         args=(self.context, self.df_data, self.force, self.args.gpu,))
            p5.start()
            p5.join()

        # if args.pc3d:
        #     pass

        if self.args.ensemble_2d and self.check_if_do(self.context.camera_det_ensemble):
            print("Run ensamble Object Detection")
            self.ensambler.ensemble_from_dir(path_context=self.context, df=self.df_data, force=self.force)
            # ensemble_from_dir(self.destination_path, self.df_data, self.force)

        if self.args.ensemble_3d:
            ensemble_3d.run(df=self.df_data, path_context=self.context, visualize=50, save=True)
            # self.ensambler.ensemble_3d_from_dir(path_context=self.context, df=self.df_data, force=self.force)

    def engines_tasks(self):
        engine = WisenseEngine(base_name="Wisense", base_path_drive=self.context.destination_path, df=self.df_data, rect_mode=True)
        if self.args.lidar_road:
            infer_lidar_road(engine=engine, force=self.args.force)
        if self.args.lidar_road_del:
            infer_lidar_road_delimiter(engine=engine, force=self.args.force)
        if self.args.clf:
            run_camera_lidar_fusion_labeling(engine=engine, force=self.args.force)
            if not self.args.samples:
                associate_and_gen_instance_df(engine)
        else:
            if self.args.associate:
                associate_and_gen_instance_df(engine)
        if self.args.val:
            engine = WisenseEngine(base_name="Wisense", base_path_drive=self.context.destination_path, df=self.df_data, rect_mode=True)

    def rectify_rgb(self):
        print("\n################ Rectify RGB ####################\n")
        if not self.check_if_do(self.context.camera_rgb_rectified_original, force_not_affecting=True):
            return

        for idx in tqdm(range(len(self.df_data))):
            rgbImPath = self.context.camera_rgb + "/camera_rgb_" + str(self.df_data.iloc[idx]["timestampCamera"]) + ".png"
            img = cv2.imread(rgbImPath)
            rec = rectify(img, K=self.context.K, D=self.context.D)
            rgbImPath = self.context.camera_rgb_rectified + "/camera_rgb_" + str(self.df_data.iloc[idx]["timestampCamera"]) + ".png"
            cv2.imwrite(rgbImPath, rec)
        return

    def run(self, i):
        np.random.seed()
        self.index = i
        self.context = Context(args=self.args, index=i)
        # os.system("source /opt/ros/melodic/setup.bash")
        print("Working On: ", self.context.src_dir)

        self.preprocess()

        if self.args.csv_only:
            Filterator.run(self.context, args=self.args, df=self.df_data)
            print("EXITING JUST GENERATING SYNC DATA")
            exit()

        #################################################

        if self.args.skip:
            print("Skipping radar-camera-lidar")

        # extract_lidar:
        if self.args.lidar and not self.args.skip and not self.args.skip_lidar:
            if self.check_if_do(self.context.lidar_points):
                self.unbag_lidar()

        if (self.args.camExport or self.args.radar or self.args.radarPc or self.args.processed_delta) and not self.args.skip:
            cam_and_radar_process = Process(target=self.radar_and_camera)
            cam_and_radar_process.start()
            cam_and_radar_process.join()
            cam_and_radar_process.terminate()

            # if self.args.flip:
            #     for filename in tqdm(Path(self.context.camera_rgb).rglob('*.png')):
            #         filename = str(filename)
            #         rgb_img = cv2.imread(filename)
            #         rgb_img = np.fliplr(np.flipud(rgb_img))
            #         cv2.imwrite(filename, rgb_img)

        # todo bring it back
        self.ai_process()
        #self.rectify_rgb()  # Before Paths change
        # self.context.set_rect_paths(True)
        # self.ai_process()
        # self.context.set_rect_paths(False)

        self.engines_tasks()

        self.after_process()
        print('------------------------------------------------------------------------')
        print('Done')


if __name__ == "__main__":
    # print("\n\n\nstart\n\n\n")
    # args = parser.parse_args(namespace=eval(str(sys.argv[1])))
    # os.system("source /opt/ros/melodic/setup.bash")
    # args = eval(str(sys.argv[1]))
    # print("in docker args", args)
    args = parser.parse_args()
    # print(args)
    torch.multiprocessing.set_start_method('spawn')
    # dataPipeline = Pipeline(args)

    if True:
        srcs = pd.read_csv("/workspace/AI/Harel/AI_Infrastructure/DataPipeline/src/datasets_index.csv", index_col=False)
        if args.rev:
            srcs = srcs[::-1].reset_index(drop=True)
        args.src = [srcs.loc[src_idx]["dataset"] for src_idx in range(len(srcs))]

        for src_idx in range(len(srcs)):
            drive = srcs.loc[src_idx]["dataset"]
            print(drive)
            if pd.isna(drive):
                continue
            if str(int(srcs.loc[src_idx]["radar_wf"])) != args.wf:
                # print(str(int(srcs.loc[src_idx]["radar_wf"])), "!=", args.wf)
                continue
            if args.single:
                found = False
                print(args.single)
                for word in args.single.split():
                    if word in drive:
                        found = True
                        break
                if not found:
                    continue
                # and not args.single in drive:
                # print("Specified Drive ", args.single, "Not in src", drive)
                # continue
            # print("\n\n\n\nDrive: ", drive, "\n\n\n")
            args.lidarSrc = "/workspace/" + str(srcs.loc[src_idx]["lidarRawData_amper"]) + "/"
            args.radarSrc = "/workspace/" + str(srcs.loc[src_idx]["radarRawData_amper"]) + "/"
            args.cameraSrc = "/workspace/" + str(srcs.loc[src_idx]["cameraRawData_amper"]) + "/"
            args.dest = "/workspace/" + str(srcs.loc[src_idx]["amper_disc"]) + "/"
            print("camera flip =", str(srcs.loc[src_idx]["camera_flip"]))
            args.flip = bool(float(str(srcs.loc[src_idx]["camera_flip"])))
            print("args.radarSrc=", args.radarSrc)
            dataPipeline = Pipeline(args, src_index=src_idx)
            # dataPipeline.set_args(args)

            # print(args)
            p = Process(target=dataPipeline.run, args=(src_idx,))
            p.start()
            p.join()

    # Generate All zed
    elif args.all_zed:
        srcs = pd.read_csv("/workspace/AI/Harel/zed_collection/datasets_index.csv", index_col=False)
        args.src = [srcs.loc[src_idx]["drive"] for src_idx in range(len(srcs))]
        print(args.src)
        for src_idx in range(len(srcs)):
            drive = srcs.loc[src_idx]["drive"]
            print("Drive: ", drive)
            args.depthSrc = "/workspace/" + srcs.loc[src_idx]["depthRawData"] + "/64mWF/"
            args.radarSrc = "/workspace/" + srcs.loc[src_idx]["radarRawData"] + "/64mWF/"
            args.cameraSrc = "/workspace/" + srcs.loc[src_idx]["cameraRawData"] + "/64mWF/"

            p = Process(target=dataPipeline.run, args=(src_idx,))
            p.start()
            p.join()

    else:
        print("args.src", args.src)
        for run_idx in range(len(args.src)):
            if args.delete_unused:
                dataPipeline.run(run_idx)
            else:
                p = Process(target=dataPipeline.run, args=(run_idx,))
                p.start()
                p.join()
