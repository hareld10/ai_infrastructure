import cv2
import pandas as pd
from tqdm import tqdm
import sys
from multiprocessing import Process
import codecs, json
import argparse
import time
from pathlib import Path
import sys
import pathlib
import numpy as np
import glob
import ast
import shutil
import os
import datetime

from shared_utils import toRAzEl, lidar_to_radar_coordinates, apply_6DOF


class LidarReader:
    LIDAR_FOV = 80

    def __init__(self, args, calibration_params=None):
        self.args = args
        self.calibration_params = calibration_params
        if self.calibration_params is None:
            fileName = "/workspace/AI/Harel/AI_Infrastructure/DataPipeline/configs/calibration_params_lidar_radar_200909"
            with open(fileName, "r") as fp:
                self.calibration_params = json.load(fp)

    def filter_fov_az(self, pts, az):
        if az == 0:
            return pts
        pts = pts[pts[:, 1] > 0]
        r, Az, El = toRAzEl(x=pts[:, 1], y=pts[:, 0], z=pts[:, 2])
        mask = np.logical_not(np.logical_and(Az < (az // 2), (-az // 2) < Az))
        return pts[mask, :]

    def fix_lidar_mat(self, pts):
        pts[:, :3] = lidar_to_radar_coordinates(pts[:, :3])
        pts[:, :3], rot = apply_6DOF(pts[:, :3], yaw=self.calibration_params["yaw-radar-lidar"],
                                     roll=self.calibration_params["roll-radar-lidar"],
                                     pitch=self.calibration_params["pitch-radar-lidar"],
                                     tx=self.calibration_params["tx-radar-lidar"],
                                     ty=self.calibration_params["ty-radar-lidar"],
                                     tz=self.calibration_params["tz-radar-lidar"])

        pts = self.filter_fov_az(pts, LidarReader.LIDAR_FOV)
        return pts
