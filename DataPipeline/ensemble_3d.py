import shutil

import numpy as np
import os

from matplotlib import gridspec, lines
from tqdm import tqdm
import glob
import sys
import json
import os
import sys
import time
import datetime
import argparse
import matplotlib.pyplot as plt
import pandas as pd
from tqdm import tqdm
from PIL import Image
import cv2

from Calibrator.calibrator_utils import make_video_from_dir

plt.style.use('dark_background')
import torch
import matplotlib

from DataPipeline.settings import Context
from DataPipeline.shared_utils import read_label, PrintException, filter_fov, toRAzEl, get_iou, non_max_suppression_fast

scatter_size = 1
point_rcnn_thresh = 0.9
pvrcnn_thresh = 0.9
iou_thresh = 0.5
nm_thresh = 0.5
score_idx = 8


def get_bbs_array(lidar_3dvd_label):
    lidar_3dvd_label_proj = np.zeros((len(lidar_3dvd_label["pred_boxes"]), 9), dtype=np.float32)  # (cx, cy, cz, dx, dy, dz, heading, category, score)
    for idx_label in range(len(lidar_3dvd_label["pred_boxes"])):
        lidar_3dvd_label_proj[idx_label, :7] = lidar_3dvd_label["pred_boxes"][idx_label]
        lidar_3dvd_label_proj[idx_label, 7] = lidar_3dvd_label["pred_labels"][idx_label]
        lidar_3dvd_label_proj[idx_label, 8] = lidar_3dvd_label["pred_scores"][idx_label]
    return lidar_3dvd_label_proj


def filter_range(min_range, max_range, pts, index=3):
    x = pts[np.linalg.norm(pts[:, :index], axis=-1) < max_range]
    x = x[np.linalg.norm(x[:, :index], axis=-1) > min_range]
    return x


def filter_fov_az(pts, az, x_idx=0, y_idx=1):
    if az == 0:
        return pts
    pts = pts[pts[:, y_idx] > 0]
    r, Az, El = toRAzEl(x=pts[:, x_idx], y=pts[:, y_idx], z=pts[:, 2])
    mask = np.logical_and(Az < (az // 2), (-az // 2) < Az)
    return pts[mask, :]


def lidar_to_radar_coordinates(points, ang=90):
    theta = np.deg2rad(ang)
    rotation_matrix = np.array([[np.cos(theta), -np.sin(theta), 0],
                                [np.sin(theta), np.cos(theta), 0],
                                [0, 0, 1]])

    return np.matmul(rotation_matrix, points.T).T


def get_bb_bev(bb):
    cx, cy = bb[:2]
    dx, dy = bb[3:5]
    heading = bb[6]
    c = np.cos(heading)
    s = np.sin(heading)
    rot_mat = [[c, -s], [s, c]]

    center = [cx, cy]
    p1 = center + np.dot(rot_mat, [dy / 2, dx / 2])
    p2 = center + np.dot(rot_mat, [dy / 2, -dx / 2])
    p3 = center + np.dot(rot_mat, [-dy / 2, -dx / 2])
    p4 = center + np.dot(rot_mat, [-dy / 2, dx / 2])

    heading_1 = center + np.dot(rot_mat, [dy / 2, dx / 2])
    heading_2 = center + np.dot(rot_mat, [-dy / 2, dx / 2])

    return p1, p2, p3, p4, heading_1, heading_2


def plot_bb_bev(ax, lidar_3dvd_label_proj):
    for bb in lidar_3dvd_label_proj:
        cx, cy = bb[:2]
        l, w, h = bb[3:6]

        p1, p2, p3, p4, heading_1, heading_2 = get_bb_bev(bb)

        pts = [p1, p2, p3, p4]

        pairs = [[0, 1], [1, 2], [2, 3], [3, 0]]
        for ind in pairs:
            plt.plot([pts[ind[0]][0], pts[ind[1]][0]], [pts[ind[0]][1], pts[ind[1]][1]], c='w')

        # Plot heading
        l1 = lines.Line2D((cx, p1[0]), (cy, p1[1]), color='w', linewidth=2)
        ax.add_line(l1)
        l1 = lines.Line2D((cx, p4[0]), (cy, p4[1]), color='w', linewidth=2)
        ax.add_line(l1)

        # Plot score
        ax.annotate(str(np.round(bb[8], 2)), (np.max([p1[0], p2[0], p3[0], p4[0]]), np.max([p1[1], p2[1], p3[1], p4[1]])), color='w', fontsize=12)


def add_2d_cordinate(pts):
    ret = np.zeros((pts.shape[0], pts.shape[1] + 4), dtype=np.float32)
    for idx, elem in enumerate(pts):
        ret[idx, :9] = elem
        p1_1, p2_1, p3_1, p4_1, heading_1, heading_1 = get_bb_bev(elem)
        points_array = np.vstack([p1_1, p2_1, p3_1, p4_1])
        ret[idx, 9:] = np.min(points_array[:, 0]), np.min(points_array[:, 1]), np.max(points_array[:, 0]), np.max(points_array[:, 1])
    return ret


def run(df, path_context, visualize=50, vid="", save=True):
    if not vid:
        base_path_ensembles_images = path_context.lidar_3d_ensemble_images
    else:
        base_path_ensembles_images = vid

    def range_fix(pts):
        pts[:, score_idx] = pts[:, score_idx] + np.linalg.norm(pts[:, :3], axis=-1)/280
        return pts
    for idx in tqdm(range(len(df))):
        try:
            rgb_path = path_context.camera_rgb + "/camera_rgb_" + str(df.iloc[idx]["timestampCamera"]) + ".png"
            im = cv2.cvtColor(cv2.imread(rgb_path), cv2.COLOR_BGR2RGB)
            lidar_1 = read_label(path_context.lidar_3d + "/lidar_3d_" + str(df.loc[idx, "timestampLidar"])[:-4] + ".json")
            lidar_1 = get_bbs_array(lidar_1)
            lidar_1[:, :3] = lidar_to_radar_coordinates(lidar_1[:, :3])
            lidar_1 = filter_fov_az(lidar_1, 100, x_idx=0, y_idx=1)
            lidar_1 = add_2d_cordinate(lidar_1)
            lidar_1 = lidar_1[lidar_1[:, 7] == 1]
            lidar_1 = lidar_1[lidar_1[:, score_idx] > 0.25]
            lidar_1 = range_fix(lidar_1)

            lidar_2 = read_label(path_context.lidar_3d_pointrcnn + "/lidar_3d_pointrcnn_" + str(df.loc[idx, "timestampLidar"])[:-4] + ".json")
            lidar_2 = get_bbs_array(lidar_2)
            lidar_2[:, :3] = lidar_to_radar_coordinates(lidar_2[:, :3])
            lidar_2 = filter_fov_az(lidar_2, 100, x_idx=0, y_idx=1)
            lidar_2 = add_2d_cordinate(lidar_2)
            lidar_2 = lidar_2[lidar_2[:, 7] == 1]
            lidar_2 = lidar_2[lidar_2[:, score_idx] > 0.25]
            lidar_2 = range_fix(lidar_2)

            lidar_pc_path = path_context.lidar_points + "/lidar_pc_" + str(df.loc[idx, 'timestampLidar'])[:-4] + ".npz"
            lidar_pc = np.load(lidar_pc_path)['arr_0']  # ndarray (x,y,z)
            lidar_pc[:, :3] = lidar_to_radar_coordinates(lidar_pc[:, :3])

            lidar_pc = filter_fov_az(lidar_pc, 80, x_idx=0, y_idx=1)
            lidar_pc = filter_range(0, 100, lidar_pc)

        except Exception as e:
            PrintException()
            continue

        finals = []

        for l_1_idx, l_1 in enumerate(lidar_1):
            if l_1[score_idx] >= pvrcnn_thresh:
                finals.append(l_1)

        for l_2_idx, l_2 in enumerate(lidar_2):
            if l_2[score_idx] >= point_rcnn_thresh:
                finals.append(l_2)

        for l_1_idx, l_1 in enumerate(lidar_1):
            for l_2_idx, l_2 in enumerate(lidar_2):
                b1 = l_1[9:]
                b2 = l_2[9:]

                iou = get_iou(b1, b2)
                if iou > iou_thresh:
                    add_flag = True
                    for new_bb in finals:
                        if np.array_equal(new_bb, l_1):
                            add_flag = False
                    if add_flag:
                        finals.append(l_1)

        finals = np.array(finals)
        finals = np.array(non_max_suppression_fast(finals, nm_thresh))

        if idx % visualize == 0:
            plt.close()
            gs = gridspec.GridSpec(ncols=2, nrows=2)
            fig = plt.figure(figsize=(20, 10))

            im_ax = fig.add_subplot(gs[0, 0])
            im_ax.imshow(im)
            im_ax.set_title("Reference RGB")

            lidar_ax = fig.add_subplot(gs[0, 1])
            lidar_ax.set_title("Ensemble")
            lidar_ax.set_xlim(-30, 30)
            lidar_ax.set_ylim(0, 80)
            scat = lidar_ax.scatter(lidar_pc[:, 0], lidar_pc[:, 1], c=lidar_pc[:, 2], cmap='jet', s=scatter_size)  # plot point cloud
            plot_bb_bev(ax=lidar_ax, lidar_3dvd_label_proj=finals)
            plt.colorbar(scat, ax=lidar_ax, fraction=0.026, pad=0.08, orientation='horizontal')

            lidar_ax1 = fig.add_subplot(gs[1, 0])
            lidar_ax1.set_title("PV-RCNN")
            lidar_ax1.set_xlim(-30, 30)
            lidar_ax1.set_ylim(0, 80)
            plot_bb_bev(ax=lidar_ax1, lidar_3dvd_label_proj=lidar_1)
            scat = lidar_ax1.scatter(lidar_pc[:, 0], lidar_pc[:, 1], c=lidar_pc[:, 2], cmap='jet', s=scatter_size)  # plot point cloud
            plt.colorbar(scat, ax=lidar_ax1, fraction=0.026, pad=0.08, orientation='horizontal')

            lidar_ax2 = fig.add_subplot(gs[1, 1])
            lidar_ax2.set_title("Point-RCNN")
            lidar_ax2.set_xlim(-30, 30)
            lidar_ax2.set_ylim(0, 80)
            plot_bb_bev(ax=lidar_ax2, lidar_3dvd_label_proj=lidar_2)
            scat = lidar_ax2.scatter(lidar_pc[:, 0], lidar_pc[:, 1], c=lidar_pc[:, 2], cmap='jet', s=scatter_size)  # plot point cloud
            plt.colorbar(scat, ax=lidar_ax2, fraction=0.026, pad=0.08, orientation='horizontal')

            plt.tight_layout()
            plt.savefig(base_path_ensembles_images + str(df.iloc[idx]["timestampRadar"]) + ".png")
            plt.close()

        if save:
            if finals.shape[0] == 0:
                finals = np.zeros((0, 9), dtype=np.float32)
            finals[:, :3] = lidar_to_radar_coordinates(finals[:, :3], ang=-90)
            np.savez(path_context.lidar_3d_ensemble + "/lidar_3d_ensemble_" + str(df.loc[idx, "timestampLidar"])[:-4], finals[:, :9])


if __name__ == "__main__":
    paths = ["200903_lidar_drive_1_W92_M15_RF1", "200831_lidar_drive_1_urban_W92_M15_RF1", "200902_lidar_drive_highway_W92_M15_RF1", ]
    paths = [paths[2]]
    # starts = [3000, 5000, 6000, 7000]
    # starts = [1250, 2050, 2550, 2900]
    starts = [1300, 1500, 1800, 1150]
    for p in paths:
        for start in starts:
            end = start + 200
            base_path = "/media/amper/data_backup7/76mWF/" + p
            VIDEO_DIR = "/home/amper/AI/Harel/example_images/" + p + "/"
            if os.path.exists(VIDEO_DIR):
                shutil.rmtree(VIDEO_DIR)
            os.makedirs(VIDEO_DIR, exist_ok=True)

            _path_context = Context(args=None, index=0, create_dirs=False, base_path=base_path)
            _df = pd.read_csv(_path_context.df_path)
            _df = _df[start:end].reset_index(drop=True)
            run(_df, path_context=_path_context, visualize=1, vid=VIDEO_DIR, save=False)
            make_video_from_dir(src_dir=VIDEO_DIR, out_dir=VIDEO_DIR + "../", video_name=p + "_" + str(start) + "_" + str(end))
