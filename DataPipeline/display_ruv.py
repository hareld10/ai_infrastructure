import linecache
import os
import pickle
import sys
import traceback

import cv2
import numpy as np
from matplotlib.figure import Figure
from tqdm import tqdm
import pandas as pd
from pprint import pprint
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from matplotlib import gridspec, pyplot

from shared_utils import db


def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    tb = traceback.format_exc()
    print(tb)
    print('EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj))


class DisplayRUV:
    def __init__(self):
        self.df = None
        # self.base = "/mnt/hdd/new_lidar_drives/200903_lidar_drive_3_W92_M15_RF1/"
        # self.base = "/mnt/hdd/new_lidar_drives/200902_lidar_drive_2_W92_M15_RF1/"
        # self.base = "/mnt/hdd/new_lidar_drives/200902_lidar_cali_front_W92_M15_RF1/"
        # self.base = "/mnt/hdd/new_lidar_drives/200902_lidar_cali_left_W92_M15_RF1//"
        self.base = "/mnt/hdd/new_lidar_drives/200902_lidar_drive_highway_W92_M15_RF1//"
        # self.base = "/mnt/hdd/new_lidar_drives/200901_cali_front_W92_M15_RF1"
        # self.base = "/mnt/hdd/new_lidar_drives/200902_single_car_train_W92_M15_RF1//"
        self.df = pd.DataFrame()
        self.df = pd.read_csv(self.base + "/sample_df.csv", index_col=None)
        self.df = self.df.sample(20).reset_index(drop=True)
        # self.df = self.df.reset_index(drop=True)
        self.df['base_path'] = self.base
        self.bins = {}
        self.radar_param, self.im, self.pc_lidar, self.pc_radar, self.rd = None, None, None, None, None
        return

    def load_radar_param(self, idx):
        with open(str(self.df.loc[idx, 'base_path']) + "/radar_param_point_cloud.pkl", 'rb') as f:
            self.radar_param = pickle.load(f)

        keys = ["range", "az", "el", "doppler"]
        # min max bins
        for k in keys:
            self.bins[k] = (self.radar_param[k][1] - self.radar_param[k][0]) / self.radar_param[k][2]

        if idx == 0:
            pprint(self.radar_param)
            print("bins", self.bins)

    def get_frame(self, idx):
        rgb_path = str(self.df.loc[idx, 'base_path']) + "/rgbCamera/rgbCamera_" + str(self.df.loc[idx, 'timestampCamera']) + ".png"
        pc_path = str(self.df.loc[idx, 'base_path']) + "/lidar_pc/lidar_pc_" + str(self.df.loc[idx, 'timestampLidar'])[:-4] + ".npz"
        S_ANT_path = str(self.df.loc[idx, 'base_path']) + "/S_ANT/S_ANT_" + str(self.df.loc[idx, "timestampRadar"]) + ".npy"
        radar_pc_path = str(self.df.loc[idx, 'base_path']) + "/pcRadar/pc_" + str(self.df.loc[idx, "timestampRadar"]) + ".csv"

        # self.im = cv2.imread(rgb_path)[..., ::-1]
        # print("self.im.shape", self.im.shape)
        # self.pc_lidar = np.load(pc_path)['arr_0']
        # self.pc_radar = np.load(radar_pc_path).T
        # self.pc_radar = pd.read_csv(radar_pc_path)
        # self.rd = np.load(S_ANT_path)

        self.load_radar_param(idx)

    def draw_by_thresholds(self):
        return

    def inverse_db(self, arr):
        return (arr / 10) ** 10

    def db(self, array):
        return 10 * np.log10(array)

    def get_range_az(self, pc_radar):
        rdaz = np.zeros((self.radar_param["doppler"][2],
                         self.radar_param["range"][2],
                         self.radar_param["az"][2],
                         self.radar_param["el"][2]), dtype=np.complex)

        range_bin = (self.radar_param["range"][1] - self.radar_param["range"][0]) / (self.radar_param["range"][2]-1)
        u_bin = (self.radar_param["az"][1] - self.radar_param["az"][0]) / (self.radar_param["az"][2]-1)
        v_bin = (self.radar_param["el"][1] - self.radar_param["el"][0]) / (self.radar_param["el"][2]-1)
        dop_bin = (self.radar_param["doppler"][1] - self.radar_param["doppler"][0]) / (self.radar_param["doppler"][2]-1)

        dop_indices = np.round((pc_radar["dop"].values - self.radar_param["doppler"][0]) / dop_bin).astype(np.int)
        range_indices = np.round(((pc_radar["r"].values - self.radar_param["range"][0]) / range_bin)).astype(np.int)
        az_indices = np.round(((pc_radar["u_sin"].values - self.radar_param["az"][0]) / u_bin)).astype(np.int)
        el_indices = np.round(((pc_radar["v_sin"].values - self.radar_param["el"][0]) / v_bin)).astype(np.int)

        # value -> real
        # power -> img
        rdaz[dop_indices, range_indices, az_indices, el_indices] = db(np.abs(pc_radar["value"].values*pc_radar["value"].values + pc_radar["power"].values*pc_radar["power"].values))
        rng_az = np.max(np.max(self.db(np.abs(rdaz)), axis=0), axis=-1)
        rng_az = np.nan_to_num(rng_az, nan=0.0, neginf=0)
        return rng_az

    def run(self):

        for idx in tqdm(range(0, len(self.df))):
            try:
                self.get_frame(idx)
                # print(self.pc_radar.shape)

                plt_count = 2
                for plt_idx, coarse in enumerate(range(10, 20, 2)):
                    radar_pc_path = str(self.df.loc[idx, 'base_path']) + "/pcRadar/pc_" + str(self.df.loc[idx, "timestampRadar"]) + "_coarse_cfar_" + str(coarse) + ".csv"
                    if os.path.exists(radar_pc_path):
                        plt_count += 1

                gs = gridspec.GridSpec(ncols=plt_count, nrows=1, width_ratios=[5] + [1] * (plt_count - 1))
                fig = plt.figure(figsize=(20, 10))
                ax = fig.add_subplot(gs[0])
                rgb_path = str(self.df.loc[idx, 'base_path']) + "/rgbCamera/rgbCamera_" + str(self.df.loc[idx, 'timestampCamera']) + ".png"
                s
                ax.imshow(im)

                plt_idx = 1
                for coarse in range(10, 20, 2):
                    radar_pc_path = str(self.df.loc[idx, 'base_path']) + "/pcRadar/pc_" + str(self.df.loc[idx, "timestampRadar"]) + "_coarse_cfar_" + str(coarse) + ".csv"
                    if not os.path.exists(radar_pc_path):
                        print("Doesnt exists", radar_pc_path)
                        continue

                    # print("Exists!", coarse)
                    ax = fig.add_subplot(gs[plt_idx])
                    plt_idx += 1
                    pc_radar = pd.read_csv(radar_pc_path)
                    rng_az = self.get_range_az(pc_radar)
                    rng_az = cv2.resize(rng_az, None, fx=4, fy=1)
                    ax.imshow(rng_az, cmap="jet")
                    ax.locator_params(axis='y', nbins=20, tight=True)
                    ax.locator_params(axis='x', nbins=5, tight=True)
                    ax.invert_yaxis()

                    reduce_in_axis_x = 2
                    reduce_in_axis_y = 2
                    x_ticks = np.roll(np.rad2deg(np.arcsin(np.linspace(self.radar_param["az"][0], self.radar_param["az"][1], len(ax.get_xticklabels()) - reduce_in_axis_x))).astype(np.int), 1)
                    ax.xaxis.set_ticklabels(x_ticks)

                    y_ticks = np.roll(np.linspace(self.radar_param["range"][0], self.radar_param["range"][1], len(ax.get_yticklabels()) - reduce_in_axis_y).astype(np.int), 1)
                    ax.yaxis.set_ticklabels(y_ticks)

                    ax.set_title("Co: " + str(coarse) + " " + str(len(pc_radar)))
                # plt.show()
                # plt.tight_layout()
                plt.suptitle("ts: " + str(self.df.loc[idx, "timestampRadar"]) + " f_name: " + str(self.df.loc[idx, "filenameRadar"]))
                plt.savefig("/home/amper/AI/Harel/example_images/" + str(self.df.loc[idx, "filenameRadar"]) + ".png")
                plt.close()
                # print("rdaz", rdaz.shape)
                # return
            except Exception as e:
                print(e)
                PrintException()
                continue


d = DisplayRUV()
d.run()
