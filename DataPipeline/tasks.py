import os
import signal
import subprocess
import time

import pandas as pd
from psutil import process_iter
from signal import SIGTERM  # or SIGKILL
from DataPipeline.pipeline4_utils import HOME, PrintException
from DataPipeline.runner import resume_docker, run_docker_command
from Serving.serving import Wrapper


class Task:
    os.environ['CUDA_VISIBLE_DEVICES'] = f"{0},{1}"
    pcdet_client = Wrapper(port=90)

    def __init__(self, engine):
        self.engine = engine
        self.wrapper = None
        self.process = None
        self.plot_every = 200
        self.kwargs = {}

    def set_kwargs(self, kwargs):
        self.kwargs = kwargs

    def set_idx_status(self, idx, status, every=100):
        self.engine.df.loc[idx, self["name"]] = status
        if idx == len(self.engine)-1 or idx % every == 0:
            self.save_drive_df()

    def set_task_status(self, status):
        self.engine.df.loc[:, self["name"]] = status
        print(status, self.engine.df)
        self.save_drive_df()

    def save_drive_df(self):
        print("Saved to ", self["df_path"])
        self.engine.df.reset_index(drop=True).to_csv(self["df_path"])

    def __getitem__(self, item):
        if item in self.kwargs:
            return self.kwargs[item]
        print("item", item, "not in kwargs")

    def __enter__(self):
        print("Task=", self.__class__.__name__)
        return self

    def run(self):
        pass

    def __exit__(self, type=None, value=None, traceback=None):
        if self.process is not None:
            pid = self.process.pid
            self.process.kill()
            # self.process.terminate()
            print("\n\npid\n\n", pid)
            os.kill(pid, signal.SIGINT)
        PrintException()
        return True


class WiseSDKTask(Task):
    wise_sdk_client = Wrapper(port=1030)

    def __init__(self, engine):
        super(WiseSDKTask, self).__init__(engine)
        os.environ['CUDA_VISIBLE_DEVICES'] = f"{0},{1}"

    def __enter__(self):
        print("initing WiseSDK service")
        script_path = f"{HOME}/workspace/wise_sdk/AI/serving.py"
        try:
            self.__exit__()
            command_str = f"python3 {script_path} --port {self.wise_sdk_client.port} --gpu 0"
            print(command_str)
            WiseSDKTask.wise_sdk_process = subprocess.Popen(command_str, cwd=f"{HOME}/workspace/wise_sdk/", shell=True)
            print("Loaded WiseSDKTask.wise_sdk_process.pid", WiseSDKTask.wise_sdk_process.pid)
            time.sleep(10)
        except OSError as e:
            pass
        return self

    def __exit__(self, type=None, value=None, traceback=None):
        print("Trying to close WiseSDK")
        try:
            time.sleep(0.5)
            print("sending shutdown")
            WiseSDKTask.wise_sdk_client.send(service="shutdown", packet={"": ""})
            pid = WiseSDKTask.wise_sdk_client.send({"": ""}, service="get_pid")["pid"]
            print("GOT PID", pid)
            os.kill(pid, signal.SIGKILL)
            time.sleep(7)
        except Exception as e:
            print("close error", e)


class DockerTask(Task):
    def __init__(self, engine, port, docker_name, script_cmd):
        super(DockerTask, self).__init__(engine)
        self.port = port
        self.docker_name = docker_name
        self.script_cmd = script_cmd
        self.client = Wrapper(port=self.port)
        self.wait = 30

    def __enter__(self):
        # if not check_running_docker_name('yolor'):
        resume_docker(self.docker_name, command="start")
        run_docker_command(self.docker_name, exec_cmd=self.script_cmd)
        time.sleep(self.wait)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        super(DockerTask, self).__exit__()
        resume_docker(self.docker_name, command="kill")
