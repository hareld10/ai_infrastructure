import os
import pathlib

import numpy as np
import pandas as pd
from easydict import EasyDict as edict

from DataPipeline.shared_utils import read_label
from OpenSource.AudiEngine import AudiEngine
from OpenSource.WaymoEngine import WaymoEngine
from OpenSource.WisenseEngine import WisenseEngine

module_path = str(pathlib.Path(__file__).parent.absolute())


def get_index(src="datasets_index.csv", path_only=False):
    srcs_path = module_path + "/" + src
    if path_only:
        return srcs_path
    srcs = pd.read_csv(srcs_path, index_col=0)
    return srcs


def get_engines(src="datasets_index.csv", wf="92", val_train="train", drive_str=None,
                rect=True, randomize=True, envelope_active=False, df=None, **kwargs):
    if wf == "open_source" or drive_str is None or "audi" in drive_str.lower() or "waymo" in drive_str.lower():
        if drive_str is not None:
            if drive_str.lower() == "audi":
                audi_engine = AudiEngine(base_path="/workspace/HDD/open_source/audi/", val=True, force_obj=False)
                return [audi_engine]
            if drive_str.lower() == "waymo":
                waymo_engine = WaymoEngine(base_path="/media/amper/wiseData11/waymo/", val=True, force_obj=False)
                return [waymo_engine]
        elif type(src) == list:
            oss = []
            for elem_idx, elem in enumerate(src):
                if type(val_train) == list:
                    val_train_flag = False if val_train[elem_idx] == "train" else True
                else:
                    val_train_flag = False if val_train == "train" else True

                if elem.lower() == "audi":
                    audi_engine = AudiEngine(base_path="/workspace/HDD/open_source/audi/", val=val_train_flag, force_obj=False)
                    oss.append(audi_engine)
                if elem.lower() == "waymo":
                    waymo_engine = WaymoEngine(base_path="/media/amper/wiseData11/waymo/", val=val_train_flag, force_obj=False)
                    oss.append(waymo_engine)
            return oss

    srcs_path = module_path + "/" + src
    srcs = pd.read_csv(srcs_path).reset_index(drop=True)
    engines = []

    c_name_path = "/workspace/computer_name.json"
    if not os.path.exists(c_name_path):
        computer_context = edict({
            "dest": "amper_disc",
            "cameraRawData": "cameraRawData_amper",
            "radarRawData": "radarRawData_amper",
            "lidarRawData": "lidarRawData_amper",
            "gpsRawData": "gpsRawData_amper"
        })
        print("get_engines: Assuming Amper")
    else:
        computer_context = edict(read_label("/workspace/computer_name.json"))

    # print(computer_context, computer_context.dest)
    for src_idx in range(len(srcs)):
        drive = srcs.loc[src_idx]["dataset"]
        if pd.isna(drive):
            continue
        # print(drive, "desired wf", wf)
        if drive_str is not None and drive_str not in drive:
            continue
        if drive_str is not None and drive_str in drive:
            pass
        else:
            if val_train != "all" and str(srcs.loc[src_idx]["val_train"]) != val_train:
                # print("val_train", str(srcs.loc[src_idx]["val_train"]))
                continue
            if str(int(srcs.loc[src_idx]["radar_wf"])) != str(wf):
                # print("radar_wf", str(int(srcs.loc[src_idx]["radar_wf"])))
                continue
            if pd.isna(srcs.loc[src_idx][computer_context.dest]):
                print("dest is empty")
                continue

        dest = str(srcs.loc[src_idx]["suffix"]) + "//" + str(srcs.loc[src_idx][computer_context.dest])
        base_path = dest + "/" + drive
        cur_engine = WisenseEngine(base_path_drive=base_path, base_name="Wisense", rect_mode=rect, envelope_active=envelope_active, df=df)
        lidar_src = str(srcs.loc[src_idx]["suffix"]) + str(srcs.loc[src_idx][computer_context.lidarRawData]) + "/"
        radar_src = str(srcs.loc[src_idx]["suffix"]) + str(srcs.loc[src_idx][computer_context.radarRawData]) + "/"
        camera_src = str(srcs.loc[src_idx]["suffix"]) + str(srcs.loc[src_idx][computer_context.cameraRawData]) + "/"
        gps_src = str(srcs.loc[src_idx]["suffix"]) + str(srcs.loc[src_idx][computer_context.gpsRawData]) + "/"
        if cur_engine.df is not None:
            cur_engine.df["radar_src"] = radar_src + drive + "/radarRawData/"
        cur_engine.src_df = srcs.loc[src_idx].copy()
        cur_engine.drive_context.set_directly(drive_name=drive, lidar_src=lidar_src, camera_src=camera_src, radar_src=radar_src, depth_src="", dest=dest, gps_src=gps_src)
        cur_engine.srcs_path = srcs_path
        cur_engine.drive_context.set_rect_paths(rect)
        engines.append(cur_engine)
    if randomize:
        engines = np.random.permutation(engines)

    if int(wf) >= 198:
        [x.set_wise4_keys() for x in engines]
    return engines


def get_from_ts(engines, ts):
    for engine in engines:
        idx = engine.get_idx_from_ts(ts=ts)
        if idx is None:
            continue
        return engine, idx
    return None
