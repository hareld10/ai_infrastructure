import shutil
from collections import defaultdict
from multiprocessing.sharedctypes import Value
from pathlib import Path
from settings import Context
import ctypes
import cv2
from tqdm import tqdm
from threading import Thread, Lock
import json
import glob
from importlib import reload
import pickle
import matplotlib
import pandas as pd
import os
import time
import numpy as np
from pprint import pprint
import sys, pathlib
from shared_utils import load_json, db
module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, module_path)
from DataPipeline.shared_utils import pickle_read, pickle_write
import WiseSDK
import subprocess
import signal
from multiprocessing import Process
from WiseSDKAI import getFrameFormats, updateProgressBar, save_obj, load_obj, PrintException
from copy import deepcopy

dummy = lambda: None


def handler(signum, frame):
    print("Forever is over!")
    raise Exception("end of time")


class SDKRunner:
    def __init__(self, context, debug=False, init=True):
        self.context = context
        self.img_name_map = {0: "cuda_delta", 2: "cuda_4dfft"}
        self.velocity_tracker = pd.DataFrame(columns=['vx_delta_cuda', 'vy_delta_cuda', 'vz_delta_cuda', 'v_delta_cuda', 'vx_delta', 'vy_delta', 'vz_delta', 'v_delta', 'timestamp'])
        self.vel_est_threshold = 1
        # if init:
        #     self.WiseSDK_init()
        self.server_id = deepcopy(WiseSDK.SERVER_ID_E.SERVER_ID_INTERNAL)
        WiseSDK.setDSPCalibration("%s/configs/cal.json" % module_path, self.server_id)
        self.current_configuration = deepcopy(WiseSDK.DSPConfiguration().dict())
        print("\n\n\n current configuration")
        pprint(self.current_configuration)

        self.open_by_range = False
        self.add_amp_phase = True
        self.debug = debug
        self.num_consecutive_errors = 20
        self.first_frame_sleep = 10
        # self.skipped_indices_pkl = self.context.aux_path + "/skipped_indices.pkl"
        return

    def getRadarFrame(self, timestamp, dfSync):
        idx = dfSync.index[dfSync['timestampRadar'] == timestamp].values
        try:
            dfRow = dfSync.iloc[idx[0]]
        except Exception as e:
            print('index is out of boundaries!')

        radarFilename = dfRow.loc['filenameRadar']
        radarFrameIdx = ctypes.c_uint(int(dfRow.loc['inFileIdxRadar'])).value
        radarPath = str(self.context.radarDataRaw_path + radarFilename)

        print("Cur Path: ", radarPath, " idx ", radarFrameIdx, "size", Path(radarPath).stat().st_size)
        if not os.path.exists(radarPath):
            print("warning - frame path doesn't exist", radarPath)
            return None
            # print("Cur Path: ", radarPath, " idx ", radarFrameIdx, "size", Path(radarPath).stat().st_size)
        if self.debug:
            print(radarPath, radarFrameIdx)
        frame = WiseSDK.getRadarFrame(radarPath, radarFrameIdx)
        # print("mode", frame.getHeader().mode_id, "wf", frame.getHeader().waveform_id)
        if (frame.getHeader().dims.size1) == 0:
            print("Error! Frame was not found")

        return frame

    def open_camera_frames_by_folder(self, dfData, rgb_flag, depth_flag, pc_flag, context, flip):
        cameraFilenames = dfData.filenameCamera.unique()
        print("cameraFilenames", len(cameraFilenames))
        for cameraFilename in tqdm(cameraFilenames):
            timestamps = np.array(dfData.loc[dfData['filenameCamera'] == cameraFilename, 'timestampCamera']).astype(np.int64)
            indices = np.array(dfData.loc[dfData['filenameCamera'] == cameraFilename, 'inFileIdxCamera']).astype(np.int64)
            # print(str(settings.cameraSrc_path + settings.cameraDataRaw_path + str(cameraFilename)))
            os.makedirs(context.destination_path + "/rgbCamera/", exist_ok=True)
            status = WiseSDK.saveCameraFrame(
                str(context.cameraDataRaw_path + str(cameraFilename)), context.destination_path,
                indices, timestamps, getFrameFormats(rgb_flag, depth_flag, pc_flag), dummy)

            if flip:
                # print("flipping")
                for timestamp in timestamps:
                    filename = context.camera_rgb + "/camera_rgb_" + str(timestamp) + ".png"
                    rgb_img = cv2.imread(filename)
                    rgb_img = np.fliplr(np.flipud(rgb_img))
                    cv2.imwrite(filename, rgb_img)

        source_dir = context.destination_path + "/rgbCamera/"
        target_dir = context.camera_rgb
        file_names = os.listdir(source_dir)
        for file_name in file_names:
            try:
                shutil.move(os.path.join(source_dir, file_name), target_dir)
            except Exception as e:
                continue
            os.rename(os.path.join(target_dir, file_name), os.path.join(target_dir, file_name.replace("rgbCamera", "camera_rgb")))
        return

    def save_radar_param(self, frame, suffix=""):
        radar_param = {}
        ax = frame.getAxisParams()

        headers = ["range", "az", "el", "doppler"]
        for i, name in enumerate(headers):
            radar_param[name] = [ax[i].min, ax[i].max, ax[i].num_bins]

        pprint(radar_param)
        save_path = self.context.aux_path + "/radar_param_" + str(suffix) + ".pkl"

        save_obj(radar_param, save_path)
        d = load_obj(save_path)

        print("Radar Param: ", suffix, d)

        pc_metadata = frame.getMetadata()
        axis_params = pc_metadata.dict()['axis_params']
        print("Axis_params:")
        pprint(axis_params)
        return axis_params

    def update_inidces(self, pc, axis_params):
        # range, az, elevation. doppler
        range_bin = (axis_params[0][1] - axis_params[0][0]) / (axis_params[0][2] - 1)
        u_bin = (axis_params[1][1] - axis_params[1][0]) / (axis_params[1][2] - 1)
        v_bin = (axis_params[2][1] - axis_params[2][0]) / (axis_params[2][2] - 1)
        dop_bin = (axis_params[3][1] - axis_params[3][0]) / (axis_params[3][2] - 1)

        range_indices = np.round(((pc["r"].values - axis_params[0][0]) / range_bin)).astype(np.int)
        az_indices = np.round(((pc["u_sin"].values - axis_params[1][0]) / u_bin)).astype(np.int)
        el_indices = np.round(((pc["v_sin"].values - axis_params[2][0]) / v_bin)).astype(np.int)
        dop_indices = np.round((pc["dop"].values - axis_params[3][0]) / dop_bin).astype(np.int)

        pc["range_indices"] = range_indices
        pc["az_indices"] = az_indices
        pc["el_indices"] = el_indices
        pc["dop_indices"] = dop_indices

        if "robust_dop" in pc.keys():
            robust_dop_indices = np.round((pc["robust_dop"].values - axis_params[3][0]) / dop_bin).astype(np.int)
            pc["robust_dop_indices"] = robust_dop_indices

        if "uncorrected_dop" in pc.keys():
            uncorrected_dop_indices = np.round((pc["uncorrected_dop"].values - axis_params[3][0]) / dop_bin).astype(np.int)
            pc["uncorrected_dop_indices"] = uncorrected_dop_indices
        return pc

    @staticmethod
    def get_pc_from_frame(frame):
        header = frame.getHeader()
        data = frame.getData()
        num_of_fields = int(data.size / header.dims.size2)
        data_shape = [header.dims.size2, num_of_fields]
        pc = pd.DataFrame(data=np.reshape(data, data_shape),
                          columns=["x", "y", "z", "r", "u_sin", "v_sin", "dop", "uncorrected_dop", "value_real", "power_im", "noise",
                                   "cluster_id", "original_cluster_id", "x_global", "y_global", "trk_id", "vx", "vy", "vz", "trk_extrapolation_flag", "delta_peak_flag", "rcs"])

        # pc = pd.DataFrame(data=np.reshape(data, data_shape),
        #                   columns=["x", "y", "z", "r", "u_sin", "v_sin", "dop", "uncorrected_dop", "value_real", "power_im", "noise", "cluster_id", "x_global", "y_global", "trk_id", "vx", "vy",
        #                            "vz",
        #                            "trk_extrapolation_flag", "delta_peak_flag"])
        return pc

    def order_data(self, frame, radar_frame=None):
        if frame.getDataType() == WiseSDK.DATA_TYPE_E.POINT_CLOUD_DATA_TYPE or frame.getDataType() == WiseSDK.DATA_TYPE_E.PROCESSED_POINT_CLOUD_DATA_TYPE:
            pc = SDKRunner.get_pc_from_frame(frame)
            # header = frame.getHeader()
            # data = frame.getData()
            # num_of_fields = int(data.size / header.dims.size2)
            # data_shape = [header.dims.size2, num_of_fields]
            # pc = pd.DataFrame(data=np.reshape(data, data_shape),
            #                   columns=["x", "y", "z", "r", "u_sin", "v_sin", "dop", "uncorrected_dop", "value_real", "power_im", "noise", "cluster_id", "x_global", "y_global", "trk_id", "vx", "vy",
            #                            "vz",
            #                            "trk_extrapolation_flag", "delta_peak_flag"])

            # if self.add_amp_phase:
            #     print("Adding phase amp")
            #     self.current_configuration["ALGI"]["IMG_ALG_TYPE"] = 2
            #     WiseSDK.setDSPParameters(self.current_configuration, self.server_id)
            #     time.sleep(1)
            #
            #     print("Radar_frame", radar_frame)
            #     fft_pc_frame = WiseSDK.processFrame(radar_frame, WiseSDK.DATA_TYPE_E.POINT_CLOUD_DATA_TYPE)
            #     if fft_pc_frame.getHeader().dims.size1 > 0:
            #         fft_pc = SDKRunner.get_pc_from_frame()
            #         pc["fft_im"] = fft_pc["power_im"]
            #         pc["fft_real"] = fft_pc["value_real"]
            #         print("4D-FFT", len(fft_pc), "Delta", len(pc))
            #     else:
            #         print("4D FFT is None")
            #     self.current_configuration["ALGI"]["IMG_ALG_TYPE"] = 0
            #     WiseSDK.setDSPParameters(self.current_configuration, self.server_id)
            #     time.sleep(1)
            # if self.current_configuration["ALGI"]["IMG_ALG_TYPE"] == 2: # FFT
            #     pc["total_power"] = pc["value_real"].values ** 2 + pc["power_im"].values ** 2
            # else: # Delta
            #     pc["total_power"] = pc["value_real"]
            # pc["noise"] = pc["noise"]
            pc_metadata = frame.getMetadata()
            axis_params = pc_metadata.dict()['axis_params']

            pc = self.update_inidces(pc, axis_params)

            return pc

        elif frame.getDataType() == WiseSDK.DATA_TYPE_E.RANGE_DOPPLER_DATA_TYPE:
            header = frame.getHeader()
            data = np.reshape(frame.getData(),
                              (header.dims.size1, header.dims.size2 * header.dims.size3, header.dims.size4), order='F')
            return data
        else:
            return None

    def get_radar_save_path(self, data_type, frame_ts):
        if data_type == WiseSDK.DATA_TYPE_E.RANGE_DOPPLER_DATA_TYPE:
            return self.context.radar_sant_save_path + '/S_ANT_' + str(frame_ts)
        elif data_type == WiseSDK.DATA_TYPE_E.POINT_CLOUD_DATA_TYPE:
            if self.current_configuration["ALGI"]["IMG_ALG_TYPE"] == 2:
                return self.context.cuda_4dfft + '/cuda_4dfft_' + str(frame_ts) + ".csv"
            elif self.current_configuration["ALGI"]["IMG_ALG_TYPE"] == 0:
                return self.context.cuda_delta + '/cuda_delta_' + str(frame_ts) + ".csv"
        elif data_type == WiseSDK.DATA_TYPE_E.PROCESSED_POINT_CLOUD_DATA_TYPE:
            return self.context.cuda_processed_delta + "/cuda_processed_delta_" + str(frame_ts) + ".csv"
        elif data_type == "radar4d_phase_amp":
            return self.context.radar4d_phase_amp + "/radar4d_phase_amp_" + str(frame_ts) + ".csv"
        else:
            print("getRadarSavePath: Error - Couldn't resolve data type")
            return ""

    def save_radar_frame(self, frame, df, index, radar_frame=None, suffix=""):
        if index == 0:
            if frame.getDataType() == WiseSDK.DATA_TYPE_E.POINT_CLOUD_DATA_TYPE:
                self.save_radar_param(frame, suffix="point_cloud")
            if frame.getDataType() == WiseSDK.DATA_TYPE_E.RANGE_DOPPLER_DATA_TYPE:
                self.save_radar_param(frame, suffix="range_dop")

        frame_timestamp = frame.getHeader().timestamp

        if frame.getDataType() == WiseSDK.DATA_TYPE_E.POINT_CLOUD_DATA_TYPE or frame.getDataType() == WiseSDK.DATA_TYPE_E.PROCESSED_POINT_CLOUD_DATA_TYPE:
            PointCloud = self.order_data(frame, radar_frame)
            if suffix:
                frame_timestamp = str(frame_timestamp) + suffix
            frame_path = self.get_radar_save_path(frame.getDataType(), frame_timestamp)
            if self.current_configuration["ALGI"]["IMG_ALG_TYPE"] == 2:
                PointCloud = PointCloud.drop(columns=["cluster_id", "x_global", "y_global", "trk_id", "vx", "vy", "vz", "trk_extrapolation_flag"])

            # for x in ["range_indices", "el_indices", "az_indices"]:
            #     print(x, len(np.unique(PointCloud[x])))

            PointCloud.to_csv(frame_path, index=False)

        elif frame.getDataType() == WiseSDK.DATA_TYPE_E.RANGE_DOPPLER_DATA_TYPE:
            RD = self.order_data(frame)
            framePath = self.get_radar_save_path(frame.getDataType(), frame_timestamp)
            np.save(framePath, RD)
        else:
            print("saveRadarFrame: Error! data type " + str(frame.getDataType()) + " is not supported")

    def save_meta_data(self, point_cloud, ts):
        metadata = point_cloud.getMetadata().dict()

        ax = point_cloud.getAxisParams()
        headers = ["range", "az", "el", "doppler"]
        for i, name in enumerate(headers):
            metadata[name] = {}
            metadata[name]["min"] = ax[i].min
            metadata[name]["max"] = ax[i].max
            metadata[name]["num_bins"] = ax[i].num_bins
            metadata[name]["bin"] = (ax[i].max - ax[i].min) / (ax[i].num_bins - 1)

        with open(self.context.metadata + '/metadata_' + str(self.img_name_map[self.current_configuration["ALGI"]["IMG_ALG_TYPE"]]) + "_" + str(ts) + '.json', 'w') as fp:
            json.dump(metadata, fp, sort_keys=True, indent=2)
        return

    def set_config_dict(self, config_dict):
        # time.sleep(3)
        # self.WiseSDK_kill()
        # time.sleep(5)
        # self.WiseSDK_init()
        # time.sleep(3)

        if WiseSDK.setDSPParameters(config_dict, self.server_id) == -1:
            print("ERRORRRR in setting config")
            exit()
            return
        time.sleep(2)

    def set_config(self, config_name, set_config_dict=True):
        c_path = module_path + "/configs/" + config_name + ".json"
        with open(c_path, 'r') as fp:
            dsp_configuration = json.load(fp)

        if set_config_dict:
            self.set_config_dict(config_dict=dsp_configuration)
        if self.context is not None:
            try:
                shutil.copy(c_path, self.context.aux_path + "/config_" + os.path.split(c_path)[-1])
            except Exception as e:
                pass
        print("Setting WiseConfig:")
        pprint(dsp_configuration)

        self.current_configuration = dsp_configuration
        return dsp_configuration

    def kill_process(self, last_five, i, failed_indices):
        print("\n############ Too Many Consecutive Errors ##########\n")
        # Hard Kill
        self.WiseSDK_kill()
        time.sleep(2)
        batcmd = "ps -aux"
        result = subprocess.check_output(batcmd, shell=True)
        result = result.decode().split("\n")
        pids = []
        for line in result[1:]:
            if "/usr/bin/python3" in line:
                print("Line", line)
                line = line.split()
                if len(line) > 2:
                    pid = line[1]
                    print("PID", line[1])
                    print(line[1])
                    try:
                        pids.append(int(pid))
                    except Exception as e:
                        print(e)
        print("PIDS", pids, "Our PID", os.getpid())
        [os.kill(cur_pid, signal.SIGKILL) for cur_pid in pids]
        # self.set_config_dict(self.current_configuration)
        try:
            failed_indices.remove(i - self.num_consecutive_errors)
        except ValueError:
            pass
        return True

    def get_skipped_obj(self):
        if os.path.exists(self.skipped_indices_pkl):
            print("\n############ Reading Skipped Indices File ##########\n")
            return pickle_read(self.skipped_indices_pkl)

        skipped_dict = defaultdict(dict)
        return skipped_dict

    def save_skipped_obj(self, obj):
        print("Saving Skipped Obj\n", obj)
        pickle_write(file_path=self.skipped_indices_pkl, obj=obj)

    def gen_internal_type(self, dfData, pc_type, force=False):
        config_Start = {
            "ALGI": {
                "COARS_CFAR_THR_DB": 11.0,
                "FINE_CFAR_THR_DB": 6.0,
                "FINE_CFAR_LOCAL_THR_DB": 10.0,
                "IMG_ALG_TYPE": 0,
                "POSTFILT_MAPPER_FLAG": 0,
                "POSTFILT_STATIC_VEL_THR_MS": 0.3,
                "FINE_AZP_OVS_FACT": 10,
                "FINE_ELP_OVS_FACT": 2,
                "VEL_EST_DIM": 3,
                "VEL_EST_N_POINTS_MIN": 10,
                "VEL_EST_VMAX_MS": 30.0,
                "VEL_EST_VMIN_MS": -1.0,
                "VEL_TRCK_INIT_TIME_SEC": 0.9,
                "VEL_TRCK_ON": 1,
                "VEL_TRCK_RESTART_TIME_SEC": 5.0
            },
            "ALGM": {
                "AZ_STEER_MAX_DEG": 40.0,
                "AZ_STEER_MIN_DEG": -40.0,
                "EL_STEER_MAX_DEG": 10.0,
                "EL_STEER_MIN_DEG": -10.0,
                "RCM_FLAG": 0,
                "RNG_MAX": 76.0,
                "RNG_MIN": 3.0,
                "MAX_DETECTIONS": 20000
            },
            "ALGMPR": {
                "MAPPER_RESET_OCCUPANCY_MAP": 0,
                "MIN_CORRELATION": 0.4
            },
            "ALGTRCK": {
                "LOST_MT_TRK_AGE": 0.75,
                "LOST_STAT_TRK_AGE": 0.75,
                "VEL_MAX_MS": 50.0
            }
        }
        server_id = WiseSDK.SERVER_ID_E.SERVER_ID_INTERNAL

        for i in tqdm(range(len(dfData))):
            try:
                # radar_frame = self.getRadarFrame(dfData.loc[i, 'timestampRadar'], dfData)
                radarFilename = dfData.loc[i, 'filenameRadar']
                radarFrameIdx = ctypes.c_uint(int(dfData.loc[i, 'inFileIdxRadar'])).value
                radar_raw_path = str(self.context.radarDataRaw_path + radarFilename)
                if pc_type == "radar4d_phase_amp":
                    if os.path.exists(self.get_radar_save_path(pc_type, str(dfData.loc[i, 'timestampRadar']))) and not force:
                        print("skipping, frame exists")
                        continue

                    if not os.path.exists(radar_raw_path):
                        print("warning - radar raw doesnt exists", radar_raw_path)
                        continue

                    time.sleep(0.001)
                    config_Start["ALGI"]["IMG_ALG_TYPE"] = 0
                    status = WiseSDK.setDSPParameters(config_Start, server_id)
                    point_cloud = WiseSDK.processFrame(radar_raw_path, radarFrameIdx, WiseSDK.DATA_TYPE_E.PROCESSED_POINT_CLOUD_DATA_TYPE)
                    pc = SDKRunner.get_pc_from_frame(point_cloud)
                    frame_path = self.get_radar_save_path(pc_type, point_cloud.getHeader().timestamp)
                    frame_path = frame_path.replace(".csv", "_delta.csv")
                    pc.to_csv(frame_path)
                    print("1st=", pc.shape)

                    config_Start["ALGI"]["IMG_ALG_TYPE"] = 2
                    status = WiseSDK.setDSPParameters(config_Start, server_id)
                    radar_frame = self.getRadarFrame(dfData.loc[i, 'timestampRadar'], dfData)
                    frame = WiseSDK.processFrame(radar_raw_path, radarFrameIdx, WiseSDK.DATA_TYPE_E.POINT_CLOUD_DATA_TYPE)
                    pc2 = SDKRunner.get_pc_from_frame(frame)
                    print("2nd=", pc2.shape)
                    frame_path = self.get_radar_save_path(pc_type, point_cloud.getHeader().timestamp)
                    frame_path = frame_path.replace(".csv", "_fft.csv")
                    pc2.to_csv(frame_path)

                    # if point_cloud.getHeader().dims.size1 > 0:
                    #     pc_dataframe = self.order_data(point_cloud, radar_frame)
                    #     frame_path = self.get_radar_save_path(pc_type, point_cloud.getHeader().timestamp)
                    #     pc_dataframe = pc_dataframe.drop(columns=["cluster_id", "x_global", "y_global", "trk_id", "vx", "vy", "vz", "trk_extrapolation_flag",
                    #                                               'az_indices', 'el_indices', 'dop_indices', 'uncorrected_dop_indices', "x", "y", "z", 'delta_peak_flag', 'range_indices'])
                    #     pc_dataframe.to_csv(frame_path)
            except Exception as e:
                PrintException()
                continue

    def dsp(self, dfData, force=False, pc_type=False, s_ant=False):
        failed_indices = []

        for i in tqdm(range(len(dfData))):
            try:
                radar_frame = self.getRadarFrame(dfData.loc[i, 'timestampRadar'], dfData)

                if s_ant:
                    if radar_frame.getHeader().dims.size1 != 0:
                        processed_frame = WiseSDK.processFrame(radar_frame, WiseSDK.DATA_TYPE_E.RANGE_DOPPLER_DATA_TYPE)
                        if i == 0: time.sleep(8)

                        if processed_frame.getHeader().dims.size1 > 0:
                            self.save_radar_frame(processed_frame, dfData, i)

                if pc_type:
                    if os.path.exists(self.get_radar_save_path(pc_type, str(dfData.loc[i, 'timestampRadar']))) and not force:
                        print("skipping, frame exists")
                        continue

                    time.sleep(0.001)
                    point_cloud = WiseSDK.processFrame(radar_frame, pc_type)
                    if i == 0: time.sleep(8)

                    if point_cloud.getHeader().dims.size1 > 0:
                        self.save_radar_frame(point_cloud, dfData, i, radar_frame=radar_frame)
                        self.save_meta_data(point_cloud, ts=str(dfData.loc[i, 'timestampRadar']))
                    else:
                        print("timestamp", str(dfData.loc[i, 'timestampRadar']), "filename", str(dfData.loc[i, 'filenameRadar']))
                        failed_indices.append(i)
                        # print("Can't write metadata point_cloud.getHeader().dims.size1 <= 0")
                    # print("The above is idx", i, "ts-radar", str(dfData.loc[i, 'timestampRadar']))
            except Exception as e:
                PrintException()
                continue
        return failed_indices

    def generate_online(self, df, idx, data_type=WiseSDK.DATA_TYPE_E.PROCESSED_POINT_CLOUD_DATA_TYPE):
        if type(df) == str:
            radar_frame = WiseSDK.getRadarFrame(df, idx)
        else:
            radar_frame = self.getRadarFrame(df.loc[idx, 'timestampRadar'], df)
        if radar_frame is None:
            return None
        time.sleep(0.01)
        point_cloud = WiseSDK.processFrame(radar_frame, data_type)
        if idx == 0: time.sleep(8)
        if point_cloud.getHeader().dims.size1 > 0:
            processed_frame = self.order_data(point_cloud)
            time.sleep(0.01)
            return processed_frame
        else:
            return None

    def dsp_by_range(self, dfData, force=False, pc_type=False, s_ant=False):
        # Get Axes Params
        print("########### Saving Axes Params ###########")
        axis_params = None
        for i in range(10):
            radar_frame = self.getRadarFrame(dfData.loc[i, 'timestampRadar'], dfData)
            if pc_type:
                time.sleep(0.001)
                point_cloud = WiseSDK.processFrame(radar_frame, pc_type)

                if i == 0: time.sleep(self.first_frame_sleep)
                if point_cloud.getHeader().dims.size1 > 0:
                    axis_params = self.save_radar_param(frame=point_cloud, suffix=self.current_configuration["ALGI"]["IMG_ALG_TYPE"])
                    if axis_params is not None:
                        print("Axes Params Saved")
                        break
        if axis_params is None:
            print("\n Couldn't Get Radar Param! \n\nExiting!\n\n")
            exit(0)

        print("\n########### Getting Failed Indices ###########\n")
        failed_inidces = self.dsp(dfData=dfData, force=force, pc_type=pc_type, s_ant=False, by_range=False)
        print("\n########### There Are", len(failed_inidces), "Fails, Extracting By Range ###########\n")
        timestamps = np.array([dfData.loc[i, 'timestampRadar'] for i in failed_inidces])

        print(failed_inidces)
        print("There Are ", len(np.unique(timestamps)), "Unique timestamps", timestamps)
        rng = (self.current_configuration["ALGM"]["RNG_MAX"] - self.current_configuration["ALGM"]["RNG_MIN"]) * 0.3
        splits = [{"RNG_MIN": self.current_configuration["ALGM"]["RNG_MIN"], "RNG_MAX": self.current_configuration["ALGM"]["RNG_MIN"] + rng},
                  {"RNG_MIN": self.current_configuration["ALGM"]["RNG_MIN"] + rng, "RNG_MAX": self.current_configuration["ALGM"]["RNG_MAX"]}]

        for split in splits:
            print("\n########### Setting Range ###########\n")
            dsp_configuration = self.current_configuration
            dsp_configuration["ALGM"]["RNG_MIN"] = split["RNG_MIN"]
            dsp_configuration["ALGM"]["RNG_MAX"] = split["RNG_MAX"]
            dsp_configuration["ALGM"]["MAX_DETECTIONS"] = 15000
            self.set_config_dict(config_dict=dsp_configuration)

            print("Setting WiseConfig:")
            pprint(dsp_configuration)

            for i in tqdm(failed_inidces):
                try:
                    radar_frame = self.getRadarFrame(dfData.loc[i, 'timestampRadar'], dfData)
                    if pc_type:
                        if os.path.exists(self.get_radar_save_path(pc_type, str(dfData.loc[i, 'timestampRadar']))) and not force:
                            continue

                        time.sleep(0.001)
                        point_cloud = WiseSDK.processFrame(radar_frame, pc_type)
                        if i == failed_inidces[0]: time.sleep(self.first_frame_sleep)
                        if point_cloud.getHeader().dims.size1 > 0:
                            print("timestamp", str(dfData.loc[i, 'timestampRadar']), "filename", str(dfData.loc[i, 'filenameRadar']))
                            self.save_radar_frame(point_cloud, dfData, i, suffix="_by_range_" +
                                                                                 str(int(dsp_configuration["ALGM"]["RNG_MIN"])) + "_" +
                                                                                 str(int(dsp_configuration["ALGM"]["RNG_MAX"])))
                            self.save_meta_data(point_cloud, ts=str(dfData.loc[i, 'timestampRadar']))
                        else:
                            print("Can't write metadata point_cloud.getHeader().dims.size1 <= 0")
                        # print("The above is idx", i, "ts-radar", str(dfData.loc[i, 'timestampRadar']))
                except Exception as e:
                    PrintException()
                    continue

        print("\n########### Rebuilding Point Clouds ###########\n")

        def get_unique_vals(_pc):
            unique_vals = pd.unique(_pc)
            return unique_vals

        def fix_key_to_unique(dfs_to_fix, keys):
            # Iterate over each Key
            for key in keys:
                # print("Processing Key = ", key)
                total_unique = []
                # Getting Amount of unique values
                for pc in dfs_to_fix:
                    if key not in pc.keys():
                        # print("key is not in one of the dfs")
                        return pd.concat(dfs_to_fix, ignore_index=True)
                    unique_vals = get_unique_vals(pc[key])
                    print(key, "Unique values", unique_vals)
                    total_unique.append(len(unique_vals))

                # print("total_unique", total_unique)
                for pc_idx, pc in enumerate(dfs_to_fix[1:]):
                    cur_uniqe_vals = get_unique_vals(pc[key])
                    for un in sorted(cur_uniqe_vals, reverse=True):
                        if un == -1:
                            continue
                        # print("Fixing", un, "to", un+total_unique[0])
                        pc.loc[pc[key] == un, key] = pc.loc[pc[key] == un, key] + total_unique[0]

            merged_pc_ = pd.concat(dfs_to_fix, ignore_index=True)
            for key in keys: print(key, pd.unique(merged_pc_[key]))

            return merged_pc_

        for i in tqdm(failed_inidces):
            frame_timestamp = str(dfData.loc[i, 'timestampRadar'])

            frame_path = self.get_radar_save_path(pc_type, frame_timestamp)
            print("current ts", str(dfData.loc[i, 'timestampRadar']), str(dfData.loc[i, 'filenameRadar']))
            dfs = []

            for x_file in glob.glob(frame_path[:-4] + "*"):
                if "by_range" in x_file: dfs.append(x_file)

            if len(dfs) == 0:
                print("Couldn't Find any matches in dfs")
                continue
            else:
                print("index", i, "Found", len(dfs), "Matched pc to concat", )

            pcs = [pd.read_csv(x) for x in dfs]

            # merged_pc = pd.concat(pcs, ignore_index=True)

            merged_pc = fix_key_to_unique(dfs_to_fix=pcs, keys=["cluster_id", "trk_id"])
            merged_pc = self.update_inidces(merged_pc, axis_params=axis_params)

            merged_pc.to_csv(frame_path, index=False)
            [os.remove(x) for x in dfs]

        self.set_config_dict(config_dict=self.current_configuration)
        return True

    def velocity_compensation(self, v_fold, pts):

        # Projection operator
        Ai = np.array([pts['u_sin'], np.sqrt(1 - pts['u_sin'] ** 2 - pts['v_sin'] ** 2), pts['v_sin']]).T

        # Platform velocity in Line-of-Sight (los) direction
        idy = len(self.velocity_tracker) - 1
        platform_velocity = np.array([self.velocity_tracker.loc[idy, 'vx_delta'], self.velocity_tracker.loc[idy, 'vy_delta'], self.velocity_tracker.loc[idy, 'vz_delta']])
        platform_vel_LOS_ms = Ai @ platform_velocity

        # Correct velocity
        pts['robust_dop'] = pts['uncorrected_dop'] + platform_vel_LOS_ms

        # Fold to [-vfold/2,vfold/2]
        pts['robust_dop'] = np.mod(pts['robust_dop'] + v_fold / 2, v_fold) - v_fold / 2

        return pts, platform_velocity

    def get_corrected_velocity(self, df, idx, meta_data_delta):
        # Record timestamp and velocity for corrected velocity
        idy = len(self.velocity_tracker)
        self.velocity_tracker.loc[idy, 'timestamp'] = df.loc[idx, "timestampRadar"] * 1e-9  # convert to timestamp in [s]
        self.velocity_tracker.loc[idy, 'vx_delta_cuda'] = meta_data_delta['platform_velocity'][0]
        self.velocity_tracker.loc[idy, 'vy_delta_cuda'] = meta_data_delta['platform_velocity'][1]
        self.velocity_tracker.loc[idy, 'vz_delta_cuda'] = meta_data_delta['platform_velocity'][2]
        self.velocity_tracker.loc[idy, 'v_delta_cuda'] = np.linalg.norm(meta_data_delta['platform_velocity'])
        self.velocity_tracker.loc[idy, 'vx_delta'] = meta_data_delta['platform_velocity'][0]
        self.velocity_tracker.loc[idy, 'vy_delta'] = meta_data_delta['platform_velocity'][1]
        self.velocity_tracker.loc[idy, 'vz_delta'] = meta_data_delta['platform_velocity'][2]
        self.velocity_tracker.loc[idy, 'v_delta'] = np.linalg.norm(meta_data_delta['platform_velocity'])

        # Get corrected velocity
        if idy > 3:

            # Get current velocity vec
            current_vel = np.array([self.velocity_tracker.loc[idy, 'vx_delta_cuda'], self.velocity_tracker.loc[idy, 'vy_delta_cuda'], self.velocity_tracker.loc[idy, 'vz_delta_cuda']])

            # Get previous velocity vec
            prev_vel = np.array([self.velocity_tracker.loc[idy - 1, 'vx_delta'], self.velocity_tracker.loc[idy - 1, 'vy_delta'], self.velocity_tracker.loc[idy - 1, 'vz_delta']])
            prev_prev_vel = np.array([self.velocity_tracker.loc[idy - 2, 'vx_delta'], self.velocity_tracker.loc[idy - 2, 'vy_delta'], self.velocity_tracker.loc[idy - 2, 'vz_delta']])

            # Get estimated velocity vec from kinematics
            accelaration = (prev_vel - prev_prev_vel) / (self.velocity_tracker.loc[idy - 1, 'timestamp'] - self.velocity_tracker.loc[idy - 2, 'timestamp'])
            estimated_vel = prev_vel + accelaration * (self.velocity_tracker.loc[idy, 'timestamp'] - self.velocity_tracker.loc[idy - 1, 'timestamp'])

            # Check if Nan
            if np.isnan(np.linalg.norm(current_vel)):

                self.velocity_tracker.loc[idy, 'vx_delta'] = estimated_vel[0]
                self.velocity_tracker.loc[idy, 'vy_delta'] = estimated_vel[1]
                self.velocity_tracker.loc[idy, 'vz_delta'] = estimated_vel[2]
                self.velocity_tracker.loc[idy, 'v_delta'] = np.linalg.norm(estimated_vel)

            # Check if velocity jump
            elif np.abs(np.linalg.norm(estimated_vel) - np.linalg.norm(current_vel)) > self.vel_est_threshold:

                # Get future velocity vec
                future_vel = np.nan
                idz = idx
                while np.isnan(np.linalg.norm(future_vel)):
                    idz += 1
                    with open(self.context.metadata + "//metadata_cuda_delta_" + str(df.loc[idz, "timestampRadar"]) + ".json") as f:
                        meta_data_delta_future = json.load(f)

                    future_vel = meta_data_delta_future['platform_velocity']

                # Get velocity estimation from future velocity
                backward_estimated_velocity = (prev_vel + future_vel) / 2

                self.velocity_tracker.loc[idy, 'vx_delta'] = backward_estimated_velocity[0]
                self.velocity_tracker.loc[idy, 'vy_delta'] = backward_estimated_velocity[1]
                self.velocity_tracker.loc[idy, 'vz_delta'] = backward_estimated_velocity[2]
                self.velocity_tracker.loc[idy, 'v_delta'] = np.linalg.norm(backward_estimated_velocity)

        return

    def apply_velocity_correction(self, df):
        self.velocity_tracker = pd.DataFrame(columns=['vx_delta_cuda', 'vy_delta_cuda', 'vz_delta_cuda', 'v_delta_cuda', 'vx_delta', 'vy_delta', 'vz_delta', 'v_delta', 'timestamp'])
        for i in tqdm(range(len(df))):
            ts = str(df.loc[i, 'timestampRadar'])
            try:
                meta_data_delta = load_json(self.context.metadata + "/metadata_cuda_delta_" + str(ts) + ".json")
                meta_data_4dfft = load_json(self.context.metadata + "/metadata_cuda_4dfft_" + str(ts) + ".json")

                if self.current_configuration["ALGI"]["IMG_ALG_TYPE"] == 2:
                    radar = pd.read_csv(self.context.cuda_4dfft + "/cuda_4dfft_" + str(ts) + ".csv", index_col=False)
                    meta_data = meta_data_4dfft
                elif self.current_configuration["ALGI"]["IMG_ALG_TYPE"] == 0:
                    radar = pd.read_csv(self.context.cuda_delta + "/cuda_delta_" + str(ts) + ".csv", index_col=False)
                    meta_data = meta_data_delta
                else:
                    print("apply_velocity_correction: UNKNOWN IMG ALG TYP FORMAT")
                    continue
            except Exception:
                PrintException()
                continue
            self.get_corrected_velocity(df, i, meta_data_delta)

            v_fold = meta_data['axis_params'][3][1]
            radar, platform_velocity = self.velocity_compensation(v_fold, radar)

            meta_data["robust_platform_velocity"] = list(platform_velocity)
            with open(self.context.metadata + '/metadata_' + str(self.img_name_map[self.current_configuration["ALGI"]["IMG_ALG_TYPE"]]) + "_" + str(ts) + '.json', 'w') as fp:
                json.dump(meta_data, fp, sort_keys=True, indent=2)

            radar = self.update_inidces(pc=radar, axis_params=meta_data['axis_params'])
            frame_path = self.get_radar_save_path(WiseSDK.DATA_TYPE_E.POINT_CLOUD_DATA_TYPE, ts)
            radar.to_csv(frame_path, index=False)
            # print("saved", frame_path)
        return

    def WiseSDK_kill(self):
        time.sleep(2)
        WiseSDK.modulesDestroy()
        time.sleep(2)

    def WiseSDK_init(self):
        # import WiseSDK
        # time.sleep(1)
        # reload(WiseSDK)
        # time.sleep(1)
        WiseSDK.modulesInit()

    def __del__(self):
        self.WiseSDK_kill()
