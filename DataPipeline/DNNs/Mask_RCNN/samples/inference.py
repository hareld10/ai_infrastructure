import os
import pathlib
import sys
import random
import math
import numpy as np
import skimage.io
import matplotlib
import matplotlib.pyplot as plt

from DataPipeline.DNNs.Mask_RCNN.mrcnn import utils
import DataPipeline.DNNs.Mask_RCNN.mrcnn.model as modellib
from DataPipeline.DNNs.Mask_RCNN.mrcnn import visualize

module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, module_path)
sys.path.insert(0, module_path + "/../")

sys.path.append(os.path.join(module_path, "coco/"))  # To find local version
import coco

COCO_MODEL_PATH = os.path.join(module_path, "mask_rcnn_coco.h5")