import time

import torch
from torch.backends import cudnn
import json
import sys
import os
import pathlib
import inspect
import traceback

# traceback.print_stack()
# time.sleep(2)

# print("OS GET CWD", os.getcwd())

module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, module_path)
sys.path.insert(0, "/workspace/AI/Harel/AI_Infrastructure/")
from pprint import pprint
# pprint(sys.path)
from OpenSource.WisenseEngine import WisenseEngine


from pprint import pprint
# pprint(sys.path)

from DNNs.EfficientDet.backbone import EfficientDetBackbone
# from backbone import EfficientDetBackbone
import logging
import cv2
import numpy as np

from tqdm import tqdm
import glob

from DNNs.EfficientDet.efficientdet.utils import BBoxTransform, ClipBoxes
from DNNs.EfficientDet.utils.utils import preprocess_original, invert_affine, postprocess

# from DNNs.wrappers.wrappers import OdWrapper


# class EfficientDetWrapper(OdWrapper):
#     def __init__(self, gpu=0):
#         compound_coef = 7
#         threshold = 0.2
#         iou_threshold = 0.2
#         device = "cuda:" + str(gpu)
#         cudnn.fastest = True
#         cudnn.benchmark = False
#         gpu_list = [1]
#         os.environ["CUDA_LAUNCH_BLOCKING"] = "1"
#         input_sizes = [512, 640, 768, 896, 1024, 1280, 1280, 1536]
#
#         obj_list = ['person', 'bicycle', 'car', 'motorcycle', 'airplane', 'bus', 'train', 'truck', 'boat', 'traffic light',
#                     'fire hydrant', '', 'stop sign', 'parking meter', 'bench', 'bird', 'cat', 'dog', 'horse', 'sheep',
#                     'cow', 'elephant', 'bear', 'zebra', 'giraffe', '', 'backpack', 'umbrella', '', '', 'handbag', 'tie',
#                     'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball', 'kite', 'baseball bat', 'baseball glove',
#                     'skateboard', 'surfboard', 'tennis racket', 'bottle', '', 'wine glass', 'cup', 'fork', 'knife', 'spoon',
#                     'bowl', 'banana', 'apple', 'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza', 'donut',
#                     'cake', 'chair', 'couch', 'potted plant', 'bed', '', 'dining table', '', '', 'toilet', '', 'tv',
#                     'laptop', 'mouse', 'remote', 'keyboard', 'cell phone', 'microwave', 'oven', 'toaster', 'sink',
#                     'refrigerator', '', 'book', 'clock', 'vase', 'scissors', 'teddy bear', 'hair drier',
#                     'toothbrush']
#
#         self.model = EfficientDetBackbone(compound_coef=compound_coef, num_classes=len(obj_list))
#         self.model.load_state_dict(torch.load(f'/{module_path}/weights/efficientdet-d{compound_coef}.pth', map_location='cpu'))
#         self.model.requires_grad_(False)
#         self.model.eval()
#         self.model = self.model.to(device)
#         return
#
#     def infer_im(self, im):
#         # Forward
#         return [Label2D()]


def display(preds, imgs, obj_list, imwrite=False, base_save_im=None, image_name=None, thresh=None, label_path=None):
    for i in range(len(imgs)):
        labels_save = []
        for j in range(len(preds[i]['rois'])):
            obj = obj_list[preds[i]['class_ids'][j]]
            cat_id = obj_list.index(obj)
            if obj == "person":
                obj = 'pedestrian'

            (x1, y1, x2, y2) = preds[i]['rois'][j].astype(np.int)

            score = float(preds[i]['scores'][j])
            cur_label = {}
            cur_label['cat'] = obj
            cur_label['cat_id'] = cat_id
            cur_label['score'] = float(score)  # score at center
            cur_label['bbox'] = [int(x1), int(y1), int(x2), int(y2)]
            cur_label['img_res'] = imgs[i].shape[:2]
            labels_save.append(cur_label)

            if imwrite:
                cv2.rectangle(imgs[i], (x1, y1), (x2, y2), (255, 255, 0), 1)
                cv2.putText(imgs[i], '{}, {:.3f}'.format(obj, score),
                            (x1, y1 - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                            (255, 255, 0), 1)
                cv2.imwrite(base_save_im + "/EfficientDetD7_" + image_name + ".jpg", imgs[i])

        with open(label_path, 'w') as fp:
            # print("write label", label_path)
            json.dump(labels_save, fp, sort_keys=True, indent=4)


# def infer_from_df(base_path, df, force=False, gpu=1):
#     print("infer efficientDet gpu", gpu, type(gpu))
#     model = EfficientDetBackbone(compound_coef=compound_coef, num_classes=len(obj_list))
#     model.load_state_dict(torch.load(f'/{module_path}/weights/efficientdet-d{compound_coef}.pth'))
#     model.requires_grad_(False)
#     model.eval()
#
#     model = model.cuda(gpu)
#
#     base_save_path = base_path + '/camera_det_EfficientDetD7/'
#     os.makedirs(base_save_path, exist_ok=True)
#
#     base_save_path_images = base_save_path + "/images/"
#     os.makedirs(base_save_path_images, exist_ok=True)
#     #
#     # option_1 = glob.glob(base_path + "/rgbCamera/*.png")
#     # option_2 = glob.glob(base_path + "/camera_rgb/*.png")
#     # if len(option_1) > 0:
#     #     option = option_1
#     # elif len(option_2) > 0:
#     #     option = option_2
#     # else:
#     #     print("Warning, couldn't figure camera_dir")
#     #     return
#
#     # for idx, image_path in tqdm(enumerate(option)):
#
#     for idx in tqdm(range(len(df))):
#         image_path = "/camera_rgb/rgb_camera_" + str(df.loc[idx, 'timestampCamera']) + ".png"
#         if not os.path.exists(image_path):
#             image_path = "/rgbCamera/rgbCamera_" + str(df.loc[idx, 'timestampCamera']) + ".png"
#             if not os.path.exists(image_path):
#                 print("Couldnt found image")
#                 continue
#
#         image_name = os.path.split(image_path)[-1][:-4]
#         ts = os.path.split(image_path)[-1].split("_")[-1].split(".")[0]
#         label_path = base_save_path + "/camera_det_" + str(ts) + ".json"
#
#         if os.path.exists(label_path) and not force:
#             continue
#
#         ori_imgs, framed_imgs, framed_metas = preprocess_original(image_path)
#         # x = torch.stack([torch.from_numpy(fi).cuda(gpu) for fi in framed_imgs], 0)
#         x = torch.stack([torch.from_numpy(fi).cpu() for fi in framed_imgs], 0)
#         x = x.to(torch.float32).permute(0, 3, 1, 2)
#
#         with torch.no_grad():
#             features, regression, classification, anchors = model(x)
#
#             regressBoxes = BBoxTransform()
#             clipBoxes = ClipBoxes()
#
#             out = postprocess(x,
#                               anchors, regression, classification,
#                               regressBoxes, clipBoxes,
#                               threshold, iou_threshold)
#         if idx % 70 == 0:
#             imwrite = True
#         else:
#             imwrite = False
#
#         display(out, ori_imgs, imwrite=imwrite, base_save_im=base_save_path_images, image_name=image_name,
#                 thresh=threshold, label_path=label_path)


# def try_tensor():
#     arr = np.random.rand(10, 10, 3)
#     x = torch.from_numpy(arr).clone()
#     x = x.to("cuda:0")
#     x = torch.stack([x], dim=0)
# print("!!!!!!!!!!!!!!!X_RAND!!!!!!!!!!!!!!!", x.shape)


def infer_from_base_path(context, df, force=False, gpu=0, rectified_flag=False):
    # device = "cpu"
    # try_tensor()

    compound_coef = 7
    threshold = 0.2
    iou_threshold = 0.2
    device = "cuda:" + str(gpu)
    cudnn.fastest = True
    cudnn.benchmark = False
    gpu_list = [1]
    os.environ["CUDA_LAUNCH_BLOCKING"] = "1"
    input_sizes = [512, 640, 768, 896, 1024, 1280, 1280, 1536]

    obj_list = ['person', 'bicycle', 'car', 'motorcycle', 'airplane', 'bus', 'train', 'truck', 'boat', 'traffic light',
                'fire hydrant', '', 'stop sign', 'parking meter', 'bench', 'bird', 'cat', 'dog', 'horse', 'sheep',
                'cow', 'elephant', 'bear', 'zebra', 'giraffe', '', 'backpack', 'umbrella', '', '', 'handbag', 'tie',
                'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball', 'kite', 'baseball bat', 'baseball glove',
                'skateboard', 'surfboard', 'tennis racket', 'bottle', '', 'wine glass', 'cup', 'fork', 'knife', 'spoon',
                'bowl', 'banana', 'apple', 'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza', 'donut',
                'cake', 'chair', 'couch', 'potted plant', 'bed', '', 'dining table', '', '', 'toilet', '', 'tv',
                'laptop', 'mouse', 'remote', 'keyboard', 'cell phone', 'microwave', 'oven', 'toaster', 'sink',
                'refrigerator', '', 'book', 'clock', 'vase', 'scissors', 'teddy bear', 'hair drier',
                'toothbrush']

    model = EfficientDetBackbone(compound_coef=compound_coef, num_classes=len(obj_list))
    model.load_state_dict(torch.load(f'/{module_path}/weights/efficientdet-d{compound_coef}.pth', map_location='cpu'))
    model.requires_grad_(False)
    model.eval()
    model = model.to(device)

    base_save_path = context.camera_det_EfficientDetD7
    base_save_path_images = context.camera_det_EfficientDetD7_images
    if rectified_flag:
        base_save_path = context.rectified_labels
        base_save_path_images = context.rectified_labels_images

    print("Edet7: force=", force)
    for idx in tqdm(range(len(df))):
        image_path = context.camera_rgb + "/camera_rgb_" + str(df.loc[idx, 'timestampCamera']) + ".png"
        if not os.path.exists(image_path):
            # print("Couldnt found image")
            logging.info("infer_from_base_path: Couldnt found image")
            continue
        image_name = os.path.split(image_path)[-1][:-4]
        ts = os.path.split(image_path)[-1].split("_")[-1].split(".")[0]
        label_path = base_save_path + "/camera_det_EfficientDetD7_" + str(ts) + ".json"

        if os.path.exists(label_path) and not force:
            continue

        ori_imgs, framed_imgs, framed_metas = preprocess_original(image_path, context=context, rectified_flag=rectified_flag)

        x = [torch.from_numpy(fi).clone().to(device) for fi in framed_imgs]
        x = torch.stack(x, 0)
        x = x.to(torch.float32).permute(0, 3, 1, 2)

        with torch.no_grad():
            features, regression, classification, anchors = model(x)

            regressBoxes = BBoxTransform()
            clipBoxes = ClipBoxes()

            out = postprocess(x,
                              anchors, regression, classification,
                              regressBoxes, clipBoxes,
                              threshold, iou_threshold)
        if idx % 70 == 0:
            imwrite = True
        else:
            imwrite = False

        display(out, ori_imgs, imwrite=imwrite, obj_list=obj_list, base_save_im=base_save_path_images, image_name=image_name,
                thresh=threshold, label_path=label_path)

    del model


if __name__ == '__main__':
    print("Object Detection D7 Inside docker")
    build_engine = WisenseEngine(base_path_drive=sys.argv[1], base_name="Wisense", df=sys.argv[2])
    infer_from_base_path(context=build_engine.drive_context, df=build_engine.df, force=bool(int(sys.argv[3])), gpu=int(sys.argv[4]))