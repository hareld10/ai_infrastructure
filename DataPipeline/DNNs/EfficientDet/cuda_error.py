import time

import torch
from torch.backends import cudnn
import json
import sys

import pathlib

module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, module_path)

from DNNs.EfficientDet.backbone import EfficientDetBackbone
# from backbone import EfficientDetBackbone

import cv2
import numpy as np
import os
from tqdm import tqdm
import glob

from DNNs.EfficientDet.efficientdet.utils import BBoxTransform, ClipBoxes
from DNNs.EfficientDet.utils.utils import preprocess_original, invert_affine, postprocess

cudnn.fastest = True
cudnn.benchmark = True

device = "cuda:0"
arr = np.random.rand(1248, 2208, 3)
x = torch.from_numpy(arr)
x = x.to("cuda:0")
x = torch.stack([x], 0)

print(x)