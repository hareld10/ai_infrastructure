import pathlib
import sys

import cv2
import numpy as np
import torch
import torch.nn.functional as F
from PIL import Image

from DataPipeline.DNNs.deeplab.inference_pipeline import DeepLabModel
from DataPipeline.DNNs.wrappers.wrappers import ModelWrapper
from wiseTypes.SegLabel import SegLabel


# %tensorflow_version 1.x


class DeepLabWrapper(ModelWrapper):
    def __init__(self, weights=None, gpu=1):
        super().__init__(model_name="DeepLabV3plus")
        self.module_path = str(pathlib.Path(__file__).parent.absolute())
        sys.path.insert(0, self.module_path)
        self.gpu = gpu
        self.model = None

    def load_model(self, ):
        download_path = self.module_path + "/../deeplab/deeplab_cityscapes_xception71_trainfine_2018_09_08.tar.gz"
        self.model = DeepLabModel(download_path, gpu=self.gpu)
        return

    def infer_im(self, im):
        if self.model is None:
            self.load_model()

        resized_orig_im = cv2.resize(im.copy(), (1824, 940))

        original_im = Image.fromarray(resized_orig_im)
        resized_im, seg_map = self.model.run(original_im)

        seg_map = seg_map.astype(np.uint8)

        # seg heat map
        batch_seg_heatmap = self.model.sess.run(
            self.model.HEATMAP_OUTPUT_TENSOR_NAME,
            feed_dict={self.model.INPUT_TENSOR_NAME: [np.asarray(resized_im)]})
        heatmaps = batch_seg_heatmap[0]

        # print(im_cv.shape, "should be height, width")
        heatmaps = heatmaps[:im.shape[0], :im.shape[1]]
        heatmaps = torch.from_numpy(heatmaps)

        heatmaps = torch.nn.functional.softmax(heatmaps, -1)
        probs, preds = torch.max(heatmaps, -1)

        combined_heatmap = torch.zeros(probs.shape)
        for i in range(19):
            combined_heatmap[preds == i] = probs[preds == i]

        combined_heatmap = combined_heatmap.numpy()
        combined_heatmap = cv2.resize(combined_heatmap, (im.shape[1], im.shape[0]))

        combined_heatmap = combined_heatmap.astype(np.float16)

        return cv2.resize(seg_map, (im.shape[1], im.shape[0])), combined_heatmap


    def get_seg_classes(self):
        ret = []
        ret.append(["road", 0])
        ret.append(["sidewalk", 1])
        ret.append(["buildings", 2])
        ret.append(["wall", 3])
        ret.append(["fence", 4])
        ret.append(["pole", 5])
        ret.append(["traffic-light", 6])
        ret.append(["traffic-sign", 7])
        ret.append(["vegetation", 8])
        ret.append(["terrain", 9])
        ret.append(["sky", 10])
        ret.append(["person",11])
        ret.append(["rider", 12])
        ret.append(["car", 13])
        ret.append(["truck", 14])
        ret.append(["bus", 15])
        ret.append(["train", 16])
        ret.append(["motorcycle", 17])
        ret.append(["bicycle", 18])
        return ret

    def cast_to_label(self, hard_label):
        return SegLabel(img=hard_label, engine=self)