from DataPipeline.DNNs.wrappers.exceptions import NotValidClass
from DataPipeline.DNNs.wrappers.wrappers import ModelWrapper
from wiseTypes.Label2D import Label2D
from Serving.serving import Wrapper


class YoloRWrapper(ModelWrapper):
    def __init__(self, model_name="YolorR-p6"):
        super().__init__(model_name, path_only=True)
        self.server = Wrapper(port=1025)

    def infer_im(self, im):
        bbs = self.server.send({"img": im})

        labels = []
        for x in bbs:
            try:
                cur_label = Label2D(cls=x["cat"], score=x["score"], x1=x["bbox"][0], y1=x["bbox"][1], x2=x["bbox"][2], y2=x["bbox"][3], force_obj=True)
                labels.append(cur_label)
            except NotValidClass as e:
                continue
        return labels
