from DataPipeline.DNNs.wrappers.exceptions import NotValidClass
from DataPipeline.DNNs.wrappers.wrappers import ModelWrapper

from DataPipeline.DNNs.PyTorch_YOLOv3.models import *
from DataPipeline.DNNs.PyTorch_YOLOv3.utils.utils import *
from DataPipeline.DNNs.PyTorch_YOLOv3.utils.datasets import *

import numpy as np
import os
from tqdm import tqdm
import glob
import sys
import json
import os
import sys
import time
import datetime
import argparse

from PIL import Image
import cv2
import torch
from torch.utils.data import DataLoader
from torchvision import datasets
from torch.autograd import Variable

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.ticker import NullLocator
import pathlib

from wiseTypes.Label2D import Label2D

default_config = {"nms_thres": 0.4,
                  "conf_thres": 0.7,
                  "version": "v0",
                  "data": "031521"}


class YoloWrapper(ModelWrapper):
    def __init__(self, config=default_config, weights=None, gpu=1):
        super().__init__(model_name="YoloV3")
        module_file = str(pathlib.Path(__file__).parent.absolute())
        self.yolo_dir = module_file + "/../PyTorch_YOLOv3"
        self.model_def = self.yolo_dir + "/config/yolov3.cfg"
        self.nms_thres = config["nms_thres"]
        self.conf_thres = config["conf_thres"]
        self.weights_path = self.yolo_dir + "/weights/yolov3.weights"

        self.class_path = self.yolo_dir + "/data/coco.names"
        self.classes = load_classes(self.class_path)

        cmap = plt.get_cmap("tab20b")
        self.colors = [cmap(i) for i in np.linspace(0, 1, 20)]
        self.gpu = gpu

        self.model = None
        self.im_size = None

    def load_model(self, im):
        self.im_size = max(im.shape)
        self.model = Darknet(self.model_def, img_size=self.im_size, gpus_list=[self.gpu]).cuda(self.gpu)
        self.model.load_darknet_weights(self.weights_path)
        self.model.eval()

    def infer_im(self, im):
        if self.model is None:
            self.load_model(im)

        im_to_pred = Image.fromarray(im.copy(), 'RGB')
        ratio = min(self.im_size / im_to_pred.size[0], self.im_size / im_to_pred.size[1])
        imw = round(im_to_pred.size[0] * ratio)
        imh = round(im_to_pred.size[1] * ratio)
        # resizing to box set by img_size and zero-padding
        img_transforms = transforms.Compose([transforms.Resize((imh, imw)),
                                             transforms.Pad((max(int((imh - imw) / 2), 0),
                                                             max(int((imw - imh) / 2), 0), max(int((imh - imw) / 2), 0),
                                                             max(int((imw - imh) / 2), 0)), (0, 0, 0)),
                                             transforms.ToTensor()
                                             ])

        img = img_transforms(im_to_pred)
        # convert image
        processed_im = img.float().cuda(self.gpu)
        # todo this might be version dependant
        #input_imgs = torch.unsqueeze(Variable(processed_im), axis=0)
        input_imgs = torch.unsqueeze(Variable(processed_im), dim=0)

        with torch.no_grad():
            detections = self.model(input_imgs)
            detections = non_max_suppression(detections, self.conf_thres, self.nms_thres)

        if detections is None: return []
        detections = detections[0]
        if detections is None: return []
        detections = rescale_boxes(detections, self.im_size, im.shape[:2])

        labels_save = []
        for x1, y1, x2, y2, conf, cls_conf, cls_pred in detections:
            score = cls_conf.item() * conf.item()
            x1, x2, y1, y2 = x1.numpy(), x2.numpy(), y1.numpy(), y2.numpy()
            cls_pred = cls_pred.numpy()
            try:
                labels_save.append(Label2D(cls=self.classes[int(cls_pred)], score=float(score), x1=x1, y1=y1, x2=x2, y2=y2))
            except NotValidClass as e:
                continue
        return labels_save