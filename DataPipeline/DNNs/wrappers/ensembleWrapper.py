# from tensorflow_core.python.client import device_lib

# from DataPipeline.DNNs.wrappers.deepLabWrapper import DeepLabWrapper
from DataPipeline.DNNs.wrappers.efficientDetWrapper import EfficientDetWrapper
from DataPipeline.DNNs.wrappers.wrappers import ModelWrapper
from DataPipeline.DNNs.wrappers.yoloWrapper import YoloWrapper
from DataPipeline.DNNs.wrappers.yolorWrapper import YoloRWrapper
from DataPipeline.ensamble import all_ensemble_contexts, NewEnsemble
from DataPipeline.ensamble import Ensemble
from LabelsManipulation.clf_utils import non_max_suppression_fast
from wiseTypes.Label import Label
from wiseTypes.Label2D import Label2D
import cv2
# import tensorflow as tf
import numpy as np

# def get_available_gpus():
#     local_device_protos = device_lib.list_local_devices()
#     return [x.name for x in local_device_protos if x.device_type == 'GPU']
#
# print(get_available_gpus())


class EnsembleWrapper(ModelWrapper):
    def __init__(self, yolor_thresh, yolov3_thresh, d7_thresh, iou_thresh, nms_thresh):
        super().__init__(model_name="EnsembleOd")
        self.e_det_wrapper = EfficientDetWrapper(gpu=1)
        self.yolor_wrapper = YoloRWrapper()
        self.yolov3_wrapper = YoloWrapper(gpu=1)
        # self.seg_wrapper = DeepLabWrapper()
        # self.ensembler = NewEnsemble(self.contexts)
        # self.model_name = f"ensemble_2d_{contexts[0].final_class_name}_{round(contexts[0].yolo_thresh, 2)}_{round(contexts[0].d7_thresh, 2)}_{round(contexts[0].iou_thresh, 2)}_{round(contexts[0].nms_thresh, 2)}"
        self.yolor_thresh, self.yolov3_thresh, self.d7_thresh, self.iou, self.nms = yolor_thresh, yolov3_thresh, d7_thresh, iou_thresh, nms_thresh
        self.model_name = f"ensemble_2d_{yolor_thresh}_{yolov3_thresh}_{d7_thresh}_{iou_thresh}_{nms_thresh}"
    def run_engine_idx(self, engine, engine_idx, generate=False, force=False, save=True):
        # seg_map, combined_heatmap = self.seg_wrapper.run_engine_idx(engine, engine_idx, force=False)
        seg_map = None
        e_det_label = self.e_det_wrapper.run_engine_idx(engine, engine_idx, generate=generate, force=False)
        yolor_label = self.yolor_wrapper.run_engine_idx(engine, engine_idx, generate=generate, force=False)
        yolov3_label = self.yolov3_wrapper.run_engine_idx(engine, engine_idx, generate=generate, force=False)

        if e_det_label is None:
            e_det_label = []
        if yolor_label is None:
            yolor_label = []
        if yolov3_label is None:
            yolov3_label = []

        # print(len(e_det_label), len(yolor_label), len(yolov3_label))
        # im = engine.get_image(engine_idx)

        finals = []
        [finals.append(x) for x in e_det_label if x.score >= self.d7_thresh]
        [finals.append(x) for x in yolor_label if x.score >= self.yolor_thresh]
        [finals.append(x) for x in yolov3_label if x.score >= self.yolov3_thresh]

        debug = False

        if debug:
            print()
            print("Ensemble 2D ", engine.get_id(engine_idx))
            if len(e_det_label) > 0:
                print(f"edet={type(e_det_label[0])}\n", e_det_label)
            print()
            if len(yolor_label) > 0:
                print(f"yolo={type(yolor_label[0])}\n", yolor_label)
            print(f"len(edet)={len(e_det_label)}, len(yolo)={len(yolor_label)}")
            print(f"finals before iou={len(finals)}")

        for x in e_det_label:
            for y in yolor_label:
                iou = Label.get_2d_iou(x, y)
                if iou > self.iou:
                    x.score = min((x.score + y.score)/2, 1)
                    finals.append(x)
                    break

        for x_idx in range(len(finals)):
            for y in yolov3_label:
                iou = Label.get_2d_iou(finals[x_idx], y)
                if iou > self.iou:
                    y.score = min((y.score + y.score)/2, 1)
                    finals.append(y)
                    break
        if debug:
            print(f"finals after iou={len(finals)}")

        bboxes = np.array([x.get_bev_bbox() for x in finals])

        # print(bboxes)

        indices = np.array(non_max_suppression_fast(bboxes, self.nms))
        # print("nms_keep_indices", indices)
        finals = np.asarray(finals)
        ret = []
        if len(indices) > 0:
            ret = finals[indices]

        if debug:
            print(f"finals after nms={len(ret)}")
        # ret = self.ensembler.ensemble_from_obj(img=im, e_det=e_det_label, yolo_det=yolo_label, seg_label=seg_map)
        # print(self.model_name)
        # print(f"d7={len(e_det_label)}, yolo={len(yolo_label)}, ensemble={len(ret)}")
        # print(f"yolo={type(yolo_label[0])}, edet={type(e_det_label[0])}, ensemble={type(ret[0])}")
        return ret

    def infer_im(self, im):
        seg_map, combined_heatmap = self.seg_wrapper.infer_im(im)
        e_det_label = self.e_det_wrapper.infer_im(im)
        yolo_label = self.yolo_wrapper.infer_im(im)

        return self.ensembler.ensemble_from_obj(img=im, e_det=e_det_label, yolo_det=yolo_label, seg_label=seg_map)