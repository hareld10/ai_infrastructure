from flask import Flask, jsonify, request
from DataPipeline.DNNs.wrappers.efficientDetWrapper import EfficientDetWrapper
import json, os, cv2

port = 1027

app = Flask(__name__)
wrapper = EfficientDetWrapper()
wrapper.load_model()


@app.route('/predict', methods=['POST'])
def predict():
    if request.method == 'POST':
        df_json = request.get_json()
        json_load = json.loads(df_json)

        path = json_load["img"]
        if os.path.exists(path):
            img = cv2.imread(path)
        else:
            print("path doesnt exists", path)
            return json.dumps({'pred_hard': "None", "pred_soft": "None"})

        labels_2d = wrapper.infer_im(img)
        labels_2d = [x.get_dict() for x in labels_2d]
        return json.dumps(labels_2d)


@app.route('/get_pid', methods=['POST'])
def get_pid():
    if request.method == 'POST':
        return {"pid": os.getpid()}


if __name__ == '__main__':
    print("wrapper service", os.getpid())
    app.run(host="0.0.0.0", port=port, debug=False)
