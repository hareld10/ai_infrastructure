import os

import numpy as np


class ModelWrapper:
    def __init__(self, model_name, path_only=False):
        self.model_name = model_name
        self.default_config = {}
        self.path_only = path_only
        return

    def run_engine_idx(self, engine, engine_idx, force=False, generate=False, save=True):
        pred_path = f"{engine.get_predictions_dir()}/{self.get_name()}/{engine.get_id(engine_idx)}.npy"
        if not force:
            os.makedirs(os.path.split(pred_path)[0], exist_ok=True)
            if os.path.exists(pred_path):
                # print("loaded", pred_path)
                return np.load(pred_path, allow_pickle=True)
            else:
                print(f"pred_path dowsn'y exists {pred_path}")
        if not generate:
            return
        # print("need to generate")
        im = engine.get_image(engine_idx, path_only=self.path_only)
        predictions = self.infer_im(im)
        if save:
            np.save(pred_path, predictions)
        return predictions

    def get_name(self):
        return self.model_name

    def infer_im(self, im):
        return

    def get_default_config(self):
        return self.default_config
