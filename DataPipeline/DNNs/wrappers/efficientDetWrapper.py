import os

import torch
from torch.backends import cudnn
import pathlib
import sys

from DataPipeline.DNNs.wrappers.exceptions import NotValidClass

module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, module_path)
edet_moudule = module_path + "/../EfficientDet/"
sys.path.insert(0, edet_moudule)
sys.path.insert(0, module_path + "/../../../")
sys.path.insert(0, module_path + "/../../")

import torch
from torch.backends import cudnn
import numpy as np
from DataPipeline.DNNs.EfficientDet.efficientdet.utils import BBoxTransform, ClipBoxes
from DataPipeline.DNNs.EfficientDet.utils.utils import preprocess_original, postprocess, preprocess_direct
from DataPipeline.DNNs.wrappers.wrappers import ModelWrapper
from wiseTypes.Label2D import Label2D

from DataPipeline.DNNs.EfficientDet.backbone import EfficientDetBackbone


# from backbone import EfficientDetBackbone

# from DNNs.wrappers.wrappers import OdWrapper


# default_config = {"compound_coef": 7,
#                   "threshold": 0.2,
#                   "iou_threshold": 0.2,
#                   "weights": module_path + "/../EfficientDet//weights/efficientdet-d7.pth",
#                   "version": "v0",
#                   "date": "031521"}
print("\n\nedet config thresh change to 0")

default_config = {"compound_coef": 7,
                  "threshold": 0.01,
                  "iou_threshold": 0.2,
                  "weights": module_path + "/../EfficientDet//weights/efficientdet-d7.pth",
                  "version": "v0",
                  "date": "031521"}

class EfficientDetWrapper(ModelWrapper):
    def __init__(self, config=default_config, weights=None, gpu=0):
        super().__init__(model_name="EfficientDetD7")
        self.compound_coef = config["compound_coef"]
        self.threshold = config["threshold"]
        self.iou_threshold = config["iou_threshold"]
        self.device = "cuda:" + str(gpu)
        cudnn.fastest = True
        cudnn.benchmark = False
        # os.environ["CUDA_LAUNCH_BLOCKING"] = "1"
        self.input_sizes = [512, 640, 768, 896, 1024, 1280, 1280, 1536]
        self.rectified_flag = False
        self.config = default_config
        self.obj_list = ['person', 'bicycle', 'car', 'motorcycle', 'airplane', 'bus', 'train', 'truck', 'boat', 'traffic light',
                         'fire hydrant', '', 'stop sign', 'parking meter', 'bench', 'bird', 'cat', 'dog', 'horse', 'sheep',
                         'cow', 'elephant', 'bear', 'zebra', 'giraffe', '', 'backpack', 'umbrella', '', '', 'handbag', 'tie',
                         'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball', 'kite', 'baseball bat', 'baseball glove',
                         'skateboard', 'surfboard', 'tennis racket', 'bottle', '', 'wine glass', 'cup', 'fork', 'knife', 'spoon',
                         'bowl', 'banana', 'apple', 'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza', 'donut',
                         'cake', 'chair', 'couch', 'potted plant', 'bed', '', 'dining table', '', '', 'toilet', '', 'tv',
                         'laptop', 'mouse', 'remote', 'keyboard', 'cell phone', 'microwave', 'oven', 'toaster', 'sink',
                         'refrigerator', '', 'book', 'clock', 'vase', 'scissors', 'teddy bear', 'hair drier',
                         'toothbrush']

        self.model = None
        return

    @staticmethod
    def get_default_config():
        return default_config

    def load_model(self):
        self.model = EfficientDetBackbone(compound_coef=self.compound_coef, num_classes=len(self.obj_list))
        # self.model.load_state_dict(torch.load(f'/{edet_moudule}/weights/efficientdet-d{self.compound_coef}.pth', map_location='cpu'))
        self.model.load_state_dict(torch.load(self.config["weights"], map_location='cpu'))
        self.model.requires_grad_(False)
        self.model.eval()
        self.model = self.model.to(self.device)
        return True

    def infer_im(self, im):
        if self.model is None:
            self.load_model()

        imgs = im
        ori_imgs, framed_imgs, framed_metas = preprocess_direct(imgs)

        x = [torch.from_numpy(fi).clone().to(self.device) for fi in framed_imgs]
        x = torch.stack(x, 0)
        x = x.to(torch.float32).permute(0, 3, 1, 2)

        with torch.no_grad():
            features, regression, classification, anchors = self.model(x)

            regressBoxes = BBoxTransform()
            clipBoxes = ClipBoxes()

            preds = postprocess(x,
                                anchors, regression, classification,
                                regressBoxes, clipBoxes,
                                self.threshold, self.iou_threshold)

        for i in range(len(ori_imgs)):
            labels_save = []

            for j in range(len(preds[i]['rois'])):
                (x1, y1, x2, y2) = preds[i]['rois'][j].astype(np.int)
                try:
                    cur_label = Label2D(cls=self.obj_list[preds[i]['class_ids'][j]], score=float(preds[i]['scores'][j]),
                                        x1=x1, y1=y1, x2=x2, y2=y2)
                except NotValidClass as e:
                    continue

                labels_save.append(cur_label)

        return labels_save
