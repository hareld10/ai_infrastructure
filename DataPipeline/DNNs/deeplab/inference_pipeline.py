import os
import tarfile

import cv2
import numpy as np
# %tensorflow_version 1.x
import tensorflow as tf
import torch
import torch.nn.functional as F
from PIL import Image
from matplotlib import gridspec
from matplotlib import pyplot as plt
from tqdm import tqdm

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

# print(tf.__version__)


class DeepLabModel(object):
    """Class to load deeplab model and run inference."""

    INPUT_TENSOR_NAME = 'ImageTensor:0'
    OUTPUT_TENSOR_NAME = 'SemanticPredictions:0'
    INPUT_SIZE = 1824
    FROZEN_GRAPH_NAME = 'frozen_inference_graph'
    HEATMAP_OUTPUT_TENSOR_NAME = 'ResizeBilinear_1:0'

    def __init__(self, tarball_path, gpu=1):
        """Creates and loads pretrained deeplab model."""
        self.graph = tf.Graph()
        # print("GPU", gpu)
        os.environ["CUDA_VISIBLE_DEVICES"] = str(gpu)
        print("os:", os.environ['CUDA_VISIBLE_DEVICES'])
        self.config = tf.ConfigProto(log_device_placement=True)

        graph_def = None
        # Extract frozen graph from tar archive.
        tar_file = tarfile.open(tarball_path)
        for tar_info in tar_file.getmembers():
            if self.FROZEN_GRAPH_NAME in os.path.basename(tar_info.name):
                file_handle = tar_file.extractfile(tar_info)
                graph_def = tf.GraphDef.FromString(file_handle.read())
                break

        tar_file.close()

        if graph_def is None:
            raise RuntimeError('Cannot find inference graph in tar archive.')

        with self.graph.as_default():
            tf.import_graph_def(graph_def, name='')

        self.sess = tf.Session(graph=self.graph, config=self.config)

    # @staticmethod
    # def set_input_size(im):
    #     DeepLabModel.INPUT_SIZE = np.max(im.shape)

    def run(self, image):
        """Runs inference on a single image.

        Args:
          image: A PIL.Image object, raw input image.

        Returns:
          resized_image: RGB image resized from original input image.
          seg_map: Segmentation map of `resized_image`.
        """
        width, height = image.size
        resize_ratio = 1.0 * self.INPUT_SIZE / max(width, height)
        spacing = 0
        if width != self.INPUT_SIZE:  # ZED WIDTH
            spacing = -1
        elif width == 2208: # ZED WIDTH
            spacing=-1
        target_size = (int(resize_ratio * width), int(resize_ratio * height)+spacing)
        resized_image = image.convert('RGB').resize(target_size, Image.ANTIALIAS)
        batch_seg_map = self.sess.run(
            self.OUTPUT_TENSOR_NAME,
            feed_dict={self.INPUT_TENSOR_NAME: [np.asarray(resized_image)]})

        seg_map = batch_seg_map[0]

        return resized_image, seg_map


def create_pascal_label_colormap():
    """Creates a label colormap used in PASCAL VOC segmentation benchmark.

    Returns:
    A Colormap for visualizing segmentation results.
    """
    colormap = np.zeros((256, 3), dtype=int)
    ind = np.arange(256, dtype=int)

    for shift in reversed(range(8)):
        for channel in range(3):
            colormap[:, channel] |= ((ind >> channel) & 1) << shift
        ind >>= 3

    colormap = np.array([(128, 64, 128),
                         (244, 35, 232),
                         (220, 20, 60),
                         (255, 0, 0),
                         (0, 0, 142),
                         (0, 0, 70),
                         (0, 60, 100),
                         (0, 80, 100),
                         (0, 0, 230),
                         (119, 11, 32),
                         (70, 70, 70),
                         (102, 102, 156),
                         (190, 153, 153),
                         (153, 153, 153),
                         (220, 220, 0),
                         (250, 170, 30),
                         (107, 142, 35),
                         (152, 251, 152),
                         (70, 130, 180),
                         ])
    return colormap


def label_to_color_image(label):
    """Adds color defined by the dataset colormap to the label.

    Args:
    label: A 2D array with integer type, storing the segmentation label.

    Returns:
    result: A 2D array with floating type. The element of the array
      is the color indexed by the corresponding element in the input label
      to the PASCAL color map.

    Raises:
    ValueError: If label is not of rank 2 or its value is larger than color
      map maximum entry.
    """
    if label.ndim != 2:
        raise ValueError('Expect 2-D input label')

    colormap = create_pascal_label_colormap()

    if np.max(label) >= len(colormap):
        raise ValueError('label value too large.')

    return colormap[label]


def vis_segmentation(image, seg_map, fig_path="."):
    """Visualizes input image, segmentation map and overlay view."""
    plt.figure(figsize=(40, 10))
    grid_spec = gridspec.GridSpec(1, 3, width_ratios=[6, 6, 6, ])

    plt.subplot(grid_spec[0])
    plt.imshow(image)
    plt.axis('off')
    plt.title('input image')

    plt.subplot(grid_spec[1])
    seg_image = label_to_color_image(seg_map).astype(np.uint8)
    plt.imshow(seg_image)
    plt.axis('off')
    plt.title('segmentation map')

    plt.subplot(grid_spec[2])
    plt.imshow(image)
    plt.imshow(seg_image, alpha=0.7)
    plt.axis('off')
    plt.title('segmentation overlay')

    #     unique_labels = np.unique(seg_map)
    #     ax = plt.subplot(grid_spec[3])
    #     plt.imshow(
    #       FULL_COLOR_MAP[unique_labels].astype(np.uint8), interpolation='nearest')
    #     ax.yaxis.tick_right()
    #     plt.yticks(range(len(unique_labels)), LABEL_NAMES[unique_labels])
    #     plt.xticks([], [])
    #     ax.tick_params(width=0.0)
    plt.grid('off')
    plt.savefig(fig_path)
    plt.close()


def vis_segmentation_debug(image, seg_map, fig_path, grid_spec):
    """Visualizes input image, segmentation map and overlay view."""

    plt.subplot(grid_spec[0])
    plt.imshow(image)
    plt.axis('off')
    plt.title('input image')

    plt.subplot(grid_spec[1])
    seg_image = label_to_color_image(seg_map).astype(np.uint8)
    plt.imshow(seg_image)
    plt.axis('off')
    plt.title('segmentation map')

    plt.subplot(grid_spec[2])
    plt.imshow(image)
    plt.imshow(seg_image, alpha=0.7)
    plt.axis('off')
    plt.title('segmentation overlay')

    #     base = os.path.split(os.path.split(path)[0])[0]

    #     ts = os.path.split(path)[-1].split("_")[-1].split(".")[0]

    # road
    #     label = base + '/camera_road_seg/camera_road_seg_' + ts + ".npz"
    #     label = np.load(label)["arr_0"]
    #     print("image", image.size)
    # #     print("label1", label.shape, label.dtype)
    #     label[label<0.2] = None
    #     plt.subplot(grid_spec[4])
    #     plt.imshow(image)
    #     plt.imshow(label, cmap=plt.cm.jet_r, alpha=0.4)
    #     plt.axis('off')
    #     plt.title('our road seg')

    #     label = base + '/camera_vehicle_seg/camera_vehicle_seg_' + ts + ".npz"
    #     label = np.load(label)["arr_0"]
    #     label = label.astype(np.float32)
    #     label[label<0.2] = None
    # #     print("label2", label.shape, label.dtype)
    #     plt.subplot(grid_spec[5])
    #     plt.imshow(image)
    #     plt.imshow(label, cmap=plt.cm.jet_r, alpha=0.4)
    #     plt.axis('off')
    #     plt.title('our veh seg')

    plt.grid('off')
    plt.savefig(fig_path)
    plt.close()


LABEL_NAMES = np.asarray([
    "road", "sidewalk", "building", "wall", "fence", "pole", "traffic-light", "traffic-sign", "vegetation", "terrain",
    "sky",
    "person", "rider", "car", "truck", "bus", "train", "motorcycle", "bicycle",

])


# FULL_LABEL_MAP = np.arange(len(LABEL_NAMES)).reshape(len(LABEL_NAMES), 1)
# FULL_COLOR_MAP = label_to_color_image(FULL_LABEL_MAP)


def deeplab_from_base_path(context, df, force=False, gpu=0):
    os.environ["CUDA_VISIBLE_DEVICES"]=str(gpu)
    download_path = "/workspace/AI/Harel/Segmentation/models/research/deeplab/deeplab_cityscapes_xception71_trainfine_2018_09_08.tar.gz"
    MODEL = DeepLabModel(download_path, gpu=gpu)

    base_save_path = context.camera_seg_class
    base_save_path_images = context.camera_seg_class_images
    base_save_path_soft_seg = context.camera_seg_soft
    base_save_path_images_soft = context.camera_seg_soft_images

    print('model loaded successfully!')

    for idx in tqdm(range(len(df))):
        rgb_path = context.camera_rgb + "/camera_rgb_" + str(df.iloc[idx]["timestampCamera"]) + ".png"
        if not os.path.exists(rgb_path):
            continue

        label_path = base_save_path + "/camera_seg_class_" + str(df.iloc[idx]["timestampCamera"]) + ".npz"
        label_path_soft = base_save_path_soft_seg + "/camera_seg_soft_" + str(df.iloc[idx]["timestampCamera"]) + ".npz"

        if (os.path.exists(label_path) and os.path.exists(label_path_soft)) and not force:
            continue

        try:
            original_im = Image.open(rgb_path)
            im_cv = cv2.imread(rgb_path)[..., ::-1]
        except Exception as e:
            print('Cannot retrieve image ')
            continue
        resized_im, seg_map = MODEL.run(original_im)

        seg_map = seg_map.astype(np.uint8)

        # seg heat map
        batch_seg_heatmap = MODEL.sess.run(
            MODEL.HEATMAP_OUTPUT_TENSOR_NAME,
            feed_dict={MODEL.INPUT_TENSOR_NAME: [np.asarray(resized_im)]})
        heatmaps = batch_seg_heatmap[0]

        # print(im_cv.shape, "should be height, width")
        heatmaps = heatmaps[:im_cv.shape[0], :im_cv.shape[1]]
        heatmaps = torch.from_numpy(heatmaps)

        heatmaps = torch.nn.functional.softmax(heatmaps, -1)
        probs, preds = torch.max(heatmaps, -1)

        combined_heatmap = torch.zeros(probs.shape)
        for i in range(19):
            combined_heatmap[preds == i] = probs[preds == i]

        combined_heatmap = combined_heatmap.numpy()

        if im_cv.shape[1] != DeepLabModel.INPUT_SIZE:
        # if im_cv.shape[1]==2208:
            combined_heatmap = cv2.resize(combined_heatmap, (im_cv.shape[1], im_cv.shape[0]))
            seg_map = cv2.resize(seg_map, (im_cv.shape[1], im_cv.shape[0]))

        combined_heatmap = combined_heatmap.astype(np.float16)

        np.savez(label_path_soft, combined_heatmap)
        np.savez(label_path, seg_map)

        # print("seg_map.shape", seg_map.shape)
        # print("combined_heatmap.shape", combined_heatmap.shape)
        # print("im_cv.shape", im_cv.shape)

        if idx % 70 == 0:
            plt.figure(figsize=(30, 30))
            combined_heatmap = combined_heatmap.astype(np.float32)
            combined_heatmap[combined_heatmap < 0.2] = None
            plt.imshow(im_cv)
            plt.imshow(combined_heatmap, cmap=plt.cm.jet_r, alpha=0.4)
            cbar = plt.colorbar()
            cbar.set_alpha(1)
            cbar.draw_all()
            plt.savefig(base_save_path_images_soft + str(df.iloc[idx]["timestampCamera"]) + "_heatmap.png")
            plt.close()

            vis_segmentation(im_cv, seg_map,
                             fig_path=base_save_path_images + str(df.iloc[idx]["timestampCamera"]) + ".png")

#         except Exception as e:
#             print("err", e)
