import json
import linecache
import os
import os.path
import pickle
import socket
import sys
import traceback
from contextlib import closing

import cv2
import numpy as np


def is_docker():
    path = '/proc/self/cgroup'
    return (
            os.path.exists('/.dockerenv') or
            os.path.isfile(path) and any('docker' in line for line in open(path))
    )


def find_open_ports():
    for port in range(1, 15000):
        with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as sock:
            res = sock.connect_ex(('localhost', port))
            if res == 0:
                yield port


def find_data_size_properties(data_frame):
    num_axis1 = data_frame.getHeader().dims.size1
    num_axis2 = data_frame.getHeader().dims.size2
    num_axis3 = data_frame.getHeader().dims.size3
    num_axis4 = data_frame.getHeader().dims.size4
    # print("num_axis1, num_axis2, num_axis3, num_axis4:", num_axis1, num_axis2, num_axis3, num_axis4)
    return num_axis1, num_axis2, num_axis3, num_axis4


def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    tb = traceback.format_exc()
    print(tb)
    print('EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj))


def read_label(path):
    with open(path, "r") as fp:
        frame_data = json.load(fp)
    return frame_data


def pickle_write(file_path, obj):
    """
    Serialize an object to a provided file_path
    """

    with open(file_path, 'wb') as file:
        pickle.dump(obj, file)


def pickle_read(file_path):
    """
    De-serialize an object from a provided file_path
    """

    with open(file_path, 'rb') as file:
        return pickle.load(file)


def filter_fov(fov_az, fov_el, pts, x_index=0, y_index=1, z_index=2):
    def to_rng_az_el(x, y, z, angle_type):
        """Cartesian (x,y,z) to spherical (range azimuth elevation) coordinates
        """
        r = np.sqrt(x ** 2 + y ** 2 + z ** 2)

        # Following Kuti's definition
        az = np.arcsin(x / r)  # az
        el = np.arcsin(z / r)  # phi

        if angle_type == 'deg':
            az = np.rad2deg(az)
            el = np.rad2deg(el)

        return (r, az, el)

    if fov_az == 0 and fov_el == 0:
        return pts

    #     print('before rng-az transform',np.max(pts[:,2]))
    # Transform to range-azimuth-elevation
    r, Az, El = to_rng_az_el(x=pts[:, x_index], y=pts[:, y_index], z=pts[:, z_index], angle_type='deg')

    # Filter azimuth
    left_az = -(fov_az // 2)
    right_az = (fov_az // 2)
    az_filter = np.logical_and(left_az <= Az, Az <= right_az)

    # Filter elevation
    down_el = -(fov_el // 2)
    up_el = (fov_el // 2)
    el_filter = np.logical_and(down_el <= El, El <= up_el)
    fov_filter = np.logical_and(az_filter, el_filter)

    return pts[fov_filter, :]


def db(array):
    return 10 * np.log10(array)


def load_json(file_name):
    with open(file_name, "r") as fp:
        js = json.load(fp)
    return js


def lidar_to_radar_coordinates(points):
    theta = np.deg2rad(90)
    rotation_matrix = np.array([[np.cos(theta), -np.sin(theta), 0],
                                [np.sin(theta), np.cos(theta), 0],
                                [0, 0, 1]])

    return np.matmul(rotation_matrix, points.T).T


def toRAzEl(x, y, z):
    """Cartesian (x,y,z) to spherical (range azimuth elevation) coordinates
    """
    r = np.sqrt(x ** 2 + y ** 2 + z ** 2)
    # Following Kuti's definition
    Az = np.rad2deg(np.arcsin(x / r))  # Az
    El = np.rad2deg(np.arcsin(z / r))  # phi
    return (r, Az, El)


def toXYZ(r, Az, El):
    """Spherical (range azimuth elevation) to cartesian (x,y,z) coordinates
    """
    u = np.sin(np.deg2rad(Az))  # u == cos(phi)sin(theta)
    v = np.sin(np.deg2rad(El))  # v == sin(phi)
    # following Kuti's definition
    x = r * u
    y = r * np.sqrt(1 - u ** 2 - v ** 2)
    z = r * v
    return (x, y, z)


def filter_fov_az(pts, az):
    if az == 0:
        return pts
    pts = pts[pts[:, 0] > 0]
    r, Az, El = toRAzEl(x=pts[:, 0], y=pts[:, 1], z=pts[:, 2])
    mask = np.logical_not(np.logical_and(Az < (az // 2), (-az // 2) < Az))
    return pts[mask, :]


def get_nci(s_ant):
    nci = np.zeros([s_ant.shape[0], s_ant.shape[2]])
    for i in range(s_ant.shape[1]):
        nci += np.abs(s_ant[:, i, :])
    nci = np.fft.fftshift(nci, axes=(1,))
    cur_plot = db(nci).T
    h, w = cur_plot.shape[:2]
    cur_plot = cv2.resize(cur_plot, dsize=(int(h) * 6, int(w / 2)), interpolation=cv2.INTER_NEAREST)
    return cur_plot


def get_iou(a, b, epsilon=1e-5):
    """ Given two boxes `a` and `b` defined as a list of four numbers:
            [x1,y1,x2,y2]
        where:
            x1,y1 represent the upper left corner
            x2,y2 represent the lower right corner
        It returns the Intersect of Union score for these two boxes.

    Args:
        a:          (list of 4 numbers) [x1,y1,x2,y2]
        b:          (list of 4 numbers) [x1,y1,x2,y2]
        epsilon:    (float) Small value to prevent division by zero

    Returns:
        (float) The Intersect of Union score.
    """
    # COORDINATES OF THE INTERSECTION BOX
    x1 = max(a[0], b[0])
    y1 = max(a[1], b[1])
    x2 = min(a[2], b[2])
    y2 = min(a[3], b[3])

    # AREA OF OVERLAP - Area where the boxes intersect
    width = (x2 - x1)
    height = (y2 - y1)
    # handle case where there is NO overlap
    if (width < 0) or (height < 0):
        # print("Got", a, b)
        return 0.0
    area_overlap = width * height

    # COMBINED AREA
    area_a = (a[2] - a[0]) * (a[3] - a[1])
    area_b = (b[2] - b[0]) * (b[3] - b[1])
    area_combined = area_a + area_b - area_overlap

    # RATIO OF AREA OF OVERLAP OVER COMBINED AREA
    iou = area_overlap / (area_combined + epsilon)
    return iou


def non_max_suppression_fast(boxes, overlapThresh, x1_start_idx=9):
    # if there are no boxes, return an empty list
    # boxes = []
    # for elem in frame_data:
    #     boxes.append([elem["bbox"][0], elem["bbox"][1], elem["bbox"][2], elem["bbox"][3]])

    boxes = np.array(boxes)
    if len(boxes) == 0:
        return []
    # if the bounding boxes integers, convert them to floats --
    # this is important since we'll be doing a bunch of divisions

    if boxes.dtype.kind == "i":
        boxes = boxes.astype("float")

    # initialize the list of picked indexes
    pick = []
    # grab the coordinates of the bounding boxes
    x1 = boxes[:, x1_start_idx]
    y1 = boxes[:, x1_start_idx + 1]
    x2 = boxes[:, x1_start_idx + 2]
    y2 = boxes[:, x1_start_idx + 3]
    # compute the area of the bounding boxes and sort the bounding
    # boxes by the bottom-right y-coordinate of the bounding box
    area = (x2 - x1 + 1) * (y2 - y1 + 1)
    idxs = np.argsort(y2)
    # keep looping while some indexes still remain in the indexes
    # list
    while len(idxs) > 0:
        # grab the last index in the indexes list and add the
        # index value to the list of picked indexes
        last = len(idxs) - 1
        i = idxs[last]
        pick.append(i)
        # find the largest (x, y) coordinates for the start of
        # the bounding box and the smallest (x, y) coordinates
        # for the end of the bounding box
        xx1 = np.maximum(x1[i], x1[idxs[:last]])
        yy1 = np.maximum(y1[i], y1[idxs[:last]])
        xx2 = np.minimum(x2[i], x2[idxs[:last]])
        yy2 = np.minimum(y2[i], y2[idxs[:last]])
        # compute the width and height of the bounding box
        w = np.maximum(0, xx2 - xx1 + 1)
        h = np.maximum(0, yy2 - yy1 + 1)
        # compute the ratio of overlap
        overlap = (w * h) / area[idxs[:last]]
        # delete all indexes from the index list that have
        idxs = np.delete(idxs, np.concatenate(([last],
                                               np.where(overlap > overlapThresh)[0])))
    # return only the bounding boxes that were picked using the
    # integer data type

    # build back frame data

    return boxes[pick].tolist()


def get_rotation_matrix(theta_z, theta_x, theta_y):
    z_theta = np.array([[np.cos(theta_z), -np.sin(theta_z), 0],
                        [np.sin(theta_z), np.cos(theta_z), 0],
                        [0, 0, 1]])

    x_theta = np.array([[1, 0, 0],
                        [0, np.cos(theta_x), -np.sin(theta_x)],
                        [0, np.sin(theta_x), np.cos(theta_x)]])

    y_theta = np.array([[np.cos(theta_y), 0, np.sin(theta_y)],
                        [0, 1, 0],
                        [-np.sin(theta_y), 0, np.cos(theta_y)]])

    rotation_matrix = z_theta @ x_theta @ y_theta
    return rotation_matrix


def apply_6DOF(pts, yaw, pitch, roll, tx, ty, tz):
    rot = get_rotation_matrix(theta_z=np.deg2rad(yaw),
                              theta_x=np.deg2rad(pitch),
                              theta_y=np.deg2rad(roll))
    # print("rot", rot.shape)
    camera_lidar_mat = np.zeros((3, 4), dtype=np.float32)
    camera_lidar_mat[:, :3] = rot
    camera_lidar_mat[:, 3] = np.array([tx,
                                       ty,
                                       tz])

    pts_velo_lidar = np.vstack((pts.T, np.ones((1, pts.shape[0]))))
    return (camera_lidar_mat @ pts_velo_lidar).T, rot


def rectify(img, K, D, fish=True, DIM=(1824, 940), balance=0):
    """Undistort camera image based on saved parameters.
    Accept image as image array (cv2 format; BGR)
    Returns undistorted image array (cv2 format).
    """
    # in case image is not cv2 format?
    # img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)

    D = D[0][:4]
    dim1 = img.shape[:2][::-1]  # dim1 is the dimension of input image to un-distort
    assert dim1[0] / dim1[1] == DIM[0] / DIM[1], "Image to undistort needs to have same aspect ratio as the ones used in calibration"
    scaled_K = K * dim1[0] / DIM[0]  # The values of K is to scale with image dimension.
    scaled_K[2][2] = 1.0  # Except that K[2][2] is always 1.0

    if fish is True:
        new_K = cv2.fisheye.estimateNewCameraMatrixForUndistortRectify(scaled_K, D, dim1, np.eye(3), balance=balance)
        new_K[0, 2] = K[0, 2]
        new_K[1, 2] = K[1, 2]  # manually set centerPrinicipalPoint=True
        map1, map2 = cv2.fisheye.initUndistortRectifyMap(scaled_K, D, np.eye(3), new_K, dim1, cv2.CV_32FC1)
    else:
        centerPrinicipalPoint = True
        new_K, roi = cv2.getOptimalNewCameraMatrix(scaled_K, D, dim1, balance, dim1, centerPrinicipalPoint)
        map1, map2 = cv2.initUndistortRectifyMap(scaled_K, D, None, new_K, dim1, cv2.CV_32FC1)

    # undistort
    undistorted_img = cv2.remap(img, map1, map2, interpolation=cv2.INTER_LINEAR, borderMode=cv2.BORDER_CONSTANT)
    return undistorted_img
