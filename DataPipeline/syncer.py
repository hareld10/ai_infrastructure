import cv2
import pandas as pd
from tqdm import tqdm
import sys
import numpy as np
from multiprocessing import Process
import codecs, json
import argparse
import time
from pathlib import Path
import sys
import pathlib
import glob
import shutil
import os
import logging

RADAR_DF = 0
CAMERA_DF = 1
LIDAR_DF = 2


class EngineSyncer:
    def __init__(self, engine, pipeline_config):
        self.pipeline_config = pipeline_config
        self.engine = engine
        self.context = self.engine.drive_context

        self.df_radar = None
        self.df_camera = None
        self.df_lidar = None
        self.df_sync = None
        self.df_depth = None

        self.df_radar_lidar = None
        self.df_radar_camera = None
        self.df_lidar_camera = None
        self.df_radar_lidar_camera = None
        return

    def run(self, df_lidar=None):
        merge_dfs = []
        if self.pipeline_config["sync"]["radar"]:
            self.df_radar = self.process_radar_df()
            merge_dfs.append(self.df_radar)
        if self.pipeline_config["sync"]["camera"]:
            self.df_camera = self.process_camera_df()
            merge_dfs.append(self.df_camera)
        if self.pipeline_config["sync"]["depth"]:
            self.df_depth = self.process_depth_df()
            merge_dfs.append(self.df_depth)
        if self.pipeline_config["sync"]["lidar"]:
            if df_lidar is None:
                if os.path.exists(self.context.lidar_csv_path):
                    df_lidar = pd.read_csv(self.context.lidar_csv_path, index_col=None)
                    logging.info("Reading dfLidar %s", len(df_lidar))
                else:
                    logging.warning("Syncer: Run: Please add df_lidar as argument")
            self.df_lidar = df_lidar
            self.df_lidar = self.df_lidar.astype({'timeInt': 'int64'})
            self.df_lidar = self.df_lidar.sort_values(by='timeInt')
            merge_dfs.append(self.df_lidar)
        return self.merge_dfs(merge_dfs)

    def process_camera_df(self):
        if os.path.exists(self.context.aux_path + '/dfCamera.csv'):
            logging.info("Reading dfCamera")
            df_camera = pd.read_csv(self.context.aux_path + '/dfCamera.csv', index_col=None)
            df_camera = df_camera.drop(columns=['platformVelocity'])
            logging.info('# of camera frames: %d', df_camera.shape[0])
        else:
            logging.info("process_camera_df: Reading from %s", self.context.cameraDataRaw_path)
            camera_csvs = [f for f in os.listdir(self.context.cameraDataRaw_path) if os.path.splitext(f)[-1] == '.csv']
            list_ = []
            for file_ in tqdm(camera_csvs):
                try:
                    df = pd.read_csv(self.context.cameraDataRaw_path + '/' + file_, index_col=None)
                    try:
                        df.__delitem__("csvVersion")
                    except:
                        pass
                    file_ = file_
                    file_ = file_.replace("_timestamp", "")
                    file_ = file_.replace(".csv", ".raw")
                    df.loc[:, 'filenameCamera'] = file_
                    df.loc[:, 'inFileIdxCamera'] = np.arange(len(df))
                    df.loc[:, 'platformVelocity'] = ""
                except:
                    continue
                list_.append(df)
            df_camera = pd.concat(list_, ignore_index=True)
            df_camera = df_camera.dropna(axis=1, how='all')
            logging.info('# of camera frames: %d', df_camera.shape[0])
            df_camera.to_csv(self.context.aux_path + '/dfCamera.csv', sep=',', index=False)

        df_camera['timeInt'] = pd.to_numeric(df_camera['timestampCamera'])
        df_camera = df_camera.sort_values(by='timestampCamera')
        df_camera = df_camera.reset_index(drop=True)
        df_camera['timestampCamera'] = df_camera['timestampCamera'].astype(str)
        return df_camera

    def process_radar_df(self,):
        df_radar_path = self.context.aux_path + '/dfRadar.csv'
        if os.path.exists(df_radar_path):
            logging.info("Reading dfRadar")
            df_radar = pd.read_csv(df_radar_path, index_col=None)
            logging.info('# of radar frames: %d', df_radar.shape[0])
        else:
            logging.info("Generating dfRadar: %d", df_radar_path)
            radar_csvs = sorted([f for f in os.listdir(self.context.radarDataRaw_path) if os.path.splitext(f)[-1] == '.csv'])
            logging.info("len(radarCSVs)= %d", len(radar_csvs))
            list_ = []
            for file_ in tqdm(radar_csvs):
                try:
                    df = pd.read_csv(self.context.radarDataRaw_path + '/' + file_, index_col=None)
                    try:
                        df.__delitem__("csvVersion")
                    except:
                        pass
                    file_ = file_.replace(".csv", ".raw")
                    df.loc[:, 'filenameRadar'] = file_
                    df.loc[:, 'inFileIdxRadar'] = np.arange(len(df))

                except Exception as e:
                    logging.error("Exception %s", str(e))
                    continue
                list_.append(df)
            df_radar = pd.concat(list_, ignore_index=True)
            df_radar = df_radar.dropna(axis=1, how='all')
            logging.info('# of radar frames: %d', df_radar.shape[0])
            df_radar.to_csv(df_radar_path, sep=',', index=False)

        df_radar['timeInt'] = df_radar['timestampRadar']
        df_radar = df_radar.sort_values(by='timestampRadar')
        df_radar = df_radar.reset_index(drop=True)
        df_radar['timestampRadar'] = df_radar['timestampRadar'].astype(str)
        return df_radar

    def process_depth_df(self):
        # depthPath = self.args.depthSrc + "/" + src + "/" + self.context.depthDataRaw_path + "/"
        depthCsv = glob.glob(self.context.depthDataRaw_path + "/*.csv")
        if len(depthCsv) != 1:
            print("depthPath ", self.context.depthDataRaw_path)
            print("Too many or no depth_sync.csv", len(depthCsv))
        depthCsv = depthCsv[0]
        #         depthCsv = depthPath + "/depth_sync.csv"
        df = pd.read_csv(depthCsv, index_col=None)
        df = df.dropna(axis=1, how='all')

        col = {"timestampDepth": "timestampCamera", "filenameDepth": "filenameCamera", "idDepth": "idCamera"}
        df = df.astype('int64')
        df = df.rename(columns=col)
        df.insert(0, 'csvVersion', ',')
        df.insert(1, 'idCamera', ',')
        df.loc[:, 'idCamera'] = np.arange(len(df))
        df.loc[:, 'platformVelocity'] = ""
        df.loc[:, 'filenameCamera'] = "rgb_depth.svo"
        df.loc[:, 'inFileIdxCamera'] = np.arange(len(df))
        df['timeInt'] = pd.to_numeric(df['timestampCamera'])
        print('# of camera(depth) frames:', df.shape[0])
        return df

    def prepare_df(self, df_sync):
        df_sync = df_sync.dropna()
        # df_sync = df_sync.drop(['timeInt'], axis=1)
        # print("Summary", df_sync.describe())
        df_sync = df_sync.rename(index=str, columns={"timestamp_x": "timestampCamera", "timestamp_y": "timestampRadar"})
        # df_sync = df_sync.rename(index=str, columns={"timestampCamera_x": "timestampCamera", "timestampRadar_y": "timestampRadar", "idRadar_x": "idRadar"})

        df_sync.timestampCamera = pd.to_numeric(df_sync.timestampCamera, errors='coerce', downcast='integer')
        df_sync.timestampRadar = pd.to_numeric(df_sync.timestampRadar, errors='coerce', downcast='integer')
        if self.df_lidar is not None:
            df_sync.timestampLidar = pd.to_numeric(df_sync.timestampLidar, errors='coerce', downcast='integer')

        # print("df_sync before drop dups", len(df_sync), df_sync.keys())
        df_sync = df_sync.drop_duplicates('timestampCamera')
        # print("df_sync before dups camera", len(df_sync), df_sync.keys())
        df_sync = df_sync.drop_duplicates('timestampRadar')
        # print("df_sync after drop dups", len(df_sync), df_sync.keys())

        df_sync = df_sync.reset_index(drop=True)
        df_sync['idRadar'] = df_sync['idRadar'].astype(np.int64)
        df_sync = df_sync.astype({'timestampCamera': 'str'})
        if self.df_lidar is not None:
            df_sync = df_sync.astype({'timestampLidar': 'str'})
        return df_sync

    def merge_dfs(self, dfs):
        radar_lidar = self.pipeline_config["sync"]["sync_threshold"]["radar_lidar"]* np.power(10, 6)
        camera_lidar = self.pipeline_config["sync"]["sync_threshold"]["camera_lidar"]* np.power(10, 6)
        camera_radar = self.pipeline_config["sync"]["sync_threshold"]["camera_radar"]* np.power(10, 6)

        logging.info("Syncer: Got %d Dataframes", len(dfs))
        logging.info("Syncer: Time sync radar_lidar=%d, camera_lidar=%d, camera_radar=%d", radar_lidar/np.power(10, 6), camera_lidar/np.power(10, 6), camera_radar/np.power(10, 6))
        if len(dfs) == 2:
            df_sync_main = pd.merge_asof(dfs[RADAR_DF], dfs[CAMERA_DF], on='timeInt', direction='nearest', tolerance=min(radar_lidar, camera_radar))
        elif len(dfs) == 3:
            logging.info("unique_camera %d", len(dfs[CAMERA_DF].timestampCamera.unique()))
            logging.info("unique_radar %d", len(dfs[RADAR_DF].timestampRadar.unique()))
            logging.info("unique_lidar %d", len(dfs[LIDAR_DF].timestampLidar.unique()))

            for idx, df in enumerate(dfs):
                logging.info("idx=%d len=%d", idx, len(df))
                dfs[idx] = dfs[idx].sort_values(by='timeInt')
            df_sync3 = pd.merge_asof(dfs[RADAR_DF], dfs[LIDAR_DF], on='timeInt', direction='nearest', tolerance=radar_lidar)
            df_sync3 = df_sync3.sort_values(by='timeInt')
            df_sync_main = pd.merge_asof(df_sync3, dfs[CAMERA_DF], on='timeInt', direction='nearest', tolerance=camera_radar)

            def merge(main1, main2, secondary, time_sync, sync_coeff=5):
                tmp_df = pd.merge_asof(dfs[main1], dfs[main2], on='timeInt', direction='nearest', tolerance=time_sync)
                tmp_df = tmp_df.sort_values(by='timeInt')
                tmp_df = pd.merge_asof(tmp_df, dfs[secondary], on='timeInt', direction='nearest', tolerance=time_sync * sync_coeff)
                tmp_df = tmp_df.dropna()
                tmp_df = tmp_df.drop(['timeInt'], axis=1)
                tmp_df = tmp_df.rename(index=str, columns={"timestamp_x": "timestampCamera", "timestamp_y": "timestampRadar"})
                tmp_df.timestampCamera = pd.to_numeric(tmp_df.timestampCamera, errors='coerce', downcast='integer')
                tmp_df.timestampRadar = pd.to_numeric(tmp_df.timestampRadar, errors='coerce', downcast='integer')
                tmp_df.timestampLidar = pd.to_numeric(tmp_df.timestampLidar, errors='coerce', downcast='integer')
                tmp_df = tmp_df.drop_duplicates('timestampCamera')
                tmp_df = tmp_df.drop_duplicates('timestampRadar')
                tmp_df = tmp_df.reset_index(drop=True)
                tmp_df['idRadar'] = tmp_df['idRadar'].astype(np.int64)
                tmp_df = tmp_df.astype({'timestampCamera': 'str'})
                tmp_df = tmp_df.astype({'timestampLidar': 'str'})
                logging.info("m1=" + str(main1) + ", m2=" + str(main2) + "second=" + str(secondary) +  ", len=" + str(len(tmp_df)) + ", sync_coeff=" + str(sync_coeff))
                return self.prepare_df(tmp_df)

            _df1 = merge(RADAR_DF, CAMERA_DF, LIDAR_DF, time_sync=camera_radar)
            _df2 = merge(RADAR_DF, LIDAR_DF, CAMERA_DF, time_sync=radar_lidar, sync_coeff=1)
            _df3 = merge(CAMERA_DF, LIDAR_DF, RADAR_DF, time_sync=camera_lidar)

            _df1.to_csv(self.context.aux_path + "/sync_data_radar_camera.csv", sep=',', index=False)
            _df2.to_csv(self.context.aux_path + "/sync_data_radar_lidar_camera.csv", sep=',', index=False)
            _df3.to_csv(self.context.aux_path + "/sync_data_camera_lidar.csv", sep=',', index=False)

            _all = pd.concat([_df1, _df2, _df3])
            logging.info("TEST: len(_all) before %d", len(_all))
            _all = _all.drop_duplicates()
            logging.info("TEST: len(_all) after %d", len(_all))

        else:
            logging.error("too many dfs")
            return

        df_sync_main = self.prepare_df(df_sync_main)
        df_sync_main.to_csv(self.context.df_path, sep=',', index=False)

        # Print stats
        logging.info('# of Synced Meas: %d %d', len(df_sync_main), len(df_sync_main.index))
        self.df_sync = df_sync_main
        logging.info("preprocess: df_data=%d", len(self.df_sync))
        return self.df_sync


# Backward compatibility
def match_time(timestamps_df, radar_df, camera_df, key='timestampCamera'):
    print("In Match Time")
    radar_arr = radar_df.loc[:, 'timestampRadar'].to_numpy(dtype='int64')[1:]

    camera_arr = camera_df.loc[:, key].to_numpy(dtype='int64')[1:]
    timestamps_arr = timestamps_df.to_numpy(dtype='int64')[1:]

    final_camera_arr = np.copy(camera_arr)
    # print("len(final_camera_arr)", len(final_camera_arr))
    # Camera Original TimeStamps
    camera_ts = camera_arr[:].astype("int64")

    for index, val in enumerate(camera_arr[:-1]):
        # Get ts to fix from radar csv
        t_s = float(val)

        # find best index from matching timestamps
        best_index = np.argmin(np.abs((timestamps_arr[:, 0] - t_s)))

        # Get Camera timestamp
        matched_radar = timestamps_arr[best_index][1]
        matched_camera = timestamps_arr[best_index][0]

        # Get the offset
        offset = matched_camera - matched_radar
        final_camera_arr[index] -= offset
    #         print(offset, best_index, camera_df.iloc[index+1,1], final_camera_arr[index])

    camera_df.loc[1:, "timeInt"] = final_camera_arr
    camera_df = camera_df.sort_values(by='timeInt')
    return camera_df

class Syncer:
    def __init__(self, args, context, time_sync=10):
        self.time_sync = 10
        self.set_time_sync(args.sync)
        # self.destination_path = args.dest
        # self.current_src_out = None
        self.context = context
        self.args = args

        self.df_radar = None
        self.df_camera = None
        self.df_lidar = None
        self.df_sync = None
        self.df_depth = None

        self.df_radar_lidar = None
        self.df_radar_camera = None
        self.df_lidar_camera = None
        self.df_radar_lidar_camera = None

        return

    def set_time_sync(self, time_sync):
        self.time_sync = time_sync * np.power(10, 6)

    def run(self, src, context, df_lidar=None):
        self.context = context
        # self.destination_path = dest_path
        # print("Syncer: Starts syncing sensors")
        # self.current_src_out = dest_path
        # print("Syncer: current_src_out", self.current_src_out)
        merge_dfs = []
        if self.args.radar or self.args.radarPc or self.args.radar_raw or self.args.radar4d or self.args.processed_delta or self.args.phase_amp:
            self.df_radar = self.process_radar_df(src)
            merge_dfs.append(self.df_radar)
        if self.args.rgb:
            self.df_camera = self.process_camera_df(src)
            merge_dfs.append(self.df_camera)
        if self.args.depth:
            self.df_depth = self.process_depth_df(src)
            merge_dfs.append(self.df_depth)
        if self.args.lidar:
            if df_lidar is None:
                print("Syncer: Run: Please add df_lidar as argument")
            self.df_lidar = df_lidar
            self.df_lidar = self.df_lidar.astype({'timeInt': 'int64'})
            self.df_lidar = self.df_lidar.sort_values(by='timeInt')
            merge_dfs.append(self.df_lidar)

        # self.df_sync = self.merge_dfs(merge_dfs, src)
        # return self.df_sync
        return self.merge_dfs(merge_dfs, src)

    def process_camera_df(self, src):
        # camera_src_dir = self.args.cameraSrc + "/" + src + "/" + self.context.cameraDataRaw_path + "/"

        if os.path.exists(self.context.aux_path + '/dfCamera.csv'):
            print("Reading dfCamera")
            df_camera = pd.read_csv(self.context.aux_path + '/dfCamera.csv', index_col=None)
            df_camera = df_camera.drop(columns=['platformVelocity'])
            print('# of camera frames:', df_camera.shape[0])
        else:
            print("process_camera_df: Reading from", self.context.cameraDataRaw_path)
            camera_csvs = [f for f in os.listdir(self.context.cameraDataRaw_path) if os.path.splitext(f)[-1] == '.csv']
            list_ = []
            for file_ in tqdm(camera_csvs):
                try:
                    df = pd.read_csv(self.context.cameraDataRaw_path + '/' + file_, index_col=None)
                    try:
                        df.__delitem__("csvVersion")
                    except:
                        pass
                    file_ = file_
                    file_ = file_.replace("_timestamp", "")
                    file_ = file_.replace(".csv", ".raw")
                    df.loc[:, 'filenameCamera'] = file_
                    df.loc[:, 'inFileIdxCamera'] = np.arange(len(df))
                    df.loc[:, 'platformVelocity'] = ""
                except Exception as e:
                    continue
                list_.append(df)
            df_camera = pd.concat(list_, ignore_index=True)
            df_camera = df_camera.dropna(axis=1, how='all')
            print('# of camera frames:', df_camera.shape[0])
            df_camera.to_csv(self.context.aux_path + '/dfCamera.csv', sep=',', index=False)

        df_camera['timeInt'] = pd.to_numeric(df_camera['timestampCamera'])
        df_camera = df_camera.sort_values(by='timestampCamera')
        df_camera = df_camera.reset_index(drop=True)
        df_camera['timestampCamera'] = df_camera['timestampCamera'].astype(str)
        return df_camera

    def process_radar_df(self, src):
        df_radar_path = self.context.aux_path + '/dfRadar.csv'
        if os.path.exists(df_radar_path):
            print("Reading dfRadar")
            df_radar = pd.read_csv(df_radar_path, index_col=None)
            print('# of radar frames:', df_radar.shape[0])
        else:
            print("Generating dfRadar,", df_radar_path)
            radar_csvs = sorted([f for f in os.listdir(self.context.radarDataRaw_path) if os.path.splitext(f)[-1] == '.csv'])
            print("len(radarCSVs) ", len(radar_csvs))
            list_ = []
            for file_ in tqdm(radar_csvs):
                try:
                    df = pd.read_csv(self.context.radarDataRaw_path + '/' + file_, index_col=None)
                    try:
                        df.__delitem__("csvVersion")
                    except:
                        pass
                    file_ = file_.replace(".csv", ".raw")
                    df.loc[:, 'filenameRadar'] = file_
                    df.loc[:, 'inFileIdxRadar'] = np.arange(len(df))

                except Exception as e:
                    print("Exception ", str(e))
                    continue
                list_.append(df)
            df_radar = pd.concat(list_, ignore_index=True)
            df_radar = df_radar.dropna(axis=1, how='all')
            print('# of radar frames:', df_radar.shape[0])
            df_radar.to_csv(df_radar_path, sep=',', index=False)

        df_radar['timeInt'] = df_radar['timestampRadar']
        df_radar = df_radar.sort_values(by='timestampRadar')
        df_radar = df_radar.reset_index(drop=True)
        df_radar['timestampRadar'] = df_radar['timestampRadar'].astype(str)
        return df_radar

    def process_depth_df(self, src):
        # depthPath = self.args.depthSrc + "/" + src + "/" + self.context.depthDataRaw_path + "/"
        depthCsv = glob.glob(self.context.depthDataRaw_path + "/*.csv")
        if len(depthCsv) != 1:
            print("depthPath ", self.context.depthDataRaw_path)
            print("Too many or no depth_sync.csv", len(depthCsv))
        depthCsv = depthCsv[0]
        #         depthCsv = depthPath + "/depth_sync.csv"
        df = pd.read_csv(depthCsv, index_col=None)
        df = df.dropna(axis=1, how='all')

        col = {"timestampDepth": "timestampCamera", "filenameDepth": "filenameCamera", "idDepth": "idCamera"}
        df = df.astype('int64')
        df = df.rename(columns=col)
        df.insert(0, 'csvVersion', ',')
        df.insert(1, 'idCamera', ',')
        df.loc[:, 'idCamera'] = np.arange(len(df))
        df.loc[:, 'platformVelocity'] = ""
        df.loc[:, 'filenameCamera'] = "rgb_depth.svo"
        df.loc[:, 'inFileIdxCamera'] = np.arange(len(df))
        df['timeInt'] = pd.to_numeric(df['timestampCamera'])
        print('# of camera(depth) frames:', df.shape[0])
        return df

    def prepare_df(self, df_sync):
        df_sync.to_csv("./test.csv")
        df_sync = df_sync.dropna()
        print("after drop na", len(df_sync))

        # df_sync = df_sync.drop(['timeInt'], axis=1)
        # print("Summary", df_sync.describe())
        df_sync = df_sync.rename(index=str, columns={"timestamp_x": "timestampCamera", "timestamp_y": "timestampRadar"})
        # df_sync = df_sync.rename(index=str, columns={"timestampCamera_x": "timestampCamera", "timestampRadar_y": "timestampRadar", "idRadar_x": "idRadar"})

        df_sync.timestampCamera = pd.to_numeric(df_sync.timestampCamera, errors='coerce', downcast='integer')
        df_sync.timestampRadar = pd.to_numeric(df_sync.timestampRadar, errors='coerce', downcast='integer')
        if self.df_lidar is not None:
            df_sync.timestampLidar = pd.to_numeric(df_sync.timestampLidar, errors='coerce', downcast='integer')

        # print("df_sync before drop dups", len(df_sync), df_sync.keys())
        df_sync = df_sync.drop_duplicates('timestampCamera')
        # print("df_sync before dups camera", len(df_sync), df_sync.keys())
        df_sync = df_sync.drop_duplicates('timestampRadar')
        # print("df_sync after drop dups", len(df_sync), df_sync.keys())

        df_sync = df_sync.reset_index(drop=True)
        df_sync['idRadar'] = df_sync['idRadar'].astype(np.int64)
        df_sync = df_sync.astype({'timestampCamera': 'str'})
        if self.df_lidar is not None:
            df_sync = df_sync.astype({'timestampLidar': 'str'})
        return df_sync

    def merge_dfs(self, dfs, src):
        print("Syncer: Got", len(dfs), "Dataframes, sync=", self.time_sync)
        for idx, df in enumerate(dfs):
            print("idx", idx, "len", len(df))
        if len(dfs) == 2:
            print(len(dfs[RADAR_DF]), len(dfs[CAMERA_DF]))
            df_sync_main = pd.merge_asof(dfs[RADAR_DF], dfs[CAMERA_DF], on='timeInt', direction='nearest', tolerance=self.time_sync)
            if len(df_sync_main.dropna()) == 0:
                print("Maybe we need hard sensor sync")
                if os.path.exists(self.context.cameraDataRaw_path + '/SensorsOffset.sync'):
                    dfOffsets = pd.read_csv(self.context.cameraDataRaw_path + '/SensorsOffset.sync', index_col=None)
                    # print(dfs[CAMERA_DF], dfs[CAMERA_DF].dtypes)
                    dfs[CAMERA_DF]['timeInt'] = pd.to_numeric(dfs[CAMERA_DF]['timestampCamera'])
                    dfs[CAMERA_DF] = match_time(dfOffsets, dfs[RADAR_DF], dfs[CAMERA_DF], key="timeInt")
                    # print(dfs[CAMERA_DF], dfs[CAMERA_DF].dtypes)

                    df_sync_main = pd.merge_asof(dfs[RADAR_DF], dfs[CAMERA_DF], on='timeInt', direction='nearest', tolerance=self.time_sync)
                    print("Now! df_sync_main= ", len(df_sync_main.dropna()))
                else:
                    print("No SensorsOffset found")
        elif len(dfs) == 3:
            print("self.time_sync", self.time_sync)
            print("unique_camera", len(dfs[CAMERA_DF].timestampCamera.unique()))
            print("unique_radar", len(dfs[RADAR_DF].timestampRadar.unique()))
            print("unique_lidar", len(dfs[LIDAR_DF].timestampLidar.unique()))
            print("-" * 30)
            df_sync3 = pd.merge_asof(dfs[RADAR_DF], dfs[LIDAR_DF], on='timeInt', direction='nearest', tolerance=self.time_sync)
            print("df_sync3-unique", len(df_sync3.timestampRadar.unique()), len(df_sync3.timestampLidar.unique()))
            print("df_sync3", len(df_sync3), df_sync3.keys())

            df_sync3 = df_sync3.sort_values(by='timeInt')
            df_sync_main = pd.merge_asof(df_sync3, dfs[CAMERA_DF], on='timeInt', direction='nearest', tolerance=self.time_sync * 5)

            def merge(main1, main2, secondary, sync_coeff=5):
                tmp_df = pd.merge_asof(dfs[main1], dfs[main2], on='timeInt', direction='nearest', tolerance=self.time_sync)
                tmp_df = tmp_df.sort_values(by='timeInt')
                tmp_df = pd.merge_asof(tmp_df, dfs[secondary], on='timeInt', direction='nearest', tolerance=self.time_sync * sync_coeff)
                tmp_df = tmp_df.dropna()
                tmp_df = tmp_df.drop(['timeInt'], axis=1)
                tmp_df = tmp_df.rename(index=str, columns={"timestamp_x": "timestampCamera", "timestamp_y": "timestampRadar"})
                tmp_df.timestampCamera = pd.to_numeric(tmp_df.timestampCamera, errors='coerce', downcast='integer')
                tmp_df.timestampRadar = pd.to_numeric(tmp_df.timestampRadar, errors='coerce', downcast='integer')
                tmp_df.timestampLidar = pd.to_numeric(tmp_df.timestampLidar, errors='coerce', downcast='integer')
                tmp_df = tmp_df.drop_duplicates('timestampCamera')
                tmp_df = tmp_df.drop_duplicates('timestampRadar')
                tmp_df = tmp_df.reset_index(drop=True)
                tmp_df['idRadar'] = tmp_df['idRadar'].astype(np.int64)
                tmp_df = tmp_df.astype({'timestampCamera': 'str'})
                tmp_df = tmp_df.astype({'timestampLidar': 'str'})
                print("m1", main1, "m2", main2, "second", secondary, "len", len(tmp_df), "sync_coeff", sync_coeff)

                return self.prepare_df(tmp_df)

            _df1 = merge(RADAR_DF, CAMERA_DF, LIDAR_DF)
            _df2 = merge(RADAR_DF, LIDAR_DF, CAMERA_DF, sync_coeff=1)
            _df3 = merge(CAMERA_DF, LIDAR_DF, RADAR_DF)

            _df1.to_csv(self.context.aux_path + "/sync_data_radar_camera.csv", sep=',', index=False)
            _df2.to_csv(self.context.aux_path + "/sync_data_radar_lidar_camera.csv", sep=',', index=False)
            _df3.to_csv(self.context.aux_path + "/sync_data_camera_lidar.csv", sep=',', index=False)

            _all = pd.concat([_df1, _df2, _df3])
            print("TEST: len(_all) before", len(_all))
            _all = _all.drop_duplicates()
            print("TEST: len(_all) after", len(_all))

        else:
            print("too many dfs")
            return

        df_sync_main = self.prepare_df(df_sync_main)
        df_sync_main.to_csv(self.context.df_path, sep=',', index=False)

        # Print stats
        print('# of Synced Meas:', len(df_sync_main), len(df_sync_main.index))
        print('Time Sync:', int(self.time_sync / 1000000), 'ms')
        self.df_sync = df_sync_main
        return self.df_sync
