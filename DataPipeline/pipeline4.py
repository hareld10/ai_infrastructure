import argparse
import gettext
import glob
import shutil
import signal
import subprocess
import sys
import time
import uuid
from datetime import datetime
from itertools import repeat
from pprint import pprint

import pandas as pd
import requests

from DataPipeline.runner import run_docker_template, resume_docker, run_docker_command
from DataPipeline.src.GetEngines import get_engines, get_index
from LabelsManipulation.associator import Associator
from LabelsManipulation.clf_utils import run_size_estimation_docker, non_max_suppression_fast
from OpenSource.WisenseEngine import WisenseEngine
from Serving.serving import Wrapper, check_running_docker_name
from LabelsManipulation.camera_lidar_fusion import CameraLidarFusion
from multiprocessing import Pool
import os
import json

from Statistics.depth_map_ import infer_road_surface, infer_road_surface_idx
from Tracking.tracking_3d import run_tracking_3d_engine, validate_uuid
from tasks import Task, WiseSDKTask, DockerTask
import numpy as np
from tqdm import tqdm
import functools
from pipeline4_utils import *
import pickle
import os

from wiseTypes.Label import Label


# python3 DataPipeline/pipeline4.py --cfg_file ~/Desktop/cfg_example_92.json
# python3 DataPipeline/pipeline4.py --cfg_file ~/Desktop/cfg_example_debug.json


class RGB(WiseSDKTask):
    def run(self):
        for engine_idx in tqdm(range(len(self.engine))):
            row = self.engine.get_row(engine_idx)
            save_path = self.engine.get_image(engine_idx, path_only=True)
            file_name = f"{self.engine.drive_context.cameraDataRaw_path}/{row['filename_camera']}"
            idx = row['in_file_idx_camera']
            print("RGB Sending", engine_idx)
            WiseSDKTask.wise_sdk_client.send(service="rgb",
                                             packet={"filename_camera": file_name, "in_file_idx_camera": idx, "save_path": save_path,
                                                     "rect": int(self.engine.rect_mode)})
            if os.path.exists(save_path):
                self.set_idx_status(engine_idx, status=1)


class GenCSV(WiseSDKTask):
    def run(self):
        self.engine.drive_context.run_dirs_creation()
        if len(self.engine) == 0:
            print("In GenCSV")
            packet = {**self.kwargs, **{"base_path": self.engine.drive_context.radar_src}}
            ret_dict = WiseSDKTask.wise_sdk_client.send(service="get_df", packet=packet)
            df = json.loads(ret_dict["df"])
            print(type(df))
            print(df)
            df = pd.DataFrame.from_dict(df)
            csv_name = f"{self.engine.drive_context.aux_path}/sync_data_{ret_dict['df_str']}.csv"
            df.to_csv(csv_name)
            df.timestamp_radar = pd.to_numeric(df.timestamp_radar, errors='coerce', downcast='integer')
            df.timestamp_camera = pd.to_numeric(df.timestamp_camera, errors='coerce', downcast='integer')
            df.timestamp_lidar = pd.to_numeric(df.timestamp_lidar, errors='coerce', downcast='integer')
            df.timestamp_gps = pd.to_numeric(df.timestamp_gps, errors='coerce', downcast='integer')

            df.to_csv(csv_name)
            self.engine.set_df(df)
        self.set_task_status(1)
        return True


class Lidar(WiseSDKTask):
    def __enter__(self):
        super(Lidar, self).__enter__()
        # save spatial calibration
        camera_msg = WiseSDKTask.wise_sdk_client.send(service="get_camera", packet={"": ""})
        camera_obj = pickle.loads(camera_msg.content)

        lidar_msg = WiseSDKTask.wise_sdk_client.send(service="get_lidar", packet={"": ""})
        lidar_obj = pickle.loads(lidar_msg.content)

        with open(self.engine.drive_context.camera_obj_path, 'wb') as handle:
            pickle.dump(camera_obj, handle, protocol=pickle.HIGHEST_PROTOCOL)

        with open(self.engine.drive_context.lidar_obj_path, 'wb') as handle:
            pickle.dump(lidar_obj, handle, protocol=pickle.HIGHEST_PROTOCOL)

        self.engine.set_camera_obj(camera_obj)
        self.engine.set_lidar_obj(lidar_obj)
        return self

    def run(self):
        for engine_idx in tqdm(range(len(self.engine))):
            row = self.engine.get_row(engine_idx)
            save_path = self.engine.get_lidar_df(engine_idx, path_only=True)
            filename = f"{self.engine.drive_context.lidar_raw_data}/{row['filename_lidar']}"
            idx = row['in_file_idx_lidar']
            WiseSDKTask.wise_sdk_client.send(service="lidar",
                                             packet={"filename": filename, "idx": idx, "save_path": save_path})
            if os.path.exists(save_path):
                self.set_idx_status(engine_idx, status=1)


class Radar(WiseSDKTask):
    wise_sdk_process = None

    def __init__(self, engine, config=None):
        super().__init__(engine)
        self.config = config
        self.calib_file = f"{self.engine.drive_context.radar_src}/cal.json"
        if MEIR_DEBUG:
            print('HARD CODED CALIBRATION')
            # self.calib_file = f"/workspace/data_backup7/76mWF_Raw/200903_lidar_drive_3_W92_M15_RF1/cal.json"
            # self.calib_file = f"/workspace/data_backup7/76mWF_Raw/200903_lidar_drive_3_W92_M15_RF1/cal.json"
            self.calib_file = "/media/amper/storage/2021/210909/cal.json"

        if self.config is None:
            self.config = {}
            # self.config = {"FINE_AZP_OVS_FACT": 4,
            #                 "FINE_ELP_OVS_FACT": 2,
            #                  "MAX_DETECTIONS": 18000}
            # 4
            # self.config = {"FINE_AZP_OVS_FACT": 4,
            #                "FINE_ELP_OVS_FACT": 2,
            #                "FINE_CFAR_GLOBAL_THR_DB": 4,
            #                "FINE_CFAR_LOCAL_THR_DB": 4,
            #                'POSTFILT_MAPPER_FLAG': 0,
            #                "MAX_DETECTIONS": 17000,
            #                "FINE_DELTA_RNG_DEP_N_FILT": 1,
            #                "FFT_CFAR_THR0_DB": 10,
            #                "FFT_CFAR_THR1_DB": 10,
            #                }
            # 3.5T
            # self.config = {"FINE_AZP_OVS_FACT": 8,
            #                "FINE_ELP_OVS_FACT": 3,
            #                "FINE_CFAR_GLOBAL_THR_DB": 4,
            #                "FINE_CFAR_LOCAL_THR_DB": 4,
            #                'POSTFILT_MAPPER_FLAG': 0,
            #                "MAX_DETECTIONS": 20000,
            #                "FINE_DELTA_RNG_DEP_N_FILT": 1,
            #                "FFT_CFAR_THR0_DB": 10,
            #                "FFT_CFAR_THR1_DB": 10,
            #                }
            pass

    def __enter__(self):
        super(Radar, self).__enter__()
        # set calibration

        if not os.path.exists(self.calib_file):
            print("calibration doesn't exists - aborting", self.calib_file)
            return False

        status = WiseSDKTask.wise_sdk_client.send(service="set_calibration", check_status=True, packet={"calib_file": self.calib_file})
        status = status and WiseSDKTask.wise_sdk_client.send(service="set_DSPparams", check_status=True, packet={"dsp_params": self.config})

        print("status", status)
        return self

    def run(self):
        # bad_frame = BadFrames(engine=engine)

        first = 1
        for engine_idx in tqdm(range(len(self.engine))):
            row = self.engine.get_row(engine_idx)
            save_path = self.engine.get_processed_delta(engine_idx, True)
            if ".csv" in save_path:
                save_path = save_path.replace(".csv", ".pkl")
            # print(save_path)
            filename = f"{self.engine.drive_context.radarDataRaw_path}/{row[self.engine.file_name_radar]}"
            if not os.path.exists(filename):
                print("run: error: doesn't exists", filename)
                continue
            idx = row[self.engine.in_file_idx_radar]
            engine_idx = int(engine_idx)
            idx = int(idx)
            WiseSDKTask.wise_sdk_client.send(service="radar",
                                             packet={"filename": filename, "idx": idx, "save_path": save_path, "engine_idx": engine_idx,
                                                     "first": first})
            first = 0
            if os.path.exists(save_path):
                self.set_idx_status(engine_idx, 1)


class RadarPCP(Radar):
    def run(self):
        status = WiseSDKTask.wise_sdk_client.send(service="radar_pcp",
                                                  packet={"base_raw_path": self.engine.drive_context.radar_src,
                                                          "suffix": "cuda_processed_delta",
                                                          "delta_save_path": self.engine.drive_context.cuda_processed_delta},
                                                  check_status=True)
        self.set_task_status(status=int(status))


class LC(DockerTask):
    # model = "3DSSD_xy_0.1_z_0_epochs_80"
    model = "base_line"

    def __init__(self, engine, port=90,
                 docker_name="size_estimation_3dssd_90",
                 script_cmd=f"python3 /workspace/AI/Harel/Lidar/3DSSD-pytorch-openPCDet/tools/infer_service.py --port 90 --gpu 0 "
                            f" --cfg_file /workspace/AI/Harel/Lidar/3DSSD-pytorch-openPCDet/tools/cfgs/kitti_models/3DSSD_openPCDet.yaml"):
        super().__init__(engine, port, docker_name, script_cmd)

    @staticmethod
    def get_args_from_name(name):
        args_json = {}
        words = name.split("_")
        args_json["config"] = "3DSSD_openPCDet"
        args_json["xy"] = float(words[words.index("xy") + 1]) if "xy" in words else 0
        args_json["z"] = float(words[words.index("z") + 1]) if "z" in words else 0
        args_json["model"] = name
        return args_json

    def __enter__(self):
        super(LC, self).__enter__()
        args_json = LC.get_args_from_name(self.model)
        print(f"setting model: {self.model}, params:")
        pprint(args_json)
        status = Task.pcdet_client.send(packet=args_json, service="set_model", check_status=True)
        return self

    def run(self):
        for engine_idx in tqdm(range(len(self.engine))):
            lidar = self.engine.get_lidar_df(engine_idx)
            # print(lidar)
            lidar[["orig_x", "orig_y", "orig_z", "r"]] = lidar[["orig_x", "orig_y", "orig_z", "intensity"]]
            to_send = lidar[["orig_x", "orig_y", "orig_z", "r"]].to_json()
            ans = self.pcdet_client.send(packet=to_send, service="predict", dump=False)

            ssd3d_path = self.engine.get_lidar_3dssd(idx=engine_idx, as_obj=False, path_only=True)
            if ans["status"] == 0:
                np.save(ssd3d_path, ans["pred_dicts"])
                self.set_idx_status(engine_idx, 1)


class YoloRTask(DockerTask):
    def __init__(self, engine, port=1025,
                 docker_name="yolor",
                 script_cmd=f"python3 /yolor/infer_service.py"):
        super().__init__(engine, port, docker_name, script_cmd)

    def run(self):
        for engine_idx in tqdm(range(len(self.engine))):
            img_path = self.engine.get_image(engine_idx, path_only=True)
            bbs = self.client.send({"img": img_path})
            yolor_path = self.engine.get_2d_label(idx=engine_idx, kind="yolor", path_only=True)
            save_obj(obj=bbs, path=yolor_path)
            if len(bbs) >= 1:
                self.set_idx_status(engine_idx, 1)


class CC(DockerTask):
    version = "v2_full"

    def __init__(self, engine, gpu=0):
        super().__init__(engine,
                         docker_name="size_estimation_77",
                         script_cmd=f"python3 /workspace/AI/Harel/Lidar/3DSSD-pytorch-openPCDet/tools/infer_service.py --port 77 --gpu {gpu}",
                         port=77)
        print("CC constructor called")
        self.alg = CameraLidarFusion(data_engine=self.engine, debug=False, config=CC.version, se_docker=77, load_docker=False)

    def run_idx(self, engine_idx):
        label_path = self.engine.get_lidar_camera_fusion_labels(engine_idx, path_only=True, as_obj=False)
        try:
            processes_labels_2d = self.alg.run_engine_idx(self.engine, engine_idx, force=True, write=False)
        except FileNotFoundError as e:
            print(e)
            return
        labels2save = [label.get_dict() for label in processes_labels_2d]
        save_obj(obj=labels2save, path=label_path)
        # print("saved", label_path)
        if engine_idx % self.plot_every == 0:
            for l_idx, p_label in enumerate(processes_labels_2d):
                save_path = f"{self.engine.drive_context.camera_to_lidar_images}/{self.engine.get_id(engine_idx)}_{l_idx}.png"
                self.alg.plot_instances(engine=self.engine, idx=engine_idx, labels_2d=[p_label], save_path=save_path)

        return engine_idx

    def run(self):
        with Pool(6) as p:
            l = len(self.engine)
            r = list(tqdm(p.imap(self.run_idx, range(l)), total=l))

        for engine_idx in range(len(self.engine)):
            label_path = self.engine.get_lidar_camera_fusion_labels(engine_idx, path_only=True, as_obj=False)
            if os.path.exists(label_path):
                self.set_idx_status(engine_idx, 1)


class SegFormer(Task):
    client = Wrapper(port=1024)
    script_path = f"{HOME}/AI/Harel/Segmentation/SegFormer/run_service.sh"

    def __enter__(self):
        self.process = subprocess.Popen(f"bash {SegFormer.script_path}", shell=True)
        time.sleep(40)
        return self

    def single(self, im_path, label_path_soft, label_path_hard):
        packet = {"img": im_path,
                  "save_soft": label_path_soft,
                  "save_hard": label_path_hard,
                  }
        to_send = json.dumps(packet, cls=NumpyEncoder)
        ret = requests.post(f'http://localhost:1024/predict_and_save', json=to_send)
        try:
            if ret.json()["status"] != 0:
                print("some error happened")
        except Exception as e:
            print("Failed to read json")

    def run(self):
        for engine_idx in range(len(self.engine)):
            label_path_soft = f"{self.engine.drive_context.camera_labels}/camera_segformer_soft/camera_segformer_soft_{self.engine.get_camera_id(engine_idx)}.npy"
            label_path_hard = f"{self.engine.drive_context.camera_labels}/camera_segformer_hard/camera_segformer_hard_{self.engine.get_camera_id(engine_idx)}.npy"

            os.makedirs(os.path.split(label_path_soft)[0], exist_ok=True)
            os.makedirs(os.path.split(label_path_hard)[0], exist_ok=True)

            im_path = self.engine.get_image(engine_idx, path_only=True)
            if engine_idx % 1000 == 0:
                print(engine_idx, im_path)

            self.single(im_path, label_path_soft, label_path_hard)

            if os.path.exists(label_path_soft) and os.path.exists(label_path_hard):
                self.set_idx_status(engine_idx, 1)


class EfficientDet(Task):
    script_path = f"{HOME}/AI/Harel/AI_Infrastructure/DataPipeline/DNNs/wrappers/edet_service_run.sh"

    def __enter__(self):
        self.wrapper = Wrapper(port="1027")
        self.process = subprocess.Popen(f"bash {EfficientDet.script_path}", shell=True)
        time.sleep(20)
        return self

    def run(self):
        for engine_idx in tqdm(range(len(self.engine))):
            img_path = self.engine.get_image(engine_idx, path_only=True)
            bbs = self.wrapper.send({"img": img_path})
            edet_path = self.engine.get_2d_label(idx=engine_idx, kind="edet", path_only=True)
            save_obj(obj=bbs, path=edet_path)
            # if len(bbs) >= 1:
            if os.path.exists(edet_path):
                self.set_idx_status(engine_idx, 1)

    def __exit__(self, exc_type, exc_val, exc_tb):
        pid = self.wrapper.send({"": ""}, service="get_pid")["pid"]
        print("GOT PID", pid)
        os.kill(pid, signal.SIGKILL)
        super(EfficientDet, self).__exit__()


class EnsembleModel:
    def __init__(self, name, score):
        self.name = name
        self.score = score


class Ensemble2D(Task):
    def __init__(self, engine):
        super().__init__(engine)
        self.model1 = EnsembleModel("yolor", 0.6)
        self.model2 = EnsembleModel("edet", 0.6)
        self.iou = 0.2
        self.nms = 0.75

    def run(self):
        for engine_idx in tqdm(range(len(self.engine))):
            label_path = self.engine.get_2d_label(idx=engine_idx, kind="ensemble", path_only=True)

            model1 = self.engine.get_2d_label(idx=engine_idx, kind=self.model1.name, force_obj=True)
            model2 = self.engine.get_2d_label(idx=engine_idx, kind=self.model2.name, force_obj=True)

            if model1 == -2 or model2 == -2:
                print("ensemble some labels missing, can't perform ensemble", model1, model2)
                continue

            finals = []
            [finals.append(x) for x in model1 if x.score >= self.model1.score]
            [finals.append(x) for x in model2 if x.score >= self.model1.score]

            for x in model1:
                for y in model2:
                    iou = Label.get_2d_iou(x, y)
                    if iou > self.iou:
                        x.score = min((x.score + y.score) / 2, 1)
                        finals.append(x)
                        break

            bboxes = np.array([x.get_bev_bbox() for x in finals])

            indices = np.array(non_max_suppression_fast(bboxes, self.nms))
            finals = np.asarray(finals)
            labels2save = []
            if len(indices) > 0:
                labels2save = [x.get_dict() for x in finals[indices]]

            save_obj(obj=labels2save, path=label_path)
            self.set_idx_status(engine_idx, 1)


class FilterInputs(Task):
    def run(self):
        if self.engine.start_idx != 0:
            print("Don't filter when you sample")
            return
        attrs = ["get_image", "get_processed_delta", "get_lidar_df"]
        indices_to_remove = []

        for engine_idx in tqdm(range(len(self.engine))):
            for a in attrs:
                caller = getattr(self.engine, a)
                path = caller(engine_idx, path_only=True)
                if not os.path.exists(path):
                    indices_to_remove.append(engine_idx)

        indices_to_remove = set(indices_to_remove)
        print("+", len(self.engine.df))
        new_df = self.engine.df.copy().drop(indices_to_remove)
        print("-", len(new_df))
        new_df.reset_index(drop=True).to_csv(f"{self.engine.drive_context.aux_path}/filtered_{self.engine.df_name}")
        self.set_task_status(1)


class Copy(Task):
    def run(self):
        fs = FreeSpace(engine=self.engine, index_callback=get_index)
        fs.copy_engine()
        self.set_task_status(1)


class Register(Task):
    def run(self):
        if "nan" not in self.engine.drive_context.base_destination:
            print(self.engine, "Already registered")
            self.set_task_status(1)
            return

        fs = FreeSpace(engine=self.engine, index_callback=get_index)
        fs.register_engine()
        self.set_task_status(1)


class Ensemble3D(Task):
    def __init__(self, engine,
                 ssd3d_thresh=0.8,
                 clf_thresh=0.4,
                 ssn_thresh=0.1,
                 mid_level_thresh=0.8,
                 iou_thresh=0.25,
                 nms_thresh=0.7,
                 ssn_drop_thresh=0,
                 ssd_drop_thresh=0,
                 ):
        super().__init__(engine)
        self.ssd3d_thresh = ssd3d_thresh
        self.clf_thresh = clf_thresh
        self.ssn_thresh = ssn_thresh
        self.ssn_drop_thresh = ssn_drop_thresh
        self.ssd_drop_thresh = ssd_drop_thresh
        self.mid_level_thresh = mid_level_thresh
        self.iou_thresh = iou_thresh
        self.nms_thresh = nms_thresh

    def get_name(self):
        return f"ensemble_3d_ssd_ssn_clf_{self.ssd3d_thresh}_" \
               f"{self.clf_thresh}_{self.ssn_thresh}_{self.mid_level_thresh}_{self.iou_thresh}" \
               f"_{self.nms_thresh}_{self.ssn_drop_thresh}_{self.ssd_drop_thresh}"

    class EnsembleLabels:
        @staticmethod
        def run(m1, m2, c1, c2, iou_thresh, nms_thresh):
            finals = []
            [finals.append(x) for x in m1 if x.score >= c1]
            [finals.append(x) for x in m2 if x.score >= c2]

            for x in m1:
                for y in m2:
                    iou = Label.get_2d_iou(x, y)
                    if iou > iou_thresh:
                        x.score = x.score * max(1, (x.score + y.score) / 2)
                        finals.append(x)

            bboxes = np.array([x.get_bev_bbox() for x in finals])
            indices = np.array(non_max_suppression_fast(bboxes, nms_thresh))
            finals = np.asarray(finals)

            if len(indices) > 0:
                finals = finals[indices]
            return finals

    def run_engine_idx(self, engine, engine_idx, **kwargs):
        clf = self.engine.get_lidar_camera_fusion_labels(engine_idx, True)

        for x in clf:
            x.annotate = "clf"
            x.write_data["ensemble_src"] = "clf"

        ssd3d = self.engine.get_lidar_3dssd(engine_idx, True)
        ssd3d = [x for x in ssd3d if x.score > self.ssd_drop_thresh]
        for x in ssd3d:
            x.annotate = "ssd"
            x.write_data["ensemble_src"] = "ssd"

        if clf is None or ssd3d is None:
            return []

        clf_to_ensemble = [x for x in clf if x.cls == "car"]
        clf_to_not_ensemble = [x for x in clf if x.cls != "car"]

        mid_level = Ensemble3D.EnsembleLabels.run(ssd3d, clf_to_ensemble, self.ssd3d_thresh,
                                                  self.clf_thresh, self.iou_thresh, self.nms_thresh)
        mid_level = mid_level.tolist() + clf_to_not_ensemble

        # 2nd stage
        ssn = self.engine.get_lidar_ssn(engine_idx, True)
        ssn = [x for x in ssn if x.score > self.ssn_drop_thresh]

        for x in ssn:
            x.annotate = "ssn"
            x.write_data["ensemble_src"] = "ssn"

        finals = []
        for cls in set([x.cls for x in mid_level] + [x.cls for x in ssn]):
            # m1 = []
            # if cls != "large_vehicle":  # do not include large_veh in clf
            m1 = [x for x in mid_level if x.cls == cls]
            m2 = [x for x in ssn if x.cls == cls]
            finals += Ensemble3D.EnsembleLabels.run(m1, m2, self.mid_level_thresh, self.ssn_thresh, self.iou_thresh,
                                                    self.nms_thresh).tolist()

        bboxes = np.array([x.get_bev_bbox() for x in finals])
        indices = np.array(non_max_suppression_fast(bboxes, 0.4))
        finals = np.asarray(finals)
        if len(indices) > 0:
            finals = finals[indices]

        return finals

    def run(self):
        for engine_idx in tqdm(range(len(self.engine))):
            label_path = self.engine.get_ensemble_3d(idx=engine_idx, as_obj=False, path_only=True)
            os.makedirs(os.path.split(label_path)[0], exist_ok=True)
            finals = self.run_engine_idx(self.engine, engine_idx)
            labels2save = [x.get_dict() for x in finals]
            save_obj(obj=labels2save, path=label_path)
            self.set_idx_status(engine_idx, 1)


class SSN(DockerTask):
    def __init__(self, engine):
        super().__init__(engine, port=1028, docker_name="mmdetection3d", script_cmd="python3 /mmdetection3d/demo/infer_service.py")
        self.wait = 40

    def run(self):
        for idx in tqdm(range(len(self.engine))):
            load_path = self.engine.get_lidar_df(idx, path_only=True)
            save_path = self.engine.get_lidar_ssn(idx, path_only=True, as_obj=True)
            self.client.send(packet={"load_path": load_path, "save_path": save_path})
            # validate_uuid(self.engine, idx)
            if os.path.exists(save_path):
                self.set_idx_status(idx, 1)
            else:
                print("SSN engine id", idx, "seem to fail\nload_path: ", load_path, "\nsave_path: ", save_path)


class RoadSurface(Task):
    def run(self):
        with Pool(10) as p:
            l = len(self.engine)
            # r = list(tqdm(p.imap(infer_road_surface_idx, range(l)), total=l))
            r = tqdm(p.starmap(infer_road_surface_idx, zip(repeat(self.engine), range(l), repeat(150))), total=l)
            # p.starmap(infer_road_surface_idx,)
        for engine_idx in range(len(self.engine)):
            label_path = self.engine.get_road_surface(engine_idx, path_only=True)
            if os.path.exists(label_path):
                self.set_idx_status(engine_idx, 1)

        # infer_road_surface(engine=self.engine, force=True, plot_every=100, mark_done=self)


class Associate(Task):
    def run(self):
        ass = Associator(self.engine)
        for engine_idx in tqdm(range(len(self.engine))):
            processed_delta = self.engine.get_processed_delta(engine_idx)
            delta_clusters = self.engine.get_clusters(p_delta=processed_delta)
            labels_3d = self.engine.get_ensemble_3d(engine_idx, True)
            label_path = self.engine.get_ensemble_3d(engine_idx, path_only=True, as_obj=True)
            [ass.associate(x, delta_clusters) for x in labels_3d]
            labels2save = [x.get_dict() for x in labels_3d]
            save_obj(obj=labels2save, path=label_path)
            self.set_idx_status(engine_idx, 1)


class GenerateBursts(Task):
    def run(self):
        pass


# todo
class GPS(WiseSDKTask):
    def run(self):
        for engine_idx in tqdm(range(len(self.engine))):
            row = self.engine.get_row(engine_idx)
            save_path = self.engine.get_gps(engine_idx, path_only=True)
            file_name = f"{self.engine.drive_context.gpsDataRaw_path}/{row['filename_gps']}"
            idx = row['in_file_idx_gps']
            WiseSDKTask.wise_sdk_client.send(service="gps",
                                             packet={"filename_gps": file_name, "in_file_idx_gps": idx, "save_path": save_path})
            if os.path.exists(save_path):
                self.set_idx_status(engine_idx, status=1, every=10000)


# todo
class Tracking2D(Task):
    def __init__(self, engine):
        super().__init__(engine)
        self.kinds = ["yolor", "edet", "ensemble"]
        # self.kinds = ['original']

    def run(self):
        # add uuids
        print("Adding UUID's")
        gen_uuids = 0
        if gen_uuids:
            for kind in self.kinds:
                for engine_idx in tqdm(range(len(self.engine))):
                    labels = self.engine.get_2d_label(idx=engine_idx, kind=kind, force_obj=False)
                    if labels == -2:
                        continue
                    label_path = self.engine.get_2d_label(idx=engine_idx, kind=kind, path_only=True)
                    for label in labels:
                        label["uuid"] = str(uuid.uuid4())
                    with open(label_path, 'w') as fp:
                        json.dump(labels, fp, indent=2)

        for kind in self.kinds:
            d = dict(drive_str=self.engine.get_drive_name(), rect=self.engine.rect_mode, df=self.engine.df_name, detection_kind=kind)
            args = " ".join([f"--{k} {v}" for k, v in d.items()])
            print(args)

            process = subprocess.call(
                f"python3 /home/amper/AI/Meir/radar_perception/external_repos/deep_sort_pytorch/generate_track_df.py " + args, shell=True)


# todo
class Tracking3D(Task):
    def __init__(self, engine):
        super().__init__(engine)
        self.kinds = ["ssn", "3dssd", "cc", "ensemble"]

    def run(self):
        run_tracking_3d_engine(self.engine)


# todo
class PhaseAmpCalibration(Task):
    pass


# todo
class SpatialCalibration(Task):
    pass


class YoloV3Task(Task):
    def __init__(self, engine):
        super().__init__(engine)
        from DataPipeline.DNNs.wrappers.yoloWrapper import YoloWrapper
        self.yolo_wrapper = YoloWrapper()

    def run(self):
        for engine_idx in tqdm(range(len(self.engine))):
            im = self.engine.get_image(engine_idx)
            bbs = self.yolo_wrapper.infer_im(im)
            yolor_path = self.engine.get_2d_label(idx=engine_idx, kind="yolo", path_only=True)
            bbs = [x.get_dict() for x in bbs]
            save_obj(obj=bbs, path=yolor_path)
            if os.path.exists(yolor_path):
                self.set_idx_status(engine_idx, 1)


class VersionManager:
    runs_base = "/workspace/HDD/Harel/pipeline_runs/"
    run_id = "run_id"

    def __init__(self, args):
        self.restart = args.restart
        self.cfg_file = args.cfg_file
        self.run_id = args.run_id
        self.run_base, self.tasks_base = None, None

        if self.run_id:  # Load run id
            self.set_run_base()
            self.cfg_file = f"{self.run_base}/run_config.json"
            self.load_run_id()
        else:
            self.cfg = load_json(self.cfg_file)
            # Assign unique df names per task
            idx = 0
            for run in self.cfg["runs"]:
                for task, task_params in run["tasks"].items():
                    task_params["name"] = f"{task}_{idx}"
                    idx += 1

            self.set_run_id()

        print("Run ID=", self.run_id)
        print("Run Base=", self.run_base)

    def load_run_id(self):
        self.cfg = load_json(self.cfg_file)

    def set_run_base(self):
        if self.run_id is not None:
            self.run_base = f"{VersionManager.runs_base}/{self.run_id}"
            os.makedirs(self.run_base, exist_ok=True)
            self.tasks_base = f"{self.run_base}/engines/"
            os.makedirs(self.tasks_base, exist_ok=True)

    def set_run_id(self):
        cur_runs = len(glob.glob(f"{VersionManager.runs_base}/{VersionManager.date_name()}*"))
        cur_runs += 1
        self.run_id = f"{VersionManager.date_name()}_{cur_runs}"
        self.cfg[VersionManager.run_id] = self.run_id
        self.set_run_base()
        save_json(obj=self.cfg, path=f"{self.run_base}/run_config.json")

    @staticmethod
    def date_name():
        now = datetime.now()
        data_dir = str(now.year)[-2:] + "{0:0=2d}".format(now.month) + "{0:0=2d}".format(now.day)
        return data_dir

    def load_engines(self):
        kwargs = self.cfg["get_engines"]
        engines = get_engines(**kwargs)
        for engine in engines:
            engine.drive_context.radarDataRaw_path = engine.drive_context.radarDataRaw_path.replace('//workspace/data_backup10/wise4_raw//',
                                                                                                    '/media/amper/storage/2021/210909/')
            print(engine.drive_context.radarDataRaw_path)

            print(engine.drive_context.cuda_processed_delta)
        if MEIR_DEBUG:
            for engine in engines:
                # engine.drive_context.cuda_processed_delta = engine.drive_context.cuda_processed_delta.replace('data_backup7',
                #                                                                                           'data_backup10')
                engine.drive_context.radarDataRaw_path = engine.drive_context.radarDataRaw_path.replace(
                    '//workspace/data_backup10/wise4_raw//', '/media/amper/storage/2021/210909/')

        for func, attrs in self.cfg["sample"].items():
            for engine in engines:
                caller = getattr(engine, func)
                caller(attrs)
        return engines

    def check_task_run(self, engine: WisenseEngine, task):
        df_path = f"{self.tasks_base}/{engine.get_drive_name()}.csv"
        task.kwargs["df_path"] = df_path

        if not os.path.exists(df_path):
            engine_df = engine.df.copy()
            engine_df.to_csv(df_path)
        else:
            engine_df = pd.read_csv(df_path, dtype={
                "timestamp_camera": "str",
                "timestamp_radar": "str",
                "timestamp_lidar": "str",
                "timestamp_gps": "str",
            }, index_col=0)
            engine_df = engine_df.loc[:, ~engine_df.columns.str.contains('^Unnamed')]
            engine_df = engine_df.reset_index(drop=True)

        if task["name"] not in engine_df.keys():
            engine_df[task["name"]] = 0

        num_left = (engine_df.loc[:, task["name"]] == 0).sum()
        print(task, type(task))
        if "GenCSV" in str(type(task)):
            print("\nIt's indeed isinstance!\n")
            return engine_df
        if num_left >= 20 or len(engine_df) < 20:
            print("Task is not done", num_left, "frames are left")
            return engine_df
        else:
            print("Task is done", num_left, "frames are left")
            return 1


class Wise4Pipeline:
    @staticmethod
    def run_version(ver_manager: VersionManager):
        engines = ver_manager.load_engines()
        print("Got engines:", engines)
        for engine in engines:
            engine.create_dirs()
            # print("\n\nsetting range 0, 20\n\n")
            # engine.update_df_range(0, 20)
            for cfg in ver_manager.cfg["runs"]:
                Wise4Pipeline.run_engine_cfg(engine, cfg, ver_manager)

    @staticmethod
    def run_engine_cfg(engine: WisenseEngine, cfg, ver_manager):
        if "W92" not in engine.get_drive_name():
            engine.set_wise4_keys()
        # engine.drive_context.set_rect_paths(cfg["rect"])
        engine.set_rect_mode(mode=cfg["rect"])
        tasks_str = cfg["tasks"]

        tasks = []
        for task_str, kwargs in tasks_str.items():
            mod = __import__('DataPipeline.pipeline4', fromlist=[task_str])
            klass = getattr(mod, task_str)
            engine_task = klass(engine=engine)
            engine_task.set_kwargs(kwargs)
            tasks.append(engine_task)

        print(tasks)
        for task in tasks:
            res = ver_manager.check_task_run(engine, task)
            if type(res) == int and res == 1:
                continue
            engine.set_df(res)
            print("len(engine)", len(engine))
            with task as t:
                t.run()

    @staticmethod
    def run_tasks(tasks, engine):
        for task in tasks:
            task_obj = task(engine=engine)
            with task_obj as t:
                t.run()

    @staticmethod
    def run_engine(engine: WisenseEngine):
        engine.set_wise4_keys()

        engine.drive_context.set_rect_paths(False)
        tasks = [
            # Register,
            # Copy,
            # PhaseAmpCalibration,
            # SpatialCalibration,
            # GenCSV,
            # RGB,
            # Lidar,
            # Radar,
            # RadarPCP,
            # FilterInputs,
            # YoloRTask,
            # SegFormer,
            # EfficientDet,
            # Ensemble2D,
        ]
        Wise4Pipeline.run_tasks(tasks=tasks, engine=engine)

        engine.drive_context.set_rect_paths(True)
        task_rect = [
            # RGB,
            # YoloRTask,
            # EfficientDet,
            # Ensemble2D,
            # LC,
            # CC,
            Ensemble3D,
            # SegFormer,
            # RoadSurface,
            # GenerateBursts
        ]
        Wise4Pipeline.run_tasks(tasks=task_rect, engine=engine)


MEIR_DEBUG = False


def main():
    parser = argparse.ArgumentParser(description='Evaluation arguments')
    parser.add_argument('--restart', action='store_true', default=None, help="path to specific dataframe")
    parser.add_argument('--cfg_file', type=str, required=False, default=None, help='specify a ckpt directory to be evaluated if needed')
    parser.add_argument('--run_id', type=str, required=False, default=None, help='specify a ckpt directory to be evaluated if needed')

    args = parser.parse_args()

    version_manager = VersionManager(args)
    p = Wise4Pipeline()
    p.run_version(version_manager)

    if 0:
        p = Wise4Pipeline()
        # engines = get_engines(drive_str="2_Urban_GT_WF_198", df="sync_data_10_20_50.csv", rect=False, envelope_active=False)
        # engines = get_engines(drive_str="3_Highway_GT_WF_198", df="sync_data_10_20_50.csv", rect=True, envelope_active=False)
        engines = get_engines(wf="198", df="sync_data_lr10_cr10_lc10_gr50.csv", rect=True, drive_str="210720")

        for engine in engines:
            # engine.sample_df(30)
            p.run_engine(engine=engine)

    # 3.5T
    # engines = get_engines(wf="92", val_train="val", rect=True, randomize=False)
    # for engine in engines:
    #     engine.drive_context.cuda_processed_delta = f"{engine.drive_context.cuda_processed_delta[:-1]}_with_meta/"
    #     os.makedirs(engine.drive_context.cuda_processed_delta, exist_ok=True)
    #     print(engine.drive_context.cuda_processed_delta)
    #     p.run_engine(engine=engine)


if __name__ == '__main__':
    main()
