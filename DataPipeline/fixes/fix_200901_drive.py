import ctypes
import json
import linecache
import os
import pathlib
import sys
import glob
import traceback
from datetime import datetime
import shutil
from tqdm import tqdm
import pandas as pd
import numpy as np
from tqdm import tqdm
module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, module_path + "/../")

# import WiseSDK

def db(array):
    return 10 * np.log10(array)


def load_json(file_name):
    with open(file_name, "r") as fp:
        js = json.load(fp)
    return js


def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    tb = traceback.format_exc()
    print(tb)
    print('EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj))

axis_params = [
    [
      0.0,
      76.59697723388672,
      512
    ],
    [
      -0.6468479633331299,
      0.6468479633331299,
      61
    ],
    [
      -0.28748756647109985,
      0.28748762607574463,
      5
    ],
    [
      -13.553004264831543,
      13.553004264831543,
      384
    ]]

WiseSDK.modulesInit()

base_dir = "/workspace/HDD/new_lidar_drives/drives/"
for directory in os.listdir(base_dir):
    dir_to_process =base_dir + "/" + directory + "/"
    print("processing ", dir_to_process)

    if not("200901_lidar_drive_2_W92_M15_RF1" in dir_to_process):
        continue

    df_path = dir_to_process + "/aux/sync_data_radar_lidar.csv"
    df = pd.read_csv(df_path, index_col=False)

    indices_to_remove = []

    for idx in tqdm(range(30)):
    # for idx in tqdm(range(len(df))):
        try:
            radarFilename = df.loc[idx, 'filenameRadar']
            radarFrameIdx = ctypes.c_uint(int(df.loc[idx, 'inFileIdxRadar'])).value

            to_del = "/workspace/data_backup5/250mWF/" + directory + "/radarRawData/"
            radarPath = str( to_del + radarFilename)
            if not os.path.exists(radarPath):
                print("warning - frame path doesn't exist")
                print("Cur Path: ", radarPath, " idx ", radarFrameIdx, "size", Path(radarPath).stat().st_size)

            frame = WiseSDK.getRadarFrame(radarPath, radarFrameIdx)

            mode = frame.getHeader().mode_id
            if mode == 15:
                continue
            print("mode", mode, "wf", frame.getHeader().waveform_id)
            # with open(dir_to_process + "/metadata/metadata_cuda_4dfft_" + str(df.loc[idx, "timestampRadar"]) + ".json") as f:
            #     meta_data_4dfft = json.load(f)
            # with open(dir_to_process + "/metadata/metadata_cuda_delta_" + str(df.loc[idx, "timestampRadar"]) + ".json") as f:
            #     meta_data_delta = json.load(f)

            # if meta_data_4dfft["axis_params"][1][2] == 25 and meta_data_4dfft["axis_params"][3][2] == 480:
            #     print("Found!")

            indices_to_remove.append(idx)
            # filenameRadar
            to_del = "/workspace/data_backup5/250mWF/" + directory + "/radarRawData/" + str(df.loc[idx, "filenameRadar"]).replace(".raw", ".csv")
            to_del_camera = "/workspace/data_backup5/250mWF/" + directory + "/cameraRawData/" + str(df.loc[idx, "filenameCamera"]).replace(".raw", ".csv")
            if os.path.exists(to_del) and os.path.exists(to_del_camera):
                # os.remove(to_del)
                # os.remove(to_del_camera)
                print("File exists")

        except Exception as e:
            PrintException()
            break
            continue

    df = df.drop(indices_to_remove)
    # df.to_csv(df_path, index=False)
    print(indices_to_remove)
    print(len(indices_to_remove), len(df))