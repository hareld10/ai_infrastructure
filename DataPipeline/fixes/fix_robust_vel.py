import json
import linecache
import os
import sys
import glob
import traceback
from datetime import datetime
import shutil

import cv2
from tqdm import tqdm
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from DataPipeline.settings import Context


def db(array):
    return 10 * np.log10(array)


def load_json(file_name):
    with open(file_name, "r") as fp:
        js = json.load(fp)
    return js


def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    tb = traceback.format_exc()
    print(tb)
    print('EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj))

axis_params = [
    [
      0.0,
      76.59697723388672,
      512
    ],
    [
      -0.6468479633331299,
      0.6468479633331299,
      61
    ],
    [
      -0.28748756647109985,
      0.28748762607574463,
      5
    ],
    [
      -13.553004264831543,
      13.553004264831543,
      384
    ]]

for directory in os.listdir(sys.argv[1]):
    dir_to_process = sys.argv[1] + "/" + directory + "/"
    print("processing ", dir_to_process)

    if not("200903_lidar_drive_1_W92_M15_RF1" in dir_to_process):
        continue

    df_path = dir_to_process + "/aux/sync_data_radar_lidar.csv"
    df = pd.read_csv(df_path, index_col=False)
    print(len(df))
    df = df.sample(10).reset_index(drop=True)

    context = Context(args=None, base_path=dir_to_process, create_dirs=False)
    for idx in range(len(df)):
        plt.close()
        plt.figure(figsize=(20, 10))
        try:
            with open(dir_to_process + "/metadata/metadata_cuda_4dfft_" + str(df.loc[idx, "timestampRadar"]) + ".json") as f:
                meta_data_4dfft = json.load(f)
            with open(dir_to_process + "/metadata/metadata_cuda_delta_" + str(df.loc[idx, "timestampRadar"]) + ".json") as f:
                meta_data_delta = json.load(f)
            radar_4dfft_path = pd.read_csv(context.cuda_4dfft + "/cuda_4dfft_" + str(df.loc[idx, "timestampRadar"]) + ".csv", index_col=False)
            radar_delta_pc_path = pd.read_csv(context.cuda_delta + "/cuda_delta_" + str(df.loc[idx, "timestampRadar"]) + ".csv", index_col=False)
            rgb_path = context.camera_rgb + "/camera_rgb_" + str(df.loc[idx, 'timestampCamera']) + ".png"

            print(radar_delta_pc_path.keys())
            ax = plt.subplot(1, 4, 2)
            n, bins, patches = ax.hist(radar_4dfft_path["robust_dop"], bins=150, density=True, alpha=0.75, color='r', label='delta')
            n, bins, patches = ax.hist(radar_delta_pc_path["robust_dop"], bins=150, density=True, alpha=0.75, color='b',label='4dfft')
            ax.legend()

            ax = plt.subplot(1, 4, 3)
            scat_size = 1
            scat = ax.scatter(radar_4dfft_path['x'], radar_4dfft_path['y'], c=radar_4dfft_path['robust_dop'], cmap='jet', s=scat_size)
            plt.colorbar(scat, ax=ax, fraction=0.025, pad=0.06)

            ax = plt.subplot(1, 4, 4)
            scat = ax.scatter(radar_delta_pc_path['x'], radar_delta_pc_path['y'], c=radar_delta_pc_path['robust_dop']+meta_data_4dfft["doppler"]["max"]/2, cmap='jet', s=scat_size)
            plt.colorbar(scat, ax=ax, fraction=0.025, pad=0.06)
            ax.legend()

            ax = plt.subplot(1, 4, 1)
            im = cv2.cvtColor(cv2.imread(rgb_path), cv2.COLOR_BGR2RGB)
            ax.imshow(im)
            plt.show()

        except Exception as e:
            PrintException()
            continue

