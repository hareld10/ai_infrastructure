import json
import linecache
import os
import sys
import glob
import traceback
from datetime import datetime

from tqdm import tqdm
import pandas as pd
import numpy as np

def db(array):
    return 10 * np.log10(array)


def load_json(file_name):
    with open(file_name, "r") as fp:
        js = json.load(fp)
    return js


def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    tb = traceback.format_exc()
    print(tb)
    print('EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj))

axis_params = [
    [
      0.0,
      76.59697723388672,
      512
    ],
    [
      -0.6468479633331299,
      0.6468479633331299,
      61
    ],
    [
      -0.28748756647109985,
      0.28748762607574463,
      5
    ],
    [
      -13.553004264831543,
      13.553004264831543,
      384
    ]]

now = datetime.now()
today8am = now.replace(hour=15, minute=0, second=0, microsecond=0)
print("Today8AM", today8am)

for directory in os.listdir(sys.argv[1]):
    dir_to_process = sys.argv[1] + "/" + directory + "/"
    print("processing ", dir_to_process)

    if not("urban" in dir_to_process or "highway" in dir_to_process):
        continue
    for pc_path in tqdm(sorted(glob.glob(dir_to_process + "/radar_data/*/*.csv"))):
        try:
            modified_time = os.path.getmtime(pc_path)
            if modified_time > today8am.timestamp():
                print("NO NEED TO EDIT")
                continue

            pc = pd.read_csv(pc_path, index_col=False)
            # print("pc-path mean noise", np.mean(pc["noise"]))
            pc["noise"] = np.power(10, pc["noise"]/10)
            pc.to_csv(pc_path, index=False)
        except Exception as e:
            PrintException()
            continue

    for pc_path in np.random.permutation(glob.glob(dir_to_process + "/radar_data/*/*.csv"))[:20]:
        pc = pd.read_csv(pc_path, index_col=False)
        print("pc-path mean noise", np.mean(pc["noise"]))

    # dop_bin = (axis_params[3][1] - axis_params[3][0]) / (axis_params[3][2] - 1)
    #             robust_dop_indices = np.round((pc["robust_dop"].values - axis_params[3][0]) / dop_bin).astype(np.int)
    #             pc["robust_dop_indices"] = robust_dop_indices
    #             pc["total_power"] = db(pc["value_real"].values ** 2 + pc["power_im"].values ** 2)
        #     metadata = load_json(m_data)
    #     headers = ["range", "az", "el", "doppler"]
    #     for i, name in enumerate(headers):
    #         ax = metadata["axis_params"]
    #         metadata[name] = {}
    #         metadata[name]["min"] = ax[i][0]
    #         metadata[name]["max"] = ax[i][1]
    #         metadata[name]["num_bins"] = ax[i][2]
    #         metadata[name]["bin"] = (ax[i][1] - ax[i][0]) / (ax[i][2] - 1)
    #
    #     with open(m_data, 'w') as fp:
    #         json.dump(metadata, fp, sort_keys=True, indent=2)
