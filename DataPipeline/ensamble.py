from pprint import pprint

import numpy as np
import os
from tqdm import tqdm
import glob
import sys
import json
import os
import sys
import time
import datetime
import argparse
import matplotlib.pyplot as plt
import pandas as pd
from tqdm import tqdm
from PIL import Image
import cv2
import torch
import matplotlib

from OpenSource.WisenseEngine import WisenseEngine
from wiseTypes.Label2D import Label2D

font = {
    'weight': 'bold',
    'size': 28}

matplotlib.rc('font', **font)


class PedContext:
    relevant_class = {"person": True, "pedestrian": True}
    final_class_name = "pedestrian"
    nms_thresh = 0.8
    yolo_thresh = 0.8
    d7_thresh = 0.6
    iou_thresh = 0.5


class MotorcycleContext:
    relevant_class = {"motorcycle": True, "motorbike": True}
    final_class_name = "motorcycle"
    nms_thresh = 0.8
    yolo_thresh = 0.7
    d7_thresh = 0.5
    iou_thresh = 0.5


class VehicleContext:
    relevant_class = {"car": True, "motorcycle": True, "motorbike": True, "bus": True, "Truck": True, "vehicle": True, "truck": True}
    final_class_name = "vehicle"
    nms_thresh = 0.8
    yolo_thresh = 0.7
    d7_thresh = 0.5
    iou_thresh = 0.5


class CarContext:
    relevant_class = {"car": True, "vehicle": True}
    final_class_name = "car"
    nms_thresh = 0.8
    yolo_thresh = 0.7
    d7_thresh = 0.5
    iou_thresh = 0.5


class BusContext:
    relevant_class = {"bus": True}
    final_class_name = "bus"
    nms_thresh = 0.8
    yolo_thresh = 0.7
    d7_thresh = 0.5
    iou_thresh = 0.5


class BicycleContext:
    relevant_class = {"bicycle": True}
    final_class_name = "bicycle"
    nms_thresh = 0.8
    yolo_thresh = 0.7
    d7_thresh = 0.5
    iou_thresh = 0.5


class TruckContext:
    relevant_class = {"truck": True}
    final_class_name = "truck"
    nms_thresh = 0.8
    yolo_thresh = 0.7
    d7_thresh = 0.5
    iou_thresh = 0.5


all_ensemble_contexts = [PedContext(), MotorcycleContext(), CarContext(), BusContext(), BicycleContext(), TruckContext()]
all_ensemble_contexts_callable = [PedContext, MotorcycleContext, CarContext, BusContext, BicycleContext, TruckContext]

"""
For Pipeline
"""


class Ensemble:
    def __init__(self, contexts=None):
        self.contexts = contexts if contexts is not None else all_ensemble_contexts
        self.cur_context = None
        return

    def read_label(self, path):
        with open(path, "r") as fp:
            frame_data = json.load(fp)
        return frame_data

    @staticmethod
    def get_rel_classes(frame_data, classes):
        ret = []
        irrelevant = []
        for i in range(len(frame_data)):
            if frame_data[i]["cat"] in classes:
                ret.append(frame_data[i])
            else:
                irrelevant.append(frame_data[i])
        return ret, irrelevant

    @staticmethod
    def get_rel_classes_from_label(frame_data, classes):
        ret = []
        irrelevant = []
        for i in range(len(frame_data)):
            if frame_data[i].cls in classes:
                ret.append(frame_data[i])
            else:
                irrelevant.append(frame_data[i])
        return ret, irrelevant

    def non_max_suppression_fast(self, frame_data, overlapThresh):
        # if there are no boxes, return an empty list
        boxes = []
        for elem in frame_data:
            boxes.append([elem["bbox"][0], elem["bbox"][1], elem["bbox"][2], elem["bbox"][3]])

        boxes = np.array(boxes)
        if len(boxes) == 0:
            return []
        # if the bounding boxes integers, convert them to floats --
        # this is important since we'll be doing a bunch of divisions

        if boxes.dtype.kind == "i":
            boxes = boxes.astype("float")

        # initialize the list of picked indexes
        pick = []
        # grab the coordinates of the bounding boxes
        x1 = boxes[:, 0]
        y1 = boxes[:, 1]
        x2 = boxes[:, 2]
        y2 = boxes[:, 3]
        # compute the area of the bounding boxes and sort the bounding
        # boxes by the bottom-right y-coordinate of the bounding box
        area = (x2 - x1 + 1) * (y2 - y1 + 1)

        idxs = np.argsort(y2)
        # keep looping while some indexes still remain in the indexes
        # list
        while len(idxs) > 0:
            # grab the last index in the indexes list and add the
            # index value to the list of picked indexes
            last = len(idxs) - 1
            i = idxs[last]
            pick.append(i)
            # find the largest (x, y) coordinates for the start of
            # the bounding box and the smallest (x, y) coordinates
            # for the end of the bounding box
            xx1 = np.maximum(x1[i], x1[idxs[:last]])
            yy1 = np.maximum(y1[i], y1[idxs[:last]])
            xx2 = np.minimum(x2[i], x2[idxs[:last]])
            yy2 = np.minimum(y2[i], y2[idxs[:last]])
            # compute the width and height of the bounding box
            w = np.maximum(0, xx2 - xx1 + 1)
            h = np.maximum(0, yy2 - yy1 + 1)
            # compute the ratio of overlap
            overlap = (w * h) / area[idxs[:last]]
            # delete all indexes from the index list that have
            idxs = np.delete(idxs, np.concatenate(([last],
                                                   np.where(overlap > overlapThresh)[0])))
        # return only the bounding boxes that were picked using the
        # integer data type

        # build back frame data

        return np.array(frame_data)[pick].tolist()

    def get_iou(self, a, b, epsilon=1e-5):
        """ Given two boxes `a` and `b` defined as a list of four numbers:
                [x1,y1,x2,y2]
            where:
                x1,y1 represent the upper left corner
                x2,y2 represent the lower right corner
            It returns the Intersect of Union score for these two boxes.

        Args:
            a:          (list of 4 numbers) [x1,y1,x2,y2]
            b:          (list of 4 numbers) [x1,y1,x2,y2]
            epsilon:    (float) Small value to prevent division by zero

        Returns:
            (float) The Intersect of Union score.
        """
        # COORDINATES OF THE INTERSECTION BOX
        x1 = max(a[0], b[0])
        y1 = max(a[1], b[1])
        x2 = min(a[2], b[2])
        y2 = min(a[3], b[3])

        # AREA OF OVERLAP - Area where the boxes intersect
        width = (x2 - x1)
        height = (y2 - y1)
        # handle case where there is NO overlap
        if (width < 0) or (height < 0):
            return 0.0
        area_overlap = width * height

        # COMBINED AREA
        area_a = (a[2] - a[0]) * (a[3] - a[1])
        area_b = (b[2] - b[0]) * (b[3] - b[1])
        area_combined = area_a + area_b - area_overlap

        # RATIO OF AREA OF OVERLAP OVER COMBINED AREA
        iou = area_overlap / (area_combined + epsilon)
        return iou

    def draw_pred_on_img(self, preds, img, color):
        for cur_label in preds:
            obj = cur_label['cat']
            cat_id = cur_label['cat_id']
            score = cur_label['score']
            x1, y1, x2, y2 = cur_label['bbox']
            if obj == "cyclist" or obj == "rider" or obj == "motorbiker":
                color = (0, 0, 255)
            cv2.rectangle(img, (x1, y1), (x2, y2), color, 2)
            cv2.putText(img, '{}, {:.3f}'.format(obj, score),
                        (x1, y1 + 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                        color, 2)
        return img

    def filter_single(self, preds, thresh):
        final_preds = []
        to_check = []
        for i in range(len(preds)):
            if preds[i]["score"] > thresh:
                final_preds.append(preds[i])
            else:
                to_check.append(preds[i])
        return final_preds, to_check

    def filter_check(self, yolo_check, d7_check):
        after_filter = []
        for yolo_idx in range(len(yolo_check)):
            for d7_idx in range(len(d7_check)):
                cur_iou = self.get_iou(yolo_check[yolo_idx]["bbox"], d7_check[d7_idx]["bbox"])
                if cur_iou > self.cur_context.iou_thresh:
                    new_label = {}
                    new_label['cat'] = self.cur_context.final_class_name
                    new_label['cat_id'] = "undefined"
                    new_label['score'] = (yolo_check[yolo_idx]["score"] + d7_check[d7_idx]["score"]) / 2
                    x1, y1, x2, y2 = yolo_check[yolo_idx]["bbox"]
                    new_label['bbox'] = [int(x1), int(y1), int(x2), int(y2)]
                    new_label['img_res'] = [940, 1824]
                    after_filter.append(new_label)
        return after_filter

    def filter_preds(self, yolo_preds, d7_preds):
        d7_final, d7_check = self.filter_single(d7_preds, self.cur_context.d7_thresh)
        yolo_final, yolo_check = self.filter_single(yolo_preds, self.cur_context.yolo_thresh)

        finals = d7_final + yolo_final
        checked = self.filter_check(yolo_check, d7_check)
        finals += checked

        # NMS
        finals = self.non_max_suppression_fast(finals, self.cur_context.nms_thresh)
        return finals, len(checked)

    def ensemble_from_dir(self, path_context, df, force=False):
        base_path_ensembles = path_context.camera_det_ensemble
        base_path_ensembles_images = path_context.camera_det_ensemble_images

        rider_df = pd.DataFrame()
        for idx in tqdm(range(len(df))):
            rgbImPath = path_context.camera_rgb + "/camera_rgb_" + str(df.iloc[idx]["timestampCamera"]) + ".png"
            e_det = path_context.camera_det_EfficientDetD7 + "/camera_det_EfficientDetD7_" + str(df.iloc[idx]["timestampCamera"]) + ".json"
            yolo_det = path_context.camera_det_yoloV3 + "/camera_det_yoloV3_" + str(df.iloc[idx]["timestampCamera"]) + ".json"

            if not os.path.exists(yolo_det) or not os.path.exists(e_det):
                continue

            label_path = base_path_ensembles + "/camera_det_ensemble_" + str(df.iloc[idx]["timestampCamera"]) + ".json"
            if os.path.exists(label_path) and not force:
                print("label exists - skipping")
                continue

            context_labels = []
            classes_to_add = []
            count = 0
            for context in self.contexts:
                self.cur_context = context
                for c in self.cur_context.relevant_class:
                    classes_to_add.append(c)

                yolo_label, other_classes_yolo = Ensemble.get_rel_classes(self.read_label(yolo_det), classes=self.cur_context.relevant_class)
                d7_label, other_classes_d7 = Ensemble.get_rel_classes(self.read_label(e_det), classes=self.cur_context.relevant_class)

                finals, cur_count = self.filter_preds(yolo_label, d7_label)
                count += cur_count
                if finals is not None:
                    context_labels += finals

            yolo_label, other_classes_yolo = Ensemble.get_rel_classes(self.read_label(yolo_det), classes=classes_to_add)
            d7_label, other_classes_d7 = Ensemble.get_rel_classes(self.read_label(e_det), classes=classes_to_add)
            if context_labels is not None:
                context_labels += other_classes_d7
            else:
                context_labels = other_classes_d7

            for j in range(len(context_labels)):
                context_labels[j]['img_res'] = [940, 1824]
                if context_labels[j]['cat'] == "person":
                    context_labels[j]['cat'] = "pedestrian"

            # Ensamble with seg
            label_path_hard = path_context.camera_seg_class + "/camera_seg_class_" + str(df.iloc[idx]["timestampCamera"]) + ".npz"
            label_path_soft = path_context.camera_seg_soft + "/camera_seg_soft_" + str(df.iloc[idx]["timestampCamera"]) + ".npz"

            # if not (os.path.exists(label_path_hard) and os.path.exists(label_path_soft)):
            #     continue

            seg_label = np.load(label_path_hard)["arr_0"]
            seg_ped_mask = np.zeros(seg_label.shape, dtype=seg_label.dtype)
            img = cv2.cvtColor(cv2.imread(rgbImPath), cv2.COLOR_BGR2RGB)

            # 0 road
            # 1 sidewalk
            # 2 building
            # 3 wall
            # 4 fence
            # 5 pole
            # 6 traffic-light
            # 7 traffic-sign
            # 8 vegetation
            # 9 terrain
            # 10 sky
            # 11 person
            # 12 rider
            # 13 car
            # 14 truck
            # 15 bus
            # 16 train
            # 17 motorcycle
            # 18 bicycle

            # Forbidden classes
            for i in [8]: #, 12, 17, 18]:
                seg_ped_mask[seg_label == i] = True

            seg_ped_mask = cv2.resize(seg_ped_mask, (img.shape[1], img.shape[0]))
            filter_ped_class = []
            count_fixes = 0
            sums_lst = []
            for idx_v2, bb in enumerate(context_labels):
                if bb['cat'] == 'pedestrian' or bb['cat'] == 'person':
                    x1, y1, x2, y2 = bb['bbox']
                    mask = np.zeros(img.shape[:2], dtype=seg_label.dtype)
                    mask[y1:y2, x1:x2] = True

                    total_sum = np.logical_and(seg_ped_mask, mask).sum()
                    final_res = total_sum / ((y2 - y1) * (x2 - x1))

                    sums_lst.append(round(final_res, 2))
                    filter_ped_class.append(bb)
                    if final_res > 0.3:
                        count_fixes += 1
                        context_labels[idx_v2]['cat'] = "forbidden_pedestrian"

            plot_rider = False
            try:
                seg_label_path = path_context.camera_seg_class + "/camera_seg_class_" + str(df.iloc[idx]["timestampCamera"]) + ".npz"
                seg_label = np.load(seg_label_path)["arr_0"]
                num_riders = 0
                iou_thresh = 0.15

                m_cycles = [x for x in context_labels if x["cat"] in BicycleContext.relevant_class]
                if len(m_cycles) != 0:
                    for idx2, x in enumerate(context_labels):
                        max_iou = 0
                        if x["cat"] in PedContext.relevant_class:
                            for motor in m_cycles:
                                max_iou = max(max_iou, self.get_iou(motor["bbox"], x["bbox"]))
                        if max_iou > iou_thresh:
                            context_labels[idx2]["cat"] = "cyclist"
                            num_riders += 1

                # motorbiker
                m_cycles = [x for x in context_labels if x["cat"] in MotorcycleContext.relevant_class]
                if len(m_cycles) != 0:
                    for idx2, x in enumerate(context_labels):
                        max_iou = 0
                        if x["cat"] in PedContext.relevant_class:
                            for motor in m_cycles:
                                max_iou = max(max_iou, self.get_iou(motor["bbox"], x["bbox"]))
                        if max_iou > iou_thresh:
                            context_labels[idx2]["cat"] = "motorbiker"
                            num_riders += 1

                if num_riders > 0:
                    plot_rider = True
                    # print("Found some Rider!")
                    rider_df.append(df.iloc[idx])
                    with open(label_path, 'w') as fp:
                        json.dump(context_labels, fp, sort_keys=True, indent=4)

            except Exception as e:
                print("Err", e)
                continue

            with open(label_path, 'w') as fp:
                json.dump(context_labels, fp, sort_keys=True, indent=4)

            # if plot_rider:
            #     plt.figure(figsize=(20, 10))
            #     img_final = self.draw_pred_on_img(context_labels, img.copy(), (255, 0, 0))
            #     plt.title("after filter")
            #     plt.imshow(img_final)
            #     print("plotting rider")
            #     plt.savefig(base_path_ensembles_images + "/rider_" + str(df.iloc[idx]["timestampCamera"]) + "_" + str(count) + ".png")
            #     plt.close()

            if idx % 1000 == 0:
                if not os.path.exists(rgbImPath):
                    continue

                img_yolo = self.draw_pred_on_img(yolo_label, img.copy(), color=(0, 255, 0))
                plt.figure(figsize=(50, 25))
                plt.subplot(2, 3, 1)
                plt.title("Yolo")
                plt.imshow(img_yolo)

                img_d7 = self.draw_pred_on_img(d7_label, img.copy(), (0, 0, 255))
                plt.subplot(2, 3, 2)
                plt.title("D7")
                plt.imshow(img_d7)

                img_both = self.draw_pred_on_img(d7_label, img_yolo, (0, 0, 255))
                plt.subplot(2, 3, 4)
                plt.title("both")
                plt.imshow(img_both)

                img_final = self.draw_pred_on_img(context_labels, img.copy(), (255, 0, 0))
                plt.subplot(2, 3, 5)
                plt.title("after filter")
                plt.imshow(img_final)

                plt.subplot(2, 3, 3)
                plt.title("seg map")
                plt.imshow(seg_label)

                plt.savefig(base_path_ensembles_images + "/" + str(df.iloc[idx]["timestampCamera"]) + "_" + str(count) + ".png")
                plt.close()
        try:
            rider_df.to_csv(path_context.aux_path + "/riders_df.csv")
        except Exception as e:
            print(e)
            rider_df.to_csv("./riders_df.csv")


"""
For Evaluation
"""


class NewEnsemble:
    def __init__(self, contexts=None):
        self.contexts = contexts if contexts is not None else all_ensemble_contexts
        self.cur_context = None
        return

    @staticmethod
    def get_rel_classes_from_label(labels, classes):
        ret = []
        irrelevant = []
        for i in range(len(labels)):
            if labels[i].cls in classes:
                ret.append(labels[i])
            else:
                irrelevant.append(labels[i])
        return ret, irrelevant

    @staticmethod
    def get_iou(a, b, epsilon=1e-5):
        """ Given two boxes `a` and `b` defined as a list of four numbers:
                [x1,y1,x2,y2]
            where:
                x1,y1 represent the upper left corner
                x2,y2 represent the lower right corner
            It returns the Intersect of Union score for these two boxes.

        Args:
            a:          (list of 4 numbers) [x1,y1,x2,y2]
            b:          (list of 4 numbers) [x1,y1,x2,y2]
            epsilon:    (float) Small value to prevent division by zero

        Returns:
            (float) The Intersect of Union score.
        """
        # COORDINATES OF THE INTERSECTION BOX
        x1 = max(a[0], b[0])
        y1 = max(a[1], b[1])
        x2 = min(a[2], b[2])
        y2 = min(a[3], b[3])

        # AREA OF OVERLAP - Area where the boxes intersect
        width = (x2 - x1)
        height = (y2 - y1)
        # handle case where there is NO overlap
        if (width < 0) or (height < 0):
            return 0.0
        area_overlap = width * height

        # COMBINED AREA
        area_a = (a[2] - a[0]) * (a[3] - a[1])
        area_b = (b[2] - b[0]) * (b[3] - b[1])
        area_combined = area_a + area_b - area_overlap

        # RATIO OF AREA OF OVERLAP OVER COMBINED AREA
        iou = area_overlap / (area_combined + epsilon)
        return iou

    def filter_check(self, yolo_check, d7_check):
        after_filter = []
        for yolo_idx in range(len(yolo_check)):
            for d7_idx in range(len(d7_check)):
                cur_iou = NewEnsemble.get_iou(yolo_check[yolo_idx].get_coords(as_int=True), d7_check[d7_idx].get_coords(as_int=True))
                if cur_iou > self.cur_context.iou_thresh:
                    x1, y1, x2, y2 = yolo_check[yolo_idx].get_coords(as_int=True)
                    new_label = Label2D(cls=self.cur_context.final_class_name, score=(max(yolo_check[yolo_idx].score, d7_check[d7_idx].score)), x1=x1, y1=y1, x2=x2, y2=y2)
                    after_filter.append(new_label)
                    # print("!!Added!!", new_label, "\n")
                # else:
                    # print("IOU not sufficient", cur_iou)
                    # print(yolo_check[yolo_idx], d7_check[d7_idx])
                    # print()
        return after_filter

    @staticmethod
    def non_max_suppression_fast(frame_data, overlapThresh):
        # if there are no boxes, return an empty list
        boxes = []
        for elem in frame_data:
            boxes.append(elem.get_coords())

        # pprint(boxes)

        boxes = np.array(boxes)
        if len(boxes) == 0:
            return []
        # if the bounding boxes integers, convert them to floats --
        # this is important since we'll be doing a bunch of divisions

        if boxes.dtype.kind == "i":
            boxes = boxes.astype("float")

        # initialize the list of picked indexes
        pick = []
        # grab the coordinates of the bounding boxes
        x1 = boxes[:, 0]
        y1 = boxes[:, 1]
        x2 = boxes[:, 2]
        y2 = boxes[:, 3]
        # compute the area of the bounding boxes and sort the bounding
        # boxes by the bottom-right y-coordinate of the bounding box
        area = (x2 - x1 + 1) * (y2 - y1 + 1)

        # print("area=", area)
        # print("x1", x1)

        idxs = np.argsort(y2)
        # print("idxs", idxs)
        # keep looping while some indexes still remain in the indexes
        # list
        while len(idxs) > 0:
            # grab the last index in the indexes list and add the
            # index value to the list of picked indexes
            last = len(idxs) - 1
            i = idxs[last]
            pick.append(i)
            # print("i in test", i, "\n")
            # find the largest (x, y) coordinates for the start of
            # the bounding box and the smallest (x, y) coordinates
            # for the end of the bounding box
            xx1 = np.maximum(x1[i], x1[idxs[:last]])
            yy1 = np.maximum(y1[i], y1[idxs[:last]])
            xx2 = np.minimum(x2[i], x2[idxs[:last]])
            yy2 = np.minimum(y2[i], y2[idxs[:last]])
            # compute the width and height of the bounding box
            w = np.maximum(0, xx2 - xx1 + 1)
            h = np.maximum(0, yy2 - yy1 + 1)
            # compute the ratio of overlap
            overlap = (w * h) / area[idxs[:last]]
            # delete all indexes from the index list that have
            # print("to_test", x1[i])
            # print(list(zip(xx1, yy1, xx2, yy2, overlap)))
            # print(overlap)
            idxs = np.delete(idxs, np.concatenate(([last], np.where(overlap > overlapThresh)[0])))
        # return only the bounding boxes that were picked using the
        # integer data type

        # build back frame data
        # print("picks", pick)
        return np.array(frame_data)[pick].tolist()

    @staticmethod
    def filter_single(preds, thresh):
        final_preds = []
        to_check = []
        for i in range(len(preds)):
            if preds[i].score > thresh:
                final_preds.append(preds[i])
            else:
                to_check.append(preds[i])
        return final_preds, to_check

    def filter_preds(self, yolo_preds, d7_preds):
        d7_final, d7_check = self.filter_single(d7_preds, self.cur_context.d7_thresh)
        yolo_final, yolo_check = self.filter_single(yolo_preds, self.cur_context.yolo_thresh)

        finals = d7_final + yolo_final
        checked = self.filter_check(yolo_check, d7_check)
        finals += checked

        finals = self.non_max_suppression_fast(finals, self.cur_context.nms_thresh)
        return finals, len(checked)

    def ensemble_from_obj(self, img, e_det, yolo_det, seg_label):
        context_labels = []
        classes_to_add = []
        count = 0
        for context in self.contexts:
            self.cur_context = context
            for c in self.cur_context.relevant_class:
                classes_to_add.append(c)

            yolo_label, other_classes_yolo = NewEnsemble.get_rel_classes_from_label(yolo_det, classes=self.cur_context.relevant_class)
            d7_label, other_classes_d7 = NewEnsemble.get_rel_classes_from_label(e_det, classes=self.cur_context.relevant_class)

            finals, cur_count = self.filter_preds(yolo_label, d7_label)
            count += cur_count
            if finals is not None:
                context_labels += finals

        d7_label, other_classes_d7 = NewEnsemble.get_rel_classes_from_label(e_det, classes=classes_to_add)

        if context_labels is not None:
            context_labels += other_classes_d7
        else:
            context_labels = other_classes_d7

        labels2d = []
        for label in context_labels:
            if label is not None:
                labels2d.append(label)

        return labels2d
