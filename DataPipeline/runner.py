import sys
from argparse import Namespace
import os
# from arg_parser import parser
import time
import subprocess
import logging

import numpy as np

base_mounts = "/workspace/"
mounts = [" -v " + base_mounts + x + ":/workspace/" + x + " " for x in os.listdir(base_mounts)]

disks_locations = " -v /home/amper/:/workspace/ " + " ".join(mounts) + \
                  " -v /media/amper/storage:/workspace/storage/"

docker_template_cmd = "sudo nvidia-docker run -it -d --env=\"DISPLAY\" -v /tmp/.X11-unix:/tmp/.X11-unix " + disks_locations + \
                      "--shm-size=64g --ulimit memlock=-1 --ulimit stack=61708864 --name "


def run_docker(image, container_name, exec_cmd, run_melodic=False):
    os.system("docker kill " + container_name)
    os.system("docker rm " + container_name)
    time.sleep(0.05)

    docker_run_cmd = docker_template_cmd + container_name + " " + image
    logging.info("run_docker: container command: docker_run_cmd: %s", docker_run_cmd)
    subprocess.call(docker_run_cmd, shell=True)

    logging.info("run_docker: sleeping")
    time.sleep(1)
    logging.info("run_docker: wake-up")

    if run_melodic:
        run_cmd = "docker exec -i " + container_name + " source /opt/ros/melodic/setup.bash"

    subprocess.call(run_cmd, shell=True)

    run_cmd = "docker exec -i " + container_name + " " + exec_cmd
    logging.info("run_docker: exec command: run_cmd: %s", run_cmd)
    subprocess.call(run_cmd, shell=True)
    return


def run_docker_template(image, docker_image_cmd, container_name, exec_cmd, sleep=0.5, d="-d"):
    time.sleep(np.random.randint(10))
    os.system("docker kill " + container_name)
    os.system("docker rm " + container_name)
    time.sleep(0.05)

    # docker_run_cmd = docker_image_cmd + container_name + " " + image
    docker_run_cmd = f'{docker_image_cmd} --name {container_name} {image}'
    logging.info("run_docker: container command: docker_run_cmd: %s", docker_run_cmd)
    subprocess.call(docker_run_cmd, shell=True)

    logging.info("run_docker: sleeping")
    time.sleep(sleep)
    logging.info("run_docker: wake-up")

    run_cmd = "docker exec -d -i " + container_name + " bash -c 'source /root/.bashrc'"
    logging.info("run_docker: exec command: run_cmd: %s", run_cmd)
    subprocess.call(run_cmd, shell=True)
    time.sleep(2)

    run_cmd = f"docker exec {d} -i {container_name} {exec_cmd}"
    logging.info("run_docker: exec command: run_cmd: %s", run_cmd)
    subprocess.call(run_cmd, shell=True)
    time.sleep(45)
    pass


def resume_docker(container_name, command="start"):
    run_cmd = f"nvidia-docker {command} {container_name}"
    subprocess.Popen(run_cmd, shell=True)
    time.sleep(3)
    print("docker container", container_name, "loaded")


def run_docker_command(container_name, exec_cmd, d=""):
    run_cmd = f"docker exec {d} -i {container_name} {exec_cmd}"
    subprocess.Popen(run_cmd, shell=True)
    return run_cmd

# args = parser.parse_args()
# args_as_string = str(args)
# print(args_as_string)
# Run Pipeline Command
