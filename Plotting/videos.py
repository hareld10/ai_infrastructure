import cv2
import os
from tqdm import tqdm

from DataPipeline.src.GetEngines import get_engines


def make_video_engine(engine, suffix="", save_path=None):
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    df = engine.df

    if save_path is None:
        save_path = "./"
    video_name = save_path + "/video" + suffix + ".mp4"

    first = True
    for idx in tqdm(range(len(df))):  # len(config.df))):
        if first:
            # frame = cv2.imread(images_dir + "/" + str(df.loc[idx, "timestampRadar"]) + '.png')
            frame = engine.get_image(idx=idx)
            if frame is None:
                print("Video | Frame is None")
                continue
            height, width, layers = frame.shape
            video = cv2.VideoWriter(video_name, fourcc, 6, (width, height))
            first = False

        video.write(engine.get_image(idx=idx)[..., ::-1])

    cv2.destroyAllWindows()
    video.release()


if __name__ == '__main__':
    engines = get_engines(wf="92", val_train="val", randomize=False, rect=True)
    for p_engine in engines:
        print(p_engine.get_drive_name())
        p_engine.sample_df_range(range_len=300)
        make_video_engine(p_engine, suffix="_short_" + p_engine.get_drive_name(), save_path="./video_dir/")