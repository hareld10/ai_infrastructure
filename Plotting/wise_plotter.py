import os
import glob
import pathlib
import time
from pathlib import Path
from typing import Dict

from tqdm import tqdm
import pandas as pd
import numpy as np
from matplotlib import gridspec, colors
import matplotlib.pyplot as plt
from IPython.display import clear_output
from mpl_toolkits.axes_grid1 import make_axes_locatable

from wiseTypes.Cluster import Cluster

plt.style.use('dark_background')
from Calibrator.calibrator_utils import process_lidar_to_image_on_ax_df
from DataPipeline.envelope import get_el_rgb_indices
from Evaluation.evaluation_utils import draw, make_video_from_dir
from DataPipeline.src.GetEngines import get_engines
module_path = str(pathlib.Path(__file__).parent.absolute())
from tqdm import tqdm


class WisePlotter:
    def __init__(self, engine):
        self.engine = engine
        print("Plotter | ", self.engine)
        self.video_dir = "%s/plots/" % module_path
        self.pred_df = pd.read_csv("/workspace/HDD/Meir/radar/radar_classification/%s.csv" % self.engine.get_drive_name())

        os.makedirs(self.video_dir, exist_ok=True)

    def plot_rgb(self, ax, engine_idx):
        rgb_img = self.engine.get_image(engine_idx)
        seg_label = self.engine.get_segmentation_label(engine_idx)
        labels_2d = self.engine.get_2d_label(engine_idx)
        ax.imshow(draw(rgb_img, labels_2d))
        ax.imshow(seg_label.get_img(colors=True), alpha=0.3)
        ax.set_xticks([])
        ax.set_yticks([])
        ax.set_title('Camera Image')
        return

    def plot_delta(self, ax, engine_idx, labels=True, pred=False):
        processed_delta = self.engine.get_processed_delta(engine_idx)
        delta_clusters: Dict[int, Cluster] = self.engine.get_clusters(p_delta=processed_delta)

        for cluster_id, cluster in delta_clusters.items():
            cluster.plot(ax=ax)

        if pred:
            cur_instances = self.pred_df[self.pred_df["timestamp_radar"].astype(str) == self.engine.get_id(engine_idx)]
            # Clusters
            for row_idx, row in cur_instances[cur_instances["sub_cluster"] == -1].iterrows():
                delta_clusters[int(row["cluster_id"])].plot_bounding_box(ax=ax, annotate=row["predicted_class"])
            # Sub cluster
            for row_idx, row in cur_instances[cur_instances["sub_cluster"] != -1].iterrows():
                cur_cluster = delta_clusters[int(row["cluster_id"])]
                sub_clusters = cur_cluster.get_sub_clusters()
                sub_clusters[int(row["sub_cluster"])].plot_bounding_box(ax=ax, annotate=row["predicted_class"])

        plt.title('Delta | cmap=id')
        plt.xlabel('[m]')
        plt.ylabel('[m]')
        plt.xlim([-self.engine.radar_fov_az // 2, self.engine.radar_fov_az // 2])
        plt.ylim(([self.engine.min_range, self.engine.max_range]))
        if labels:
            labels_3d = self.engine.get_lidar_camera_fusion_labels(idx=engine_idx, as_obj=True)
            [x.plot(ax, radar=True) for x in labels_3d]

    def make_video(self, start_idx, end_idx):
        self.engine.update_df_range(start_idx, end_idx)

        for idx in tqdm(range(len(self.engine))):
            self.draw_single(idx)
            plt.savefig(self.video_dir + str(self.engine.get_id(idx)) + ".png")
            plt.close()

        make_video_from_dir(df=self.engine.df, frame_rate=4,
                            images_dir=self.video_dir, suffix="_" + str(time.time()).split(".")[0])
        return

    def draw_single(self, idx):
        plt.close()
        gs = gridspec.GridSpec(ncols=2, nrows=1)
        fig = plt.figure(figsize=(24, 13))

        self.plot_rgb(ax=fig.add_subplot(gs[0, 0]), engine_idx=idx)
        self.plot_delta(ax=fig.add_subplot(gs[0, 1]), engine_idx=idx, pred=True)
        return


def add_rcs(df, ax):
    # X Values
    num_bins_x = len(ax.get_xticks())
    edges = np.linspace(ax.get_xlim()[0], ax.get_xlim()[1], num_bins_x)
    labels = np.arange(len(edges) - 1)
    df["cut_x"] = pd.cut(df["x"], edges, labels=labels)
    x_values = df.groupby(["cut_x"])["total_power"].sum().fillna(0).values

    # Y Values
    num_bins_y = len(ax.get_yticks())
    edges = np.linspace(ax.get_ylim()[0], ax.get_ylim()[1], num_bins_y)
    labels = np.arange(len(edges) - 1)
    df["cut_y"] = pd.cut(df["y"], edges, labels=labels)
    y_values = df.groupby(["cut_y"])["total_power"].sum().fillna(0).values

    divider = make_axes_locatable(ax)
    top_ax = divider.append_axes("top", 1.05, pad=0.1, sharex=ax)
    right_ax = divider.append_axes("right", 1.05, pad=0.3, sharey=ax)
    top_ax.set_ylabel('X-CS [dB]')
    right_ax.set_xlabel('Y-CS [dB]')
    right_ax.autoscale(enable=False)
    top_ax.autoscale(enable=False)
    top_ax.xaxis.set_tick_params(labelbottom=False)
    right_ax.yaxis.set_tick_params(labelleft=False)
    right_ax.set_xlim(right=y_values.max())
    top_ax.set_ylim(top=x_values.max())
    v_prof, = right_ax.plot(y_values, ax.get_yticks()[1:], 'r-')
    h_prof, = top_ax.plot(ax.get_xticks()[:-1], x_values, 'r-')


def project_delta_clusters(ax, delta_clusters, im, c_norm=None):
    ax.axis(False)
    for cluster_idx, cluster in delta_clusters.items():
        process_lidar_to_image_on_ax_df(display_df=cluster.get_pts(), im=im, ax=ax, points_size=16, specific_cNorm=c_norm, cbar=False)
    ax.imshow(im)
    ax.set_anchor('C')
    ax.set_title("Delta | cmap=range [m]")


def plot_3d_evaluation(labels, preds, engine, idx):
    plt.close()
    plt.style.use('dark_background')
    gs = gridspec.GridSpec(ncols=2, nrows=1)
    fig = plt.figure(figsize=(30, 15))
    ax = fig.add_subplot(gs[0, 0])

    im = engine.get_image(idx)
    ax.imshow(engine.map_lidar_points_onto_image(im, engine.get_lidar(idx)))
    ax.axis(False)
    ax.set_title("Camera | Lidar | cmap=range")

    ax = fig.add_subplot(gs[0, 1])
    ax.set_title("Lidar | BEV")
    [x.plot(ax=ax, c="white", bound="r-bbox") for x in labels]
    [x.plot(ax=ax, c="red", bound="r-bbox") for x in preds]

    lidar = engine.get_lidar_df(idx)
    norms = lidar["z"]
    cNorm = colors.Normalize(vmin=np.min(norms), vmax=np.max(norms))
    scat = ax.scatter(lidar["orig_x"], lidar["orig_y"],
                      s=10,
                      c=norms,
                      cmap="jet",
                      norm=cNorm)


def plot_rgb(ax, im, engine, labels_2d=None, pad=10):
    im_to_plot = im.copy()

    if labels_2d is not None:
        im_to_plot = draw(im_to_plot, label2d=labels_2d)

    if ax is not None:
        ax.axis(False)
        ax.imshow(im_to_plot)
        ax.set_title("Camera | Object Detection", pad=pad)
        el_min, el_max = get_el_rgb_indices(engine)
        if el_min is not None:
            ax.set_ylim(int(el_min), int(el_max))
            ax.invert_yaxis()
    return im_to_plot


def cut_pc_by_lims(pc, x_min, x_max, y_min, y_max):
    df = pc.copy()
    df = df[((df["x"] >= x_min) & (df["x"] <= x_max)) & ((df["y"] >= y_min) & (df["y"] <= y_max))]
    return df


def main():
    engines = get_engines(wf="92", val_train="val", randomize=False, envelope_active=False)
    ws = WisePlotter(engine=engines[0])
    ws.draw_single(800)
    plt.show()

    # ws.make_video(start_idx=1286, end_idx=1500)
    return

if __name__ == '__main__':
    main()