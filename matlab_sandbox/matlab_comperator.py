from glob import glob
import scipy.io as sio
from scipy.io import loadmat
import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
from termcolor import colored
import colorama
from colorama import Fore, Style

class Compare:
    def __init__(self, key, matlab_value):
        self.key = key
        self.matlab_value = matlab_value
        self.python_value = None

    def add_python_value(self, p_value):
        if p_value.shape != self.matlab_value.shape:
            print(Fore.RED + "#"*20)
            print("Error: Bad shape", self.key)
            print("Error: Python Array", p_value.shape, "MatlabArray", self.matlab_value.shape)
            print("#" * 20)
            print(Style.RESET_ALL)
            return False

        self.python_value = p_value.copy()
        return True

    def validate(self):
        if self.python_value is None or self.matlab_value is None:
            # print("One of the values is missing, check for error above")
            return False
        return True

    def get_diff(self):
        if not self.validate():
            return False

        val = np.abs(self.python_value - self.matlab_value).sum()
        if val == 0:
            print("Values are the same")
        return val

    def get_headline(self, obj):
        return str(" " + str(self.key) + " " + str(obj.dtype) + " " + str(obj.shape))

    def plot_preprocess(self, arr):
        shapes = arr.shape
        if len(shapes) > 3:
            print("TOO MANY DIMENSIONS!")
            return arr

        min_idx = shapes.index(min(shapes))
        return np.max(arr, axis=min_idx)

    def get_visual(self, fig, gs, plot_idx):
        if not self.validate():
            return False

        txt_ax = fig.add_subplot(gs[plot_idx, 0])
        txt_ax.axis('off')

        txt = ""
        txt += "Diff " + str(self.get_diff()) + "\n"
        txt += "Python: " + self.get_headline(self.python_value) + "\n"
        txt += "Matlab: " + self.get_headline(self.matlab_value) + "\n"
        for i in range(1,4):
            eps = 10**(-i)
            txt += "All-close?: 10^(-" + str(i) + ") " + str(np.allclose(self.python_value, self.matlab_value, eps)) + "\n"

        txt_ax.text(0.5, 0.5, txt, horizontalalignment='center', verticalalignment='center', )

        mat_ax = fig.add_subplot(gs[plot_idx, 1])
        mat_ax.imshow(self.plot_preprocess(self.matlab_value))
        mat_ax.set_title("Matlab:")

        python_ax = fig.add_subplot(gs[plot_idx, 2])
        python_ax.imshow(self.plot_preprocess(self.python_value))
        python_ax.set_title("Python:")

        plt.tight_layout(pad=3.0)

        return


class MatlabComperator:
    def __init__(self, files_dir):
        self.files_dir = files_dir
        self.file_map = {}
        self.load_files_to_compare()
        self.fig = None
        self.gs = None

    def load_files_to_compare(self):
        for mat_file in glob(self.files_dir + "/*.mat"):
            print(sio.whosmat(mat_file))
            key = os.path.split(mat_file)[-1].split(".")[0]
            matlab_value = np.array(loadmat(mat_file)[key], dtype=np.float64)

            if key == "ppa":
                matlab_value = matlab_value.transpose((1, 2, 0))
            self.file_map[key] = Compare(key=key, matlab_value=matlab_value)
        return

    def add_key(self, arr, key):
        arr = np.array(arr)
        if key not in self.file_map:
            print("Error: Key not in map")
            return False

        if self.file_map[key].add_python_value(arr):
            print(Fore.GREEN)
            print("Added: ", key, "shape", arr.shape)
            print(Style.RESET_ALL)

    def get_summary(self):
        for key, comp in self.file_map.items():
            print("#" * 20)
            print("Summary for:", key)
            print("abs and sum", comp.get_diff())
            print("#" * 20)
        return

    def get_len(self):
        return len([x for x in self.file_map.keys() if self.file_map[x].validate()])

    def plot(self):
        if self.get_len() == 0:
            print(Fore.RED)
            print("Nothing to plot")
            print(Style.RESET_ALL)
            return
        self.fig = plt.figure(figsize=(20, 10))
        self.gs = gridspec.GridSpec(ncols=3, nrows=self.get_len())

        print("self.get_len()", self.get_len())
        plot_idx = 0
        for row_idx, (key, comp) in enumerate(self.file_map.items()):
            if not comp.validate():
                continue
            comp.get_visual(fig=self.fig, gs=self.gs, plot_idx=plot_idx)
            plot_idx += 1

        plt.savefig("./comparison_results.png")
        plt.show()

# m = MatlabComperator(files_dir="/home/amper/AI/Harel/AI_Infrastructure/matlab_sandbox/matlab_files")


# Total Rand
# rand_arr = np.random.rand(10, 10, 3)
# m.add_key(arr=rand_arr, key="rand_arr")
#
# # Match Rand
# semi_rand = np.array(loadmat("./matlab_files/semi_rand.mat")["semi_rand"], dtype=np.float64)
# m.add_key(arr=semi_rand, key="semi_rand")
#
# # EXact Ones
# ones_arr = np.ones((10, 10, 3))
# m.add_key(arr=ones_arr, key="ones_arr")
#
# # semi ones Ones
# ones_only_arr = np.zeros((10, 10, 3))
# m.add_key(arr=ones_only_arr, key="ones_only_arr")

# m.plot()
# m.get_summary()
