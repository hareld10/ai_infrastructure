import pathlib

import numpy as np
import glob
import os
import pandas as pd
import torch
import torch.nn.functional as F
import cv2
from typing import Union
import matplotlib.pyplot as plt
import sys
from pathlib import Path

# file_dir = os.path.dirname(__file__)
from matlab_sandbox.matlab_comperator import MatlabComperator

data_pipeline_path = "DataPipeline/"
sys.path.insert(0, data_pipeline_path)
data_pipeline_path = "matlab_sandbox/"
sys.path.insert(0, data_pipeline_path)

print("get_cwd", os.getcwd())
# print(os.listdir(data_pipeline_path))

from DataPipeline.raw_reader import RawReader
from matlab_sandbox.DSP_python.utils import *

module_path = str("//home/amper/workspace/wise_sdk/SDK/python/")
sys.path.insert(0, module_path)
module_path = "/workspace/workspace/wise_sdk/SDK/python/"
sys.path.insert(0, module_path)
module_path = os.getcwd()
sys.path.insert(0, module_path)

cur_module_path = str(pathlib.Path(__file__).parent.absolute())


class config:
    base_path = '/workspace/HDD/76mWF/200902_lidar_drive_highway_W92_M15_RF1/'

    # DSP parameters

    # ppa
    cfar_type = 'nci'  # [nci,fft]
    cfar_percentile = 63
    ppa_cfar_threshold = 10  # [db]
    ppa_hamming_alpha = 0.54
    ppa_hamming_beta = 0.46
    ppa_window_range = 'han'
    ppa_window_dop = 'han'

    # mimo
    mimo_cfar_threshold = 6
    mimo_window_range = 'han'
    mimo_window_full_doppler = 'han'
    mimo_window_az = 'han'
    mimo_window_el = 'han'
    n_az = 20
    n_el = 6
    zero_pad_az = 4
    zero_pad_el = 4

    az_fov = 100
    el_fov = 20

    # Waveform parameters
    n_rx = 8
    n_tx = 6

    n_pulses = 64
    n_samples = 1024

    full_dop_size = n_pulses * n_tx
    n_dop = n_pulses
    n_range = n_samples // 2
    PRI_usec = 20e-6  # [micro sec]
    pzl_asc_max_vel_ms = 1
    bin_rng_ppa = 0.25  # [m]
    bin_rng_mimo = 0.25  # [m]
    bin_dop_ppa = 1  # [m/sec]
    bin_dop_mimo = 0.25  # [m/sec]

    # Full doppler phase compensation
    full_dop_phase_mat = build_full_dop_phase_mat(n_dop, n_tx, n_rx)

    # Use cfar on mimo sub-frame
    use_mimo_cfar = False

    # Get virtual array channel layout
    df_array = virtual_array(print_layout=False)

    # Get expansion kernel for coarse association
    expansion_kernel = get_expansion_kernel(bin_rng_ppa, bin_rng_mimo, bin_dop_ppa, bin_dop_mimo)

    comperator = None


#######################################################
config = config()
raw_reader = RawReader()

paths = [str(path) for path in Path(cur_module_path + '/../matlab_compares/').rglob('*.raw')]

for p in paths:
    matlab_comperator = MatlabComperator(files_dir=os.path.split(p)[0])
    config.comperator = matlab_comperator

    mimo_raw, ppa_raw = raw_reader.get_ppa_mimo_raw(p, sleep=True)

    if ppa_raw.dtype == np.complex64:
        ppa_raw = np.real(ppa_raw)
    if mimo_raw.dtype == np.complex64:
        mimo_raw = np.real(mimo_raw)

    ppa_raw = torch.from_numpy(ppa_raw)
    mimo_raw = torch.from_numpy(mimo_raw)

    # Fails
    matlab_comperator.add_key(mimo_raw, key="mimo")

    # ppa_raw = ppa_raw.permute((2, 0, 1))
    ppa_raw = ppa_raw[..., 1:]
    matlab_comperator.add_key(ppa_raw, key="ppa")

    # PPA Subframe: Processing & Coarse Detection
    ppa_cfar = ppa_processing(config, ppa_raw)  # (range,doppler)

    # MIMO Subframe: Processing & Coarse Detection
    mimo_ffta,mimo_cfar = mimo_processing(config,mimo_raw)

    # Association and filtering
    # mimo_4dfft,ppa_cfar_exp = puzzle_coarse_association(config,mimo_ffta,mimo_cfar,ppa_cfar)

    # print('mimo_4dfft',mimo_4dfft.shape)

    matlab_comperator.plot()
