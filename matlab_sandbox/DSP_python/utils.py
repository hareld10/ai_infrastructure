# +
import torch
import torch.nn.functional as F
import numpy as np
import pandas as pd
import cv2
from typing import Union
import matplotlib.pyplot as plt
# %matplotlib inline

# -

def mimo_processing(config, array_raw):
    # in: (samples,rx,pulses)

    # Create complex array: (samples,rx,pulses,real/imag)
    array = torch.zeros([array_raw.shape[0], array_raw.shape[1], array_raw.shape[2], 2])
    array[:, :, :, 0] = array_raw

    # Velocity compensation

    # Range processing
    array = range_processing(config.mimo_window_range, array.permute(1, 2, 3, 0))  # in: (rx,pulses,real/imag,samples)  out: (rx,pulses,range,real/imag)

    # Doppler processing
    array = full_doppler_processing(config, array)  # in: (rx,pulses,range,real/imag)  out:(rx,range,doppler,real/imag)

    # MIMO channel calibration

    # Build mimo array
    array = mimo_expansion(config, array)  # in: (rx,range,doppler,real/imag)  out:(range,doppler,ch_az,ch_el,real/imag)

    # TODO: make sure the doppler size for each channel should be n_pulses or n_pulses*n_tx (if so, how build_mimo_array should change?)

    # Angular processing
    array = angular_processing(config, array)  # in: (rx,range,real/imag,pulses)  out: (rx,range,doppler,real/imag)

    if config.use_mimo_cfar:

        if config.cfar_type == 'nci':
            # Get mimo nci
            mimo_nci = nci(array, 'mimo')  # in: (rx,range,doppler,real/imag) out: (range,doppler) [db]

            # Get mimo cfar
            mimo_cfar = cfar(config, config.mimo_cfar_threshold, mimo_nci)  # in: (range,doppler) [db] out: (range,doppler) [idx]

        elif config.cfar_type == 'fft':
            pass


    else:
        mimo_cfar = torch.ones([array.shape[0], array.shape[1]])

    return array, mimo_cfar


def ppa_processing(config, array_raw):
    # in: (samples,rx,pulses)

    # Create complex array: (samples,rx,pulses,real/imag)
    array = torch.zeros([array_raw.shape[0], array_raw.shape[1], array_raw.shape[2], 2])
    array[:, :, :, 0] = array_raw

    # Velocity compensation

    # Range processing
    array = range_processing(config.ppa_window_range, array.permute(1, 2, 3, 0))  # in: (rx,pulses,real/imag,samples) # out: (rx,pulses,range,real/imag)
    config.comperator.add_key(array.clone(), key="S_FFTR")

    # Doppler processing
    array = doppler_processing(config.ppa_window_dop, array.permute(0, 2, 3, 1))  # in: (rx,range,real/imag,pulses) # out: (rx,range,doppler,real/imag)
    config.comperator.add_key(array.clone(), key="S_FFTD")
    # PPA channel calibration

    if config.cfar_type == 'nci':

        # Get ppa nci
        array = nci(array, 'ppa')  # in: (channels,range,doppler,real/imag) out: (range,doppler) [db]

        # Get ppa cfar
        array = cfar(config, config.ppa_cfar_threshold, array)  # in: (range,doppler) [db] out: (range,doppler) [idx]

    elif config.cfar_type == 'fft':

        # Azimuth processing
        array = azimuth_processing(config, array.permute(1, 2, 3, 0))  # in: (range,doppler,real/imag,rx) # out: (range,doppler,rx,real/imag)

    return array


def puzzle_coarse_association(config, mimo_ffta, mimo_cfar, ppa_cfar):
    # Expand ppa
    #     ppa_cfar_exp = (F.conv2d(ppa_cfar[None,None,:,:], config.expansion_kernel[None,None,:,:]) > 0)*1 # TODO: issue with size after conv
    ppa_cfar_exp = ppa_cfar

    # Merge with mimo coarse
    ppa_cfar_exp = F.interpolate(ppa_cfar_exp[None, None, :, :], size=(mimo_cfar.shape[0], mimo_cfar.shape[1])).squeeze()
    #     print('ppa_cfar_exp',ppa_cfar_exp.shape)
    #     print('mimo_cfar.shape',mimo_cfar.shape)

    # Filter mimo ffta
    mimo_ffta = mimo_ffta * mimo_cfar[:, :, None, None, None] * ppa_cfar_exp[:, :, None, None, None]

    return mimo_ffta, ppa_cfar_exp


def get_expansion_kernel(bin_rng_ppa, bin_rng_mimo, bin_dop_ppa, bin_dop_mimo):
    # Expansion due to velocity
    #     tot_sec         = torch.sum([config.Npulses] * [config.PRI_usec]*1e-6) #[sec] overall Time-on-Target
    #     R_dilation_m    = (tot_sec/2)*config.pzl_asc_max_vel_ms
    #     R_dilation_bins = torch.ceil((R_dilation_m/config.bin_rng_ppa)/2)*2
    R_dilation_bins = 0

    # Expansion due to resolution
    rng_expand_ratio = np.int(np.ceil(bin_rng_ppa / bin_rng_mimo))
    dop_expand_ratio = np.int(np.ceil(bin_dop_ppa / bin_dop_mimo))
    kernel = torch.ones([rng_expand_ratio + R_dilation_bins, dop_expand_ratio])

    return kernel.long()


def mimo_expansion(config, array):
    # in: (rx,range,full_doppler,real/imag) out:(range,doppler,ch_az,ch_el,real/imag)

    array_mimo = torch.zeros([config.n_range, config.n_dop, config.n_az, config.n_el, 2])  # 3.5T: (range,doppler,20,6,2)
    for i in range(len(config.df_array)):
        array_mimo[:, :, config.df_array.loc[i, 'virtual_array_row'],
        config.df_array.loc[i, 'virtual_array_row'], :] = array[config.df_array.loc[i, 'rx'], :, config.df_array.loc[i, 'tx'] * config.n_dop:(config.df_array.loc[i, 'tx'] + 1) * config.n_dop, :]

    return array_mimo


def fov_filter(array, fov, axis):
    min_idx = int(np.ceil(array.shape[axis] // 2 - fov // 2 / 180 * array.shape[axis]) - 1)
    max_idx = int(np.floor(array.shape[axis] // 2 + fov // 2 / 180 * array.shape[axis]) + 1)

    if axis == 2:
        return array[:, :, min_idx:max_idx, :, :]
    elif axis == 3:
        return array[:, :, :, min_idx:max_idx, :]


def angular_processing(config, array):
    # in:(range,doppler,ch_az,ch_el,real/imag)

    # Window az
    array = windowing(config.mimo_window_az, array.permute(0, 1, 3, 4, 2))  # (range,doppler,ch_el,real/imag,az)

    # Window el
    array = windowing(config.mimo_window_el, array.permute(0, 1, 4, 3, 2))  # (range,doppler,az,real/imag,el)

    # Permute for fft
    array = array.permute(0, 1, 2, 4, 3)  # (range,doppler,az,el,real/imag)

    # Zero padding
    if not config.zero_pad_az == 0:
        array = F.pad(array, (0, 0, 0, config.n_az * config.zero_pad_az - config.n_az, 0, 0), "constant", 0)  # TODO: make sure api call is correct

    if not config.zero_pad_el == 0:
        array = F.pad(array, (0, 0, 0, 0, 0, config.n_el * config.zero_pad_el - config.n_el), "constant", 0)

    # 2D fft (az,el)
    array = torch.fft(array, 2, normalized=True)  # (range,doppler,az,el,real/imag)

    # fft shift az
    array = torch.roll(array, shifts=array.shape[2] // 2, dims=2)

    # FOV filter az
    array = fov_filter(array, config.az_fov, axis=2)

    # fft shift el
    array = torch.roll(array, shifts=array.shape[3] // 2, dims=3)

    # FOV filter
    array = fov_filter(array, config.el_fov, axis=3)

    return array


def build_full_dop_phase_mat(n_fft_dop, n_tx, n_rx):
    # (channels,range,doppler,real/imag)

    tx_vec = np.arange(0, n_tx)[None, :]
    dop_vec = np.arange(0, n_fft_dop)[:, None]

    phase_mat_np = np.exp(-1j * 2 * np.pi / n_fft_dop * dop_vec * tx_vec)
    phase_mat_np = np.repeat(phase_mat_np, n_rx, axis=1)  # (doppler,channels)

    phase_mat = torch.zeros([1, phase_mat_np.shape[0], phase_mat_np.shape[1], 2])  # (range,doppler,channels,real/imag)
    phase_mat[0, :, :, 0] = torch.from_numpy(np.real(phase_mat_np))
    phase_mat[0, :, :, 1] = torch.from_numpy(np.imag(phase_mat_np))

    return phase_mat.permute(2, 0, 1, 3)  # reorder to (channels,range,doppler,real/imag)


# +
def full_doppler_processing(config, array):
    # in: (channels,pulses,range,real/imag)

    # Windowing
    array = windowing(config.mimo_window_full_doppler, array.permute(0, 2, 3, 1))  # (rx,range,real/imag,pulses)

    # Reorder dimensions
    array = array.permute(0, 1, 3, 2)  # (rx,range,pulses,real/imag)

    #     print('array',array.shape)
    #     print('config.full_dop_phase_mat',config.full_dop_phase_mat.shape)

    # Full doppler phase compensation
    #     array *= config.full_dop_phase_mat # TODO: dimensions do not match

    # fft per Tx
    for i in range(config.n_tx):
        array[:, :, i * config.n_dop:(i + 1) * config.n_dop, :] = torch.fft(array[:, :, i * config.n_dop:(i + 1) * config.n_dop, :], 1)

    return array


# -

def doppler_processing(window_type, array):
    # in: (channels,samples,real/imag,pulses)

    # Windowing
    array = windowing(window_type, array)

    # fft
    array = torch.fft(array.permute(0, 1, 3, 2), 1)  # reorder as (channels,pulses,samples,real/imag)

    return array


def range_processing(window_type, array):
    # in: (channels,pulses,real/imag,samples)

    # Windowing
    array = windowing(window_type, array)

    # fft
    array = torch.fft(array.permute(0, 1, 3, 2), 1)  # reorder as (channels,pulses,samples,real/imag)

    # Truncate range values
    array = array[:, :, :array.shape[2] // 2, :]

    return array


def windowing(window_type, array):
    # Get window
    if window_type == 'han':
        window = torch.hann_window(window_length=array.shape[-1])

    elif window_type == 'hamming':
        window = torch.hamming_window(window_length=array.shape[-1], alpha=config.hamming_alpha, beta=config.hamming_beta)

    elif window_type == 'bartlett':
        window = torch.bartlett_window(window_length=array.shape[-1])

    elif window_type == 'blackman':
        window = torch.blackman_window(window_length=array.shape[-1])

    # Expand to array dimensions
    for i in range(array.dim() - 1):
        window = window.unsqueeze(0)

    return array * window


def torch_percentile(t: torch.tensor, q: float) -> Union[int, float]:
    """
    Return the ``q``-th percentile of the flattened input tensor's data.
    :param t: Input tensor.
    :param q: Percentile to compute, which must be between 0 and 100 inclusive.
    :return: Resulting value (scalar).
    """
    # Note that ``kthvalue()`` works one-based, i.e. the first sorted value
    # indeed corresponds to k=1, not k=0! Use float(q) instead of q directly,
    # so that ``round()`` returns an integer, even if q is a np.float32.
    k = 1 + round(.01 * float(q) * (t.numel() - 1))
    result = t.view(-1).kthvalue(k).values.item()
    return result


def db(array):
    return 10 * torch.log10(array)


# +
def nci(array, sub_frame):
    if sub_frame == 'ppa':
        # in: (channels,range,doppler,real/imag)

        # Convert to db
        nci = db(torch.sqrt(array[:, :, :, 0] ** 2 + array[:, :, :, 1] ** 2))

        # Sum over channels
        nci = nci.sum(0) / array.shape[0]

    elif sub_frame == 'mimo':
        # in: (range,doppler,az,el,real/imag)

        # Convert to db
        nci = db(torch.sqrt(array[:, :, :, :, 0] ** 2 + array[:, :, :, :, 1] ** 2))

        # Sum over channels
        nci = nci.sum(dim=(2, 3)) / (array.shape[2] + array.shape[3])

    #         print('nci',nci.shape)

    return nci


# -

def cfar(config, cfar_threshold, array):
    # in: (range,doppler) [db]

    return (array > (torch_percentile(array, config.cfar_percentile) + cfar_threshold)) * 1.0


def virtual_array(print_layout=False):
    if print_layout:
        plt.figure(figsize=(10, 10))
        plt.imshow(cv2.cvtColor(cv2.imread('/workspace/AI/Itai/DSP/dsp_python/array_3.5T.png'), cv2.COLOR_BGR2RGB))
        plt.xticks([])
        plt.yticks([])
        plt.title('Antenna Array 3.5T')

    df_array_layout = pd.DataFrame()

    ####################################
    # TX 2 (top left)
    tx = 2

    i = 0
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 1, 0
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 0

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 0, 1
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 1

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 1, 2
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 2

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 0, 3
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 3

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 1, 4
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 4

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 0, 5
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 5

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 1, 6
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 6

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 0, 7
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 7

    ####################################
    # TX 5 (top right)
    tx = 5

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 1, 8
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 0

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 0, 9
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 1

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 1, 10
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 2

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 0, 11
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 3

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 1, 12
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 4

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 0, 13
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 5

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 1, 14
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 6

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 0, 15
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 7

    ####################################
    # TX 1 (middle left)
    tx = 1

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 3, 2
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 0

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 2, 3
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 1

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 3, 4
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 2

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 2, 5
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 3

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 3, 6
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 4

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 2, 7
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 5

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 3, 8
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 6

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 2, 9
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 7

    ####################################
    # TX 4 (middle right)
    tx = 4

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 3, 10
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 0

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 2, 11
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 1

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 3, 12
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 2

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 2, 13
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 3

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 3, 14
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 4

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 2, 15
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 5

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 3, 16
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 6

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 2, 17
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 7

    ####################################
    # TX 0 (bottom left)
    tx = 4

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 5, 4
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 0

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 4, 5
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 1

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 5, 6
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 2

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 4, 7
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 3

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 5, 8
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 4

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 4, 9
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 5

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 5, 10
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 6

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 4, 11
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 7

    ####################################
    # TX 3 (bottom right)
    tx = 3

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 5, 12
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 0

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 4, 13
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 1

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 5, 14
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 2

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 4, 15
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 3

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 5, 16
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 4

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 4, 17
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 5

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 5, 18
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 6

    i += 1
    df_array_layout.loc[i, 'virtual_array_row'], df_array_layout.loc[i, 'virtual_array_col'] = 4, 19
    df_array_layout.loc[i, 'tx'], df_array_layout.loc[i, 'rx'] = tx, 7

    return df_array_layout.astype('int8')
