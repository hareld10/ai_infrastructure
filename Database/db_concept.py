from flask import Flask, render_template, request, flash
from flask_sqlalchemy import SQLAlchemy, BaseQuery

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:1@localhost/sandbox'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.secret_key = 'secret string'

db = SQLAlchemy(app)


class Drive(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    drive_name = db.Column(db.String(240), unique=True, nullable=False)
    path = db.Column(db.String(), nullable=False)

    def __init__(self, drive_name, path):
        self.drive_name = drive_name
        self.path = path


if __name__ == '__main__':
    db.create_all()
    # app.run()
    entry = Drive('highway', '/path/to/nothingggg')
    db.session.add(entry)
    db.session.commit()
    q: BaseQuery = db.session.query(Drive)
    all = q.all()
    for first in all:
        print(first.id, first.drive_name, first.path)
