

class Model:
    def __init__(self):
        self.name = "A"
    def __repr__(self):
        return f"model={self.name}"

model = Model()
# model =1
print(model)

def do():
    global model
    model.name = "B"
    # model = 2
    print(model)

do()
print(model)
