import sys
from collections import defaultdict
from multiprocessing import Pool

import cv2
import os
from glob import glob
import pandas as pd
import numpy as np
from matplotlib import gridspec, colors
import matplotlib.pyplot as plt
from DataPipeline.shared_utils import PrintException, toRAzEl
from wiseTypes.Cluster import Cluster
from wiseTypes.Types import DebugTypes
import matplotlib

module_path = os.path.abspath('')
sys.path.insert(0, module_path)
sys.path.insert(0, module_path + "/../")
from LabelsManipulation.camera_lidar_fusion import CameraLidarFusion
from DataPipeline.src.GetEngines import get_engines
from OpenSource.WisenseEngine import WisenseEngine
from tqdm import tqdm


def get_2d(engine, idx):
    return engine.get_2d_label(idx, force_obj=True)


def get_3d(engine, idx):
    return engine.get_lidar_camera_fusion_labels(idx=idx, as_obj=True)


# alg = CameraLidarFusion(data_engine=None)


def gen_3d_dynamically(engine, idx):
    alg.data_engine = engine
    processes_labels_2d = alg.run_engine_idx(engine, idx)
    processed_delta = engine.get_processed_delta(idx)
    delta_clusters = engine.get_clusters(p_delta=processed_delta)
    for p_l_idx, p_label in enumerate(processes_labels_2d):
        if "3d_center_clustering" not in p_label.meta_data: continue

        l_cluster = Cluster(cluster_df=p_label.debug_data[DebugTypes.predicted_cluster_from_bb], power_key=None)
        best_iou_idx, best_iou_val = l_cluster.get_best_iou(list(delta_clusters.values()))
        best_dist_idx, best_dist_val = l_cluster.get_best_distance(list(delta_clusters.values()))
        best_cross_dist_idx, best_cross_dist_val = l_cluster.get_best_cross_distance(list(delta_clusters.values()))

        tx, ty, tz = l_cluster.get_3d_center()
        p_label.meta_data["label_tx"] = tx
        p_label.meta_data["label_ty"] = ty
        p_label.meta_data["label_tz"] = tz

        cx, cy = l_cluster.get_bounding_box_center()
        p_label.meta_data["label_bounding_cx"] = cx
        p_label.meta_data["label_bounding_cy"] = cy

        p_label.meta_data["cluster_diameter"] = l_cluster.max_diameter
        p_label.meta_data["label_max_diameter"] = p_label.obj.m_max_cluster_diameter

        p_label.meta_data["2d_iou_val"] = best_iou_val
        p_label.meta_data["2d_iou_idx"] = best_iou_idx
        p_label.meta_data["2d_iou_cluster"] = delta_clusters[best_iou_idx].get_3d_center().tolist()

        p_label.meta_data["3d_distance_val"] = best_dist_val
        p_label.meta_data["3d_distance_idx"] = best_dist_idx
        p_label.meta_data["3d_distance_cluster"] = delta_clusters[best_dist_idx].get_3d_center().tolist()

        p_label.meta_data["2d_center_bounding_box_val"] = best_cross_dist_val
        p_label.meta_data["2d_center_bounding_box_idx"] = best_cross_dist_idx
        p_label.meta_data["2d_center_bounding_box__cluster"] = delta_clusters[best_cross_dist_idx].get_3d_center().tolist()

        p_label.meta_data["timestampRadar"] = engine.get_id(idx)
        p_label.meta_data["timestampLidar"] = engine.get_lidar_id(idx)
        p_label.meta_data["timestampCamera"] = engine.get_camera_id(idx)

    return processes_labels_2d


def gen_df_statistics(engine, func="2d", name="instances_3d_analysis_diameter"):
    if func == "2d":
        func = get_2d
    elif func == "3d":
        func = get_3d
    else:
        print("must define func")
        return
    part_count = 1
    destination = engine.drive_context.aux_path + "/" + name + ".csv"
    [os.remove(x) for x in glob(engine.drive_context.aux_path + "/" + name + "_part*")]
    instances_df = pd.DataFrame()
    for engine_idx in tqdm(range(len(engine))):
        try:
            input_lst = func(engine, engine_idx)
            for label in input_lst:
                instances_df = instances_df.append({**label.get_dict(add_meta=False), **label.meta_data}, ignore_index=True)

            instances_df = instances_df.drop(columns=["3d_center_clustering", "3d_dimensions", "3d_heading", "bb_com", "bb_median_range_to_obj", "prediction_score"],
                                             errors="ignore")
            if len(instances_df) > 40000:
                print("writing part", part_count)
                instances_df.to_csv(engine.drive_context.aux_path + "/" + name + "_part_" + str(part_count) + ".csv")
                part_count += 1
                instances_df = pd.DataFrame()
        except Exception as e:
            PrintException()
            print("error", e)
            continue

    instances_df.to_csv(engine.drive_context.aux_path + "/" + name + "_part_" + str(part_count) + ".csv")
    dfs = [x for x in glob(engine.drive_context.aux_path + "/" + name + "_part*")]
    instances_df = pd.concat([pd.read_csv(x) for x in dfs])
    instances_df = instances_df.reset_index(drop=True)
    instances_df.drop(instances_df.filter(regex="Unname"), axis=1, inplace=True)
    instances_df.reset_index(drop=True).to_csv(destination)
    [os.remove(x) for x in glob(engine.drive_context.aux_path + "/" + name + "_part*")]
    return instances_df


def gen_2d_fail_cases(analysis_df):
    fail_2d = analysis_df[~analysis_df.label_bounding_cx.notna()]
    fail_2d.loc[:, "2d_size"] = np.sqrt(fail_2d["w"].values + fail_2d["h"].values)

    # 2D Fail Cases
    plt.close()
    fig = plt.figure(figsize=(20, 10))
    gs = gridspec.GridSpec(ncols=3, nrows=1)

    ax = fig.add_subplot(gs[0, 0])
    ax.set_yscale("log")
    fail_2d["2d_size"].plot(kind="hist", ax=ax)
    title = "2D BBOX size | sqrt(h*w)"
    ax.set_title(title)

    ax = fig.add_subplot(gs[0, 1])
    ax.set_yscale("log")
    fail_2d["w"].plot(kind="hist", ax=ax)
    title = "2D BBOX width"
    ax.set_title(title)

    ax = fig.add_subplot(gs[0, 2])
    ax.set_yscale("log")
    title = "2D BBOX height"
    ax.set_title(title)
    fail_2d["h"].plot(kind="hist", ax=ax)

    plt.suptitle("2D Fail cases")
    plt.tight_layout(pad=2)
    plt.savefig("./figures/fail_cases_2D_BBOX.jpg")
    plt.close()


def gen_fraction_of_useable(analysis_df):
    # fraction of usuable
    ious = np.linspace(0.05, 0.4, 10)
    bbox_centers = np.linspace(0.1, 1, 10)

    metric_vecs = {"large_vehicle": {"ious": np.linspace(0.05, 0.5, 10),
                                     "bbox_centers": np.linspace(0.1, 2, 10)},
                   "vehicle": {"ious": np.linspace(0.05, 0.5, 10),
                               "bbox_centers": np.linspace(0.1, 1, 10)},
                   "pedestrian": {"ious": np.linspace(0.05, 0.5, 10),
                                  "bbox_centers": np.linspace(0.1, 0.8, 10)}, }

    classes = ["vehicle", "pedestrian", "large_vehicle"]
    font = {'size': 22}
    matplotlib.rc('font', **font)
    plt.close()

    for metric in ["and", "or"]:
        fig, axes = plt.subplots(1, 3, figsize=(25, 12))  # , sharex=True, sharey=True)
        axes = axes.flatten()
        for ax_idx, cat in enumerate(classes):
            ax = axes[ax_idx]
            vals = np.zeros((len(metric_vecs[cat]["ious"]), len(bbox_centers)))
            bad_df = analysis_df[analysis_df["cat"] == cat].reset_index(drop=True)
            bad_df = bad_df[bad_df["dz"] <= 5]
            for iou_idx, iou in enumerate(metric_vecs[cat]["ious"]):
                for b_idx, bbox_center_dist in enumerate(metric_vecs[cat]["bbox_centers"]):
                    if metric == "or":
                        frac_of_useable = len(bad_df[(bad_df["2d_iou_val"] >= iou) | (bad_df["2d_center_bounding_box_val"] <= bbox_center_dist)]) / len(bad_df)
                    elif metric == "and":
                        frac_of_useable = len(bad_df[(bad_df["2d_iou_val"] >= iou) & (bad_df["2d_center_bounding_box_val"] <= bbox_center_dist)]) / len(bad_df)
                    else:
                        print("error")
                    vals[b_idx, iou_idx] = frac_of_useable

            ax.set_xlabel("iou")
            ax.set_ylabel("2d-com-distance [m]")
            scat = ax.pcolormesh(metric_vecs[cat]["ious"], metric_vecs[cat]["bbox_centers"], vals, shading='gouraud', vmin=vals.min(), vmax=vals.max(), cmap="turbo")
            plt.colorbar(scat, ax=ax, label="percentage out of dataset")
            ax.set_title(cat)

        plt.suptitle("fraction of usuable data | %s logic" % metric)
        plt.tight_layout(pad=2)
        plt.savefig("./figures/fraction_usuable_%s.jpg" % metric)
        plt.close()
    pass


def generate_labels_3d_csv(engines):
    keys = ["cat", "projected_3d_center_clustering", "size_estimation",
            "fail_cause", "score", "m_max_cluster_diameter", "best_iou_val", "best_size_estimation_iou_val",
            "best_3d_cog_distance", "best_3d_com_distance", 'bbox', "associate_cluster_id", "sub_cluster_id",
            "num_points_in_lidar_cluster", "size_estimation_score", "m_max_cluster_diameter_vel"]
    for engine in engines:
        dfs = []
        for idx in tqdm(range(len(engine))):
            labels_3d = engine.get_lidar_camera_fusion_labels(idx=idx, as_obj=False)
            cur_frame = []
            for label in labels_3d:
                d = defaultdict(lambda: -1, label)
                row = {k:d[k] for k in keys}
                row["timestampRadar"] = engine.get_id(idx)
                row["timestampLidar"] = engine.get_lidar_id(idx)
                row["timestampCamera"] = engine.get_camera_id(idx)
                if type(d["projected_3d_center_clustering"]) == int:
                    d["projected_3d_center_clustering"] = [-1, -1, -1]
                row["x"] = d["projected_3d_center_clustering"][0]
                row["y"] = d["projected_3d_center_clustering"][1]
                row["z"] = d["projected_3d_center_clustering"][2]
                row["range"], row["az"], row["el"] = toRAzEl(row["x"] , row["y"], row["z"])
                cur_frame.append(row)
            dfs.append(pd.DataFrame(cur_frame))
        derive_df = pd.concat(dfs).reset_index(drop=True)
        path = f'{engine.drive_context.aux_path}/labels_3d.csv'
        derive_df.to_csv(path)
        print("saved", path)

def generate_labels_2d_csv(engines):
    keys = ["cat", "bbox", "score"]
    for engine in engines:
        dfs = []
        for idx in tqdm(range(len(engine))):
            labels_3d = engine.get_2d_label(idx=idx, force_obj=False)
            cur_frame = []
            for label in labels_3d:
                d = defaultdict(lambda: -1, label)
                row = {k:d[k] for k in keys}
                row["timestampRadar"] = engine.get_id(idx)
                row["timestampLidar"] = engine.get_lidar_id(idx)
                row["timestampCamera"] = engine.get_camera_id(idx)
                if type(d["projected_3d_center_clustering"]) == int:
                    d["projected_3d_center_clustering"] = [-1, -1, -1]
                row["x1"] = d["bbox"][0]
                row["y1"] = d["bbox"][1]
                row["x2"] = d["bbox"][2]
                row["y2"] = d["bbox"][3]
                cur_frame.append(row)
            dfs.append(pd.DataFrame(cur_frame))
        derive_df = pd.concat(dfs).reset_index(drop=True)
        path = f'{engine.drive_context.aux_path}/labels_2d.csv'
        derive_df.to_csv(path)
        print("saved", path)

def gen_counts_r_az_plots(df_2d=None, df_3d=None, df_instances=None):
    engines = get_engines(wf="92", randomize=False)
    # if df_2d is None:
    #     df_2d = pd.concat([pd.read_csv(x.drive_context.instances_2d_df) for x in engines])
    if df_instances is None:
        df_instances = WisenseEngine.concat_dfs(engines, suffix="/instances_3d_df.csv")
    if df_3d is None:
        df_3d = WisenseEngine.concat_dfs(engines, suffix="/labels_3d.csv")

    # Instances count
    plt.figure(figsize=(15, 10))
    plt.yscale("log")
    ax = plt.gca()
    df = pd.DataFrame({'2d labels': df_3d["cat"].value_counts(),
                       '3d_labels': df_3d[df_3d.projected_3d_center_clustering != '-1']["cat"].value_counts(),
                       "classification": df_instances["cat"].value_counts()}, index=df_3d["cat"].value_counts().index)
    df = df.drop(["motorbiker", "cyclist"])
    df.plot.bar(ax=ax)
    plt.suptitle("Instances counts | log scale")
    plt.tight_layout(pad=2)
    plt.savefig("./figures/instances comparison.jpg")
    plt.close()

    # Azimuth
    fig = plt.figure(figsize=(30, 10))
    gs = gridspec.GridSpec(ncols=3, nrows=1)

    # 2D
    ax = fig.add_subplot(gs[0, 0])
    # title = "2D labels x-distribution"
    # df_2d["cx"].plot(kind="hist", ax=ax)
    # ax.set_title(title)
    # plt.tight_layout(pad=2)
    #
    # # 3D
    # ax = fig.add_subplot(gs[0, 1])
    title = "3D labels azimuth-distribution"
    df_3d["az"].plot(kind="hist", ax=ax)
    ax.set_title(title)
    plt.tight_layout(pad=2)

    # instances
    ax = fig.add_subplot(gs[0, 2])
    title = "Classification azimuth-distribution"
    df_instances["az"].plot(kind="hist", ax=ax)
    ax.set_title(title)
    plt.tight_layout(pad=2)

    plt.savefig("./figures/az_dist.jpg")
    plt.close()

    # Range
    fig = plt.figure(figsize=(30, 10))
    gs = gridspec.GridSpec(ncols=2, nrows=1)

    # 2D
    ax = fig.add_subplot(gs[0, 0])
    title = "3D labels range-distribution"
    df_3d["range"].plot(kind="hist", ax=ax)
    ax.set_title(title)
    plt.tight_layout(pad=2)

    # instances
    ax = fig.add_subplot(gs[0, 1])
    title = "Classification range-distribution"
    df_instances["r"].plot(kind="hist", ax=ax)
    ax.set_title(title)
    plt.tight_layout(pad=2)

    plt.savefig("./figures/r_dist.jpg")
    plt.close()


def write_open_source(df_instances, name):
    df = df_instances[df_instances["ty"] >= 0]
    # print(len(df))
    df = df[(df["az"] >= -40) & (df["az"] <= 40)]
    # print(len(df))
    df = df[df["cat"] != "sign"]
    # print(len(df))

    plt.figure(figsize=(12, 10))
    ax = plt.gca()
    df["cat"].value_counts()
    df["range"].plot(kind="hist", ax=ax)
    ax.set_title("3D range-distribution | %s | len=%d" % (name, len(df)))
    ax.set_xlabel("range [m]")
    plt.tight_layout(pad=2)
    plt.savefig("./figures/%s_ranges.jpg" % name)

    plt.close()
    plt.figure(figsize=(12, 10))
    ax = plt.gca()
    df["az"].plot(kind="hist")
    ax.set_title("3D azimuth-distribution | %s | len=%d" % (name, len(df)))
    ax.set_xlabel("azimuth [deg]")
    plt.tight_layout(pad=4)
    plt.savefig("./figures/%s_azimuths.jpg" % name)
    plt.close()
    return


def open_source_figures():
    df = pd.read_csv("/media/amper/wiseData11/waymo/instances_3d_df.csv")
    write_open_source(df, name="waymo")

    df = pd.read_csv("/media/amper/open_source5/audi/instances_3d_df.csv")
    write_open_source(df, name="audi")


def preprocess_df(df):
    df.loc[:, "2d_iou_cluster"] = df.loc[:, "2d_iou_cluster"].apply(lambda x: np.fromstring(x[1:-1], sep=",")).values
    df.loc[:, "2d_center_bounding_box__cluster"] = df.loc[:, "2d_center_bounding_box__cluster"].apply(lambda x: np.fromstring(x[1:-1], sep=",")).values
    df.loc[:, "projected_3d_center_clustering"] = df.loc[:, "projected_3d_center_clustering"].apply(lambda x: np.fromstring(x[1:-1], sep=",")).values
    x1 = np.stack(np.asarray(df.loc[:, "2d_iou_cluster"].values), axis=0)
    x2 = np.stack(np.asarray(df.loc[:, "2d_center_bounding_box__cluster"].values), axis=0)
    x3 = np.stack(np.asarray(df.loc[:, "projected_3d_center_clustering"].values), axis=0)
    df.loc[:, "dz"] = np.minimum(np.abs(x1[:, 2] - x3[:, 2]), np.abs(x2[:, 2] - x3[:, 2]))
    return df


def metric1(df, filtered_df):
    nc = len(filtered_df)
    Nc = len(df)
    return nc / Nc


def metric2(df, filtered_df):
    nc = len(filtered_df)
    bc = len(filtered_df[filtered_df["cluster_diameter"] <= filtered_df["label_max_diameter"]])
    # print(filtered_df["label_max_diameter"])
    return bc / nc


def analyze_association_and_plot(df):
    cat = "vehicle"
    metric_df = pd.DataFrame()
    for r in np.linspace(0.1, 1, 10):
        cur_df = df[df["cat"] == cat].reset_index(drop=True)
        cur_df = cur_df[cur_df["dz"] <= 5].reset_index(drop=True)
        filtered_df = cur_df[(cur_df["3d_distance_val"] <= 0.5) |
                             ((cur_df["2d_iou_val"] >= r) | (cur_df["2d_center_bounding_box_val"] <= 0.6))]

        m1 = metric1(df=cur_df, filtered_df=filtered_df)
        m2 = metric2(df=cur_df, filtered_df=filtered_df)
        metric_df.loc[r, "m1"] = m1
        metric_df.loc[r, "m2"] = m2

    res = metric_df
    fig = plt.figure(figsize=(15, 7))
    gs = gridspec.GridSpec(ncols=2, nrows=1)
    ax = fig.add_subplot(gs[0, 0])
    ax.set_title("Metric1 ~ Recall")
    ax.set_ylabel("M1")
    ax.set_xlabel("IOU")
    res["m1"].plot(ax=ax)

    ax = fig.add_subplot(gs[0, 1])
    ax.set_title("Metric2 ~ Precision")
    res["m2"].plot(ax=ax)
    ax.set_ylabel("M2")
    ax.set_xlabel("IOU")
    plt.suptitle("Association Analysis | Vehicle\n3d-d<=0.5m || 2d-com<=0.6")
    plt.tight_layout()
    plt.savefig("./figures/association_metrics.jpg")

    return metric_df


def main():
    engines = get_engines(wf="92", val_train="all", randomize=False)

    # for engine in engines:
    #     gen_df_statistics(engine)
    #     exit()

    # Gen 2D statistics
    with Pool(6) as p:
        p.map(gen_df_statistics, engines)

    # for engine in engines:
    #     print(engine.get_drive_name())
    #     gen_2d_statistics(engine)
    pass


if __name__ == '__main__':
    # analyze_association_and_plot()
    main()
