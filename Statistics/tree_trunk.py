#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import glob
from pathlib import Path
from tqdm import tqdm
import pandas as pd
import numpy as np
from matplotlib import gridspec, colors
import matplotlib.pyplot as plt
from IPython.display import clear_output
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import PercentFormatter
from scipy import stats
from PIL import Image, ImageEnhance
import cv2
get_ipython().run_line_magic('matplotlib', 'inline')
plt.style.use('dark_background')
import sys
from pprint import pprint
module_path = os.path.abspath('')
sys.path.insert(0, module_path)
import time
sys.path.insert(0, module_path + "/../")
from Calibrator.calibrator_utils import process_lidar_to_image_on_ax_df
from Calibrator.projection import toXYZ
from Evaluation.evaluation_utils import make_video_from_dir
from DataPipeline.src.GetEngines import get_engines
from OpenSource.WisenseEngine import WisenseEngine
from Plotting.wise_plotter import add_rcs

clear_output(wait=False)
import matplotlib
font = {'size'   : 22}

matplotlib.rc('font', **font)
from scipy.interpolate import griddata
matplotlib.rcParams['figure.figsize'] = (15, 7)
def get_cmap(num_colors, name='hsv'):
    '''Returns a function that maps each index in 0, 1, ..., n-1 to a distinct
    RGB color; the keyword argument name must be a standard mpl colormap name.'''
    return plt.cm.get_cmap(name, num_colors)


cmap_colors = get_cmap(20)


# In[3]:


engines = get_engines(val_train="all", randomize=False)
engine = engines[2]
# engine.sample_df(50)
# engine_idx = np.random.randint(len(engine))
engine_idx = 4500
print(engine_idx, engine.get_drive_name())

im = engine.get_image(engine_idx)
seg = engine.get_segmentation_label(engine_idx)
lidar_df = engine.get_lidar_df(engine_idx)
lidar_df = lidar_df.apply(pd.to_numeric, errors='coerce')
veg_mask_bool = (seg.label == 19).astype(np.uint8)
veg_mask = np.expand_dims(veg_mask_bool, axis=-1)
veg_mask = np.repeat(veg_mask, 3, axis=-1)
im2 = im * veg_mask

# plt.close()
# plt.imshow(im2)
# plt.savefig("//home/amper/AI/Harel/AI_Infrastructure/Statistics/tree_trunks/veg_only" + str(engine.get_drive_name()) + "_" + str(engine_idx) + ".png")

plt.close()

fig = plt.figure(figsize=(30, 20), dpi=200)
gs = gridspec.GridSpec(ncols=3, nrows=3)

ax = fig.add_subplot(gs[0, 0])
ax.imshow(im)

ax = fig.add_subplot(gs[0, 1])
ax.imshow(im2)

ax = fig.add_subplot(gs[0, 2])
ax.imshow(seg.get_img(colors=True))

ax = fig.add_subplot(gs[1, 0])
process_lidar_to_image_on_ax_df(display_df=lidar_df, im=im, ax=ax, points_size=2)
ax.imshow(im)

ax = fig.add_subplot(gs[1, 2])
hsv_im = cv2.cvtColor(im2, cv2.COLOR_RGB2HSV)
# process_lidar_to_image_on_ax_df(display_df=lidar_df, im=im, ax=ax, points_size=2, semantic=True)
hsv_im[..., 2]+=50
hsv_im[..., 1]+=5
ax.imshow(cv2.cvtColor(hsv_im, cv2.COLOR_HSV2RGB))


hough_im = im.copy()

gray = cv2.cvtColor(hough_im,cv2.COLOR_BGR2GRAY)
gray = gray*veg_mask[..., 0]
edges = cv2.Canny(gray,200,500,apertureSize = 3)
ax = fig.add_subplot(gs[2, 1])
# edges = edges*veg_mask[..., 0]
ax.imshow(edges)

# minLineLength = 5000
# maxLineGap =  5
# lines = cv2.HoughLinesP(edges,1,np.pi/180,1,minLineLength,maxLineGap)
# if lines is not None:
#     print(lines.shape)
#     for elem in lines:
#         x1,y1,x2,y2 = elem[0]
#         cv2.line(hough_im,(x1,y1),(x2,y2),(0,255,0),10)
    
lines = cv2.HoughLines(edges,1,np.pi/180,80)
if lines is not None:
#     print(lines.shape)
#     print(lines[1])
    for elem in lines:
        rho,theta = elem[0]
        print(theta)
        a = np.cos(theta)
        b = np.sin(theta)
        x0 = a*rho
        y0 = b*rho
        x1 = int(x0 + 2000*(-b))
        y1 = int(y0 + 2000*(a))
        x2 = int(x0 - 2000*(-b))
        y2 = int(y0 - 2000*(a))
        cv2.line(hough_im,(x1,y1),(x2,y2),(0,0,255),2)
    
ax = fig.add_subplot(gs[1, 1])
ax.imshow(hough_im*veg_mask)

plt.show()
# plt.figure(figsize=(20, 10))
# plt.subplot(121)
# plt.imshow(im2)
# plt.subplot(122)
# plt.imshow(dst)


# In[ ]:


# engine = engines[-2]
# print(len(engine))
# engine_idx = 6750

for engine in engines:
    engine.sample_df(50)
    for engine_idx in range(len(engine)):
        im = engine.get_image(engine_idx)
        seg = engine.get_segmentation_label(engine_idx)
        veg_mask_bool = (seg.label == 19).astype(np.uint8)
        
#         hsv = cv2.cvtColor(im, cv2.COLOR_BGR2HSV)
#         greenMask = cv2.inRange(hsv, (30, 10, 30), (85, 100, 255))
#         brownMask = cv2.inRange(hsv, (10, 10, 30), (30, 100, 255))
#         hsv[:,:,1] = 255
#         im = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)

        if veg_mask_bool.sum() < 5000:
            continue

        lidar_df = engine.get_lidar_df(engine_idx)
        lidar_df = lidar_df.apply(pd.to_numeric, errors='coerce')
        processed_delta = engine.get_processed_delta(engine_idx)
        delta_clusters = engine.get_clusters(p_delta=processed_delta)

        veg_mask = np.expand_dims(veg_mask_bool, axis=-1)
        veg_mask = np.repeat(veg_mask, 3, axis=-1)
        im2 = im * veg_mask

#         kernel = cv2.getGaussianKernel(15, 5)
#         window = np.outer(kernel, kernel.transpose())
#         dst = cv2.filter2D(im2,-1,kernel)

        hsv_im = cv2.cvtColor(im2, cv2.COLOR_RGB2HSV)

        hsv_im[:, :, 0] = np.interp(hsv_im[:, :, 0], (0, 179), (0, 359))
        hsv_im[:, :, 1] = np.interp(hsv_im[:, :, 1], (0, 255), (0, 100))
        hsv_im[:, :, 2] = np.interp(hsv_im[:, :, 2], (0, 255), (0, 100))
        lower_bound = (20, 0, 0)
        upper_bound = (40, 100, 100)
        
#         lower_bound = (20, 20, 35)
#         upper_bound = (40, 100, 100)
        trunk_mask = cv2.inRange(hsv_im, lower_bound, upper_bound)

        plt.close()
        fig = plt.figure(figsize=(25, 15), dpi=200)
        gs = gridspec.GridSpec(ncols=3, nrows=3)
        

        # OD
        ax = fig.add_subplot(gs[0, 0])
        ax.imshow(im)
        ax = fig.add_subplot(gs[1, 0])
        ax.imshow(im2)
        ax = fig.add_subplot(gs[2, 0])
        ax.imshow(seg.get_img(colors=True))
        
        pil_im = Image.fromarray(im2)
        
        e = ImageEnhance.Sharpness(pil_im)
        p_im1 = e.enhance(1)
        e = ImageEnhance.Color(p_im1)
        p_im1 = e.enhance(0.5)
        ax = fig.add_subplot(gs[0, 1])
        ax.imshow(p_im1)
        
        e = ImageEnhance.Sharpness(pil_im)
        p_im2 = e.enhance(3)
        e = ImageEnhance.Color(p_im2)
        p_im2 = e.enhance(0.5)
        ax = fig.add_subplot(gs[1, 1])
        ax.imshow(p_im2)
        
        ax = fig.add_subplot(gs[:, 2])
        ax.imshow(trunk_mask)
        plt.tight_layout()
        plt.savefig("//home/amper/AI/Harel/AI_Infrastructure/Statistics/tree_trunks/" + str(engine.get_drive_name()) + "_" + str(engine_idx) + ".png")


# In[ ]:


# veg_mask_bool = (seg.label == 19).astype(np.uint8)
# contours, hierarchy = cv2.findContours(veg_mask_bool, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
# cls_im = np.zeros((seg.label.shape[0], seg.label.shape[1], 3), dtype=np.uint8)

# hierarchy = hierarchy[0] # get the actual inner list of hierarchy descriptions
# # rel_index = max(enumerate(contours), key = lambda x: cv2.contourArea(x[1]))[0]

# colors_m = [(0, 0, 255), (0, 255, 0), (255, 0, 0), (255, 255, 0),(0, 255, 255),]
# plt_color = 0

# arr = []
# for component in zip(contours, hierarchy):
#     currentContour = component[0]
#     currentHierarchy = component[1]
    
#     if cv2.contourArea(currentContour) > 5000:
#         img = cv2.drawContours(img, [currentContour],  -1,  (0, 255, 0), -1)        
#         sq = currentContour.squeeze().T
#         hsv_im = cv2.cvtColor(im, cv2.COLOR_RGB2HSV)
#         final_shape = hsv_im[sq[1, :], sq[0, :], :]
# #         cls_im[sq[1, :], sq[0, :]] = 1
#         arr.append(final_shape)
#         cls_im = cv2.drawContours(cls_im, [currentContour],  -1,  colors_m[plt_color], -1)
#         plt_color+=1
            
# #     if currentHierarchy[3] == rel_index and cv2.contourArea(currentContour) > 2000:
# #         print("hi")
# #         x,y,w,h = cv2.boundingRect(currentContour)
# #         img = cv2.drawContours(img, [currentContour],  -1,  (0, 255, 0), -1)
# #         if currentHierarchy[2] < 0:
# #             cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),3)
# #         elif currentHierarchy[3] < 0:
# #             cv2.rectangle(img,(x,y),(x+w,y+h),(0,255,0),3)

# plt.subplot(121)
# plt.imshow(im)
# plt.imshow(img, alpha=0.3)
# plt.subplot(122)
# plt.imshow(cls_im)

