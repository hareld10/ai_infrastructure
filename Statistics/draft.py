#         filtered_df = mask_df.copy()
#         print("before", len(filtered_df))
#         desc = filtered_df["lap"].describe()
#         scale = 1/20
#         filtered_df = filtered_df[(filtered_df["lap"] <= (filtered_df["lap"].mean() + filtered_df["lap"].std()*scale)) &
#                                  (filtered_df["lap"] >= (filtered_df["lap"].mean() - filtered_df["lap"].std()*scale))]
#         print("after", len(filtered_df))
#         filtered_df = filtered_df.reset_index(drop=True)

#         scat = ax.scatter(filtered_df["x"], filtered_df["y"], c=filtered_df["z"], cmap="turbo", norm=h_cNorm)
#         ax.scatter(road_seg_df["x"], road_seg_df["y"], s=0.1, c="r")
#         plt.colorbar(scat, ax=ax, orientation='horizontal')


###############################
# x,y cubic
#         method="cubic"
#         f = interpolate.interp2d(radar_df["x"], radar_df["y"], radar_df["z"], kind='cubic', fill_value=np.nan)
#         xi_grid = np.linspace(x_min, x_max, int(np.sum(np.absolute([x_min, x_max])) / x_bin))
#         yi_grid = np.linspace(y_min, y_max, int(np.sum(np.absolute([y_min, y_max])) / x_bin))
#         xi, yi = np.meshgrid(xi_grid, yi_grid)
#         zi = f(xi_grid, yi_grid)
#         zi = zi.clip(-5, max_range)

#         # x,y cubic: BEV
#         ax = fig.add_subplot(gs[1, 2])
#         cNorm = colors.Normalize(vmin=z[~np.isnan(z)].min(), vmax=z[~np.isnan(z)].max())
#         x, y, z = (xi.flatten(), yi.flatten(), zi.flatten())
#         scat = ax.scatter(x, y, c=z, cmap="turbo", s=1, norm=cNorm)
#         plt.colorbar(scat, ax=ax, orientation='horizontal')
#         scat = ax.scatter(radar_df["x"], radar_df["y"], s=1, c="r") #c=radar_df["z"], norm=cNorm,  marker="X",  cmap="turbo")
#         ax.set_title("BEV | x,y | interp=cubic | cmap=z [m]", pad=20)
#         ax.set_xlim(-40, 40)
#         ax.set_ylim(0, max_range)

#         # x,y image
#         ax = fig.add_subplot(gs[0, 1])
#         u, v =  engine.drive_context.camera_obj.Cart2Image(x=x, y=y, z=z)#, D=engine.drive_context.D)
#         r = np.linalg.norm([x, y, z], axis=0)
#         scat = ax.scatter(u, v, c=r, cmap="turbo")
#         plt.colorbar(scat, ax=ax, orientation='vertical', fraction=0.026, pad=0.08)
#         ax.imshow(im, alpha=0.6)
#         ax.set_title("Depth Map | x,y | interp=linear | cmap=r [m]\n min_range=" + str(round(r[~np.isnan(r)].min(), 2)) + ",max_range=" + str(round(r[~np.isnan(r)].max(), 2)), pad=30)


# zi = np.nan_to_num(zi, nan=0)
# z_map = z.reshape(zi.shape)
# ax = fig.add_subplot(gs[1, 1])
# plt.axis(False)
# scat = ax.imshow(z_map, cmap="turbo")
# ax.set_title("Height Map | cmap=z [m]")
# plt.colorbar(scat, ax=ax, fraction=0.026)


# RANSAC Plane
# ax = fig.add_subplot(gs[0, 5], projection='3d')
# scat = ax.scatter(road_seg_df["x"], road_seg_df["y"], road_seg_df["z"], c=distances, cmap="jet")
# # plt.colorbar(scat, ax=ax, orientation='horizontal')
# ax.plot_surface(xi, yi, z_func(xi, yi))
# # ax.view_init(0, 0)
# ax.set_title("Predicted Plane", pad=15)
# ax.set_xlim(-40, 40)
# ax.set_ylim(0, max_range)
# ax.set_xlabel('X [m]\n', labelpad=5)
# ax.set_ylabel('Y [m]', labelpad=5)

# Histogram od distances
# ax = fig.add_subplot(gs[1, 4], projection='3d')
# scat = ax.scatter(road_seg_df["x"], road_seg_df["y"], road_seg_df["z"], c=distances, cmap="jet")
# plt.colorbar(scat, ax=ax, orientation='horizontal')
# ax.plot_surface(xi, yi, z_func(xi, yi))
# ax.view_init(0, 90)
# ax.set_title("Predicted Plane", pad=15)
# ax.set_xlim(-40, 40)
# ax.set_ylim(0, max_range)
# ax.set_xlabel('X [m]\n', labelpad=5)
# ax.set_ylabel('Y [m]', labelpad=5)


# Define DataFrame for plane points
# lap = cv2.Laplacian(zi, cv2.CV_64F, ksize=1)
# gradients = np.gradient(zi)
# coordinates = peak_local_max(np.nan_to_num(zi, np.inf), min_distance=10, exclude_border=True, num_peaks=100)
# print(coordinates)
# z_mask = np.zeros_like(zi)
# z_mask[coordinates[:, 0], coordinates[:, 1]] = 1
# grid_df = pd.DataFrame(data={"x": x, "y": y, "z": z, "u": u, "v": v, "gradients": np.sum(gradients, axis=0).flatten(),
#                              "xi": xi.flatten(), "yi": yi.flatten(), "zi": zi.flatten(),
#                              #"peak": z_mask.flatten(),
#                              "r": np.linalg.norm([x, y, z], axis=0), "lap": np.absolute(lap.flatten())})

# grid_df = grid_df[seg.get_mask_of_points(grid_df[["u", "v"]].T.to_numpy(), cls=Road)].reset_index(drop=True)