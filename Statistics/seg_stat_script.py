import os
import glob
from pathlib import Path
from tqdm import tqdm
import pandas as pd
import numpy as np
from matplotlib import gridspec, colors
import matplotlib.pyplot as plt
# from IPython.display import clear_output
from matplotlib.ticker import PercentFormatter
from collections import defaultdict

from Calibrator.calibrator_utils import save_obj
from wiseTypes.classes_dims import Road, WiseClassesSegmentation

wise_classes = WiseClassesSegmentation()
plt.style.use('dark_background')
import sys
import cv2
module_path = os.path.abspath('')
sys.path.insert(0, module_path)
sys.path.insert(0, module_path + "/../")
from DataPipeline.src.GetEngines import get_engines
from OpenSource.WisenseEngine import WisenseEngine
engines = get_engines()
road = Road()
dilate_factor = 1
stat = defaultdict(int)

for engine in engines:
    # engine.df = engine.df.sample(5).reset_index(drop=True)

    for idx in tqdm(range(len(engine))):
        seg = engine.get_segmentation_label(idx=idx)

        road_mask = cv2.UMat((seg.label == road.unified_idx).astype(np.uint8))
        contours, hierarchy = cv2.findContours(road_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        contours = [max([(c, c.get().shape[0]) for c in contours], key=lambda x: x[1])[0]]

        # contours = [c for c in contours if c.get().shape[0] > 100]
        empty_mat = cv2.UMat(np.zeros((seg.label.shape[0], seg.label.shape[1], 3), dtype=np.uint8))
        empty_mat = cv2.drawContours(empty_mat, contours, -1, (0, 255, 0), 3)
        result = cv2.UMat.get(empty_mat)
        kernel = np.ones((dilate_factor, dilate_factor), np.uint8)
        result = cv2.dilate(result, kernel, iterations=1)

        label_idxs, counts = np.unique(seg.label[result.nonzero()[:2]], return_counts=True)
        for label_idx, count in zip(label_idxs, counts):
            stat[int(label_idx)]+= int(count)


example_seg = WiseClassesSegmentation()

save_obj(dict(stat), path="./seg_border_classes.json")
stat.pop(road.unified_idx, None)

stat = {k: v for k, v in sorted(stat.items(), key=lambda item: item[1], reverse=True)}

total_sum = 0
for k, v in stat.items():
    total_sum +=v
for k, v in stat.items():
    stat[k] = v/total_sum

plt.figure(figsize=(10, 5), dpi=300)
ax = plt.gca()
ax.yaxis.set_major_formatter(PercentFormatter(1))
# ax.set_yscale('log')
plt.bar(range(len(stat)), list(stat.values()), align='center')
plt.xticks(range(len(stat)), [example_seg.get_obj_by_idx(un_idx).name for un_idx in stat])
plt.setp(plt.gca().get_xticklabels(), rotation=30, horizontalalignment='right')
plt.title("Segmentation Border Classes")
plt.tight_layout(pad=1)
plt.savefig("./road_border_classes.png")
# plt.show()


