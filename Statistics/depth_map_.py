import os
import glob
from multiprocessing import Pool
from pathlib import Path

from sklearn.cluster import DBSCAN
from tqdm import tqdm
import pandas as pd
import numpy as np
from matplotlib import gridspec, colors
import matplotlib.pyplot as plt
from scipy.spatial import distance
from IPython.display import clear_output
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import PercentFormatter
from scipy import stats
from PIL import Image, ImageEnhance
import cv2
import torch

from DataPipeline.shared_utils import PrintException, toRAzEl

plt.style.use('dark_background')
import sys
from scipy import interpolate
from pprint import pprint

module_path = os.path.abspath('')
sys.path.insert(0, module_path)
import time

sys.path.insert(0, module_path + "/../")
from wiseTypes.classes_dims import Road
from Calibrator.calibrator_utils import process_lidar_to_image_on_ax_df
from Calibrator.projection import toXYZ
from Evaluation.evaluation_utils import make_video_from_dir
from DataPipeline.src.GetEngines import get_engines
from OpenSource.WisenseEngine import WisenseEngine
from Plotting.wise_plotter import add_rcs
import matplotlib
import cProfile
from sklearn import linear_model, datasets
from scipy.ndimage.filters import maximum_filter, minimum_filter
from scipy.ndimage.morphology import generate_binary_structure, binary_erosion
from scipy.ndimage import binary_dilation
from scipy import ndimage as ndi
from skimage.feature import peak_local_max
from skimage import data, img_as_float

font = {'size': 14}
matplotlib.rc('font', **font)
from scipy.interpolate import griddata

matplotlib.rcParams['figure.figsize'] = (15, 7)


def get_cmap(num_colors, name='hsv'):
    '''Returns a function that maps each index in 0, 1, ..., n-1 to a distinct
    RGB color; the keyword argument name must be a standard mpl colormap name.'''
    return plt.cm.get_cmap(name, num_colors)


cmap_colors = get_cmap(20)


def detect_peaks(image, peak_threshold, ker_size):
    """
    Takes an image and detect the peaks usingthe local maximum filter.
    Returns a boolean mask of the peaks (i.e. 1 when
    the pixel's value is the neighborhood maximum, 0 otherwise)
    """

    # Filter pixels below threshold
    image[image < peak_threshold] = np.nan

    # define an 8-connected neighborhood
    # neighborhood = np.ones((ker_size, ker_size)).astype(bool)
    neighborhood = generate_binary_structure(2, 2)

    # data_max = maximum_filter(image, footprint=neighborhood)
    # maxima = (image == data_max)
    # data_min = minimum_filter(image, footprint=neighborhood)
    # diff = ((data_max - data_min) > 0.3)
    # maxima[diff == 0] = 0

    # apply the local maximum filter; all pixel of maximal value
    # in their neighborhood are set to 1
    local_max = maximum_filter(image, footprint=neighborhood) == image
    # local_max is a mask that contains the peaks we are
    # looking for, but also the background.
    # In order to isolate the peaks we must remove the background from the mask.

    # we create the mask of the background
    background = (image == np.nan)

    # a little technicality: we must erode the background in order to
    # successfully subtract it form local_max, otherwise a line will
    # appear along the background border (artifact of the local maximum filter)
    eroded_background = binary_erosion(background, structure=neighborhood, border_value=1)

    # we obtain the final mask, containing only peaks,
    # by removing the background from the local_max mask (xor operation)
    detected_peaks = local_max ^ eroded_background

    return detected_peaks  # * maxima


# Params
max_range = 80
min_height, max_height = -3, 2
x_min, x_max, x_bin = -40, 40, 0.1
y_min, y_max, y_bin = 0, max_range, 0.1
dist_from_plane = 0.2
x_slice_m, x_overlap_ratio = 5, 0.2
y_slice_m, y_overlap_ratio = 2.5, 0.2
grad_change = 0.05
dbscan_epsilon = 0.4
dbscan_min_points = 4
delta_z = 0.1
delta_z_outlier = 0.3
dzdr_thresh = 0.2
dzdr_radius = 1
h_cNorm = colors.Normalize(vmin=-2.5, vmax=0)
debug = False


def process_df(df):
    df = df[df["x"].notna()].reset_index(drop=True)
    df = df[df["r"] <= max_range]
    df = df[(df["z"] <= max_height) & (df["z"] >= min_height)]
    return df[df["x"].notna()].reset_index(drop=True)


def get_xi_yi():
    xi_grid = np.linspace(x_min, x_max, int(np.sum(np.absolute([x_min, x_max])) / x_bin))
    yi_grid = np.linspace(y_min, y_max, int(np.sum(np.absolute([y_min, y_max])) / y_bin))
    xi, yi = np.meshgrid(xi_grid, yi_grid)
    return xi, yi


def apply_RANSAC(df):
    x_train = df[["x", "y"]]
    y_train = df["z"]
    ransac = linear_model.RANSACRegressor(linear_model.LinearRegression())  # , min_samples=40, max_trials=100, residual_threshold=0.02)
    ransac.fit(x_train, y_train)
    a, b = ransac.estimator_.coef_  # coefficients
    d = ransac.estimator_.intercept_  # intercept
    c = 1
    z = lambda x, y: a * x + b * y + d

    x1, y1, z1 = x_train.iloc[:, 0], x_train.iloc[:, 1], y_train
    dist = np.abs((a * x1 + b * y1 + z1 - d))
    e = np.sqrt((a * a + b * b + c * c))
    distances = dist / e
    return z, distances, ransac


def add_patches_info(df):
    df["in_z_range"] = False
    df["outlier"] = False
    xs = np.arange(x_min, x_max + x_slice_m, x_slice_m)
    ys = np.arange(y_min, y_max + y_slice_m, y_slice_m)
    for x_idx in range(len(xs[:-1])):
        for y_idx in range(len(ys[:-1])):
            indices = (((df["x"] >= xs[x_idx]) & (df["x"] <= (xs[x_idx] + x_slice_m) * (1 + x_overlap_ratio))) &
                       ((df["y"] >= ys[y_idx]) & (df["y"] <= (ys[y_idx] + y_slice_m) * (1 + y_overlap_ratio))))
            z_values = df.loc[indices, "z"]
            df.loc[indices, "in_z_range"] = df.loc[indices, "in_z_range"] | \
                                            ((z_values >= (z_values.median() - delta_z)) & (z_values <= (z_values.median() + delta_z)))
            df.loc[indices, "outlier"] = ((z_values <= (z_values.median() - delta_z_outlier)) | (z_values >= (z_values.median() + delta_z_outlier)))
    return df


def plot_bev(ax, xi, yi, zi, df, title):
    x, y, z = (xi.flatten(), yi.flatten(), zi.flatten())
    scat = ax.scatter(x, y, c=z, cmap="turbo", s=1, norm=h_cNorm)
    plt.colorbar(scat, ax=ax, orientation='horizontal')
    scat = ax.scatter(df["x"], df["y"], s=1, c="r")
    ax.set_title(title, pad=20)
    ax.set_xlim(-40, 40)
    ax.set_ylim(0, max_range)
    return x, y, z


def plot_img(ax, title, im, engine, x, y, z, df):
    u, v = engine.drive_context.camera_obj.Cart2Image(x=x, y=y, z=z)
    r = np.linalg.norm([x, y, z], axis=0)
    scat = ax.scatter(u, v, c=r, cmap="turbo")
    process_lidar_to_image_on_ax_df(display_df=df, im=im, ax=ax, points_size=0.3, cbar=None)
    ax.imshow(im, alpha=0.6)
    plt.colorbar(scat, ax=ax, orientation='horizontal', fraction=0.026, pad=0.08)
    plt.axis(False)
    ax.set_title(title, pad=30)
    return u, v


def plot_df(df, ax, title, cmap="z", hnorm=True, c=None):
    cNorm = None
    if cmap != "color":
        cNorm = colors.Normalize(vmin=df[cmap].min(), vmax=df[cmap].max())
    if c is None:
        c = df[cmap]
    scat = ax.scatter(df["x"], df["y"], c=c, cmap="jet", s=2, norm=h_cNorm if hnorm else cNorm)
    plt.colorbar(scat, ax=ax, orientation='horizontal')
    ax.set_xlim(-40, 40)
    ax.set_ylim(0, max_range)
    ax.set_title(title)


def apply_DBSCAN(df):
    coord_keys = ["x", "y", "z"]
    db = DBSCAN(eps=dbscan_epsilon, min_samples=dbscan_min_points)
    X = df[coord_keys]
    db_res = db.fit(X)
    labels = db_res.labels_
    core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
    core_samples_mask[db.core_sample_indices_] = True
    unique_labels = set(labels)

    cur_points = df[core_samples_mask]
    return cur_points


def generate_grid(df, engine, outliers_mask=None):
    method = "linear"
    xi, yi = get_xi_yi()
    zi = griddata((df["x"], df["y"]), df["z"], (xi, yi), method=method)
    zi_score = griddata((df["x"], df["y"]), df["confidence"], (xi, yi), method=method)
    if outliers_mask is not None:
        assert zi.shape == outliers_mask.shape
        zi = np.multiply(zi, outliers_mask)
        zi[zi == 0] = np.nan
    # gradients = np.gradient(zi)
    x, y, z = (xi.flatten(), yi.flatten(), zi.flatten())
    r, az, el = toRAzEl(x=x, y=y, z=z)

    confidence = zi_score.flatten()
    if hasattr(engine.drive_context.camera_obj, "Cart2Image"):
        u, v = engine.drive_context.camera_obj.Cart2Image(x=x, y=y, z=z)
    else:
        u, v = engine.drive_context.camera_obj.fromRadarXYZ(x=x, y=y, z=z)

    # peak_mask = detect_peaks(zi, peak_threshold=-5, ker_size=8)
    grid_df = pd.DataFrame(data={"x": x, "y": y, "z": z, "x_pixel": u, "y_pixel": v, "confidence": confidence,
                                 # "gradients": np.sum(gradients, axis=0).flatten(),
                                 "grid_x": np.indices(zi.shape)[0].flatten(), "grid_y": np.indices(zi.shape)[1].flatten(),
                                 # "peak": peak_mask.flatten(),
                                 })
    grid_df["r"] = r
    grid_df["Az"] = az
    grid_df["u_sin"] = np.sin(np.deg2rad(grid_df["Az"]))

    grid_df["El"] = el
    grid_df["v_sin"] = np.sin(np.deg2rad(grid_df["El"]))
    return grid_df



def infer_road_surface_idx(engine, engine_idx, plot_every):
    try:
        label_path = engine.get_road_surface(idx=engine_idx, path_only=True)
        os.makedirs(os.path.split(label_path)[0], exist_ok=True)
        # if not force and os.path.exists(label_path): continue
        # Data
        im = engine.get_image(engine_idx)
        seg = engine.get_segmentation_label(engine_idx)
        radar_df = process_df(engine.get_lidar_df(engine_idx))
        road_seg_df = process_df(engine.get_lidar_df(idx=engine_idx, filter_by_seg_class=Road, keep_semantic=True))
        road_seg_df["peak"] = False
        road_seg_df["dzdr"] = 0
        road_seg_df["color"] = "white"
        x_idx = (road_seg_df["x"] - x_min) / x_bin
        y_idx = (road_seg_df["y"] - y_min) / y_bin
        road_seg_df["x_grid"] = x_idx.astype(int)
        road_seg_df["y_grid"] = y_idx.astype(int)

        plot_df_init = road_seg_df.copy()

        # Apply DBSCAN and calculate grid
        road_seg_df = apply_DBSCAN(road_seg_df).reset_index(drop=True)
        plot_df_after_dbscan = road_seg_df.copy()

        # Filter z outliers along patches and outliers grid
        road_seg_df = add_patches_info(road_seg_df)
        outliers_df = road_seg_df[road_seg_df["outlier"]].reset_index(drop=True).copy()
        outliers_df.loc[:, "color"] = "red"
        road_seg_df = road_seg_df[road_seg_df["in_z_range"]].reset_index(drop=True)
        plot_df_after_z_patch_filter = road_seg_df.copy()

        # Peak detection on inliers (To get more outliers)
        df_on_grid = np.ones(get_xi_yi()[0].shape, dtype=np.float32) * (-100)
        df_on_grid[road_seg_df["y_grid"], road_seg_df["x_grid"]] = road_seg_df["z"]
        index_grid = np.zeros(get_xi_yi()[0].shape, dtype=np.float32)
        index_grid[road_seg_df["y_grid"], road_seg_df["x_grid"]] = road_seg_df.index
        peaks = detect_peaks(df_on_grid, peak_threshold=-5, ker_size=10)
        u, v = peaks.nonzero()
        peaks_indices = index_grid[u, v].astype(int)
        road_seg_df.loc[peaks_indices, "peak"] = True

        # Compute Distances
        D = distance.squareform(distance.pdist(road_seg_df[["x", "y", "z"]]))
        # closest = np.argsort(D, axis=1)
        # Keep only in radius
        D = D * (D <= dzdr_radius)

        # Calculate dz/dr
        road_seg_df = road_seg_df.reset_index(drop=True)
        for peak_index in peaks_indices:
            rel_ranges = D[peak_index, :]
            indices = rel_ranges.nonzero()[0]
            rel_ranges = rel_ranges[indices]
            if len(rel_ranges) == 0:
                continue
            rel_z = road_seg_df.loc[indices, "z"]
            dr = rel_ranges[0] - rel_ranges
            dz = rel_z.values[0] - rel_z.values
            road_seg_df.loc[peak_index, "dzdr"] = np.clip(np.median(dz[1:] / dr[1:]), -1, 1)

        road_seg_df = road_seg_df.replace([np.inf, -np.inf], np.nan)
        dzdr_arr = road_seg_df[road_seg_df["dzdr"].notna()]["dzdr"].values.copy()

        dzdr_outliers = road_seg_df[np.abs(road_seg_df["dzdr"]) > dzdr_thresh].copy()
        dzdr_outliers.loc[:, "color"] = "blue"
        outliers_df = outliers_df.append(dzdr_outliers).reset_index(drop=True)

        outliers_mask = np.zeros(get_xi_yi()[0].shape, dtype=np.float32)
        outliers_mask[outliers_df["y_grid"], outliers_df["x_grid"]] = 1
        outliers_mask = binary_dilation(outliers_mask, structure=np.ones((10, 10)))
        outliers_mask = np.logical_not(outliers_mask)

        road_seg_df = road_seg_df[np.abs(road_seg_df["dzdr"]) <= dzdr_thresh]
        df_a_dzdr_clip = road_seg_df.copy()

        if len(road_seg_df) <= 2:
            print("grid too small", len(road_seg_df))
            return

        grid_df = generate_grid(road_seg_df, engine=engine, outliers_mask=outliers_mask)

        grid_df = grid_df[seg.get_mask_of_points(grid_df[["x_pixel", "y_pixel"]].T.to_numpy(), cls=Road)].reset_index(drop=True)

        grid_df.to_csv(label_path)

        # if mark_done is not None:
        #     mark_done.set_idx_status(engine_idx, 1)

        if engine_idx%100 == 0:
            print("Road surface progress", engine_idx)
        if engine_idx % plot_every != 0: return

        plt.close()
        fig = plt.figure(figsize=(25, 15))
        gs = gridspec.GridSpec(ncols=8, nrows=2, height_ratios=[1, 2])

        ax = fig.add_subplot(gs[0, :3])
        plt.axis(False)
        process_lidar_to_image_on_ax_df(colormap="range", display_df=road_seg_df, im=engine.get_image(engine_idx), ax=ax, points_size=2,
                                        orientation="horizontal")
        ax.imshow(im)
        ax.imshow(seg.get_img(colors=True), alpha=0.4)
        ax.set_title("Reference cmap=r [m]")

        ax = fig.add_subplot(gs[0, 3:6])
        plt.axis(False)
        process_lidar_to_image_on_ax_df(colormap="elevation", display_df=road_seg_df, im=engine.get_image(engine_idx), ax=ax, points_size=2,
                                        orientation="horizontal")
        ax.imshow(im)
        ax.imshow(seg.get_img(colors=True), alpha=0.4)
        ax.set_title("Reference cmap=z [m]")

        # ax = fig.add_subplot(gs[0, 5])
        # ax.hist(dzdr_arr)
        # ax.set_title("dzdr histogram\nmean=" + str(round(dzdr_arr.mean(), 2)) + ", median=" + str(round(np.median(dzdr_arr), 2)) + ", std=" + str(round(np.std(dzdr_arr), 2)))

        # Lidar BEV
        ax = fig.add_subplot(gs[1, 0])
        plot_df(df=radar_df, ax=ax, title="BEV | Lidar | cmap=z [m]")

        ax = fig.add_subplot(gs[1, 1])
        cmap = "z"
        plot_df(df=plot_df_init, ax=ax, title="BEV | Initial Lidar Road\ncmap=" + cmap, cmap=cmap)

        ax = fig.add_subplot(gs[1, 2])
        cmap = "z"
        plot_df(df=plot_df_after_dbscan, ax=ax, title="BEV | after DBSCAN\ncmap=" + str(cmap), cmap=cmap)

        ax = fig.add_subplot(gs[1, 3])
        cmap = "dzdr"
        plot_df(df=df_a_dzdr_clip, ax=ax, title="BEV | after dzdr filter\ncmap=z [m]", cmap=cmap, hnorm=False)

        ax = fig.add_subplot(gs[1, 4])
        plot_df(df=plot_df_after_z_patch_filter, ax=ax, title="BEV | z-patches-filtering=" + str(delta_z) + "\n | cmap=z [m]", hnorm=False)

        ax = fig.add_subplot(gs[1, 5])
        plot_df(df=grid_df, ax=ax, title="BEV | Final Grid\ncmap=z [m]")

        ax = fig.add_subplot(gs[1, 6])
        cmap = "color"
        plot_df(df=outliers_df, ax=ax, title="BEV | Outliers Grid\n | cmap=z [m]", cmap=cmap, hnorm=False)

        plt.suptitle("Lidar Depth Map | " + str(engine.get_id(engine_idx)) + "\n" + engine.get_dt_title(engine_idx))
        plt.tight_layout(pad=1)
        os.makedirs(engine.drive_context.lidar_road_surface_images, exist_ok=True)
        plt.savefig(engine.drive_context.lidar_road_surface_images + "/" + str(engine.get_id(engine_idx)) + ".png")
        # plt.savefig("//home/amper/AI/Harel/AI_Infrastructure/Statistics/tree_trunks/" + str(engine.get_id(engine_idx)) + ".png")
        plt.close()
    except Exception as e:
        PrintException()


def infer_road_surface(engine, plot_every=150):
    for engine_idx in tqdm(range(len(engine))):
        infer_road_surface_idx(engine=engine, engine_idx=engine_idx, plot_every=plot_every)



def gen_video(engine):
    for engine_idx in tqdm(range(len(engine))):
        try:
            road_surface = engine.get_road_surface(idx=engine_idx)
            im = engine.get_image(engine_idx)
            seg = engine.get_segmentation_label(engine_idx)
            all_lidar = process_df(engine.get_lidar_df(engine_idx))
            road_seg_df = process_df(engine.get_lidar_df(idx=engine_idx, filter_by_seg_class=Road, keep_semantic=True))

            plt.close()
            fig = plt.figure(figsize=(25, 14))
            gs = gridspec.GridSpec(ncols=4, nrows=1) #, height_ratios=[1, 2])

            ax = fig.add_subplot(gs[0, 1:3])
            plt.axis(False)

            process_lidar_to_image_on_ax_df(colormap="range", display_df=road_seg_df, im=engine.get_image(engine_idx), ax=ax,
                                            points_size=2, location="left", orientation="vertical", pad=0.04)
            ax.imshow(im)
            ax.imshow(seg.get_img(colors=True), alpha=0.4)
            ax.set_title("Reference cmap=r [m]")
            ax.set_anchor("C")

            ax = fig.add_subplot(gs[:, 0])
            plot_df(df=all_lidar, ax=ax, title="BEV | Lidar | cmap=z [m]")

            ax = fig.add_subplot(gs[:, 3])
            plot_df(df=road_surface, ax=ax, title="BEV | Final Grid\ncmap=z [m]")
            plt.suptitle("Lidar Road Surface | " + str(engine.get_id(engine_idx)) + "\n" + engine.get_dt_title(engine_idx))

            plt.savefig("//home/amper/AI/Harel/AI_Infrastructure/Statistics/tree_trunks/" + str(engine.get_id(engine_idx)) + ".png")
            plt.close()
        except Exception as e:
            PrintException()
            continue
    make_video_from_dir(df=engine.df, images_dir="//home/amper/AI/Harel/AI_Infrastructure/Statistics/tree_trunks/", suffix="_" + str(time.time()).split(".")[0])
    return

def generate_road_surface_video():
    engines = get_engines(val_train="all", randomize=True)
    for engine in engines:
        engine.sample_df_range(300)

    with Pool(3) as p2:
        p2.map(gen_video, engines)
    return


def infer_road_surface_old(engine, force=True, plot_every=100, write_csv=True):
    for engine_idx in tqdm(range(len(engine))):
        try:
            label_path = engine.get_road_surface(idx=engine_idx, path_only=True)
            os.makedirs(os.path.split(label_path)[0], exist_ok=True)
            if not force and os.path.exists(label_path): continue

            # Data
            im = engine.get_image(engine_idx)
            seg = engine.get_segmentation_label(engine_idx)
            radar_df = process_df(engine.get_lidar_df(engine_idx))
            road_seg_df = process_df(engine.get_lidar_df(idx=engine_idx, filter_by_seg_class=Road, keep_semantic=True))

            # Algorithm
            road_seg_df = add_patches_info(road_seg_df)
            road_seg_df_filtered = road_seg_df[road_seg_df["dist_from_plane"] < dist_from_plane]

            method = "linear"
            xi, yi = get_xi_yi()
            zi = griddata((road_seg_df_filtered["x"], road_seg_df_filtered["y"]), road_seg_df_filtered["z"], (xi, yi), method=method)
            gradients = np.gradient(zi)
            x, y, z = (xi.flatten(), yi.flatten(), zi.flatten())
            u, v = engine.drive_context.camera_obj.Cart2Image(x=x, y=y, z=z)

            peak_mask = detect_peaks(zi, peak_threshold=-5, ker_size=8)

            grid_df = pd.DataFrame(data={"x": x, "y": y, "z": z, "u": u, "v": v, "gradients": np.sum(gradients, axis=0).flatten(),
                                         "grid_x": np.indices(zi.shape)[0].flatten(), "grid_y": np.indices(zi.shape)[1].flatten(),
                                         "peak": peak_mask.flatten(),
                                         "r": np.linalg.norm([x, y, z], axis=0)})

            grid_df_filtered = grid_df[seg.get_mask_of_points(grid_df[["u", "v"]].T.to_numpy(), cls=Road)].reset_index(drop=True)
            grid_df_final = grid_df_filtered[(grid_df_filtered["gradients"] >= -grad_change) & (grid_df_filtered["gradients"] <= grad_change)]
            grid_df_final = grid_df_final.reset_index(drop=True)

            if write_csv:
                grid_df_final.to_csv(label_path)

            if engine_idx % plot_every != 0: continue

            # Reference Views
            plt.close()
            fig = plt.figure(figsize=(25, 15))
            gs = gridspec.GridSpec(ncols=6, nrows=2)
            if debug: gs = gridspec.GridSpec(ncols=2, nrows=3)

            ax = fig.add_subplot(gs[0, 0])
            plt.axis(False)
            c = "dist_from_plane" if debug else "range"
            process_lidar_to_image_on_ax_df(colormap=c, display_df=road_seg_df, im=engine.get_image(engine_idx), ax=ax, points_size=2, orientation="horizontal")
            ax.imshow(im)
            ax.imshow(seg.get_img(colors=True), alpha=0.4)
            ax.set_title("Reference")

            # Lidar BEV
            ax = fig.add_subplot(gs[1, 0])
            cNorm = colors.Normalize(vmin=radar_df["z"].min(), vmax=radar_df["z"].max())
            c = radar_df["z"]
            if debug:
                c = road_seg_df["z"]
                radar_df = road_seg_df
                cNorm = colors.Normalize(vmin=radar_df["z"].min(), vmax=radar_df["z"].max())
            scat = ax.scatter(radar_df["x"], radar_df["y"], c=c, cmap="turbo", s=10, norm=cNorm, marker="x")
            plt.colorbar(scat, ax=ax, orientation='horizontal')
            ax.set_xlim(-40, 40)
            ax.set_ylim(0, max_range)
            ax.set_title("BEV | Lidar | cmap=z [m]")

            if debug:
                ax = fig.add_subplot(gs[:, 1], projection='3d')
                plt.axis("auto")
                scat = ax.scatter(road_seg_df["x"], road_seg_df["y"], road_seg_df["z"], c=distances, cmap="jet")
                # ax.plot_surface(xi, yi, z_func(xi, yi))
                ax.set_title("Predicted Plane", pad=15)
                ax.set_xlim(-40, 40)
                ax.set_ylim(0, max_range)
                ax.set_xlabel('X [m]\n', labelpad=5)
                ax.set_ylabel('Y [m]', labelpad=5)

                ax = fig.add_subplot(gs[2, 0])

                cNorm = colors.Normalize(vmin=road_seg_df["orig_z"].min(), vmax=road_seg_df["orig_z"].max())
                c = road_seg_df["orig_z"]
                scat = ax.scatter(road_seg_df["orig_x"], road_seg_df["orig_y"], c=c, cmap="turbo", s=10, norm=cNorm, marker="x")
                plt.colorbar(scat, ax=ax, orientation='horizontal')
                # ax.set_xlim(-40, 40)
                # ax.set_ylim(0, max_range)
                ax.set_title("BEV | Original Lidar | cmap=z [m]")
                plt.tight_layout()
                plt.show()
                plt.close()
                continue

            ###############################
            # X,Y Linear NO-RANSAC road
            ax = fig.add_subplot(gs[1, 1])
            method = "linear"
            xi, yi = get_xi_yi()
            zi = griddata((road_seg_df["x"], road_seg_df["y"]), road_seg_df["z"], (xi, yi), method=method)
            x, y, z = plot_bev(ax=ax, xi=xi, yi=yi, zi=zi, df=road_seg_df, title="BEV | Road | No-RANSAC | x,y\ninterp=linear | cmap=z [m]")

            # Img
            ax = fig.add_subplot(gs[0, 1])
            plot_img(ax=ax, engine=engine, x=x, y=y, z=z, df=road_seg_df, im=engine.get_image(engine_idx),
                     title="Depth | Road | No-RANSAC | x,y\n interp=linear | cmap=r [m]")

            ###############################
            # X,Y Linear RANSAC road
            method = "linear"
            # road_seg_df_filtered = road_seg_df[road_seg_df["dist_from_plane"] < dist_from_plane]
            xi, yi = get_xi_yi()
            zi = griddata((road_seg_df_filtered["x"], road_seg_df_filtered["y"]), road_seg_df_filtered["z"], (xi, yi), method=method)

            # BEV
            ax = fig.add_subplot(gs[1, 2])
            x, y, z = plot_bev(ax=ax, xi=xi, yi=yi, zi=zi, df=road_seg_df_filtered, title="BEV | Road | RANSAC | x,y\ninterp=linear | cmap=z [m]")

            # Img
            ax = fig.add_subplot(gs[0, 2])
            u, v = plot_img(ax=ax, engine=engine, x=x, y=y, z=z, df=road_seg_df_filtered, im=engine.get_image(engine_idx),
                            title="Depth | Road | RANSAC | x,y\n interp=linear | cmap=r [m]")

            ###############################
            # Filter points from image
            # Img
            ax = fig.add_subplot(gs[0, 3])
            scat = ax.scatter(grid_df_filtered["u"], grid_df_filtered["v"], c=grid_df_filtered["r"], cmap="turbo")
            process_lidar_to_image_on_ax_df(display_df=road_seg_df_filtered, im=engine.get_image(engine_idx), ax=ax, points_size=0.3, cbar=None)
            ax.imshow(im, alpha=0.6)
            plt.colorbar(scat, ax=ax, orientation='horizontal', fraction=0.026, pad=0.08)
            plt.axis(False)
            ax.set_title("Depth | intersected\ncmap=r", pad=30)

            # BEV
            ax = fig.add_subplot(gs[1, 3])
            scat = ax.scatter(grid_df_filtered["x"], grid_df_filtered["y"], c=grid_df_filtered["z"], cmap="turbo", norm=h_cNorm)
            ax.scatter(road_seg_df_filtered["x"], road_seg_df_filtered["y"], s=1, c="r")
            # print(grid_df[grid_df["peak"] == 1]["peak"].sum())
            ax.scatter(grid_df_filtered[grid_df_filtered["peak"] == 1]["x"], grid_df_filtered[grid_df_filtered["peak"] == 1]["y"], c="yellow", s=100, marker="x")
            plt.colorbar(scat, ax=ax, orientation='horizontal')
            ax.set_title("BEV | intersected | cmap=z", pad=15)
            ax.set_xlim(-40, 40)
            ax.set_ylim(0, max_range)

            ######################################3
            # Gradients
            ax = fig.add_subplot(gs[1, 4])
            # suspicious = grid_df_filtered[(grid_df_filtered["gradients"] <= -suspicious_thresh) & (grid_df_filtered["gradients"] >= suspicious_thresh)]
            # grid_df = grid_df[(grid_df["gradients"] >= -grad_change) & (grid_df["gradients"] <= grad_change)]
            # cNorm = colors.Normalize(vmin=grid_df["gradients"].min(), vmax=grid_df["gradients"].max())
            scat = ax.scatter(grid_df_final["x"], grid_df_final["y"], c=grid_df_final["gradients"], cmap="turbo", norm=cNorm)
            ax.scatter(suspicious["x"], suspicious["y"], c="blue")
            ## ax.scatter(road_seg_df_filtered["x"], road_seg_df_filtered["y"], s=0.1, c="r")
            plt.colorbar(scat, ax=ax, orientation='horizontal')
            ax.set_title("BEV | Gradients\n grad_change<=" + str(grad_change) + "m", pad=15)
            ax.set_xlim(-40, 40)
            ax.set_ylim(0, max_range)

            ######################################3
            # Final
            ax = fig.add_subplot(gs[0, 4:6])
            scat = ax.scatter(grid_df_final["u"], grid_df_final["v"], c=grid_df_final["r"], cmap="turbo")
            process_lidar_to_image_on_ax_df(display_df=road_seg_df_filtered, im=engine.get_image(engine_idx), ax=ax, points_size=0.3, cbar=None)
            ax.imshow(im, alpha=0.6)
            plt.colorbar(scat, ax=ax, orientation='horizontal', fraction=0.026, pad=0.08)
            plt.axis(False)
            ax.set_title("Depth | Final Road Surface\ncmap=r", pad=30)

            # ax = fig.add_subplot(gs[0, 5])
            # scat = ax.scatter(grid_df["u"], grid_df["v"], c=grid_df["z"], cmap="turbo")
            # process_lidar_to_image_on_ax_df(display_df=road_seg_df_filtered, im=engine.get_image(engine_idx), ax=ax, points_size=0.3, cbar=None)
            # ax.imshow(im, alpha=0.6)
            # plt.colorbar(scat, ax=ax, orientation='horizontal', fraction=0.026, pad=0.08)
            # plt.axis(False)
            # ax.set_title("Depth | Final Road Surface\ncmap=z", pad=30)

            ######################################3
            ax = fig.add_subplot(gs[1, 5])
            scat = ax.scatter(grid_df_final["x"], grid_df_final["y"], c=grid_df_final["z"], cmap="turbo", norm=h_cNorm)
            ax.scatter(road_seg_df_filtered["x"], road_seg_df_filtered["y"], s=1, c="r")
            plt.colorbar(scat, ax=ax, orientation='horizontal')
            ax.set_title("BEV | Final Road Surface\ncmap=z [m]", pad=15)
            ax.set_xlim(-40, 40)
            ax.set_ylim(0, max_range)

            # Plot surface
            # ax = fig.add_subplot(gs[:, -1], projection='3d')
            # ax.scatter(grid_df["x"], grid_df["y"], grid_df["z"], c=grid_df["z"], cmap="turbo", norm=h_cNorm)
            # ax.set_title("Predicted Surface", pad=15)
            # ax.set_xlim(-40, 40)
            # ax.set_ylim(0, max_range)
            # ax.view_init(0, 90)
            # ax.set_xlabel('X [m]\n', labelpad=5)
            # ax.set_ylabel('Y [m]', labelpad=5)

            #################
            # Peaks
            # image = zi
            # peak_mask = detect_peaks(image, peak_threshold=0)
            # print(peak_mask.sum(), image.shape, np.nanmin(zi), np.nanmax(zi))
            # ax = fig.add_subplot(gs[0, 4])
            # print("lap", lap.shape, lap.dtype, np.nanmin(lap), np.nanmax(lap))
            # ax.imshow(lap)
            # ax.set_title("Peaks")

            plt.suptitle("Lidar Depth Map | " + str(engine.get_id(engine_idx)) + "\n" + engine.get_dt_title(engine_idx))
            plt.tight_layout(pad=1)
            # plt.show()
            os.makedirs(engine.drive_context.lidar_road_surface_images, exist_ok=True)
            plt.savefig(engine.drive_context.lidar_road_surface_images + "/" + str(engine.get_id(engine_idx)) + ".png")
            plt.savefig("//home/amper/AI/Harel/AI_Infrastructure/Statistics/tree_trunks/" + str(engine.get_id(engine_idx)) + ".png")
            plt.close()

        except Exception as e:
            PrintException()
            continue
    # make_video_from_dir(df=engine.df, images_dir="//home/amper/AI/Harel/AI_Infrastructure/Statistics/tree_trunks/", suffix="_" + str(time.time()).split(".")[0])


def generate():
    with Pool(7) as p2:
        engines = get_engines(val_train="all", randomize=False)
        print(p2.map(infer_road_surface, engines))


def profile():
    e = get_engines(val_train="all", randomize=False)[0]
    e.sample_df(1)
    infer_road_surface(e, plot_every=1)


if __name__ == '__main__':
    generate()
    # profile()
    # generate_road_surface_video()
    if False:
        _engines = get_engines(val_train="val", randomize=True)
        for _engine in _engines:
            # ts = "1598876335492864076"
            # # ts = "1599115429570310867"
            # idx = engine.get_idx_from_ts(ts=ts , key="timestampCamera")
            # if idx is None:
            #     continue
            # print("Found")
            # engine.update_df_range(idx, idx + 1, shuffle=False)
            _engine.sample_df(30)
            infer_road_surface(_engine, plot_every=1)
            # infer_road_surface(engine=engine, force=True, plot_every=1, write_csv=False)

    # for engine in engines:

    # idx = engine.get_idx_from_ts(ts=ts)
    # if idx is None:
    #     continue
    # engine.update_df_range(idx, idx+1, shuffle=False)
    # engine.sample_df(10)
