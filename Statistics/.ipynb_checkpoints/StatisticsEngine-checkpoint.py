import numpy as np
from PIL import Image
from tqdm import tqdm
import glob
import pandas as pd
import os
import json
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
# %matplotlib inline
from collections import defaultdict
from IPython.core.display import display, HTML
display(HTML("<style>.container { width:95% !important; }</style>"))

class StatisticsEngine:
    def __init__(self):
        self.df = None
        self.fov_h = 90
        self.fov_v = 60
        
        self.ped_det = {"az": [], "el": [], "size": [], "frames_count": 0, "instacnes_count": 0, "title": "Ped-Det"}
        self.veh_det = {"az": [], "el": [], "size": [], "frames_count": 0, "instacnes_count": 0, "title": "Veh-Det"}
        
    def analyze_label(self, label_path, dic):
        with open(label_path, 'r') as fp:
            frame_data = json.load(fp)
            if len(frame_data) > 0:
                dic["frames_count"] += 1
                
            for i in range(len(frame_data)):  
                # BB
                x1 = frame_data[i]['bbox'][0]
                y1 = frame_data[i]['bbox'][1]
                x2 = frame_data[i]['bbox'][2]
                y2 = frame_data[i]['bbox'][3]
                
                cx = int(x1+(x2-x1)/2)
                cy = int(y1+(y2-y1)/2)
                
                height = frame_data[0]['img_res'][0]
                width = frame_data[0]['img_res'][1]
                
                dic["az"].append((cx / width) * self.fov_h  - self.fov_h//2)
                dic["el"].append((cy / height) * self.fov_v - self.fov_v//2) 
                dic["size"].append(np.sqrt(np.abs(x2 - x1)*np.abs(y2 - y1)))
                dic["instacnes_count"] += 1
            del frame_data
    
    def plot_hist(self, ax, dic, key):
        N, bins, patches = ax.hist(dic[key])  # arguments are passed to np.histogram
        ax.set_title(dic["title"] + " " + str(key))
        return
    
    def plot(self):
        fig = plt.figure(figsize=(20,30))
        
        title = "Statistics of " + str(len(self.df)) + " Frames\n"
        title += "Ped-Det: #Frames " + str(self.ped_det["frames_count"]) + ", #Instances " + str(self.ped_det["instacnes_count"]) + "\n"
        title += "Veh-Det: #Frames " + str(self.veh_det["frames_count"]) + ", #Instances " + str(self.veh_det["instacnes_count"])
        
        fig.suptitle(title)
        
        ax = fig.add_subplot(321)
        self.plot_hist(ax, self.ped_det, "az")
        ax = fig.add_subplot(323)
        self.plot_hist(ax, self.ped_det, "el")
        ax = fig.add_subplot(325)
        self.plot_hist(ax, self.ped_det, "size")
        
        ax = fig.add_subplot(322)
        self.plot_hist(ax, self.veh_det, "az")
        ax = fig.add_subplot(324)
        self.plot_hist(ax, self.veh_det, "el")
        ax = fig.add_subplot(326)
        self.plot_hist(ax, self.veh_det, "size")
        
        plt.savefig("Lats_Run.png")
        plt.show()
        return
    
    def run(self, df):
        self.df = df
        print(len(df))
        
        for idx in tqdm(range(len(self.df))):
            sampleDataset   = str(self.df.loc[idx,'Dataset'])
            sampleDisc      = self.df.loc[idx,'Disc']
            sampleWF        = self.df.loc[idx,'WF']
            timestampCamera = str(self.df.loc[idx,'timestampCamera'])
            timestampRadar  = str(self.df.loc[idx,'timestampRadar'])

            ped_label_path = '/workspace/'+sampleDisc+'/'+sampleWF+'/'+ sampleDataset+'/camera_ped_det/camera_ped_det_'+timestampCamera+'.json'
            veh_label_path = '/workspace/'+sampleDisc+'/'+sampleWF+'/'+ sampleDataset+'/camera_veh_det/camera_veh_det_'+timestampCamera+'.json'

            self.analyze_label(ped_label_path, self.ped_det)
            self.analyze_label(veh_label_path, self.veh_det)
            
