import os
import sys


module_path = os.getcwd() + "/../"
if module_path not in sys.path:
    sys.path.insert(0, module_path)

from DataLoading.DataLoaderUtils import make_df_folders
from StatisticsEngine import StatisticsEngine

dirs = ['200319_drive_0_W60_M8_RF0', '200319_drive_1_W60_M8_RF0', '200319_drive_2_W60_M8_RF0',
       '200322_drive_0_W60_M8_RF0', '200322_drive_1_W60_M8_RF0', '200322_drive_2_W60_M8_RF0']

disc = ['nvme2', 'nvme2', 'HDD', 'HDD', 'nvme2', 'HDD']

df = make_df_folders(disc, "64mWF", dirs)

stat_eng = StatisticsEngine()
stat_eng.run(df[:5000])

stat_eng.plot()