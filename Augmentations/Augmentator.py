import time
from math import sqrt
import torch
import torch.utils.data as data
import numpy as np
import cv2
import glob
from matplotlib import pyplot as plt
import imutils
from tqdm import tqdm
import random
from collections import defaultdict
import os
from skimage.draw import random_shapes
import albumentations as a
from albumentations.augmentations.transforms import *
from albumentations.augmentations.functional import *
from albumentations import Compose
from PyPDF2 import PdfFileWriter, PdfFileReader
from skimage import transform as tf
from matplotlib.backends.backend_pdf import PdfPages


class Images:
    def __init__(self):
        #         src1 = ["/media/amper/ssd25/250mWF/*/rgbCamera/*.png"]
        src2 = ["//mnt/nvme2/250mWF/*/rgbCamera/*.png"]
        # src1 = ["/workspace/nvme1/64mWF/*/camera_rgb/*.png"]
        # src2 = []
        src1 = []
        bank = []
        for i in src1 + src2:
            bank += glob.glob(i)
        self.bank = np.random.permutation(bank)
        # print("len_bank", len(bank))
        self.idx = 0

        return

    def get_image(self):
        while True:
            self.idx += 1
            path = self.bank[self.idx]

            ts = os.path.split(path)[-1].split(".")[0].split("_")[-1]
            label_path = os.path.split(path)[0] + "/../camera_road_seg/camera_road_seg_" + ts + ".npz"
            if os.path.exists(label_path):
                im = cv2.cvtColor(cv2.imread(path), cv2.COLOR_BGR2RGB)
                label = np.load(label_path)["arr_0"]
                label = cv2.resize(label, (im.shape[1], im.shape[0]))
            else:
                print("label don't exists")
                continue

            yield im, label


class Augmentator:
    def __init__(self, eval_augmentations=None):
        self.im = None
        self.label = None
        self.debug = False
        self.cropped = False
        self.blured = False
        self.extreme_min = False
        self.extreme_max = False
        self.max_len = None
        self.dataset = None

        self.images_loader = Images()

        self.images = []
        self.max_images, self.min_images = [], []
        self.timer = defaultdict(float)

        self.deprecated = [self.random_snow, self.solarize]
        self.mixup_augs = [self.mixup, self.augment_and_mix, self.cut_mix, self.mosaic]
        self.weather_augs = [self.random_rain_1, self.random_rain_2, self.random_sun_flare]
        self.colors_augs = [self.brightness, self.hue, self.saturation, self.equalize, self.color_space,
                            self.invert_img, self.sepia, self.postarize, self.clahe, self.random_contrast, self.noise]
        self.size_augs = [self.crop, self.resize, self.flip_lr, self.flip_ud, self.rotate, self.shear]
        self.distortion_augs = [self.distortion, self.shadow, self.anti_shadow, self.disturbance, self.grid_distorion, self.blur,
                                self.motion_blur, ]


        # ALl augs
        # self.func_pool = self.mixup_augs + self.weather_augs + self.colors_augs + self.size_augs + self.distortion_augs
        self.func_pool = [self.brightness, self.disturbance, self.saturation, self.postarize, self.motion_blur, self.random_contrast, self.flip_lr, self.grid_distorion,
                          self.shadow, self.noise]
        self.evaluation_pool = []

        # self.to_check = [self.random_sun_flare, self.clahe, ]#self.blur, self.motion_blur, self.saturation, self.anti_shadow, self.noise]

        self.probabilities = {self.mixup: 0.1,
                              self.augment_and_mix: 0.01,
                              self.cut_mix: 0.02,
                              self.random_rain_1: 1,
                              self.random_rain_2: 1,
                              self.random_snow: 0,
                              self.random_sun_flare: 1,
                              self.brightness: 0.2,
                              self.hue: 0.25,
                              self.saturation: 0.2,
                              self.equalize: 0.2,
                              self.solarize: 0.2,
                              self.color_space: 0.1,
                              self.invert_img: 0.1,
                              self.sepia: 0.1,
                              self.postarize: 0.1,
                              self.clahe: 0.1,
                              self.random_contrast: 0.2,
                              self.noise: 0.3,
                              self.crop: 0.2,
                              self.resize: 0.2,
                              self.flip_lr: 0.2,
                              self.flip_ud: 0.2,
                              self.rotate: 0.2,
                              self.shear: 0.2,
                              self.distortion: 0.2,
                              self.shadow: 0.3,
                              self.disturbance: 0.3,
                              self.grid_distorion: 0.2,
                              self.blur: 0.3,
                              self.mosaic: 0.2,
                              self.motion_blur: 0.2,
                              self.anti_shadow: 0.2
                              }
        self.aug_mix_func_pool = [self.equalize, self.solarize, self.rotate, self.shear,
                                  self.random_contrast, self.color_space, self.saturation]

        if eval_augmentations is not None:
            self.set_evaluation_pool(eval_augmentations)
        # self.aug_result_path = "./augmentation_result/"
        # os.makedirs(self.aug_result_path, exist_ok=True)

        # self.output_pdf = PdfPages('canvas.pdf')
        # self.load_images()
        return

    def reset(self):
        self.cropped = False
        self.blured = False
        self.extreme_max = False
        self.extreme_min = False
        self.images = []

    def close(self):
        pass
        # self.output_pdf.close()

    def set_evaluation_pool(self, func_prob_dict):
        self.evaluation_pool =[]
        for f, p in func_prob_dict.items():
            cur_func = getattr(self, f)
            print("Augmentator sets eval mode ", cur_func, "prob", p)
            self.evaluation_pool.append(cur_func)
            self.probabilities[cur_func] = p
        return

    def run_eval_mode(self, im, label):
        for f in self.evaluation_pool:

            if np.random.random() < self.probabilities[f]:
                im, label, _ = f(im, label)
        return im, label

    def rand_bbox(self, size, lam):
        W = size[1]
        H = size[0]

        cut_rat = np.sqrt(1. - lam)
        cut_w = np.int(W * cut_rat)
        cut_h = np.int(H * cut_rat)

        # uniform
        cx = int(self.random_range(0, W))
        cy = int(self.random_range(0, H))

        bbx1 = np.clip(cx - cut_w // 2, 0, W)
        bby1 = np.clip(cy - cut_h // 2, 0, H)
        bbx2 = np.clip(cx + cut_w // 2, 0, W)
        bby2 = np.clip(cy + cut_h // 2, 0, H)

        return bbx1, bby1, bbx2, bby2, cx, cy

    @staticmethod
    def ensure_same_shape(im1, im2, label2):
        if im1.shape != im2.shape:
            return cv2.resize(im2, (im1.shape[1], im1.shape[0])), cv2.resize(label2, (im1.shape[1], im1.shape[0]))
        return im2, label2

    @staticmethod
    def augment(aug, image, label, params=None):
        if not params:
            params = {}

        c = Compose([aug(p=1, **params)], p=1)
        ans = c(image=image, mask=label)
        return ans['image'], ans['mask']

    def generate_random_blur_coordinates(self, imshape, hw):
        blur_points = []
        midx = imshape[1] // 2 - 2 * hw
        midy = imshape[0] // 2 - hw
        index = 1
        while (midx > -hw or midy > -hw):
            for i in range(hw // 10 * index):
                x = np.random.randint(midx, imshape[1] - midx - hw)
                y = np.random.randint(midy, imshape[0] - midy - hw)

                blur_points.append((x, y))
            midx -= 3 * hw * imshape[1] // sum(imshape)
            midy -= 3 * hw * imshape[0] // sum(imshape)
            index += 1
        return blur_points

    @staticmethod
    def image_resize(image, width=None, height=None, inter=cv2.INTER_AREA):
        # initialize the dimensions of the image to be resized and
        # grab the image size
        dim = None
        (h, w) = image.shape[:2]

        # if both the width and height are None, then return the
        # original image
        if width is None and height is None:
            return image

        # check to see if the width is None
        if width is None:
            # calculate the ratio of the height and construct the
            # dimensions
            r = height / float(h)
            dim = (int(w * r), height)

        # otherwise, the height is None
        else:
            # calculate the ratio of the width and construct the
            # dimensions
            r = width / float(w)
            dim = (width, int(h * r))

        # resize the image
        resized = cv2.resize(image, dim, interpolation=inter)

        # return the resized image
        return resized

    def random_range(self, min_val=0.0, max_val=1.0):
        if self.extreme_min:
            return min_val
        elif self.extreme_max:
            return max_val
        return np.random.uniform(min_val, max_val)

    def mixup(self, im, label):
        # get another im
        # im2, label2 = next(self.images_loader.get_image())
        im2, label2 = self.dataset.get_im_label(np.random.randint(self.max_len))
        im2, label2 = Augmentator.ensure_same_shape(im, im2, label2)

        alpha_mixup = self.random_range(min_val=0.3, max_val=0.8)
        img = (alpha_mixup * im + (1 - alpha_mixup) * im2).astype(np.uint8)
        label = (alpha_mixup * label + (1 - alpha_mixup) * label2)
        return img, label, {"Alpha Mixup": alpha_mixup}

    def augment_and_mix(self, image, label, width=3, depth=-1, alpha=1.):
        ws = np.float32(np.random.dirichlet([alpha] * width))
        m = np.float32(np.random.beta(alpha, alpha))

        mix_img = np.zeros(image.shape, dtype=np.float32)
        mix_label = np.zeros_like(label)

        for i in range(width):
            image_aug = image.copy()
            label_aug = label.copy()

            depth = depth if depth > 0 else np.random.randint(1, 4)
            for _ in range(depth):
                op = np.random.choice(self.aug_mix_func_pool)
                image_aug, label_aug, random_numbers = op(image_aug, label_aug)
            # Preprocessing commutes since all coefficients are convex
            mix_img += ws[i] * image_aug.astype(np.float32)
            mix_label += ws[i] * label_aug

        mixed_img = (1 - m) * image + m * mix_img
        mixed_label = (1 - m) * label + m * mix_label
        return mixed_img.astype(np.uint8), mixed_label, {}

    def cut_mix(self, im, label):
        beta = 1
        # generate mixed sample
        lam = np.random.beta(beta, beta)

        # im2, label2 = next(self.images_loader.get_image())
        im2, label2 = self.dataset.get_im_label(np.random.randint(self.max_len))
        im2, label2 = Augmentator.ensure_same_shape(im, im2, label2)

        bbx1, bby1, bbx2, bby2, cx, cy = self.rand_bbox(im.shape, lam)
        im[bby1:bby2, bbx1:bbx2, :] = im2[bby1:bby2, bbx1:bbx2, :]
        label[bby1:bby2, bbx1:bbx2] = label2[bby1:bby2, bbx1:bbx2]

        return im, label, {"cx": cx, "cy": cy}

    def distortion(self, im, label):
        # Lens distortion
        fx = 1388 * (1 + (self.random_range() * 1.0 - 0.5))  # 50% movment
        fy = 1391 * (1 + (self.random_range() * 1.0 - 0.5))
        cx = im.shape[1] // 2.0 * (1.0 + (self.random_range() * 0.3 - 0.15))  # 15% movment
        cy = im.shape[0] // 2.0 * (1.0 + (self.random_range() * 0.3 - 0.15))
        k1 = 0.5375 * (self.random_range() * 2.0 - 1.0)  # 100% movment
        k2 = -0.2245 * (self.random_range() * 2.0 - 1.0)
        p1 = 0.0041 * (self.random_range() * 2.0 - 1.0)
        p2 = -0.0011 * (self.random_range() * 2.0 - 1.0)

        mtx = np.array([[fx, 0, cx], [0, fy, cy], [0, 0, 1]])
        dist = np.array([k1, k2, p1, p2])
        im = cv2.undistort(im, mtx, dist)
        label = cv2.undistort(label, mtx, dist)
        rand_param = str("fx %.2f" % fx) + " " + str("fy %.2f" % fy) + "\n" + str("cx %.2f" % cx) + " " + str(
            "cy %.2f" % cy) \
                     + " " + str("k1 %.2f" % k1) + "\n" + str("k2 %.2f" % k2) + " " + str("p1 %.4f" % p1) + " " + str(
            "p2 %.4f" % p2)
        return im, label, {"rand_param": rand_param}

    def crop(self, im, label):
        self.cropped = True
        h, w = im.shape[:2]
        new_h = h // 2
        new_w = w // 2

        top = int(self.random_range(0, h - new_h))
        left = int(self.random_range(0, w - new_w))

        im = im[top: top + new_h, left: left + new_w, :]
        label = label[top: top + new_h, left: left + new_w]

        if im.shape[0] % 32 !=0 or im.shape[1] % 32 != 0:
            mask = np.zeros((math.ceil(im.shape[0]/32)*32, math.ceil(im.shape[1]/32)*32, 3), dtype=im.dtype)
            mask_label = np.zeros((math.ceil(im.shape[0] / 32)*32, math.ceil(im.shape[1] / 32)*32), dtype=label.dtype)

            mask[:im.shape[0], :im.shape[1], :] = im
            mask_label[:label.shape[0], :label.shape[1]] = label
            return mask, mask_label, {"top": top, "left": left}

        return im, label, {"top": top, "left": left}

    def mosaic(self, im, label):
        # to do

        h, w = im.shape[:2]

        cx = int(self.random_range(w // 5, 4 * w // 5))
        cy = int(self.random_range(h // 5, 4 * h // 5))

        mask = np.zeros_like(im)
        mask_label = np.zeros_like(label)

        im = Augmentator.image_resize(im, width=cx, height=cy)
        label = Augmentator.image_resize(label, width=cx, height=cy)
        mask[:im.shape[0], :im.shape[1]] = im
        mask_label[:im.shape[0], :im.shape[1]] = label

        # im1, label1 = next(self.images_loader.get_image())
        im1, label1 = self.dataset.get_im_label(np.random.randint(self.max_len))
        # print("got ", im1.shape)
        im1 = Augmentator.image_resize(im1, width=w - cx, height=cy)
        mask[:im1.shape[0], -im1.shape[1]:] = im1
        label1 = Augmentator.image_resize(label1, width=w - cx, height=cy)
        mask_label[:im1.shape[0], -im1.shape[1]:] = label1

        # im2, label2 = next(self.images_loader.get_image())
        im2, label2 = self.dataset.get_im_label(np.random.randint(self.max_len))
        im2 = Augmentator.image_resize(im2, width=cx, height=h - cy)
        mask[-im2.shape[0]:, :im2.shape[1]] = im2
        label2 = Augmentator.image_resize(label2, width=cx, height=h - cy)
        mask_label[-im2.shape[0]:, :im2.shape[1]] = label2

        # im3, label3 = next(self.images_loader.get_image())
        im3, label3 = self.dataset.get_im_label(np.random.randint(self.max_len))
        im3 = Augmentator.image_resize(im3, width=w - cx, height=h - cy)
        mask[-im3.shape[0]:, -im3.shape[1]:] = im3
        label3 = Augmentator.image_resize(label3, width=w - cx, height=h - cy)
        mask_label[-im3.shape[0]:, -im3.shape[1]:] = label3

        return mask, mask_label, {"cx": cx, "cy": cy}

    def resize(self, im, label):
        if not self.cropped:
            size_factor = int(self.random_range(1, 3)) * 2  # size factor 2/4
            im = cv2.resize(im, (im.shape[1] // size_factor, im.shape[0] // size_factor))
            label = cv2.resize(label, (label.shape[1] // size_factor, label.shape[0] // size_factor))

            if im.shape[0] % 32 != 0 or im.shape[1] % 32 != 0:
                mask = np.zeros((math.ceil(im.shape[0] / 32)*32, math.ceil(im.shape[1] / 32)*32, 3), dtype=im.dtype)
                mask_label = np.zeros((math.ceil(im.shape[0] / 32)*32, math.ceil(im.shape[1] / 32)*32), dtype=label.dtype)

                mask[:im.shape[0], :im.shape[1], :] = im
                mask_label[:label.shape[0], :label.shape[1]] = label
                return mask, mask_label, {"size_factor": size_factor}

            return im, label, {"size_factor": size_factor}
        return im, label, {}

    def rotate(self, im, label):
        orgShape = (im.shape[1], im.shape[0])
        maxAngle = 90
        minAngle = -maxAngle
        angle = (maxAngle - minAngle) * self.random_range() + minAngle
        im = cv2.resize(imutils.rotate_bound(im, angle), orgShape)
        label = cv2.resize(imutils.rotate_bound(label, angle), orgShape)
        return im, label, {"Angle": angle}

    def flip_lr(self, im, label):
        if torch.rand(1).item() <= 0.5 or self.debug:
            im = np.ascontiguousarray(np.fliplr(im))
            label = np.ascontiguousarray(np.fliplr(label))
        return im, label, {}

    def flip_ud(self, im, label):
        # Flip up down
        im = np.ascontiguousarray(np.flipud(im))
        label = np.ascontiguousarray(np.flipud(label))
        return im, label, {}

    def brightness(self, im, label):
        bias_factor = int(self.random_range(-60, 120))
        bias = np.ones(im.shape, dtype=np.int16) * bias_factor
        im = np.clip(im.astype(np.int16) + bias, 0, 255).astype(np.uint8)
        return im, label, {"bias factor": bias_factor}

    def hue(self, im, label):
        im = cv2.cvtColor(im, cv2.COLOR_BGR2HSV)
        hue_range = int(self.random_range(0, 180))
        im[:, :, 0] = im[:, :, 0] + hue_range
        im = cv2.cvtColor(im, cv2.COLOR_HSV2BGR)
        return im, label, {"hue-range": hue_range}

    def saturation(self, im, label):
        # Saturation
        im = cv2.cvtColor(im, cv2.COLOR_BGR2HSV)
        saturation_range = int(self.random_range(-60, 60))
        im[:, :, 1] = np.clip(im[:, :, 1] + saturation_range, 0, 255)
        im = cv2.cvtColor(im, cv2.COLOR_HSV2BGR)
        return im, label, {"saturation range": saturation_range}

    def color_space(self, im, label):
        idx = int(self.random_range(1, 3))
        if idx == 1:
            im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)  # RGB

        if idx == 2:
            im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)  # GRAY
            im = im[:, :, None] * (1, 1, 1)
        return im.astype(np.uint8), label.astype(np.float32), {"idx": idx}

    def blur(self, im, label):
        if torch.rand(1).item() <= 0.5 or self.debug:
            self.blured = True
            kernelx = random.randrange(1, 32, 2)
            kernely = random.randrange(1, 32, 2)
            im = cv2.GaussianBlur(im, (kernelx, kernely), 0)
            return im, label, {"ker_x": kernelx, "ker_y": kernely}
        return im, label, {}

    def noise(self, im, label):
        noise_range = int(self.random_range(1, 70))
        noise = np.random.randint(noise_range, size=im.shape, dtype=np.int16)
        im = np.clip(im.astype(np.int16) + noise, 0, 255).astype(np.uint8)
        return im, label, {"noise_range": noise_range}

    def shadow(self, im, label):
        alpha = self.random_range(0.4, 0.8)
        shadows, _ = random_shapes(im.shape[:2], min_shapes=4, max_shapes=14, min_size=0.05 * np.min(im.shape[:2]),
                                   max_size=3 * np.min(im.shape[:2]), allow_overlap=True, multichannel=False,
                                   intensity_range=((0, 70)))
        shadows = np.stack((shadows, shadows, shadows), 2)
        im = im.astype(np.uint8)
        im = np.where(shadows != 255, cv2.addWeighted(im, alpha, shadows, 1.0 - alpha, 0.0), im)
        return im, label, {"alpha": alpha}

    def anti_shadow(self, im, label):
        alpha = self.random_range(0.4, 0.8)
        anti_shadows, _ = random_shapes(im.shape[:2], min_shapes=4, max_shapes=14, min_size=0.05 * np.min(im.shape[:2]),
                                   max_size=3 * np.min(im.shape[:2]), allow_overlap=True, multichannel=False,
                                   intensity_range=((180, 255)))
        anti_shadows = np.stack((anti_shadows, anti_shadows, anti_shadows), 2)
        im = im.astype(np.uint8)
        im = np.where(anti_shadows != 255, cv2.addWeighted(im, alpha, anti_shadows, 1.0 - alpha, 0.0), im)
        return im, label, {"alpha": alpha}

    def disturbance(self, im, label):
        shapes, _ = random_shapes(im.shape[:2], min_shapes=1, max_shapes=7, min_size=0.01 * np.min(im.shape[:2]),
                                  max_size=0.3 * np.min(im.shape[:2]),
                                  allow_overlap=True, intensity_range=((0, 255),))
        im = np.where(shapes != 255, shapes, im)
        return im, label, {}

    def random_rain_2(self, im, label):
        rain, _ = random_shapes(im.shape[:2], min_shapes=50, max_shapes=1000,
                                min_size=0.005 * np.min(im.shape[:2]),
                                max_size=0.02 * np.min(im.shape[:2]), allow_overlap=True, multichannel=False)
        rain = np.stack((rain, rain, rain), 2)
        im = np.where(rain != 255, rain, im)
        return im, label, {}

    def equalize(self, im, label):
        im = equalize(im)
        return im, label, {}

    def solarize(self, im, label):
        thresh = int(self.random_range(75, 225))
        im = solarize(im, threshold=thresh)
        return im, label, {"thresh": thresh}

    def random_sun_flare(self, im, label):
        # flare_roi
        # x_min = 0
        # y_min = 0
        #
        # x_max = self.random_range(min_val=0.1, max_val=1)
        # y_max = self.random_range(min_val=0.1, max_val=1)

        src_radius = int(self.random_range(min_val=100, max_val=500))

        params = {"src_radius":src_radius}
        im, _label = Augmentator.augment(RandomSunFlare, im, label, params=params)
        return im, label, params

    def random_rain_1(self, im, label):
        fog_coef = self.random_range(min_val=0.3, max_val=0.3)
        alpha = self.random_range(min_val=0, max_val=0.3)
        hw = int(im.shape[1] // 3 * fog_coef)
        haze_list = self.generate_random_blur_coordinates(im.shape, hw)

        im = add_fog(im, fog_coef=fog_coef, alpha_coef=alpha, haze_list=haze_list)

        # im, label = Augmentator.augment(RandomFog, im, label)
        return im, label, {"alpha": alpha}

    def sepia(self, im, label):
        im, _label = Augmentator.augment(ToSepia, im, label)
        return im, label, {}

    def invert_img(self, im, label):
        im, _label = Augmentator.augment(InvertImg, im, label)
        return im, label, {}

    def clahe(self, im, label):
        clip_limit = self.random_range(1, 4)
        grid_size = int(self.random_range(1, 8))
        params = {"clip_limit": clip_limit, "tile_grid_size": (grid_size, grid_size)}
        im, _label = Augmentator.augment(CLAHE, im, label, params=params)
        return im, label, {}

    def random_contrast(self, im, label):
        factor = float(self.random_range(0.15, 2))
        im = np.clip(128 + factor * im - factor * 128, 0, 255).astype(np.uint8)
        # im, _label = Augmentator.augment(RandomContrast, im, label)
        return im, label, {"factor": factor}

    def grid_distorion(self, im, label):
        im, label = Augmentator.augment(GridDistortion, im, label)
        return im, label, {}

    def motion_blur(self, im, label):
        im, _label = Augmentator.augment(MotionBlur, im, label)
        return im, label, {}

    def random_snow(self, im, label):
        im, _label = Augmentator.augment(RandomSnow, im, label)
        return im, label, {}

    def postarize(self, im, label):
        n_bits = self.random_range(3, 8)
        im = posterize(im, n_bits)
        return im, label, {"num bits": n_bits}

    def shear(self, im, label):
        def apply_shear(img, shear_factor):
            shear_factor += 0.0001
            w, h = img.shape[1], img.shape[0]
            M = np.array([[1, abs(shear_factor), 0], [0, 1, 0]])
            nW = img.shape[1] + abs(shear_factor * img.shape[0])
            img = cv2.warpAffine(img, M, (int(nW), img.shape[0]))
            img = cv2.resize(img, (w, h))

            return img

        factor = self.random_range(0, 0.8)
        im = apply_shear(im, factor)
        label = apply_shear(label, factor)
        return im, label, {"shear factor": factor}

    def run(self, im, label):
        assert im.shape[:2] == label.shape[:2]
        cur_func_pool = self.draw_sequence()
        for f in cur_func_pool:
            im, label, _ = f(im, label)
            self.images.append((im, label, f.__name__))
        return im, label

    def run_func(self, im, label, func):
        # start = time.time()
        if self.debug:
            print("Before", func.__name__, "im", im.shape, im.dtype, "label", label.shape, label.dtype)
        im, label, random_numbers = func(im, label)
        if self.debug:
            print("After", func.__name__, "im", im.shape, im.dtype, "label", label.shape, label.dtype)
        return im, label, random_numbers
        # end = time.time() - start
        # self.timer[func.__name__] += round(end, 5)

    def save_all_as_subplots(self, fig_title, images):
        length = len(images)
        plt.cla()
        plt.figure(figsize=(30, 15))

        for idx, (img, label, title) in enumerate(images):
            label[label < 0.2] = None
            plt.subplot(int(sqrt(length)), int(sqrt(length)), idx + 1)
            plt.imshow(img)
            plt.imshow(label, cmap=plt.cm.jet_r, alpha=0.4)
            plt.title(title, fontdict={'fontsize': 14})

        plt.suptitle(fig_title, fontsize=30)
        plt.tight_layout()
        plt.subplots_adjust(top=0.9)
        self.output_pdf.savefig()
        plt.savefig(self.aug_result_path + fig_title + ".png")
        plt.close()

    def save_all_as_singles(self, suffix=""):
        for idx, (img, label, title) in enumerate(self.images):
            label[label < 0.2] = None
            plt.cla()
            plt.figure(figsize=(20, 10))
            plt.imshow(img)
            plt.imshow(label, cmap=plt.cm.jet_r, alpha=0.4)
            plt.title(title, fontdict={'fontsize': 15})
            plt.tight_layout()
            plt.savefig(self.aug_result_path + title + "_" + suffix + ".png")
            plt.close()

    def save_all_as_docs(self, min_lst, max_lst, d_map):
        for idx in range(len(min_lst)):
            plt.cla()
            plt.figure(figsize=(30, 15))
            plt.subplot(2, 3, 1)
            plt.imshow(min_lst[idx][0])
            plt.title(min_lst[idx][2] + "_min", fontdict={'fontsize': 20})

            plt.subplot(2, 3, 2)
            plt.imshow(max_lst[idx][0])
            plt.title(min_lst[idx][2] + "_max", fontdict={'fontsize': 20})

            fig_idx = 3
            for im_name, im_batch in d_map.items():
                plt.subplot(2, 3, fig_idx)
                plt.imshow(im_batch[idx][0])
                plt.title(im_batch[idx][2], fontdict={'fontsize': 20})
                fig_idx += 1

            plt.tight_layout()
            plt.savefig(self.aug_result_path + min_lst[idx][2] + "_docs.png")
            plt.close()

            plt.cla()
            plt.figure(figsize=(20, 10))
            fig_idx = 1
            for im_name, im_batch in d_map.items():
                plt.subplot(2, 2, fig_idx)
                plt.imshow(im_batch[idx][0])
                plt.title(im_batch[idx][2], fontdict={'fontsize': 20})
                fig_idx += 1

            plt.tight_layout()
            plt.savefig(self.aug_result_path + min_lst[idx][2] + "_images_example.png")
            plt.close()

    def save_time(self, num_runs):
        plt.cla()
        plt.figure(figsize=(30, 15))

        lst = sorted(self.timer.items(), key=lambda x: x[1])

        objects = [i[0] for i in lst]
        y_pos = np.arange(len(objects))
        performance = [i[1] / num_runs for i in lst]

        plt.bar(y_pos, performance, align='center', alpha=0.5)
        plt.xticks(y_pos, objects, rotation=60, fontsize=15)
        plt.ylabel('Time')
        plt.title('Aug Time Avg to ' + str(num_runs) + " iterations")
        plt.savefig(self.aug_result_path + "times.png")

    def draw_sequence(self):
        max_num = np.random.randint(4)
        augs = np.random.permutation(self.func_pool)[:max_num]
        return augs

        # augs = np.array([])
        # if np.random.random() <= 0.1:
        #     augs = np.append(augs, (np.random.choice(self.mixup_augs)))
        #
        # if np.random.random() <= 0.2:
        #     augs = np.append(augs, (np.random.choice(self.weather_augs)))
        #
        # for func in self.colors_augs + self.size_augs + self.distortion_augs:
        #     if np.random.random() <= self.probabilities[func]:
        #         augs = np.append(augs, func)
        # return np.random.permutation(augs)[:4]

    def run_and_print(self, f, num_images=1, fig_title=""):
        lst = []
        for i in range(num_images):
            im, label = next(self.images_loader.get_image())
            ret_im, ret_label, random_numbers = self.run_func(im.copy(), label.copy(), f)
            cur_title = f.__name__
            for k, v in random_numbers.items():
                if type(v) == float:
                    v = "%.2f" % v
                cur_title += ", " + str(k) + ":" + str(v)
            lst.append((ret_im, ret_label, cur_title))
        self.save_all_as_subplots(fig_title=fig_title, images=lst)

    def test_edge_cases(self):
        num_images = 4

        self.debug = False
        for f in tqdm(self.func_pool):
            self.reset()
            self.run_and_print(f, num_images, fig_title=f.__name__ + " Normal")
            # self.reset()
            # self.extreme_max = True
            # self.run_and_print(f, num_images, fig_title=f.__name__ + " Max")
            # self.reset()
            # self.extreme_min = True
            # self.run_and_print(f, num_images, fig_title=f.__name__ + " Min")

    def test_func(self):
        self.debug = True
        self.reset()
        im, label = next(self.images_loader.get_image())
        im, label = self.mosaic(im, label)
        plt.figure(figsize=(20, 10))
        plt.imshow(im)

        plt.show()
        #         self.run_all_separately(im, label, self.images)
        #         self.save_all_as_subplots(fig_title="_mix_test")
        return

    def test_complete(self):
        self.debug = True
        for _ in tqdm(range(30)):
            arr = []
            for idx in range(16):
                self.reset()
                im, label = next(self.images_loader.get_image())
                aug_im, aug_label = self.run(im, label)
                arr.append((aug_im, aug_label, str(idx)))
            self.save_all_as_subplots(fig_title=str(idx) + "_complete", images=arr)


# a = Augmentator()
# for elem in a.func_pool:
#     print(elem)
# a.set_evaluation_pool({"flip_ud": 0.5})
#
# im, label = next(a.images_loader.get_image())
# im, label = a.run_eval_mode(im, label)
# plt.imshow(im)
# plt.show()
# a.test_func()
# a.test_complete()
# a.test_edge_cases()
# a.close()
