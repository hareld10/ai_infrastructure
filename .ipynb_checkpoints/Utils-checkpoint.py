from scipy.ndimage.filters import maximum_filter
from scipy.ndimage.morphology import generate_binary_structure, binary_erosion
from scipy import signal
import numpy as np
import torch

""
def detect_peaks(image,peak_threshold):
    """
    Takes an image and detect the peaks usingthe local maximum filter.
    Returns a boolean mask of the peaks (i.e. 1 when
    the pixel's value is the neighborhood maximum, 0 otherwise)
    """
    
    # Filter pixels below threshold
    image[image<peak_threshold] = 0

    # define an 8-connected neighborhood
    neighborhood = generate_binary_structure(3,3)

    #apply the local maximum filter; all pixel of maximal value 
    #in their neighborhood are set to 1
    local_max = maximum_filter(image, footprint=neighborhood)==image
    #local_max is a mask that contains the peaks we are 
    #looking for, but also the background.
    #In order to isolate the peaks we must remove the background from the mask.

    #we create the mask of the background
    background = (image==0)

    #a little technicality: we must erode the background in order to 
    #successfully subtract it form local_max, otherwise a line will 
    #appear along the background border (artifact of the local maximum filter)
    eroded_background = binary_erosion(background, structure=neighborhood, border_value=1)

    #we obtain the final mask, containing only peaks, 
    #by removing the background from the local_max mask (xor operation)
    detected_peaks = local_max ^ eroded_background

    return detected_peaks  

""
def gaussian_kernel(y,x, normalised=False):
    '''
    Generates a n_x x n_y matrix with a centered gaussian 
    of standard deviation std centered on it. If normalised,
    its volume equals 1
    '''
    
    std_x = np.clip(x//4,1,5000)
    std_y = np.clip(y//4,1,5000)
    
    gaussian1D_x = signal.gaussian(x, std_x)
    gaussian1D_y = signal.gaussian(y, std_y)
    gaussian2D = np.outer(gaussian1D_x, gaussian1D_y)
    if normalised:
        gaussian2D /= (2*np.pi*(std**2))
    return gaussian2D

""
def box_nms(row,col,pred,nms_thresh,bb_thresh):

    scores = np.zeros(len(row))
    bb = np.zeros([len(row),4])
        
    for i in range(bb.shape[0]):
        
        if row[i]==0 or col[i]==0 or row[i]==pred.shape[0] or col[i]==pred.shape[1]:
            continue
                    
        # Create bb
        bb[i,0] = col[i] - np.argmax(np.flip(pred[row[i],:col[i]])<bb_thresh) # x1 (left)
        bb[i,1] = row[i] - np.argmax(np.flip(pred[:row[i],col[i]])<bb_thresh) # y1 (top)
        bb[i,2] = col[i] + np.argmax(pred[row[i],col[i]:]<bb_thresh) # x2 (right)
        bb[i,3] = row[i] + np.argmax(pred[row[i]:,col[i]]<bb_thresh) # y2 (bottom)
        
        scores[i] = pred[row[i],col[i]]
    
    x1 = bb[:,0]
    y1 = bb[:,1]
    x2 = bb[:,2]
    y2 = bb[:,3]

    areas = (x2 - x1 + 1) * (y2 - y1 + 1)
    order = scores.argsort()[::-1] # get boxes with more ious first
    
    keep = []
    while order.size > 0:
        i = order[0] # pick maximum iou box
        keep.append(i)
        xx1 = np.maximum(x1[i], x1[order[1:]])
        yy1 = np.maximum(y1[i], y1[order[1:]])
        xx2 = np.minimum(x2[i], x2[order[1:]])
        yy2 = np.minimum(y2[i], y2[order[1:]])

        w = np.maximum(0.0, xx2 - xx1 + 1) # maximum width
        h = np.maximum(0.0, yy2 - yy1 + 1) # maximum height
        inter = w * h
        ovr = inter / (areas[i] + areas[order[1:]] - inter)
                
        inds = np.where(ovr <= nms_thresh)[0]
        order = order[inds + 1]

    return bb[keep,:]
