import torch
import os
from tqdm import tqdm
import cv2
import numpy as np
import matplotlib.pyplot as plt
from Inference import InferenceEngine
from Utils import box_nms

class InferenceObjectDetection(InferenceEngine):
    def __init__(self, model, gpus_list=[0]):
        InferenceEngine.__init__(self, model, gpus_list=[0], size_factor=1)
        self.size_factor= 1
        
    def gen_label(self, x1, y1, x2, y2, classification=0, frameId=""):
        cur_label = {}
        cur_label["frameId"] = frameId
        cur_label["category"] = classification
        cur_label["bbx"] = [x1, y1, x2, y2]
        return cur_label
    
    def infer_single(self, model_input, name):
        with torch.no_grad():
            pred        = model(model_input)
            pred        = torch.squeeze(pred[0],0)
            pred[0,:,:] = torch.sigmoid(pred[0,:,:])
            pred        = pred.cpu().numpy()

            centers = np.stack([pred[0,:,:],pred[0,:,:],pred[0,:,:]],axis=2)
            peaks   = detect_peaks(centers,peak_thresh)
            peaks   = peaks[:,:,0]*1
            
        row,col = peaks.nonzero()
        bb = box_nms(row,col,pred,nms_thresh)
        
        bounding_boxes = []
        
        for i in range(bb.shape[0]):
            x1 = int(bb[i,0])
            y1 = int(bb[i,1])
            x2 = int(bb[i,2])
            y2 = int(bb[i,3])
            bounding_boxes.append(self.gen_label(x1, y1, x2, y2))
                                  
        return bounding_boxes
    
    def parse_label(self, batch):
        bbs = []
        
        cx_vec     = batch['cx_vec']
        cy_vec     = batch['cy_vec']
        w_vec      = batch['w_vec']
        h_vec      = batch['h_vec']

        for i in range(batch['label_num'][0]):
            x1 = int(cx_vec[i] - w_vec[i]/2)
            x2 = int(cx_vec[i] + w_vec[i]/2)
            y1 = int(cy_vec[i] - h_vec[i]/2)
            y2 = int(cy_vec[i] + h_vec[i]/2)
            bbs.append(gen_label(x1, y1, x2, y2))
        
        return bbs
    
    def infer_DataLoader(self, data_loader, input_key="input", label_key="label"):
        pbar = tqdm(iter(data_loader),leave=False, total=len(data_loader))
        
        # Empty Predcitions and Labels
        self.current_predictions, self.current_labels = [], []
        for batch in pbar:
            cur_pred = self.infer_single(batch[input_key].cuda(self.gpus_list[0]), name=)
            self.current_predictions.append(cur_pred)
            
            self.current_labels.append(self.parse_label(batch))
            
        return self.current_predictions
