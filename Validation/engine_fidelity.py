from matplotlib import gridspec, colors

from Calibrator.calibrator_utils import plot_pc_simple, process_lidar_to_image_on_ax_simple, process_lidar_to_image_on_ax
from wiseTypes.classes_dims import Road
from tqdm import tqdm
import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd
import time
from scipy.spatial import distance
import scipy as sp
import scipy.interpolate
from sklearn.cluster import DBSCAN
from DataPipeline.shared_utils import PrintException, toRAzEl, pickle_write
from Evaluation.evaluation_utils import make_video_from_dir, draw
from OpenSource.WisenseEngine import WisenseEngine
from scipy.interpolate import interp1d, interp2d
import matplotlib

font_size = 12
scatter_size = 0.5
cfar_thresh = 10
plt.style.use('dark_background')


def engine_fidelity(engine, force, debug=False, video=False, gen_instances_df=False):
    print("\n\n######### Engine Fidelity #########")
    vel_delta = np.zeros(len(engine), dtype=np.float32)
    # robust_vel_4dfft = np.zeros(len(engine), dtype=np.float32)
    # platform_velocity_delta = np.zeros(len(engine), dtype=np.float32)

    save_path = "./data_fidelity/images/"
    for engine_idx in tqdm(range(len(engine))):
        try:
            label_path = engine.drive_context.camera_to_lidar + "/camera_to_lidar_" + engine.get_lidar_id(idx=engine_idx) + ".json"
            if (not debug) and (not force) and os.path.exists(label_path): continue

            # Data
            rgb_img = engine.get_image(engine_idx)
            seg_label = engine.get_segmentation_label(engine_idx)
            lidar_df = engine.get_lidar_df(engine_idx)
            lidar_df = lidar_df.apply(pd.to_numeric, errors='coerce')
            processed_delta = engine.get_processed_delta(engine_idx)
            delta = engine.get_delta(engine_idx)
            cude_4d_fft = engine.get_4dfft(engine_idx)
            delta_clusters = engine.get_clusters(p_delta=processed_delta)
            lidar_camera_df = lidar_df[lidar_df['x_lidar_camera'].notna()]
            lidar_radar_df = lidar_df[lidar_df["x"].notna()]
            labels_2d = engine.get_2d_label(engine_idx)
            labels_3d = engine.get_lidar_camera_fusion_labels(idx=engine_idx, as_obj=True)
            vel_delta[engine_idx] = engine.get_platform_velocity(idx=engine_idx)

            plt.close()
            fig = plt.figure(figsize=(20, 12), dpi=300)
            gs = gridspec.GridSpec(ncols=6, nrows=3)
            plt.suptitle('Data Fidelity' + '\n' + '\n' + engine.get_drive_name() + ' | ' + engine.get_id(engine_idx), fontsize=font_size)

            # Camera image
            ax = fig.add_subplot(gs[0, 0])
            plt.imshow(draw(rgb_img, labels_2d))
            plt.imshow(seg_label.get_img(colors=True), alpha=0.3)
            plt.xticks([])
            plt.yticks([])
            plt.title('Camera Image', fontsize=font_size)

            # 4d FFT
            ax = fig.add_subplot(gs[1, 0])
            plt.scatter(cude_4d_fft['x'], cude_4d_fft['y'], c=cude_4d_fft['dop'], cmap='jet', s=scatter_size)
            plt.colorbar(orientation='horizontal')
            plt.title('4D FFT Scatter\n (colormap=dop' + ') | CFAR=' + str(cfar_thresh) + ' [dB]', fontsize=font_size)
            plt.xlabel('[m]', fontsize=font_size)
            plt.ylabel('[m]', fontsize=font_size)
            plt.xlim([-engine.radar_fov_az // 2, engine.radar_fov_az // 2])
            plt.ylim(([engine.min_range, engine.max_range]))

            # Delta
            ax = fig.add_subplot(gs[1, 1])
            plt.scatter(delta['x'], delta['y'], c=delta['dop'], cmap='jet', s=scatter_size)
            plt.colorbar(orientation='horizontal')
            plt.title('Delta Scatter\n (colormap=dop' + ') | CFAR=' + str(cfar_thresh) + ' [dB]', fontsize=font_size)
            plt.xlabel('[m]', fontsize=font_size)
            plt.ylabel('[m]', fontsize=font_size)
            plt.xlim([-engine.radar_fov_az // 2, engine.radar_fov_az // 2])
            plt.ylim(([engine.min_range, engine.max_range]))
            [x.plot(ax, radar=True) for x in labels_3d]

            # Lidar Plot
            ax = fig.add_subplot(gs[2, 0])
            plt.scatter(lidar_radar_df["x"], lidar_radar_df["y"], c=lidar_radar_df["z"], cmap='jet', s=scatter_size)  # plot point cloud
            plt.colorbar(orientation='horizontal')
            # plot_bb_bev(config, ax, lidar_3dvd_label_proj)
            plt.title('Lidar Scatter\n (colormap=height)', fontsize=font_size)
            plt.xlabel('[m]', fontsize=font_size)
            plt.ylabel('[m]', fontsize=font_size)
            plt.xlim([-engine.radar_fov_az // 2, engine.radar_fov_az // 2])
            plt.ylim(([engine.min_range, engine.max_range]))
            [x.plot(ax, radar=True) for x in labels_3d]

            # Processed Delta
            ax = fig.add_subplot(gs[:, -2:])
            for cluster_id, cluster in delta_clusters.items():
                cluster.plot(ax=ax)
            plt.title('Processed Delta, Macro', fontsize=font_size)
            plt.xlabel('[m]', fontsize=font_size)
            plt.ylabel('[m]', fontsize=font_size)
            plt.xlim([-engine.radar_fov_az // 2, engine.radar_fov_az // 2])
            plt.ylim(([engine.min_range, engine.max_range]))
            [x.plot(ax, radar=True) for x in labels_3d]

            # Velocity
            ax = fig.add_subplot(gs[2, 1])
            plt.plot(vel_delta[:engine_idx], 'r', label='vel_delta')
            plt.title('Velocity', fontsize=font_size)
            plt.legend()
            plt.ylabel('[m/s]', fontsize=font_size)
            plt.xlabel('Frame', fontsize=font_size)
            plt.grid(True)

            # Histograms
            ax = fig.add_subplot(gs[0, 2])
            plt.hist(processed_delta["value_real"], rwidth=0.9)
            plt.title('Processed Delta | value_real', fontsize=font_size)

            ax = fig.add_subplot(gs[1, 2])
            plt.hist(processed_delta["power_im"], rwidth=0.9)
            plt.title('Processed Delta | power_im', fontsize=font_size)

            ax = fig.add_subplot(gs[2, 2])
            plt.hist(processed_delta["noise"], rwidth=0.9)
            plt.title('Processed Delta  | Noise', fontsize=font_size)

            ax = fig.add_subplot(gs[0, 3])
            plt.hist(cude_4d_fft["value_real"], rwidth=0.9)
            plt.title('4D FFT | value_real', fontsize=font_size)

            ax = fig.add_subplot(gs[1, 3])
            plt.hist(cude_4d_fft["power_im"], rwidth=0.9)
            plt.title('4D FFT | power_im', fontsize=font_size)

            ax = fig.add_subplot(gs[2, 3])
            plt.hist(cude_4d_fft["noise"], rwidth=0.9)
            plt.title('4D FFT  | Noise', fontsize=font_size)

            plt.tight_layout(pad=1)
            if video or debug:
                plt.savefig(save_path + engine.get_id(idx=engine_idx) + ".png")
            if debug:
                # plt.show()
                exit()

        except Exception as e:
            PrintException()
            if debug:
                exit()
            continue

    if video: make_video_from_dir(df=engine.df, images_dir=save_path, suffix="_" + str(time.time()).split(".")[0])

if __name__ == '__main__':
    data_engine1 = WisenseEngine(base_name="Wisense", base_path_drive="/media/amper/data_backup7/76mWF/200902_lidar_drive_highway_W92_M15_RF1/", rect_mode=True)
    data_engine1.update_df_range(start_idx=1600, end_idx=1950)
    data_engine2 = WisenseEngine(base_name="Wisense", base_path_drive="/media/amper/data_backup7/76mWF/200831_lidar_drive_1_urban_W92_M15_RF1/", rect_mode=True)
    data_engine2.update_df_range(start_idx=550, end_idx=800, shuffle=False)

    for data_engine in [data_engine2, data_engine1]:
        engine_fidelity(data_engine, force=True, debug=False, video=True)
