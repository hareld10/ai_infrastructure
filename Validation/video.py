from Validation.data_fidelity_runner import config
import cv2
from tqdm import tqdm
from os import path

fourcc = cv2.VideoWriter_fourcc(*'mp4v')

video_name = config.save_path + "/../" +  config.dataset + '.mp4'

first = True
for idx in tqdm(range(config.start_idx, config.end_idx)):  # len(config.df))):
    if first:
        frame = cv2.imread(config.save_path + str(idx) + '_' + str(config.df.loc[idx, "timestampRadar"]) + '.png')
        if frame is None:
            continue
        height, width, layers = frame.shape
        video = cv2.VideoWriter(video_name, fourcc, 6, (width, height))
        first = False

    img_path = config.save_path + str(idx) + '_' + str(config.df.loc[idx, "timestampRadar"]) + '.png'

    if path.exists(img_path):
        video.write(cv2.imread(img_path))

cv2.destroyAllWindows()
video.release()