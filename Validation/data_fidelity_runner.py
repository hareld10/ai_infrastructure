import pathlib

import matplotlib
import pandas as pd
import cv2
import os
import json
import numpy as np
import matplotlib.pyplot as plt
from IPython.core.display import display, HTML
import torch
import warnings

from matplotlib import gridspec
from tqdm import tqdm_notebook as tqdm
import os.path
from os import path
import matplotlib.lines as lines

from DataPipeline.DNNs.wrappers.deepLabWrapper import DeepLabWrapper
from DataPipeline.settings import Context
from wiseTypes.SegLabel import SegLabel

module_path = str(pathlib.Path(__file__).parent.absolute())
# plt.style.use('dark_background')
from DataPipeline.shared_utils import PrintException

plt.style.use('dark_background')

from Validation.utils import *

def get_cmap(num_colors, name='hsv'):
    '''Returns a function that maps each index in 0, 1, ..., n-1 to a distinct
    RGB color; the keyword argument name must be a standard mpl colormap name.'''
    return plt.cm.get_cmap(name, num_colors)

class config:
    radar_fov_az = 80  # [deg]
    radar_fov_el = 30  # [deg]
    min_range = 2  # [m]
    max_range = 76  # [m]
    label_confidence = 0.4
    cfar_thresh = 20  # [dB]

    cmap = get_cmap(30)

    vel_est_threshold = 1  # [m/s]

    # Plot parameters
    plt_x_min = -35  # [m]
    plt_x_max = 35  # [m]
    plt_y_max = 75  # [m]
    fontsize = 18

    num_rows = 3
    num_cols = 4
    scatter_size = 0.5

    # Load spatial calibration file
    module_path = str(pathlib.Path(__file__).parent.absolute())
    fileName = module_path + "/spatial_calibration_200909"
    with open(fileName, "r") as fp:
        spatial_calibration_params = json.load(fp)

    ds_map = {"200902_lidar_drive_highway_W92_M15_RF1" : (0, 2000),
              "200831_lidar_drive_1_urban_W92_M15_RF1" : (0, 2500)}

    dataset = '200902_lidar_drive_highway_W92_M15_RF1'  # '200831_lidar_drive_1_urban_W92_M15_RF1' #
    # dataset = '200831_lidar_drive_1_urban_W92_M15_RF1'

    disc = '/data_backup7/76mWF/'

    b_path = '/media/amper/' + disc + '//' + dataset
    df = pd.read_csv(b_path + '/aux/sync_data_radar_lidar.csv')
    df['base_path'] = b_path

    # df = df.sample(frac=1).reset_index(drop=True)
    save_path = module_path + '/data_fidelity/' + dataset + '/'
    os.makedirs(save_path, exist_ok=True)

    # Velocity tracker
    velocity_tracker = pd.DataFrame(columns=['vx_delta_cuda', 'vy_delta_cuda', 'vz_delta_cuda', 'v_delta_cuda', 'vx_delta', 'vy_delta', 'vz_delta', 'v_delta', 'timestamp'])

    start_idx = ds_map[dataset][0]
    if start_idx == 0:
        end_idx = len(df)
    else:
        end_idx = ds_map[dataset][1]  # len(df)

    colormap = "robust_dop" # "z"
    context = Context(args=None, index=0, create_dirs=False, base_path=b_path)
    deep_lab_engine = DeepLabWrapper()
    debug_mode = False

    colors = {"car": (0, 0, 255), "traffic light": (255, 0, 0), "truck": (135, 206, 250),
              "bus": (100, 149, 237),
              "pedestrian": (255, 165, 0), "person": (255, 165, 0), "bicycle": (170, 0, 255),
              "motorcycle": (216, 0, 155), "forbidden_pedestrian": (238, 130, 238)}


robust_vel_delta = np.zeros((len(config.df)), dtype=np.float32)
robust_vel_4dfft = np.zeros((len(config.df)), dtype=np.float32)
platform_velocity_delta = np.zeros((len(config.df)), dtype=np.float32)

if __name__ == '__main__':
    for idx in tqdm(range(config.start_idx, config.end_idx)):
        print(idx)
        try:
            with open(str(config.df.loc[idx, 'base_path']) + "/metadata/metadata_cuda_4dfft_" + str(config.df.loc[idx, "timestampRadar"]) + ".json") as f:
                meta_data_4dfft = json.load(f)
            with open(str(config.df.loc[idx, 'base_path']) + "/metadata/metadata_cuda_delta_" + str(config.df.loc[idx, "timestampRadar"]) + ".json") as f:
                meta_data_delta = json.load(f)

            robust_vel_delta[idx] = np.linalg.norm(meta_data_delta["robust_platform_velocity"])
            platform_velocity_delta[idx] = np.linalg.norm(meta_data_delta["platform_velocity"])

            # Update bins
            meta_data_4dfft['range_bin'] = (meta_data_4dfft['axis_params'][0][1] - meta_data_4dfft['axis_params'][0][0]) / (meta_data_4dfft['axis_params'][0][2] - 1)
            meta_data_4dfft['az_bin'] = (meta_data_4dfft['axis_params'][1][1] - meta_data_4dfft['axis_params'][1][0]) / (meta_data_4dfft['axis_params'][1][2] - 1)
            meta_data_4dfft['el_bin'] = (meta_data_4dfft['axis_params'][2][1] - meta_data_4dfft['axis_params'][2][0]) / (meta_data_4dfft['axis_params'][2][2] - 1)
            meta_data_4dfft['dop_bin'] = (meta_data_4dfft['axis_params'][3][1] - meta_data_4dfft['axis_params'][3][0]) / (meta_data_4dfft['axis_params'][3][2] - 1)
            rgb_path = config.df.loc[idx, 'base_path'] + "/camera_data/camera_rgb/camera_rgb_" + str(config.df.loc[idx, 'timestampCamera']) + ".png"
            # rgb_img = cv2.imread(rgb_path)[..., ::-1]
            rgb_img = cv2.cvtColor(cv2.imread(rgb_path), cv2.COLOR_BGR2RGB)
            lidar_pc_path = config.df.loc[idx, 'base_path'] + "/lidar_data/lidar_pc/lidar_pc_" + str(config.df.loc[idx, 'timestampLidar'])[:-4] + ".npz"
            lidar_pc = np.load(lidar_pc_path)['arr_0']  # ndarray (x,y,z)

            # Project lidar to radar coordinate frame
            lidar_pc_proj = project_lidar_2_radar(config, lidar_pc)

            # RGB Perception
            hard_label = np.load(config.context.camera_seg_class + "/camera_seg_class_" + str(config.df.loc[idx, 'timestampCamera']) + ".npz")["arr_0"]
            soft_label = np.load(config.context.camera_seg_soft + "/camera_seg_soft_" + str(config.df.loc[idx, 'timestampCamera']) + ".npz")["arr_0"]
            seg_label = SegLabel(img=hard_label, engine=config.deep_lab_engine)

            od_label = read_label(config.context.camera_det_ensemble + "/camera_det_ensemble_" + str(config.df.loc[idx, 'timestampCamera']) + ".json")
            for cur_label in od_label:
                obj = cur_label['cat']
                cat_id = cur_label['cat_id']
                score = cur_label['score']
                x1, y1, x2, y2 = cur_label['bbox']
                # color = self.other_colors[cat_id]
                if obj in config.colors:
                    color = config.colors[obj]
                else:
                    color = (0, 255, 0)
                cv2.rectangle(rgb_img, (x1, y1), (x2, y2), color, 2)
                cv2.putText(rgb_img, '{}, {:.3f}'.format(obj, score),
                            (x1, y1 + 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                            color, 1)

            # Get lidar 3dvd label
            lidar_3dvd_path = str(config.df.loc[idx, 'base_path']) + "/lidar_labels/lidar_3d/lidar_3d_" + str(config.df.loc[idx, "timestampLidar"])[:-4] + ".json"
            lidar_3dvd_label = read_label(lidar_3dvd_path)  # (cx,cy,cz,dx,dy,dz,heading) notation in lidar coordinates

            # Build label array
            lidar_3dvd_label_proj = np.zeros((len(lidar_3dvd_label["pred_boxes"]), 9), dtype=np.float32)  # (cx, cy, cz, dx, dy, dz, heading, category, score)
            for idx_label in range(len(lidar_3dvd_label["pred_boxes"])):
                lidar_3dvd_label_proj[idx_label, :7] = lidar_3dvd_label["pred_boxes"][idx_label]
                lidar_3dvd_label_proj[idx_label, 7] = lidar_3dvd_label["pred_labels"][idx_label]
                lidar_3dvd_label_proj[idx_label, 8] = lidar_3dvd_label["pred_scores"][idx_label]

            # Project lidar label to radar coordinate frame (x,y,z)
            lidar_3dvd_label_proj = project_lidar_2_radar(config, lidar_3dvd_label_proj)

            # Filter by category
            # vehicle == 1
            lidar_3dvd_label_proj = lidar_3dvd_label_proj[lidar_3dvd_label_proj[:, 7] == 1]

            # Filter by score
            lidar_3dvd_label_proj = lidar_3dvd_label_proj[lidar_3dvd_label_proj[:, 8] > config.label_confidence]

            ####################################################
            # Get radar delta
            ####################################################
            def read_pc(pc_path):
                tmp_df = pd.read_csv(pc_path)
                tmp_df = tmp_df[tmp_df["r"] > config.min_range]
                tmp_df = tmp_df.reset_index(drop=True)
                return tmp_df

            radar_delta_path = str(config.df.loc[idx, 'base_path']) + "/radar_data/cuda_delta/cuda_delta_" + str(config.df.loc[idx, "timestampRadar"]) + ".csv"
            radar_delta = read_pc(radar_delta_path)

            # radar_delta = pd.read_csv(radar_delta_path)
            # radar_delta = radar_delta[radar_delta["r"] > config.min_range]
            # radar_delta = radar_delta.reset_index(drop=True)

            radar_p_delta_path = str(config.df.loc[idx, 'base_path']) + "/radar_data/cuda_processed_delta/cuda_processed_delta_" + str(config.df.loc[idx, "timestampRadar"]) + ".csv"
            radar_p_delta = read_pc(radar_p_delta_path)

            radar_4dfft_path = str(config.df.loc[idx, 'base_path']) + "/radar_data/cuda_4dfft/cuda_4dfft_" + str(config.df.loc[idx, "timestampRadar"]) + ".csv"
            radar_4dfft = read_pc(radar_4dfft_path)
            # radar_4dfft = pd.read_csv(radar_4dfft_path)
            # radar_4dfft = radar_4dfft[radar_4dfft["r"] > config.min_range]
            # radar_4dfft = radar_4dfft.reset_index(drop=True)

            radar_4dfft["total_power"] = db_np(radar_4dfft["value_real"].values ** 2 + radar_4dfft["power_im"].values ** 2)  # value=real ; power=imaginary
            radar_4dfft["noise"] = db_np(radar_4dfft["noise"])

            rng_shape = meta_data_4dfft['axis_params'][0][2]
            dop_shape = meta_data_4dfft['axis_params'][3][2]
            az_shape = meta_data_4dfft['axis_params'][1][2]
            el_shape = meta_data_4dfft['axis_params'][2][2]

            radar_4dfft_filtered = pd.DataFrame()
            for key in radar_4dfft.keys():
                radar_4dfft_filtered[key] = radar_4dfft[key][radar_4dfft['total_power'] > (radar_4dfft['noise'] + config.cfar_thresh)]

            # Velocity compensation
            v_fold = meta_data_4dfft['axis_params'][3][1]
            #         radar_4dfft_filtered = velocity_compensation(config, v_fold, radar_4dfft_filtered)

            # Build 4dfft dense tensor ###### TODO: dop_indices were calculated based on garbage dop values
            radar_fft4d_dense = torch.zeros(rng_shape, dop_shape, az_shape, el_shape, 2)  # (range,doppler,az,el,real/imag)
            radar_fft4d_dense[radar_4dfft_filtered['range_indices'].to_numpy(),
                              radar_4dfft_filtered['dop_indices'].to_numpy(),
                              radar_4dfft_filtered['az_indices'].to_numpy(),
                              radar_4dfft_filtered['el_indices'].to_numpy(), 0] = torch.tensor(radar_4dfft_filtered['value_real'].to_numpy()).float()
            radar_fft4d_dense[radar_4dfft_filtered['range_indices'].to_numpy(),
                              radar_4dfft_filtered['dop_indices'].to_numpy(),
                              radar_4dfft_filtered['az_indices'].to_numpy(),
                              radar_4dfft_filtered['el_indices'].to_numpy(), 1] = torch.tensor(radar_4dfft_filtered['power_im'].to_numpy()).float()

            # Max on doppler to reduce sparsity and data size
            radar_fft4d_dense_max_dop = torch.max(radar_fft4d_dense, dim=1).values  # (range,az,el,real/imag)

            # Reorder to range-az view (weights on real/image el)
            model_input = torch.zeros(radar_fft4d_dense_max_dop.shape[0], radar_fft4d_dense_max_dop.shape[1], radar_fft4d_dense_max_dop.shape[2] * 2)
            model_input[:, :, ::2] = radar_fft4d_dense_max_dop[:, :, :, 0]
            model_input[:, :, 1::2] = radar_fft4d_dense_max_dop[:, :, :, 1]

            model_input = model_input.permute(2, 0, 1)

            # torch.save(model_input, './model_input.pt')

            eps = 1e-7
            model_input_np = model_input[::2, :, :].numpy() + 1j * model_input[1::2, :, :].numpy() + eps

            model_input_np_db = db_np(np.abs(model_input_np) ** 2)
            model_input_np_db = np.clip(model_input_np_db, 0, model_input_np_db.max())
            model_input_np_db_plot = model_input_np_db.max(axis=0)
            model_input_np_db_plot = model_input_np_db_plot.T
            model_input_np_db_plot = cv2.resize(model_input_np_db_plot, (512, 256))

            # 4dFFT scatter
            pt_filter = np.logical_and(radar_4dfft_filtered['z'] > -2, radar_4dfft_filtered['z'] < 4)

            radar_4dfft_scatter = {}

            radar_4dfft_scatter = pd.DataFrame()
            for key in radar_4dfft_filtered.keys():
                radar_4dfft_scatter[key] = radar_4dfft_filtered[key][pt_filter]

            # Create figure
            fig = plt.figure(figsize=(25, 20), dpi=100)
            gs = gridspec.GridSpec(ncols=4, nrows=3)

            plt.suptitle('Data Fidelity' + '\n' + '\n' + config.dataset + ' | ' + str(config.df.loc[idx, "timestampRadar"]), fontsize=config.fontsize)

            # Camera image
            # plt.subplot(config.num_rows, config.num_cols, 1)
            ax = fig.add_subplot(gs[0, 0])
            plt.imshow(rgb_img)
            plt.imshow(seg_label.get_img(colors=True), alpha=0.3)
            plt.xticks([])
            plt.yticks([])
            plt.title('Camera Image', fontsize=config.fontsize)

            # 4dfft plot
            # ax = plt.subplot(config.num_rows, config.num_cols, 2)
            ax = fig.add_subplot(gs[0, 1])
            plt.imshow(model_input_np_db_plot, cmap='jet')
            plt.colorbar(orientation='horizontal')
            plt.title('4D FFT Range-Az | CFAR=' + str(config.cfar_thresh) + ' [dB]', fontsize=config.fontsize)
            plt.ylabel('Az', fontsize=config.fontsize)
            plt.xlabel('Range', fontsize=config.fontsize)
            #         plt.yticks([])
            plot_bb_rng_az(config, meta_data_4dfft, ax, lidar_3dvd_label_proj, model_input_np_db_plot.shape)
            ax.set_xlim(0, model_input_np_db_plot.shape[1])
            ax.set_ylim(1, model_input_np_db_plot.shape[0])
            # Velocity plot
            #plt.subplot(config.num_rows, config.num_cols, 3)
            ax = fig.add_subplot(gs[2, 1])
            plt.plot(robust_vel_delta[config.start_idx:idx], 'r', label='robust_vel_delta')
            plt.plot(platform_velocity_delta[config.start_idx:idx], 'b', label="original delta platform velocity")

            #         plt.plot(config.velocity_tracker['v_delta'],'r',label='delta corrected')
            #         plt.plot(config.velocity_tracker['v_delta_cuda'],'--w',label='delta')
            plt.title('Velocity', fontsize=config.fontsize)
            plt.legend()
            plt.ylabel('[m/s]', fontsize=config.fontsize)
            plt.xlabel('Frame', fontsize=config.fontsize)
            #         plt.ylim([np.min(np.array(config.velocity_tracker['v_delta'])),np.max(np.array(config.velocity_tracker['v_delta']))])
            plt.grid(True)

            # 4dfft scatter
            # ax = plt.subplot(config.num_rows, config.num_cols, 6)
            ax = fig.add_subplot(gs[1, 0])
            plt.scatter(radar_4dfft_scatter['x'], radar_4dfft_scatter['y'], c=radar_4dfft_scatter[config.colormap], cmap='jet', s=config.scatter_size)
            plt.colorbar(orientation='horizontal')
            #             plt.scatter(lidar_3dvd_label_proj[:,0],lidar_3dvd_label_proj[:,1],c='w',s=config.scatter_size) # plot label center
            plot_bb_bev(config, ax, lidar_3dvd_label_proj)
            plt.title('4D FFT Scatter\n (colormap=' + config.colormap + ') | CFAR=' + str(config.cfar_thresh) + ' [dB]', fontsize=config.fontsize)
            plt.xlabel('[m]', fontsize=config.fontsize)
            plt.ylabel('[m]', fontsize=config.fontsize)
            plt.xlim([config.plt_x_min, config.plt_x_max])
            plt.ylim(([0, config.plt_y_max]))

            # Delta plot
            # colormap = radar_delta['z']

            # ax = plt.subplot(config.num_rows, config.num_cols, 7)
            ax = fig.add_subplot(gs[1, 1])
            plt.scatter(radar_delta['x'], radar_delta['y'], c=radar_delta[config.colormap], cmap='jet', s=config.scatter_size)
            # plt.clim(-v_fold/2, v_fold/2)
            plt.colorbar(orientation='horizontal')
            #             plt.scatter(lidar_3dvd_label_proj[:,0],lidar_3dvd_label_proj[:,1],c='w',s=config.scatter_size) # plot label center
            plot_bb_bev(config, ax, lidar_3dvd_label_proj)
            plt.title('Delta Scatter\n (colormap=' + str(config.colormap) + ')', fontsize=config.fontsize)
            plt.xlabel('[m]', fontsize=config.fontsize)
            plt.ylabel('[m]', fontsize=config.fontsize)
            plt.xlim([config.plt_x_min, config.plt_x_max])
            plt.ylim(([0, config.plt_y_max]))

            # Cluster Example
            # ax = plt.subplot(config.num_rows, config.num_cols, 9)
            ax = fig.add_subplot(gs[:, 2:])
            for cls_id in range(int(radar_p_delta["cluster_id"].max())):
                cluster_df = radar_p_delta[radar_p_delta["cluster_id"] == cls_id]
                # Draw quiver
                centroid = np.mean(np.vstack([cluster_df['x'].values, cluster_df['y'].values, cluster_df['z'].values]), axis=-1)
                centroid_v = np.mean(np.vstack([cluster_df['vx'].values, cluster_df['vy'].values]), axis=-1)
                # print(centroid, centroid_v)
                plt.scatter([centroid[0]], [centroid[1]], c="yellow", s=config.scatter_size * 6)
                plt.quiver([centroid[0]], [centroid[1]], [centroid_v[0]], [centroid_v[1]], color="yellow")
                # scale=3
                # plt.arrow([centroid[0]], [centroid[1]], [centroid_v[0]*scale], [centroid_v[1]*scale], scale=5)
                plt.scatter(cluster_df['x'], cluster_df['y'], c=config.cmap(cls_id), s=config.scatter_size*4)
            # plt.colorbar(orientation='horizontal')
            #             plt.scatter(lidar_3dvd_label_proj[:,0],lidar_3dvd_label_proj[:,1],c='w',s=config.scatter_size) # plot label center
            plot_bb_bev(config, ax, lidar_3dvd_label_proj)
            plt.title('Processed Delta, Macro', fontsize=config.fontsize)
            plt.xlabel('[m]', fontsize=config.fontsize)
            plt.ylabel('[m]', fontsize=config.fontsize)
            plt.xlim([config.plt_x_min, config.plt_x_max])
            plt.ylim(([0, config.plt_y_max]))

            # ax = plt.subplot(config.num_rows, config.num_cols, 10)
            # cluster_id_choice = np.random.choice(radar_p_delta["cluster_id"].values)
            # cluster_df = radar_p_delta[radar_p_delta["cluster_id"] == cluster_id_choice]
            # plt.scatter(cluster_df['x'], cluster_df['y'], c=cluster_df['z'], cmap='jet', s=config.scatter_size)
            # # plt.colorbar(orientation='horizontal')
            # #             plt.scatter(lidar_3dvd_label_proj[:,0],lidar_3dvd_label_proj[:,1],c='w',s=config.scatter_size) # plot label center
            # # plot_bb_bev(config, ax, lidar_3dvd_label_proj)
            # plt.title('Cluster ' + str(cluster_id_choice) + 'Example Scatter, Micro', fontsize=config.fontsize)
            # plt.xlabel('[m]', fontsize=config.fontsize)
            # plt.ylabel('[m]', fontsize=config.fontsize)

            # Lidar plot
            # ax = plt.subplot(config.num_rows, config.num_cols, 5)
            ax = fig.add_subplot(gs[2, 0])
            plt.scatter(lidar_pc_proj[:, 0], lidar_pc_proj[:, 1], c=lidar_pc_proj[:, 2], cmap='jet', s=config.scatter_size)  # plot point cloud
            plt.colorbar(orientation='horizontal')
            #             plt.scatter(lidar_3dvd_label_proj[:,0],lidar_3dvd_label_proj[:,1],c='w',s=config.scatter_size) # plot label center
            plot_bb_bev(config, ax, lidar_3dvd_label_proj)
            plt.title('Lidar Scatter\n (colormap=height) | min score:' + str(config.label_confidence), fontsize=config.fontsize)

            plt.xlabel('[m]', fontsize=config.fontsize)
            plt.ylabel('[m]', fontsize=config.fontsize)
            plt.xlim([config.plt_x_min, config.plt_x_max])
            plt.ylim(([0, config.plt_y_max]))

            # Plot velocity histogram
            # vx_delta = -radar_delta['dop'] * np.cos(np.arcsin(radar_delta['u_sin']))
            # vy_delta = -radar_delta['dop'] * radar_delta['u_sin']
            #
            # vx_4dfft_unc = -radar_4dfft_scatter['uncorrected_dop'] * np.cos(np.arcsin(radar_4dfft_scatter['u_sin']))
            # vy_4dfft_unc = -radar_4dfft_scatter['uncorrected_dop'] * radar_4dfft_scatter['u_sin']
            #
            # vx_4dfft = -radar_4dfft_scatter['dop'] * np.cos(np.arcsin(radar_4dfft_scatter['u_sin']))
            # vy_4dfft = -radar_4dfft_scatter['dop'] * radar_4dfft_scatter['u_sin']
            #
            # vx_4dfft_robust_dop = -radar_4dfft_scatter['robust_dop'] * np.cos(np.arcsin(radar_4dfft_scatter['u_sin']))
            # vy_4dfft_robust_dop = -radar_4dfft_scatter['robust_dop'] * radar_4dfft_scatter['u_sin']
            #
            # vx_delta_robust_dop = -radar_delta['robust_dop'] * np.cos(np.arcsin(radar_delta['u_sin']))
            # vy_delta_robust_dop = -radar_delta['robust_dop'] * radar_delta['u_sin']
            #
            # plt.subplot(config.num_rows, config.num_cols, 8)
            # # n, bins, patches = plt.hist(vx_delta, bins=150, density=True, alpha=0.75, color='green', label='delta')
            # #         n, bins, patches = plt.hist(vx_4dfft_unc, bins=150, density=True, alpha=0.75, color='green',label='4dfft')
            # # n, bins, patches = plt.hist(vx_4dfft, bins=150, density=True, alpha=0.75, color='white', label='4dfft corrected')
            # n, bins, patches = plt.hist(vx_4dfft_robust_dop, bins=150, density=True, alpha=0.6, color='red', label='4dfft robust_dop')
            # n, bins, patches = plt.hist(vx_delta_robust_dop, bins=150, density=True, alpha=0.6, color='blue', label='delta robust_dop')
            # #             plt.grid(True)
            # plt.title('Vx Histogram', fontsize=config.fontsize)
            # plt.legend()
            # plt.xlim([-v_fold, v_fold])


            # Uncorrected Dop
            # plt.subplot(config.num_rows, config.num_cols, 12)
            # n, bins, patches = plt.hist(radar_4dfft_scatter['dop'], bins=150, density=True, alpha=0.6, color='green', label='4dfft - dop')
            # n, bins, patches = plt.hist(radar_delta['dop'], bins=150, density=True, alpha=0.6, color='white', label='delte - dop')
            #
            # #             plt.grid(True)
            # plt.title('Raw Dop Histogram', fontsize=config.fontsize)
            # plt.legend()
            # plt.xlim([-v_fold, v_fold])

            # Uncorrected Dop
            # plt.subplot(config.num_rows, config.num_cols, 11)
            # n, bins, patches = plt.hist(radar_4dfft_scatter['uncorrected_dop'], bins=150, density=True, alpha=0.6, color='red', label='4dfft - uncorrected_dop')
            # n, bins, patches = plt.hist(radar_delta['uncorrected_dop'], bins=150, density=True, alpha=0.6, color='blue', label='delta - uncorrected_dop')
            # plt.title('Raw Uncorrected Dop Histogram', fontsize=config.fontsize)
            # plt.legend()
            # plt.xlim([-v_fold, v_fold])

            # plt.subplot(config.num_rows, config.num_cols, 4)
            # # n, bins, patches = plt.hist(vy_delta, bins=150, density=True, alpha=0.75, color='green', label='delta')
            # #         n, bins, patches = plt.hist(vy_4dfft_unc, bins=150, density=True, alpha=0.75, color='green',label='4dfft')
            # # n, bins, patches = plt.hist(vy_4dfft, bins=150, density=True, alpha=0.75, color='white', label='4dfft corrected')
            # n, bins, patches = plt.hist(vy_4dfft_robust_dop, bins=150, density=True, alpha=0.6, color='red', label='4dfft robust_dop')
            # n, bins, patches = plt.hist(vy_delta_robust_dop, bins=150, density=True, alpha=0.6, color='blue', label='delta robust_dop')

            #             plt.grid(True)
            # plt.title('Vy Histogram', fontsize=config.fontsize)
            # plt.legend()
            # plt.xlim([-v_fold, v_fold])

            # Save figure
            plt.tight_layout(pad=3)
            plt.savefig(config.save_path + str(idx) + '_' + str(config.df.loc[idx, "timestampRadar"]))
            # plt.show()
            # plt.close()
            if config.debug_mode and idx == config.start_idx:
                break
        except Exception as e:
            PrintException()
            print('bad frame:', config.df.loc[idx, "timestampRadar"], e)


    if 1:
        fourcc = cv2.VideoWriter_fourcc(*'mp4v')

        video_name = config.save_path + "/../" +  config.dataset + '.mp4'

        first = True
        for idx in tqdm(range(config.start_idx, config.end_idx)):  # len(config.df))):
            if first:
                frame = cv2.imread(config.save_path + str(idx) + '_' + str(config.df.loc[idx, "timestampRadar"]) + '.png')
                if frame is None:
                    continue
                height, width, layers = frame.shape
                video = cv2.VideoWriter(video_name, fourcc, 6, (width, height))
                first = False

            img_path = config.save_path + str(idx) + '_' + str(config.df.loc[idx, "timestampRadar"]) + '.png'

            if path.exists(img_path):
                video.write(cv2.imread(img_path))

        cv2.destroyAllWindows()
        video.release()