import numpy as np
import pandas as pd
from matplotlib import gridspec
from tqdm import tqdm
import matplotlib.pyplot as plt
import os
import time
import matplotlib
from DataPipeline.src.GetEngines import get_engines
from Plotting.wise_plotter import WisePlotter

plt.style.use('dark_background')
plt_font = 22
font = {'size': plt_font}
matplotlib.rc('font', **font)


def mapper_fidelity(engine):
    plotter = WisePlotter(engine=engine)
    for idx in tqdm(range(len(engine))):
        plt.close()
        fig = plt.figure(figsize=(20, 14))
        gs = gridspec.GridSpec(ncols=3, nrows=2)

        # RGB
        ax = fig.add_subplot(gs[0, 0])
        plotter.plot_rgb(ax=ax, engine_idx=idx)

        # Delta
        ax = fig.add_subplot(gs[:, 1])
        plotter.plot_delta(ax=ax, engine_idx=idx)

        # Mapper
        plt.show()

def main():
    engines = get_engines(wf="92", val_train="all", randomize=False)
    for engine in engines:
        mapper_fidelity(engine)
        exit()


if __name__ == '__main__':
    main()
