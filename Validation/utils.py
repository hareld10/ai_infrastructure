import pandas as pd
import cv2
import os
import json
import numpy as np
import matplotlib.pyplot as plt
from IPython.core.display import display, HTML
import torch
import warnings
from tqdm import tqdm_notebook as tqdm
import os.path
from os import path
import matplotlib.lines as lines


def db_np(array):
    return 10 * np.log10(array)


def db_torch(array):
    return 10 * torch.log10(array)


def read_label(path):
    with open(path, "r") as fp:
        frame_data = json.load(fp)
    return frame_data


def to_rng_az_el(x, y, z, angle_type):
    """Cartesian (x,y,z) to spherical (range azimuth elevation) coordinates
    """
    r = np.sqrt(x ** 2 + y ** 2 + z ** 2)

    # Following Kuti's definition
    az = np.arcsin(x / r)  # az
    el = np.arcsin(z / r)  # phi

    if angle_type == 'deg':
        az = np.rad2deg(az)
        el = np.rad2deg(el)

    return (r, az, el)


def filter_fov_az(az, pts):
    if az == 0:
        return pts
    pts = pts[pts[:, 1] > 0]  # pts in front of the vehicle
    r, Az, El = to_rng_az_el(x=pts[:, 1], y=pts[:, 0], z=pts[:, 2], angle_type='deg')
    mask = np.logical_not(np.logical_and(Az < (az // 2), (-az // 2) < Az))
    return pts[mask, :]


def filter_fov_el(el, pts):
    if el == 0:
        return pts
    pts = pts[pts[:, 1] > 0]  # pts in front of the vehicle
    r, Az, El = to_rng_az_el(x=pts[:, 1], y=pts[:, 0], z=pts[:, 2], angle_type='deg')
    mask = np.logical_not(np.logical_and(El < (el // 2), (-el // 2) < El))
    return pts[mask, :]


def filter_fov(fov_az, fov_el, pts):
    if fov_az == 0 and fov_el == 0:
        return pts

    # Pts in front of the vehicle
    pts = pts[pts[:, 1] > 0]

    # Transform to range-azimuth-elevation
    r, Az, El = to_rng_az_el(x=pts[:, 1], y=pts[:, 0], z=pts[:, 2], angle_type='deg')

    # Filter points
    az_filter = np.logical_not(np.logical_and(Az < (fov_az // 2), (-fov_az // 2) < Az))
    el_fiter = np.logical_and(El < (fov_el // 2), (-fov_el // 2) < El)
    fov_filter = np.logical_and(az_filter, el_fiter)

    return pts[fov_filter, :]


def filter_range(min_range, max_range, pts, index=3):
    x = pts[np.linalg.norm(pts[:, :index], axis=-1) < max_range]
    x = x[np.linalg.norm(x[:, :index], axis=-1) > min_range]
    return x


def lidar_to_radar_coordinates(points):
    theta = np.deg2rad(90)
    rotation_matrix = np.array([[np.cos(theta), -np.sin(theta), 0],
                                [np.sin(theta), np.cos(theta), 0],
                                [0, 0, 1]])

    return np.matmul(rotation_matrix, points.T).T


def apply_6DOF(pts, yaw, pitch, roll, tx, ty, tz):
    rot = get_rotation_matrix(theta_z=np.deg2rad(yaw),
                              theta_x=np.deg2rad(pitch),
                              theta_y=np.deg2rad(roll))
    # print("rot", rot.shape)
    camera_lidar_mat = np.zeros((3, 4), dtype=np.float32)
    camera_lidar_mat[:, :3] = rot
    camera_lidar_mat[:, 3] = np.array([tx, ty, tz])

    pts_velo_lidar = np.vstack((pts.T, np.ones((1, pts.shape[0]))))
    return (camera_lidar_mat @ pts_velo_lidar).T


def get_rotation_matrix(theta_z, theta_x, theta_y):
    z_theta = np.array([[np.cos(theta_z), -np.sin(theta_z), 0],
                        [np.sin(theta_z), np.cos(theta_z), 0],
                        [0, 0, 1]])

    x_theta = np.array([[1, 0, 0],
                        [0, np.cos(theta_x), -np.sin(theta_x)],
                        [0, np.sin(theta_x), np.cos(theta_x)]])

    y_theta = np.array([[np.cos(theta_y), 0, np.sin(theta_y)],
                        [0, 1, 0],
                        [-np.sin(theta_y), 0, np.cos(theta_y)]])

    rotation_matrix = z_theta @ x_theta @ y_theta
    return rotation_matrix


def project_lidar_2_radar(config, pts):
    # Project points
    pts[:, :3] = lidar_to_radar_coordinates(pts[:, :3])

    # Calibrate points
    pts[:, :3] = apply_6DOF(pts[:, :3],
                            yaw=config.spatial_calibration_params["yaw-radar-lidar"],
                            roll=config.spatial_calibration_params["roll-radar-lidar"],
                            pitch=config.spatial_calibration_params["pitch-radar-lidar"],
                            tx=config.spatial_calibration_params["tx-radar-lidar"],
                            ty=config.spatial_calibration_params["ty-radar-lidar"],
                            tz=config.spatial_calibration_params["tz-radar-lidar"])

    # Filter points
    #     pts = filter_fov_az(config.radar_fov_az, pts) # filter by az fov
    #     pts = filter_fov_el(config.radar_fov_el, pts) # filter by az fov
    pts = filter_fov(config.radar_fov_az, config.radar_fov_el, pts)  # filter by az fov
    pts = filter_range(config.min_range, config.max_range, pts)  # filter by range

    return pts


# def project_lidar_2_radar_params_old(pts, spatial_calibration_params, radar_fov_az, radar_fov_el, min_range, max_range):
#     # Project points
#     pts[:, :3] = lidar_to_radar_coordinates(pts[:, :3])
#
#     # Calibrate points
#     pts[:, :3] = apply_6DOF(pts[:, :3],
#                             yaw=spatial_calibration_params["yaw-radar-lidar"],
#                             roll=spatial_calibration_params["roll-radar-lidar"],
#                             pitch=spatial_calibration_params["pitch-radar-lidar"],
#                             tx=spatial_calibration_params["tx-radar-lidar"],
#                             ty=spatial_calibration_params["ty-radar-lidar"],
#                             tz=spatial_calibration_params["tz-radar-lidar"])
#
#     # Filter points
#     #     pts = filter_fov_az(config.radar_fov_az, pts) # filter by az fov
#     #     pts = filter_fov_el(config.radar_fov_el, pts) # filter by az fov
#     pts = filter_fov(radar_fov_az, radar_fov_el, pts)  # filter by az fov
#     pts = filter_range(min_range, max_range, pts)  # filter by range
#
#     return pts

def project_lidar_2_radar_params(pts, spatial_calibration_params,
                                 radar_fov_az, radar_fov_el, min_range, max_range,
                                 semantic=None,
                                 filter_fov_flag=True):
    pts_velo = np.zeros((pts.shape[0], pts.shape[1] + 4), dtype=np.float32)
    # print(pts_velo.shape)
    pts_velo[:, :pts.shape[1]] = pts.copy()
    pts_velo[:, 3:6] = pts[:, :3].copy()

    if semantic is not None:
        pts_velo[:, 6] = semantic

    # Project points
    pts_velo[:, :3] = lidar_to_radar_coordinates(pts_velo[:, :3])

    # Calibrate points
    pts_velo[:, :3] = apply_6DOF(pts_velo[:, :3],
                                 yaw=spatial_calibration_params["yaw-radar-lidar"],
                                 roll=spatial_calibration_params["roll-radar-lidar"],
                                 pitch=spatial_calibration_params["pitch-radar-lidar"],
                                 tx=spatial_calibration_params["tx-radar-lidar"],
                                 ty=spatial_calibration_params["ty-radar-lidar"],
                                 tz=spatial_calibration_params["tz-radar-lidar"])

    if filter_fov_flag:
        pts_velo = filter_fov(radar_fov_az, radar_fov_el, pts_velo)  # filter by az fov
        pts_velo = filter_range(min_range, max_range, pts_velo)  # filter by range

    return pts_velo


def plot_bb_bev(config, ax, lidar_3dvd_label_proj):
    for bb in lidar_3dvd_label_proj:

        cx, cy = bb[:2]
        p1, p2, p3, p4, heading_1, heading_2 = get_bb_bev(config, bb)

        #         cx, cy = bb[:2]
        #         dx, dy = bb[3:5]
        #         heading = bb[6] + np.deg2rad(config.spatial_calibration_params["yaw-radar-lidar"])
        #         c = np.cos(heading)
        #         s = np.sin(heading)
        #         rot_mat = [[c, -s], [s, c]]

        #         center = [cx, cy]
        #         p1 = center + np.dot(rot_mat, [ dy / 2,  dx / 2])
        #         p2 = center + np.dot(rot_mat, [ dy / 2, -dx / 2])
        #         p3 = center + np.dot(rot_mat, [-dy / 2, -dx / 2])
        #         p4 = center + np.dot(rot_mat, [-dy / 2,  dx / 2])

        #         heading1 = center + np.dot(rot_mat, [dy/2,  dx/2])
        #         heading2 = center + np.dot(rot_mat, [-dy/2,  dx/2])

        pts = [p1, p2, p3, p4]

        pairs = [[0, 1], [1, 2], [2, 3], [3, 0]]
        for ind in pairs:
            plt.plot([pts[ind[0]][0], pts[ind[1]][0]], [pts[ind[0]][1], pts[ind[1]][1]], c='w')

        # Plot heading
        l1 = lines.Line2D((cx, p1[0]), (cy, p1[1]), color='w', linewidth=2)
        ax.add_line(l1)
        l1 = lines.Line2D((cx, p4[0]), (cy, p4[1]), color='w', linewidth=2)
        ax.add_line(l1)

        # Plot score
        ax.annotate(str(np.round(bb[8], 2)), (np.max([p1[0], p2[0], p3[0], p4[0]]), np.max([p1[1], p2[1], p3[1], p4[1]])), color='w', fontsize=12)


#         plt.text(np.max([p1[0],p2[0],p3[0],p4[0]]) , np.min([p1[1],p2[1],p3[1],p4[1]]) ,str(np.round(bb[8],2)),'w',fontsize=10)

def get_bb_bev(config, bb):
    cx, cy = bb[:2]
    dx, dy = bb[3:5]
    heading = bb[6] + np.deg2rad(config.spatial_calibration_params["yaw-radar-lidar"])
    c = np.cos(heading)
    s = np.sin(heading)
    rot_mat = [[c, -s], [s, c]]

    center = [cx, cy]
    p1 = center + np.dot(rot_mat, [dy / 2, dx / 2])
    p2 = center + np.dot(rot_mat, [dy / 2, -dx / 2])
    p3 = center + np.dot(rot_mat, [-dy / 2, -dx / 2])
    p4 = center + np.dot(rot_mat, [-dy / 2, dx / 2])

    heading_1 = center + np.dot(rot_mat, [dy / 2, dx / 2])
    heading_2 = center + np.dot(rot_mat, [-dy / 2, dx / 2])

    return p1, p2, p3, p4, heading_1, heading_2


def plot_bb_rng_az(config, meta_data_4dfft, ax, lidar_3dvd_label_proj, plot_shape):
    #     print('plot_shape',plot_shape)#  (250,500)

    for bb in lidar_3dvd_label_proj:

        cx, cy, cz = bb[:3]
        center_rng_az_el = to_rng_az_el(cx, cy, cz, 'rad')

        # Get bb range-az coordinates
        p1_rng_az, p2_rng_az, p3_rng_az, p4_rng_az = get_bb_rng_az(config, bb)

        # Get range-az indices
        p1_rng_az_indices = get_rng_az_indices(config, meta_data_4dfft, p1_rng_az)
        p2_rng_az_indices = get_rng_az_indices(config, meta_data_4dfft, p2_rng_az)
        p3_rng_az_indices = get_rng_az_indices(config, meta_data_4dfft, p3_rng_az)
        p4_rng_az_indices = get_rng_az_indices(config, meta_data_4dfft, p4_rng_az)
        center_rng_az_el_indices = get_rng_az_indices(config, meta_data_4dfft, center_rng_az_el)

        # Adjust indices based on plot_shape (in case there was a resize)
        p1_rng_az_indices = resize_rng_az_indices(meta_data_4dfft, plot_shape, p1_rng_az_indices)
        p2_rng_az_indices = resize_rng_az_indices(meta_data_4dfft, plot_shape, p2_rng_az_indices)
        p3_rng_az_indices = resize_rng_az_indices(meta_data_4dfft, plot_shape, p3_rng_az_indices)
        p4_rng_az_indices = resize_rng_az_indices(meta_data_4dfft, plot_shape, p4_rng_az_indices)
        center_rng_az_el_indices = resize_rng_az_indices(meta_data_4dfft, plot_shape, center_rng_az_el_indices)

        pts = [p1_rng_az_indices, p2_rng_az_indices, p3_rng_az_indices, p4_rng_az_indices]

        pairs = [[0, 1], [1, 2], [2, 3], [3, 0]]
        for ind in pairs:
            plt.plot([pts[ind[0]][0], pts[ind[1]][0]], [pts[ind[0]][1], pts[ind[1]][1]], c='w', )

        # Plot heading
        l1 = lines.Line2D((center_rng_az_el_indices[0], p1_rng_az_indices[0]), (center_rng_az_el_indices[1], p1_rng_az_indices[1]), color='w', linewidth=2)
        ax.add_line(l1)
        l1 = lines.Line2D((center_rng_az_el_indices[0], p4_rng_az_indices[0]), (center_rng_az_el_indices[1], p4_rng_az_indices[1]), color='w', linewidth=2)
        ax.add_line(l1)

        # Plot score
        ax.annotate(str(np.round(bb[8], 2)), (np.max([p1_rng_az_indices[0], p2_rng_az_indices[0], p3_rng_az_indices[0], p4_rng_az_indices[0]]),
                                              np.max([p1_rng_az_indices[1], p2_rng_az_indices[1], p3_rng_az_indices[1], p4_rng_az_indices[1]])), color='w', fontsize=10)


#         plt.text(np.max([p1[0],p2[0],p3[0],p4[0]]) , np.min([p1[1],p2[1],p3[1],p4[1]]) ,str(np.round(bb[8],2)),'w',fontsize=10)

def resize_rng_az_indices(meta_data, plot_shape, pts_indices):
    #     print('rng', pts_indices[0],plot_shape[1],meta_data['axis_params'][0][2])
    #     print('az', pts_indices[1],plot_shape[0],meta_data['axis_params'][1][2])

    # Range
    rng_indices_resize = np.round(pts_indices[0] * plot_shape[1] / meta_data['axis_params'][0][2]).astype(np.int)

    # Azimuth
    az_indices_resize = np.round(pts_indices[1] * plot_shape[0] / meta_data['axis_params'][1][2]).astype(np.int)

    # Elevation
    el_indices_resize = pts_indices[2]

    return (rng_indices_resize, az_indices_resize, el_indices_resize)


def get_bb_rng_az(config, bb):
    cx, cy, cz = bb[:3]
    dx, dy, dz = bb[3:6]
    heading = bb[6] + np.deg2rad(config.spatial_calibration_params["yaw-radar-lidar"])
    c = np.cos(heading)
    s = np.sin(heading)
    rot_mat = [[c, -s, 0], [s, c, 0], [0, 0, 1]]

    center = [cx, cy, cz]
    p1 = center + np.dot(rot_mat, [dy / 2, dx / 2, dz / 2])
    p2 = center + np.dot(rot_mat, [dy / 2, -dx / 2, dz / 2])
    p3 = center + np.dot(rot_mat, [-dy / 2, -dx / 2, dz / 2])
    p4 = center + np.dot(rot_mat, [-dy / 2, dx / 2, dz / 2])

    p1_rng_az = to_rng_az_el(p1[0], p1[1], p1[2], 'rad')
    p2_rng_az = to_rng_az_el(p2[0], p2[1], p2[2], 'rad')
    p3_rng_az = to_rng_az_el(p3[0], p3[1], p3[2], 'rad')
    p4_rng_az = to_rng_az_el(p4[0], p4[1], p4[2], 'rad')

    return p1_rng_az, p2_rng_az, p3_rng_az, p4_rng_az


def get_rng_az_indices(config, meta_data, pts_rng_az):
    # Get range index
    rng_indices = np.round(((pts_rng_az[0] - meta_data['axis_params'][0][0]) / meta_data['range_bin'])).astype(np.int)

    # Get azimuth index
    az_indices = np.round(((pts_rng_az[1] - meta_data['axis_params'][1][0]) / meta_data['az_bin'])).astype(np.int)

    #     print("pts_rng_az[1]",pts_rng_az[1])
    #     print("meta_data['axis_params'][1][0]",meta_data['axis_params'][1][0])
    #     print("meta_data['az_bin']",meta_data['az_bin'])
    #     print('az_indices',az_indices)

    # Get elevation index
    el_indices = np.round(((pts_rng_az[2] - meta_data['axis_params'][2][0]) / meta_data['el_bin'])).astype(np.int)

    return (rng_indices, az_indices, el_indices)

# def get_corrected_velocity(config,idx,meta_data_delta):

#     # Record timestamp and velocity for corrected velocity
#     idy = len(config.velocity_tracker)
#     plat_vel_key = "robust_platform_velocity"
#     config.velocity_tracker.loc[idy,'timestamp'] = config.df.loc[idx, "timestampRadar"]*1e-9 # convert to timestamp in [s]
#     config.velocity_tracker.loc[idy,'vx_delta_cuda'] = meta_data_delta['platform_velocity'][0]
#     config.velocity_tracker.loc[idy,'vy_delta_cuda'] = meta_data_delta['platform_velocity'][1]
#     config.velocity_tracker.loc[idy,'vz_delta_cuda'] = meta_data_delta['platform_velocity'][2]
#     config.velocity_tracker.loc[idy,'v_delta_cuda']  = np.linalg.norm(meta_data_delta['platform_velocity'])
#     config.velocity_tracker.loc[idy,'vx_delta'] = meta_data_delta['platform_velocity'][0]
#     config.velocity_tracker.loc[idy,'vy_delta'] = meta_data_delta['platform_velocity'][1]
#     config.velocity_tracker.loc[idy,'vz_delta'] = meta_data_delta['platform_velocity'][2]
#     config.velocity_tracker.loc[idy,'v_delta']  = np.linalg.norm(meta_data_delta['platform_velocity'])


#     # Get corrected velocity
#     if idy>3:

#         # Get current velocity vec
#         current_vel     = np.array([config.velocity_tracker.loc[idy,'vx_delta_cuda'], config.velocity_tracker.loc[idy,'vy_delta_cuda'], config.velocity_tracker.loc[idy,'vz_delta_cuda']])

#         # Get previous velocity vec
#         prev_vel      = np.array([config.velocity_tracker.loc[idy-1,'vx_delta'], config.velocity_tracker.loc[idy-1,'vy_delta'], config.velocity_tracker.loc[idy-1,'vz_delta']])
#         prev_prev_vel = np.array([config.velocity_tracker.loc[idy-2,'vx_delta'], config.velocity_tracker.loc[idy-2,'vy_delta'], config.velocity_tracker.loc[idy-2,'vz_delta']])

#         # Get estimated velocity vec from kinematics
#         accelaration = (prev_vel - prev_prev_vel) / (config.velocity_tracker.loc[idy-1,'timestamp'] - config.velocity_tracker.loc[idy-2,'timestamp'])
#         estimated_vel = prev_vel + accelaration * (config.velocity_tracker.loc[idy,'timestamp'] - config.velocity_tracker.loc[idy-1,'timestamp'])

#         # Check if Nan
#         if np.isnan(np.linalg.norm(current_vel)):

#             config.velocity_tracker.loc[idy,'vx_delta'] = estimated_vel[0]
#             config.velocity_tracker.loc[idy,'vy_delta'] = estimated_vel[1]
#             config.velocity_tracker.loc[idy,'vz_delta'] = estimated_vel[2]
#             config.velocity_tracker.loc[idy,'v_delta']  = np.linalg.norm(estimated_vel)

#         # Check if velocity jump
#         elif np.abs(np.linalg.norm(estimated_vel) - np.linalg.norm(current_vel)) > config.vel_est_threshold:


#             # Get future velocity vec
#             future_vel = np.nan
#             idz = idx
#             while np.isnan(np.linalg.norm(future_vel)):
#                 idz += 1
#                 with open(str(config.df.loc[idz, 'base_path']) + "/metadata/metadata_cuda_delta_" + str(config.df.loc[idz, "timestampRadar"]) + ".json") as f:
#                     meta_data_delta_future = json.load(f)

#                 future_vel = meta_data_delta_future['platform_velocity']
#                 future_timestamp = str(config.df.loc[idz, "timestampRadar"])

#             # Get velocity estimation from future velocity
#             backward_estimated_velocity = (prev_vel + future_vel) / 2

# #             print('frame:',idx, 'future_vel',np.linalg.norm(future_vel),'current_vel',np.linalg.norm(current_vel),'prev_vel',np.linalg.norm(prev_vel),'backward_estimated_velocity',np.linalg.norm(backward_estimated_velocity))

#             config.velocity_tracker.loc[idy,'vx_delta'] = backward_estimated_velocity[0]
#             config.velocity_tracker.loc[idy,'vy_delta'] = backward_estimated_velocity[1]
#             config.velocity_tracker.loc[idy,'vz_delta'] = backward_estimated_velocity[2]
#             config.velocity_tracker.loc[idy,'v_delta']  = np.linalg.norm(backward_estimated_velocity)


#     return config


# def velocity_compensation(config, v_fold, pts):

#     # Projection operator
#     Ai = np.array([pts['u_sin'], np.sqrt(1-pts['u_sin']**2 - pts['v_sin']**2), pts['v_sin']]).T

#     # Platform velocity in Line-of-Sight (los) direction
#     idy = len(config.velocity_tracker)-1
#     platform_velocity = np.array([config.velocity_tracker.loc[idy,'vx_delta'], config.velocity_tracker.loc[idy,'vy_delta'], config.velocity_tracker.loc[idy,'vz_delta']])
#     platform_vel_LOS_ms = Ai @ platform_velocity

#     # Correct velocity
#     pts['dop'] = pts['uncorrected_dop'] + platform_vel_LOS_ms

#     # Fold to [-vfold/2,vfold/2]
#     pts['dop'] = np.mod(pts['dop'] + v_fold/2, v_fold) - v_fold/2


#     return pts
