from copy import deepcopy
from glob import glob
from pathlib import Path
from pprint import pprint

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from Evaluation.evaluation_utils import calculate_df
from Evaluation.names import *
import os
from tqdm import tqdm
import matplotlib
from scipy.integrate import simps


class Plotter:
    def __init__(self, root="/workspace/HDD/EvaluationResults/"):
        self.root = root

    def get_date(self, path):
        words = path.split("/")
        idx = words.index("EvaluationResults")
        date = int(words[idx + 1])
        return date

    def get_name(self, path):
        words = path.split("/")
        idx = words.index("EvaluationResults")
        return f'{words[idx + 3]}_{words[idx + 4]}'

    def get_classes(self, dfs):
        return dfs[list(dfs.keys())[0]].columns

    def process_paths(self, paths, instances=False):
        dfs = {}
        threshes = []
        for x in paths:
            df = pd.read_csv(x, index_col=0)
            dfs[self.get_name(x)] = calculate_df(df) if not instances else df
            threshes.append(os.path.split(x)[0].split("_")[-1])
        return dfs, threshes

    def get_dfs(self, task, min_date=210529, max_date=999999, exp_name="", instances=False, defined_paths=None, fixed_path=None):
        csv_name = "*confusion.csv" if not instances else "*df_instances.csv"

        paths_to_process = []
        if fixed_path is not None:
            paths_to_process = glob(f"{fixed_path}/*/{csv_name}")
            # print(paths_to_process)
            # print(len(paths_to_process))
        else:
            dirs = [f"{self.root}/{x}/{task}" for x in os.listdir(f"{self.root}") if (min_date <= int(x) <= max_date)]
            # print("dirs", dirs)
            paths = []
            for cur_root in dirs:
                paths += [str(x.absolute()) for x in sorted(Path(cur_root).rglob(f"{csv_name}"))]
            for x in paths:
                if not (f"{task}/{exp_name}" in x):  # and self.get_date(x) >= min_date):
                    continue
                if defined_paths is not None:
                    found = False
                    for d_p in defined_paths:
                        if x.startswith(d_p):
                            found = True
                            break
                    if found:
                        paths_to_process.append(x)
                paths_to_process.append(x)

        return self.process_paths(paths_to_process, instances=instances)

    def save(self, save_path):
        if save_path is None:
            plt.show()
        else:
            print("saved", save_path)
            plt.savefig(save_path)
            plt.close()

    def draw_key(self, dfs, key, save_path=None, ax=None, save=True):
        fontsize = 22
        d = {k: v.loc[key, :] for k, v in dfs.items()}
        dfs = pd.DataFrame(d, index=d[list(d.keys())[0]].index)
        if ax is None:
            plt.figure(figsize=(15, 10))
            ax = plt.gca()
        dfs.plot(kind="bar", rot=45, ax=ax, fontsize=fontsize)
        ax.set_title(key, fontsize=fontsize)
        plt.legend(loc='center left', bbox_to_anchor=(1.0, 0.5), prop={'size': fontsize})
        if save:
            plt.tight_layout(pad=2)
            self.save(save_path)
        return

    def draw_class_on_ax(self, c, dfs, ax, label="",
                         x_key=RECALL, y_key=PRECISION,
                         title="PR-Curve | ",
                         **kwargs):
        fontsize = 22
        keys = [y_key, x_key]
        df = pd.DataFrame()
        # print("#" * 20)

        # for index, (k, v) in enumerate(dfs.items()):
        for index, (k, v) in enumerate(sorted(dfs.items(), key=lambda x: x[0].split("_")[-1])):
            for key in keys:
                if type(key) == list:
                    continue
                df.loc[k, key] = v.loc[key, c]
        if type(x_key) == list:
            df[THRESHES] = x_key
            x_key = THRESHES
        df = df.fillna(1)
        df.loc[df[RECALL]==0, PRECISION] = 1
        df = df.sort_values(RECALL)
        ap = np.round(np.trapz(y=df[PRECISION], x=df[RECALL]), 3)
        df.plot(x=x_key, y=y_key, ax=ax, fontsize=fontsize, legend=False, label=f"{label}, ap={ap}")
        if c == "vehicle":
            c = "car"

        ax.set_title(f"{title} cls=%s" % c, fontsize=fontsize)
        ax.set_xlabel(x_key, fontsize=fontsize)
        ax.set_ylabel(y_key, fontsize=fontsize)
        ax.set_ylim(0, 1)
        pass

    def plot_pr_curve(self, dfs, ax=None, save_path=None, save=True, label=""):
        # plt.rcParams.update({'font.size': fontsize})
        for c in self.get_classes(dfs):
            if ax is None:
                plt.figure(figsize=(10, 7))
                ax = plt.gca()
            self.draw_class_on_ax(c=c, ax=ax, dfs=dfs, label=label)
            if ax is None:
                plt.tight_layout()
            if save:
                self.save(save_path + "_" + c + ".jpg")
        return

    def analyze_grid_search(self, grid_search, cls, task="od3d", ious=["iou_0.25"],
                            mindate=210613, title="", name="", get_df=False):
        # get models names
        # grid_search = glob("//home/amper/AI/Harel/AI_Infrastructure/Evaluation/EvaluationResults/210613/od3d/ensemble_*_*_*_*")

        print("len(grid_search)", len(grid_search))
        ret_dict = {}

        models = [os.path.split(x)[-1] for x in grid_search]
        data, threshes = get_data(self, models, ious=ious,
                                  task=task, mindate=mindate, defined_paths=grid_search)

        ret_dict["data"] = deepcopy(data)
        ret_dict["threshes"] = deepcopy(threshes)

        print("found", len(data))

        # Filter Classes
        classes = [cls]
        for metric_key, evaluation_res in data.items():
            data[metric_key] = {k: evaluation_res[k][classes] for k in evaluation_res}

        res = pd.DataFrame()
        for label, dfs in data.items():
            df = pd.DataFrame()
            for idx, (k, v) in enumerate(dfs.items()):
                df.loc[idx, RECALL] = v.loc[RECALL][0]
                df.loc[idx, PRECISION] = v.loc[PRECISION][0]
            # df = df.drop_duplicates([PRECISION, RECALL]).reset_index(drop=True)
            df.loc[df[RECALL] == 0, PRECISION] = 1
            df = df.dropna(subset=[PRECISION, RECALL]).reset_index(drop=True)
            df["mult"] = df[RECALL] * df[PRECISION]
            df = df.sort_values(RECALL)
            # print(df["mult"])
            # print(label, "\n", df)
            # if label in ["ensemble_2d_pedestrian_0.1_0.5_0.74_0.77_Waymo_Train/iou_0.25", "ensemble_2d_pedestrian_0.3_0.35_0.0_0.8_Waymo_Train/iou_0.25"]:
            #     print(label)
            #     print(df)

            res.loc[label, "ap"] = np.trapz(y=df[PRECISION], x=df[RECALL])
            # res.loc[label, "ap"] = df[RECALL].max()
            res.loc[label, "ap_p_r"] = df["mult"].mean()
            # res.loc[label, "ap"] = df["mult"].sort_values()[-4:].mean()
            # res.loc[label, "ap"] = df["mult"][-1:].mean()

        ret_dict["res"] = deepcopy(res)

        if get_df:
            return res

        print("Best model")
        print(res["ap"].idxmax(), res["ap"].max())

        res = res.sort_values(["ap"])
        res["index"] = range(len(res))

        plt.style.use('dark_background')
        plt.figure(figsize=(8, 8))

        font = {'family': 'normal',
                'weight': 'bold',
                'size': 20}

        matplotlib.rc('font', **font)

        # res["ap"].plot(kind="hist", title="avg(precision*recall)", bins=30)
        ax = res.plot(x="index", ax=plt.gca())
        ax.set_xticklabels([])
        plt.xlabel("avg. precision")
        plt.title(title)
        plt.tight_layout(pad=1)
        plt.savefig(f"./figures/ap_scores_{name}.png")
        plt.close()

        res = res.sort_values("ap", ascending=False)
        res.to_csv(f"./figures/ap_scores_{name}_df.csv")
        print(res)
        print("plotted")
        return ret_dict


def plot(data, p, ax=None, classes=["vehicle", "large_vehicle"], legend=True, title="pr_curve", **kwargs):
    for c in classes:
        if ax is None:
            plt.figure(figsize=(10, 7))
            ax = plt.gca()
        for label, dfs in data.items():
            p.draw_class_on_ax(c=c, dfs=dfs, ax=ax, label=label, title=title, **kwargs)
        if legend:
            ax.legend()
        if ax is None:
            save_path = f"./figures/{title.lower()}_{c}.jpg"
            plt.savefig(save_path)


def data_to_range(data, rng_bin=5):
    ret_data = {}
    rngs = list(range(0, 80, rng_bin))
    for exp in data.keys():
        ret_data[exp] = {}
        for min_r in rngs:
            exp_df = pd.DataFrame()
            vals = []
            for df in data[exp].keys():
                # data
                dd = data[exp][df]
                slices_df = (dd[(data[exp][df]["range"] >= min_r) & (data[exp][df]["range"] <= min_r + rng_bin)]).copy().reset_index(
                    drop=True)

                tp = len(slices_df[(slices_df.cat == "vehicle") & (slices_df.confusion_matrix == "tp")])
                fp = len(slices_df[(slices_df.cat == "vehicle") & (slices_df.confusion_matrix == "fp")])
                fn = len(slices_df[(slices_df.cat == "vehicle") & (slices_df.confusion_matrix == "fn")])

                precision = tp / (tp + fp + 1e-4)
                recall = tp / (tp + fn + 1e-4)
                vals.append(precision * recall)
            vals = np.asarray(vals)
            exp_df.loc["ap", "vehicle"] = vals.mean()
            ret_data[exp][min_r] = exp_df
    return ret_data, rngs


def get_data(p, models, task="od3d", mindate=210613, max_date=999999, ious=["iou_0.25"], instances=False, defined_paths=None):
    data = {}
    threshes = []
    if defined_paths is None:
        for model in tqdm(models):
            for iou in ious:
                look_for = f"{model}/{iou}"
                data[look_for], threshes = p.get_dfs(task=task, min_date=mindate, max_date=max_date,
                                                     exp_name=look_for, instances=instances, defined_paths=defined_paths)
    else:
        for path in tqdm(defined_paths):
            model_name = path.split("/od/")[-1]
            # print(f"get_data: model_name = {model_name}")
            data[f"{model_name}/{ious[0]}"], threshes = p.get_dfs(task=task, min_date=mindate,
                                                                  exp_name="", instances=instances,
                                                                  defined_paths=defined_paths,
                                                                  fixed_path=path)
    to_pop = []
    for k, v in data.items():
        if not v:
            print(k, "is empty")
            to_pop.append(k)
    [data.pop(k) for k in to_pop]
    return data, threshes


def main():
    p = Plotter()
    # ious = ["bev0.25"]
    # models = ["3DSSD_uniform_0.15_Audi_Val", "CameraLidarFusion_v2_Audi_Val", "3DSSD_uniform_z_0.15_Audi_Val",
    #           "3DSSD_baseline_Audi_Val",
    #           "ensemble_0.4_0.7_0.4_0.25_3DSSD_uniform_0.15_CameraLidarFusion_v2_Audi_Val"]
    # legend = True

    grid_search = glob("//home/amper/AI/Harel/AI_Infrastructure/Evaluation/EvaluationResults/210613/od3d/ensemble_*_*_*_*")
    models = [os.path.split(x)[-1] for x in grid_search]
    legend = False

    data, threshes = get_data(p, models)

    classes = ["vehicle", "large_vehicle"]
    for metric_key, evaluation_res in data.items():
        data[metric_key] = {k: evaluation_res[k][classes] for k in evaluation_res}

    # pr curve
    plot(data=data, p=p, classes=classes)
    plot(data=data, p=p, classes=classes, title=IOU, y_key=IOU, x_key=threshes)
    plot(data=data, p=p, classes=classes, title=F1_SCORE, y_key=F1_SCORE, x_key=threshes)


if __name__ == '__main__':
    main()

# ensemble 2d:
if 0:
    p = Plotter()
#     dates = ["210712"] # car maybe add Train in gglob
    dates = ["210822"] # pedestrain
    grid_search = []
    # for date in dates:
    #     grid_search += glob(f"/workspace/HDD/EvaluationResults/{date}/od/ensemble_2d_ped*_*_*_*_*Waymo_Train", recursive=True)
    # grid_search += ["/workspace/HDD/EvaluationResults/210822/od/ensemble_2d_pedestrian_0.1_0.5_0.74_0.77_Waymo_Train"]
    # grid_search += ["/workspace/HDD/EvaluationResults/210822/od/ensemble_2d_pedestrian_0.3_0.35_0.0_0.8_Waymo_Train"]
    grid_search += ["/workspace/HDD/EvaluationResults/210822/od/ensemble_2d_pedestrian_0.66_0.75_1.0_0.5_Waymo_Train"]
    grid_search += ["/workspace/HDD/EvaluationResults/210822/od/ensemble_2d_pedestrian_0.5_0.0_1.0_0.5_Waymo_Train"]
    # grid_search += ["/workspace/HDD/EvaluationResults/210822/od/YolorR-p6_Waymo_Train"]
    print("len grid", len(grid_search))
    print(grid_search[0])
    x = grid_search[0]
#     grid_search = np.random.permutation(grid_search)[:100]
    df = p.analyze_grid_search(grid_search, task="od", cls="pedestrian", title="Late Fusion 2D | Grid-search", name="2d_ensemble", mindate=210628)

# clf grid-search
if 1:
    p = Plotter()
    grid_search = []
    # for date in dates:
    #     grid_search += glob(f"/workspace/HDD/EvaluationResults/{date}/od3d/CameraLidarFusion_*_*_*")
    # grid_search += ["/workspace/HDD/EvaluationResults/210822/od/ensemble_2d_pedestrian_0.1_0.5_0.74_0.77_Waymo_Train"]
    # grid_search += ["/workspace/HDD/EvaluationResults/210822/od/ensemble_2d_pedestrian_0.3_0.35_0.0_0.8_Waymo_Train"]
    # grid_search += ["/workspace/HDD/EvaluationResults/210822/od/ensemble_2d_pedestrian_0.3_0.1_0.0_0.8_Waymo_Train"]
    # grid_search += ["/workspace/HDD/EvaluationResults/210823/od3d/CameraLidarFusion_1_0.3_1_Audi_Val/"]
    # grid_search += ["/workspace/HDD/EvaluationResults/210825/od3d/get_lidar_camera_fusion_labels_Audi_Val/"]
    grid_search += ["/workspace/HDD/EvaluationResults/210829/od3d/ensemble_3d_ssd_ssn_clf_0.0_0.25_0.25_0.0_0.75_0.5_0.5_Audi_Val/"]
    grid_search += ["/workspace/HDD/EvaluationResults/210829/od3d/ensemble_3d_ssd_ssn_clf_0.0_0.25_0.25_0.0_0.75_0.5_0.75_Audi_Val/"]
    grid_search += ["/workspace/HDD/EvaluationResults/210830/od3d/ensemble_3d_ssd_ssn_clf_0.75_0.5_1.0_0.0_1.0_0.75_0.0_Audi_Val/"]
    print("len grid", len(grid_search))
    print(grid_search[0])
    ret_dict = p.analyze_grid_search(grid_search, title="CC | Grid-search", cls="pedestrian", name="cl_gs")