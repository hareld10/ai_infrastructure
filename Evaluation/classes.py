import pathlib
import pickle
import sys
from collections import defaultdict

import pandas as pd
import cv2
from matplotlib import gridspec
from sklearn.cluster import KMeans
import os
import glob
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib import cm
import matplotlib
from scipy import stats
from tqdm import tqdm

module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, module_path)
sys.path.insert(0, module_path + "/../")

from DataPipeline.shared_utils import pickle_write

plt.style.use('dark_background')
matplotlib.use('Agg')
font = {'size': 15}
matplotlib.rc('font', **font)
from Evaluation.EvalMetrices.show_bb import evaluate_frame_centers, evaluate_frame_ious, evaluate_frame_3d_centers
from wiseTypes.classes_dims import WiseClassesSegmentation, WiseClassesObjectDetection
from Evaluation.evaluation_utils import get_iou_key
from names import *

FRAMES_WITH_INSTANCE = "frames_with_instance"
iou_threshes = np.linspace(0, 1, 10).round(2)
IOU_THRESHES = ["ap_" + str(x) for x in iou_threshes]

class Config(object):
    iou_thresholds = iou_threshes


class Plotter:
    line_options = ['-', '-', '-']
    line_width = 1

    def __init__(self, datas, out_path):
        self.out_path = out_path
        self.out_path_calculated = out_path + "/calculated_df/"
        os.makedirs(self.out_path_calculated, exist_ok=True)
        self.datas = datas
        classes, metrices = set(), set()
        for k, data in datas.items():
            data.calculate(out_path=self.out_path_calculated)
            classes, metrices = data.get_classes_and_metrices()
            classes.update(classes)
            metrices.update(metrices)
        if "pedestrian" in classes:
            classes.remove("pedestrian")
        self.classes = classes
        self.metrices = metrices
        return

    def plot_PR_curve(self, X=RECALL, Y=PRECISION, set_0_1=False, legend=True):
        for cls in sorted(list(self.classes)):
            fig = plt.figure(figsize=(12, 12))
            ax = fig.add_subplot(2, 1, 1)
            x_plot = X.replace("_", "-")
            y_plot = Y.replace("_", "-")

            if set_0_1:
                plt.xlim(0, 1)
                plt.ylim(0, 1)

            fig_name = str(cls) + "_PR_curve" + "_" + x_plot + "_" + y_plot
            for model_idx, model in enumerate(self.datas):
                model_data = self.datas[model].df_map
                threshes = sorted(list(model_data.keys()), reverse=True)
                x_points, y_points = [], []

                for thresh_idx, thresh in enumerate(threshes):
                    x_point = model_data[thresh].loc[X, cls]
                    x_points.append(x_point)
                    y_point = model_data[thresh].loc[Y, cls]
                    y_points.append(y_point)

                plt.plot(x_points, y_points, linewidth=Plotter.line_width, label=model)  # ,linestyle=(0, (model_idx + 1, 2)))
            plt.suptitle("PR-Curve | " + str(cls).capitalize())
            plt.xlabel(X.capitalize())
            plt.ylabel(Y.capitalize())

            if legend:
                self.add_legend(fig)

            # plt.legend(bbox_to_anchor=(0.5, -0.2), loc='lower center')
            # plt.legend(bbox_to_anchor=(0.5, -0.2 + (-0.1) * len(self.datas)), loc='lower center')
            # plt.tight_layout(pad=2)
            plt.savefig(self.out_path + "/" + fig_name + ".png", bbox_inches='tight')
            plt.close()
        return

    @staticmethod
    def plot_PR_curve_as_vector(datas_vec, classes, X=RECALL, Y=PRECISION, set_0_1=False, legend=True, out_path=None):
        for cls in sorted(list(classes)):
            plt.figure(figsize=(12, 12))
            x_plot = X.replace("_", "-")
            y_plot = Y.replace("_", "-")

            if set_0_1:
                plt.xlim(0, 1)
                plt.ylim(0, 1)

            fig_name = "PR_curve_" + str(cls) + "_" + x_plot + "_" + y_plot
            for model_idx, model in enumerate(datas_vec):
                df_vec = datas_vec[model]
                x_points, y_points = [], []

                for thresh, model_data_per_thresh in enumerate(df_vec):
                    x_point = model_data_per_thresh.loc[X, cls]
                    x_points.append(x_point)
                    y_point = model_data_per_thresh.loc[Y, cls]
                    y_points.append(y_point)

                plt.plot(x_points, y_points, linewidth=Plotter.line_width, label=model.split("_")[-1])  # ,linestyle=(0, (model_idx + 1, 2)))
            plt.suptitle("PR-Curve | " + str(cls).capitalize())
            plt.xlabel(X.capitalize())
            plt.ylabel(Y.capitalize())
            if legend:
                plt.legend()

            # plt.legend(bbox_to_anchor=(0.5, -0.2), loc='lower center')
            # plt.legend(bbox_to_anchor=(0.5, -0.2 + (-0.1) * len(self.datas)), loc='lower center')
            # plt.tight_layout(pad=2)
            if out_path is not None:
                plt.savefig(out_path + "/" + fig_name + ".png")
            plt.close()
        return

    @staticmethod
    def plot_PR_curve_as_vector_on_ax(ax, datas_vec, cls, X=RECALL, Y=PRECISION, set_0_1=False, legend=True):
        # x_plot = X.replace("_", "-")
        # y_plot = Y.replace("_", "-")
        if set_0_1:
            plt.xlim(0, 1)
            plt.ylim(0, 1)

        for model_idx, model in enumerate(datas_vec):
            df_vec = datas_vec[model]
            x_points, y_points = [], []

            for thresh, model_data_per_thresh in enumerate(df_vec):
                x_point = model_data_per_thresh.loc[X, cls]
                x_points.append(x_point)
                y_point = model_data_per_thresh.loc[Y, cls]
                y_points.append(y_point)

            ax.plot(x_points, y_points, linewidth=Plotter.line_width, label=model.split("_")[-1])  # ,linestyle=(0, (model_idx + 1, 2)))
        ax.set_xlabel(X.capitalize())
        ax.set_ylabel(Y.capitalize())
        ax.set_title(str(cls.capitalize()) + " | PR Curve", pad=25)
        if legend:
            ax.legend()
        return

    @staticmethod
    def plot_matrix_results(datas_vec, classes, keys, x_labels, xlabel="", out_path=None, suffix="", row=False, task=""):
        if row:
            if len(classes) != 1:
                print("Warning! Row model only in single class")
            fig = plt.figure(figsize=(5 * len(keys) + 5, 5), dpi=200)
            gs = gridspec.GridSpec(ncols=len(keys) + 1, nrows=1)
        else:
            fig = plt.figure(figsize=(5 * len(classes), 5 * len(keys) + 5), dpi=200)
            gs = gridspec.GridSpec(ncols=len(classes), nrows=len(keys) + 1)

        fig_name = "matrix_results" + suffix
        for cls_idx, cls in enumerate(sorted(list(classes))):
            if row:
                ax = fig.add_subplot(gs[0, 0])
            else:
                ax = fig.add_subplot(gs[0, cls_idx])
            Plotter.plot_PR_curve_as_vector_on_ax(ax=ax, datas_vec=datas_vec, cls=cls, legend=True)

        for key_idx, key in enumerate(keys):
            for cls_idx, cls in enumerate(sorted(list(classes))):
                if row:
                    ax = fig.add_subplot(gs[0, key_idx + 1])
                else:
                    ax = fig.add_subplot(gs[key_idx + 1, cls_idx])
                ax.set_title(cls.capitalize() + " | " + key, pad=25)

                for model_idx, model in enumerate(datas_vec):
                    df_vec = datas_vec[model]
                    x_points, y_points = [], []

                    for thresh, model_data_per_thresh in enumerate(df_vec):
                        # x_point = model_data_per_thresh.loc[X, cls]
                        x_points.append(thresh)
                        y_point = model_data_per_thresh.loc[key, cls]
                        y_points.append(y_point)

                    ax.plot(x_points, y_points, linewidth=Plotter.line_width, label=model.split("_")[-1])  # ,linestyle=(0, (model_idx + 1, 2)))

                ax.set_xticks(np.arange(0, len(x_labels), 1))
                ax.set_xticklabels(x_labels, rotation='vertical')
                ax.set_xlabel(xlabel)
                plt.ylabel(key)
                plt.legend()

            # plt.legend(bbox_to_anchor=(0.5, -0.2), loc='lower center')
            # plt.legend(bbox_to_anchor=(0.5, -0.2 + (-0.1) * len(self.datas)), loc='lower center')

        plt.tight_layout(pad=4)
        plt.suptitle(task)
        if out_path is not None:
            suffix = "_row" if row else ""
            plt.savefig(out_path + "/" + fig_name + suffix + ".png")
        plt.close()

        return

    def plot_PR_curve_all_classes(self, side_plot=True, X=RECALL, Y=PRECISION, set_0_1=False, legend=True):
        fig = plt.figure(figsize=(25, 25))
        x_plot = X.replace("_", "-")
        y_plot = Y.replace("_", "-")
        if set_0_1:
            plt.xlim(0, 1)
            plt.ylim(0, 1)

        fig_name = "PR_curve_Multiclass_" + x_plot + "_" + y_plot
        for cls in sorted(list(self.classes)):
            for model_idx, model in enumerate(self.datas):
                model_data = self.datas[model].df_map
                threshes = sorted(list(model_data.keys()), reverse=True)
                x_points, y_points = [], []

                for thresh in threshes:
                    x_point = model_data[thresh].loc[X, cls]
                    x_points.append(x_point)
                    y_point = model_data[thresh].loc[Y, cls]
                    y_points.append(y_point)

                plt.plot(x_points, y_points, linewidth=self.line_width, label=model + "_" + cls.capitalize())  # ,linestyle=(0, (model_idx + 1, 2)))
        plt.suptitle("PR-Curve")
        plt.xlabel(X.capitalize())
        plt.ylabel(Y.capitalize())
        if legend:
            self.add_legend(fig)
            # plt.legend()
        plt.savefig(self.out_path + "/" + fig_name + ".png")
        plt.close()
        return

    def add_legend(self, fig):
        row, col = fig.axes[0].get_subplotspec().get_gridspec().get_geometry()
        handles, labels = plt.gca().get_legend_handles_labels()
        ax = fig.add_subplot(row, col, row*col)
        plt.axis(False)
        ax.legend(handles, labels)

    def plot_key(self, desired_keys):
        for cls in sorted(list(self.classes)):
            plt.figure(figsize=(10 * len(desired_keys), 10))
            fig_name = str("-".join([x.capitalize() for x in desired_keys])) + " | " + str(cls).capitalize()
            fig_save_name = str(cls).capitalize() + "_" + str("_".join([x.capitalize() for x in desired_keys]))
            for plt_idx, desired_key in enumerate(desired_keys):
                plt.subplot(1+1, len(desired_keys), plt_idx + 1)

                for model_idx, model in enumerate(self.datas):
                    model_data = self.datas[model].df_map
                    threshes = sorted(list(model_data.keys()))
                    x_points, y_points = [], []

                    for thresh in threshes:
                        x_point = thresh
                        x_points.append(x_point)
                        y_point = model_data[thresh].loc[desired_key, cls]
                        y_points.append(y_point)

                    plt.plot(x_points, y_points, label=model, linewidth=self.line_width)  # , linestyle=(0, (model_idx + 1, 2)))
                plt.title(desired_key.replace("_", "-").capitalize())
                plt.xlabel(CONFIDENCE)
                plt.ylabel(desired_key.replace("_", "-").capitalize())
                # plt.legend()
                # plt.legend(bbox_to_anchor=(0.5, -0.3 + (-0.2) * len(self.datas)), loc='lower center')

            self.add_legend(plt.gcf())
            plt.suptitle(cls.capitalize())
            # plt.tight_layout()
            plt.savefig(self.out_path + "/" + fig_save_name + ".png")
            plt.close()

    def plot_keys(self, desired_keys):
        for cls in sorted(list(self.classes)):
            plt.figure(figsize=(10, 10))
            fig_name = str("-".join([x.capitalize() for x in desired_keys])) + " | " + str(cls).capitalize()
            fig_save_name = str("_".join([x.capitalize() for x in desired_keys])) + "_" + str(cls).capitalize()
            for desired_key in desired_keys:
                for model_idx, model in enumerate(self.datas):
                    model_data = self.datas[model].df_map
                    threshes = sorted(list(model_data.keys()))
                    x_points, y_points = [], []

                    for thresh in threshes:
                        x_point = thresh
                        x_points.append(x_point)
                        y_point = model_data[thresh].loc[desired_key, cls]
                        y_points.append(y_point)

                    plt.plot(x_points, y_points, label=desired_key + "_" + model.split("_")[-1], linewidth=self.line_width)  # , linestyle=(0, (model_idx + 1, 2)))
            plt.suptitle(fig_name.capitalize())
            plt.xlabel("Threshold")
            plt.ylabel("Value")
            plt.legend(bbox_to_anchor=(0.5, -0.4), loc='lower center')
            plt.tight_layout()
            plt.savefig(self.out_path + "/" + fig_save_name + ".png")
            plt.close()

    def plot_planes(self):
        for cls in sorted(list(self.classes)):
            combined_data = defaultdict(dict)
            for key in [PRECISION, RECALL]:
                fig = plt.figure(figsize=(10 * len(self.datas), 10))
                fig_name = "Plane_" + key + "_" + str(cls).capitalize()
                for model_idx, model in enumerate(self.datas):
                    ax = plt.subplot(1, len(self.datas), model_idx + 1)  # , projection='3d')
                    model_data = self.datas[model].df_map
                    y_confidene = sorted(list(model_data.keys()), reverse=True)
                    X, Y = np.linspace(0, 1, 10).round(2), [float(y) for y in y_confidene]
                    Z = np.zeros((len(IOU_THRESHES), len(y_confidene)), dtype=np.float32)

                    for y_idx, y_thresh in enumerate(y_confidene):
                        for x_idx, x_iou_thresh in enumerate(iou_threshes):
                            val = float(model_data[y_thresh].loc[get_iou_key(key=key, thresh=x_iou_thresh), cls])
                            Z[y_idx, x_idx] = np.nan_to_num(val, nan=1)

                    if key not in combined_data[model]: combined_data[model] = {}
                    combined_data[model][key] = (Z, y_confidene, iou_threshes)

                    c_mesh = ax.pcolormesh(X, Y, Z, shading="auto", cmap="turbo", norm=mpl.colors.Normalize(vmin=0, vmax=1))
                    fig.colorbar(c_mesh, shrink=0.5, aspect=10)
                    plt.title(model.split("_")[-1])
                    plt.xlabel("IOU")
                    plt.ylabel(CONFIDENCE)

                plt.suptitle(key.capitalize() + " Plane | " + str(cls).capitalize())
                plt.tight_layout(pad=3)
                plt.savefig(self.out_path + "/" + fig_name + ".png")
                plt.close()

            # Plot Combined
            # fig = plt.figure(figsize=(10 * len(self.datas), 10))
            # fig_name = "Combined_Plane_" + key + "_" + str(cls).capitalize()
            # for model_idx, (model, model_dict) in enumerate(combined_data.items()):
            #     ax = plt.subplot(1, len(self.datas), model_idx + 1)
            #     X, Y = np.linspace(0, 1, 10).round(2), np.linspace(0, 1, 10).round(2)
            #     Z, y_confidene, iou_threshes =
            #     for y_idx, y_thresh in enumerate(y_confidene):
            #         for x_idx, x_iou_thresh in enumerate(iou_threshes):
            #             val = float(model_data[y_thresh].loc[get_iou_key(key=key, thresh=x_iou_thresh), cls])
            #             Z[y_idx, x_idx] = np.nan_to_num(val, nan=1)

    def plot_bars_per_class(self, desired_key):
        for model_idx, model in enumerate(self.datas):
            plt.figure(figsize=(10, 10))
            fig_name = str(desired_key) + "_" + str(model)
            model_data = self.datas[model].df_map
            plot_idx = 1

            for df_key, model_df in model_data.items():
                plt.figure(figsize=(10, 10))
                ax = plt.gca()
                # ax = plt.subplot(len(list(model_data.keys())), 1, plot_idx)
                xtickets, cls_plot = [], 0
                plot_idx += 1
                ax.set_ylim(0, 1)
                for cls_idx, cls in enumerate(sorted(list(self.classes))):
                    val = model_df.loc[desired_key, cls]
                    if val == 0:
                        continue
                    xtickets.append(cls)
                    rects1 = ax.bar(cls_plot, val, 0.8)
                    cls_plot += 1

                plt.ylabel(desired_key.capitalize())
                ax = plt.gca()
                plt.setp(ax.get_xticklabels(), rotation=30, horizontalalignment='right')
                ax.set_xticks(np.arange(len(xtickets)))
                ax.set_xticklabels(sorted(list(xtickets)))
                plt.title(fig_name)
                plt.xlabel("Classes")
                plt.tight_layout(pad=4)
                plt.savefig(self.out_path + "/" + fig_name + ".png")
                plt.close()

    def plot_example_models(self, models_lst, min_amount=10):
        out_images = self.out_path + "/compare_models_images/"
        os.makedirs(out_images, exist_ok=True)
        sets = [set(os.listdir(model_dir)) for model_dir in models_lst]
        intersection = list(set.intersection(*sets))

        for i in tqdm(range(min(min_amount, len(intersection)))):
            if not intersection[i].endswith(".png"):
                continue
            imgs = [cv2.imread(model_dir + "/" + intersection[i]) for model_dir in models_lst]
            plt.figure(figsize=(10 * len(imgs), 10))

            for plt_idx, im in enumerate(imgs):
                plt.subplot(1, len(imgs), plt_idx + 1)
                plt.imshow(im[..., ::-1])
                plt.title(os.path.split(models_lst[plt_idx])[-1].split("_")[-2])
            plt.suptitle("Compare models")
            plt.tight_layout()
            plt.savefig(out_images + "/merged_" + str(i))

            plt.close()

        return


class Data(object):
    """
    Holds df_map with thresh as key and result df as value
    """

    def __init__(self, dataset_name, model_name):
        self.df_map = {}
        self.dataset_name = dataset_name
        self.model_name = model_name

    def calculate(self, out_path=""):
        for k in self.df_map.keys():
            self.df_map[k] = Data.calculate_df(self.df_map[k])
            # self.df_map[k].loc[PRECISION] = self.df_map[k].loc[TP, :] / (self.df_map[k].loc[TP, :] + self.df_map[k].loc[FP, :])
            # self.df_map[k].loc[RECALL] = self.df_map[k].loc[TP, :] / (self.df_map[k].loc[TP, :] + self.df_map[k].loc[FN, :])
            # self.df_map[k].loc[F1_SCORE] = (2 * self.df_map[k].loc[PRECISION] * self.df_map[k].loc[RECALL]) / (self.df_map[k].loc[PRECISION] + self.df_map[k].loc[RECALL])
            # self.df_map[k].loc[FALSE_DISCOVERY_RATE] = self.df_map[k].loc[FP, :] / (self.df_map[k].loc[TP, :] + self.df_map[k].loc[FP, :])
            # self.df_map[k].loc[IOU] = self.df_map[k].loc[TP, :] / (self.df_map[k].loc[TP, :] + self.df_map[k].loc[FP, :] + self.df_map[k].loc[FN, :])
            # self.df_map[k].fillna(1, inplace=True)
            #
            # for iou_thresh in iou_threshes:
            #     if TP + "_iou_" + str(iou_thresh) not in self.df_map[k].index:
            #         continue
            #     self.df_map[k].loc[PRECISION + "_iou_" + str(iou_thresh)] = self.df_map[k].loc[TP + "_iou_" + str(iou_thresh), :] / (
            #             self.df_map[k].loc[TP + "_iou_" + str(iou_thresh), :] + self.df_map[k].loc[FP + "_iou_" + str(iou_thresh), :])
            #     self.df_map[k].loc[RECALL + "_iou_" + str(iou_thresh)] = self.df_map[k].loc[TP + "_iou_" + str(iou_thresh), :] / (
            #             self.df_map[k].loc[TP + "_iou_" + str(iou_thresh), :] + self.df_map[k].loc[FN + "_iou_" + str(iou_thresh), :])

        if out_path:
            for k in self.df_map.keys():
                self.df_map[k].to_csv(out_path + "/" + self.model_name + "_calculated_" + str(k) + ".csv")

    @staticmethod
    def calculate_df(df):
        df.loc[PRECISION] = df.loc[TP, :] / (df.loc[TP, :] + df.loc[FP, :])
        df.loc[RECALL] = df.loc[TP, :] / (df.loc[TP, :] + df.loc[FN, :])
        df.loc[F1_SCORE] = (2 * df.loc[PRECISION] * df.loc[RECALL]) / (df.loc[PRECISION] + df.loc[RECALL])
        df.loc[FALSE_DISCOVERY_RATE] = df.loc[FP, :] / (df.loc[TP, :] + df.loc[FP, :])
        df.loc[IOU] = df.loc[TP, :] / (df.loc[TP, :] + df.loc[FP, :] + df.loc[FN, :])
        df.loc[FALSE_RATE] = df.loc[FP, :] / (df.loc[TP, :] + df.loc[FP, :])

        try:
            df.loc[TRUE_RATE] = df.loc[TP, :] / (df.loc[TP, :] + df.loc[TN, :])
        except Exception as e:
            print("calculate_df", e)
            pass
        # df.fillna(1, inplace=True)
        return df

    def grab_rand_df(self):
        rand_thresh = list(self.df_map.keys())[0]
        return self.df_map[rand_thresh]

    def get_classes_and_metrices(self):
        rand_df = self.grab_rand_df()
        classes, metrices = set(), set()
        classes.update(list(rand_df.keys()))
        rand_key = list(classes)[0]
        metrices.update(rand_df[rand_key].index)
        return classes, metrices

    def __repr__(self):
        return "Data: " + str(self.dataset_name) + "_" + str(self.model_name) + "\n" + str(self.df_map)


class EvalResults(object):
    def __init__(self, model_name, dataset_name, out_path, config=None):
        self.model_name = model_name
        self.dataset_name = dataset_name
        self.title = self.dataset_name + "_" + self.model_name
        self.out_path = out_path
        self.calculated_df_out_path = self.out_path + "/dfs/"
        os.makedirs(self.calculated_df_out_path, exist_ok=True)
        self.images_out_path = self.out_path + "/images/"
        os.makedirs(self.images_out_path, exist_ok=True)

        self.thresh_vec = np.linspace(0, 1, 10).round(2)

        self.df_map = {}
        self.ap_map = {}
        for ths in self.thresh_vec:
            self.df_map[ths] = pd.DataFrame()
            self.ap_map[ths] = {}

        if config is not None:
            self.save_pkl(obj=config, path=self.out_path + "/config.pkl")
            self.config = config
        self.num_frames = 0
        self.data = None

        self.frames_results = {}

        return

    def save_pkl(self, obj, path):
        with open(path, 'wb') as handle:
            pickle.dump(obj, handle, protocol=pickle.HIGHEST_PROTOCOL)

    def set_ap(self, iou_key, val, cls, thresh, iters=1):
        cur_ap = self.ap_map[thresh]
        # iou_key = "ap_" + str(iou)
        if cls not in cur_ap.keys():
            cur_ap[cls] = {}
        if iou_key not in cur_ap[cls]:
            cur_ap[cls][iou_key] = []
        for i in range(iters):
            cur_ap[cls][iou_key].append(val)

    def set_stat_param(self, cur_df, key, val, cls):
        if cls not in cur_df.keys():
            cur_df[cls] = 0
        if key not in cur_df[cls].index:
            cur_df.loc[key, cls] = 0
        cur_df.loc[key, cls] += val

    def keep_frame_results(self, frame_results, id):
        self.frames_results[id] = frame_results

    def set_frame_results(self, frame_results):
        self.num_frames += 1
        for thresh in self.thresh_vec:
            for c in frame_results.get_classes():
                cur_labels = frame_results.get_centers_by_cls_and_score(cls=c, score=thresh, pred=False)
                cur_predictions = frame_results.get_centers_by_cls_and_score(cls=c, score=thresh, pred=True)

                stat_param_dict = evaluate_frame_centers(predictions_centers=cur_predictions, gt_boxes=cur_labels, gt_boxes_as_centers=True, iou=0)
                for k, v in stat_param_dict.items():
                    self.set_stat_param(cur_df=self.df_map[thresh], key=k, val=v, cls=c)

                for iou_thresh in self.config.iou_thresholds:
                    stat_param_dict = evaluate_frame_centers(predictions_centers=cur_predictions, gt_boxes=cur_labels, gt_boxes_as_centers=True, iou=iou_thresh)
                    for k, v in stat_param_dict.items():
                        self.set_stat_param(cur_df=self.df_map[thresh], key=k + "_iou_" + str(iou_thresh), val=v, cls=c)

                # res = evaluate_frame_ious(predictions_centers=cur_predictions, gt_boxes=cur_labels, iou_thresholds=self.config.iou_thresholds, gt_boxes_as_centers=True)
                # for ap_idx, ap in enumerate(res["ap"]):
                #     self.set_ap(iou_key="ap_" + str(self.config.iou_thresholds[ap_idx]), val=ap, cls=c, thresh=thresh, iters=len(cur_labels))

                # for ap_idx, precision in enumerate(res["precisions"]):
                #     self.set_ap(iou_key="precision_" + str(self.config.iou_thresholds[ap_idx]), val=precision, cls=c, thresh=thresh, iters=len(cur_labels))
                #
                # for ap_idx, recall in enumerate(res["recalls"]):
                #     self.set_ap(iou_key="recall_" + str(self.config.iou_thresholds[ap_idx]), val=recall, cls=c, thresh=thresh, iters=len(cur_labels))

                self.set_stat_param(cur_df=self.df_map[thresh], key=FRAMES_WITH_INSTANCE, val=1, cls=c)
        return

    def get_data(self):
        return self.data

    def save_results(self, save_eval_results=True):
        self.data = Data(model_name=self.model_name, dataset_name=self.dataset_name)
        for thresh in self.thresh_vec:
            cur_df = self.df_map[thresh]
            cur_ap = self.ap_map[thresh]

            for cls, iou_d in cur_ap.items():
                for iou_k, val in iou_d.items():
                    if iou_k not in cur_df[cls].index:
                        cur_df.loc[iou_k, cls] = 0
                    cur_df.loc[iou_k, cls] = round(np.array(val).mean(), 2)

            cur_df.to_csv(self.calculated_df_out_path + "/results_" + str(self.num_frames) + "_thresh_" + str(thresh) + ".csv")
            self.data.df_map[thresh] = cur_df

        pickle_write(file_path=self.out_path + "/" + self.title + ".pkl", obj=self.data)
        if save_eval_results: pickle_write(file_path=self.out_path + "/evaluation_results_" + self.title + ".pkl", obj=self)

    @staticmethod
    def plot_dfs(df_map, out_path):
        plt.figure(figsize=(20, 40), dpi=150)

        classes, metrices = set(), set()
        for k, v in df_map.items():
            df_map[k].loc["precision"] = df_map[k].loc["tp", :] / (df_map[k].loc["tp", :] + df_map[k].loc["fp", :])
            df_map[k].loc["recall"] = df_map[k].loc["tp", :] / (df_map[k].loc["tp", :] + df_map[k].loc["fn", :])

        for k, v in df_map.items():
            # rand_model = list(df_map.keys())[0]
            classes.update(list(v.keys()))
            rand_key = list(classes)[0]
            metrices.update(v[rand_key].index)

        first_figure = {}
        second_figure = {"tp": True, "fp": True, "fn": True}
        for met in metrices:
            if met not in second_figure:
                first_figure[met] = True

        n_cols = 2
        n_rows = len(classes)
        plot_idx = 1
        width = 0.2

        legend_flag = True
        for cls_idx, cls in enumerate(sorted(classes)):
            for g_idx, graph in enumerate([sorted(list(first_figure.keys()), reverse=True), sorted(list(second_figure.keys()))]):
                x = np.arange(len(graph))
                ax = plt.subplot(n_rows, n_cols, plot_idx)
                num_maps = len(df_map)
                if num_maps == 1:
                    margin = [0]
                else:
                    margin = np.linspace(-width, width, len(df_map))

                for model_idx, (model_name, df) in enumerate(df_map.items()):
                    if cls in list(df.keys()):
                        rects1 = ax.bar(x - margin[model_idx], df.loc[graph, cls], width, label=model_name)

                ax.set_ylabel('Score' if g_idx == 0 else "Count")
                ax.set_title('Metrices Scores: ' + str(cls)).capitalize()
                ax.set_xticks(x)
                ax.set_xticklabels(graph)
                if g_idx == 0:
                    ax.set_ylim(0, 1)
                    ax.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
                plot_idx += 1
        plt.tight_layout(pad=3)
        plt.savefig(out_path + "/results.png")
        plt.close()

    @staticmethod
    def plot_data(datas, out_path, set_0_1=True):
        """

        @param datas: dict {str: Model_Name, Datas: model Data object which holds all df's with thresholds}
        @param out_path:
        @param set_0_1:
        """
        plotter = Plotter(out_path=out_path, datas=datas)
        plotter.plot_PR_curve()
        plotter.plot_PR_curve_all_classes(side_plot=False, set_0_1=set_0_1, legend=True)
        plotter.plot_key(desired_keys=[PRECISION, RECALL, TRUE_RATE, FALSE_RATE])


class EvalResultsSeg(EvalResults):
    def __init__(self, model_name, dataset_name, out_path, config):
        super().__init__(model_name, dataset_name, out_path, config)
        self.wise_classes = WiseClassesSegmentation()
        return

    def get_data(self):
        return self.data

    def set_frame_results(self, frame_results):
        label_data = frame_results.label.get_img()
        for thresh in self.thresh_vec:
            pred_data = frame_results.hard_prediction.get_img().copy()
            pred_data[frame_results.soft_prediction <= thresh] = 0

            for unified_idx in frame_results.get_classes():
                TP_count = np.logical_and(pred_data == unified_idx, label_data == unified_idx).sum()
                TN_count = np.logical_and(pred_data != unified_idx, label_data != unified_idx).sum()
                FP_count = np.logical_and(pred_data == unified_idx, label_data != unified_idx).sum()
                FN_count = np.logical_and(pred_data != unified_idx, label_data == unified_idx).sum()
                self.set_stat_param(cur_df=self.df_map[thresh], key=TP, val=TP_count, cls=self.wise_classes.get_obj_by_idx(unified_idx=unified_idx).name)
                self.set_stat_param(cur_df=self.df_map[thresh], key=TN, val=TN_count, cls=self.wise_classes.get_obj_by_idx(unified_idx=unified_idx).name)
                self.set_stat_param(cur_df=self.df_map[thresh], key=FP, val=FP_count, cls=self.wise_classes.get_obj_by_idx(unified_idx=unified_idx).name)
                self.set_stat_param(cur_df=self.df_map[thresh], key=FN, val=FN_count, cls=self.wise_classes.get_obj_by_idx(unified_idx=unified_idx).name)


class EvalResults3D(EvalResults):
    def __init__(self, model_name, dataset_name, out_path, config):
        super().__init__(model_name, dataset_name, out_path, config)
        self.wise_classes = WiseClassesObjectDetection()
        self.metrics = config.metrics
        # Holds df_vec of thresholds
        self.df_vec = []
        self.classes = set()
        self.task = "task=detection"
        self.vehicle_classes = {"car": True, "bus": True, "truck": True, "bicycle": True, "motorcycle": True}
        for metric_idx in self.metrics:
            self.df_vec.append(pd.DataFrame())
        return

    def get_data(self):
        return self.data

    def set_frame_results(self, frame_results):
        self.num_frames += 1
        for metric_idx, metric in enumerate(self.metrics):
            classes_results = metric.calculate(frame_results)
            for cls, cls_result in classes_results.items():
                self.classes.add(cls)
                for conf_matrix_name, conf_matrix_value in cls_result.items():
                    self.set_stat_param(cur_df=self.df_vec[metric_idx], key=conf_matrix_name, val=conf_matrix_value, cls=cls)

    def save_results(self, save_eval_results=True, save_datas=True):
        self.data = Data(model_name=self.model_name, dataset_name=self.dataset_name)
        for metric_idx, metric_df in enumerate(self.df_vec):
            self.df_vec[metric_idx] = Data.calculate_df(metric_df)

        for metric_idx, metric_df in enumerate(self.df_vec):
            cur_df = self.df_vec[metric_idx]
            cur_df.to_csv(self.calculated_df_out_path + "/results_" + self.metrics[metric_idx].get_name() + "_" + str(self.num_frames) + ".csv")
            self.data.df_map[self.metrics[metric_idx].get_name()] = cur_df

        if save_datas: pickle_write(file_path=self.out_path + "/" + self.title + ".pkl", obj=self.data)
        if save_eval_results: pickle_write(file_path=self.out_path + "/evaluation_results_" + self.title + ".pkl", obj=self)

        if len(self.classes) > 0:
            Plotter.plot_matrix_results(datas_vec={self.model_name: self.df_vec}, classes=self.classes, out_path=self.out_path,
                                        keys=[PRECISION, RECALL, TRUE_RATE, FALSE_RATE], x_labels=[x.get_threshold_str() for x in self.metrics], xlabel=self.metrics[0].get_xlabel(), task=self.task)

            Plotter.plot_matrix_results(datas_vec={self.model_name: self.df_vec}, classes=self.classes, out_path=self.out_path,
                                        keys=[PRECISION, RECALL, TRUE_RATE, FALSE_RATE], x_labels=[x.get_threshold_str() for x in self.metrics], row=True, xlabel=self.metrics[0].get_xlabel(), task=self.task)
        return


class FrameResults(object):
    def __init__(self, labels=None, predictions=None, projected_lidar=None):
        self.labels = labels
        self.predictions = predictions
        self.projected_lidar = projected_lidar
        self.data_dict = {}
        return

    def set_labels(self, item):
        self.labels = item
        return

    def get_labels(self):
        return self.labels

    def set_prediction(self, item):
        """
        put array of predictions
        """
        self.predictions = item
        return

    def get_labels_classes(self):
        clsses = set()
        [clsses.add(x.cls) for x in list(self.labels)]
        return sorted(list(clsses))

    def get_predictions_classes(self):
        clsses = set()
        [clsses.add(x.cls) for x in list(self.predictions)]
        return sorted(list(clsses))

    def get_classes(self):
        clsses = set()
        [clsses.add(x.cls) for x in list(self.predictions)]
        [clsses.add(x.cls) for x in list(self.labels)]
        return sorted(list(clsses))

    def get_centers_by_cls_and_score(self, cls, score, pred=True):
        cur_list = self.predictions if pred else self.labels
        centers_return = []
        for elem in cur_list:
            if elem.cls == cls and elem.score >= score:
                centers_return.append(elem.get_center())
        return np.array(centers_return)

    def get_objects_by_cls_and_score_as_list(self, cls, score=0, pred=True):
        cur_list = self.predictions if pred else self.labels
        objs = []
        for elem in cur_list:
            if (elem.cls == cls or cls == True) and elem.score >= score:
                objs.append(elem)
        return np.array(objs)

    def get_objects_by_cls_and_score(self, cls, score, pred=True):
        cur_list = self.predictions if pred else self.labels
        centers_return = {}
        for elem_idx, elem in cur_list.items():
            if (elem.cls == cls or cls == True) and elem.get_score() >= score:
                centers_return[elem.get_id()] = elem
        return centers_return


class FrameResultsSeg(object):
    def __init__(self):
        self.label = None
        self.hard_prediction = None
        self.soft_prediction = None
        return

    def set_label(self, item):
        self.label = item
        return

    def set_prediction(self, hard_pred, soft_pred):
        self.hard_prediction = hard_pred
        self.soft_prediction = soft_pred
        return

    def get_classes(self):
        clsses = set(np.unique(self.label.get_img()))
        clsses.update(set(np.unique(self.hard_prediction.get_img())))
        return clsses

# class ResultsDataFrame(pd.DataFrame):
#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#
#     def add_dict(self, d):
#         for k, v in d.ite

def main():
    if 0:
        b = "/home/amper/AI/Harel/AI_Infrastructure/Evaluation/eval_results/combined_evaluation_Waymo_Val/"
        file_path = b + "/datas_od"

        with open(file_path, 'rb') as file:
            datas = pickle.load(file)

        p = Plotter(datas=datas, out_path=b)

        p.plot_PR_curve()

        keys_to_plot = ["ap_0.5", "ap_0.1", "ap_0.3", "ap_0.75"]
        for key in keys_to_plot:
            p.plot_key(desired_key=key)

    if 0:
        b = "/home/amper/AI/Harel/AI_Infrastructure/Evaluation/eval_results/combined_evaluation_Audi_Val/"
        file_path = b + "/datas_seg"

        with open(file_path, 'rb') as file:
            datas = pickle.load(file)

        p = Plotter(datas=datas, out_path=b)

        # p.plot_PR_curve(side_plot=False, set_0_1=True, legend=False)
        p.plot_PR_curve_all_classes(side_plot=False, set_0_1=True, legend=True)
        # p.plot_key(desired_keys=[PRECISION, RECALL])
        # p.plot_bars_per_class(IOU)
        # keys_to_plot = [FALSE_DISCOVERY_RATE, F1_SCORE]
        # for key in keys_to_plot:
        #     p.plot_key(desired_key=key)

    if 0:
        models = ["od_evaluation_Waymo_Val_EfficientDetD7", "od_evaluation_Waymo_Val_YoloV3", "od_evaluation_Waymo_Val_Ensemble"]
        b = "/home/amper/AI/Harel/AI_Infrastructure/Evaluation/eval_results/"
        out_path = b + "/combined_evaluation_Waymo_Val/"
        os.makedirs(out_path, exist_ok=True)
        datas_od = {}
        for m in models:
            model_name = m.replace("od_evaluation_", "")
            file_path = glob.glob(b + m + "/W*.pkl")[0]

            with open(file_path, 'rb') as file:
                datas = pickle.load(file)

            datas_od[model_name] = datas

        p = Plotter(datas=datas_od, out_path=out_path)
        # p.plot_planes()

        # p.plot_example_models(models_lst=[b + model for model in ["od_evaluation_Waymo_Val_EfficientDetD7_backup", "od_evaluation_Waymo_Val_YoloV3_backup", "od_evaluation_Waymo_Val_Ensemble_backup"]],
        #                       min_amount=200)
        p.plot_PR_curve()
        # keys_to_plot = ["ap_0.5", "ap_0.1", "ap_0.3", "ap_0.75"]
        # p.plot_key(desired_keys=keys_to_plot)
        p.plot_key(desired_keys=[PRECISION, RECALL])

        for iou_thresh in iou_threshes:
            p.plot_key(desired_keys=[get_iou_key(PRECISION, thresh=iou_thresh), get_iou_key(RECALL, thresh=iou_thresh)])
            p.plot_PR_curve(X=get_iou_key(RECALL, thresh=iou_thresh), Y=get_iou_key(PRECISION, thresh=iou_thresh))

        # p.plot_key(desired_keys=[IOU]/)

    if 0:
        # models = ["od_evaluation_Waymo_Val_EfficientDetD7_backup", "od_evaluation_Waymo_Val_YoloV3_backup", "od_evaluation_Waymo_Val_Ensemble_backup"]
        models = ["od_evaluation_Waymo_Val_Ensemble_backup"]
        # models = ["od_evaluation_Waymo_Val_EfficientDetD7", "od_evaluation_Waymo_Val_YoloV3", "od_evaluation_Waymo_Val_Ensemble"]
        b = "/home/amper/AI/Harel/AI_Infrastructure/Evaluation/eval_results/"
        datas_od = {}
        for m in models:
            model_name = m.replace("od_evaluation_", "")
            file_path = glob.glob(b + m + "/evaluation_results*.pkl")[0]

            with open(file_path, 'rb') as file:
                datas = pickle.load(file)
                out_path = (b + m).replace("_backup", "")
                os.makedirs(out_path, exist_ok=True)
                e_results = EvalResults(dataset_name=datas.dataset_name, model_name=datas.model_name, out_path=out_path, config=datas.config)

                max_val, min_val = 1, 0.2
                scores = []
                for ts, frame_result in tqdm(datas.frames_results.items()):
                    for idx, label in enumerate(frame_result.predictions):
                        frame_result.predictions[idx].score = (frame_result.predictions[idx].score - min_val) / (max_val - min_val)
                        scores.append(frame_result.predictions[idx].score)
                    e_results.set_frame_results(frame_results=frame_result)

                e_results.save_results(save_eval_results=False)

                print()
                print(m)
                described = stats.describe(scores)
                print(described)
            # datas_od[model_name] = datas

        # p = Plotter(datas=datas_od, out_path=b + "/combined_evaluation_Waymo_Val/")
        # p.plot_example_models(models_lst=[b + model for model in models])
        # p.plot_PR_curve()
        #
        # keys_to_plot = ["ap_0.0", "ap_0.1", "ap_0.3", "ap_0.5", "ap_0.75"]
        # p.plot_key(desired_keys=keys_to_plot)
        # p.plot_key(desired_keys=[PRECISION, RECALL])
        # p.plot_key(desired_keys=[IOU])

    if 0:
        b = "/home/amper/AI/Harel/AI_Infrastructure/Evaluation/EvaluationResults/201223/od3d/Evaluation_combined_Audi_Val/"
        file_path = b + "/datas_3d"

        with open(file_path, 'rb') as file:
            datas = pickle.load(file)

        p = Plotter(datas=datas, out_path=b)

        # keys_to_plot = [FP, FN, TP, FALSE_DISCOVERY_RATE, F1_SCORE, ]
        # for key in keys_to_plot:
        #     p.plot_key(desired_key=key)

        p.plot_PR_curve()
        # p.plot_bars_per_class(RECALL)
        # p.plot_bars_per_class(PRECISION)
        # p.plot_keys([FP, FN, TP])
        # p.plot_keys([RECALL, PRECISION])
        # p.plot_keys([FALSE_DISCOVERY_RATE, F1_SCORE])

    if 1:
        base = "/home/amper/AI/Harel/AI_Infrastructure/Evaluation/EvaluationResults/210123/detection2/Evaluation_camera_lidar_v1.0_200831_lidar_drive_1_urban_W92_M15_RF1/"
        # base = "/home/amper/AI/Harel/AI_Infrastructure/Evaluation/EvaluationResults/201227/od/Evaluation_camera_lidar_v1.0_200831_lidar_drive_1_urban_W92_M15_RF1/"
        f_name = "200831_lidar_drive_1_urban_W92_M15_RF1_camera_lidar_v1.0.pkl"
        # f_name = "evaluation_results_200831_lidar_drive_1_urban_W92_M15_RF1_camera_lidar_v1.0_full.pkl"
        with open(base + f_name, 'rb') as file:
            eval_results = pickle.load(file)

        eval_results.save_results(save_eval_results=False, save_datas=False)


if __name__ == "__main__":
    main()
