import argparse
import os
import pathlib
import cv2
import matplotlib
from matplotlib import gridspec, colors
from tqdm import tqdm
import sys
import matplotlib.patches as mpatches
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import json
# define drive
from Calibrator.calibrator_utils import get_3d_ax, apply_6DOF
from DataPipeline.settings import Context
from DataPipeline.shared_utils import read_label, PrintException
from DataPipeline.src.GetEngines import get_engines
from Evaluation.Metrics import CenterOverlay, OccupancyGridMetric
from Evaluation.classes import EvalResults, FrameResults, EvalResults3D
from Evaluation.evaluation_utils import plot_boxes, get_cur_working_directory, build_objs, draw, make_video_from_dir
from Evaluation.occupancy_grid import OccupancyGrid
from OpenSource.WisenseEngine import WisenseEngine
from Validation.utils import project_lidar_2_radar
from wiseTypes.Label3D import Label3D
from Evaluation.evaluation_utils import cmap_clusters
from wiseTypes.classes_dims import Road

plt.style.use('dark_background')
font = {'size': 16}
matplotlib.rc('font', **font)
static_cluster_color = "dodgerblue"
dynamic_cluster_color = "yellow"


class Config:
    # Params
    object_diameter_m = 2
    minimum_dop_norm = 0.5
    plt_every_x_frames = 50000
    plot_center_size = 150
    scatter_size = 1
    line_width = 1

    radar_fov_az = 80  # [deg]
    radar_fov_el = 30  # [deg]
    min_range = 2  # [m]
    max_range = 76  # [m]

    # define metrics
    # metrics = list(reversed([CenterOverlay(detections_cfar=x) for x in np.linspace(28, 42, 10)]))
    metrics = list(reversed([OccupancyGridMetric(detections_cfar=x) for x in [10, 20, 25, 30, 35, 45, 55]]))
    spatial_calibration_params = None


config = Config()

# val_drives = [get_engines(val_train="val")[0]]
val_drives = get_engines(val_train="val")
for drive_idx, data_engine in enumerate(val_drives):
    config.spatial_calibration_params = data_engine.drive_context.spatial_calibration_params
    drive = data_engine.get_drive_name()
    print(drive)

    data_engine.sample_df(20)

    model_name = "camera_lidar_v1.0"
    out_path, images_path = get_cur_working_directory(model_name=model_name, data_engine_name=drive, task="detection")
    # out_path_static, images_path_static = get_cur_working_directory(model_name=model_name, data_engine_name=drive, suffix="_static", task="od")
    # out_path_dynamic, images_path_dynamic = get_cur_working_directory(model_name=model_name, data_engine_name=drive, suffix="_dynamic", task="od")

    # e_results_static = EvalResults3D(dataset_name=drive, model_name=model_name, out_path=out_path_static, config=Config())
    # e_results_dynamic = EvalResults3D(dataset_name=drive, model_name=model_name, out_path=out_path_dynamic, config=Config())
    e_results = EvalResults3D(dataset_name=drive, model_name=model_name, out_path=out_path, config=Config())

    for frame_idx in tqdm(range(len(data_engine))):
        # save_fig_path_static = images_path_static + "/" + data_engine.get_id(idx=frame_idx) + ".png"
        # save_fig_path_dynamic = images_path_dynamic + "/" + data_engine.get_id(idx=frame_idx) + ".png"
        save_fig_path = images_path + "/occupancy_" + data_engine.get_id(idx=frame_idx) + ".png"

        try:
            ####### Read Data #######
            rgb_img = data_engine.get_image(idx=frame_idx)
            _, _projected_lidar = data_engine.get_lidar(idx=frame_idx, concat_original_points=True)
            lidar_df = data_engine.get_lidar_df(idx=frame_idx, filter_by_seg_class=Road())
            projected_lidar_df = lidar_df.dropna()
            radar_delta = data_engine.get_delta(idx=frame_idx)
            radar_p_delta = data_engine.get_processed_delta(idx=frame_idx)
            labels = data_engine.get_lidar_camera_fusion_labels(idx=frame_idx, as_obj=True, obj_diameter=config.object_diameter_m)
            delta_clusters = data_engine.get_clusters(p_delta=radar_p_delta)
            lidar_clusters = data_engine.get_lidar_clusters(lidar_df)

            ####### Get Predictions #######
            not_static_delta = radar_p_delta[np.linalg.norm([radar_p_delta["vx"], radar_p_delta["vy"], radar_p_delta["vz"]], axis=0) > config.minimum_dop_norm]
            static_delta = radar_p_delta[np.linalg.norm([radar_p_delta["vx"], radar_p_delta["vy"], radar_p_delta["vz"]], axis=0) <= config.minimum_dop_norm]

            try:
                dynamic_detections = build_objs(clusters=data_engine.get_clusters(not_static_delta), w_d=config.object_diameter_m, h_d=config.object_diameter_m, l_d=config.object_diameter_m)
                static_detections = build_objs(clusters=data_engine.get_clusters(static_delta), w_d=config.object_diameter_m, h_d=config.object_diameter_m, l_d=config.object_diameter_m)
            except Exception as e:
                print(e, frame_idx)
                continue

            ####### Register Frame Results #######
            # f_results_static = FrameResults(labels=labels, predictions=static_detections, projected_lidar=projected_lidar)
            # e_results_static.set_frame_results(frame_results=f_results_static)
            #
            # f_results_dynamic = FrameResults(labels=labels, predictions=dynamic_detections, projected_lidar=projected_lidar)
            # e_results_dynamic.set_frame_results(frame_results=f_results_dynamic)

            f_results = FrameResults(labels=lidar_clusters, predictions=delta_clusters)
            e_results.set_frame_results(frame_results=f_results)

            # e_results_dynamic.keep_frame_results(frame_results=f_results_dynamic, id=data_engine.get_id(idx=frame_idx))
            # e_results_static.keep_frame_results(frame_results=f_results_static, id=data_engine.get_id(idx=frame_idx))
            e_results.keep_frame_results(frame_results=f_results, id=data_engine.get_id(idx=frame_idx))

            ####### Plot #######
            if frame_idx % config.plt_every_x_frames != 0: continue
            fig = plt.figure(figsize=(25, 14), dpi=300)
            gs = gridspec.GridSpec(ncols=3, nrows=2)
            im_ax = fig.add_subplot(gs[:, -1:])
            im_ax.imshow(draw(rgb_img, label2d=labels, color=(0, 255, 0)))
            if "urban" in drive:
                im_ax.set_title("Classes | Pedestrian")
            if 'highway' in drive:
                im_ax.set_title("Classes | Car, Truck, Bus, Motorcycle, Pedestrian")
            im_ax.axis("off")


            def plt_radar_ax(loc, title, pc, boxes=True, show_dynamic=False, show_static=False, show_label=True):
                ax = fig.add_subplot(loc)
                ax.set_xlim(-40, 40)
                ax.set_ylim(0, 76)
                ax.set_xlabel("X(m)")
                ax.set_ylabel("Y(m)")
                if pc is not None:
                    for cls_id in np.unique(pc["cluster_id"]):
                        if np.isnan(cls_id):
                            continue
                        cluster_df = pc[pc["cluster_id"] == cls_id]
                        plt.scatter(cluster_df['x'], cluster_df['y'], color=cmap_clusters(int(cls_id)), s=config.scatter_size * 4)

                legends = []

                if show_dynamic:
                    plot_boxes(ax=ax, boxes=dynamic_detections, config=config, color_label=["dynamic-detections-com", dynamic_cluster_color], center_only=True)
                    legends.append(mpatches.Patch(color=dynamic_cluster_color, label='dynamic-detections-com'))
                if show_static:
                    plot_boxes(ax=ax, boxes=static_detections, config=config, color_label=["static-detections-com", static_cluster_color], center_only=True)
                    legends.append(mpatches.Patch(color=static_cluster_color, label='static-detections-com'))
                if show_label:
                    plot_boxes(ax=ax, boxes=labels, config=config, line_style="--", color_label=["label", "w"])
                    legends.append(mpatches.Patch(color='white', label='labels'))
                plt.legend(handles=legends, bbox_to_anchor=(0.5, -0.2 - 0.1 * len(legends)), loc='lower center')
                ax.set_title(title)
                return ax


            plt_radar_ax(loc=gs[0, 0], title="Delta | CFAR=10dB | colormap=cluster_id", pc=radar_p_delta, show_static=True, show_dynamic=True)
            plt_radar_ax(loc=gs[1, 1], title="Delta | CFAR=10dB | Velocity > " + str(config.minimum_dop_norm) + "m/s\ncolormap=cluster_id", pc=not_static_delta, show_dynamic=True)
            plt_radar_ax(loc=gs[1, 0], title="Delta | CFAR=10dB | Velocity <= " + str(config.minimum_dop_norm) + "m/s\ncolormap=cluster_id", pc=static_delta, show_static=True)

            lidar_ax = plt_radar_ax(loc=gs[0, 1], title="Lidar | colormap=height", pc=None)
            norms = projected_lidar_df["z"]
            cNorm = colors.Normalize(vmin=np.min(norms), vmax=np.max(norms))
            scat = plt.scatter(projected_lidar_df["x"], projected_lidar_df["y"], c=norms, cmap="turbo", s=config.scatter_size * 4, norm=cNorm)
            cb = plt.colorbar(scat, ax=lidar_ax, fraction=0.026, pad=0.04)
            plot_boxes(ax=lidar_ax, boxes=labels, config=config, line_style="--", color_label=["label", "w"])
            plot_boxes(ax=lidar_ax, boxes=dynamic_detections, config=config, color_label=["dynamic-detections-com", dynamic_cluster_color], center_only=True)
            plt.legend(handles=[mpatches.Patch(color=static_cluster_color, label='dynamic-detections-com'), mpatches.Patch(color='white', label='labels')], bbox_to_anchor=(0.5, -0.4),
                       loc='lower center')
            # plot_boxes(ax=lidar_ax, boxes=static_detections, config=config, color_label=["static-detections", "purple"], center_only=True)

            # o_grid_ax = fig.add_subplot(gs[1, -3:], projection="3d")
            fig.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0, hspace=0)

            # plt.tight_layout(pad=2)
            plt.subplots_adjust(top=0.92)
            plt.suptitle(drive + " | " + data_engine.get_id(idx=frame_idx) + "\n")

            # plt.savefig(save_fig_path_static)
            # plt.savefig(save_fig_path_dynamic)
            plt.savefig(save_fig_path)
            plt.close()

        except Exception as e:
            print("err")
            PrintException()
            continue

    # e_results_static.save_results()
    # e_results_dynamic.save_results()
    e_results.save_results(save_eval_results=False)
    # make_video_from_dir(df=data_engine.df, images_dir=images_path, suffix="_" + str(start) + "_" + str(end))
