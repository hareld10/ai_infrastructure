import torch
import os
from tqdm import tqdm
import cv2
import numpy as np
import matplotlib.pyplot as plt
from Evaluation.EvaluationBase import Evaluation
import glob
import os.path as osp
import json
import numpy as np
from PIL import Image
from tqdm import tqdm
import sys
sys.path.insert(0, "/workspace/AI/Harel/AI_Infrastructure/")


class EvaluationSegmentation(Evaluation):
    def __init__(self, num_classes=2):
        Evaluation.__init__(self)
        self.gt_id_set = set()
        self.num_classes = num_classes
    
    def per_class_iu(self, hist):
        ious = np.diag(hist) / (hist.sum(1) + hist.sum(0) - np.diag(hist))
        ious[np.isnan(ious)] = 0
        return ious
    
    def fast_hist(self, gt, prediction, n):
        k = (gt >= 0) & (gt < n)
        return np.bincount(
            n * gt[k].astype(int) + prediction[k], minlength=n ** 2).reshape(n, n)
    
    def find_all_png(self, folder):
        paths = []
        for root, dirs, files in os.walk(folder, topdown=True):
            paths.extend([osp.join(root, f)
                          for f in files if osp.splitext(f)[1] == '.png'])
        return paths
    
    def reset(self):
        self.ground_truths = []
        self.predictions = []
        self.gt_dict = None
        
    def add_gt_pred_pair(self, gt, pred):
        self.ground_truths.append(gt)
        self.predictions.append(pred)
        self.gt_id_set.update(np.unique(gt).tolist())
        
    def load_predictions(self):
        for key in sorted(self.gt_dict.keys()):
            gt_path = self.gt_dict[key]
            result_path = self.result_dict[key]
            gt = np.asarray(Image.open(gt_path, 'r'))
            self.ground_truths.append(gt)
            
            self.gt_id_set.update(np.unique(gt).tolist())
            prediction = np.asanyarray(Image.open(result_path, 'r'))
            self.predictions.append(prediction)
            
    def log(self):
        print("miou ", '{:.2f}'.format(self.miou))
        print(', '.join(['class ' + str(idx) + ': {:.2f}'.format(n) for idx, n in enumerate(list(self.ious))]))
              

        print("Results logged to ", self.out_path + "/eval_results.json")

        with open(self.out_path + "/eval_results.json", "w") as fp:
            fp.write("Mean IOU: " + '{:.2f}'.format(self.miou) + "\n")
            fp.write("Class : Score\n")
            for idx, score in enumerate(list(self.ious)):
                fp.write(str(idx) + " " + str('{:.2f}'.format(score)) + "\n")
                
    def evaluate_current(self, out_path):
        self.out_path = out_path
              
        hist = np.zeros((self.num_classes, self.num_classes))
        
        for gt, prediction in tqdm(zip(self.ground_truths , self.predictions)):
            hist += self.fast_hist(gt.flatten(), prediction.flatten(), self.num_classes)
        
        self.ious = self.per_class_iu(hist) * 100
        self.miou = np.mean(self.ious[list(self.gt_id_set)])
        
        self.log()
        self.reset()
        
        
    def evaluate_segmentation(self, gt_dir, result_dir, num_classes, key_length=17):
        self.gt_id_set = set()
        self.num_classes = num_classes
        self.out_path = gt_dir + "/../"
        self.gt_dict = dict([(osp.split(p)[1][:key_length], p) for p in self.find_all_png(gt_dir)])
        self.result_dict = dict([(osp.split(p)[1][:key_length], p) for p in self.find_all_png(result_dir)])
        self.result_gt_keys = set(self.gt_dict.keys()) & set(self.result_dict.keys())
        if len(self.result_gt_keys) != len(self.gt_dict):
            raise ValueError('Result folder only has {} of {} ground truth files.'.format(len(self.result_gt_keys), len(self.gt_dict)))
        print('Found', len(self.result_dict), 'results')
        print('Evaluating', len(self.gt_dict), 'results')
        
        # Load Predictions
        self.load_predictions()
        
        self.evaluate_current(out_path=self.out_path)
        
        self.log()
        
        self.reset()
