import torch
import os
from tqdm import tqdm
import cv2
import numpy as np
import matplotlib.pyplot as plt
from Evaluation.EvaluationBase import Evaluation
import glob
import json
from collections import defaultdict
from Inference.InferenceObjectDetection import InferenceObjectDetection

class EvaluationObjectDetection(Evaluation):
    def __init__(self, basePath="."):
        Evaluation.__init__(self)
        self.gt_label, self.gt_set, self.results_set = [], set(), set()
        self.results_label = []
        self.log_dir_path = basePath + "/Evaluation/"
        self.suffix=""

        os.makedirs(self.log_dir_path , exist_ok=True)
        self.inf_engine = InferenceObjectDetection()

    def group_by_key(self, detections, key):
        groups = defaultdict(list)
        for d in detections:
            groups[d[key]].append(d)
        return groups
    
    
    def get_ap(self, recalls, precisions):
        # correct AP calculation
        # first append sentinel values at the end
        recalls = np.concatenate(([0.], recalls, [1.]))
        precisions = np.concatenate(([0.], precisions, [0.]))

        # compute the precision envelope
        for i in range(precisions.size - 1, 0, -1):
            precisions[i - 1] = np.maximum(precisions[i - 1], precisions[i])

        # to calculate area under PR curve, look for points
        # where X axis (recall) changes value
        i = np.where(recalls[1:] != recalls[:-1])[0]

        # and sum (\Delta recall) * prec
        ap = np.sum((recalls[i + 1] - recalls[i]) * precisions[i + 1])
        return ap

    def cat_pc(self, gt, predictions, thresholds):
        """
        Implementation refers to https://github.com/rbgirshick/py-faster-rcnn
        """
        num_gts = len(gt)
        image_gts = self.group_by_key(gt, 'name')
        image_gt_boxes = {k: np.array([[float(z) for z in b['bbox']]
                                       for b in boxes])
                          for k, boxes in image_gts.items()}
        image_gt_checked = {k: np.zeros((len(boxes), len(thresholds)))
                            for k, boxes in image_gts.items()}
        predictions = sorted(predictions, key=lambda x: x['score'], reverse=True)

        # go down dets and mark TPs and FPs
        nd = len(predictions)
        tp = np.zeros((nd, len(thresholds)))
        fp = np.zeros((nd, len(thresholds)))
        for i, p in enumerate(predictions):
            box = p['bbox']
            ovmax = -np.inf
            jmax = -1
            try:
                gt_boxes = image_gt_boxes[p['name']]
                gt_checked = image_gt_checked[p['name']]
            except KeyError:
                gt_boxes = []
                gt_checked = None

            if len(gt_boxes) > 0:
                # compute overlaps
                # intersection
                ixmin = np.maximum(gt_boxes[:, 0], box[0])
                iymin = np.maximum(gt_boxes[:, 1], box[1])
                ixmax = np.minimum(gt_boxes[:, 2], box[2])
                iymax = np.minimum(gt_boxes[:, 3], box[3])
                iw = np.maximum(ixmax - ixmin + 1., 0.)
                ih = np.maximum(iymax - iymin + 1., 0.)
                inters = iw * ih

                # union
                uni = ((box[2] - box[0] + 1.) * (box[3] - box[1] + 1.) +
                       (gt_boxes[:, 2] - gt_boxes[:, 0] + 1.) *
                       (gt_boxes[:, 3] - gt_boxes[:, 1] + 1.) - inters)

                overlaps = inters / uni
                ovmax = np.max(overlaps)
                jmax = np.argmax(overlaps)

            for t, threshold in enumerate(thresholds):
                if ovmax > threshold:
                    if gt_checked[jmax, t] == 0:
                        tp[i, t] = 1.
                        gt_checked[jmax, t] = 1
                    else:
                        fp[i, t] = 1.
                else:
                    fp[i, t] = 1.

        # compute precision recall
        fp = np.cumsum(fp, axis=0)
        tp = np.cumsum(tp, axis=0)
        recalls = tp / float(num_gts)
        # avoid divide by zero in case the first detection matches a difficult
        # ground truth
        precisions = tp / np.maximum(tp + fp, np.finfo(np.float64).eps)
        ap = np.zeros(len(thresholds))
        for t in range(len(thresholds)):
            ap[t] = self.get_ap(recalls[:, t], precisions[:, t])

        return recalls, precisions, ap
    
    
    def _evaluate_detection(self, gt_path, result_path):
        gt = gt_path
        pred = result_path
        cat_gt = self.group_by_key(gt, 'category')
        cat_pred = self.group_by_key(pred, 'category')
        self.cat_list = sorted(cat_gt.keys())
        thresholds = [0.3]
        aps = np.zeros((len(thresholds), len(self.cat_list)))
        for i, cat in enumerate(self.cat_list):
            if cat in cat_pred:
                r, p, ap = self.cat_pc(cat_gt[cat], cat_pred[cat], thresholds)
                aps[:, i] = ap
        aps *= 100
        m_ap = np.mean(aps)
        self.mean, self.breakdown = m_ap, aps.flatten().tolist()

        print("mAP: {:.10f}".format(self.mean))
        for index, n in enumerate(self.breakdown):
            c = self.cat_list[index]
            print("For Class ", c, ': {:.10f}'.format(n))
            
            
    def change_box_order_np(self, boxes, order):
        """
        Change box order between (xmin, ymin, xmax, ymax) and (xcenter, ycenter, width, height).
        :param boxes: (tensor) bounding boxes, sized [N, 4]
        :param order: (str) either 'xyxy2xywh' or 'xywh2xyxy'.
        :return: (tensor) converted bounding boxes, size [N, 4]
        """
        assert order in ['xyxy2xywh', 'xywh2xyxy']
        a = boxes[:, :2]
        b = boxes[:, 2:]
        if order == 'xyxy2xywh':
            return np.concatenate([(a + b) / 2., b - a], 1)
        return np.concatenate([a - b / 2., a + b / 2.], 1)


    def fixLabels(self, labels, name, xywh=False):
        for l in labels:
            l['name'] = name
            if xywh:
                res = self.change_box_order_np(np.array([l['bbox']]), 'xywh2xyxy')
                l['bbox'] = res[0]
        return labels
    
    
    def findClasses(self, labels, name):
        s = set()
        for elem in labels:
            s.add(elem['category'])

#         print(name + " Classes: ")
#         print("Found ", s)
#         print("Total ", len(labels), " Objects ")
#         print("Example Label ", labels[0])
        return s
    
    def reset(self):
        self.gt_label, self.gt_set, self.results_set = [], set(), set()
        self.results_label = []
        
    
    def update_file_names(self):
        for index, label in enumerate(self.gt_label):
            name = label["name"]
            self.gt_set.add(name)
            
        for index, pred in enumerate(self.results_label):
            name = pred["name"]
            self.results_set.add(name)
        
        return
        
        
    def evaluate_current_od(self, log_dir_path):
        self.reset()
        self.log_dir_path = log_dir_path
        
        for index, (label, pred) in enumerate(zip(self.ground_truths, self.predictions)):
            name = str(index)
            
            # Add Ground Truth Labels
            self.gt_set.add(name)
            self.gt_label += label
            
            try:
                # Add Predictions
                results_set.add(name)
                self.results_label += pred
            except Exception as e:
                print("Error ", str(e))
                print(label)
        
        self.run()
        
    def log(self):
        l = self.log_dir_path + "/eval_results_" + str(self.suffix) + ".json"
        print("Logged :", l) 
        with open(l, "w") as fp:
            fp.write("Found " + str(len(self.gt_set)) + " In Ground Truth\n")
            fp.write("Found " + str(len(self.results_set)) +" In Labels\n")
            fp.write("Intersection Of Size " + str(len(self.intersection)) + "\n")
            fp.write("mAP: {:.10f}".format(self.mean) + "\n")
            for index, n in enumerate(self.breakdown):
                c = self.cat_list[index]
                fp.write("For Class "+ str(c) + ': {:.10f}'.format(n) + "\n")
            fp.write("End Of Results\n")

    def run(self, suffix):
        self.suffix=suffix
        self.findClasses(self.results_label, "Predictions Labels")
        # Found Classes
        self.findClasses(self.gt_label, "Ground Truth")
        
        self.update_file_names()
        
        # Comparison
        print("----------------------------")
        self.intersection = list(self.gt_set.intersection(self.results_set))
#         print("Found ", len(self.gt_set), " In Ground Truth")
#         print("Found ", len(self.results_set), " In Labels")
#         print("Intersection Of Size ", len(self.intersection))
#         print("----------------------------")

        self._evaluate_detection(self.gt_label, self.results_label)
        self.log()
       
    def add_gt_pred_pair(self, gt, pred):
        self.gt_label += gt
        if len(gt) > 0:
            self.gt_set.add(gt[0]["name"])

        self.results_label += pred
        if len(pred) > 0:
            self.results_set.add(pred[0]["name"])

    def add_raw_gt_pred(self, pred, batch, single_class):
        bb = self.inf_engine.get_bbs_raw(pred)
        
        bbs = []
        for i in range(bb.shape[0]):
            x1 = int(bb[i,0])
            y1 = int(bb[i,1])
            x2 = int(bb[i,2])
            y2 = int(bb[i,3])
            bbs.append(self.inf_engine.gen_label(x1, y1, x2, y2, classification=single_class, frameId=batch['frameID'][0], score=float(bb[i,4])))
        self.gt_label += bbs
        self.gt_set.add(batch['frameID'][0])
        self.results_set.add(batch['frameID'][0])

        bbs = []
        cx_vec     = batch['cx_vec'][0]
        cy_vec     = batch['cy_vec'][0]
        w_vec      = batch['w_vec'][0]
        h_vec      = batch['h_vec'][0]

        for i in range(len(cx_vec)):
            x1 = int(cx_vec[i] - w_vec[i]/2)
            x2 = int(cx_vec[i] + w_vec[i]/2)
            y1 = int(cy_vec[i] - h_vec[i]/2)
            y2 = int(cy_vec[i] + h_vec[i]/2)
            
            if not (x1 == 0 and y1 ==0 and x2 == -1 and y2 == -1):
                bbs.append(self.inf_engine.gen_label(x1, y1, x2, y2, classification=single_class, frameId=batch['frameID'][0]))

        self.results_label += bbs
        return True


    def evaluate_detection(self, pathForValidation, pathForResults, log_dir_path):
        # Prepare Ground Truth Labels
        self.reset()
        self.log_dir_path = log_dir_path
        
        numOfLabels = 0
        
        for label in sorted(glob.glob(pathForValidation + "*.json")):
            name = os.path.split(label)[1]
            self.gt_set.add(name)
            labels = json.load(open(label, 'r'))
            self.gt_label += labels

        print("------------------------")
        # Prepare Predictions

        lst2 = sorted(glob.glob(pathForResults + "*.json"))
        self.results_label = []

        try:
            for label in lst2:
                name = os.path.split(label)[1]
                self.results_set.add(name)
                labels = json.load(open(label, 'r'))
                self.results_label += labels
        except Exception as e:
            print("Error ", str(e))
            print(label)
            
        self.run()



            

