import argparse
import json
import os
from collections import defaultdict

import glob
import os.path as osp
import json

import numpy as np
from PIL import Image
from tqdm import tqdm

class Evaluation:
    def __init__(self):
        self.ground_truths = []
        self.predictions = []
        self.gt_dict = None
        self.out_path = ""
                
    def reset(self):
        self.ground_truths = []
        self.predictions = []
    
    def add_gt_pred_pair(self, gt, pred):
        self.ground_truths.append(gt)
        self.predictions.append(pred)
    
                 
    def evaluate_current(self, out_path):
        self.out_path = out_path
              
        hist = np.zeros((num_classes, num_classes))
        
        for gt, prediction in tqdm(zip(self.ground_truths , self.predictions)):
            hist += fast_hist(gt.flatten(), prediction.flatten(), num_classes)
        
        self.ious = self.per_class_iu(hist) * 100
        self.miou = np.mean(ious[list(self.gt_id_set)])
        
        self.log()
        self.reset()
        
        
    def evaluate_segmentation(self, gt_dir, result_dir, num_classes, key_length=17):
              
        self.out_path = gt_dir + "/../eval_results.json"
        self.gt_dict = dict([(osp.split(p)[1][:key_length], p) for p in find_all_png(gt_dir)])
        self.result_dict = dict([(osp.split(p)[1][:key_length], p) for p in find_all_png(result_dir)])
        self.result_gt_keys = set(gt_dict.keys()) & set(result_dict.keys())
        if len(self.result_gt_keys) != len(self.gt_dict):
            raise ValueError('Result folder only has {} of {} ground truth files.'.format(len(self.result_gt_keys), len(self.gt_dict)))
        print('Found', len(self.result_dict), 'results')
        print('Evaluating', len(self.gt_dict), 'results')
        
        # Load Predictions
        self.load_predictions()
        
        self.evaluate_current()
        
        self.log()
        
        self.reset()

        hist = np.zeros((num_classes, num_classes))
        i = 0
        self.gt_id_set = set()
        for key in sorted(self.gt_dict.keys()):
            gt_path = gt_dict[key]
            result_path = result_dict[key]
            gt = np.asarray(Image.open(gt_path, 'r'))
            self.gt_id_set.update(np.unique(gt).tolist())
            prediction = np.asanyarray(Image.open(result_path, 'r'))
            hist += fast_hist(gt.flatten(), prediction.flatten(), num_classes)
            i += 1
            if i % 100 == 0:
                print('Finished', i, self.per_class_iu(hist) * 100)

        ious = self.per_class_iu(hist) * 100
        miou = np.mean(ious[list(gt_id_set)])

        print('{:.2f}'.format(miou),
              ', '.join(['{:.2f}'.format(n) for n in list(ious)]))

        with open(gt_dir + "/../eval_results.json", "w") as fp:
            fp.write("Mean IOU: " + '{:.2f}'.format(miou) + "\n")
            fp.write("Class : Score\n")
            for idx, score in enumerate(list(ious)):
                fp.write(str(idx) + " " + str('{:.2f}'.format(score)) + "\n")

        
    
    def add_json_gt_label(self, gt, label):
        self.ground_truths.append(gt)
        self.predictions.append(pred)
        
    def evaluate_current_od(self):
        self.gt_label, self.gt_set, self.labels_set = [], set(), set()
        self.results_label = []
        
        
        for index, (label, pred) in enumerate(zip(self.ground_truths, self.predictions)):
            name = str(index)
            
            # Add Ground Truth Labels
            self.gt_set.add(name)
            label = self.fixLabels(label, name, xywh=True)
            self.gt_label += label
            
            try:
                # Add Ground Predictions
                labels_set.add(name)
                pred = fixLabels(pred, name)
                self.results_label += pred
            except Exception as e:
                print("Error ", str(e))
                print(label)
            
        
        self.findClasses(self.results_label, "Predictions Labels")

        # Comparison
        print("----------------------------")
        intersection = list(self.gt_set.intersection(labels_set))
        print("Found ", len(self.gt_set), " In Ground Truth")
        print("Found ", len(self.labels_set), " In Labels")
        print("Intersection Of Size ", len(intersection))
        print("----------------------------")

        self._evaluate_detection(self.gt_label, results_label)
        
            
        
    def evaluate_detection(self, pathForValidation, pathForResults):
        # Prepare Ground Truth Labels
        lst = sorted(glob.glob(pathForValidation + "*.json"))
        gt_label, gt_set, labels_set = [], set(), set()
        numOfLabels = None
        
        for label in lst:
            name = os.path.split(label)[1]
            gt_set.add(name)
            labels = json.load(open(label, 'r'))
            gt_label += labels

        # Found Classes
        self.findClasses(gt_label, "Ground Truth")

        print("------------------------")
        # Prepare Predictions

        lst2 = sorted(glob.glob(pathForResults + "*.json"))
        results_label = []

        try:
            for label in lst2:
                name = os.path.split(label)[1]
                labels_set.add(name)
                labels = json.load(open(label, 'r'))
                results_label += labels
        except Exception as e:
            print("Error ", str(e))
            print(label)


        self.findClasses(results_label, "Predictions Labels")

        # Comparison
        print("----------------------------")
        intersection = list(gt_set.intersection(labels_set))
        print("Found ", len(gt_set), " In Ground Truth")
        print("Found ", len(labels_set), " In Labels")
        print("Intersection Of Size ", len(intersection))
        print("----------------------------")

        self._evaluate_detection(gt_label, results_label)
