import numpy as np
import glob
import os
import cv2
import pandas as pd
from pprint import pprint
from sklearn.metrics import confusion_matrix
import ast

class ClassificationEvaluator:
    def __init__(self):
        pass

    def run(self, results):
        y_true = results["label"]
        y_pred = results[["p0", "p1", "p2"]].values.argmax(axis=1)
        print(y_pred)
        c_matrix = confusion_matrix(y_true, y_pred)
        print(c_matrix)
        pass


def main():
    df = pd.DataFrame()
    df["label"] = np.random.randint(0, 2, size=30)
    df["p0"] = np.random.random(30)
    df["p1"] = np.random.random(30)
    df["p2"] = np.random.random(30)
    e = ClassificationEvaluator()
    e.run(results=df)
    # print(df)
    pass

if __name__ == '__main__':
    main()
