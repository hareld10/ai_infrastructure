from datetime import datetime

import cv2
import pandas as pd
from matplotlib import gridspec, lines
from sklearn.cluster import KMeans
import os
import glob
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib import cm
import matplotlib
from scipy import stats
from tqdm import tqdm
import pathlib
import pickle
import os
from datetime import datetime
from wiseTypes.Label3D import Label3D
from Evaluation.names import *
import time


def get_iou_key(key, thresh):
    return key + "_iou_" + str(thresh)


def draw(im, label2d, color=None,
         suffix="", font_size=0.9, thickness=2,
         title=True,
         y_delta=15,
         bb_thickness=2):

    for label in label2d:
        obj = suffix + label.cls
        # print(obj)
        score = label.score
        x1, y1, x2, y2 = label.get_coords()
        final_color = label.obj.color

        if color != None:
            final_color = color

        # print("blas", int(x1), int(y1), int(x2), int(y2))
        cv2.rectangle(im, (int(x1), int(y1)), (int(x2), int(y2)), color=final_color, thickness=bb_thickness)
        if title:
            cv2.putText(im, '{}, {:.2f}, {} {}'.format(obj, round(score, 2), str(label.instance_id), str(label.annotate)),
                        (x1, y1 + y_delta), cv2.FONT_HERSHEY_SIMPLEX, font_size,
                        final_color, thickness)

    return im


def get_iou(a, b, epsilon=1e-5):
    """ Given two boxes `a` and `b` defined as a list of four numbers:
            [x1,y1,x2,y2]
        where:
            x1,y1 represent the upper left corner
            x2,y2 represent the lower right corner
        It returns the Intersect of Union score for these two boxes.

    Args:
        a:          (list of 4 numbers) [x1,y1,x2,y2]
        b:          (list of 4 numbers) [x1,y1,x2,y2]
        epsilon:    (float) Small value to prevent division by zero

    Returns:
        (float) The Intersect of Union score.
    """
    # COORDINATES OF THE INTERSECTION BOX
    x1 = max(a[0], b[0])
    y1 = max(a[1], b[1])
    x2 = min(a[2], b[2])
    y2 = min(a[3], b[3])

    # AREA OF OVERLAP - Area where the boxes intersect
    width = (x2 - x1)
    height = (y2 - y1)
    # handle case where there is NO overlap
    if (width < 0) or (height < 0):
        # print("Got", a, b)
        return 0.0
    area_overlap = width * height

    # COMBINED AREA
    area_a = (a[2] - a[0]) * (a[3] - a[1])
    area_b = (b[2] - b[0]) * (b[3] - b[1])
    area_combined = area_a + area_b - area_overlap

    # RATIO OF AREA OF OVERLAP OVER COMBINED AREA
    iou = area_overlap / (area_combined + epsilon)
    return iou


def get_bb_bev(config, bb):
    cx, cy = bb.tx, bb.ty
    dx, dy = bb.width, bb.length
    heading = bb.heading + np.deg2rad(config.spatial_calibration_params["yaw-radar-lidar"])
    c = np.cos(heading)
    s = np.sin(heading)
    rot_mat = [[c, -s], [s, c]]

    center = [cx, cy]
    p1 = center + np.dot(rot_mat, [dy / 2, dx / 2])
    p2 = center + np.dot(rot_mat, [dy / 2, -dx / 2])
    p3 = center + np.dot(rot_mat, [-dy / 2, -dx / 2])
    p4 = center + np.dot(rot_mat, [-dy / 2, dx / 2])

    heading_1 = center + np.dot(rot_mat, [dy / 2, dx / 2])
    heading_2 = center + np.dot(rot_mat, [-dy / 2, dx / 2])

    return p1, p2, p3, p4, heading_1, heading_2


def get_cmap(num_colors, name='hsv'):
    '''Returns a function that maps each index in 0, 1, ..., n-1 to a distinct
    RGB color; the keyword argument name must be a standard mpl colormap name.'''
    return plt.cm.get_cmap(name, num_colors)


cmap_clusters = get_cmap(20)


def plot_boxes(ax, boxes, color_label, config, line_style="-", center_only=False):
    for bb in boxes:
        cx, cy = bb.tx, bb.ty

        if center_only:
            ax.scatter([cx], [cy], s=config.plot_center_size, c=color_label[1], marker="x", linewidths=1.2, facecolors='none', alpha=0.9)
            continue

        p1, p2, p3, p4, heading_1, heading_2 = get_bb_bev(config, bb)

        pts = [p1, p2, p3, p4]

        pairs = [[0, 1], [1, 2], [2, 3], [3, 0]]
        for ind in pairs:
            plt.plot([pts[ind[0]][0], pts[ind[1]][0]], [pts[ind[0]][1], pts[ind[1]][1]], line_style, c=color_label[1])

        # Plot heading
        l1 = lines.Line2D((cx, p1[0]), (cy, p1[1]), color=color_label[1], linewidth=config.line_width, linestyle=line_style)
        ax.add_line(l1)
        l1 = lines.Line2D((cx, p4[0]), (cy, p4[1]), color=color_label[1], linewidth=config.line_width, label=color_label[0], linestyle=line_style)
        ax.add_line(l1)

        # Plot score
        # ax.annotate(str(np.round(bb.score, 2)), (np.max([p1[0], p2[0], p3[0], p4[0]]), np.max([p1[1], p2[1], p3[1], p4[1]])), color=color_label[1], fontsize=12)
        ax.annotate(str(bb.instance_id), (np.max([p1[0], p2[0], p3[0], p4[0]]), np.max([p1[1], p2[1], p3[1], p4[1]])), color=color_label[1], fontsize=12)
    return


def get_cur_working_directory(model_name, data_engine_name, task, metric, suffix="", force=True):
    exp_name = str(model_name) + "_" + str(data_engine_name) + str(suffix)
    now = datetime.now()
    data_dir = str(now.year)[-2:] + "{0:0=2d}".format(now.month) + "{0:0=2d}".format(now.day)
    cur_working_dir = "/workspace/HDD/EvaluationResults/" + str(data_dir) + "/" + task + "/" + exp_name + "/" + str(metric) + "/"
    if not force and os.path.exists(cur_working_dir):
        return False, False

    os.makedirs(cur_working_dir, exist_ok=True)
    images_path = cur_working_dir + "/images/"
    os.makedirs(images_path, exist_ok=True)
    return cur_working_dir, images_path


def ensure_same_shape(gt, pred):
    if gt.shape != pred.shape:
        return cv2.resize(pred, (gt.shape[1], gt.shape[0]))
    return pred


def build_objs(clusters, w_d, h_d, l_d):
    ret_labels = []
    for cluster_idx, cluster in clusters.items():
        tx, ty, tz = cluster.get_3d_center()
        score = cluster.get_score()
        label = Label3D(cls="unclassified", score=score, tx=tx, ty=ty, tz=tz, heading=0, width=w_d, height=h_d, length=l_d, instance_id=cluster_idx, associate_cluster=cluster)
        ret_labels.append(label)
    return ret_labels


def make_video_from_dir(images_dir, df, suffix="_" + str(time.time()).replace(".", ""), save_path=None, frame_rate=7, key="timestamp_radar"):
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')

    if save_path is None:
        save_path = images_dir + "/../"
    video_name = save_path + "/video" + suffix + ".mp4"

    first = True
    video = None
    for idx in tqdm(range(len(df))):  # len(config.df))):
        if first:
            frame = cv2.imread(images_dir + "/" + str(df.loc[idx, key]) + '.png')
            if frame is None:
                print("Video | Frame is None")
                continue
            height, width, layers = frame.shape
            video = cv2.VideoWriter(video_name, fourcc, frame_rate, (width, height))
            first = False

        img_path = images_dir + "/" + str(df.loc[idx, key]) + '.png'

        if os.path.exists(img_path) and video is not None:
            video.write(cv2.imread(img_path))

    cv2.destroyAllWindows()
    if video is not None:
        video.release()


def get_colors(n):
    colors = ["red", "green", "blue", "olive", "orange", "cyan", "pink", "purple", "brown"]
    return colors[:n]


def calculate_df(df):
    df.loc[PRECISION] = df.loc[TP, :] / (df.loc[TP, :] + df.loc[FP, :])
    df.loc[RECALL] = df.loc[TP, :] / (df.loc[TP, :] + df.loc[FN, :])
    df.loc[F1_SCORE] = (2 * df.loc[PRECISION] * df.loc[RECALL]) / (df.loc[PRECISION] + df.loc[RECALL])
    df.loc[FALSE_DISCOVERY_RATE] = df.loc[FP, :] / (df.loc[TP, :] + df.loc[FP, :])
    df.loc[IOU] = df.loc[TP, :] / (df.loc[TP, :] + df.loc[FP, :] + df.loc[FN, :])
    df.loc[FALSE_RATE] = df.loc[FP, :] / (df.loc[TP, :] + df.loc[FP, :])

    try:
        df.loc[TRUE_RATE] = df.loc[TP, :] / (df.loc[TP, :] + df.loc[TN, :])
    except Exception as e:
        pass
    # df.fillna(1, inplace=True)
    return df
