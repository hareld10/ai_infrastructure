import glob
from math import sqrt
import numpy as np
import cv2
import matplotlib.pyplot as plt
import os
from tqdm import tqdm

from Evaluation.evaluation_utils import get_iou

output_shape = (300, 300, 3)


def evaluate_frame_centers(predictions_centers, gt_boxes, preds_as_coords=False, gt_boxes_as_centers=False, iou=0):
    """
    :param predictions_centers: (N, 4) - cx, cy, w, h
    :param gt_boxes: (M, 4) - x1, y1, x2, y2
    :return:
    """
    gt_boxes_flags = np.zeros((gt_boxes.shape[0]))  # mark if box has been predicted

    tp, fp, fn = 0, 0, 0
    for idx, center in enumerate(predictions_centers):
        cx, cy, w, h = center
        if preds_as_coords:
            cx, cy, w, h = get_center(center)

        match = False
        for gt_idx, bbox in enumerate(gt_boxes):
            x1, y1, x2, y2 = bbox
            if gt_boxes_as_centers:
                x1, y1, x2, y2 = get_coords(bbox)
            if iou:
                iou_results = get_iou(a=[x1, y1, x2, y2], b=[cx - w, cy - h, cx + w, cy + h])
                if iou_results < iou:
                    continue
            elif not (x1 <= cx <= x2 and y1 <= cy <= y2):
                continue

            if not gt_boxes_flags[gt_idx]:
                gt_boxes_flags[gt_idx] = True
                match = True
                tp += 1
                break
        if not match:
            fp += 1

    # fn = np.count_nonzero(gt_boxes_flags == 0)
    fn = (gt_boxes_flags == 0).sum()
    return {"tp": tp, "fp": fp, "fn": fn}


def evaluate_frame_3d_centers(predictions_centers, gt_boxes, preds_as_coords=False, gt_boxes_as_centers=False):
    """
    :param predictions_centers: (N, 4) - cx, cy, w, h
    :param gt_boxes: (M, 4) - x1, y1, x2, y2
    :return:
    """
    gt_boxes_flags = np.zeros((gt_boxes.shape[0]))  # mark if box has been predicted

    tp, fp, fn = 0, 0, 0
    for idx, center in enumerate(predictions_centers):
        cx, cy, w, h = center
        if preds_as_coords:
            cx, cy, w, h = get_center(center)

        match = False
        for gt_idx, bbox in enumerate(gt_boxes):
            x1, y1, x2, y2 = bbox
            if gt_boxes_as_centers:
                x1, y1, x2, y2 = get_coords(bbox)
            elif not (x1 <= cx <= x2 and y1 <= cy <= y2):
                continue

            if not gt_boxes_flags[gt_idx]:
                gt_boxes_flags[gt_idx] = True
                match = True
                tp += 1
                break
        if not match:
            fp += 1

    # fn = np.count_nonzero(gt_boxes_flags == 0)
    fn = (gt_boxes_flags == 0).sum()
    return {"tp": tp, "fp": fp, "fn": fn}


def get_ap(recalls, precisions):
    # correct AP calculation
    # first append sentinel values at the end
    recalls = np.concatenate(([0.], recalls, [1.]))
    precisions = np.concatenate(([0.], precisions, [0.]))

    # compute the precision envelope
    for i in range(precisions.size - 1, 0, -1):
        precisions[i - 1] = np.maximum(precisions[i - 1], precisions[i])

    # to calculate area under PR curve, look for points
    # where X axis (recall) changes value
    i = np.where(recalls[1:] != recalls[:-1])[0]

    # and sum (\Delta recall) * prec
    ap = np.sum((recalls[i + 1] - recalls[i]) * precisions[i + 1])
    return ap


def evaluate_frame_ious(predictions_centers, gt_boxes, iou_thresholds=None, gt_boxes_as_centers=False):
    """

    :param predictions_centers:
    :param gt_boxes:
    :return: AP, width_ious, heights_ious
    """
    if iou_thresholds is None:
        iou_thresholds = [0.1]
    num_gts = len(gt_boxes)

    result_matrix = np.zeros((len(gt_boxes), len(iou_thresholds)))

    width_ious = np.zeros((predictions_centers.shape[0]))
    height_ious = np.zeros((predictions_centers.shape[0]))

    # go down dets and mark TPs and FPs
    nd = len(predictions_centers)
    tp = np.zeros((nd, len(iou_thresholds)))
    fp = np.zeros((nd, len(iou_thresholds)))

    if gt_boxes_as_centers:
        for idx, elem in enumerate(gt_boxes):
            gt_boxes[idx] = get_coords(elem)

    for i, bbox in enumerate(predictions_centers):
        cx, cy, w, h = bbox
        x1 = cx - w
        x2 = cx + w
        y1 = cy - h
        y2 = cy + h
        box = [x1, y1, x2, y2]

        ovmax = -np.inf
        jmax = -1

        if len(gt_boxes) > 0:
            # compute overlaps
            # intersection
            ixmin = np.maximum(gt_boxes[:, 0], box[0])
            iymin = np.maximum(gt_boxes[:, 1], box[1])
            ixmax = np.minimum(gt_boxes[:, 2], box[2])
            iymax = np.minimum(gt_boxes[:, 3], box[3])
            iw = np.maximum(ixmax - ixmin + 1., 0.)
            ih = np.maximum(iymax - iymin + 1., 0.)
            inters = iw * ih

            # union
            uni = ((box[2] - box[0] + 1.) * (box[3] - box[1] + 1.) +
                   (gt_boxes[:, 2] - gt_boxes[:, 0] + 1.) *
                   (gt_boxes[:, 3] - gt_boxes[:, 1] + 1.) - inters)

            overlaps = inters / uni
            ovmax = np.max(overlaps)
            jmax = np.argmax(overlaps)

            # Width iou
            uni_w = ((box[2] - box[0] + 1.) + (gt_boxes[:, 2] - gt_boxes[:, 0] + 1.) - iw)
            overlaps_w = iw / uni_w
            ovmax_w = np.max(overlaps_w)
            width_ious[i] = ovmax_w

            # Height iou
            uni_h = ((box[3] - box[1] + 1.) + (gt_boxes[:, 3] - gt_boxes[:, 1] + 1.) - ih)
            overlaps_h = ih / uni_h
            ovmax_h = np.max(overlaps_h)
            height_ious[i] = ovmax_h

        for t, threshold in enumerate(iou_thresholds):
            if ovmax > threshold:
                if result_matrix[jmax, t] == 0:
                    tp[i, t] = 1.
                    result_matrix[jmax, t] = 1
                else:
                    fp[i, t] = 1.
            else:
                fp[i, t] = 1.

    fp = np.cumsum(fp, axis=0)
    tp = np.cumsum(tp, axis=0)
    recalls = tp / float(num_gts)
    # avoid divide by zero in case the first detection matches a difficult
    # ground truth
    precisions = tp / np.maximum(tp + fp, np.finfo(np.float64).eps)
    ap = np.zeros(len(iou_thresholds))
    for t in range(len(iou_thresholds)):
        ap[t] = get_ap(recalls[:, t], precisions[:, t])

    ap = np.nan_to_num(ap, 0)
    return {"ap": ap.tolist(), "height_ious": height_ious.mean().tolist(), "width_ious": width_ious.mean().tolist(), "recalls": recalls, "precisions": precisions}


def get_center(bbox):
    x1, y1, x2, y2 = bbox
    cx = (x2 + x1) / 2
    cy = (y1 + y2) / 2
    w = (x2 - x1) / 2
    h = (y2 - y1) / 2
    return cx, cy, w, h


def get_coords(bbox):
    cx, cy, w, h = bbox

    x1 = (cx - w)
    y1 = (cy - h)
    x2 = (cx + w)
    y2 = (cy + h)

    return x1, y1, x2, y2


def get_random_bbox(num=1, centers=False):
    ret = []
    for _ in range(num):
        top_x = output_shape[0]
        top_y = output_shape[1]

        x1 = np.random.randint(1, top_x)
        length = np.minimum(np.random.randint(10, 100), top_x)
        x2 = x1 + length

        y1 = np.random.randint(1, top_y)
        length = np.minimum(np.random.randint(10, 100), top_y)
        y2 = y1 + length

        if centers:
            ret.append(get_center([x1, y1, x2, y2]))
        else:
            ret.append([x1, y1, x2, y2])
    return np.array(ret)


def plot(test_batch):
    plt.figure(figsize=(50, 25))
    for idx, (labels, preds) in enumerate(test_batch):
        plt.subplot(int(sqrt(len(test_batch))) + 1, int(sqrt(len(test_batch))) + 1, idx + 1)
        img = np.zeros(output_shape, dtype=np.uint8)

        label_color = (0, 255, 0)
        pred_color = (255, 0, 0)
        for label in labels:
            x1, y1, x2, y2 = label
            img = cv2.rectangle(img, (x1, y1), (x2, y2), label_color, 1)

        for pred in preds:
            cx, cy, w, h = pred

            x1 = cx - w
            x2 = cx + w
            y1 = cy - h
            y2 = cy + h
            cv2.circle(img, (cx, cy), radius=3, color=(255, 0, 0), thickness=3)
            cv2.rectangle(img, (x1, y1), (x2, y2), (255, 0, 0), 1)

        title = "Label color" + str(label_color) + ", Pred color " + str(pred_color) + "\n"
        title += str(evaluate_frame_centers(predictions_centers=preds, gt_boxes=labels)) + "\n"
        res = evaluate_frame_ious(predictions_centers=preds, gt_boxes=labels)
        title += str("AP " + str(res["ap"])) + "\n"
        title += str("width_ious " + str(res["width_ious"])) + "\n"
        title += str("height_ious " + str(res["height_ious"])) + "\n"

        plt.imshow(img)
        plt.title(title)
    plt.savefig("./bbox_plot.png")


def get_test_batch():
    label_test = np.array([[80, 80, 160, 160], [200, 200, 220, 220]])
    pred_test = np.array([[100, 100, 50, 50], [190, 190, 10, 10]])

    label_test1 = np.array([[80, 80, 160, 160], [200, 200, 220, 220]])
    pred_test1 = np.array([[100, 100, 50, 50]])

    bbox = [50, 150, 100, 200]
    label_test2 = np.array([bbox])
    pred_test2 = np.array([get_center(bbox)])

    bbox = [100, 150, 150, 200]
    label_test3 = np.array([bbox])
    pred_test3 = np.array([])

    bbox = [100, 150, 150, 200]
    label_test4 = np.array([])
    pred_test4 = np.array([get_center(bbox)])

    t_batch = [[label_test, pred_test], [label_test1, pred_test1], [label_test2, pred_test2],
               [label_test3, pred_test3], [label_test4, pred_test4], ]

    for i in range(10):
        num_boxes_label = np.random.randint(1, 40)
        num_boxes_pred = np.random.randint(1, 40)

        cur_label = get_random_bbox(num_boxes_label)
        cur_pred = get_random_bbox(num_boxes_pred, centers=True)
        t_batch.append(np.array([cur_label, cur_pred]))
    return t_batch


if __name__ == "__main__":
    plot(get_test_batch())
