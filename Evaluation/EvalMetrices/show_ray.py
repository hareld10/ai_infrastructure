import glob
from math import sqrt
import numpy as np
import cv2
import matplotlib.pyplot as plt
import os
from tqdm import tqdm

X_IDX = 0
Y_IDX = 1
THETA_IDX = 2
RANGE_IDX = 3
eps=0.0001


def calculate_l1(label_batch_input, pred_batch_input):
    """
    :param point:
    :param label_batch_input:
    :param pred_batch_input:
    :param gt_batch: (B,N,3,3)
    :param pred_batch:
    :return:
    """

    return np.sum(np.linalg.norm(label_batch_input[..., RANGE_IDX] - pred_batch_input[..., RANGE_IDX], axis=-1), axis=-1)


def calculate_diff(label_batch_input, pred_batch_input, point):
    return label_batch_input[:, :, point, RANGE_IDX] - pred_batch_input[:, :, point, RANGE_IDX]


def createLineIterator(P1, P2, img):
    """
    Produces and array that consists of the coordinates and intensities of each pixel in a line between two points
    Parameters:
        -P1: a numpy array that consists of the coordinate of the first point (x,y)
        -P2: a numpy array that consists of the coordinate of the second point (x,y)
        -img: the image being processed
    Returns:
        -it: a numpy array that consists of the coordinates and intensities of each pixel in the radii (shape: [numPixels, 3], row = [x,y,intensity])
    """
    # define local variables for readability
    imageH = img.shape[0]
    imageW = img.shape[1]
    P1X = P1[0]
    P1Y = P1[1]
    P2X = P2[0]
    P2Y = P2[1]

    # difference and absolute difference between points
    # used to calculate slope and relative location between points
    dX = P2X - P1X
    dY = P2Y - P1Y
    dXa = np.abs(dX)
    dYa = np.abs(dY)

    # predefine numpy array for output based on distance between points
    itbuffer = np.empty(shape=(np.maximum(dYa, dXa), 3), dtype=np.float32)
    itbuffer.fill(np.nan)

    # Obtain coordinates along the line using a form of Bresenham's algorithm
    negY = P1Y > P2Y
    negX = P1X > P2X
    if P1X == P2X:  # vertical line segment
        itbuffer[:, 0] = P1X
        if negY:
            itbuffer[:, 1] = np.arange(P1Y - 1, P1Y - dYa - 1, -1)
        else:
            itbuffer[:, 1] = np.arange(P1Y + 1, P1Y + dYa + 1)
    elif P1Y == P2Y:  # horizontal line segment
        itbuffer[:, 1] = P1Y
        if negX:
            itbuffer[:, 0] = np.arange(P1X - 1, P1X - dXa - 1, -1)
        else:
            itbuffer[:, 0] = np.arange(P1X + 1, P1X + dXa + 1)
    else:  # diagonal line segment
        steepSlope = dYa > dXa
        if steepSlope:
            slope = dX.astype(np.float32) / dY.astype(np.float32)
            if negY:
                itbuffer[:, 1] = np.arange(P1Y - 1, P1Y - dYa - 1, -1)
            else:
                itbuffer[:, 1] = np.arange(P1Y + 1, P1Y + dYa + 1)
            itbuffer[:, 0] = (slope * (itbuffer[:, 1] - P1Y)).astype(np.int) + P1X
        else:
            slope = dY.astype(np.float32) / dX.astype(np.float32)
            if negX:
                itbuffer[:, 0] = np.arange(P1X - 1, P1X - dXa - 1, -1)
            else:
                itbuffer[:, 0] = np.arange(P1X + 1, P1X + dXa + 1)
            itbuffer[:, 1] = (slope * (itbuffer[:, 0] - P1X)).astype(np.int) + P1Y

    # Remove points outside of image
    colX = itbuffer[:, 0]
    colY = itbuffer[:, 1]
    itbuffer = itbuffer[(colX >= 0) & (colY >= 0) & (colX < imageW) & (colY < imageH)]

    # Get intensities from img ndarray
    itbuffer[:, 2] = img[itbuffer[:, 1].astype(np.uint), itbuffer[:, 0].astype(np.uint)]

    return itbuffer


def cut_thresh_and_engine(array, thresh):
    cut_w_s, cut_w_e = 500, 1600
    cut_h_s, cut_h_e = 1100, None
    array[array < thresh] = np.inf
    array[cut_h_s:cut_h_e, cut_w_s:cut_w_e] = 0
    return array


def get_points(theta, array, length=4000):
    theta = theta * np.pi / 180
    center = [array.shape[1] // 2, array.shape[0] - 1]
    x1, y1 = int(center[0]), int(center[1])
    x2 = int(x1 + length * np.cos(theta))
    y2 = int(y1 + length * np.sin(theta))

    line_iter = createLineIterator(np.array([x1, y1]), np.array([x2, y2]), array)
    keep = [i for i, n in enumerate(line_iter[:, 2]) if i != 0 and (
            (np.isinf(n) and not np.isinf(line_iter[i - 1, 2])) or (np.isinf(line_iter[i - 1, 2]) and not np.isinf(n)))]

    return keep, line_iter, center


def get_theta_vec(d_theta, fov, img_fov=90):
    return np.linspace(0 + np.abs(img_fov - fov) // 2, 180 - np.abs(img_fov - fov) // 2, int(fov / d_theta)) - 180


def ray_batch(batch, d_theta, fov, thresh):
    """

    :param fov:
    :param d_theta:
    :param batch:
    :param thresh:
    :return: (B, N, 3, 3)
    """
    # calculate theta vec
    theta_vec = get_theta_vec(d_theta=d_theta, fov=fov)
    ret_arr = np.zeros((batch.shape[0], theta_vec.shape[0], 3, 4))
    for batch_idx, cur_input in enumerate(batch):
        label = cut_thresh_and_engine(cur_input, thresh)
        points_array = np.zeros((theta_vec.shape[0], 3, 4))
        for theta_idx, theta in enumerate(theta_vec):
            # get the points along the ray
            keep, line_iter, center = get_points(theta=theta, array=label)
            if len(keep) > 0:
                points = line_iter[keep[:3]]
                points_array[theta_idx][:points.shape[0], :2] = points[:, :2]
                points_array[theta_idx][:points.shape[0], 2] = theta + 180
                points_array[theta_idx][:points.shape[0], 3] = np.linalg.norm(points[:, :2] - center)

        ret_arr[batch_idx] = points_array
    return ret_arr


def draw_points(batch, img, color=(0, 165, 255)):
    for ray_points in batch:
        for point in ray_points:
            x1, y1 = point[:2]
            cv2.circle(img, (int(x1), int(y1)), 10, thickness=2, color=color)


def draw_rays(batch, img, theta_vec, color=(0, 165, 255), length=4000):
    """
    :param length:
    :param color:
    :param batch: (N,3, 3)
    :param img:
    :param theta_vec:
    :return:
    """
    for idx, points in enumerate(batch):
        center = [img.shape[1] // 2, img.shape[0] - 1]

        x1, y1 = None, None
        for point in points:
            if point[:2].sum() != 0:
                x1, y1 = point[:2]

        theta = theta_vec[idx] * np.pi / 180
        x1_line, y1_line = int(center[0]), int(center[1])
        x2_line = int(x1_line + length * np.cos(theta))
        y2_line = int(y1_line + length * np.sin(theta))

        if x1 is not None and y1 is not None:
            x2_line, y2_line = x1, y1

        cv2.line(img, (int(x1_line), int(y1_line)), (int(x2_line), int(y2_line)), color, 7)

    return


def visualize_batch(label_batch, pred_batch, rgb_batch, d_theta, fov, thresh):
    plt.cla()
    plt.figure(figsize=(60, 30))
    theta_vec = get_theta_vec(d_theta=d_theta, fov=fov)
    label_batch_results = ray_batch(batch=label_batch, d_theta=d_theta, fov=fov, thresh=thresh)
    pred_batch_batch_results = ray_batch(batch=pred_batch, d_theta=d_theta, fov=fov, thresh=thresh)

    print("label_batch_results", label_batch_results.shape, "pred_batch_batch_results", pred_batch_batch_results.shape)

    for idx, (label_results, pred_results) in enumerate(zip(label_batch_results, pred_batch_batch_results)):
        plt.subplot(int(sqrt(label_batch_results.shape[0])), int(sqrt(label_batch_results.shape[0])), idx + 1)

        # plot circles
        draw_points(pred_results, rgb_batch[idx], color=(255, 0, 0))
        draw_points(label_results, rgb_batch[idx], color=(0, 255, 0))

        # plot rays
        draw_rays(batch=label_results, img=rgb_batch[idx], theta_vec=theta_vec)
        draw_rays(batch=pred_results, img=rgb_batch[idx], theta_vec=theta_vec)

        # plot image
        plt.imshow(rgb_batch[idx])
        plt.imshow(label_batch[idx], cmap=plt.cm.jet_r, alpha=0.4)
        plt.imshow(pred_batch[idx], cmap=plt.cm.jet, alpha=0.4)

    plt.tight_layout()
    plt.savefig("./batch_visuzlize")
    plt.close()
    return


def plot_error_vec(vec, theta_vec, img_fov=90, title="results"):
    plt.cla()
    plt.figure(figsize=(20, 10))
    theta_vec = theta_vec + 180 - img_fov
    for idx, cur_input in enumerate(vec):
        plt.subplot(int(sqrt(vec.shape[0])), int(sqrt(vec.shape[0])), idx + 1)
        plt.bar(theta_vec, cur_input)
        plt.title(title)
        plt.xlabel("Theta")
        plt.ylabel("Error")
    plt.savefig("./" + str(title) + ".png")
    plt.close()


def get_raw_batch(bs=2, rand=False):
    label = np.load("./examples/img1.npz")["arr_0"]
    img = cv2.cvtColor(cv2.imread("./examples/img1.png"), cv2.COLOR_BGR2RGB)
    label = cv2.resize(label, (img.shape[1], img.shape[0]))
    ret_batch = np.zeros((bs, label.shape[0], label.shape[1]))

    for i in range(bs):
        if rand:
            axs = np.random.randint(0, 2)
            roll = np.random.randint(40, 120)
            ret_batch[i] = np.roll(label.copy(), shift=roll, axis=-1)
        else:
            ret_batch[i] = label.copy()
    return ret_batch


def get_raw_rgb_batch(bs=2):
    img = cv2.cvtColor(cv2.imread("./examples/img1.png"), cv2.COLOR_BGR2RGB)
    return np.stack([img] * bs)


d_theta_test = 5
fov_test = 90
thresh_test = 0.6

# Prepare input
pred_batch_test = get_raw_batch(4, rand=True)
label_batch_test = get_raw_batch(4)
rgb_batch_test = get_raw_rgb_batch(4)
print("pred_batch", pred_batch_test.shape, "label_batch", label_batch_test.shape, "rgb_batch_test", rgb_batch_test.shape)

#Visualize Batch
visualize_batch(label_batch=label_batch_test, pred_batch=pred_batch_test, rgb_batch=rgb_batch_test, d_theta=d_theta_test, fov=fov_test, thresh=thresh_test)

# calculations
pred_results_batch = ray_batch(pred_batch_test, d_theta=d_theta_test, fov=fov_test, thresh=thresh_test)
label_results_batch = ray_batch(label_batch_test, d_theta=d_theta_test, fov=fov_test, thresh=thresh_test)


l1 = calculate_l1(label_results_batch, pred_results_batch)
print(l1.shape)
plot_error_vec(vec=l1, theta_vec=get_theta_vec(d_theta=d_theta_test, fov=fov_test), title="l1_err")


for n_points in range(3):
    diff = calculate_diff(label_results_batch, pred_results_batch, point=n_points)
    print("#points", n_points, "diff shape ", diff.shape, "diff mean", diff.mean())
    plot_error_vec(vec=diff, theta_vec=get_theta_vec(d_theta=d_theta_test, fov=fov_test), title="diff_hit_" + str(n_points))