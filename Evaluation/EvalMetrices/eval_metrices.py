import glob
from math import sqrt

import torch.nn as nn
import torch
import torch.nn.functional as F
import numpy as np
import cv2
import matplotlib.pyplot as plt
import os

from tqdm import tqdm


class Images:
    def __init__(self):
        src2 = ["//media/amper/1a7755a0-b563-46a9-b204-ce56bededfb910/64mWF/*/camera_rgb/*.png"]
        bank = []
        for i in src2:
            bank += glob.glob(i)
        self.bank = np.random.permutation(bank)
        print("len_bank", len(bank))
        self.idx = 0
        self.cut_w_s, self.cut_w_e = 500, 1600
        self.cut_h_s, self.cut_h_e = 1100, None
        self.label_thresh = 0.6

        return

    def get_image(self):
        while True:

            path = self.bank[self.idx]
            im = cv2.cvtColor(cv2.imread(path), cv2.COLOR_BGR2RGB)

            ts = os.path.split(path)[-1].split(".")[0].split("_")[-1]
            r_seg = os.path.split(path)[0] + "/../camera_road_seg/camera_road_seg_" + ts + ".npz"

            self.idx += 1
            if os.path.exists(r_seg):
                seg_map = np.load(r_seg)["arr_0"]
                seg_map = cv2.resize(seg_map, (im.shape[1], im.shape[0]))
            else:
                continue

            seg_map[seg_map < self.label_thresh] = np.inf
            seg_map[self.cut_h_s:self.cut_h_e, self.cut_w_s:self.cut_w_e] = 0
            yield im, seg_map


class EvalMetrics:
    def __init__(self):
        self.images_loader = Images()
        self.theta_vec = np.linspace(0, 180, 30) - 180
        self.length = 10000
        return

    @staticmethod
    def createLineIterator(P1, P2, img):
        """
        Produces and array that consists of the coordinates and intensities of each pixel in a line between two points
        Parameters:
            -P1: a numpy array that consists of the coordinate of the first point (x,y)
            -P2: a numpy array that consists of the coordinate of the second point (x,y)
            -img: the image being processed
        Returns:
            -it: a numpy array that consists of the coordinates and intensities of each pixel in the radii (shape: [numPixels, 3], row = [x,y,intensity])
        """
        #define local variables for readability
        imageH = img.shape[0]
        imageW = img.shape[1]
        P1X = P1[0]
        P1Y = P1[1]
        P2X = P2[0]
        P2Y = P2[1]

        #difference and absolute difference between points
        #used to calculate slope and relative location between points
        dX = P2X - P1X
        dY = P2Y - P1Y
        dXa = np.abs(dX)
        dYa = np.abs(dY)

        #predefine numpy array for output based on distance between points
        itbuffer = np.empty(shape=(np.maximum(dYa,dXa),3),dtype=np.float32)
        itbuffer.fill(np.nan)

        #Obtain coordinates along the line using a form of Bresenham's algorithm
        negY = P1Y > P2Y
        negX = P1X > P2X
        if P1X == P2X: #vertical line segment
            itbuffer[:,0] = P1X
            if negY:
                itbuffer[:,1] = np.arange(P1Y - 1,P1Y - dYa - 1,-1)
            else:
                itbuffer[:,1] = np.arange(P1Y+1,P1Y+dYa+1)
        elif P1Y == P2Y: #horizontal line segment
            itbuffer[:,1] = P1Y
            if negX:
                itbuffer[:,0] = np.arange(P1X-1,P1X-dXa-1,-1)
            else:
                itbuffer[:,0] = np.arange(P1X+1,P1X+dXa+1)
        else: #diagonal line segment
            steepSlope = dYa > dXa
            if steepSlope:
                slope = dX.astype(np.float32)/dY.astype(np.float32)
                if negY:
                    itbuffer[:,1] = np.arange(P1Y-1,P1Y-dYa-1,-1)
                else:
                    itbuffer[:,1] = np.arange(P1Y+1,P1Y+dYa+1)
                itbuffer[:,0] = (slope*(itbuffer[:,1]-P1Y)).astype(np.int) + P1X
            else:
                slope = dY.astype(np.float32)/dX.astype(np.float32)
                if negX:
                    itbuffer[:,0] = np.arange(P1X-1,P1X-dXa-1,-1)
                else:
                    itbuffer[:,0] = np.arange(P1X+1,P1X+dXa+1)
                itbuffer[:,1] = (slope*(itbuffer[:,0]-P1X)).astype(np.int) + P1Y

        #Remove points outside of image
        colX = itbuffer[:,0]
        colY = itbuffer[:,1]
        itbuffer = itbuffer[(colX >= 0) & (colY >=0) & (colX<imageW) & (colY<imageH)]

        #Get intensities from img ndarray
        itbuffer[:,2] = img[itbuffer[:,1].astype(np.uint),itbuffer[:,0].astype(np.uint)]

        return itbuffer

    def ensure_size(self):
        self.label = cv2.resize(self.label, (self.img.shape[1], self.img.shape[0]))

    @staticmethod
    def canny(img):
        # img = cv2.imread('messi5.jpg', 0)
        edges = cv2.Canny(img, 100, 200)

        plt.subplot(121), plt.imshow(img, cmap='gray')
        plt.title('Original Image'), plt.xticks([]), plt.yticks([])
        plt.subplot(122), plt.imshow(edges, cmap='gray')
        plt.title('Edge Image'), plt.xticks([]), plt.yticks([])

        plt.show()

    def display_derivative(self):
        plt.imshow(self.img)

        diff_input = self.label[::4, ::4]
        print(diff_input.shape)
        self.label = np.abs(np.diff(diff_input, axis=0))
        self.ensure_size()
        print(self.label.shape, "min", np.min(self.label), "max", np.max(self.label))
        self.label[self.label < 0.05] = None
        plt.imshow(self.label, cmap=plt.cm.jet_r, alpha=0.4)
        plt.show()

    def original_ray(self):
        img = np.ones((1080, 1920))
        img[:img.shape[0]-500,] = 0

        img_draw = img.copy()
        center = [img.shape[1]//2, img.shape[0]-1]
        length = 10000

        print(center)
        theta_vec = np.linspace(0,180,10)-180

        for theta in theta_vec:
            theta = theta*np.pi/180
            x1, y1 = int(center[0]), int(center[1])
            x2 = int(x1 + length * np.cos(theta))
            y2 = int(y1 + length * np.sin(theta))
            cv2.line(img_draw,(x1,y1),(x2, y2),(3),4)

            line_iter = EvalMetrics.createLineIterator(np.array([x1, y1]), np.array([x2, y2]), img)
            x = np.argmin(line_iter[:, 2])
            if x == 0:
                continue
            x1, y1, intense = line_iter[x]
            cv2.circle(img_draw,(x1, y1), 30, thickness=3, color=8)

        plt.figure(figsize=(10,5))
        plt.imshow(img_draw)
        plt.colorbar()
        plt.show()

    def old_ray(self):

        center = [self.label.shape[1] // 2, self.label.shape[0] - 1]
        length = 10000

        print("min", np.min(self.label), "max", np.max(self.label))
        print(center)
        theta_vec = np.linspace(0, 180, 30) - 180

        theta_vec = [theta_vec[8]]
        for theta in theta_vec:
            theta = theta * np.pi / 180
            x1, y1 = int(center[0]), int(center[1])
            x2 = int(x1 + length * np.cos(theta))
            y2 = int(y1 + length * np.sin(theta))
            cv2.line(self.img, (x1, y1), (x2, y2), (255,165,0), 4)

            line_iter = EvalMetrics.createLineIterator(np.array([x1, y1]), np.array([x2, y2]), self.label)
            from itertools import groupby


            # argmin_input = line_iter[:, 2] > 0.1

            # sub_arrays = np.split(line_iter, np.where(line_iter[:, 2] == np.inf)[0], axis=0)
            # for elem in sub_arrays:
            #     np.append(elem, np.inf)

            # indices_to_remove = []
            # for idx, arr in enumerate(sub_arrays):
            #     if len(arr) == 1:
            #         indices_to_remove.append(idx)
            #
            # print("len(sub_arrays)", len(sub_arrays))
            # print(len(indices_to_remove))
            # sub_arrays = np.delete(sub_arrays, indices_to_remove, axis=0)

            # for sub_arr in sub_arrays:
            #     if sub_arr.shape[0] == 1:
            #         continue
            #     # print(sub_arr.shape)
            #     x = np.argmax(sub_arr[:, 2])
            #     if sub_arr[x][-1] != np.inf:
            #         x = len(sub_arr) - 1
            #     # x = np.argmin(argmin_input)
            #     # if x == 0:
            #     #     continue
            #     x1, y1, intense = sub_arr[x]
            #     print("intense", intense)
            #     cv2.circle(self.img, (x1, y1), 30, thickness=3, color=(255,165,0))

        plt.figure(figsize=(10, 5))
        plt.imshow(self.img)
        # self.label[self.label < 0.2] = None
        plt.imshow(self.label, cmap=plt.cm.jet_r, alpha=0.4)

        plt.colorbar()
        plt.show()

    def get_coords(self, line_iter, img, color=(255, 165, 0)):
        keep = [i for i, n in enumerate(line_iter[:, 2]) if i != 0 and ((np.isinf(n) and not np.isinf(line_iter[i - 1, 2])) or (np.isinf(line_iter[i - 1, 2]) and not np.isinf(n)))]
        coords = []
        for x in keep[:3]:
            x1, y1, intense = line_iter[x]
            coords.append((x1, y1))
            cv2.circle(img, (x1, y1), 30, thickness=3, color=color)
        return np.array(coords)

    def get_L1(self, gt_coords, pred_coords):
        if len(gt_coords)==0 or len(pred_coords) ==0:
            return 0

        # print("Before Final sizes", len(gt_coords), len(pred_coords))
        if len(gt_coords) > len(pred_coords):
            amount = len(gt_coords) - len(pred_coords)
            # print(amount)
            add = [gt_coords[-1] for _ in range(amount)]
            # print(add)
            pred_coords = np.append(pred_coords, add, axis=0)
        elif len(gt_coords) < len(pred_coords):
            amount = len(pred_coords) - len(gt_coords)
            # print(amount)
            add = [pred_coords[-1] for _ in range(amount)]
            # print(add)
            gt_coords = np.append(gt_coords, add, axis=0)
        # print("After Final sizes", len(gt_coords), len(pred_coords))

        return np.sum(np.linalg.norm(gt_coords-pred_coords,ord=1, axis=-1))


    def run_ray(self):
        num_images = 16
        save_path = "./results/"
        os.makedirs(save_path, exist_ok=True)
        for img_idx in tqdm(range(num_images)):
            img, label = next(self.images_loader.get_image())
            fake_pred = np.roll(label, shift=50)
            print("fake_pred", fake_pred.shape, "label", label.shape)
            center = [label.shape[1] // 2, label.shape[0] - 1]

            l1_norm = 0.0
            for theta in self.theta_vec:
                theta = theta * np.pi / 180
                x1, y1 = int(center[0]), int(center[1])
                x2 = int(x1 + self.length * np.cos(theta))
                y2 = int(y1 + self.length * np.sin(theta))
                cv2.line(img, (x1, y1), (x2, y2), (255, 165, 0), 4)

                line_iter = EvalMetrics.createLineIterator(np.array([x1, y1]), np.array([x2, y2]), label)
                gt_coords = self.get_coords(line_iter, img)

                line_iter = EvalMetrics.createLineIterator(np.array([x1, y1]), np.array([x2, y2]), fake_pred)
                pred_coords = self.get_coords(line_iter, img, color=(0, 165, 255))
                l1_norm +=self.get_L1(gt_coords, pred_coords)

            # plt.subplot(int(sqrt(num_images)), int(sqrt(num_images)), img_idx + 1)
            plt.cla()
            plt.figure(figsize=(20, 10))
            plt.imshow(img)
            plt.imshow(label, cmap=plt.cm.jet_r, alpha=0.4)
            plt.imshow(fake_pred, cmap=plt.cm.jet, alpha=0.4)
            plt.title("L1 Error " + str(l1_norm))
            plt.colorbar()

            # plt.show()
            plt.tight_layout()
            plt.subplots_adjust(top=0.9)
            plt.savefig(save_path + str(img_idx) +".png")
            plt.close()

        return

    def pad(self, im, mask):
        for row_idx, row in enumerate(mask):
            for col_idx, col in enumerate(row):
                if mask[row_idx][col_idx]:
                    cv2.circle(im, (col_idx, row_idx), 2, thickness=3, color=(255, 165, 0))
        return im

    def run_delimeter(self):
        num_images = 16
        save_path = "./results_edge/"
        os.makedirs(save_path, exist_ok=True)
        for img_idx in tqdm(range(num_images)):
            img, label = next(self.images_loader.get_image())

            cond1 = np.logical_and(np.isinf(label), np.bitwise_not(np.isinf(np.roll(label, shift=1))))
            cond2 = np.logical_and(np.isinf(np.roll(label, shift=1)), np.bitwise_not(np.isinf(label)))
            good_mask = np.logical_or(cond1, cond2)
            # label[np.bitwise_not(good_mask)] = np.inf

            img = self.pad(img, good_mask)
            plt.figure(figsize=(30, 15))
            plt.imshow(img)
            # plt.imshow(label, cmap=plt.cm.jet_r, alpha=0.4)
            # plt.show()
            plt.tight_layout()
            plt.savefig(save_path + str(img_idx) + ".png")
            plt.close()
            # keep = [i for i, n in enumerate(label) if i != 0 and ((np.isinf(n) and not np.isinf(img)) or (np.isinf(label[i - 1, 2]) and not np.isinf(n)))]

e = EvalMetrics()
# e.display()
# e.test_label()
e.run_ray()
# e.run_delimeter()