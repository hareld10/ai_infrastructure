import pathlib
import re
import time
from collections import defaultdict

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import gridspec, colors
from sklearn.cluster import DBSCAN
from tqdm import tqdm
import matplotlib.ticker as mticker
import matplotlib
import matplotlib.patches as mpatches

from Calibrator.calibrator_utils import save_obj
from DataPipeline.shared_utils import toRAzEl, toXYZ, PrintException
# from Evaluation.Metrics import OccupancyGridMetric
from DataPipeline.src.GetEngines import get_engines
from Evaluation.classes import EvalResults3D
from wiseTypes.classes_dims import Road

plt.style.use('dark_background')
font = {'size': 18}
matplotlib.rc('font', **font)
module_path = str(pathlib.Path(__file__).parent.absolute())
from Evaluation.evaluation_utils import cmap_clusters, get_cur_working_directory
from OpenSource.WisenseEngine import WisenseEngine


class OccupancyGrid:
    def __init__(self, az_bin=0.2, rng_bin=0.25, el_bin=5, length_m=(0, 76), width_m=(-40, 40), height_m=(-3, 10), x_bin=2, y_bin=2, z_bin=2):
        self.az_bin, self.rng_bin, self.el_bin = az_bin, rng_bin, el_bin
        self.length_m, self.width_m, self.height_m = length_m, width_m, height_m

        # r, Az, El
        self.n_az_bins = np.ceil((width_m[1] - width_m[0]) / self.az_bin).astype(np.int)
        self.n_rng_bins = np.ceil((length_m[1] - length_m[0]) / self.rng_bin).astype(np.int)
        self.n_el_bins = np.ceil((height_m[1] - height_m[0]) / self.el_bin).astype(np.int)
        self.az_indices, self.rng_indices, self.el_indices = np.indices((self.n_az_bins, self.n_rng_bins, self.n_el_bins))

        # X, Y, Z matrix
        self.x_bin, self.y_bin, self.z_bin = x_bin, y_bin, z_bin
        self.width = np.ceil((width_m[1] - width_m[0]) / self.x_bin).astype(np.int)
        self.length = np.ceil((length_m[1] - length_m[0]) / self.y_bin).astype(np.int)
        self.height = np.ceil((height_m[1] - height_m[0]) / self.z_bin).astype(np.int)
        self.x_indices, self.y_indices, self.z_indices = np.indices((self.width, self.length, self.height))

        self.results_dict = None
        self.detection_voxels_map = {}
        self.gt_voxels_map = {}
        self.conf_matrix = {}

        # Plotting settings
        # self.gt_color = "green"
        # self.det_color = "orange"
        self.tp_color = "blue"
        self.fp_color = "red"
        self.fn_color = "orange"

        def get_voxel_colors():
            a = np.zeros((self.n_az_bins, self.n_rng_bins, self.n_el_bins), dtype=np.uint8)
            b = np.empty(a.shape, dtype=object)
            return a, b

        self.gt_voxels, self.gt_colors = get_voxel_colors()
        self.det_voxels, self.det_colors = get_voxel_colors()

        self.axes_labelpad = 20
        self.legends = [  # mpatches.Patch(color=self.gt_color, label='Lidar'),
            # mpatches.Patch(color=self.det_color, label="Radar"),
            mpatches.Patch(color=self.tp_color, label="TP"),
            mpatches.Patch(color=self.fp_color, label="FP"),
            mpatches.Patch(color=self.fn_color, label="FN"), ]
        # self.legends_confusion_matrix = [mpatches.Patch(color=self.tp_color, label='TP'), mpatches.Patch(color=self.fp_color, label="FP")]
        return

    def add_point(self, x, y, z, az_tol_deg=0, r_tol_m=0, el_tol_deg=0, gt=False):
        voxel = self.get_voxel(x, y, z, az_tol_deg, r_tol_m, el_tol_deg)
        if gt:
            self.gt_voxels = self.gt_voxels | voxel
        else:
            self.det_voxels = self.det_voxels | voxel
        return voxel

    def add_points(self, pts, gt=False, detection=False):
        """

        @param pts: points Dataframe
        @param gt:
        @param detection:
        @return:
        """
        r, Az, El = toRAzEl(x=pts.iloc[:, 0], y=pts.iloc[:, 1], z=pts.iloc[:, 2])

        az_idx, rng_idx, el_idx = self.to_raz_indices(r=r, az=Az, el=El)

        ww = az_idx.to_numpy().astype(np.int)[:, None, None, None] == self.az_indices
        ll = rng_idx.to_numpy().astype(np.int)[:, None, None, None] == self.rng_indices
        hh = el_idx.to_numpy().astype(np.int)[:, None, None, None] == self.el_indices

        voxels = np.any(ww & ll & hh, axis=0)
        if gt:
            self.gt_voxels = self.gt_voxels | voxels
        elif detection:
            self.det_voxels = self.det_voxels | voxels
        else:
            print("OccupancyGrid: add_points: Warning - gt/det not mentioned")
        return voxels

    def add_clusters(self, clusters_dict, gt=False, detection=False):
        voxels_map = self.gt_voxels_map if gt else self.detection_voxels_map
        if gt and detection or (not gt and not detection):
            print("OccupancyGrid: add_clusters: Please define gt/detection")

        for gt_idx, gt_cluster in clusters_dict.items():
            voxels_map[gt_idx] = self.add_points(pts=gt_cluster.get_pts(), gt=gt, detection=detection)
        return

    def index(self, x_idx=0, y_idx=0, z_idx=0):
        x = self.width_m[0] + self.az_bin * x_idx
        y = self.length_m[0] + self.rng_bin * y_idx
        z = self.height_m[0] + self.el_bin * z_idx
        return x, y, z

    def add_points_efficient(self, gt_pts, clusters, plot_radius=3):
        # gt_pts_to_process = np.zeros((0, 3), dtype=np.float32)
        gt_pts_to_process = pd.DataFrame(columns=list(gt_pts.keys()))
        # Add points only in radios per center in order to avoid
        if plot_radius is not None:
            for cluster_idx, cluster in clusters.items():
                cluster_center = cluster.get_3d_center()
                lidar_rel = gt_pts[(np.logical_and((cluster_center[0] - plot_radius) <= gt_pts["x"], gt_pts["x"] <= (cluster_center[0] + plot_radius))) &
                                   (np.logical_and((cluster_center[1] - plot_radius) <= gt_pts["y"], gt_pts["y"] <= (cluster_center[1] + plot_radius))) &
                                   (np.logical_and((cluster_center[2] - plot_radius) <= gt_pts["z"], gt_pts["z"] <= (cluster_center[2] + plot_radius)))]
                gt_pts_to_process = gt_pts_to_process.append(lidar_rel)
        else:
            gt_pts_to_process = gt_pts.copy()
        self.add_points(pts=gt_pts_to_process, gt=True)
        return True

    def to_r_index(self, r):
        return (r - self.length_m[0]) / self.rng_bin

    def to_az_index(self, az):
        return (az - self.width_m[0]) / self.az_bin

    def to_el_index(self, el):
        return (el - self.height_m[0]) / self.el_bin

    def to_raz_indices(self, r, az, el):
        az_idx = self.to_az_index(az)
        rng_idx = self.to_r_index(r)
        el_idx = self.to_el_index(el)
        return az_idx, rng_idx, el_idx

    def to_xyz_indices(self, x, y, z):
        x_idx = (x - self.width_m[0]) / self.x_bin
        y_idx = (y - self.length_m[0]) / self.y_bin
        z_idx = (z - self.height_m[0]) / self.z_bin
        return x_idx, y_idx, z_idx

    def indices_to_value(self, r_indices, az_indices, el_indices):
        az = self.width_m[0] + self.az_bin * az_indices
        r = self.length_m[0] + self.rng_bin * r_indices
        el = self.height_m[0] + self.el_bin * el_indices
        return r, az, el

    def get_voxel(self, x, y, z, az_tol_deg=0, r_tol_m=0, el_tol_deg=0):
        r, az, el = toRAzEl(x=x, y=y, z=z)

        az_idx, rng_idx, el_idx = self.to_raz_indices(r=r, az=az, el=el)

        ww = az_idx.astype(np.int) == self.az_indices
        ll = rng_idx.astype(np.int) == self.rng_indices
        hh = el_idx.astype(np.int) == self.el_indices

        # Tolerance
        az_min_idx, az_max_idx = self.to_az_index(az - az_tol_deg), self.to_az_index(az + az_tol_deg)
        r_min_idx, r_max_idx = self.to_r_index(r - r_tol_m), self.to_r_index(r + r_tol_m)
        el_min_idx, el_max_idx = self.to_el_index(el - el_tol_deg), self.to_el_index(el + el_tol_deg)

        ww |= (np.int(az_min_idx) <= self.az_indices) & (self.az_indices <= np.int(az_max_idx))
        ll |= (np.int(r_min_idx) <= self.rng_indices) & (self.rng_indices <= np.int(r_max_idx))
        hh |= (np.int(el_min_idx) <= self.el_indices) & (self.el_indices <= np.int(el_max_idx))

        voxel = ww & ll & hh
        return voxel

    def is_occupied(self, x, y, z, az_tol_deg=0, r_tol_m=0, el_tol_deg=0):
        voxel = self.get_voxel(x, y, z, az_tol_deg, r_tol_m, el_tol_deg)
        res = voxel & self.gt_voxels
        return res.sum() > 0

    # Metric 1
    def evaluate_centers(self, clusters, az_tol_deg=0, r_tol_m=0, el_tol_deg=0):
        """
        Evaluate only by center of voxels
        @param el_tol_deg:
        @param az_tol_deg:
        @param r_tol_m:
        @param clusters:
        @return:
        """
        tp = []
        for cluster_idx, cluster_id in clusters.items():
            self.clusters_voxels_map[cluster_idx] = self.add_point(*clusters[cluster_id].get_3d_center(), az_tol_deg, r_tol_m, el_tol_deg)
            if self.is_occupied(*clusters[cluster_id].get_3d_center(), az_tol_deg, r_tol_m, el_tol_deg):
                tp.append(cluster_idx)
        self.results_dict = {"tp_indices": tp, "fp_indices": [idx for idx in range(len(clusters)) if idx not in tp]}
        self.cls_conf_matrix = {"unclassified": {"tp": len(tp), "fp": len(self.results_dict["fp_indices"]), "fn": 0}}

        return self.results_dict, self.cls_conf_matrix

    # Metric 2
    def evaluate_sum(self, clusters, fraction_of_intersection=0.1):
        """
        Evaluate by sum of intersected voxels
        @param clusters:
        @param fraction_of_intersection:
        @return:
        """
        tp = []
        for cluster_idx, cluster in clusters.items():
            pts = cluster.get_pts()
            cluster_voxel = self.add_points(pts=pts, gt=False, detection=True)
            self.clusters_voxels_map[cluster_idx] = cluster_voxel
            res = cluster_voxel & self.gt_voxels
            fraction = (res.sum() / cluster_voxel.sum())
            print(fraction, "res.sum()", res.sum(), "cluster_voxel.sum()", cluster_voxel.sum())
            if fraction >= fraction_of_intersection:
                tp.append(cluster_idx)

        self.results_dict = {"tp_indices": tp, "fp_indices": [idx for idx in range(len(clusters)) if idx not in tp]}
        self.cls_conf_matrix = {"unclassified": {"tp": len(tp), "fp": len(self.results_dict["fp_indices"]), "fn": 0}}
        return self.results_dict, self.cls_conf_matrix

    @staticmethod
    def sum_metric(gt_voxel, det_voxel, fraction_of_intersection=0.1):
        res = gt_voxel & det_voxel
        fraction = (res.sum() / det_voxel.sum())
        return fraction >= fraction_of_intersection

    @staticmethod
    def abs_metric(gt_voxel, det_voxel, abs_num_cells=1):
        res = gt_voxel & det_voxel
        return res.sum() >= abs_num_cells

    # Evaluate Clusters vs clusters
    def evaluate_clusters_vs_clusters(self, gt_clusters_voxels, det_clusters_voxels, compare_metric, **kwargs):
        gt_boxes_flags = {}  # mark if box has been predicted
        tp, fp, fn = 0, 0, 0
        tp_indices, fp_indices, fn_indices = [], [], []
        for gt_idx, gt_voxel in gt_clusters_voxels.items():
            match = False
            for det_idx, det_voxel in det_clusters_voxels.items():
                if not compare_metric(gt_voxel, det_voxel, **kwargs):
                    continue

                # if gt_idx not in gt_boxes_flags:
                gt_boxes_flags[gt_idx] = True
                match = True
                tp += 1
                tp_indices.append(det_idx)
                break
            if not match:
                fp += 1

        for det_idx, det_voxel in det_clusters_voxels.items():
            self.detection_voxels_map[det_idx] = det_voxel
            if det_idx not in tp_indices:
                fp_indices.append(det_idx)

        for gt_idx, gt_voxel in gt_clusters_voxels.items():
            self.gt_voxels_map[gt_idx] = gt_voxel
            if gt_idx not in gt_boxes_flags:
                fn_indices.append(gt_idx)

        self.results_dict = {"tp_indices": tp_indices, "fp_indices": fp_indices, "fn_indices": fn_indices}
        self.conf_matrix = {"tp": tp, "fp": len(fp_indices), "fn": len(fn_indices), "tn": np.sum(np.logical_not(self.det_voxels | self.gt_voxels))}
        # print(self.conf_matrix, len(gt_clusters_voxels), len(det_clusters_voxels))
        return self.results_dict, self.conf_matrix

    def transform_grid_to_xyz(self, voxels):
        # Extract indices
        Az_indices, r_indices, El_indices = np.nonzero(voxels)

        # indices -> r, Az, El
        r, Az, El = self.indices_to_value(r_indices=r_indices, az_indices=Az_indices, el_indices=El_indices)

        # r, Az, El -> x, y, z
        x, y, z = toXYZ(r=r, Az=Az, El=El)

        # x, y, z -> indices
        x_idx, y_idx, z_idx = self.to_xyz_indices(x=x, y=y, z=z)

        # mark voxels
        ww = x_idx.astype(np.int)[:, None, None, None] == self.x_indices
        ll = y_idx.astype(np.int)[:, None, None, None] == self.y_indices
        hh = z_idx.astype(np.int)[:, None, None, None] == self.z_indices

        return np.any(ww & ll & hh, axis=0)

    def plot_grid(self, ax, title="3D Occupancy Grid"):
        ax.set_xlim3d(0, self.width)
        labels = np.linspace(self.width_m[0], self.width_m[1], len(ax.get_xticks())).round(2).tolist()
        labels = labels[::2]
        plt.xticks(ax.get_xticks()[::2], labels)

        ax.set_ylim3d(0, self.length)
        labels = np.linspace(self.length_m[0], self.length_m[1], len(ax.get_yticks())).round(2).tolist()
        labels = labels[::2]
        ax.set_yticks(ax.get_yticks()[::2])
        ax.set_yticklabels(labels)

        ax.set_zlim3d(0, self.height)
        labels = np.linspace(self.height_m[0], self.height_m[1], len(ax.get_zticks())).round(2).tolist()
        labels = labels[::2]
        ax.set_zticks(ax.get_zticks()[::2])
        ax.set_zticklabels(labels)

        # ax.voxels(self.transform_grid_to_xyz(self.gt_voxels), facecolors=self.gt_color, edgecolor='k')
        # ax.voxels(self.transform_grid_to_xyz(self.det_voxels), facecolors=self.det_color, edgecolor='k')
        # ax.voxels(self.transform_grid_to_xyz(self.det_voxels), facecolors=self.fp_color, edgecolor='k')
        # ax.voxels(self.transform_grid_to_xyz(self.det_voxels & self.gt_voxels), facecolors=self.tp_color, edgecolor='k')

        if self.results_dict is not None:
            for fp_idx in self.results_dict["fp_indices"]:
                ax.voxels(self.transform_grid_to_xyz(self.detection_voxels_map[fp_idx]), facecolors=self.fp_color, edgecolor='k')
            for fn_idx in self.results_dict["fn_indices"]:
                ax.voxels(self.transform_grid_to_xyz(self.gt_voxels_map[fn_idx]), facecolors=self.fn_color, edgecolor='k')
            for tp_idx in self.results_dict["tp_indices"]:
                ax.voxels(self.transform_grid_to_xyz(self.detection_voxels_map[tp_idx]), facecolors=self.tp_color, edgecolor='k')

        ax.set_title(title)

        ax.set_xlabel('X[m]', labelpad=self.axes_labelpad)
        ax.set_ylabel('Y[m]', labelpad=self.axes_labelpad)
        ax.set_zlabel('Z[m]', labelpad=self.axes_labelpad // 2)

        plt.legend(handles=self.legends, bbox_to_anchor=(0.5, -0.3), loc='lower center')

    def plot(self, data_engine, frame_idx, fig_path):
        plt.close()
        rgb_img = data_engine.get_image(idx=frame_idx)
        seg_label = data_engine.get_segmentation_label(frame_idx)
        lidar_df = data_engine.get_lidar_df(idx=frame_idx, filter_by_seg_class=Road(), keep_semantic=False)
        lidar_df = lidar_df.dropna()
        radar_p_delta = data_engine.get_processed_delta(idx=frame_idx)
        clusters = data_engine.get_clusters(p_delta=radar_p_delta)
        lidar_clusters = data_engine.get_lidar_clusters(lidar_df)

        # Camera
        fig = plt.figure(figsize=(24, 14))
        gs = gridspec.GridSpec(ncols=3, nrows=3)
        im_ax = fig.add_subplot(gs[0, 0])
        im_ax.imshow(rgb_img, alpha=0.4)
        plt.imshow(seg_label.get_img(colors=True), alpha=0.3)
        im_ax.axis("off")
        im_ax.set_title("Camera  | Radar FN")
        if self.results_dict is not None:
            [lidar_clusters[gt_idx].plot_on_im(im_ax, rgb_img) for gt_idx in self.results_dict["fn_indices"]]

        # Plot Radar
        ax = fig.add_subplot(gs[1, 0])
        ax.set_title("Delta | BEV | colormap=cluster_id")
        ax.set_xlim(-40, 40)
        ax.set_ylim(0, 76)
        ax.set_ylabel("Y[m]")
        leg_x, leg_y, leg_loc = -0.4, 0.5, 'center left'
        [x.plot(ax, scatter_size=2) for x in clusters.values()]

        if self.results_dict is not None:
            for det_idx in self.results_dict["tp_indices"]:
                cluster = clusters[det_idx]
                ax.scatter([cluster[0]], [cluster[1]], s=100, c=self.tp_color, marker="x", linewidths=1.2, facecolors='none', alpha=1)
            for det_idx in self.results_dict["fp_indices"]:
                cluster = clusters[det_idx]
                ax.scatter([cluster[0]], [cluster[1]], s=100, c=self.fp_color, marker="x", linewidths=1.2, facecolors='none', alpha=1)
            for gt_idx in self.results_dict["fn_indices"]:
                cluster = lidar_clusters[gt_idx]
                ax.scatter([cluster[0]], [cluster[1]], s=100, c=self.fn_color, marker="x", linewidths=1.2, facecolors='none', alpha=1)

        plt.legend(handles=self.legends, bbox_to_anchor=(leg_x, leg_y), loc=leg_loc)

        # Plot Lidar
        ax = fig.add_subplot(gs[2, 0])
        ax.set_xlim(-40, 40)
        ax.set_ylim(0, 76)
        ax.set_title("Lidar | BEV | colormap=cluster_id")
        ax.set_xlabel("X[m]")
        ax.set_ylabel("Y[m]")
        [x.plot(ax, scatter_size=2, quiver=False) for x in lidar_clusters.values()]
        if self.results_dict is not None:
            for det_idx in self.results_dict["tp_indices"]:
                cluster = clusters[det_idx]
                ax.scatter([cluster[0]], [cluster[1]], s=100, c=self.tp_color, marker="x", linewidths=1.2, facecolors='none', alpha=1)
            for det_idx in self.results_dict["fp_indices"]:
                cluster = clusters[det_idx]
                ax.scatter([cluster[0]], [cluster[1]], s=100, c=self.fp_color, marker="x", linewidths=1.2, facecolors='none', alpha=1)
            for gt_idx in self.results_dict["fn_indices"]:
                cluster = lidar_clusters[gt_idx]
                ax.scatter([cluster[0]], [cluster[1]], s=100, c=self.fn_color, marker="x", linewidths=1.2, facecolors='none', alpha=1)
        plt.legend(handles=self.legends, bbox_to_anchor=(leg_x, leg_y), loc=leg_loc)

        # Plot Grids
        ax = fig.add_subplot(gs[:, 2], projection='3d')
        self.plot_grid(ax=ax)
        ax = fig.add_subplot(gs[:, 1], projection='3d')
        self.plot_grid(ax=ax, title="3D Occupancy Grid | BEV")
        ax.view_init(elev=88, azim=270)
        ax.set_zticks([])

        # plt.subplots_adjust(bottom=0.18)
        title = "\nGrid bins: R=" + str(self.rng_bin) + "m, Az=" + str(self.az_bin) + "deg ,El=" + str(self.el_bin) + "deg"
        if self.conf_matrix is not None:
            title += "\nMetric=cluster centers, Confusion-Matrix: " \
                     "TP=" + str(self.conf_matrix["tp"]) + \
                     ", FP=" + str(self.conf_matrix["fp"]) + \
                     ", FN=" + str(self.conf_matrix["fn"]) + \
                     ", TN=" + str(self.conf_matrix["tn"]) + "\n" + "Lidar Clustering | DBSCAN"

        plt.suptitle(data_engine.get_drive_name() + " | " + data_engine.get_id(idx=frame_idx) + title)
        plt.tight_layout()
        plt.savefig(fig_path)
        print("Plot Saved")
        return


def main():
    plot_idx = 1
    kwargs = {"abs_num_cells": 1}

    for data_engine in get_engines(wf="92", val_train="all", rect=True):
        data_engine.sample_df(10)
        for frame_idx in range(len(data_engine)):
            try:
                # Define Grid and Add Points
                o_grid = OccupancyGrid(x_bin=2, y_bin=2, z_bin=3)

                # Get Data
                lidar_df = data_engine.get_lidar_df(idx=frame_idx, filter_by_seg_class=Road(), keep_semantic=False)
                lidar_df = lidar_df.dropna()
                radar_p_delta = data_engine.get_processed_delta(idx=frame_idx)
                clusters = data_engine.get_clusters(p_delta=radar_p_delta)
                lidar_clusters = data_engine.get_lidar_clusters(lidar_df)

                # GT Clusters
                o_grid.add_clusters(clusters_dict=lidar_clusters, gt=True)
                o_grid.add_clusters(clusters_dict=clusters, detection=True)

                # Define Metric
                # res_dict, _ = o_grid.evaluate_centers(clusters, r_tol_m=1, az_tol_deg=1)
                # res_dict, _ = o_grid.evaluate_sum(clusters, fraction_of_intersection=0.1)
                o_grid.evaluate_clusters_vs_clusters(gt_clusters_voxels=o_grid.gt_voxels_map,
                                                     det_clusters_voxels=o_grid.detection_voxels_map,
                                                     compare_metric=OccupancyGrid.abs_metric, **kwargs)

                if frame_idx % plot_idx != 0:
                    print("continue")
                    continue

                o_grid.plot(data_engine=data_engine, frame_idx=frame_idx, fig_path=module_path + "/figures/" + data_engine.get_id(frame_idx) + ".png")

            except Exception as e:
                PrintException()
                print(e)
                continue

    # save_obj(obj=final_res, path=module_path + "/final_res.json")


if __name__ == '__main__':
    main()
