import argparse
import ast
import json
import os
import pathlib
import time
from copy import copy, deepcopy
from multiprocessing import Pool
import cv2
import matplotlib
import requests
from matplotlib import gridspec
from tqdm import tqdm
import sys
import numpy as np
import matplotlib.pyplot as plt

from Calibrator.calibrator_utils import apply_6DOF, project_velo_to_cam2, project_to_image, project

module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, module_path)
sys.path.insert(0, module_path + "/../")
from DataPipeline.pipeline4 import Ensemble3D
from wiseTypes.classes_dims import Car, Vehicle
from DataPipeline.DNNs.wrappers.yolorWrapper import YoloRWrapper
from DataPipeline.ensamble import CarContext, PedContext, all_ensemble_contexts, all_ensemble_contexts_callable
from LabelsManipulation.clf_utils import non_max_suppression_fast
from wiseTypes.Label import Label
from wiseTypes.Label3D import Label3D
from DataPipeline.DNNs.wrappers.wrappers import ModelWrapper
from DataPipeline.src.GetEngines import get_engines
from Evaluation.Metrics import CenterInBB, BEV, Heading
from LabelsManipulation.camera_lidar_fusion import CameraLidarFusion
from Plotting.wise_plotter import plot_3d_evaluation
from wiseTypes.SegLabel import SegLabel
# python3 Evaluation/run_eval.py --seg --audi 100 --plot-every 1
import pandas as pd
from OpenSource.CombinedDataset import CombinedDataset
from DataPipeline.shared_utils import pickle_write, is_docker
from DataPipeline.DNNs.wrappers.ensembleWrapper import EnsembleWrapper
from DataPipeline.DNNs.wrappers.efficientDetWrapper import EfficientDetWrapper
from DataPipeline.DNNs.wrappers.yoloWrapper import YoloWrapper

if is_docker():
    from DataPipeline.DNNs.wrappers.deepLabWrapper import DeepLabWrapper

from Evaluation.classes import EvalResults, FrameResults, FrameResultsSeg, EvalResultsSeg, Config
from Evaluation.evaluation_utils import draw, get_cur_working_directory, ensure_same_shape
from OpenSource.AudiEngine import AudiEngine
from OpenSource.WaymoEngine import WaymoEngine
from Calibrator.bb_overlay import Config as BB_Overlay_Config, BBDisplay
from Evaluation.evaluation_wrappers import SizeEstimationWrapper
from wiseTypes.Label2D import Label2D
from datetime import datetime
from pprint import pprint
import itertools

font = {'size': 24}
matplotlib.rc('font', **font)

parser = argparse.ArgumentParser(description='Evaluation arguments')
parser.add_argument('--od', action='store_true', default=False, help="path to specific dataframe")
parser.add_argument('--seg', action='store_true', default=False, help="path to specific dataframe")
parser.add_argument('--rrs', action='store_true', default=False, help="path to specific dataframe")
parser.add_argument('--od3d', action='store_true', default=False, help="path to specific dataframe")
parser.add_argument('--se', action='store_true', default=False, help="path to specific dataframe")
parser.add_argument('--grid_search', "--g", action='store_true', default=False, help="path to specific dataframe")
parser.add_argument('--ensure_classes', "--e", type=str, default=None, help="path to specific dataframe")
parser.add_argument('--grid_search_clf', "--gclf", type=str, default="", help="path to specific dataframe")
parser.add_argument('--uniform_eval', action='store_true', default=False, help="path to specific dataframe")
parser.add_argument('--compare', "--c", action='store_true', default=False, help="path to specific dataframe")
parser.add_argument('--projection_error_analysis', "--pea", action='store_true', default=False, help="path to specific dataframe")
parser.add_argument('--camera_pipeline_analysis', "--cpa", action='store_true', default=False, help="path to specific dataframe")

parser.add_argument('--waymo', default=0, type=int, help="specify GPU to use")
parser.add_argument('--audi', default=0, type=int, help="specify GPU to use")
parser.add_argument('--dataset', default="audi", type=str, help="specify GPU to use")
parser.add_argument('--samples', default=0, type=int, help="specify GPU to use")
parser.add_argument('--plot-every', default=100, type=int, help="specify GPU to use")
parser.add_argument('--n-plots', default=0, type=int, help="specify GPU to use")

parser.add_argument('--train', action='store_true', default=False, help="path to specific dataframe")
args = parser.parse_args()
if args.ensure_classes is not None:
    args.ensure_classes = args.ensure_classes.split("_")


class DriveContext(ModelWrapper):
    def __init__(self, model_name, engine, f_name="radar_road_seg"):
        super().__init__(model_name)
        self.engine = engine
        self.f_name = f_name
        return

    def infer_im(self, idx, gt):
        label = np.load(
            self.engine.drive_context.evaluation + "/" + self.f_name + "/" + self.f_name + "_" + self.engine.get_id(idx) + ".npy")
        label = ensure_same_shape(gt=gt.get_img(), pred=label)
        return SegLabel(img=np.zeros(label.shape, dtype=np.float32), engine=self), label

    def get_seg_classes(self):
        ret = []
        ret.append(["road", 0])
        return ret


class EvaluationRunner:
    def __init__(self, plot_every=20, n_plots=1):
        self.plot_every = plot_every
        self.n_plots = n_plots
        return

    def od_evaluation(self, model_wrapper, data_engine, metric):
        cur_working_dir, images_path = get_cur_working_directory(
            model_name=model_wrapper.get_name(),
            data_engine_name=data_engine.get_name(), task="od", metric=metric)

        df = pd.DataFrame()
        for data_idx in tqdm(range(len(data_engine))):
            f_results = FrameResults()

            im, labels = data_engine.get_image(data_idx), data_engine.get_2d_label(data_idx)

            if len(labels) == 0:
                continue

            predictions = model_wrapper.infer_im(im)

            results = metric.calculate(frame_results=f_results)
            df = df.add(results, fill_value=0)

            # f_results.set_labels(item=labels)
            # f_results.set_prediction(item=predictions)
            # e_results.set_frame_results(frame_results=f_results)
            # e_results.keep_frame_results(frame_results=f_results, id=data_engine.get_id(data_idx))

            if data_idx % self.plot_every == 0:
                im = draw(im=im, label2d=labels, color=(255, 0, 0), suffix="gt: ")
                im = draw(im=im, label2d=predictions, color=(0, 0, 255), suffix="pred: ")
                cv2.imwrite(images_path + "/" + str(data_engine.get_id(data_idx)) + ".png", im[..., ::-1])

        # e_results.save_results()
        df.to_csv(cur_working_dir + "/confusion.csv")
        return True

    def od_evaluation_multiple(self, model, data_engine, metrics, instances=True):
        for metric in metrics:
            metric.df = pd.DataFrame()
            metric.df_instances = pd.DataFrame()
            metric.cur_working_dir, metric.images_path = get_cur_working_directory(model_name=model.get_name(), metric=str(metric),
                                                                                   data_engine_name=data_engine.get_name(), task="od")

        df = pd.DataFrame()
        for data_idx in tqdm(range(len(data_engine))):
            f_results = FrameResults()

            im, labels = data_engine.get_image(data_idx), data_engine.get_2d_label(data_idx)

            if len(labels) == 0:
                continue

            predictions = model.infer_im(im)

            for metric in metrics:
                results, df_instances = metric.calculate(frame_results=f_results, instances=instances)
                metric.df = metric.df.add(results, fill_value=0)
                metric.df_instances = metric.df_instances.append(df_instances, ignore_index=True)

                if data_idx % self.plot_every == 0:
                    im = draw(im=im, label2d=labels, color=(255, 0, 0), suffix="gt: ")
                    im = draw(im=im, label2d=predictions, color=(0, 0, 255), suffix="pred: ")
                    cv2.imwrite(metric.images_path + "/" + str(data_engine.get_id(data_idx)) + ".png", im[..., ::-1])

        # e_results.save_results()
        [metric.df.to_csv(metric.cur_working_dir + "/confusion.csv") for metric in metrics]
        [metric.df_instances.to_csv(metric.cur_working_dir + "/df_instances.csv") for metric in metrics]
        return True

    def seg_evaluation(self, model_wrapper, data_engine):
        cur_working_dir, images_path = get_cur_working_directory(model_name=model_wrapper.get_name(),
                                                                 data_engine_name=data_engine.get_name(), task="seg")
        e_results = EvalResultsSeg(dataset_name=data_engine.name, model_name=model_wrapper.model_name, out_path=cur_working_dir,
                                   config=Config())

        for data_idx in tqdm(range(len(data_engine))):
            f_results = FrameResultsSeg()

            im, label = data_engine.get_image(data_idx), data_engine.get_segmentation_label(data_idx)

            hard_pred, soft_pred = model_wrapper.infer_im(im.copy())
            hard_pred = model_wrapper.cast_to_label(hard_label=hard_pred)

            f_results.set_label(item=label)
            f_results.set_prediction(hard_pred=hard_pred, soft_pred=soft_pred)

            e_results.set_frame_results(frame_results=f_results)

            if data_idx % self.plot_every == 0:
                plt.figure(figsize=(20, 10))
                plt.subplot(1, 2, 1)
                plt.imshow(im)
                plt.imshow(label.get_img(colors=True), alpha=0.4)
                plt.title("Label")
                plt.axis("off")

                plt.subplot(1, 2, 2)
                plt.imshow(im)
                plt.imshow(hard_pred.get_img(colors=True), alpha=0.4)
                plt.title("Prediction")
                plt.axis("off")
                plt.tight_layout()
                plt.savefig(images_path + "/" + str(data_idx) + ".png")

        e_results.save_results()
        return e_results

    def old_od3d_evaluation(self, od_2d_model, data_engine):
        bb = BBDisplay()
        bb.set_config(config=BB_Overlay_Config())
        model_name = "lcf_v1.0_" + od_2d_model.get_name()
        cur_working_dir, images_path = get_cur_working_directory(model_name=model_name, data_engine_name=data_engine.get_name(),
                                                                 task="od3d")
        evaluation_results = EvalResults(model_name=model_name, dataset_name=data_engine.name, out_path=cur_working_dir)
        bb.od_3d_evaluation(data_engine=data_engine, od_2d_model=od_2d_model, evaluation_results=evaluation_results,
                            plot_every=self.plot_every, center_only=True)
        evaluation_results.save_results()
        return evaluation_results

    def od3d_evaluation(self, model, data_engine, metric=BEV(score=0.1)):
        cur_working_dir, images_path = get_cur_working_directory(model_name=model.get_name(), metric=str(metric),
                                                                 data_engine_name=data_engine.get_name(), task="od3d")

        # metric = BEV(iou=0.25)
        df = pd.DataFrame()
        for engine_idx in tqdm(range(len(data_engine))):
            f_results = FrameResults()

            labels = data_engine.get_3d_label(engine_idx)
            # [x.transform_to_radar_coordinates() for x in labels]

            f_results.set_labels(labels)
            preds = model.run_engine_idx(engine=data_engine, engine_idx=engine_idx)
            # [x.transform_to_radar_coordinates() for x in preds]
            f_results.set_prediction(preds)

            results = metric.calculate(frame_results=f_results)
            df = df.add(results, fill_value=0)

            if engine_idx % self.plot_every == 0:
                plot_3d_evaluation(labels=labels, preds=preds, engine=data_engine, idx=engine_idx)
                plt.gcf().subplots_adjust(top=0.8)
                plt.suptitle(str(results))
                plt.savefig(images_path + "/" + str(engine_idx) + ".jpg")

        df.to_csv(cur_working_dir + "/confusion.csv")
        return True

    def od3d_evaluation_multiple(self, model, data_engine, metrics=[BEV(score=0.1)], force=True,
                                 instances=True, attr="get_3d_label", task="od3d", dirs_only=False):

        for metric in metrics:
            metric.df = pd.DataFrame()
            metric.df_instances = pd.DataFrame()
            metric.cur_working_dir, metric.images_path = get_cur_working_directory(model_name=model.get_name(), metric=str(metric),
                                                                                   data_engine_name=data_engine.get_name(), task=task,
                                                                                   force=force)
            if not force and metric.cur_working_dir == False:
                print("skipping", model.get_name())
                return

        if dirs_only:
            return

        frames_with_labels = 0
        for engine_idx in tqdm(range(len(data_engine))):
            f_results = FrameResults()

            caller = getattr(data_engine, attr)
            labels = caller(engine_idx)
            for x in labels:
                x.write_data["engine_idx"] = engine_idx
                x.write_data["engine_name"] = data_engine.get_name()

            if len(labels) == 0:
                continue
            if args.ensure_classes is not None:
                ret = sum([x.cls in args.ensure_classes for x in labels])
                if ret == 0:
                    continue
            frames_with_labels += 1
            f_results.set_labels(labels)
            preds = model.run_engine_idx(engine=data_engine, engine_idx=engine_idx, force=force)
            if preds is None:
                print("od3d_evaluation_multiple: got None preds, skipping")
                continue
            for p in preds:
                if p.cls == "large_vehicle":
                    p.set_cls(cls_obj=Car())
                if p.cls == "vehicle":
                    p.set_cls(cls_obj=Car())
            for p in labels:
                if p.cls == "large_vehicle":
                    p.set_cls(cls_obj=Car())
                if p.cls == "vehicle":
                    p.set_cls(cls_obj=Car())

            f_results.set_prediction(preds)

            for met_idx, metric in enumerate(metrics):
                results, df_instances = metric.calculate(frame_results=f_results, instances=instances)
                metric.df = metric.df.add(results, fill_value=0)
                metric.df_instances = metric.df_instances.append(df_instances, ignore_index=True)

                draw_ped_flag = False
                if (met_idx == 0) and ((self.plot_every != -1 and engine_idx % self.plot_every == 0) or \
                                       (frames_with_labels < self.n_plots) or \
                                       (draw_ped_flag and sum([x.cls == "pedestrian" for x in labels]) > 0)):
                    # print("\nengine_idx", engine_idx)
                    # pprint(preds)

                    if task == "od3d":
                        plot_3d_evaluation(labels=labels, preds=preds, engine=data_engine, idx=engine_idx)
                        plt.gcf().subplots_adjust(top=0.8)
                        plt.suptitle(str(results))
                        fig_path = metric.images_path + "/" + str(engine_idx) + ".jpg"
                        plt.savefig(fig_path)
                    elif task == "od":
                        im = data_engine.get_image(engine_idx)
                        im = draw(im=im, label2d=labels, color=(255, 0, 0), suffix="gt: ")
                        im = draw(im=im, label2d=preds, color=(0, 0, 255), suffix="pred: ", y_delta=40)
                        im_path = metric.images_path + "/" + str(data_engine.get_id(engine_idx)) + ".png"
                        cv2.imwrite(im_path, im[..., ::-1])

        [metric.df.to_csv(metric.cur_working_dir + "/confusion.csv") for metric in metrics]
        [metric.df_instances.to_csv(metric.cur_working_dir + "/df_instances.csv") for metric in metrics]
        print(f"frames with labels={frames_with_labels}")
        return True

    def rrs_evaluation(self, model_wrapper, data_engine, task="radar_road_seg"):
        cur_working_dir, images_path = get_cur_working_directory(model_name=model_wrapper.get_name(),
                                                                 data_engine_name=data_engine.get_name(), task=task)
        e_results = EvalResultsSeg(dataset_name=data_engine.name, model_name=model_wrapper.model_name, out_path=cur_working_dir,
                                   config=Config())

        if args.samples:
            data_engine.sample_df(int(args.samples))

        for data_idx in tqdm(range(len(data_engine))):
            f_results = FrameResultsSeg()

            try:
                im, label = data_engine.get_image(data_idx), data_engine.get_segmentation_label(data_idx)
                hard_pred, soft_pred = model_wrapper.infer_im(data_idx, label)
            except Exception as e:
                print(e)
                continue

            # hard_pred = model_wrapper.cast_to_label(hard_label=hard_pred)

            f_results.set_label(item=label)
            f_results.set_prediction(hard_pred=hard_pred, soft_pred=soft_pred)

            e_results.set_frame_results(frame_results=f_results)

            if data_idx % self.plot_every == 0:
                plt.figure(figsize=(20, 10))
                plt.subplot(1, 2, 1)
                plt.imshow(im)
                plt.imshow(label.get_img(colors=True), alpha=0.4)
                plt.title("Label")
                plt.axis("off")

                plt.subplot(1, 2, 2)
                plt.imshow(im)
                plt.imshow(hard_pred.get_img(colors=True), alpha=0.4)
                plt.title("Prediction")
                plt.axis("off")
                plt.tight_layout()
                plt.savefig(images_path + "/" + str(data_idx) + ".png")

        e_results.save_results()
        return e_results


class SSD3DWrapper:
    def __init__(self, ssd3d_docker=None, name=None):
        self.available_port = ssd3d_docker
        self.name = name
        pass

    def get_name(self):
        if self.name is not None:
            return self.name
        elif self.available_port == 78:
            return "3DSSD_uniform_0.15"
        elif self.available_port == 79:
            return "3DSSD_uniform_z_0.15"
        elif self.available_port == 81:
            return "3DSSD_baseline"
        elif self.available_port == 82:
            return "3DSSD_uniform_z_0.1"
        elif self.available_port == 83:
            return "3DSSD_uniform_z_0.2"
        else:
            return "3DSSD_what"

    def run_engine_idx(self, engine, engine_idx, force=False):
        pred_path = f"{engine.get_predictions_dir()}/{self.get_name()}/{engine.get_id(engine_idx)}npy"
        if not force:
            os.makedirs(os.path.split(pred_path)[0], exist_ok=True)
            if os.path.exists(pred_path):
                # print("returning-3dssd")
                return np.load(pred_path, allow_pickle=True)
        lidar = engine.get_lidar_df(engine_idx)
        to_send = lidar[["orig_x", "orig_y", "orig_z", "r"]].to_json()
        cls_map = {1: "vehicle", 2: "pedestrian", 3: "cyclist"}
        predictions = []
        ret = requests.post(f'http://localhost:{self.available_port}/predict', json=to_send)
        pred_dicts = ret.json()["pred_dicts"]

        for k, v in pred_dicts.items():
            pred_dicts[k] = np.asarray(v)

        boxes3d_lidar = pred_dicts["pred_boxes"].astype(float)
        scores = pred_dicts["pred_scores"].astype(float)
        clsses = pred_dicts["pred_labels"].astype(float)
        for obj_idx, obj in enumerate(boxes3d_lidar):
            tx, ty, tz, l, h, w, heading = obj
            score = scores[obj_idx]
            cur_label = Label3D(cls=cls_map[clsses[obj_idx]], score=score, tx=tx, ty=ty, tz=tz,
                                heading=heading, width=w, length=l, height=h, engine=engine)
            predictions.append(cur_label)
        np.save(pred_path, predictions)
        return predictions


class Ensemble3DWrapper:
    def __init__(self, ssd3d=None, clf=None, ssd3d_score=0.4, clf_score=0.4, iou=0.25, nms=0.8):
        # self.ssd3d : SSD3DWrapper = ssd3d #if ssd3d is not None else SSD3DWrapper(ssd3d_docker=78)
        # self.clf : CameraLidarFusion = clf
        self.clf = CameraLidarFusion(data_engine=audi_engine, se_docker=77)
        self.clf.associate_with_delta_cluster = False
        self.clf.sub_clusters_flag = False
        self.ssd3d = SSD3DWrapper(ssd3d_docker=78)

        self.ssd3d_score = ssd3d_score
        self.clf_score = clf_score
        self.iou = iou
        self.nms = nms
        self.results = {}

    def get_name(self):
        return f"ensemble_3d_{self.ssd3d_score}_{self.clf_score}_{self.iou}_{self.nms}_{self.ssd3d.get_name()}_{self.clf.get_name()}"

    def run_engine_idx(self, engine, engine_idx, force=False):
        ssd3d = self.ssd3d.run_engine_idx(engine, engine_idx, force=False)
        clf = self.clf.run_engine_idx(engine, engine_idx, force=False)

        finals = []
        [finals.append(x) for x in ssd3d if x.score >= self.ssd3d_score]
        [finals.append(x) for x in clf if x.score >= self.clf_score]

        for x in ssd3d:
            for y in clf:
                iou = Label.get_2d_iou(x, y)
                if iou > self.iou:
                    x.score = x.score * 1.3
                    finals.append(x)

        bboxes = np.array([x.get_bev_bbox() for x in finals])
        indices = np.array(non_max_suppression_fast(bboxes, self.nms))
        finals = np.asarray(finals)
        if len(indices) > 0:
            finals = finals[indices]

        # print("processing=", time.time() - start)
        self.results["3dssd"] = ssd3d
        self.results["clf"] = clf
        self.results["ensemble"] = finals

        return finals


class WrapEngineMethod:
    def __init__(self, method):
        self.name = method

    def get_name(self):
        return self.name

    def run_engine_idx(self, engine, engine_idx, force=False):
        caller = getattr(engine, self.name)
        return caller(engine_idx)


def run_model(cur_model, engine):
    # global audi_engine
    e_runner = EvaluationRunner(plot_every=args.plot_every)
    metrics = []
    for iou_test in [0.25]:
        for metric_test in [BEV(iou=iou_test, score=x) for x in np.linspace(0.0, 1, 10)]:
            metrics.append(metric_test)
    e_runner.od3d_evaluation_multiple(model=cur_model, metrics=metrics, data_engine=engine, instances=False)


def run_model_2d(cur_model, engine, metrics):
    # global audi_engine
    e_runner = EvaluationRunner(plot_every=args.plot_every, n_plots=args.n_plots)
    e_runner.od3d_evaluation_multiple(model=cur_model, metrics=metrics, data_engine=engine, instances=False, task="od", attr="get_2d_label")


def get_2d_metrics(d_iou=0.25):
    metrics = []
    for iou in [d_iou]:
        for metric in [BEV(iou=iou, score=x) for x in np.linspace(0.0, 1, 10)]:
            metrics.append(metric)
    return metrics


def get_se_metrics():
    metrics = []
    metrics += [Heading(heading_diff=0)]
    return metrics


def main():
    global audi_engine
    e_runner = EvaluationRunner(plot_every=args.plot_every, n_plots=args.n_plots)

    val_flag = not args.train
    audi_engine = AudiEngine(base_path="/workspace/HDD/open_source/audi/", val=val_flag)
    if args.audi != 0:
        # audi_engine.df = audi_engine.df.sample(args.audi).reset_index(drop=True)
        audi_engine.update_df_range(0, args.audi)

    try:
        waymo_engine = WaymoEngine(base_path="/workspace/open_source1/waymo/", val=val_flag)
        waymo_engine.update_df_range(0, 6000 if args.waymo == 0 else args.waymo)
    except IOError as e:
        pass

    data_engine = audi_engine if args.dataset == "audi" else waymo_engine

    """
    Segmentation
    """
    if args.seg:
        cur_working_dir_2, images_path = get_cur_working_directory(model_name="combined", data_engine_name=audi_engine.get_name(),
                                                                   task="seg")
        models_to_test = [DeepLabWrapper()]
        datas = {}
        for cur_model_wrapper in models_to_test:
            eval_results_seg = e_runner.seg_evaluation(model_wrapper=cur_model_wrapper, data_engine=audi_engine)
            datas[cur_model_wrapper.model_name] = eval_results_seg.get_data()
        pickle_write(obj=datas, file_path=cur_working_dir_2 + "/datas_seg")
        EvalResultsSeg.plot_data(datas=datas, out_path=cur_working_dir_2)

    if args.rrs:
        engines = get_engines(val_train="val")
        for engine in engines:
            cur_working_dir_2, images_path = get_cur_working_directory(model_name="radar_road_seg_v1.1.radar_road_seg_net_v1.delta.0.6m",
                                                                       data_engine_name=engine.get_drive_name(),
                                                                       task="radar_road_seg")
            models_to_test = [DriveContext(model_name="radar_road_seg_v1.1.radar_road_seg_net_v1.delta.0.6m", engine=engine)]
            datas = {}
            for cur_model_wrapper in models_to_test:
                eval_results_seg = e_runner.rrs_evaluation(model_wrapper=cur_model_wrapper, data_engine=engine)
                datas[cur_model_wrapper.model_name] = eval_results_seg.get_data()
            pickle_write(obj=datas, file_path=cur_working_dir_2 + "/datas_rrs")
            EvalResultsSeg.plot_data(datas=datas, out_path=cur_working_dir_2)
    """
    3D Object Detection
    """
    if args.od3d:
        # models
        # alg = CameraLidarFusion(data_engine=audi_engine, se_docker=77)
        operational_ensemble3d = WrapEngineMethod(method="get_ensemble_3d")
        ssn = WrapEngineMethod(method="get_lidar_ssn")
        ssd3d = WrapEngineMethod(method="get_lidar_3dssd")
        operational_camera = WrapEngineMethod(method="get_lidar_camera_fusion_labels")

        # ensemble_z = Ensemble3DWrapper(ssd3d=ssd3d_z, clf=alg)

        # metrics
        metrics = get_2d_metrics()
        if args.uniform_eval:
            uniform_models = [
                "3DSSD_epochs_30",
                "3DSSD_xy_0_z_0.05_epochs_30",
                "3DSSD_xy_0.1_z_0_epochs_30",
                "3DSSD_xy_0.05_z_0.25_epochs_30",
                "3DSSD_xy_0.05_z_0.15_epochs_30",
                "3DSSD_xy_0.05_z_0.1_epochs_30",
                "3DSSD_xy_0.05_z_0.05_epochs_30",
                "3DSSD_xy_0.1_z_0.25_epochs_30",
                "3DSSD_xy_0.1_z_0.15_epochs_30",
                "3DSSD_xy_0.1_z_0.1_epochs_30",
                "3DSSD_xy_0.1_z_0.05_epochs_30",
                "3DSSD_xy_0.15_z_0.25_epochs_30",
                "3DSSD_xy_0.15_z_0.15_epochs_30",
                "3DSSD_xy_0.15_z_0.1_epochs_30",
                "3DSSD_xy_0.15_z_0.05_epochs_30",
                "3DSSD_xy_0.25_z_0.25_epochs_30",
                "3DSSD_xy_0.25_z_0.15_epochs_30",
                "3DSSD_xy_0.25_z_0.1_epochs_30",
                "3DSSD_xy_0.25_z_0.05_epochs_30",

                # "3DSSD_uniform_0.001_z_epochs_30",
                # "3DSSD_uniform_0.01_z_epochs_30",
                # "3DSSD_uniform_0.05_z_epochs_30",
                # "3DSSD_uniform_0.1_z_epochs_30",
                # "3DSSD_uniform_0.01_epochs_30",
                # "3DSSD_uniform_0.05_epochs_30",
                # "3DSSD-240621_uniform_0.1_epochs_30",
                # "3DSSD-240621_uniform_0.15_epochs_30",
                # "3DSSD_uniform_0.05_z_only__epochs_30",
                # "3DSSD_uniform_0.1_z_only__epochs_30",
                # "3DSSD_uniform_0.15_z_only__epochs_30",
                # "3DSSD_uniform_0.2_z_only__epochs_30",
                # "3DSSD_uniform_0.25_z_only__epochs_30",

                # "3DSSD_uniform_0.1_z_epochs_40",
                # "3DSSD_uniform_0.15_z_epochs_40",
                # "3DSSD_uniform_0.2_z_epochs_40",
                # "3DSSD_uniform_0.25_z_epochs_40",
                # "3DSSD_uniform_0.3_z_epochs_40",
                # "3DSSD_uniform_0.1_epochs_90",
                # "3DSSD_uniform_0.15_epochs_90",
                # "3DSSD_uniform_0.2_epochs_90",
                # "3DSSD_az_0.0_epochs_30",
                # "3DSSD_uniform_0.05_epochs_30",
                # "3DSSD_uniform_0.15_epochs_30",
                # "3DSSD_uniform_0.25_epochs_30",
                # "3DSSD_uniform_0.5_epochs_30",
                # "3DSSD_uniform_0.1_z_epochs_80",
                # "3DSSD_uniform_0.15_z_epochs_80",
                # "3DSSD_uniform_0.2_z_epochs_80",
            ]

            # build arg_json
            for uniform_model in uniform_models:
                args_json = {}
                words = uniform_model.split("_")
                # todo - fix when new run
                args_json["config"] = "3DSSD_openPCDet"

                args_json["xy"] = float(words[words.index("xy") + 1]) if "xy" in words else 0
                args_json["z"] = float(words[words.index("z") + 1]) if "z" in words else 0
                args_json["model"] = uniform_model

                print(args_json)
                ret = requests.post(f'http://localhost:90/set_model', json=json.dumps(args_json))
                ans = ret.json()
                print(ans)
                cur_run_ssd3d = SSD3DWrapper(ssd3d_docker=90, name=uniform_model)
                e_runner.od3d_evaluation_multiple(model=cur_run_ssd3d, metrics=metrics, data_engine=audi_engine)

        if args.compare:
            # models = [ssd3d, operational_camera, ssn, operational_ensemble3d]
            models = [operational_camera]
            # for model in models:
            #     e_runner.od3d_evaluation_multiple(model=model, metrics=metrics, data_engine=audi_engine)

        if args.camera_pipeline_analysis:
            alg = CameraLidarFusion(data_engine=audi_engine, se_docker=77)

            e_runner.od3d_evaluation_multiple(model=operational_camera, metrics=metrics, data_engine=audi_engine, dirs_only=True)
            for metric in metrics:
                df_instance = pd.read_csv(metric.cur_working_dir.replace("210909", "210905") + "/df_instances.csv")
                for row_idx, elem in df_instance.iterrows():
                    if elem["confusion_matrix"] != "fn":
                        continue

                    # check for 2D BB
                    # print(elem)
                    x1, y1, x2, y2 = ast.literal_eval(elem["bbox"])
                    fn_label = Label2D(cls=elem["cat"], score=1, x1=x1, y1=y1, x2=x2, y2=y2)

                    engine_idx = int(elem["engine_idx"])

                    # missing 2D label
                    labels_2d: list[Label2D] = audi_engine.get_2d_label(engine_idx, kind="yolor")
                    maxes = np.asarray([x.get_iou(fn_label) for x in labels_2d])

                    if maxes.max() <= 0.1:
                        df_instance.loc[row_idx, "fail_cause"] = "no_2d_detection"
                        continue

                    labels_3d = audi_engine.get_3d_label(engine_idx)
                    # label =

                    predicted: Label3D = alg.run_algorithm([labels_2d[maxes.argmax()]], lidar_df=audi_engine.get_lidar_df(engine_idx))[0].to_label_3d()

                    dist = np.linalg.norm([predicted.tx-elem.tx, predicted.ty-elem.ty, predicted.tz-elem.tz])
                    if dist >= 2:
                        df_instance.loc[row_idx, "fail_cause"] = "wrong_clustering"
                    else:
                        print("what??", dist)
                    # print(predicted)

                    # print(max_iou)

        if args.grid_search:
            # python3 Evaluation/run_eval.py --od3d --audi 600 --plot-every 200 --grid_search
            # Ensemble 3D
            somelists = [
                np.linspace(0.0, 1, 3).round(2),
                np.linspace(0.0, 0, 1).round(2),
                np.linspace(0.0, 1, 3).round(2),
                np.linspace(0.0, 1, 3).round(2),
                np.linspace(0.25, 0.5, 2).round(2),
                np.linspace(0.5, 1, 3).round(2),
                np.linspace(0.0, 1.0, 3).round(2),
                np.linspace(0.0, 1.0, 3).round(2),
            ]
            # somelists = [
            #     [1],
            #     [0],
            #     [1],
            #     [0],
            #     [0.5],
            #     [0.5],
            #     [1],
            #     [1],
            #
            # ]
            # debug
            # for element in itertools.product(*somelists):
            #     run_model(Ensemble3D(audi_engine, *element), audi_engine)

            with Pool(10) as p:
                lst = list(itertools.product(*somelists))
                np.random.shuffle(lst)
                models = [Ensemble3D(audi_engine, *element) for element in lst]
                p.starmap(run_model, zip(models, [audi_engine] * len(models)))

        if args.grid_search_clf:
            # clf
            # python3 Evaluation/run_eval.py --dataset audi --audi 100 --od3d --n-plots 5 --gclf  pedestrian
            base_config = CameraLidarFusion.get_config(config="v2")
            configs = [base_config]
            for min_points_per_cluster in [1, 2, 3]:
                for dbscan_epsilon in [0.1, 0.3, 0.5, 0.8, 1, 1.2, 1.5]:
                    for dbscan_min_points in [1, 2, 3, ]:
                        if dbscan_min_points > min_points_per_cluster:
                            continue
                        new_config = deepcopy(base_config)
                        new_config["bb_extraction"]["m_min_points_per_cluster"] = min_points_per_cluster
                        new_config["clustering"]["dbscan_epsilon"] = dbscan_epsilon
                        new_config["clustering"]["dbscan_min_points"] = dbscan_min_points
                        new_config["version"] = f"{min_points_per_cluster}_{dbscan_epsilon}_{dbscan_min_points}"
                        configs.append(new_config)
            for config in configs:
                pprint(config)
                model = CameraLidarFusion(data_engine=audi_engine, se_docker=77, config=config)
                e_runner.od3d_evaluation_multiple(model=model, metrics=metrics,
                                                  data_engine=audi_engine, force=True)

        if args.projection_error_analysis:
            # mu = 0
            # alpha = np.asarray([0.00306422, 0.01548815, -0.003538, 0.00010452, -0.00090432, -0.00058356])
            # sigma = [0.1, 0.1, 0.1, 0.01, 0.01, 0.01]
            # beta = np.ones(len(alpha))
            #
            # def get_6dof_estimation():
            #     e = np.random.normal(mu, sigma, len(alpha))
            #     X = np.random.normal(mu, sigma, len(alpha))
            #     Y = (alpha + beta * X + e)
            #     return Y
            #
            # matrix = pd.DataFrame()
            # matrix = matrix.append({i: v for i, v in enumerate(np.zeros(6))}, ignore_index=True)
            #
            # for i in range(10):
            #     arr = get_6dof_estimation()
            #     matrix = matrix.append({i: v for i, v in enumerate(arr)}, ignore_index=True)
            # matrix = matrix.rename(columns={0: "theta_z", 1: "theta_x", 2: "theta_y", 3: "tx", 4: "ty", 5: "tz"})
            # matrix.to_csv(f"{module_path}/calibration_errors.csv")
            # print(matrix)

            # yaw, pitch, roll, tx, ty, tz
            for error_idx in range(6):
                if error_idx <= 2:
                    errors = np.linspace(-2, 2, 21).round(2)
                else:
                    errors = np.linspace(-0.4, 0.4, 10).round(2)

                # for error in list(itertools.product(*somelists)):
                for error in errors:
                    if error == 0:
                        continue
                    audi_engine = AudiEngine(base_path="/workspace/HDD/open_source/audi/", val=val_flag)
                    audi_engine.update_df_range(0, 300)
                    lidar_df_func = audi_engine.get_lidar_df
                    alg = CameraLidarFusion(data_engine=audi_engine, se_docker=77)

                    audi_engine.calibration_errors = np.zeros(6)
                    if not np.all(error == 0):
                        audi_engine.calibration_errors[error_idx] = error
                    else:
                        print("only zeros", error)

                    err_str = "_".join(str(x) for x in audi_engine.calibration_errors)
                    alg.config_version = f"{alg.config_version}_6dof_error_{err_str}"
                    # arguments.append((alg, audi_engine))
                    e_runner.od3d_evaluation_multiple(model=alg, metrics=metrics, data_engine=audi_engine, force=True)
                    # [matrix.to_csv(metric.cur_working_dir + "/calibration_errors.csv") for metric in metrics]

            # with Pool(3) as p:
            #     p.starmap(run_model, arguments)

        if False:
            pixel_error = [(0, 5), (6, 10), (11, 15), (16, 25), (26, 35), (36, 50), (51, 100)]
            lidar_df_func = audi_engine.get_lidar_df
            for error in pixel_error:
                alg = CameraLidarFusion(data_engine=audi_engine, se_docker=77)

                def mock_func(*args, **kwargs):
                    df = lidar_df_func(*args, **kwargs)
                    sign = 1 if np.random.random() < 0.5 else -1
                    df["x_pixel"] += sign * np.random.randint(low=error[0], high=error[1], size=len(df))
                    df["y_pixel"] += sign * np.random.randint(low=error[0], high=error[1], size=len(df))
                    return df

                audi_engine.get_lidar_df = mock_func
                audi_engine.get_predictions_dir = lambda: None
                alg.config_version = f"{alg.config_version}_pixel_error_{error[0]}_{error[1]}"
                e_runner.od3d_evaluation_multiple(model=alg, metrics=metrics, data_engine=audi_engine, force=True)

    if args.se:
        se_models = [
            "3DSSD_size_estimation_Car_Truck_Van",
            "3DSSD_size_estimation_Car_Truck_Van_patch_size_0.0_epochs_30",
            "3DSSD_size_estimation_Car_Truck_Van_patch_size_0.1_epochs_30",
            "3DSSD_size_estimation_Car_Truck_Van_patch_size_0.5_epochs_30",
            "3DSSD_size_estimation_Car_Truck_Van_patch_size_0.25_epochs_30",

        ]
        metrics = get_se_metrics()
        for se_model in se_models:
            cur_se = SizeEstimationWrapper(model=se_model)
            e_runner.od3d_evaluation_multiple(model=cur_se, task="size_estimation",
                                              metrics=metrics, data_engine=audi_engine)

    """
    Object Detection
    """
    if args.od:
        metrics = get_2d_metrics(d_iou=0.5)
        if args.compare:
            # python3 Evaluation/run_eval.py --dataset waymo --waymo 1000 --od --n-plots 5 --c --train --e pedestrian
            # c = CarContext()
            # c.yolo_thresh, c.d7_thresh, c.iou_thresh, c.nms_thresh = 0.4, 0.2, 0.4, 0.65
            # ped_context = PedContext()
            models = [YoloRWrapper(), EfficientDetWrapper(gpu=0), YoloWrapper(gpu=0)]
            # models = [EfficientDetWrapper(gpu=0), YoloRWrapper(), YoloWrapper(gpu=0), EnsembleWrapper(contexts=[c])]
            # models = [EnsembleWrapper(contexts=[c])]
            for model in models:
                model.name = f"compare_{model.get_name()}"
                e_runner.od3d_evaluation_multiple(model=model, data_engine=data_engine, metrics=metrics,
                                                  task="od", attr="get_2d_label")

        if args.grid_search:
            #  python3 Evaluation/run_eval.py --waymo 0 --od --n-plots 5 --g pedestrian
            somelists = [
                np.linspace(0.0, 1, 4).round(2),
                np.linspace(0.0, 1, 4).round(2),
                np.linspace(0.0, 1, 4).round(2),
                np.linspace(0.0, 1, 3).round(2),
                np.linspace(0.5, 1, 4).round(2),
            ]

            lst = list(itertools.product(*somelists))
            np.random.shuffle(lst)
            models = [EnsembleWrapper(*element) for element in lst]

            # print(models[0])
            # for model in models:
            #     run_model_2d(model, data_engine, metrics)
            with Pool(8) as p:
                p.starmap(run_model_2d, zip(models, [data_engine] * len(models), [metrics] * len(models)))


if __name__ == '__main__':
    # main_pool = Pool(6)
    main()
