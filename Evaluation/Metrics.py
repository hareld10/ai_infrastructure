from pprint import pprint

import pandas as pd
import cv2
from matplotlib import gridspec
from sklearn.cluster import KMeans, DBSCAN
import os
import glob
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib import cm
import matplotlib
from scipy import stats
from tqdm import tqdm
import pathlib
import pickle
import sys
from collections import defaultdict

from Evaluation.occupancy_grid import OccupancyGrid
from wiseTypes.Label import Label
from wiseTypes.Label3D import Label3D
from wiseTypes.classes_dims import Unclassified


class Metric:
    def __init__(self, name):
        self.name = name
        self.cur_working_dir = ""
        self.images_path = ""
        return

    def get_xlabel(self):
        return ""

    def calculate(self, frame_results):
        """

        @param frame_results:
        @return: dict (class : conf_matrix)
        """
        return

    def get_name(self):
        return self.name


class CenterOverlay(Metric):
    def __init__(self, label_confidence=0, detections_cfar=0):
        self.label_confidence = round(label_confidence, 2)
        self.detections_cfar = round(detections_cfar, 2)
        name = "centers_overlay_" + str(self.label_confidence) + "_" + str(self.detections_cfar)
        super().__init__(name=name)
        self.fix_detection_class = True
        return

    def get_threshold_str(self):
        # return "Confidence | label: " + str(self.label_confidence) + ", detection: " + str(self.detections_confidence)
        return str(self.detections_cfar) + "dB"

    def calculate(self, frame_results):
        class_results = defaultdict(dict)
        for c in frame_results.get_labels_classes():
            labels = frame_results.get_objects_by_cls_and_score_as_list(cls=c, score=self.label_confidence, pred=False)
            predictions = frame_results.get_objects_by_cls_and_score_as_list(cls="unclassified", score=self.detections_cfar, pred=True)
            predictions = np.hstack([predictions, frame_results.get_objects_by_cls_and_score_as_list(cls=c, score=self.detections_cfar, pred=True)])

            gt_boxes_flags = np.zeros(len(labels))  # mark if box has been predicted

            tp, fp, fn = 0, 0, 0
            for idx, prediction in enumerate(predictions):
                match = False
                for label_idx, label in enumerate(labels):
                    x1, y1, x2, y2 = label.get_bev_bbox()
                    if not (x1 <= prediction.tx <= x2 and y1 <= prediction.ty <= y2):
                        continue

                    if not gt_boxes_flags[label_idx]:
                        gt_boxes_flags[label_idx] = True
                        match = True
                        tp += 1
                        if self.fix_detection_class:
                            prediction.cls = label.cls
                        break
                if not match:
                    fp += 1

            fn = (gt_boxes_flags == 0).sum()
            class_results[c] = {"tp": tp, "fp": fp, "fn": fn}
        return class_results


class CenterInBB(Metric):
    def __init__(self,):
        super().__init__(name=str(type(self).__name__))
        return

    def calculate(self, frame_results):
        df = pd.DataFrame()
        for c in frame_results.get_labels_classes():
            labels = frame_results.get_objects_by_cls_and_score_as_list(cls=c, score=0, pred=False)
            predictions = frame_results.get_objects_by_cls_and_score_as_list(cls=c, score=0, pred=True)
            gt_boxes_flags = np.zeros(len(labels))  # mark if box has been predicted

            tp, fp, fn = 0, 0, 0
            for idx, prediction in enumerate(predictions):
                match = False
                for label_idx, label in enumerate(labels):
                    x1, y1, x2, y2 = label.get_bev_bbox()
                    if not (x1 <= prediction.tx <= x2 and y1 <= prediction.ty <= y2):
                        continue

                    if not gt_boxes_flags[label_idx]:
                        gt_boxes_flags[label_idx] = True
                        match = True
                        tp += 1
                        break
                if not match:
                    fp += 1

            fn = (gt_boxes_flags == 0).sum()

            df.loc["tp", c] = tp
            df.loc["fp", c] = fp
            df.loc["fn", c] = fn
        return df


class BEV(Metric):
    def __init__(self, score, iou=0.25):
        super().__init__(name=str(type(self).__name__))
        self.score = round(score, 2)
        self.iou = round(iou, 2)
        self.debug = False
        return

    def __repr__(self):
        return f'iou_{self.iou}_{self.score}'

    def calculate(self, frame_results, instances=True):
        df = pd.DataFrame()
        df_instances = pd.DataFrame()
        # print("#"*20)
        for c in frame_results.get_labels_classes():
            labels : list[Label] = frame_results.get_objects_by_cls_and_score_as_list(cls=c, score=0, pred=False)
            predictions : list[Label]= frame_results.get_objects_by_cls_and_score_as_list(cls=c, score=0, pred=True)

            # print(c)
            # print("labels")
            # pprint(labels)
            #
            # print("predictions")
            # pprint(predictions)
            # clean annotations
            for px in predictions:
                px.annotate = ""
            for py in labels:
                py.annotate = ""

            gt_boxes_flags = np.zeros(len(labels))  # mark if box has been predicted
            fn_boxes_flags = np.zeros(len(labels))

            tp, fp, fn = 0, 0, 0

            for idx, prediction in enumerate(predictions):
                match = False
                for idx2, prediction2 in enumerate(predictions):
                    if idx == idx2:
                        continue
                    if prediction2.get_iou(prediction) >= 0.85:
                        fp += 1

                # todo check if it might be problematic
                prediction.annotate += f"s={prediction.score}"
                if prediction.score < self.score:
                    continue
                prediction.annotate += f", iou={round(max([x.get_iou(prediction) for x in labels]), 2)}"
                for label_idx, label in enumerate(labels):
                    cur_iou = label.get_iou(prediction)
                    if self.debug and cur_iou > 0:
                        print("cur_iou", cur_iou)
                    if cur_iou > 0:
                        fn_boxes_flags[label_idx] = True
                    if cur_iou < self.iou:
                        continue

                    if not gt_boxes_flags[label_idx]:
                        gt_boxes_flags[label_idx] = True
                        match = True
                        tp += 1
                        if instances:
                            df_instances = df_instances.append({**prediction.get_dict(), **{"confusion_matrix": "tp"}}, ignore_index=True)
                        prediction.annotate = "tp"
                        break
                if not match:
                    fp += 1
                    prediction.annotate = "fp"
                    if instances:
                        df_instances = df_instances.append({**prediction.get_dict(), **{"confusion_matrix": "fp"}}, ignore_index = True)

            fn = np.logical_and(fn_boxes_flags == 0, gt_boxes_flags == 0).sum()
            if instances:
                for fn_idx, val in enumerate(fn_boxes_flags):
                    if val == 0 and gt_boxes_flags[fn_idx] == 0:
                        labels[fn_idx].annotate += "fn"
                        df_instances = df_instances.append({**labels[fn_idx].get_dict(), **{"confusion_matrix": "fn"}}, ignore_index=True)

            df.loc["tp", c] = tp
            df.loc["fp", c] = fp
            df.loc["fn", c] = fn
        return df, df_instances


class Heading(Metric):
    def __init__(self, heading_diff):
        self.heading_diff = heading_diff
        super().__init__(name=str(type(self).__name__))

    def __repr__(self):
        return f'heading_{self.heading_diff}'

    def calculate(self, frame_results, instances=True):
        df_instances = pd.DataFrame()
        df = pd.DataFrame()

        for label, pred in zip(frame_results.labels, frame_results.predictions):
            if pred is None:
                continue
            similiarity = label.get_heading_similiarity(pred)

            df_instances = df_instances.append({**pred.get_dict(), **{"confusion_matrix": "tp",
                                                                      "similiarity": similiarity, "iou": label.get_iou(pred),
                                                                      "label_heading": label.heading}}, ignore_index=True)

        return df, df_instances


class OccupancyGridMetric(Metric):
    def __init__(self, detections_cfar=0):
        self.detections_cfar = round(detections_cfar, 2)
        name = "occupancy_grid_" + str(self.detections_cfar)
        super().__init__(name=name)
        self.fix_detection_class = True
        self.cluster_compare_metric = OccupancyGrid.abs_metric
        self.o_grid = None
        return

    def get_threshold_str(self):
        return str(self.detections_cfar) + "dB"

    def get_xlabel(self):
        return "FFT Power | dB"

    def calculate(self, frame_results):
        predictions = frame_results.get_objects_by_cls_and_score(cls=True, score=self.detections_cfar, pred=True)
        self.o_grid = OccupancyGrid()

        # GT Clusters
        self.o_grid.add_clusters(clusters_dict=frame_results.labels, gt=True)
        self.o_grid.add_clusters(clusters_dict=predictions, detection=True)

        res_dict, conf_matrix = self.o_grid.evaluate_clusters_vs_clusters(gt_clusters_voxels=self.o_grid.gt_voxels_map,
                                                                          det_clusters_voxels=self.o_grid.detection_voxels_map,
                                                                          compare_metric=self.cluster_compare_metric)
        return {Unclassified().name: conf_matrix}
