import os
import shutil
from glob import glob
from tqdm import tqdm
import sys


# Build Hirarchy

class ChangeNames:
    def __init__(self):
        self.cur_base = None
        return

    def rename_dir(self, src, dest):
        actual_src = self.cur_base + src
        if not os.path.exists(actual_src):
            print("rename_dir: src don't exists", actual_src)
            return
        actual_dest = self.cur_base + dest
        os.rename(actual_src, actual_dest)

    def move_dir(self, src, dest):
        actual_src = self.cur_base + src
        if not os.path.exists(actual_src):
            print("move_dir: src don't exists", actual_src)
            return
        actual_dest = self.cur_base + dest
        shutil.move(actual_src, actual_dest)

    def rename_file(self, src_dir, postfix, suffix_before, suffix_after):
        actual_src = self.cur_base + src_dir
        if not os.path.exists(actual_src):
            print("rename_file: src don't exists", actual_src)
            return

        f_names = glob(actual_src + "/*." + str(postfix))
        print("rename_file: found", len(f_names), "in", actual_src)
        for f_name in tqdm(f_names):
            base = os.path.split(f_name)[0]
            name = os.path.split(f_name)[-1]
            if name.startswith(suffix_after):
                continue
            new_name = name.replace(suffix_before, suffix_after)
            dest = base + "/" + new_name
            os.rename(f_name, dest)
        return

    def process_dir(self, base_dir_path):
        self.cur_base = base_dir_path
        # Make Dirs
        aux_path = base_dir_path + "/aux/"
        os.makedirs(aux_path, exist_ok=True)

        camera_data = base_dir_path + "/camera_data/"
        os.makedirs(camera_data, exist_ok=True)

        camera_labels = base_dir_path + "/camera_labels/"
        os.makedirs(camera_labels, exist_ok=True)

        lidar_data = base_dir_path + "/lidar_data/"
        os.makedirs(lidar_data, exist_ok=True)

        lidar_labels = base_dir_path + "/lidar_labels/"
        os.makedirs(lidar_labels, exist_ok=True)

        radar_data = base_dir_path + "/radar_data/"
        os.makedirs(radar_data, exist_ok=True)

        # change names
        self.rename_dir("3DVD", "lidar_3d")
        self.rename_dir("rgbCamera", "camera_rgb")
        self.rename_dir("lidar_semantics_label", "lidar_seg")
        self.rename_dir("camera_seg", "camera_seg_class")
        self.rename_dir("cuda_pc", "cuda_delta")

        # move
        self.move_dir("lidar_3d", "/lidar_labels/")
        self.move_dir("lidar_seg", "/lidar_labels/")
        self.move_dir("lidar_pc", "/lidar_data/")
        self.move_dir("camera_rgb", "/camera_data/")

        self.move_dir("camera_det_EfficientDetD7", "/camera_labels/")
        self.move_dir("camera_det_ensemble", "/camera_labels/")
        self.move_dir("camera_det_yoloV3", "/camera_labels/")
        self.move_dir("camera_seg_class", "/camera_labels/")
        self.move_dir("camera_seg_soft", "/camera_labels/")

        self.move_dir("cuda_delta", "/radar_data/")
        self.move_dir("cuda_4dfft", "/radar_data/")
        self.move_dir("mimo_raw", "/radar_data/")
        self.move_dir("ppa_raw", "/radar_data/")
        self.move_dir("radar_raw", "/radar_data/")
        self.move_dir("S_ANT", "/radar_data/")

        # change names
        self.rename_file("/lidar_labels/lidar_3d/", postfix="json", suffix_before="3dvd_label", suffix_after="lidar_3d")
        self.rename_file("/camera_labels/camera_det_EfficientDetD7/", postfix="json", suffix_before="camera_det", suffix_after="camera_det_EfficientDetD7")
        self.rename_file("/camera_labels/camera_seg_class/", postfix="npz", suffix_before="camera_seg", suffix_after="camera_seg_class")

        self.rename_file("/radar_data/cuda_delta/", postfix="csv", suffix_before="pc", suffix_after="cuda_delta")

        self.rename_file("/camera_data/camera_rgb/", postfix="png", suffix_before="rgbCamera", suffix_after="camera_rgb")

        self.rename_file("/lidar_labels/lidar_seg/", postfix="npz", suffix_before="lidar_pc_semantics", suffix_after="lidar_seg")

        # meta_data
        f_names = glob(base_dir_path + "/metadata/*.json")
        print("metadata: found", len(f_names))
        for f_name in tqdm(f_names):
            base = os.path.split(f_name)[0]
            name = os.path.split(f_name)[-1]
            if "cuda_4dfft" in name:
                if name.startswith("metadata_cuda_4dfft"):
                    continue
                new_name = name.replace(" cuda_4dfft.", ".")
                new_name = new_name.replace("metadata", "metadata_cuda_4dfft")
            elif "cuda_pc" in name:
                if name.startswith("metadata_cuda_delta"):
                    continue
                new_name = name.replace(" cuda_pc.", ".")
                new_name = new_name.replace("metadata", "metadata_cuda_delta")
            else:
                continue
            dest = base + "/" + new_name
            os.rename(f_name, dest)

        # move to aux
        for f_name in glob(base_dir_path + "/*.csv"):
            name = os.path.split(f_name)[-1]
            dest = aux_path + name
            shutil.move(f_name, dest)
        for f_name in glob(base_dir_path + "/*.pkl"):
            name = os.path.split(f_name)[-1]
            dest = aux_path + name
            shutil.move(f_name, dest)

        sync_data = aux_path + "/filteredData.csv"
        dest = aux_path + "/sync_data_radar_lidar.csv"
        if os.path.exists(sync_data):
            os.rename(sync_data, dest)

        # delete
        # pcRadar = base_dir_path + "/pcRadar/"
        # if os.path.exists(pcRadar):
        #     shutil.rmtree(pcRadar)
        # S_ANT = base_dir_path + "/S_ANT/"
        # if os.path.exists(S_ANT):
        #     shutil.rmtree(S_ANT)


if len(sys.argv) != 2:
    print("Dont forget args")

dir_to_process = sys.argv[1]
print("processing ", dir_to_process)
c = ChangeNames()
c.process_dir(dir_to_process)

# for directory in os.listdir(sys.argv[1]):
#     dir_to_process = sys.argv[1] + "/" + directory + "/"
#     print("processing ", dir_to_process)
#     # example_dir = "//media/amper/data_backup7/76mWF/example_2_200902_lidar_drive_highway_W92_M15_RF1/"
#     c = ChangeNames()
#     c.process_dir(dir_to_process)
