import importlib
import numpy as np
import os
import torch.nn.functional as F
from tqdm import tqdm
import glob
import sys
import json
import os
import sys
from pprint import pprint
import time
import datetime
import argparse
import matplotlib.pyplot as plt
import pandas as pd
from tqdm import tqdm
from PIL import Image
import cv2
import torch
import matplotlib


class ScenarioRunner:
    def __init__(self):
        self.scenarios_dir = "/mnt/hdd/200625/"
        self.output_dir = "/mnt/hdd/scenarios_output/"
        os.makedirs(self.output_dir, exist_ok=True)
        self.selected_scenes = []
        self.model_name = "radar_percep_net_v9"
        self.task = "road_veh_ped"
        self.pretrained_path = "./weights/radar_percep_rvp_st.v3.4.1.leopard_radar_percep_net_v9_best_model.pth"
        self.model, self.net_module = None, None
        self.output_shape = (940 // 6, 1824 // 6)
        self.gpus_list = [0]
        self.batch_size = 8
        self.doppler_ch = 48
        self.zero_pad = 0
        self.output_ch = 4
        self.plot_thresh = 0.1
        self.color_palette = [(255, 0, 0), (0, 255, 0), (0, 0, 255)]

        fov_az = 110  # deg
        self.min_az_fov = -fov_az // 2
        self.max_az_fov = fov_az // 2

        fov_el = 40  # deg
        self.min_el_fov = -fov_el // 2
        self.max_el_fov = fov_el // 2

        self.ch_order = np.array(
            [[0, 4], [1, 5], [0, 6], [1, 7], [0, 8], [1, 9], [0, 10], [1, 11], [0, 12], [1, 13], [0, 14], [1, 15],
             [0, 16], [1, 17], [0, 18],
             [1, 19], [2, 2], [3, 3], [2, 4], [3, 5], [2, 6], [3, 7], [2, 8], [3, 9], [2, 10], [3, 11], [2, 12],
             [3, 13], [2, 14], [3, 15],
             [2, 16], [3, 17], [4, 0], [5, 1], [4, 2], [5, 3], [4, 4], [5, 5], [4, 6], [5, 7], [4, 8], [5, 9], [4, 10],
             [5, 11], [4, 12], [5, 13],
             [4, 14], [5, 15]])

        self.results_out = None
        return

    def get_model_params(self):
        model_params = {'output_shape': self.output_shape, 'gpus_list': self.gpus_list,
                        'batch_size': self.batch_size,
                        'doppler_ch': self.doppler_ch, 'zero_pad': self.zero_pad, 'output_ch': self.output_ch}
        return model_params

    def pre_process(self):
        self.update_selected_scenes()
        sys.path.insert(0, "./models")
        sys.path.insert(0, "./weights")
        self.net_module = importlib.import_module(self.model_name)
        self.model = self.net_module.net(**self.get_model_params())

        model_name = os.path.join(self.pretrained_path)
        self.model.load_state_dict(torch.load(model_name, map_location=lambda storage, loc: storage))
        self.model = self.model.cuda(self.gpus_list[0])

        self.results_out = self.output_dir + "/desired_name" + self.model_name
        os.makedirs(self.results_out, exist_ok=True)
        return True

    def update_selected_scenes(self):
        for i in glob.glob(self.scenarios_dir + "/*/filtered_data.csv"):
            s_name = os.path.split(i)[0]
            self.selected_scenes.append(s_name)

    def process_S_ANT(self, path_rd):
        S_ANT = np.load(path_rd)

        # Cutoff max range at 150m
        if S_ANT.shape[0] == 796:
            S_ANT = S_ANT[:600, :, :]
        else:
            S_ANT = S_ANT[4:604, :, :]

        SRe = torch.from_numpy(S_ANT.real)
        SIm = torch.from_numpy(S_ANT.imag)

        # 2D fft
        s_t = torch.zeros(SRe.shape[2], SRe.shape[0], 6, 20, 2)
        s_t[:, :, self.ch_order[:, 0], self.ch_order[:, 1], 0] = SRe.permute(2, 0, 1)
        s_t[:, :, self.ch_order[:, 0], self.ch_order[:, 1], 1] = SIm.permute(2, 0, 1)

        # Zero padding
        if not self.zero_pad == 0:
            s_t = F.pad(s_t, (0, 0, 0, 20 * self.zero_pad - 20, 0, 6 * self.zero_pad * 2 - 6), "constant", 0)

        ffta = torch.fft(s_t, 2, normalized=True)  # torch.Size([48, 600, 6, 20, 2]) (dop,rng,el,az,real/imag)

        array = torch.zeros(ffta.shape[0] * 2, ffta.shape[1], ffta.shape[2], ffta.shape[3])

        array[::2, :, :, :] = ffta[:, :, :, :, 0]
        array[1::2, :, :, :] = ffta[:, :, :, :, 1]

        # fft shift and filter az +-55deg
        axis = 3
        array = torch.roll(array, shifts=array.shape[axis] // 2, dims=axis)
        min_idx = int(np.ceil(array.shape[axis] // 2 + self.min_az_fov / 180 * array.shape[axis]) - 1)
        max_idx = int(np.floor(array.shape[axis] // 2 + self.max_az_fov / 180 * array.shape[axis]) + 1)
        array = array[:, :, :, min_idx:max_idx]

        return array

    def run_scene(self, base_path):
        scene_name = os.path.split(base_path)[-1]
        scene_output = self.results_out + "/" + scene_name
        os.makedirs(scene_output, exist_ok=True)

        df = base_path + "/filtered_data.csv"
        df = pd.read_csv(df)
        for idx in tqdm(range(len(df))):
            rgb_path = base_path + "/rgbCamera/rgbCamera_" + str(df.iloc[idx]["timestampCamera"]) + ".png"
            s_ant_path = base_path + "/S_ANT/S_ANT_" + str(df.iloc[idx]["timestampRadar"]) + ".npy"
            model_input = self.process_S_ANT(s_ant_path)

            im = cv2.imread(rgb_path)

            with torch.no_grad():
                model_input_batched = torch.cat(8*[torch.unsqueeze(model_input, 0)])
                pred = self.model(model_input_batched.cuda(self.gpus_list[0]))

            # Plot road pred
            probs = torch.softmax(pred[0].squeeze(0), axis=0)
            probs_max, pred_max = torch.max(probs, axis=0)

            probs = probs.cpu().numpy()
            probs_max = probs_max.cpu().numpy()
            pred_max = pred_max.cpu().numpy()

            pred_plot = np.zeros([pred_max.shape[0], pred_max.shape[1], 3], dtype=np.uint8)
            pred_plot[pred_max == 1] = self.color_palette[0]  # road
            pred_plot[pred_max == 2] = self.color_palette[1]  # vehicle
            pred_plot[pred_max == 3] = self.color_palette[2]  # pedestrian

            pred_plot = cv2.resize(pred_plot, (im.shape[1], im.shape[0]))
            keep = cv2.addWeighted(pred_plot, 0.4, im, 0.6, 0)
            cv2.imwrite(scene_output + "/" + str(df.iloc[idx]["timestampCamera"]) + ".png", keep)

            plt.figure('results', figsize=(25, 13))
            plt.style.use('dark_background')
            plt.suptitle(self.task + '\n' + s_ant_path + '\n' +  str(df.iloc[idx]["timestampRadar"]) )

        return

    def run(self):
        self.pre_process()
        pprint(self.selected_scenes)

        for scene in self.selected_scenes:
            print("Running", scene)
            self.run_scene(base_path=scene)
        return
