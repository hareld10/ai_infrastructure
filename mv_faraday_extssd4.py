import os

from DataPipeline.src.GetEngines import get_engines

srcs = ["/media/amper/data_backup11/250mWF/2_2_281021_drive_1_WF198_M32_F3/",
        "/media/amper/data_backup11/250mWF/2_3_281021_drive_2_bridges_WF198_M32_F3/",
        "/media/amper/data_backup11/250mWF_raw/211028/2_2_281021_drive_1_WF198_M32_F3/radarRawData/",
        "/media/amper/data_backup11/250mWF_raw/211028/2_3_281021_drive_2_bridges_WF198_M32_F3/radarRawData/",

        "/media/amper/data_backup6/250mWF_4/2_2_011121_drive_1_bridges_WF198_M32_F3/",
        "/media/amper/data_backup6/250mWF_4/2_3_011121_drive_2_WF198_M32_F3/",
        "/media/amper/data_backup6/250mWF_raw/211101/2_2_011121_drive_1_bridges_WF198_M32_F3/radarRawData/",
        "/media/amper/data_backup6/250mWF_raw/211101/2_3_011121_drive_2_WF198_M32_F3/radarRawData/"
        ]

dests = ["//extSSD4/250mWF_4/281021_drive_1_WF198_M32_F3/",
         "//extSSD4/250mWF_4/281021_drive_2_WF198_M32_F3/",
         "//extSSD4/250mWF_4/281021_drive_1_WF198_M32_F3/radar_data/radar_raw/",
         "//extSSD4/250mWF_4/281021_drive_2_WF198_M32_F3/radar_data/radar_raw/",

         "/extSSD4/250mWF_4/011121_drive_1_bridges_WF198_M32_F3/",
         "/extSSD4/250mWF_4/011121_drive_2_WF198_M32_F3/",
         "/extSSD4/250mWF_4/011121_drive_1_bridges_WF198_M32_F3//radar_data/radar_raw//",
         "/extSSD4/250mWF_4/011121_drive_2_WF198_M32_F3//radar_data/radar_raw/"
         ]


dest_prefix = "/media/amper/"
dests = [f"{dest_prefix}/{x}" for x in dests]

for src, dest in zip(srcs, dests):
    # dest_cmd = " faraday@192.168.1.189:%s" % dest
    # print("src=", src)
    # print("dest=", dest_cmd)

    # cmd_base = "//usr/bin/rsync -rahvtlzL --rsh=\"/usr/bin/sshpass -p wiseai2018 ssh -o StrictHostKeyChecking=no -l faraday\" "
    # final_cmd = cmd_base + src + dest_cmd
    cmd_base = "//usr/bin/rsync -rahvr "
    dest_cmd = dest
    final_cmd = cmd_base + " " + src + " " + dest_cmd

    print(final_cmd)
    os.system(final_cmd)