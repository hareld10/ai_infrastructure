import pickle

import numpy as np
import os
import matplotlib.pyplot as plt
import pandas as pd

from DataPipeline.DNNs.wrappers.exceptions import NotValidClass
from OpenSource.audi_utils import read_label
from OpenSource.open_source_utils import lidar_to_radar_coordinates
from wiseTypes.Label2D import Label2D
from wiseTypes.Label3D import Label3D
from wiseTypes.SegLabel import SegLabel
from wiseTypes.classes_dims import Unclassified


class Tracker:
    def __init__(self, engine):
        self.engine = engine
        self.tracks_df = {}
        self.colors = {}

        def get_cmap(n, name='hsv'):
            '''Returns a function that maps each index in 0, 1, ..., n-1 to a distinct
            RGB color; the keyword argument name must be a standard mpl colormap name.'''
            return plt.cm.get_cmap(name, n)

        self.num_colors = 20
        self.c_map = get_cmap(self.num_colors)
        self.color_idx = 0

    def __getitem__(self, kind, caller=None):
        df = self.tracks_df.get(kind, None)
        if df is None:
            p = self.engine.get_2d_label(0, kind=kind, path_only=True) if caller is None else caller(idx=0, kind=kind, path_only=True)
            df_path = os.path.split(p)[0] + "/tracks.pkl"

            if os.path.exists(df_path):
                df = pd.read_pickle(df_path)
                print("Tracker: loaded\n", df_path)
                self.tracks_df[kind]: pd.DataFrame = df
                self.tracks_df[kind].set_index("label_uuid", inplace=True)
                return self.tracks_df[kind]
            else:
                print("Tracker 2D couldn't loaded!\n", df_path)
                return None
        return df

    def get_color(self, trk_id):
        color = self.colors.get(trk_id, None)
        if color is None:
            color = self.c_map(self.color_idx % self.num_colors)[:3]
            self.color_idx += 1
            self.colors[trk_id] = tuple((np.asarray(color) * 255).tolist())
        return color


class OpenSourceEngine:
    def __init__(self, base_path, base_name, val=False, force_obj=True):
        self.class_key = None
        self.base_name = base_name
        self.base_path = base_path
        self.suffix = "_Val" if val else "_Train"
        self.name = self.base_name + self.suffix
        self.force_obj = force_obj
        self.df = pd.DataFrame()
        self.df_name = "no_df_specified"
        self.start_idx, self.end_idx = 0, None
        self.src_df = None
        self.srcs_path = ""
        self.ssd3d_suffix = ""
        self.drive_context = None
        self.rect_mode = True

        self.tracking = False
        self.tracks_df = {}
        self.set_tracking_mode(self.tracking)

    def set_tracking_mode(self, flag):
        self.tracking = flag
        if flag:
            self.tracks_df = Tracker(engine=self)

    def get_camera_id(self, idx):
        return str(self.get_id(idx))

    def get_2d_label_path(self, idx, kind):
        if kind == "ensemble":
            od_label = self.drive_context.camera_det_ensemble + "/camera_det_ensemble_" + self.get_camera_id(idx) + ".json"
        elif kind == "edet":
            od_label = self.drive_context.camera_det_EfficientDetD7 + "/camera_det_EfficientDetD7_" + self.get_camera_id(idx) + ".json"
        elif kind == "yolo":
            od_label = self.drive_context.camera_det_yoloV3 + "/camera_det_yoloV3_" + self.get_camera_id(idx) + ".json"
        elif kind == "yolor":
            od_label = self.drive_context.camera_det_yolor + "/camera_det_yolor_" + self.get_camera_id(idx) + ".json"
        else:
            od_label = "unknown kind"
        return od_label

    def process_2d_labels(self, od_label, force_obj, kind="", min_score=0.2):
        labels = []
        for instance_id, cur_label in enumerate(od_label):
            obj = cur_label['cat']
            score = cur_label['score']
            if score < min_score:
                continue
            x1, y1, x2, y2 = cur_label['bbox']
            if x1 == 0 and x2 == 0 and y1 == 0 and y2 == 0:
                continue
            uuid = cur_label.get("uuid", None)
            trk_id = None
            # print("uuid=", uuid)
            # if uuid is None:
            #     print("label 2d uuid is None")
            if self.tracking:
                if uuid in self.tracks_df[kind].index:
                    trk_id = self.tracks_df[kind].loc[uuid]["track_uuid"]
                    # print("trk_id=", trk_id.shape)
            try:
                cur_label = Label2D(cls=obj, score=score, x1=x1, y1=y1, x2=x2, y2=y2, force_obj=force_obj)
            except NotValidClass:
                continue
            cur_label.instance_id = instance_id
            cur_label.uuid = uuid

            if self.tracking:
                cur_label.trk_id = trk_id
                cur_label.color = self.tracks_df.get_color(trk_id)
            # print(cur_label.color)
            if cur_label.cls == Unclassified.name:
                continue
            labels.append(cur_label)
        return labels

    def set_wise4_keys(self):
        pass

    def set_rect_mode(self, mode):
        pass

    def create_dirs(self):
        pass

    def get_drive_name(self):
        return self.base_name

    def get_row(self, i):
        if i <= len(self.df):
            return self.df.iloc[i].copy()
        else:
            print("get_row: idx out of bound")

    def set_df(self, df):
        self.df = df

    def get_dt_title(self, idx):
        return str(idx)

    def get_instances_3d_df(self):
        return pd.read_csv(f"{self.base_path}/instances_3d_df.csv")

    def get_predictions_dir(self):
        path = f"{self.base_path}/predictions/"
        os.makedirs(path, exist_ok=True)
        return path

    def __len__(self):
        return len(self.df)

    def update_df_range(self, start_idx, end_idx, shuffle=False):
        self.start_idx, self.end_idx = start_idx, end_idx
        self.df = self.df.iloc[start_idx:end_idx].reset_index(drop=True)
        if shuffle:
            self.df = self.df.sample(frac=1).reset_index(drop=True)

    def sample_df(self, count):
        self.start_idx = -1
        self.df = self.df.sample(count).reset_index(drop=True)

    def sample_df_range(self, range_len):
        start_idx = np.random.randint(0, len(self) - range_len - 1)
        self.update_df_range(start_idx=start_idx, end_idx=start_idx + range_len, shuffle=False)

    def get_name(self):
        return self.name

    def get_image_and_label(self, idx):
        return self.get_image(idx), self.get_2d_label(idx)

    def project_lidar_to_radar(self, pts, semantic=None, filter_fov_flag=False, **kwargs):
        return lidar_to_radar_coordinates(pts)

    def get_image(self, idx, **kwargs):
        return

    def get_processed_delta(self, idx):
        return None

    def get_clusters(self, p_delta=None):
        return {}

    def get_2d_label(self, idx, force_obj=True, kind="ensemble", path_only=False, **kwargs):
        od_label = self.get_2d_label_path(idx, kind=kind)
        if path_only:
            return od_label

        if not os.path.exists(od_label):
            return -2

        od_label = read_label(od_label)
        if not force_obj:
            return od_label
        return self.process_2d_labels(od_label, force_obj=force_obj, kind=kind, **kwargs)

    def get_3d_label(self, idx, kind="ssn", **kwargs):
        if kind == "ssn":
            return self.get_lidar_ssn(idx=idx, **kwargs)
        elif kind == "cc":
            return self.get_lidar_camera_fusion_labels(idx=idx, **kwargs)
        elif kind == "3dssd":
            return self.get_lidar_3dssd(idx=idx, **kwargs)
        elif kind == "ensemble":
            return self.get_ensemble_3d(idx=idx, **kwargs)
        else:
            print("unknown kind")
            return None

    def get_id(self, idx):
        return

    def get_lidar(self, idx):
        return

    def process_lidar_on_image(self, lidar):
        return

    def map_lidar_points_onto_image(self, image_orig, lidar):
        return

    def process_lidar_ssn(self, data, min_score=0.25):
        ids = data["scores_3d"] >= min_score
        labels_ssn = []
        class_names = [
            'bicycle', 'motorcycle', 'pedestrian', 'traffic_cone', 'barrier', 'car',
            'truck', 'trailer', 'bus', 'construction_vehicle'
        ]

        for idx in ids.nonzero()[0]:
            tx, ty, tz, w, l, h, rotation_y, _, _ = data["tensor"][idx]
            score = data["scores_3d"][idx]

            uuid = data["uuids"][idx] if "uuids" in data else None
            label_idx = int(data["labels_3d"][idx])

            # print(class_names[label_idx])
            heading = -(rotation_y + np.pi / 2)
            while heading < -np.pi:
                heading += 2 * np.pi
            while heading > np.pi:
                heading -= 2 * np.pi
            heading += np.pi / 2
            cur_label = Label3D(cls=class_names[label_idx], score=score, tx=tx, ty=ty, tz=tz, heading=heading, width=w, length=l, height=h,
                                engine=self)
            cur_label.uuid = uuid
            labels_ssn.append(cur_label)

        return labels_ssn

    def process_lidar_3dssd(self, boxes, min_score=0):
        labels = []
        if type(boxes["pred_boxes"]) != list:
            boxes3d_lidar = boxes["pred_boxes"].cpu().numpy().astype(float)
            scores = boxes["pred_scores"].cpu().numpy().astype(float)
            clsses = boxes["pred_labels"].cpu().numpy().astype(float)
        else:
            boxes3d_lidar = np.asarray(boxes["pred_boxes"])
            scores = np.asarray(boxes["pred_scores"])
            clsses = np.asarray(boxes["pred_labels"])

        cls_map = {1: "vehicle", 2: "pedestrian", 3: "cyclist"}
        for obj_idx, obj in enumerate(boxes3d_lidar):
            tx, ty, tz, l, h, w, heading = obj
            score = scores[obj_idx]
            if score <= min_score:
                continue
            cur_label = Label3D(cls=cls_map[clsses[obj_idx]], score=score, tx=tx, ty=ty, tz=tz, heading=heading + np.pi / 2, width=w,
                                length=l,
                                height=h, engine=self)
            labels.append(cur_label)
        return labels

    def process_camera_lidar_fusion(self, camera_to_lidar_label, min_score=0):
        labels = []
        for instance_id, bbox in enumerate(camera_to_lidar_label):
            if "3d_center_clustering" not in bbox: continue
            x1, y1, x2, y2 = bbox["bbox"]
            score = bbox["score"]
            if score < min_score:
                continue
            tx, ty, tz = bbox["3d_center_clustering"]
            heading = None
            if "heading" in bbox:
                heading = bbox["heading"]
            if "size_estimation" in bbox:
                tx, ty, tz, l, h, w, _ = bbox["size_estimation"]
                if "heading" not in "bbox":
                    heading = _
            else:
                w, l, h, heading = 2, 2, 2, 0

            cur_label = Label3D(cls=bbox["cat"], score=bbox["score"], tx=tx, ty=ty, tz=tz, heading=heading + np.pi / 2, width=w,
                                length=l, height=h, x1=x1, y1=y1, x2=x2, y2=y2, instance_id=instance_id + 1, engine=self)

            if "associate_cluster_id" in bbox:
                cur_label.associate_cluster = bbox["associate_cluster_id"]
            cur_label.meta_data = bbox
            labels.append(cur_label)
        return labels

    def get_lidar_id(self, idx, **kwargs):
        return self.get_id(idx)

    def get_lidar_3dssd(self, idx, as_obj=True, path_only=False, **kwargs):
        p = f"{self.drive_context.lidar_3dssd}/{self.ssd3d_suffix}lidar_3dssd_{self.get_lidar_id(idx)}.npy"
        if path_only:
            return p

        if not os.path.exists(p):
            print("lidar3dssd doesnt exists\n", p)
            return None

        # os.environ['CUDA_VISIBLE_DEVICES'] = str(1)
        boxes = np.load(p, allow_pickle=True).item()
        if not as_obj:
            return boxes

        return self.process_lidar_3dssd(boxes, **kwargs)

    def get_lidar_camera_fusion_labels(self, idx, as_obj=True, path_only=False, **kwargs):
        label_path = self.drive_context.camera_to_lidar + "/camera_to_lidar_" + str(self.get_lidar_id(idx)) + ".json"
        if path_only:
            return label_path

        if not os.path.exists(label_path):
            print("get_lidar_camera_fusion_labels: Doesn't exists")
            return None

        camera_to_lidar_label = read_label(label_path)

        if not as_obj: return camera_to_lidar_label

        return self.process_camera_lidar_fusion(camera_to_lidar_label, **kwargs)

    def get_lidar_ssn(self, idx, as_obj=True, path_only=False, **kwargs):
        p = f"{self.drive_context.lidar_labels}/lidar_ssn/lidar_ssn_{str(self.get_lidar_id(idx, full=True))}.pkl"
        if path_only:
            return p

        if not os.path.exists(p):
            print("lidar ssn doesnt exists", p)
            return None

        with open(p, 'rb') as handle:
            b = pickle.load(handle)

        if not as_obj:
            return b

        if len(b) == 0:
            return []

        return self.process_lidar_ssn(b, **kwargs)

    def get_ensemble_3d(self, idx, as_obj=True, path_only=False):
        label_path = self.drive_context.ensemble_3d + "/ensemble_3d_" + str(self.get_lidar_id(idx)) + ".json"
        if path_only:
            return label_path

        frame = read_label(label_path)
        if not as_obj:
            return frame

        labels = []
        for obj_idx, obj in enumerate(frame):
            tx, ty, tz, w, l, h, heading = obj["tx"], obj["ty"], obj["tz"], obj["width"], obj["length"], obj["height"], obj["heading"]
            cur_label = Label3D(cls=obj["cat"], score=obj["score"], instance_id=obj_idx,
                                tx=tx, ty=ty, tz=tz, heading=heading, width=w, length=l, height=h, engine=self)
            cur_label.meta_data = obj
            labels.append(cur_label)
        return labels
