import json
import pprint
import numpy as np
# import open3d as o3
import cv2
import json
import numpy.linalg as la
import matplotlib.pylab as pt
import math
import os
import pandas as pd
import matplotlib.pyplot as plt
from pathlib import Path
import glob
from pprint import pprint

from Calibrator.calibrator_utils import project
from DataPipeline.DNNs.wrappers.exceptions import NotValidClass
from DataPipeline.settings import Context
from OpenSource.audi_utils import *

from OpenSource.OpenSource import OpenSourceEngine
from OpenSource.open_source_utils import lidar_to_radar_coordinates
from wiseTypes.Label2D import Label2D
from wiseTypes.Label3D import Label3D
from wiseTypes.SegLabel import SegLabel


class AudiEngine(OpenSourceEngine):
    def __init__(self, base_path="/media/amper/open_source5/audi/", val=False, force_obj=True):
        super().__init__(base_path=base_path, val=val, force_obj=force_obj, base_name="Audi")
        self.class_key = "class"
        self.df_train = pd.read_csv(self.base_path + "camera_lidar_semantic_bboxes/train.csv", dtype={'frameId': str})
        self.df_train['disc'] = self.base_path
        self.df_train['dataset'] = ''

        self.df_val = pd.read_csv(self.base_path + "camera_lidar_semantic_bboxes/val.csv", dtype={'frameId': str})
        self.df_val['disc'] = self.base_path
        self.df_val['dataset'] = ''

        self.df = self.df_train if not val else self.df_val
        self.df = self.df.astype(str)
        self.heading_alpha = 0
        with open(self.base_path + 'camera_lidar_semantic_bboxes/cams_lidars.json', 'r') as f:
            self.config = json.load(f)
        self.force_obj = True
        self.drive_context = Context(base_path=base_path, args=None, index=0, create_dirs=True)

        self.calibration_errors = None
        return

    def set_df(self, df):
        super(AudiEngine, self).set_df(df)
        self.df.loc[:, 'dataset'] = ""

    def get_id(self, idx):
        val = str(self.df.loc[idx, 'frameId'])
        val = int(val[:val.index(".")])
        return f"{val:09d}."

    def get_ids_dict(self, idx):
        row = {
               "timestampCamera": self.get_id(idx)}
        if "timestampLidar" in self.df.keys():
            row["timestampLidar"] = self.get_id(idx)
        return row

    def get_image(self, idx, undistort=True, path_only=False):
        date = self.df.loc[idx, 'date']
        orientation = self.df.loc[idx, 'orientation']

        imgPath = self.df.loc[idx, 'disc'] + '/' + self.df.loc[
            idx, 'dataset'] + '/camera_lidar_semantic_bboxes/' + date + '/camera/' + orientation + '/' + date.split("_")[0] + \
                  date.split("_")[1] + '_camera_' + orientation.split("_")[1] + orientation.split("_")[2] + '_' + self.get_id(idx) + 'png'

        if path_only:
            if undistort:
                imgPath_u = imgPath.replace(self.get_id(idx) + 'png', "undistort_" + self.get_id(idx) + 'png')
                if not os.path.exists(imgPath_u):
                    image_front_center = cv2.imread(imgPath)[..., ::-1]
                    image_front_center = undistort_image(image_front_center, 'front_center', self.config)
                    cv2.imwrite(imgPath_u, image_front_center)
                print(imgPath_u)
                return imgPath_u
            else:
                return imgPath
        # image_front_center = cv2.cvtColor(cv2.imread(imgPath), cv2.COLOR_BGR2RGB)
        image_front_center = cv2.imread(imgPath)[..., ::-1]
        if undistort:
            image_front_center = undistort_image(image_front_center, 'front_center', self.config)
        return image_front_center

    def get_segmentation_label(self, idx, undistort=True):
        date = self.df.loc[idx, 'date']
        orientation = self.df.loc[idx, 'orientation']
        imageId = self.df.loc[idx, 'frameId']

        imgPath = self.df.loc[idx, 'disc'] + '/' + self.df.loc[
            idx, 'dataset'] + '/camera_lidar_semantic_bboxes/' + date + '/label/' + orientation + '/' + date.split("_")[0] + \
                  date.split("_")[1] + '_label_' + orientation.split("_")[1] + orientation.split("_")[2] + '_' + str(imageId) + 'png'

        image_front_center = cv2.imread(imgPath)[..., ::-1]
        if undistort:
            image_front_center = undistort_image(image_front_center, 'front_center', self.config)
        return SegLabel(image_front_center, self, None)  # TODO: what about soft label?

    def get_seg_classes(self):
        """
        Return list of tuples (cls_idx, cls_name)
        """
        ret = list()
        ret.append(["car", (255, 0, 0)])
        ret.append(["car", (200, 0, 0)])
        ret.append(["car", (150, 0, 0)])
        ret.append(["car", (128, 0, 0)])

        ret.append(["bicycle", (182, 89, 6)])
        ret.append(["bicycle", (150, 50, 4)])
        ret.append(["bicycle", (90, 30, 1)])
        ret.append(["bicycle", (90, 30, 30)])

        ret.append(["pedestrian", (204, 153, 255)])
        ret.append(["pedestrian", (189, 73, 55)])
        ret.append(["pedestrian", (239, 89, 191)])

        ret.append(["truck", (255, 128, 0)])
        ret.append(["truck", (200, 128, 0)])
        ret.append(["truck", (250, 128, 0)])

        ret.append(["car", (0, 255, 0)])
        ret.append(["car", (0, 200, 0)])
        ret.append(["car", (0, 150, 0)])

        ret.append(["utility-vehicle", (255, 255, 0)])
        ret.append(["utility-vehicle", (255, 255, 200)])

        ret.append(["sidewalk", (180, 150, 200)])

        ret.append(["sky", (135, 206, 25)])

        ret.append(["buildings", (241, 230, 255)])

        ret.append(["vegetation", (147, 253, 194)])

        # Audi class labels:
        reqClass_audi_road = [(255, 0, 255),  # road
                              (128, 0, 255),  # dashed lanes
                              (255, 193, 37),  # solid lanes
                              (201, 50, 115),  # zebra crossing
                              (200, 125, 210),  # road markings
                              (180, 50, 180),  # drivable cobbleston
                              (110, 110, 0)]  # speed bump

        for req in reqClass_audi_road:
            ret.append(["road", req])
        return ret

    def get_lidar(self, idx):
        date = self.df.loc[idx, 'date']
        orientation = self.df.loc[idx, 'orientation']
        # imageId = self.df.loc[idx, 'frameId']
        # print(date, type(date))
        # print(orientation, type(orientation))
        # print(imageId, type(imageId))
        # print(self.df)
        # print(str(imageId))
        # print(self.df.dtypes)
        file_name_lidar = self.df.loc[idx, 'disc'] + '/' + \
                          self.df.loc[idx, 'dataset'] + \
                          '/camera_lidar_semantic_bboxes/' + \
                          date + '/lidar/' + orientation + '/' + date.split("_")[0] + \
                          date.split("_")[1] + '_lidar_' + orientation.split("_")[1] + \
                          orientation.split("_")[2] + '_' + self.get_id(idx) + 'npz'

        # print(file_name_lidar)
        lidar_front_center = np.load(file_name_lidar)
        return lidar_front_center

    def get_lidar_df(self, idx, path_only=False, add_seg=False):
        lidar_base = f"{self.base_path}/lidar/"
        os.makedirs(lidar_base, exist_ok=True)
        load_path = f"{lidar_base}/lidar_{self.get_id(idx)}csv"

        lidar_pts = self.get_lidar(idx)

        lidar_camera_df = pd.DataFrame(columns=["x", "y", "z"])

        imgfov_pc_pixel, imgfov_pc_velo = self.process_lidar_on_image(lidar_pts)
        lidar_camera_df["x_lidar_camera"] = imgfov_pc_velo[:, 0]
        lidar_camera_df["y_lidar_camera"] = imgfov_pc_velo[:, 1]
        lidar_camera_df["z_lidar_camera"] = imgfov_pc_velo[:, 2]
        lidar_camera_df["r_lidar_camera"] = np.linalg.norm(imgfov_pc_velo[:, :3], axis=-1)

        lidar_camera_df["x_pixel"] = imgfov_pc_pixel[0, :]
        lidar_camera_df["y_pixel"] = imgfov_pc_pixel[1, :]

        lidar_camera_df["orig_x"] = imgfov_pc_velo[:, 4]
        lidar_camera_df["orig_y"] = imgfov_pc_velo[:, 5]
        lidar_camera_df["orig_z"] = imgfov_pc_velo[:, 6]
        lidar_camera_df["r"] = np.linalg.norm(imgfov_pc_velo[:, 4:7], axis=-1)

        projected = lidar_to_radar_coordinates(imgfov_pc_velo[:, 4:7])
        lidar_camera_df["x"] = projected[:, 0]
        lidar_camera_df["y"] = projected[:, 1]
        lidar_camera_df["z"] = projected[:, 2]
        lidar_camera_df["intensity"] = 1

        if add_seg:
            seg_label = self.get_segmentation_label(idx=idx)
            lidar_camera_df["cls"] = seg_label.label[lidar_camera_df["y_pixel"].astype(int), lidar_camera_df["x_pixel"].astype(int)]

        if path_only:
            lidar_camera_df.to_csv(load_path)
            return load_path

        if self.calibration_errors is not None:
            lidar_camera_df = project(lidar_camera_df,
                                      np.asarray(self.config['cameras']['front_center']['CamMatrix']),
                                      self.calibration_errors)
        return lidar_camera_df

    def get_2d_label(self, idx, kind="original", force_obj=False, path_only=False, **kwargs):
        if kind != "original":
            return super().get_2d_label(idx=idx, kind=kind, path_only=False, **kwargs)

        date = self.df.loc[idx, 'date']
        orientation = self.df.loc[idx, 'orientation']
        file_name_bboxes = self.df.loc[idx, 'disc'] + '/' + self.df.loc[
            idx, 'dataset'] + '/camera_lidar_semantic_bboxes/' + date + '/label3D/' + orientation + '/' + date.split("_")[0] + \
                           date.split("_")[1] + '_label3D_' + orientation.split("_")[1] + orientation.split("_")[2] + '_' + self.get_id(
            idx) + 'json'
        if path_only:
            return file_name_bboxes

        boxes = read_bounding_boxes(file_name_bboxes)
        labels = []
        for index, dbox in enumerate(boxes):
            try:
                labels.append(Label2D(cls=dbox["class"].lower(), score=1, x1=int(dbox["top"]), y1=int(dbox["left"]), x2=int(dbox["bottom"]),
                                      y2=int(dbox["right"]), force_obj=self.force_obj))
            except NotValidClass as e:
                continue
        return labels

    def get_3d_label(self, idx):
        date = self.df.loc[idx, 'date']
        orientation = self.df.loc[idx, 'orientation']
        file_name_bboxes = self.df.loc[idx, 'disc'] + '/' + self.df.loc[
            idx, 'dataset'] + '/camera_lidar_semantic_bboxes/' + date + '/label3D/' + orientation + '/' + date.split("_")[0] + \
                           date.split("_")[1] + '_label3D_' + orientation.split("_")[1] + orientation.split("_")[2] + '_' + self.get_id(
            idx) + 'json'

        boxes = read_bounding_boxes(file_name_bboxes)
        labels = []
        for index, dbox in enumerate(boxes):
            x1, y1, x2, y2 = [int(dbox["top"]), int(dbox["left"]), int(dbox["bottom"]), int(dbox["right"])]
            tx, ty, tz = dbox["center"]
            length, width, height = dbox["size"]
            # heading = np.rad2deg(dbox["angle"])
            # box_points = get_points(dbox)

            heading = get_heading(dbox) + np.pi / 2
            inst = Label3D(cls=dbox["class"].lower(), tx=tx, ty=ty, tz=tz, length=length, width=width, height=height,
                           heading=heading, score=1, x1=x1, y1=y1, x2=x2, y2=y2, engine=self)
            inst.meta_data["rotation"] = dbox["rotation"]
            if self.force_obj:
                if inst.obj is None:
                    continue
            labels.append(inst)
        return labels

    def get_3d_instance(self, label, point_cloud):
        mask = point_cloud["orig_z"] >= (label.tz - label.height / 2)
        mask &= point_cloud["orig_z"] <= (label.tz + label.height / 2)
        mask &= point_cloud["orig_x"] >= (label.tx - (label.length + label.width) / 3)
        mask &= point_cloud["orig_x"] <= (label.tx + (label.length + label.width) / 3)
        mask &= point_cloud["orig_y"] >= (label.ty - (label.length + label.width) / 3)
        mask &= point_cloud["orig_y"] <= (label.ty + (label.length + label.width) / 3)

        return point_cloud[mask].reset_index(drop=True)

    def get_box_points(self, tx, ty, tz, length, width, height, heading):
        return

    def process_lidar_on_image(self, lidar):
        """
        imgfov_pc_pixel: (2, N) Points
        imgfov_pc_velo:  (N, 8) Points
            [0, 1, 2] = cx, cy, cz in camera_space
            [3]       = lidar-seg-class
            [4, 5, 6] = cx, cy, cz in lidar-space (original)
            [7]       = 1
        pts_velo:
        inds
        """
        imgfov_pc_pixel = np.vstack((lidar["col"], lidar["row"]))
        imgfov_pc_velo = np.zeros((lidar["points"].shape[0], 8), dtype=np.float32)
        imgfov_pc_velo[:, 4:7] = lidar["points"]
        return imgfov_pc_pixel, imgfov_pc_velo

    def map_lidar_points_onto_image(self, image_orig, lidar, pixel_size=8, pixel_opacity=1):
        return map_lidar_points_onto_image(image_orig, lidar, pixel_size=pixel_size, pixel_opacity=pixel_opacity)

    def get_predictions_dir(self):
        return None

    def get_lidar_camera_fusion_labels_evaluate(self, *args, **kwargs):
        labels = self.get_lidar_camera_fusion_labels(*args, **kwargs)


        pass