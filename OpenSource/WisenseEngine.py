import functools
import pickle
from typing import Dict

from sklearn.cluster import DBSCAN

from Calibrator.calibrator_utils import preprocess_lidar_to_image_simple

from DataPipeline.DNNs.wrappers.exceptions import NotValidClass

from DataPipeline.envelope import pc_envelope_decorator, rgb_envelope_decorator
from DataPipeline.pipeline4_utils import add_sdk_path, apply_uniform
from DataPipeline.settings import Context
from DataPipeline.shared_utils import read_label, toRAzEl, pickle_read
from OpenSource.OpenSource import OpenSourceEngine
import pandas as pd
import cv2
import numpy as np
import os

from Validation.utils import project_lidar_2_radar_params
from wiseTypes.Cluster import Cluster
from wiseTypes.Label2D import Label2D
from wiseTypes.Label3D import Label3D
from wiseTypes.Types import EnvelopeTypes
from wiseTypes.SegLabel import SegLabel
from wiseTypes.classes_dims import Unclassified


class WisenseEngine(OpenSourceEngine):
    def __init__(self, base_path_drive, base_name, df=None, rect_mode=True, envelope_active=False, **kwargs):
        super().__init__(base_path_drive, base_name)
        self.drive_context = Context(base_path=base_path_drive, args=None, index=0, create_dirs=False, **kwargs)
        self.drive_context.set_rect_paths(rect=rect_mode)
        self.rect_mode = rect_mode

        self.set_envelope_active(envelope_active)
        self.df_name = "filtered_data_sync_data_radar_lidar.csv"
        if df is not None and type(df) == str:
            self.df_name = df
        elif df is not None and type(df) == pd.DataFrame:
            self.df = df

        # print("csv_path", self.drive_context.aux_path, self.df_name)
        self.csv_path = self.drive_context.aux_path + "/" + self.df_name
        print("self.csv_path", self.csv_path)
        if os.path.exists(self.csv_path):
            self.df = pd.read_csv(self.csv_path)
            try:
                self.df.timestampCamera = self.df.timestampCamera.astype('str')
                self.df.timestampRadar = self.df.timestampRadar.astype('str')
            except Exception as e:
                pass
        else:
            self.df = pd.DataFrame()
            print("Warning - CSV doesn't exists")

        # Configs
        self.radar_fov_az = 120  # [deg]
        self.radar_fov_el = 50  # [deg]
        self.min_range = 2  # [m]
        self.max_range = 150  # [m]
        self.thin = False

        self.ts_camera = "timestampCamera"
        self.ts_radar = "timestampRadar"
        self.ts_lidar = "timestampLidar"
        self.ts_gps = "timestamp_gps"
        self.power_key = "power_im"
        self.wise4_mode = False
        self.ssd3d_suffix = "uniform_0.15_"
        self.file_name_radar = "filenameRadar"
        self.in_file_idx_radar = "inFileIdxRadar"
        self.default_segmentation_kind = "deeplab"
        if "198" in self.get_drive_name():
            self.set_wise4_keys()
        return

    def set_rect_mode(self, mode):
        self.drive_context.set_rect_paths(mode)

    def set_envelope_active(self, flag=True):
        self.drive_context.envelope[EnvelopeTypes.ENVELOPE_ACTIVE.name] = int(flag)

    def set_camera_obj(self, c_obj):
        self.drive_context.camera_obj = c_obj
        print("camera obj set")

    def set_lidar_obj(self, l_obj):
        self.drive_context.lidar_obj = l_obj
        print("lidar obj set")

    def set_wise4_keys(self):
        add_sdk_path()

        self.wise4_mode = True
        self.ts_camera = "timestamp_camera"
        self.ts_radar = "timestamp_radar"
        self.ts_lidar = "timestamp_lidar"
        self.ts_gps = "timestamp_gps"
        self.file_name_radar = "filename_radar"
        self.in_file_idx_radar = "in_file_idx_radar"
        self.power_key = "Delta_Power"
        self.ssd3d_suffix = ""

        if os.path.exists(self.drive_context.camera_obj_path):
            with open(self.drive_context.camera_obj_path, 'rb') as handle:
                self.set_camera_obj(pickle.load(handle))
                self.drive_context.camera_obj.Radar2Image = self.drive_context.camera_obj.fromRadarRAzEl

        if os.path.exists(self.drive_context.lidar_obj_path):
            with open(self.drive_context.lidar_obj_path, 'rb') as handle:
                self.set_lidar_obj(pickle.load(handle))

        self.get_processed_delta = self.get_processed_delta_wise4
        self.get_lidar_df = self.get_lidar_df_wise4
        self.get_2d_label = functools.partial(self.get_2d_label, kind="yolor")
        # self.get_segmentation_label = functools.partial(self.get_segmentation_label, kind="segformer")
        self.default_segmentation_kind = "segformer"
        if len(self.df) > 0:
            self.df = self.df.sort_values("timestamp_radar").reset_index(drop=True)

    def create_dirs(self):
        self.drive_context.run_dirs_creation()

    def __repr__(self):
        return self.get_drive_name() + " | " + str(len(self))

    def filter_3d_by_2d(self, labels_3d, labels_2d):
        matched_2d = []
        matched_3d = []

        for l3d in labels_3d:
            x_pixel, y_pixel = self.drive_context.camera_obj.fromRadarXYZ(x=l3d.tx, y=l3d.ty, z=l3d.tz)
            for l2d in labels_2d:
                if (l2d.x1 <= x_pixel <= l2d.x2) and (l2d.y1 <= y_pixel <= l2d.y2) and (l2d.cls == l3d.cls):
                    matched_2d.append(l2d)
                    matched_3d.append(l3d)

        return matched_3d, matched_2d
    @staticmethod
    def concat_dfs(engines, suffix):
        dfs = []
        for engine in engines:
            p = engine.drive_context.aux_path + "/" + suffix
            if not os.path.exists(p):
                continue
            dfs.append(pd.read_csv(p))
        return pd.concat(dfs).reset_index(drop=True)

    def get_s_ant(self, idx):
        return np.load(self.drive_context.radar_sant_save_path + "/S_ANT_%s.npy" % self.get_id(idx))

    def get_gps(self, idx, path_only=False):
        gps_path = f"{self.drive_context.gps_data}/gps_data_{self.get_gps_id(idx)}.pkl"
        if path_only:
            return gps_path

        return pickle_read(gps_path)

    @rgb_envelope_decorator
    def get_image(self, idx, path_only=False):
        rgb_path = self.drive_context.camera_rgb + "/camera_rgb_" + str(self.df.iloc[idx][self.ts_camera]) + ".png"
        if path_only:
            return rgb_path
        if not os.path.exists(rgb_path):
            print("warning - image doesnt exists")
        rgb_img = cv2.cvtColor(cv2.imread(rgb_path), cv2.COLOR_BGR2RGB)
        return rgb_img

    def get_id(self, idx):
        return str(self.df.loc[idx, self.ts_radar])

    def get_camera_id(self, idx):
        return str(self.df.loc[idx, self.ts_camera])

    def get_gps_id(self, idx):
        return str(self.df.loc[idx, "timestamp_gps"])

    def get_ids_dict(self, idx):
        row = {self.ts_radar: self.get_id(idx),
               self.ts_camera: self.get_camera_id(idx)}
        if self.ts_lidar in self.df.keys():
            row[self.ts_lidar] = self.get_lidar_id(idx)
        if self.ts_gps in self.df.keys():
            row[self.ts_gps] = self.get_gps_id(idx)
        return row

    def get_dt_title(self, idx):
        camera_radar = round((int(self.df.loc[idx, self.ts_camera]) - int(self.df.loc[idx, self.ts_radar])) / 1e6,
                             2)
        camera_lidar = round((int(self.df.loc[idx, self.ts_camera]) - int(self.df.loc[idx, self.ts_lidar])) / 1e6,
                             2)
        radar_lidar = round((int(self.df.loc[idx, self.ts_radar]) - int(self.df.loc[idx, self.ts_lidar])) / 1e6,
                            2)
        title = r'$\Delta$' + "t : radar-lidar=" + str(radar_lidar) + "ms, radar-camera=" + str(
            camera_radar) + "ms, lidar-camera=" + str(camera_lidar) + "ms\n"
        # title += "CFARS | Coarse=11dB, Fine=6dB, Local=10dB\n" # | FFT - Coarse=10dB\n"
        return title

    def get_ts_title(self, idx):
        return f"radar={self.get_id(idx)} lidar={self.get_lidar_id(idx)} camera={self.get_camera_id(idx)}"

    def get_lidar_id(self, idx, full=False):
        if full:
            return str(self.df.loc[idx, self.ts_lidar])
        return str(self.df.loc[idx, self.ts_lidar])[:-4]

    def get_drive_name(self):
        if self.base_path[-1] == "/": return os.path.split(self.base_path[:-1])[-1]
        return os.path.split(self.base_path)[-1]

    def get_lidar_df_wise4(self, idx, path_only=False, post_process=True, **kwargs):
        lidar_pc_path = self.drive_context.lidar_points + "/lidar_pc_" + str(self.df.loc[idx, 'timestamp_lidar']) + ".csv"
        if path_only:
            return lidar_pc_path
        if not os.path.exists(lidar_pc_path):
            print("get_raw_lidar: path doesn't exists")

        lidar = pd.read_csv(lidar_pc_path)

        if post_process:
            return self.post_process_lidar(lidar, idx, **kwargs)
        else:
            return lidar

    def apply_uniform_on_lidar_df(self, lidar, x_res, y_res, z_res):
        lidar = apply_uniform(lidar, x_res, y_res, z_res)
        x, y, z, dx, dy, dz = self.drive_context.lidar_obj.toRadarXYZ(lidar['orig_x'], lidar['orig_y'], lidar['orig_z'])
        lidar["x"] = x
        lidar["y"] = y
        lidar["z"] = z
        u, v = self.drive_context.camera_obj.fromRadarXYZ(x, y, z)
        lidar["x_pixel"] = u
        lidar["y_pixel"] = v
        return lidar

    def get_lidar(self, idx, path_only=False, concat_original_points=False):
        lidar_pc_path = self.drive_context.lidar_points + "/lidar_pc_" + str(self.df.loc[idx, 'timestampLidar'])[:-4] + ".npz"
        lidar_pc = np.load(lidar_pc_path)['arr_0']
        new_lidar = np.zeros(shape=(lidar_pc.shape[0], lidar_pc.shape[1] + 1))
        new_lidar[:, :3] = lidar_pc
        lidar_pc = new_lidar

        lidar_seg_path = self.drive_context.lidar_seg + "/lidar_seg_" + self.get_lidar_id(idx) + ".npz"
        lidar_seg = None
        if os.path.exists(lidar_seg_path):
            lidar_seg = np.load(lidar_seg_path)["arr_0"]
            lidar_pc[:, 3] = lidar_seg

        projected_lidar = self.project_lidar_to_radar(pts=lidar_pc[:, :3], semantic=lidar_seg)
        if not concat_original_points:
            projected_lidar = projected_lidar[:, :3]

        return lidar_pc, projected_lidar

    def project_lidar_to_radar(self, pts, semantic=None, filter_fov_flag=True):
        if self.wise4_mode:
            lpc = pts.copy()
            X, Y, Z, _, _, _ = self.drive_context.lidar_obj.toRadarXYZ(lpc[:, 0], lpc[:, 1], lpc[:, 2])
            ret_val = np.vstack((X, Y, Z)).T

            if len(ret_val.shape) == 1:
                ret_val = np.expand_dims(ret_val, 0)
            return ret_val

        else:
            return project_lidar_2_radar_params(pts=pts.copy(),
                                                spatial_calibration_params=self.drive_context.spatial_calibration_params,
                                                radar_fov_az=self.radar_fov_az, radar_fov_el=self.radar_fov_el,
                                                min_range=self.min_range, max_range=self.max_range, semantic=semantic,
                                                filter_fov_flag=filter_fov_flag)

    def get_road_surface(self, idx, path_only=False):
        path = self.drive_context.lidar_road_surface + "/lidar_road_surface_" + self.get_lidar_id(idx) + ".csv"
        if path_only: return path
        return pd.read_csv(path)

    @pc_envelope_decorator
    def get_delta(self, idx):
        radar_delta = pd.read_csv(
            self.drive_context.cuda_delta + "/cuda_delta_" + str(self.df.loc[idx, "timestampRadar"]) + ".csv")
        radar_delta["Az"] = np.rad2deg(np.arcsin(radar_delta["u_sin"]))
        radar_delta["El"] = np.rad2deg(np.arcsin(radar_delta["v_sin"]))
        return radar_delta

    def get_platform_velocity(self, idx):
        meta_data_delta = read_label(
            self.drive_context.metadata + "/metadata_cuda_delta_" + str(self.df.loc[idx, "timestampRadar"]) + ".json")
        return np.linalg.norm(meta_data_delta["platform_velocity"])

    def get_4dfft(self, idx):
        return pd.read_csv(
            self.drive_context.cuda_4dfft + "/cuda_4dfft_" + str(self.df.loc[idx, "timestampRadar"]) + ".csv")

    def get_idx_from_ts(self, ts, key="timestampRadar"):
        for idx in range(len(self.df)):
            if str(self.df.loc[idx, key]) == str(ts):
                return idx
        return None

    @pc_envelope_decorator
    def get_processed_delta(self, idx, path_only=False):
        delta_path = self.drive_context.cuda_processed_delta + "/cuda_processed_delta_" + str(self.df.loc[idx, self.ts_radar]) + ".csv"
        if path_only: return delta_path
        if not os.path.exists(delta_path):
            return -2
        radar_p_delta = pd.read_csv(delta_path)
        radar_p_delta["Az"] = np.rad2deg(np.arcsin(radar_p_delta["u_sin"]))
        radar_p_delta["El"] = np.rad2deg(np.arcsin(radar_p_delta["v_sin"]))
        return radar_p_delta

    def get_processed_delta_wise4(self, idx, path_only=False, pickle_only=False):
        delta_path = self.drive_context.cuda_processed_delta + "/cuda_processed_delta_" + str(self.df.loc[idx, self.ts_radar]) + ".pkl"
        if path_only: return delta_path
        if not os.path.exists(delta_path):
            return -2
        radar_p_delta = pickle_read(delta_path)
        if pickle_only:
            return radar_p_delta
        return radar_p_delta["data"]

    def process_first_hit(self, df, az_bin_deg):
        df["Az"] = np.rad2deg(np.arcsin(df["u_sin"]))
        az_index_values = np.digitize(df["Az"], bins=np.linspace(-90, 90, np.int(180 / az_bin_deg)))
        df["Az_index"] = az_index_values
        groups = df.groupby("Az_index")["r"].nsmallest(1)
        if groups.index.nlevels > 1:
            df = df.iloc[groups.index.get_level_values(1)]
        else:
            df = df.iloc[groups.index.get_level_values(0)]
        df = df.set_index("Az_index")
        df = df.reindex(pd.RangeIndex(0, 1800), fill_value=pd.NA)
        df = df.dropna()
        df = df.reset_index(drop=True)
        df = df.astype(np.float32)
        return df

    def get_depth_map(self, idx, path_only=False):
        path = self.drive_context.depth_map + "/depth_map_" + self.get_lidar_id(idx) + ".npz"
        if path_only:
            return path
        return np.load(path)["arr_0"]

    def get_lidar_road_seg(self, idx):
        lidar_pc_path = self.drive_context.lidar_road_seg + "/lidar_road_seg_" + str(
            self.df.loc[idx, 'timestampLidar'])[:-4] + ".npz"
        lidar_pc = np.load(lidar_pc_path)['arr_0']

        projected_lidar = self.project_lidar_to_radar(pts=lidar_pc)
        return lidar_pc, projected_lidar

    def get_lidar_road_seg_df(self, idx):
        return pd.read_csv(
            self.drive_context.lidar_road_seg_df + "/lidar_road_seg_df_" + self.get_lidar_id(idx) + ".csv")

    def get_lidar_road_delimiter(self, idx):
        lidar_pc_path = self.drive_context.lidar_road_delimiter + "/lidar_road_delimiter_" + str(
            self.df.loc[idx, 'timestampLidar'])[:-4] + ".csv"
        return pd.read_csv(lidar_pc_path, index_col=0)

    def process_lidar_on_image(self, lidar, im):
        return preprocess_lidar_to_image_simple(lidar, im, self.drive_context.spatial_calibration_params,
                                                self.drive_context.K, cut_by_elevation=0, camera_fov_az=70)

    def transform_projected_lidar_to_df(self, pts):
        df = pd.DataFrame(columns=["x", "y", "z"])
        df["x"] = pts[:, 0]
        df["y"] = pts[:, 1]
        df["z"] = pts[:, 2]

        df["orig_x"] = pts[:, 3]
        df["orig_y"] = pts[:, 4]
        df["orig_z"] = pts[:, 5]
        df["cls"] = pts[:, 6]

        r, az, el = toRAzEl(x=pts[:, 0], y=pts[:, 1], z=pts[:, 2])
        df["r"] = r
        df["Az"] = az
        df["u_sin"] = np.sin(np.deg2rad(df["Az"]))

        df["El"] = el
        df["v_sin"] = np.sin(np.deg2rad(df["El"]))
        return df

    @pc_envelope_decorator
    def get_lidar_df(self, idx, post_process=True, path_only=False, **kwargs):
        if path_only:
            lidar_pc_path = self.drive_context.lidar_points + "/lidar_pc_" + str(self.df.loc[idx, 'timestampLidar'])[:-4] + ".npz"
            return lidar_pc_path
        lidar, projected_lidar = self.get_lidar(idx, concat_original_points=True)
        im = self.get_image(idx)
        lidar_radar_df = self.transform_projected_lidar_to_df(pts=projected_lidar)

        # Generate_lidar_camera_df
        lidar_camera_df = pd.DataFrame(columns=["x", "y", "z"])
        imgfov_pc_pixel, imgfov_pc_velo = self.process_lidar_on_image(lidar, im)
        lidar_camera_df["x_lidar_camera"] = imgfov_pc_velo[:, 0]
        lidar_camera_df["y_lidar_camera"] = imgfov_pc_velo[:, 1]
        lidar_camera_df["z_lidar_camera"] = imgfov_pc_velo[:, 2]
        lidar_camera_df["r_lidar_camera"] = np.linalg.norm(imgfov_pc_velo[:, :3], axis=-1)

        lidar_camera_df["x_pixel"] = imgfov_pc_pixel[0, :]
        lidar_camera_df["y_pixel"] = imgfov_pc_pixel[1, :]

        lidar_camera_df["cls"] = imgfov_pc_velo[:, 3]
        lidar_camera_df["orig_x"] = imgfov_pc_velo[:, 4]
        lidar_camera_df["orig_y"] = imgfov_pc_velo[:, 5]
        lidar_camera_df["orig_z"] = imgfov_pc_velo[:, 6]

        keys = ["orig_x", "orig_y", "orig_z"]
        lidar_df = lidar_radar_df.merge(lidar_camera_df, how="outer", left_on=keys, right_on=keys)

        lidar_df = lidar_df.rename(columns={"x_x": "x", "y_x": "y", "z_x": "z", "cls_y": "cls"})
        lidar_df = lidar_df.drop(columns=["x_y", "y_y", "z_y", "cls_x"])

        if post_process:
            return self.post_process_lidar(lidar_df, idx, **kwargs)
        else:
            return lidar_df

        # if not self.thin:
        #     seg = self.get_segmentation_label(idx=idx)
        #     confidence_indices = lidar_df[["x_pixel", "y_pixel"]].dropna().T.to_numpy(dtype=np.int)
        #     confidence = seg.soft_label[confidence_indices[1, :], confidence_indices[0, :]]
        #     img_cls = seg.label[confidence_indices[1, :], confidence_indices[0, :]]
        #     lidar_df.loc[lidar_df["x_pixel"].notna(), "confidence"] = confidence
        #     lidar_df.loc[lidar_df["x_pixel"].notna(), "img_class"] = img_cls
        #
        # if filter_by_seg_class is not None:
        #     lidar_df = lidar_df[lidar_df["x_pixel"].notna()]
        #     boolean_mask = seg.get_mask_of_points(lidar_df[["x_pixel", "y_pixel"]].T.to_numpy(),
        #                                           cls=filter_by_seg_class)
        #
        #     if keep_semantic:
        #         return lidar_df[boolean_mask].reset_index(drop=True)
        #     return lidar_df[~boolean_mask].reset_index(drop=True)
        #
        # return lidar_df.reset_index(drop=True)

    def post_process_lidar(self, lidar_df, engine_idx, filter_by_seg_class=None, keep_semantic=True):
        if not self.thin:
            try:
                seg = self.get_segmentation_label(idx=engine_idx)

                lidar_df.x_pixel = np.clip(np.round(lidar_df.x_pixel), 0, 1824 - 1).astype(np.int)
                lidar_df.y_pixel = np.clip(np.round(lidar_df.y_pixel), 0, 940 - 1).astype(np.int)

                confidence_indices = lidar_df[["x_pixel", "y_pixel"]].dropna().T.to_numpy(dtype=np.int)
                confidence = seg.soft_label[confidence_indices[1, :], confidence_indices[0, :]]
                img_cls = seg.label[confidence_indices[1, :], confidence_indices[0, :]]
                lidar_df.loc[lidar_df["x_pixel"].notna(), "confidence"] = confidence
                lidar_df.loc[lidar_df["x_pixel"].notna(), "img_class"] = img_cls
            except Exception as e:
                pass
        if filter_by_seg_class is not None:
            lidar_df = lidar_df[lidar_df["x_pixel"].notna()]
            boolean_mask = seg.get_mask_of_points(lidar_df[["x_pixel", "y_pixel"]].T.to_numpy(), cls=filter_by_seg_class)

            if keep_semantic:
                return lidar_df[boolean_mask].reset_index(drop=True)
            return lidar_df[~boolean_mask].reset_index(drop=True)
        else:
            return lidar_df.reset_index(drop=True)

    def get_clusters(self, p_delta) -> Dict[int, Cluster]:
        if type(p_delta) == int:
            return {}
        clusters_centers = {}
        for cls_id in np.unique(p_delta["cluster_id"].values.astype(np.int)):
            cluster_df = p_delta[p_delta["cluster_id"] == cls_id].copy()
            # if cluster_df["power_im"].sum() == 0:
            #     continue
            proj = self.drive_context.camera_obj.Radar2Image(r=cluster_df["r"],
                                                             az=np.rad2deg(np.arcsin(cluster_df["u_sin"])),
                                                             el=np.rad2deg(np.arcsin(cluster_df["v_sin"])),
                                                             D=self.drive_context.D)
            if not self.wise4_mode:
                cluster_df.loc[:, "x_pixel"] = proj[0, :]
                cluster_df.loc[:, "y_pixel"] = proj[1, :]
            else:
                cluster_df.loc[:, "x_pixel"] = proj[0]
                cluster_df.loc[:, "y_pixel"] = proj[1]

            cur_cluster = Cluster(cluster_df=cluster_df, cluster_id=cls_id, power_key=self.power_key)
            if cur_cluster.is_valid():
                clusters_centers[cls_id] = cur_cluster
        return clusters_centers

    def get_lidar_clusters(self, lidar_df):
        lidar_split = [0, 20, 40, 60, 80]
        eps = [0.3, 0.6, 0.8, 1.2]
        min_samples = [15, 10, 8, 4]

        clusters_centers = {}
        cluster_idx = 0
        for split_idx, split_m in enumerate(lidar_split[1:]):
            cur_lidar_df = lidar_df[
                (lidar_df["y"] >= lidar_split[split_idx]) & (lidar_df["y"] < lidar_split[split_idx + 1])]
            coords_keys = ["x", "y", "z"]
            db = DBSCAN(eps=eps[split_idx], min_samples=min_samples[split_idx])
            X = cur_lidar_df[coords_keys]
            db_res = db.fit(X)
            labels = db_res.labels_
            core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
            core_samples_mask[db.core_sample_indices_] = True
            unique_labels = set(labels)

            for _ in unique_labels:
                class_member_mask = (labels == _)
                cluster_df = cur_lidar_df[class_member_mask & core_samples_mask]
                if len(cluster_df) == 0:
                    continue
                clusters_centers[cluster_idx] = Cluster(cluster_df=cluster_df, cluster_id=cluster_idx, power_key=None)
                cluster_idx += 1
        return clusters_centers

    def plot_centers(self, ax, p_delta, center_size=100, color="red"):
        for center in self.get_clusters(p_delta):
            ax.scatter([center[0]], [center[1]], s=center_size, c=color, marker="x", linewidths=1.2, facecolors='none',
                       alpha=1)

    def get_segmentation_label(self, idx, kind=None, path_only=False, hard_path=None, soft_path=None):
        from DataPipeline.DNNs.wrappers.deepLabWrapper import DeepLabWrapper
        default_wrapper = DeepLabWrapper()
        kind = kind if kind is not None else self.default_segmentation_kind

        if kind == "deeplab":
            hard_label = self.drive_context.camera_seg_class + "/camera_seg_class_" + str(self.df.loc[idx, self.ts_camera]) + ".npz"
            soft_label = self.drive_context.camera_seg_soft + "/camera_seg_soft_" + str(self.df.loc[idx, self.ts_camera]) + ".npz"
            if path_only:
                return hard_label, soft_label
            
            hard_label = np.load(hard_label)["arr_0"]
            soft_label = np.load(soft_label)["arr_0"]
        elif kind == "segformer":
            hard_label = self.drive_context.camera_segformer_class + "/camera_segformer_hard_" + str(self.df.loc[idx, self.ts_camera]) + ".npy" if hard_path is None else hard_path
            soft_label = self.drive_context.camera_segformer_soft + "/camera_segformer_soft_" + str( self.df.loc[idx, self.ts_camera]) + ".npy" if soft_path is None else soft_path
            if path_only:
                return hard_label, soft_label
            hard_label = np.load(hard_label).squeeze()
            soft_label = np.load(soft_label).squeeze()
        return SegLabel(img=hard_label, engine=default_wrapper, soft=soft_label)