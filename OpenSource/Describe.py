import pathlib
import sys
from collections import defaultdict
import os

from matplotlib import gridspec
from tqdm import tqdm
import matplotlib.pyplot as plt

module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, module_path)
sys.path.insert(0, module_path + "/../")

from Evaluation.evaluation_utils import draw
from OpenSource.audi_utils import cart2spher
from wiseTypes.classes_dims import WiseClassesSegmentation
import cv2
import pandas as pd
from OpenSource.AudiEngine import AudiEngine
from OpenSource.CombinedDataset import CombinedDataset
from OpenSource.WaymoEngine import WaymoEngine
import numpy as np
import matplotlib

plt.style.use('dark_background')
import matplotlib

font = {'size': 24}

matplotlib.rc('font', **font)

MEAN_INSTANCES_PER_FRAME = "Average instances per frame"
INSTANCES_COUNT = "Instances count"
SIZE = "Dimensions"
SCORES = "Scores"


class DescribeDataset:
    def __init__(self, out_path):
        self.out_path = out_path
        self.plot_every = 5
        self.df = None
        self.seg_classes = WiseClassesSegmentation()

        self.instances_dict = defaultdict(dict)
        self.metric_labels = {SIZE: ("Dimensions | $\sqrt{w*h}$", "Instances count"),
                              SCORES: ("Scores", "Count")}
        return

    def analyze_labels(self, labels):
        instance_count = defaultdict(int)
        for label in labels:
            label.cls = label.cls.capitalize()
            x1, y1, x2, y2 = label.get_coords()
            cx, cy, w, h = label.get_center()

            if SIZE not in self.instances_dict[label.cls]:
                self.instances_dict[label.cls][SIZE] = []
            if INSTANCES_COUNT not in self.instances_dict[label.cls]:
                self.instances_dict[label.cls][INSTANCES_COUNT] = 0
            if MEAN_INSTANCES_PER_FRAME not in self.instances_dict[label.cls]:
                self.instances_dict[label.cls][MEAN_INSTANCES_PER_FRAME] = []
            if SCORES not in self.instances_dict[label.cls]:
                self.instances_dict[label.cls][SCORES] = []

            self.instances_dict[label.cls][SIZE].append(np.sqrt(np.abs(x2 - x1) * np.abs(y2 - y1)))
            self.instances_dict[label.cls][INSTANCES_COUNT] += 1

            instance_count[label.cls] += 1
            self.instances_dict[label.cls][SCORES].append(label.score)

        for cls, ins_per_frame in instance_count.items():
            self.instances_dict[cls][MEAN_INSTANCES_PER_FRAME].append(ins_per_frame)
        return

    def describe_engine_2d(self, data_engine):
        self.out_path = self.out_path + "/describe_" + str(data_engine.name) + "/"
        os.makedirs(self.out_path, exist_ok=True)

        for data_idx in tqdm(range(len(data_engine))):
            im, labels = data_engine.get_image(data_idx), data_engine.get_2d_label(data_idx)

            if len(labels) == 0:
                continue

            self.analyze_labels(labels)

        self.plot_description(data_engine)
        return

    def describe_engine_3d(self, data_engine):
        ranges = defaultdict(list)
        azimuths = defaultdict(list)
        headings = defaultdict(list)
        rotations = defaultdict(list)
        for data_idx in tqdm(range(len(data_engine))):
            im, labels = data_engine.get_image(data_idx), data_engine.get_3d_label(data_idx)

            for l in labels:
                ranges[l["class"]].append(np.linalg.norm(np.array([l["tx"], l["ty"], l["tz"]])))
                cur_heads = l["heading"]
                if np.random.random() < 0.5:
                    cur_heads *= -1
                headings[l["class"]].append(cur_heads)
                rotations[l["class"]].append(np.rad2deg(np.arctan2(l["rotation"][2, 0], l["rotation"][2, 1])))
                azimuths[l["class"]].append(cart2spher(x=l["ty"], y=l["tx"], z=l["tz"])[1])

        for cls in ranges.keys():
            plt.figure(figsize=(12, 12))
            ax = plt.gca()
            values = ranges[cls]
            N, bins, patches = ax.hist(values)  # arguments are passed to np.histogram
            ax.set_title("Range distribution | " + cls.capitalize() + " | Audi")
            ax.set_xlabel("Range (m)")
            ax.set_ylabel("Instances count")
            plt.savefig(self.out_path + "/ranges_" + cls + ".png")
            plt.close()

            plt.figure(figsize=(12, 12))
            ax = plt.gca()
            values = headings[cls]
            N, bins, patches = ax.hist(values)  # arguments are passed to np.histogram
            ax.set_title("Heading distribution | " + cls.capitalize() + " | Audi")
            ax.set_xlabel("Heading (Deg)")
            ax.set_ylabel("Instances count")
            plt.savefig(self.out_path + "/headings_" + cls + ".png")
            plt.close()

            plt.figure(figsize=(12, 12))
            ax = plt.gca()
            values = azimuths[cls]
            N, bins, patches = ax.hist(values)  # arguments are passed to np.histogram
            ax.set_title("Azimuth distribution | " + cls.capitalize() + " | Audi")
            ax.set_xlabel("Azimuth")
            ax.set_ylabel("Instances count")
            plt.savefig(self.out_path + "/azimuths_" + cls + ".png")
            plt.close()

            plt.figure(figsize=(12, 12))
            ax = plt.gca()
            values = rotations[cls]
            N, bins, patches = ax.hist(values)  # arguments are passed to np.histogram
            ax.set_title("Rotations distribution | " + cls.capitalize() + " | Audi")
            ax.set_xlabel("Rotation")
            ax.set_ylabel("Instances count")
            plt.savefig(self.out_path + "/rotations_" + cls + ".png")
            plt.close()

        # seg_results = {k.capitalize(): v for k, v in sorted(ranges.items(), key=lambda item: item[1], reverse=True)}
        # plt.bar(range(len(seg_results)), list(seg_results.values()), align='center')
        # plt.xticks(range(len(seg_results)), list(seg_results.keys()), rotation='vertical')
        # plt.margins(0.2)
        # plt.tight_layout(pad=3)
        # plt.title("Pixels Per Class | " + str(data_engine.base_name).capitalize())
        # plt.savefig(self.out_path + "/seg_results.png")

        return

    def describe_segmentation(self, data_engine):
        plt.figure(figsize=(12, 12))
        seg_results = defaultdict(list)
        for data_idx in tqdm(range(len(data_engine))):
            # im, labels = data_engine.get_image(data_idx), data_engine.get_2d_label(data_idx)
            seg = data_engine.get_segmentation_label(data_idx)
            seg_img = seg.get_img(colors=False)
            for uni_idx in np.unique(seg_img):
                if uni_idx == 0:
                    continue
                top = np.count_nonzero(seg_img == uni_idx)
                bottom = seg_img.shape[0] * seg_img.shape[1]
                seg_results[self.seg_classes.get_obj_by_idx(uni_idx).name].append(top / bottom)

        # Plot
        ax = plt.gca()
        # ax.set_yscale('log')

        # seg_results = {k.capitalize(): np.mean(v) for k, v in sorted(seg_results.items(), key=lambda item: item[1], reverse=True)}

        for k in seg_results:
            seg_results[k] = np.mean(seg_results[k])

        plt.bar(range(len(seg_results)), list(seg_results.values()), align='center')
        plt.xticks(range(len(seg_results)), [val.capitalize() for val in list(seg_results.keys())], rotation='vertical')
        # plt.margins(0.2)
        plt.tight_layout(pad=3)
        plt.title("Average occupancy per frame | " + str(data_engine.base_name).capitalize())
        plt.savefig(self.out_path + "/seg_results_avg.png")
        return

    def plot_description(self, data_engine):
        classes = list(self.instances_dict.keys())
        self.df = pd.DataFrame()

        def plot_metric(metric):
            gs = gridspec.GridSpec(ncols=len(classes), nrows=1)
            fig = plt.figure(figsize=(40, 10))
            for col_idx, (cls, cls_dict) in enumerate(self.instances_dict.items()):
                row_idx = 0
                values = cls_dict[metric]
                ax = fig.add_subplot(gs[row_idx, col_idx])
                fig_name = str(cls).capitalize() + " | " + str(metric) + " histogram"
                N, bins, patches = ax.hist(values)  # arguments are passed to np.histogram
                ax.set_title(fig_name)
                ax.set_xlabel(self.metric_labels[metric][0])
                ax.set_ylabel(self.metric_labels[metric][1])
                row_idx += 1
            plt.tight_layout(pad=4)
            plt.suptitle("Dataset | " + str(data_engine.base_name))
            plt.savefig(self.out_path + "/" + metric + ".png")
            plt.close()

        plot_metric(metric=SIZE)
        plot_metric(metric=SCORES)
        for cls, cls_dict in self.instances_dict.items():
            self.df.loc[cls, INSTANCES_COUNT] = cls_dict[INSTANCES_COUNT]
            self.df.loc[cls, MEAN_INSTANCES_PER_FRAME] = np.mean(cls_dict[MEAN_INSTANCES_PER_FRAME])
            self.df.loc[cls, "max_score"] = np.max(cls_dict[SCORES])
            self.df.loc[cls, "min_score"] = np.min(cls_dict[SCORES])
            self.df.loc[cls, "mean_score"] = np.mean(cls_dict[SCORES])

        self.df.to_csv(self.out_path + "/instances_count.csv")

        for key in [INSTANCES_COUNT, MEAN_INSTANCES_PER_FRAME]:
            fig = plt.figure(figsize=(12, 12))
            ax = fig.gca()
            self.df.loc[:, key].plot(kind='bar', ax=ax)
            plt.tight_layout(pad=4)
            plt.title(key + " | " + data_engine.base_name)
            plt.savefig(self.out_path + "/df_" + key + ".png")

            plt.close()

    def plot_datas(self, datas):
        plt.figure(figsize=(20, 10))
        ax = plt.gca()

        margins = {0: -1, 1: 1}
        for model_idx, (model, model_df) in enumerate(datas.items()):
            labels = model_df.iloc[:, 0].values

            men_means = model_df.loc[:, INSTANCES_COUNT]

            x = np.arange(len(labels))  # the label locations
            width = 0.35  # the width of the bars

            rects1 = ax.bar(x + margins[model_idx] * (width / 2), men_means, width, label=model)

        # Add some text for labels, title and custom x-axis tick labels, etc.
        ax.set_ylabel('count')
        ax.set_title('Compare')
        ax.set_xticks(x)
        ax.set_xticklabels(labels)
        ax.legend()

        plt.savefig(self.out_path + "/compare.png")
        plt.close()
        return


if __name__ == "__main__":
    audi_engine = AudiEngine(base_path="/media/amper/open_source5/audi/", val=True, force_obj=False)
    audi_engine.df = audi_engine.df.sample(600).reset_index(drop=True)

    waymo_engine = WaymoEngine(base_path="/media/amper/wiseData11/waymo/", val=True, force_obj=False)
    # waymo_engine.df = waymo_engine.df.sample(15).reset_index(drop=True)

    # combined_dataset = CombinedDataset(data_engines=[audi_engine, waymo_engine])

    cur_working_dir = module_path + "/Describe_Results//"

    # os.makedirs(cur_working_dir, exist_ok=True)
    # for cur_engine in [waymo_engine, audi_engine]:
    #     describator = DescribeDataset(out_path=cur_working_dir)
    #     describator.describe_engine_2d(cur_engine)

    # Segmentation
    describator = DescribeDataset(out_path=cur_working_dir)
    # describator.describe_engine_3d(data_engine=audi_engine)
    describator.describe_segmentation(audi_engine)

    # describator = DescribeDataset(out_path=cur_working_dir)
    # datas_ = {}
    # for d in os.listdir(cur_working_dir):
    #     p = cur_working_dir + d + "/instances_count.csv"
    #     if os.path.exists(p):
    #         datas_[d] = pd.read_csv(p)
    # print(datas_)
    # describator.plot_datas(datas=datas_)
