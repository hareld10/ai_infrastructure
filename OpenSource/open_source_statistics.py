import pathlib
import sys
from glob import glob
import pandas as pd
from tqdm import tqdm
import os
module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, module_path)
sys.path.insert(0, module_path + "/../")
from OpenSource.AudiEngine import AudiEngine
from OpenSource.WaymoEngine import WaymoEngine



class Multidf:
    def __init__(self, df_name, output_dir, split_every=3000):
        self.df = pd.DataFrame()
        self.df_name = df_name
        self.output_dir = output_dir
        self.split_every = split_every
        self.cur_part = 0
        self.remove()
        pass

    def remove(self):
        [os.remove(x) for x in glob(self.output_dir + "/" + self.df_name + "_part*")]

    def add_row(self, row):
        self.df = self.df.append(row, ignore_index=True)
        if len(self.df) > self.split_every:
            print("writing part", self.cur_part)
            self.df.to_csv(self.output_dir + "/" + self.df_name + "_part_" + str(self.cur_part) + ".csv")

            self.df = pd.DataFrame()
            self.cur_part += 1
        return

    def finalize(self):
        self.df.to_csv(self.output_dir + "/" + self.df_name + "_part_" + str(self.cur_part) + ".csv")
        dfs = [x for x in glob(self.output_dir + "/" + self.df_name + "_part*")]
        instances_df = pd.concat([pd.read_csv(x) for x in dfs])
        instances_df = instances_df.reset_index(drop=True)
        instances_df.drop(instances_df.filter(regex="Unname"), axis=1, inplace=True)
        instances_df.reset_index(drop=True).to_csv("%s/%s.csv" % (self.output_dir, self.df_name))
        self.remove()
        return


def main():
    # engine = WaymoEngine(base_path="/media/amper/wiseData11/waymo/", val=True)
    engine = AudiEngine(val=False)
    # engine.sample_df(20)
    df = pd.DataFrame()
    multi_df = Multidf(df_name="instances_3d_df", output_dir=engine.base_path, split_every=15000)
    for idx in tqdm(range(len(engine))):
        try:
            label = engine.get_3d_label(idx=idx)
            for inst in label:
                if inst.ty < 0:
                    continue
                meta = {"frameId": engine.get_id(idx)}
                row = {**inst.get_dict(add_meta=False), **meta}
                multi_df.add_row(row)
        except Exception as e:
            print(e)
            df.to_csv(engine.base_path + "/instances_3d_df_bu.csv")
            continue
    multi_df.finalize()


if __name__ == '__main__':
    main()
