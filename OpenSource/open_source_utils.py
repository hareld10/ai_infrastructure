import numpy as np


def lidar_to_radar_coordinates(points):
    theta = np.deg2rad(90)
    rotation_matrix = np.array([[np.cos(theta), -np.sin(theta), 0],
                                [np.sin(theta), np.cos(theta), 0],
                                [0, 0, 1]])

    return np.matmul(rotation_matrix, points.T).T


def radar_to_lidar_coordinates(points):
    theta = np.deg2rad(-90)
    rotation_matrix = np.array([[np.cos(theta), -np.sin(theta), 0],
                                [np.sin(theta), np.cos(theta), 0],
                                [0, 0, 1]])

    return np.matmul(rotation_matrix, points.T).T