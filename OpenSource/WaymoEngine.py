import json
import os

import cv2
import numpy as np

from DataPipeline.settings import Context
from OpenSource.OpenSource import OpenSourceEngine
import pandas as pd
from wiseTypes.Label2D import Label2D
from wiseTypes.Label3D import Label3D


class WaymoEngine(OpenSourceEngine):
    def __init__(self, base_path, val=False, force_obj=False):
        super().__init__(base_path=base_path, val=val, force_obj=force_obj, base_name="Waymo")
        self.df_train = pd.read_csv(self.base_path + "/train.csv")
        self.df_train['disc'] = self.base_path
        self.df_train['dataset'] = ''

        self.df_val = pd.read_csv(self.base_path + "/val.csv")
        self.df_val['disc'] = self.base_path
        self.df_val['dataset'] = ''

        self.df = self.df_train if not val else self.df_val
        self.train_val_flag = "val" if val else "train"
        self.drive_context = Context(base_path=base_path, args=None, index=0, create_dirs=True)

    def get_image(self, idx, path_only=False):
        img_path = self.base_path + '/' + self.train_val_flag + '/rgb_img/' + str(self.df.loc[idx, 'frameId']) + '_rgbIm.png'
        if path_only:
            return img_path
        img = cv2.cvtColor(cv2.imread(img_path), cv2.COLOR_BGR2RGB)
        return img

    def get_id(self, idx):
        return str(self.df.loc[idx, 'frameId'])

    def get_lidar_df(self, idx):
        label_path = self.base_path + '/' + self.train_val_flag + '/point_cloud/' + str(self.df.loc[idx, 'frameId']) + '_cp_points_all.npz'

        pc = pd.DataFrame()

        return np.load(label_path)

    def get_2d_label(self, idx, kind="original", **kwargs):
        if kind != "original":
            return super().get_2d_label(idx=idx, kind=kind, **kwargs)

        label_path = self.base_path + '/' + self.train_val_flag + '/labels/' + str(self.df.loc[idx, 'frameId']) + '_labels.json'
        if not os.path.exists(label_path):
            return []

        with open(label_path) as label_json:
            all_labels = json.load(label_json)
            camera_labels = all_labels['camera_labels']

        labels = []
        for k, inst in camera_labels.items():
            cur_label = Label2D(cls=inst["type"], score=1, force_obj=self.force_obj)
            cur_label.set_center(cx=inst["tx"], cy=inst["ty"], w=inst["sl_x"] / 2, h=inst["sw_y"] / 2)
            cur_label.meta_data = inst
            labels.append(cur_label)

        return labels

    def get_3d_label(self, idx):
        label_path = self.base_path + '/' + self.train_val_flag + '/labels/' + str(self.df.loc[idx, 'frameId']) + '_labels.json'

        with open(label_path) as label_json:
            all_labels = json.load(label_json)
            camera_labels = all_labels['lidar_labels']

        labels = []
        for k, inst in camera_labels.items():
            cur_label = Label3D(cls=inst["type"], score=1,
                                tx=inst["tx"], ty=inst["ty"], tz=inst["tz"],
                                width=inst["sw_y"], height=inst["sh"], length=inst["sl_x"],
                                heading=inst["heading"], x1=0, x2=0, y1=0, y2=0,
                                engine=self)
            cur_label.meta_data = inst
            labels.append(cur_label)

        return labels


if __name__ == "__main__":
    w_engine = WaymoEngine(base_path="/media/amper/wiseData11/waymo/")
