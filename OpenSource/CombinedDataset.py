from OpenSource.OpenSource import OpenSourceEngine
import numpy as np


class CombinedDataset(OpenSourceEngine):
    def __init__(self, data_engines):
        super().__init__(base_path="", base_name="Combined_" + "_".join([data.get_name() for data in data_engines]))
        self.data_engines = data_engines
        self.lens = np.cumsum([0] + [len(data) for data in self.data_engines])
        return

    def __len__(self):
        return sum(len(data) for data in self.data_engines)

    def resolve_data_engine_index(self, desired_idx):
        for d_idx, cumsum in enumerate(self.lens[1:]):
            if desired_idx < cumsum:
                actual_idx = desired_idx - self.lens[d_idx]
                return d_idx, actual_idx

    def get_image(self, idx):
        d_engine_idx, actual_idx = self.resolve_data_engine_index(idx)
        return self.data_engines[d_engine_idx].get_image(actual_idx)

    def get_2d_label(self, idx):
        d_engine_idx, actual_idx = self.resolve_data_engine_index(idx)
        return self.data_engines[d_engine_idx].get_2d_label(actual_idx)


