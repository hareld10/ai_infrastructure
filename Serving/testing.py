import docker
# same result with below line
#cli = docker.client.DockerClient(base_url='tcp://127.0.0.1:2375')
cli = docker.DockerClient(base_url='tcp://127.0.0.1:2375')

containers = cli.containers.list(all=True)
names = [container.name for container in containers]
print(names)