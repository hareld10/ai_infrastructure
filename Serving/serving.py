from multiprocessing import Process
from pprint import pprint

import numpy as np
import pandas as pd
from tqdm import tqdm
import os
import sys, pathlib
module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, module_path)
sys.path.insert(0, "/workspace/AI/Harel/AI_Infrastructure/")
from DataPipeline.arg_parser import parser
from DataPipeline.ensamble import Ensemble
from DataPipeline.src.GetEngines import get_engines
from DataPipeline.shared_utils import read_label, is_docker
if is_docker():
    from DataPipeline.dataProcessing_args import Pipeline
import json
import requests
import docker

class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


class Wrapper:
    def __init__(self, port):
        self.port = port

    def send(self, packet, service="predict", check_status=False, dump=True):
        # print("service", service, packet)
        if dump:
            to_send = json.dumps(packet, cls=NumpyEncoder)
        else:
            to_send = packet

        ret = requests.post(f'http://0.0.0.0:{self.port}/{service}', json=to_send)
        try:
            ans = ret.json()
            if check_status:
                return Wrapper.check_status(ans, service)
            return ans
        except Exception as e:
            print("ret", ret, type(ret))
            # print("ret.json()", ret.json())
            print("Wrapper: send, Failed to read json", e)
        return ret

    @staticmethod
    def check_status(ret, service):
        if ret["status"] != 0:
            print("service", service, "Fail!", ret)
            return False
        return True


def check_running_docker_name(docker_name):
    cli = docker.DockerClient(base_url='tcp://127.0.0.1:2375')

    containers = cli.containers.list(all=True)
    names = [container.name for container in containers]

    return docker_name in names