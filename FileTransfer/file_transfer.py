import argparse
import pathlib
import sys

from paramiko import SSHClient, AutoAddPolicy
import os
import logging
import subprocess
from subprocess import call, Popen, PIPE
import pandas as pd

module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, module_path)
sys.path.insert(0, module_path + "/../")

logging.basicConfig()
logger = logging.getLogger('file_transfer')
logger.setLevel(logging.DEBUG)

from DataPipeline.src.GetEngines import get_engines

username_map = {"faraday":
    {
        "ip": "192.168.1.189",
        "secret": "wiseai2018",
        "mounts": ["nvme1", "nvme2", "extSSD1", "extSSD2", "HDD", "extSSD4", "extSSD5", "extSSD6"]  #"extSSD3", "extSSD4", "nvme1", "nvme2", "HDD"]
    },
    "amper_disc":
        {
            "ip": "192.168.1.130",
            "secret": "wiseai2018"
        }
}


class FileTransfer:
    def __init__(self, username="faraday"):
        self.client = SSHClient()
        self.client.set_missing_host_key_policy(AutoAddPolicy())
        self.connect(username)
        pass

    def connect(self, username):
        host, secret = username_map[username]["ip"], username_map[username]["secret"]
        self.client.connect(host, port=22, username=username, password=secret, allow_agent=False, look_for_keys=False)

    def run_cmd(self, cmd):
        stdin, stdout, stderr = self.client.exec_command(command=cmd)
        outlines = stdout.readlines()
        resp = ''.join(outlines)
        stdin.close()
        stdout.close()
        stderr.close()
        return resp

    def __del__(self):
        self.client.close()


class DatasetMover:
    def __init__(self, raw=False):
        self.logger = logging.getLogger("file_transfer")
        self.logger.setLevel(logging.DEBUG)
        self.fs = FileTransfer()
        self.raw = raw
        self.fs.run_cmd(cmd="bash /workspace/mount_faraday.sh")
        self.specific_type = ""

    def set_specific_type_include(self, fstring):
        self.specific_type = f" --include \"*/\" --include \"{fstring}\" --exclude \"*\"  "

    def set_specific_type_exclude(self, fstring):
        self.specific_type = f" --include \"*/\" --exclude \"{fstring}\" "

    def get_free_space(self, mount):
        df_cmd = "df %s" % mount
        self.logger.debug("get_free_space: %s", df_cmd)
        df_res = self.fs.run_cmd(df_cmd)
        self.logger.debug("get_free_space: result %s", df_res)
        free_space = int(df_res.split("\n")[1].split()[3])
        self.logger.debug("mount %s free-space %dGB", mount, free_space/(10**6))
        return free_space

    def get_dir_size(self, dir_path):
        cmd = "du -sL %s" % dir_path
        self.logger.debug("get_dir_size: %s", cmd)
        res = self.fs.run_cmd(cmd)
        self.logger.debug("get_free_space: result %s", res)
        if len(res) == 0:
            self.logger.debug("get_free_space: probably dataset doesnt exists in dest")
            return 0
        size = int(res.split()[0])
        self.logger.debug("get_dir_size: dir=%s size=%d", dir_path, size)
        return size

    def sync_dir(self, src, dest):
        dest_cmd = " faraday@192.168.1.189:%s" % dest
        print("src=", src)
        print("dest=", dest_cmd)

        cmd_base = "//usr/bin/rsync -rahvtlzL --rsh=\"/usr/bin/sshpass -p wiseai2018 ssh -o StrictHostKeyChecking=no -l faraday\" "
        final_cmd = cmd_base + self.specific_type + src + dest_cmd

        print(final_cmd)
        os.system(final_cmd)
        return final_cmd

    def move_engine(self, engine_to_move, src="amper", dest="faraday"):
        srcs = pd.read_csv(engine_to_move.srcs_path, index_col=0)

        self.logger.info("\n\n\nDatasetMover: %s from %s to %s\n\n\n", engine_to_move.get_drive_name(), src, dest)

        src_loc = str(engine_to_move.src_df.loc["suffix"]) + "//" + str(engine_to_move.src_df.loc[src + "_disc"]) + "/" + engine_to_move.get_drive_name() + "/"
        dest_mount = str(engine_to_move.src_df.loc["suffix"]) + "//" + str(engine_to_move.src_df.loc[dest + "_disc"]) + "//" + str(int(float(engine_to_move.src_df.loc["wf"]))) + "mWF//"
        dest_loc = dest_mount + "/" + engine_to_move.get_drive_name() + "/"

        drive = engine_to_move.get_drive_name()

        # Create symbolic link to RawData
        if self.raw:
            link_src = str(engine_to_move.src_df.loc["suffix"]) + "//" + str(engine_to_move.src_df.loc["radarRawData_" + src]) + "/" + engine_to_move.get_drive_name() + "/radarRawData/"
            link_dest = src_loc + "/radar_data/"
            cmd = "ln -s %s %s" % (link_src, link_dest)
            os.system(cmd)
            print("created soft link", cmd)

        size_cmd_results = subprocess.run(["du", "-sL", src_loc], stdout=subprocess.PIPE)
        drive_size = int(size_cmd_results.stdout.decode('utf-8').split()[0])
        print("cmd = du -sL ", src_loc, drive_size)
        self.logger.info("current drive size is %fGB (original=%d)", drive_size/(10**6), drive_size)

        # Case exists in csv
        if pd.isna(engine_to_move.src_df.loc[dest + "_disc"]):
            self.logger.info("drive %s destination is empty in srcs, we'll find location", drive)

            # Check if drive exists in dest -> not implmented
            cmd = "find /workspace/ -name " + drive
            find_res = self.fs.run_cmd(cmd)
            #self.logger.info("cmd=%s, result=%s", cmd, find_res)

            if True:
                # self.logger.info("drive %s doesn't exists in destination", drive)
                found = False
                for mount in username_map[dest]["mounts"]:
                    free_space = self.get_free_space(mount="/workspace/" + mount)
                    delta = (free_space - drive_size)
                    # self.logger.info("mount %s free-space %d", mount, free_space/(10**6))
                    if delta > 500000:
                        found = True
                        self.logger.info("found enough space in mount=%s, delta=%d", mount, delta)

                        dest_col = mount + "/" + str(int(float(srcs.loc[drive]["wf"]))) + "mWF/"
                        dest_base = "/workspace/" + dest_col + "/"
                        dest_final = dest_base + drive + "/"

                        mkdir_cmd = "mkdir -p " + dest_base
                        self.logger.info("mkdir cmd %s", mkdir_cmd)
                        self.fs.run_cmd(mkdir_cmd)

                        srcs.loc[drive, dest + "_disc"] = mount
                        srcs.to_csv(engine_to_move.srcs_path)
                        print("We'll copy from", src_loc, "to", dest_final)
                        final_cmd = self.sync_dir(src=src_loc, dest=dest_final)
                        print(f"final_cmd={final_cmd}")
                        break

                if not found:
                    self.logger.info("Couldn't find empty space")

            else:
                self.logger.info("drive %s does exists in destination in %s", drive, find_res)
                size = self.get_dir_size(dir_path=find_res)
                self.logger.debug("current dir size in location = %d", size)

                free_space = self.get_free_space(find_res)
                self.logger.debug("free space in location = %d", free_space)

                if (free_space - (drive_size - size)) > 200000:
                    self.logger.debug("We will update current dataset in %s", find_res)

        else:
            self.logger.info("drive %s destination is in srcs, we'll copy there", drive)
            size = self.get_dir_size(dir_path=dest_loc)
            self.logger.debug("current dir size in destination = %d", size)

            free_space = self.get_free_space(dest_mount)
            self.logger.debug("free space in location = %d", free_space)

            if (free_space - (drive_size - size)) > 200000:
                self.logger.debug("We will update current dataset in %s", dest_loc)
                print("We'll copy from", src_loc, "to", dest_loc)
                final_cmd = self.sync_dir(src=src_loc, dest=dest_loc)
                print(f"final_cmd={final_cmd}")

        if self.raw:
            cmd = "unlink %s/radarRawData" % link_dest
            os.system(cmd)
            print("unlink soft link", cmd)


if __name__ == '__main__':
    # python3 file_transfer.py -wf 92 -type *ensemble_3d_*.json
    # python3 file_transfer.py -wf 36 --raw -exclude *S_ANT*.npy

    parser = argparse.ArgumentParser(description='Evaluation arguments')
    parser.add_argument('-drive', required=False, type=str, default=False, help="specify GPU to use")
    parser.add_argument('-wf', required=True, type=str, help="specify GPU to use")
    parser.add_argument('-type', default=None, type=str, help="specify GPU to use")
    parser.add_argument('-exclude', default=None, type=str, help="specify GPU to use")
    parser.add_argument('--raw', default=False, action='store_true', help="specify GPU to use")
    parser.add_argument('-y', default=False, action='store_true', help="specify GPU to use")
    args = parser.parse_args()

    if not args.y:
        print("Did yo mounted on faraday?")
        ans = input()
        if not (ans == "y" or ans == "yes"):
            print("please mount")
            exit()

    ds = DatasetMover(raw=args.raw)

    if args.type is not None:
        print("setting specific type to include", args.type)
        ds.set_specific_type_include(fstring=args.type)
    elif args.exclude is not None:
        print("setting specific type to exclude", args.exclude)
        ds.set_specific_type_exclude(fstring=args.exclude)

    engines = get_engines(wf=args.wf, val_train="all", randomize=False, rect=False, df="sync_data_radar_lidar.csv", drive_str=None if not args.drive else args.drive)
    for engine in engines:
        print("engine.src_df.loc[faraday_disc]", engine.src_df.loc["faraday_disc"] )
        if not pd.isna(engine.src_df.loc["faraday_disc"]):
            print("skiiping")
            continue
        ds.move_engine(engine_to_move=engine)
    pass
