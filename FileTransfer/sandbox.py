import gc
from pprint import pprint
from collections import Counter


def my_func():
    a = [1] * (10 ** 6)
    b = [2] * (2 * 10 ** 7)
    del b
    return a

if __name__ == '__main__':
    a = my_func()
    c = Counter(type(o) for o in gc.get_objects())
    pprint(gc.get_objects())