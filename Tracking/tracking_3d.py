import sys
import uuid
from copy import deepcopy

from DataPipeline.pipeline4_utils import save_obj, pickle_write
from DataPipeline.src.GetEngines import get_engines
from Tracking.AB3DMOT.AB3DMOT_libs.model import AB3DMOT
from wiseTypes.Label3D import Label3D

# sys.path.insert(0, "/home/amper/AI/Meir/radar_perception/")
# sys.path.insert(0, "/home/amper/AI/Meir/radar_perception/external_repos/AB3DMOT/")
# from AB3DMOT_libs.model import AB3DMOT
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pathlib import Path
import os
import yaml
import json
import pickle
from pprint import pprint
import cv2
from tqdm import tqdm

all_debug_dicts = {}

all_trackers = {}


def get_bbox(label: Label3D):
    #     return [label.height, label.width, label.length, label.tx, label.tz, label.ty, label.heading]
    return [2, label.width, label.length, label.tx, 0, label.ty, label.heading - np.pi / 2]


def to_bbox(det_array):
    h, w, l, x, z, depth, theta = det_array[:7]
    return Label3D('car', 1, x, depth, z, w, l, h, theta + np.pi / 2, instance_id=int(det_array[7]))


def validate_uuid(engine, idx):
    labels = engine.get_lidar_ssn(idx, as_obj=False)

    if "uuid" not in labels:
        if "tensor" in labels:
            uuids = np.asarray([uuid.uuid4() for _ in labels["tensor"]])
            labels["uuids"] = uuids
            labels_path = engine.get_lidar_ssn(idx, as_obj=False, path_only=True)
            pickle_write(file_path=labels_path, obj=labels)

    # labels = engine.get_lidar_ssn(idx, as_obj=True)


def run_tracking_3d_engine(engine, max_age=4, iou_threshold=0.4):
    mot_tracker = AB3DMOT(max_age=max_age, iou_threshold=iou_threshold)

    all_tracks = []
    current_df = engine.df.copy()

    engine.df = engine.df.drop_duplicates(engine.ts_lidar).reset_index(drop=True)
    # print(engine.df.head())
    ego_vel = np.array([0, 0, 0])
    p = engine.get_lidar_ssn(0, path_only=True)
    save_tracks_path = os.path.split(p)[0] + "/tracks.pkl"

    for idx in tqdm(range(len(engine))):
        dt = engine.df.timestamp_lidar.iloc[idx - 1 if idx > 0 else idx:idx + 1].diff().iloc[-1] * 1e-9
        # print("idx", idx, "dt", dt)
        delta_pickle = engine.get_processed_delta(idx, pickle_only=True)
        if type(delta_pickle) != int:
            ego_vel = delta_pickle['metadata']['platform_velocity']
            ego_vel = np.array([ego_vel[0], 0, ego_vel[1]])

        timestamp = engine.get_lidar_id(idx)

        validate_uuid(engine, idx)
        labels = engine.get_lidar_ssn(idx, min_score=0.2)
        if len(labels) == 0:
             continue

        for label in labels:
            label.meta_data['idx'] = idx
            label.meta_data['ts'] = timestamp

        # labels = [l for l in labels if l.cls == 'car']
        dets_all = dict(
            dets=np.array(list(map(get_bbox, labels))),
            info=np.array(list(map(get_bbox, labels)))
        )
        # print(dets_all)
        mot_tracker.velocity_compensation(ego_vel, dt)
        trackers = mot_tracker.update(dets_all)
        # all_debug_dicts[idx] = deepcopy(mot_tracker.debug_dict)
        # all_trackers[idx] = trackers.copy()

        for label_idx, trk_idx in mot_tracker.debug_dict["matches"]:
            all_tracks.extend([dict(label_uuid=labels[label_idx].uuid, track_uuid=mot_tracker.debug_dict["trackers"][trk_idx].uuid,
                                    timestamp_lidar=engine.get_lidar_id(idx))])

        # all_tracks.extend(mot_tracker.debug_dict["matches"])

    # all_tracks.extend(mot_tracker.trackers)
    # all_tracks_dicts = []
    # for label, track in all_tracks:
    #     track_uuid = uuid.uuid4()
    #     all_tracks_dicts.extend([dict(track_uuid=str(track_uuid), timestamp_lidar=engine.get_lidar_id(idx), label_uuid=label.uuid) for
    #                              idx, label in track.matched_labels.items()])
    track_df = pd.DataFrame(all_tracks)
    print(track_df.shape)
    print(track_df.head())
    track_df.to_pickle(save_tracks_path)
    print("saved\n", save_tracks_path)

    engine.df = current_df


def draw_debug(engine, all_trackers, all_debug_dicts):
    for idx, trks in all_trackers.items():
        tracks_as_labels = list(map(to_bbox, trks))
        dets = engine.get_lidar_ssn(idx)
        f, axs = plt.subplots(1, 2, figsize=(15, 5), tight_layout=True)
        ax = axs[1]
        plt.sca(ax)

        #     for obj in dets:
        #         obj.plot(ax=ax, bound="r-bbox", annotation_font_size=0)

        #     for obj in tracks_as_labels:
        #         c = c_map(obj.instance_id % num_colors)[:3]
        #         c = tuple((np.asarray(c)).tolist())
        #         # tracker.tracks[j].plot(ax=ax, c=[c], s=40)
        #         obj.plot(ax=ax, bound="r-bbox", annotation_font_size=0, c=[c])
        # #     engine.get_lidar_df(idx).plot.scatter('x', 'y', s=1, c='z', ax=ax, cmap='jet')
        #     ax.set_ylim([0, 40])
        #     ax.set_xlim([-20, 20])

        for i, obj in enumerate(all_debug_dicts[idx]['d']):
            lw = 3 if i in all_debug_dicts[idx]['matches'][:, 0] else 1
            plt.plot(obj[:, 0], obj[:, 2], 'w', linewidth=lw)

        #         plt.annotate(i, obj.mean(0)[[0,2]], color='w')

        for i, obj in enumerate(all_debug_dicts[idx]['t']):
            lw = 3 if i in all_debug_dicts[idx]['matches'][:, 1] else 1
            plt.plot(obj[:, 0], obj[:, 2], 'm', linewidth=lw)
            trk_annot = f"{all_debug_dicts[idx]['t_id'][i]} {all_debug_dicts[idx]['t_vel'][i].round(1)}"
            plt.annotate(trk_annot, obj.mean(0)[[0, 2]] + 2, color='c', clip_on=True)

        for d_id, t_id in all_debug_dicts[idx]['matches']:
            match_det = all_debug_dicts[idx]['d'][d_id].mean(0)
            match_trk = all_debug_dicts[idx]['t'][t_id].mean(0)
            plt.plot([match_det[0], match_trk[0]], [match_det[2], match_trk[2]], 'r', linewidth=5)
        ax.set_ylim([0, 50])
        ax.set_xlim([-20, 20])

        ax = axs[0]
        ax.imshow(engine.get_image(idx))
        ax.axis(False)
        plt.title(idx)
        plt.show()


def main():
    engine = get_engines(wf="198", df="sync_data_lr1000_cr1000_lc1000_gr50.csv", rect=True, drive_str="210720")[0]
    engine.df = engine.df.drop_duplicates('timestamp_lidar').reset_index(drop=True)

    start_idx = 26794
    len_trk = 20
    engine.update_df_range(start_idx, start_idx + len_trk)
    # validate_uuid(engine, 0)
    run_tracking_3d_engine(engine)


if __name__ == '__main__':
    main()
