import torch
import logging
import numpy as np
from torch.fft import fft, rfft, fft2
import torch.nn.functional as F



class PhaseCalibrator:
    def __init__(self, rx, tx):
        RX_PHASES = [0, -0.182282, 0.0749999, 0.163897, 0.0140655, -0.0946823, -0.00640535, -0.0808343]
        RX_AMP = [1, 1.0346, 1.18254, 1.00691, 1.13207, 1.08796, 1.13861, 1.09922]

        TX_PHASES = [0, -0.00693047, 0.0153946, -0.0738606, 0.186147, 0.176745]
        TX_AMP = [1, 0.986186, 1.14752, 1.05445, 1.06904, 1.12718]

        num_channels = rx * tx
        self.calibration_window = np.zeros(num_channels, dtype=np.complex)
        for i_tx in range(rx):
            for i_rx in range(tx):
                phase_value = RX_PHASES[i_rx] + TX_PHASES[i_tx]
                amp_value = RX_AMP[i_rx] * TX_AMP[i_tx]
                cur_value = 0 + (-1j * phase_value)
                cur_value = np.exp(cur_value)
                cur_value *= amp_value

                win_idx = i_tx * rx + i_rx
                self.calibration_window[win_idx] = cur_value

    def calibrate(self, data):
        return


def reshape_fortran(x, shape):
    if len(x.shape) > 0:
        x = x.permute(*reversed(range(len(x.shape))))
    return x.reshape(*reversed(shape)).permute(*reversed(range(len(shape))))


class DSP:
    def __init__(self, samples, rx, tx, pulses, n_azimuth, n_elevation, logger=None):
        self.samples = samples
        self.tx, self.rx = tx, rx
        self.pulses = pulses
        # self.n_azimuth, self.n_elevation = 20, 6
        self.n_azimuth, self.n_elevation = n_azimuth, n_elevation

        self.init_shape = (self.samples, self.rx, self.tx * self.pulses)
        self.logger = logger if logger is not None else logging.getLogger()
        self.ch_order = np.array([[0, 4], [1, 5], [0, 6], [1, 7], [0, 8], [1, 9], [0, 10], [1, 11], [0, 12], [1, 13], [0, 14], [1, 15], [0, 16], [1, 17], [0, 18],
                                  [1, 19], [2, 2], [3, 3], [2, 4], [3, 5], [2, 6], [3, 7], [2, 8], [3, 9], [2, 10], [3, 11], [2, 12], [3, 13], [2, 14], [3, 15],
                                  [2, 16], [3, 17], [4, 0], [5, 1], [4, 2], [5, 3], [4, 4], [5, 5], [4, 6], [5, 7], [4, 8], [5, 9], [4, 10], [5, 11], [4, 12], [5, 13],
                                  [4, 14], [5, 15]])

    def set_array(self, ch_order):
        assert self.ch_order.shape == ch_order.shape
        self.ch_order = ch_order

    def range_processing(self, data):
        """
        @param data: flattened data, reshape inside
        @return: channels, samples, pulses, real/im
        """
        # real fft
        data_fixed = data.copy()
        data_fixed = data_fixed.reshape((self.samples, self.rx * self.tx, self.pulses), order="F")  # samples, channels, pulses
        self.logger.debug("range_processing: start data_fixed %s", data_fixed.shape)
        array = torch.from_numpy(data_fixed).float()

        # Window
        array = array.T  # pulses, channels, samples
        self.logger.debug("range_processing:before window: array=%s", array.shape)
        # window = torch.hann_window(window_length=array.shape[-1])
        window = torch.hann_window(window_length=array.shape[-1])*10
        self.logger.debug("range_processing:window=%s", window.shape)
        array = array * window
        array = array.T  # samples, channels, pulses

        # Apply FFT
        self.logger.debug("before permute %s", array.shape)
        array = array.permute(2, 1, 0)  # pulses, channels, samples
        self.logger.debug("before fft: array=%s", array.shape)
        array = rfft(array, dim=-1)  # pulses, channels, samples, real/im
        self.logger.debug("after fft: array=%s, type=%s", array.shape, array.dtype)
        array = array[:, :, :-1]
        array = array.permute(1, 2, 0)  # channels, samples, pulses
        self.logger.debug("final: array=%s", array.shape)
        return array

    def doppler_processing(self, data):
        """
        @param data: channels, samples, pulses, real/im
        @return: channels,  samples, pulses, real/im
        """
        self.logger.debug("doppler_processing: start=%s", data.shape)
        fftd_p = data.clone() #.permute((0, 1, 2))  # channels,  samples,  pulses
        self.logger.debug("doppler_processing: before window=%s", fftd_p.shape)
        window = torch.hann_window(window_length=fftd_p.shape[-1])*10
        self.logger.debug("doppler_processing: window=%s", window.shape)
        fftd_p = fftd_p * window
        fftd_p = fftd_p #.permute((0, 1, 2))  # channels,  samples, pulses
        self.logger.debug("doppler_processing: after window=%s", fftd_p.shape)
        self.logger.debug("before fft: array=%s", fftd_p.shape)
        fftd_p = fft(fftd_p, dim=-1)
        self.logger.debug("after fft: array=%s", fftd_p.shape)
        return fftd_p

    def angular_processing(self, data, os_az=1, os_el=1):
        """

        @param data: channels,  samples, pulses
        @return:
        """
        self.logger.debug("angular_processing: start=%s", data.shape)
        sant = data.clone().permute((1, 0, 2)) # range (samples), channels, dop (pulses)
        self.logger.debug("angular_processing: before_padding=%s", sant.shape)
        s_t = torch.zeros(sant.shape[2], sant.shape[0], self.n_elevation, self.n_azimuth, dtype=torch.cfloat)  # r, d, el, az, 2
        self.logger.debug("angular_processing: s_t=%s", s_t.shape)

        # print("\n\nangular_processing: Change from 0-1\n\n")

        # s_t[:, :, self.ch_order[:, 0], self.ch_order[:, 1]] = sant.permute((2, 0, 1))
        print("S_ANT", sant.shape)
        s_t[:, :, self.ch_order[:, 0], self.ch_order[:, 1]] = sant.permute((2, 0, 1))

        if os_az != 1 and os_el != 1:
            # s_t = F.pad(s_t, (0, 20 * os_az - 20, 0, 6 * os_el - 6), "constant", 0)
            s_t = F.pad(s_t, (0, self.n_azimuth * os_az - self.n_azimuth, 0, self.n_elevation * os_el - self.n_elevation), "constant", 0)
        # s_t[:, :, self.ch_order[:, 0], self.ch_order[:, 1], 1] = sant[..., 1].permute((2, 0, 1))
        # s_t[:, :, self.ch_order[:, 0], self.ch_order[:, 1], 0] = sant[..., 0].permute((2, 0, 1))
        # s_t[:, :, self.ch_order[:, 0], self.ch_order[:, 1], 1] = sant[..., 1].permute((2, 0, 1))

        # window = torch.hann_window(window_length=s_t.shape[-1])
        # s_t = s_t * window

        ffta = fft2(s_t, norm="backward")  # torch.Size([48, 600, 6, 20, 2]) (dop,rng,el,az,real/imag)
        self.logger.debug("angular_processing: final=%s", s_t.shape)
        return ffta

    def calibration(self, data):
        pass

    def run(self, raw_data):
        data = raw_data.copy().flatten(order="F").reshape(self.init_shape, order="F")
        pass


if __name__ == '__main__':
    pass
