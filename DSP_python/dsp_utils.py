import torch
import logging
import torch
import torch.utils.data as data
import numpy as np
import cv2
import random
from skimage.draw import random_shapes
import imageio
import matplotlib.pyplot as plt
import os
from matplotlib import gridspec, colors
from scipy.io import loadmat
import matplotlib

module_path = os.path.abspath('')

plt.style.use('dark_background')
font = {'size': 16}
matplotlib.rc('font', **font)
cmap = "turbo"
pad = 15


def db(array):
    return 10 * np.log10(array)


def reshape_fortran(x, shape):
    if len(x.shape) > 0:
        x = x.permute(*reversed(range(len(x.shape))))
    return x.reshape(*reversed(shape)).permute(*reversed(range(len(shape))))

def arr_to_disp(arr):
    return cv2.resize(np.log(np.abs(np.sum(arr, axis=0))), None, fx=15, fy=1)

def compare(python_arr, matlab_arr,
            title="",
            save_path=module_path + "/plots/range_processing_pytorch.png", *args, **kwargs):
    if python_arr.shape != matlab_arr.shape:
        print("arrays not in same shape", python_arr.shape, matlab_arr.shape)
        return

    def func(arr):
        return cv2.resize(np.log(np.abs(np.sum(arr, axis=0))), None, fx=15, fy=1)

    gs = gridspec.GridSpec(ncols=3, nrows=2)
    plt.figure(figsize=(30, 15), dpi=100)
    fig = plt.gcf()

    # real
    ax = fig.add_subplot(gs[0, 0])
    scat = plt.imshow(func(python_arr.real), cmap=cmap)
    plt.title("DSP-Python | PyTorch | real", pad=pad)
    plt.colorbar(scat)

    ax = fig.add_subplot(gs[1, 0])
    scat = plt.imshow(func(matlab_arr.real), cmap=cmap)
    plt.title("DSP-Matlab | real", pad=pad)
    plt.colorbar(scat)

    # imag
    ax = fig.add_subplot(gs[0, 1])
    scat = plt.imshow(func(python_arr.imag), cmap=cmap)
    plt.title("DSP-Python | PyTorch | imag", pad=pad)
    plt.colorbar(scat)

    ax = fig.add_subplot(gs[1, 1])
    scat = plt.imshow(func(matlab_arr.imag), cmap=cmap)
    plt.title("DSP-Matlab | imag", pad=pad)
    plt.colorbar(scat)

    # power
    ax = fig.add_subplot(gs[0, 2])
    scat = plt.imshow(func(python_arr), cmap=cmap)
    plt.title("DSP-Python | PyTorch | abs", pad=pad)
    plt.colorbar(scat)

    ax = fig.add_subplot(gs[1, 2])
    scat = plt.imshow(func(matlab_arr), cmap=cmap)
    plt.title("DSP-Matlab | abs", pad=pad)
    plt.colorbar(scat)

    l1_error = "\nL1=" + str(np.sum(np.abs(matlab_arr - python_arr)))
    plt.suptitle(str(title) + " comparison | PyTorch" + l1_error)
    plt.tight_layout(pad=1)
    plt.savefig(save_path)
    plt.close()


def compare_not_complex(python_arr, matlab_arr,
                        title="",
                        save_path=module_path + "/plots/range_processing_pytorch.png", *args, **kwargs):
    if python_arr.shape != matlab_arr.shape:
        print("arrays not in same shape", python_arr.shape, matlab_arr.shape)
        return

    def func(arr):
        return cv2.resize(np.log(np.abs(np.sum(arr, axis=0))), None, fx=15, fy=1)

    gs = gridspec.GridSpec(ncols=3, nrows=2)
    plt.figure(figsize=(20, 10))
    fig = plt.gcf()

    # real
    ax = fig.add_subplot(gs[0, 0])
    scat = plt.imshow(func(python_arr[..., 0]), cmap=cmap)
    plt.title("DSP-Python | PyTorch | real", pad=pad)
    plt.colorbar(scat)

    ax = fig.add_subplot(gs[1, 0])
    scat = plt.imshow(func(matlab_arr[..., 0]), cmap=cmap)
    plt.title("DSP-Matlab | real", pad=pad)
    plt.colorbar(scat)

    # imag
    ax = fig.add_subplot(gs[0, 1])
    scat = plt.imshow(func(python_arr[..., 1]), cmap=cmap)
    plt.title("DSP-Python | PyTorch | imag", pad=pad)
    plt.colorbar(scat)

    ax = fig.add_subplot(gs[1, 1])
    scat = plt.imshow(func(matlab_arr[..., 1]), cmap=cmap)
    plt.title("DSP-Matlab | imag", pad=pad)
    plt.colorbar(scat)

    # power
    ax = fig.add_subplot(gs[0, 2])
    scat = plt.imshow(func(python_arr[..., 0] + 1j * python_arr[..., 1]), cmap=cmap)
    plt.title("DSP-Python | PyTorch | abs", pad=pad)
    plt.colorbar(scat)

    ax = fig.add_subplot(gs[1, 2])
    scat = plt.imshow(func(matlab_arr[..., 0] + 1j * matlab_arr[..., 1]), cmap=cmap)
    plt.title("DSP-Matlab | abs", pad=pad)
    plt.colorbar(scat)

    l1_error = "\nL1=" + str(np.sum(np.abs(matlab_arr - python_arr)))
    plt.suptitle(str(title) + " comparison | PyTorch" + l1_error)
    plt.tight_layout(pad=1)
    plt.savefig(save_path)
    plt.close()


def compare_numpy(python_arr, matlab_arr, save_path=module_path + "/plots/range_processing_numpy.png", *args, **kwargs):
    if python_arr.shape != matlab_arr.shape:
        logging.warning("arrays not in same shape")

    logging.debug("compare_numpy python=%s, %s", python_arr.min(), python_arr.max())
    logging.debug("compare_numpy matlab_arr=%s, %s", matlab_arr.min(), matlab_arr.max())

    def func(arr):
        return cv2.resize(np.log(np.abs(np.sum(arr, axis=0))), None, fx=15, fy=1)

    gs = gridspec.GridSpec(ncols=3, nrows=2)
    plt.figure(figsize=(20, 10))
    fig = plt.gcf()

    # real
    ax = fig.add_subplot(gs[0, 0])
    scat = plt.imshow(func(python_arr.real), cmap=cmap)
    plt.title("DSP-Python | Numpy | real", pad=pad)
    plt.colorbar(scat)

    ax = fig.add_subplot(gs[1, 0])
    scat = plt.imshow(func(matlab_arr.real), cmap=cmap)
    plt.title("DSP-Matlab | real", pad=pad)
    plt.colorbar(scat)

    # imag
    ax = fig.add_subplot(gs[0, 1])
    scat = plt.imshow(func(python_arr.imag), cmap=cmap)
    plt.title("DSP-Python | Numpy | imag", pad=pad)
    plt.colorbar(scat)

    ax = fig.add_subplot(gs[1, 1])
    scat = plt.imshow(func(matlab_arr.imag), cmap=cmap)
    plt.title("DSP-Matlab | imag", pad=pad)
    plt.colorbar(scat)

    # sum
    ax = fig.add_subplot(gs[0, 2])
    scat = plt.imshow(func(python_arr), cmap=cmap)
    plt.title("DSP-Python | Numpy | abs", pad=pad)
    plt.colorbar(scat)

    ax = fig.add_subplot(gs[1, 2])
    scat = plt.imshow(func(matlab_arr), cmap=cmap)
    plt.title("DSP-Matlab | abs", pad=pad)
    plt.colorbar(scat)

    plt.suptitle("Range Processing comparison | Numpy")
    plt.tight_layout(pad=1)
    plt.savefig(save_path)
    plt.close()


def image2d(ffta, cfar=None, cut_az=0, cut_el=0):
    s_el = int(cut_el*ffta.shape[2])
    e_el = ffta.shape[2] - s_el

    s_az = int(cut_az * ffta.shape[3])
    e_az = ffta.shape[3] - s_az

    print("el=", s_el, e_el, "az=", s_az, e_az)
    ffta_n = ffta[:, :, s_el:e_el, s_az:e_az]
    # ffta_n = ffta_n[..., 0] + 1j * ffta_n[..., 1]
    ffta_n = np.sqrt(ffta_n.real ** 2 + ffta_n.imag ** 2)
    ffta_n = np.sum(np.sum(ffta_n, axis=0), axis=1)
    ffta_n = db(np.abs(ffta_n))
    if cfar is not None:
        ffta_n[ffta_n <= cfar] = None
    # return np.flipud(cv2.resize(ffta_n, None, fx=40, fy=1))
    return np.flipud(cv2.resize(ffta_n, (400, 200)))
    # plt.figure(figsize=(20, 10))
    # plt.imshow(, cmap="turbo")
