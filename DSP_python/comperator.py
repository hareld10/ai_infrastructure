import os

from DSP_python.torch_loader import DatasetFromFolder
from DataPipeline.src.GetEngines import get_engines
import numpy as np

if __name__ == '__main__':
    engines = get_engines(wf="36", val_train="all", randomize=False, df="/../filtered_data.csv", drive_str="191202")
    engine = engines[0]
    print(engine.get_drive_name())
    example_dataset = DatasetFromFolder(df=engine.df)
    save=True
    for i in np.arange(0, 1000, 100):
        # print(example_dataset[i]["timestampRadar"], example_dataset[i]["filenameRadar"])
        # print(example_dataset[i]["input"].shape, example_dataset[i]["input"].dtype)
        if save:
            save_path = "./save_matlab/" + engine.get_drive_name() + "/" + example_dataset[i]["filenameRadar"][:-4] + "/" + str(i) + "_input"
            os.makedirs(os.path.split(save_path)[0], exist_ok=True)
            # print("size", example_dataset[i]["input"].size * example_dataset[i]["input"].itemsize)
            # print(np.unique(example_dataset[i]["input"]))
            np.save(save_path, example_dataset[i]["input"])
