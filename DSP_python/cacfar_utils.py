import numpy as np
import torch


def detect_peaks_torch(x, num_train, num_guard, rate_fa):
    """
    Detect peaks with CFAR algorithm.

    num_train: Number of training cells.
    num_guard: Number of guard cells.
    rate_fa: False alarm rate.
    """
    num_cells = x.shape[0]
    num_train_half = round(num_train / 2)
    num_guard_half = round(num_guard / 2)
    num_side = num_train_half + num_guard_half

    alpha = num_train * (rate_fa ** (-1 / num_train) - 1)  # threshold factor

    peak_idx = []
    for i in range(num_side, num_cells - num_side):

        if i != i - num_side + torch.argmax(x[i - num_side:i + num_side + 1]):
            continue

        sum1 = torch.sum(x[i - num_side:i + num_side + 1])
        sum2 = torch.sum(x[i - num_guard_half:i + num_guard_half + 1])
        p_noise = (sum1 - sum2) / num_train
        threshold = alpha * p_noise

        if x[i] > threshold:
            peak_idx.append(i)

    peak_idx = torch.tensor(peak_idx, dtype=int)

    return peak_idx


def cacfar_2d_torch(mat_input, tr, tc, gc, gr, offset_noise):
    """

    @param mat_input: (row, col) with values
    @param tr: training band rows
    @param tc: training band columns
    @param gc: guard band columns
    @param gr: guard band rows
    @param offset_noise:
    @return:
    """
    w_cols = 2 * tc + 2 * gc + 1
    w_rows = 2 * tr + 2 * gr + 1

    # Create mask for patch
    mask = torch.zeros((w_rows, w_cols))
    cx, cy = np.floor(mask.shape[0] / 2).astype(int), np.floor(mask.shape[1] / 2).astype(int)
    mask[cx - gr: cx + gr + 1, cy - gc: cy + gc + 1] = 1
    mask[cx, cy] = 2

    # Pad
    pad_mat = mat_input.unsqueeze(0).unsqueeze(0)
    original_shape = pad_mat.shape[2:]
    pad_mat = torch.nn.functional.pad(pad_mat, pad=(tc+gc, tc+gc, tr+gr, tr+gr))

    # Split for patches
    patches = pad_mat.unfold(2, w_rows, 1).unfold(3, w_cols, 1).squeeze(0).squeeze(0)
    ret = torch.zeros(original_shape)
    for row_idx in range(patches.shape[0]):
        for col_idx in range(patches[row_idx].shape[0]):
            patch = patches[row_idx, col_idx]
            # Noise estimation
            noise = torch.median(patch[mask == 0]) + offset_noise
            ret[row_idx, col_idx] = patch[cx, cy] >= noise

    return ret.nonzero()


def detect_peaks(x, num_train, num_guard, rate_fa):
    """
    Detect peaks with CFAR algorithm.

    num_train: Number of training cells.
    num_guard: Number of guard cells.
    rate_fa: False alarm rate.
    """
    num_cells = x.size
    num_train_half = round(num_train / 2)
    num_guard_half = round(num_guard / 2)
    num_side = num_train_half + num_guard_half

    alpha = num_train * (rate_fa ** (-1 / num_train) - 1)  # threshold factor

    peak_idx = []

    for i in range(num_side, num_cells - num_side):

        if i != i - num_side + np.argmax(x[i - num_side:i + num_side + 1]):
            continue

        sum1 = np.sum(x[i - num_side:i + num_side + 1])
        sum2 = np.sum(x[i - num_guard_half:i + num_guard_half + 1])
        p_noise = (sum1 - sum2) / num_train
        threshold = alpha * p_noise

        if x[i] > threshold:
            peak_idx.append(i)

    peak_idx = np.array(peak_idx, dtype=int)

    return peak_idx