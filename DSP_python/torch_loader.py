import pathlib
import sys
import time
import os
import matplotlib.pyplot as plt
import torch
import torch.utils.data as data
import numpy as np
import cv2
import random
from pprint import pprint
from matplotlib import gridspec
from skimage.draw import random_shapes
import imageio

# Define data loader
from tqdm import tqdm
import pathlib
import sys
module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, "/workspace/AI/Harel/ai_infrastructure/")
sys.path.insert(0, module_path)
sys.path.insert(0, module_path + "/../")
sys.path.insert(0, "%s/../DataPipeline/" % module_path)

from Calibrator.projection import toRAzEl
from DSP_python.dsp_python import DSP
from DSP_python.dsp_utils import image2d, db, arr_to_disp
from DataPipeline.src.GetEngines import get_engines
# from LabelsManipulation.engine_tasks import generate_engine
plt_font = 20
font = {'size': plt_font}
import matplotlib
matplotlib.rc('font', **font)

import WiseSDK
from DataPipeline.raw_reader import RawReader
# from DataPipeline.src.GetEngines import get_engines
from Evaluation.evaluation_utils import make_video_from_dir


class DatasetFromFolder(data.Dataset):
    def __init__(self, df, num_samples=1024, num_rx=8, num_tx=6, num_pulses=64, engine=None):
        self.df = df
        self.raw_reader = RawReader(init=False)
        self.first_sleep = True
        self.num_samples, self.num_rx, self.num_pulses, self.num_tx = num_samples, num_rx, num_pulses, num_tx
        self.engine = engine

    def __len__(self):
        return len(self.df)

    def get_raw(self, radar_path, idx):
        radar_frame = WiseSDK.getRadarFrame(radar_path, int(float(idx)))
        header = radar_frame.getHeader()
        if (header.dims.size1 == 0):
            return [-1, [], [], [], [], []]

        # num_samples, num_rx, num_tx, num_pulses = find_data_size_properties(radar_frame)
        z_out = radar_frame.getData()
        z_out = np.reshape(z_out, (8192, 480), order='F')
        z_out = z_out[:, :384]
        z_out = z_out / 4
        z_out = z_out * 16
        z_out = z_out.astype(np.short)
        z_out = z_out / 16
        return z_out.astype(np.complex)

    def __getitem__(self, idx):
        while True:
            try:
                if idx >= len(self.df):
                    return None

                if not os.path.exists(self.engine.get_image(idx=idx, path_only=True)):
                    idx += 1
                    continue
                radar_path = str(self.df.iloc[idx]["radar_src"]) + str(self.df.iloc[idx]["filenameRadar"])
                # mimo, ppa = RawReader.get_ppa_mimo_raw(radar_path=radar_path, sleep=self.first_sleep)
                mimo = self.get_raw(radar_path=radar_path, idx=str(self.df.iloc[idx]["inFileIdxRadar"]))
                shape = (self.num_rx, self.num_samples, self.num_tx * self.num_pulses)
                mimo = mimo.reshape(shape, order="F")
                mimo = mimo.transpose((1, 0, 2))

                sample = {"input": mimo.flatten(order="F"),
                          'timestampRadar': str(self.df.loc[idx, 'timestampRadar']),
                          'filenameRadar': str(self.df.loc[idx, 'filenameRadar']),
                          "engine_idx": str(idx)}
                self.first_sleep = False
                return sample
            except Exception as e:
                print(e)
                idx += 1
                continue


def save_to_compare(example_dataset):
    save = True
    for i in range(len(example_dataset)):
        print(example_dataset[i]["timestampRadar"], example_dataset[i]["filenameRadar"])
        print(example_dataset[i]["input"].shape, example_dataset[i]["input"].dtype)
        if save:
            save_path = "./save_matlab/" + engine.get_drive_name() + "/" + example_dataset[i]["filenameRadar"][:-4] + "/" + str(i) + "_input"
            print("size", example_dataset[i]["input"].size * example_dataset[i]["input"].itemsize)
            print(np.unique(example_dataset[i]["input"]))
            np.save(save_path, example_dataset[i]["input"])
        exit()


def run_vis():
    engines = get_engines(wf="93", val_train="control", randomize=False, rect=False, df="sync_data_radar_lidar.csv", drive_str="corner")
    # 92 MIMO 1024, 8, 6, 64

    # print(engines)
    # for engine in engines:
    #     print(engine.get_drive_name())
    #     generate_engine(engine)
    #     pass

    for engine in engines:
        engine.sample_df_range(20)
        # engine.update_df_range(50, 55)
        print(engine.get_drive_name(), len(engine))
        example_dataset = DatasetFromFolder(df=engine.df, engine=engine)
        dsp = DSP(samples=1024, rx=8, tx=6, pulses=64)

        ch_order = np.array([[0, 4], [1, 5], [0, 6], [1, 7], [0, 8], [1, 9], [0, 10], [1, 11], [0, 12], [1, 13], [0, 14], [1, 15], [0, 16], [1, 17], [0, 18],
                              [1, 19], [2, 2], [3, 3], [2, 4], [3, 5], [2, 6], [3, 7], [2, 8], [3, 9], [2, 10], [3, 11], [2, 12], [3, 13], [2, 14], [3, 15],
                              [2, 16], [3, 17], [4, 0], [5, 1], [4, 2], [5, 3], [4, 4], [5, 5], [4, 6], [5, 7], [4, 8], [5, 9], [4, 10], [5, 11], [4, 12], [5, 13],
                              [4, 14], [5, 15]])

        # dsp.set_array(np.ascontiguousarray(np.random.permutation(ch_order)))
        vid_dir = "./plots/"

        cfar = -1
        for i in tqdm(range(len(example_dataset))):
            batch = example_dataset[i]
            if batch is None:
                print("batch is None")
                continue
            raw_data = batch["input"]
            engine_idx = int(batch["engine_idx"])
            assert engine.get_id(idx=engine_idx) == batch["timestampRadar"]

            fftr_p = dsp.range_processing(raw_data)  # pulses, samples, channels, real/im
            fftd_p = dsp.doppler_processing(fftr_p)
            ffta_p = dsp.angular_processing(fftd_p, os_az=8, os_el=4)
            print("ffta.shape", ffta_p.shape)
            fig = plt.figure(figsize=(30, 10))
            # gs = gridspec.GridSpec(ncols=3 if cfar is not -1 else 2 , nrows=2)
            gs = gridspec.GridSpec(ncols=3, nrows=1)

            # image
            ax = fig.add_subplot(gs[0, 0])
            ax.imshow(engine.get_image(idx=engine_idx))
            ax.axis("off")
            ax.set_title("Camera")

            # image 2d
            ax = fig.add_subplot(gs[0, 1])

            #
            # print("dsp-pyton", fftd_p.shape)
            scat = ax.imshow(arr_to_disp(arr=fftd_p.numpy()), cmap="turbo")
            ax.set_title("MIMO Range-Doppler | Python")
            plt.colorbar(scat, ax=ax, fraction=0.026, pad=0.08)

            disp = "ffta"
            ax = fig.add_subplot(gs[0, 2])
            ax.set_anchor('W')
            if disp == "sant":
                s_ant = engine.get_s_ant(idx=engine_idx)
                s_mask = np.zeros((512, 8, 96))
                s_mask[:, :, :-1] = s_ant
                s_ant = s_mask.reshape((512, 8, 16, 6), order="F")
                s_ant = s_ant.transpose((0, 1, 3, 2))
                s_ant = s_ant.reshape((512, 48, 16), order="F")
                s_ant = s_ant.transpose((1, 0, 2))
                scat = ax.imshow(arr_to_disp(arr=s_ant), cmap="turbo")
                ax.set_title("PPA Range-Doppler | Cuda")
            elif disp == "ffta":
                scat = ax.imshow(image2d(ffta_p.numpy(), cfar=cfar, cut_az=0.2, cut_el=0.2), cmap="turbo")
                ax.set_title("Image 2D | Python")
            plt.colorbar(scat, ax=ax, fraction=0.026, pad=0.08)

            if cfar is not -1:
                ax = fig.add_subplot(gs[:, 2])
                fft = engine.get_4dfft(idx=engine_idx)
                fft["total_power"] = db(np.sqrt(fft["power_im"] ** 2 + fft["value_real"] ** 2))
                fft = fft[fft["total_power"] >= cfar]

                ax.set_xlim(-30, 30)
                ax.set_ylim(0, 80)
                scat = ax.scatter(x=fft["x"], y=fft["y"], c=fft["total_power"], cmap="jet", s=3)
                plt.colorbar(scat, ax=ax, orientation="horizontal")

            plt.suptitle("Image 2D | DSP Python | cfar= " + str(cfar) + "\n" + str(engine.get_drive_name()) + " | " + str(engine.get_id(engine_idx)) + "\n")
            plt.tight_layout()
            plt.savefig(vid_dir + str(engine.get_id(engine_idx)) + ".png")
            plt.close()

        make_video_from_dir(df=engine.df, images_dir=vid_dir, suffix="_" + str(time.time()).split(".")[0])


if __name__ == '__main__':
    run_vis()
