import pathlib
import sys
module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, "/workspace/AI/Harel/ai_infrastructure/")
sys.path.insert(0, module_path)
sys.path.insert(0, module_path + "/../")
sys.path.insert(0, "%s/../DataPipeline/" % module_path)

import WiseSDK
from DataPipeline.src.GetEngines import get_engines
from LabelsManipulation.engine_tasks import generate_engine

engines = get_engines(wf="95", val_train="control", randomize=False, rect=False, df="sync_data_radar_lidar.csv", drive_str="right")
# 92 MIMO 1024, 8, 6, 64

# print(engines)
for engine in engines:
    print(engine.get_drive_name())
    generate_engine(engine)