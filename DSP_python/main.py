import pathlib
import sys

module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, module_path)
sys.path.insert(0, "/workspace/AI/Harel/AI_Infrastructure/")
from DataPipeline.raw_reader import RawReader
from DataPipeline.src.GetEngines import get_engines

engines = get_engines(wf="36", val_train="all", randomize=False, df="/../filtered_data.csv")

if __name__ == '__main__':
    raw_reader = RawReader()
    for engine in engines:
        print(engine.get_drive_name(), len(engine))
        raw_reader.run(df=engine.df, context=engine.drive_context, write=True)



