from DataPipeline.DNNs.wrappers.exceptions import NotValidClass
from wiseTypes.Cluster import cmap_clusters
from wiseTypes.Label3D import Label3D
from wiseTypes.Types import MetaDataTypes
from wiseTypes.Label import Label
from wiseTypes.classes_dims import WiseClassesObjectDetection, Unclassified
import numpy as np
import pandas as pd
from wiseTypes.wise_types_utils import get_iou, compute_box_3d_ordered
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt

wise_classes = WiseClassesObjectDetection()


class Label2D(Label):
    def __init__(self, cls, score, x1=None, y1=None, x2=None, y2=None, force_obj=True):
        super().__init__()
        self.obj = wise_classes.get_obj_by_name(obj_name=cls)
        self.score = np.round(score, 2)
        self.x1, self.y1, self.x2, self.y2 = x1, y1, x2, y2
        self.set_cls(self.obj)

    def get_bev_bbox(self):
        return self.x1, self.y1, self.x2, self.y2

    def __repr__(self):
        return str(self.cls) + " : " + str(self.score) + " | " + str(self.x1) + "," + str(self.y1) + "," + str(
            self.x2) + "," + str(self.y2) + " | " + str(self.annotate)

    def to_label_3d(self, key="projected_3d_center_clustering"):
        if key not in self.meta_data:
            return False
        if "size_estimation" in self.write_data:
            tx, ty, tz, l, h, w, heading = self.write_data["size_estimation"]
        else:
            tx, ty, tz = self.meta_data[key]
            l, h, w, heading = 1, 1, 1, 0

        label_3d = Label3D(cls=self.cls, tx=tx, ty=ty, tz=tz, heading=heading,
                           score=self.score, width=w, length=l, height=h,
                           x1=self.x1, y1=self.y1, x2=self.x2, y2=self.y2,
                           instance_id=self.instance_id)
        if "associate_cluster_id" in self.meta_data:
            label_3d.associate_cluster = self.meta_data["associate_cluster_id"]

        label_3d.meta_data = self.meta_data
        label_3d.debug_data = self.debug_data
        label_3d.write_data = self.write_data
        return label_3d

    def plot(self, ax, circle_rad=2, s=30):
        if "3d_center_clustering" not in self.meta_data:
            return
        cluster_center = self.meta_data["projected_3d_center_clustering"]
        lidar_df = pd.DataFrame(data={"x": [cluster_center[0]], "y": [cluster_center[1]], "z": [cluster_center[2]]})
        scat = ax.scatter(lidar_df["x"], lidar_df["y"], color=np.asarray(self.color) / 255, s=s)
        if circle_rad is not None:
            ax.add_patch(
                plt.Circle((cluster_center[0], cluster_center[1]), circle_rad, color=np.asarray(self.color) / 255,
                           fill=False, zorder=0))
        if MetaDataTypes.associate_cluster_id.name in self.meta_data:
            ax.annotate(str(self.meta_data[MetaDataTypes.associate_cluster_id.name]),
                        (cluster_center[0], cluster_center[1]),
                        color=cmap_clusters(int(self.meta_data[MetaDataTypes.associate_cluster_id.name])),
                        fontsize=12)
        return

    def get_crop(self, im, mask_only=False):
        if mask_only:
            mask = np.zeros(im.shape[:2], dtype=bool)
            mask[int(self.y1):int(self.y2), int(self.x1):int(self.x2)] = True
            return mask
        return im[int(self.y1):int(self.y2), int(self.x1):int(self.x2)]

    def get_points_inside(self, df):
        return df[(df['x_pixel'] >= self.x1) & (df['x_pixel'] <= self.x2) &
                  (df['y_pixel'] >= self.y1) & (df['y_pixel'] <= self.y2)]
