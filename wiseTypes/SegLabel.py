import numpy as np
import cv2
from wiseTypes.classes_dims import WiseClassesSegmentation


class SegLabel:
    wise_classes = WiseClassesSegmentation()

    def __init__(self, img, engine, soft):
        self.label = np.zeros(img.shape[:2], dtype=np.uint8)
        self.soft_label = soft
        for cls_name, cls_idx in engine.get_seg_classes():
            cls_obj = SegLabel.wise_classes.get_obj_by_name(cls_name)
            if len(img.shape) == 3:
                self.label[np.all(img == cls_idx, axis=-1)] = cls_obj.unified_idx
            elif len(img.shape) == 2:
                self.label[img == cls_idx] = cls_obj.unified_idx
            else:
                print("SegLabel: Weird shape")
                raise Exception

    def get_soft(self):
        return self.soft_label

    def get_img(self, colors=False):
        if not colors:
            return self.label
        colored_img = np.zeros((self.label.shape[0], self.label.shape[1], 3), dtype=np.uint8)
        for uni_idx in np.unique(self.label):
            color = SegLabel.wise_classes.get_color_by_idx(unified_idx=uni_idx)
            colored_img[self.label == uni_idx] = color
        return colored_img

    def get_mask_of_points(self, imgfov_pc_pixel, cls):
        """

        @param imgfov_pc_pixel:
        @param cls:
        @return:
        """
        pixels = imgfov_pc_pixel.astype(np.int).T
        indices = np.argwhere(self.label == cls.unified_idx)

        return self._get_mask_of_intersection(pixels=pixels, indices=indices, imgfov_pc_pixel=imgfov_pc_pixel)
        # pixels2 = set([tuple(x) for x in pixels])
        # indices2 = set([tuple(reversed(x)) for x in indices])
        # intersection = pixels2.intersection(indices2)
        #
        # return [idx for idx, elem in enumerate(pixels) if tuple(elem) in intersection]

    def _get_mask_of_intersection(self, pixels, indices, imgfov_pc_pixel):
        """
        @param pixels: (X, 2)
        @param indices: (X, 2)
        @param imgfov_pc_pixel: (2, X)
        @return:
        """
        pixels2 = set([tuple(x) for x in pixels])
        indices2 = set([tuple(reversed(x)) for x in indices])
        intersection = pixels2.intersection(indices2)
        mask_indices = [idx for idx, elem in enumerate(pixels) if tuple(elem) in intersection]
        boolean_mask = np.zeros(imgfov_pc_pixel.shape[1], dtype=bool)
        boolean_mask[mask_indices] = True
        return boolean_mask

    def get_mask_of_boundary(self, imgfov_pc_pixel, cls, dilate_factor=15):
        pixels = imgfov_pc_pixel.astype(np.int).T

        road_mask = cv2.UMat((self.label == cls.unified_idx).astype(np.uint8))
        contours, hierarchy = cv2.findContours(road_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        contours = [max([(c, c.get().shape[0]) for c in contours], key=lambda x: x[1])[0]]

        # contours = [c for c in contours if c.get().shape[0] > 100]
        empty_mat = cv2.UMat(np.zeros((self.label.shape[0], self.label.shape[1], 3), dtype=np.uint8))
        empty_mat = cv2.drawContours(empty_mat, contours, -1, (0, 255, 0), 3)
        result = cv2.UMat.get(empty_mat)
        kernel = np.ones((dilate_factor, dilate_factor), np.uint8)
        result = cv2.dilate(result, kernel, iterations=1)
        non_zero = result.nonzero()

        indices = np.vstack([non_zero[0], non_zero[1]]).T  # (X, 2)

        ret_mask = self._get_mask_of_intersection(pixels=pixels, indices=indices, imgfov_pc_pixel=imgfov_pc_pixel)

        return ret_mask
