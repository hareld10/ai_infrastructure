from pprint import pprint

from DataPipeline.DNNs.wrappers.exceptions import NotValidClass
from DataPipeline.shared_utils import toRAzEl
from LabelsManipulation.clf_utils import compute_box_3d
from OpenSource.open_source_utils import lidar_to_radar_coordinates, radar_to_lidar_coordinates
from wiseTypes.Cluster import cmap_clusters
from wiseTypes.Label import Label
from wiseTypes.classes_dims import WiseClassesObjectDetection
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from shapely.geometry import Polygon

from wiseTypes.wise_types_utils import get_iou
from scipy import spatial
wise_classes = WiseClassesObjectDetection()


class Label3D(Label):
    def __init__(self, cls, score, tx, ty, tz, width, length, height, heading, instance_id="",
                 associate_cluster=-1, x1=None, y1=None, x2=None, y2=None, engine=None):
        super().__init__()
        self.tx, self.ty, self.tz = tx, ty, tz
        self.r, self.az, self.el = toRAzEl(self.tx, self.ty, self.tz)
        self.width, self.length, self.height = width, length, height
        self.score = score
        self.heading = heading
        self.cls = cls
        self.instance_id = instance_id
        self.x1, self.y1, self.x2, self.y2 = x1, y1, x2, y2
        self.obj = None
        self.engine = engine
        self.associate_cluster = associate_cluster
        self.box_points = None
        self.polygon = None
        # self.annotate = str(round(self.score, 2))
        # self.annotate = associate_cluster
        self.annotate = str(instance_id)
        try:
            self.obj = wise_classes.get_obj_by_name(obj_name=cls)
            self.cls = self.obj.name
        except NotValidClass as e:
            pass

        if self.cls == "pedestrian":
            self.width, self.length, self.height = 1, 1, 2
            # self.score = np.clip(np.random.random()+np.random.random(), 0, 1)
            self.score = np.clip(0.9 - self.r/200, 0, 1)
        return

    # def transform_to_radar_coordinates(self):
    #     self.tx, self.ty, self.tz = lidar_to_radar_coordinates(np.asarray([self.tx, self.ty, self.tz]))
    #     self.r, self.az, self.el = toRAzEl(self.tx, self.ty, self.tz)
    #     return

    @property
    def obj_color(self):
        return self.obj.color

    def get_dict(self, add_meta=True):
        father_d = super().get_dict(add_meta=add_meta)
        label_dict = {"tx": float(self.tx),
                      "ty": float(self.ty),
                      "tz": float(self.tz),
                      "range": float(self.r),
                      "az": float(self.az),
                      "el": float(self.el),
                      "width": float(self.width),
                      "height": float(self.height),
                      "length": float(self.length),
                      "heading": float(self.heading),
                      "associate_cluster_id": int(self.associate_cluster)}
        label_dict = {**father_d, **label_dict}
        return label_dict

    def get_close_points(self, cloud_df, z_abs_max=None, mask_only=False):
        (xmin, ymin), (xmax, ymax) = self.get_box_points().min(0)[:2], self.get_box_points().max(0)[:2]
        mask = (cloud_df.x >= xmin) & (cloud_df.x <= xmax) & (cloud_df.y >= ymin) & (cloud_df.y <= ymax)
        if z_abs_max is not None:
            mask &= cloud_df.z.abs() <= z_abs_max
        if mask_only:
            return mask
        return cloud_df.loc[mask].copy()

    def get_pts(self):
        if self.associate_cluster is not None:
            return self.associate_cluster.get_pts()
        else:
            return pd.DataFrame(columns=["x", "y", "z"])

    def get_iou(self, other: 'Label3D'):
        a = self.get_polygon()
        b = other.get_polygon()
        iou = a.intersection(b).area / a.union(b).area
        return iou

    def get_polygon(self):
        if self.polygon is None:
            box_points = self.get_box_points()[:4, :2]
            self.polygon = Polygon(box_points).minimum_rotated_rectangle
        else:
            xy = self.polygon.exterior.xy
            bp = self.get_box_points()
            if np.sum(np.abs(bp[:4, 0] - xy[0][:-1]) + np.abs(bp[:4, 1] - xy[1][:-1])) > 0.1:
                self.polygon = None
                return self.get_polygon()
        return self.polygon

    def get_bev_bbox(self):
        x1 = self.tx - self.width / 2
        y1 = self.ty - self.length / 2
        x2 = self.tx + self.width / 2
        y2 = self.ty + self.length / 2
        return x1, y1, x2, y2

    def get_3d_center(self):
        return self.tx, self.ty, self.tz

    def get_box_points(self):
        return compute_box_3d(np.asarray([self.tx, self.ty, self.tz, self.length, self.height, self.width, self.heading]))

    def get_middles(self, rev=False):
        box_points = self.get_box_points()
        x0, y0, _ = np.mean(box_points[[0, 3] if not rev else [1, 2], :], axis=0)
        x1, y1, _ = np.mean(box_points[[1, 2] if not rev else [0, 3], :], axis=0)
        return (y1-y0, x1-x0)
        # return np.asarray([y1-y0, x1-x0])

    def get_heading_similiarity(self, other):
        # return min(spatial.distance.cosine(v1, v2), spatial.distance.cosine(v1[::-1], v2))
        theta = np.arctan2(*other.get_middles())
        theta2 = np.arctan2(*self.get_middles())

        theta3 = np.arctan2(*other.get_middles(rev=True))
        theta4 = np.arctan2(*self.get_middles())

        a = abs(np.abs(theta) - np.abs(theta2))
        b = abs(np.abs(theta3) - np.abs(theta4))

        return round(min(a, b), 4) # , theta.round(2), theta2.round(2), theta3.round(2), theta4.round(2)

    def get_heading_difference(self, other):
        a = self.heading - other.heading
        return (a + np.pi) % (2 * np.pi) - np.pi

    def __repr__(self):
        # print(type(self.tx))
        return f"{self.cls} | ({round(self.tx, 2)}, {round(self.ty, 2)}, {round(self.tz, 2)}) " \
               f"w={round(self.width, 2)}, l={round(self.length, 2)}, h={round(self.height, 2)}," \
               f" heading={round(self.heading, 2)} | score={str(round(self.score, 2))} | annotate={str(self.annotate)}"
        # return self.cls + ": [tx=" + str(round(self.tx, 2)) + ", ty=" + str(round(self.ty, 2)) + \
        #        ", tz=" + str(round(self.tz, 2)) + "], w:" + str(round(self.width, 2)) + " l:" + str(round(self.length, 2)) + \
        #        ", r=" + str(round(self.r, 2)) + " az=" + str(round(self.az, 2))  + " score=" + str(self.score)

    def plot(self, ax, radar=False, lidar=False, c=None, s=15, alpha=1, bound="circle", annotation_font_size=22, line_width=3, ann=0):
        if radar and lidar:
            print("plot: can't both")
            return
        cluster_center = np.asarray([self.tx, self.ty, self.tz])
        if radar:
            cluster_center = self.engine.project_lidar_to_radar(np.asarray([cluster_center]), filter_fov_flag=True)
            if len(cluster_center) > 0:
                cluster_center = cluster_center[0]
            else:
                print("Label3D: plot, out of FOV" , [self.tx, self.ty, self.tz])
                return
            # todo check chagne filter fov flag
        # if radar:
        #     cluster_center = self.meta_data["projected_3d_center_clustering"]
        # elif lidar:
        #     cluster_center = self.meta_data["3d_center_clustering"]
        # else:
        #     cluster_center = np.asarray([self.tx, self.ty, self.tz])

        lidar_df = pd.DataFrame(data={"x": [cluster_center[0]], "y": [cluster_center[1]], "z": [cluster_center[2]]})

        if c is None and self.cls is not None:
            c = np.repeat([self.obj.color], len(lidar_df), axis=0).astype(np.float32)/255
        elif c is None:
            c = "white"

        # plot center
        # scat = ax.scatter(lidar_df["x"], lidar_df["y"], c=c, s=s, alpha=alpha)
        patch = None

        if bound == "circle":
            patch = plt.Circle((cluster_center[0], cluster_center[1]), 2, color=c if type(c) == str else c[0], fill=False, zorder=0)
        elif bound == "r-bbox":
            box_points = self.get_box_points()
            if radar:
                # box_points = lidar_to_radar_coordinates(box_points)
                box_points = self.engine.project_lidar_to_radar(box_points, filter_fov_flag=False)
            # print(f'c={c}')
            ax.plot(box_points[:, 0], box_points[:, 1], color=c[0], linewidth=line_width, alpha=alpha)

            # Center line
            # print(box_points.shape)
            # ans = np.mean(box_points[[0, 3], :], axis=0)
            # print(ans, ans.shape)
            # print(box_points)
            x0, y0, _ = np.mean(box_points[[0, 3], :3], axis=0)
            x1, y1, _ = np.mean(box_points[[1, 2], :3], axis=0)
            #
            # print(x0, y0, x1, y1)
            # ax.annotate("start", (x0, y0), color="green", fontsize=16)
            # ax.annotate("end", (x1, y1), color="red", fontsize=16)

            # for p_idx, point in enumerate(box_points):
            #     ax.annotate(p_idx, (point[0], point[1]), color="green", fontsize=16)

            # print(type(box_points[0, 0]))
            if ann != 0:
                anno = f"{round(self.score, 2)}"
                ax.annotate(anno, (cluster_center[0], cluster_center[1]), color="white", fontsize=ann, annotation_clip=True)

            if annotation_font_size != 0:
                ax.annotate(self.annotate, (box_points[0, 0], box_points[0, 1]), color=c[0], fontsize=annotation_font_size)

        elif bound == "bbox":
            x1, y1, x2, y2 = self.get_bev_bbox()
            patch = plt.Rectangle((x1, y1), x2-x1, y2-y1, linewidth=2, edgecolor=c if type(c) == str else c[0], facecolor='none')
            # print(self.annotate)
            ax.annotate("bla", (x1, y2), color=c, fontsize=22)

        if patch:
            ax.add_patch(patch)
        # if "associate_cluster_id" in self.meta_data:
        #     ax.annotate(str(self.meta_data["associate_cluster_id"]), (cluster_center[0], cluster_center[1]), color=cmap_clusters(int(self.meta_data["associate_cluster_id"])), fontsize=12)
