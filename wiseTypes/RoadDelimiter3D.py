from tqdm import tqdm
import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd
import time
import scipy
from DataPipeline.shared_utils import toRAzEl


class RoadDelimiter3D(object):
    def __init__(self):
        self.splines = []
        self.extrapolated_points = pd.DataFrame()
        return

    def add_spline(self, spline, u):
        self.splines.append((spline, u))

    def extrapolate(self, extrapolation_factor=2, az_bin_deg=1):
        for spline_idx, (spline, u) in enumerate(self.splines):
            u = np.linspace(0, 1, u.shape[0] * extrapolation_factor)
            interp_x, interp_y, interp_z = scipy.interpolate.splev(u, spline)
            cur_spline_df = pd.DataFrame(data={"x": interp_x, "y": interp_y, "z": interp_z, "spline_idx": spline_idx})
            self.extrapolated_points = self.extrapolated_points.append(cur_spline_df)
            # plt.plot(interp_x, interp_y, '-')

        self.extrapolated_points = self.extrapolated_points.reset_index(drop=True)
        self.extrapolated_points["r"], self.extrapolated_points["Az"], self.extrapolated_points["El"] = toRAzEl(x=self.extrapolated_points["x"], y=self.extrapolated_points["y"],
                                                                                                                z=self.extrapolated_points["z"])
        self.extrapolated_points["Az_index"] = np.digitize(self.extrapolated_points["Az"], bins=np.linspace(-90, 90, np.int(180 / az_bin_deg)))
        display_df = self.extrapolated_points.iloc[self.extrapolated_points.groupby("Az_index")["r"].nsmallest(1).index.get_level_values(1)]
        display_df = display_df.reset_index(drop=True)
        return display_df

    def plot(self):
        return
