from enum import Enum


class DebugTypes(Enum):
    delta_cluster = 1
    pts_from_bb = 2
    predicted_cluster_from_bb = 3


class MetaDataTypes(Enum):
    associate_cluster_id = 1
    associate_cluster_distance = 2
    projected_3d_center_clustering = 3
    bb_com = 4
    bb_median_range_to_obj = 5
    sub_cluster_id = 6


class EvaluationDataTypes(Enum):
    lidar_clusters = 1
    delta_clusters = 2


class EnvelopeTypes(Enum):
    RNG_MIN = 1
    RNG_MAX = 2
    AZ_STEER_MIN_DEG = 3
    AZ_STEER_MAX_DEG = 4
    EL_STEER_MIN_DEG = 5
    EL_STEER_MAX_DEG = 6
    FIRST_HIT = 7
    CORRIDOR = 8
    ENVELOPE_ACTIVE = 9
