import numpy as np

from DataPipeline.DNNs.wrappers.exceptions import NotValidClass


class Car:
    name = "car"
    cls = name
    color = (0, 255, 0)
    unified_idx = 1
    min_length, max_length = 2.8, 5
    min_width, max_width = 1.3, 2.2
    width, length, height = 1.8, 4, 1.8
    desired_l_w_ratio = 2.35
    m_max_cluster_diameter = 8


class Vehicle:
    name = "vehicle"
    cls = name
    color = (0, 255, 255)
    unified_idx = 1
    min_length, max_length = 2.8, 5
    min_width, max_width = 1.3, 2.2
    width, length, height = 1.8, 4, 1.8
    desired_l_w_ratio = 2.35
    m_max_cluster_diameter = 8


class Pedestrian:
    name = "pedestrian"
    cls = name
    color = (255, 0, 0)
    unified_idx = 2
    min_length, max_length = 0.2, 0.4
    min_width, max_width = 0.2, 0.6
    width, length, height = 0.5, 0.3, 1.8
    desired_l_w_ratio = 1
    m_max_cluster_diameter = 3


class Truck:
    name = "truck"
    cls = name
    color = (255, 20, 147)
    unified_idx = 3
    min_length, max_length = 5, 12
    min_width, max_width = 2, 2.8
    width, length, height = 2.4, 7, 2.5
    desired_l_w_ratio = 3
    m_max_cluster_diameter = 25


class Bus:
    name = "bus"
    cls = name
    color = (250, 170, 30)
    unified_idx = 4
    min_length, max_length = 8, 14
    min_width, max_width = 2, 2.8
    width, length, height = 2.55, 10, 3
    desired_l_w_ratio = 4
    m_max_cluster_diameter = 20


class LargeVehicle:
    name = "large_vehicle"
    cls = name
    color = (255, 20, 147)
    unified_idx = 4
    min_length, max_length = 8, 14
    min_width, max_width = 2, 2.8
    width, length, height = 2.55, 10, 3
    desired_l_w_ratio = 4
    m_max_cluster_diameter = 20


class Motorcycle:
    name = "motorcycle"
    cls = name
    color = (50, 205, 50)
    unified_idx = 5
    min_length, max_length = 1.2, 1.7
    min_width, max_width = 0.6, 1
    width, length, height = 0.8, 1.3, 1.1
    desired_l_w_ratio = 1.6
    m_max_cluster_diameter = 5


class Bicycle:
    name = "bicycle"
    cls = name
    color = (255, 204, 0)
    unified_idx = 6
    min_length, max_length = 1.2, 1.7
    min_width, max_width = 0.6, 1
    width, length, height = 0.8, 1.3, 1.1
    desired_l_w_ratio = 1.6
    m_max_cluster_diameter = 5


class Rider:
    name = "rider"
    color = (190, 153, 153)
    unified_idx = 7
    min_length, max_length = 1.2, 1.7
    min_width, max_width = 0.6, 1
    width, length, height = 0.8, 1.3, 1.1
    desired_l_w_ratio = 1.6
    m_max_cluster_diameter = 3


class Cyclist:
    name = "cyclist"
    color = (255, 102, 0)
    unified_idx = 7
    min_length, max_length = 1.2, 1.7
    min_width, max_width = 0.6, 1
    width, length, height = 0.8, 1.3, 1.1
    desired_l_w_ratio = 1.6
    m_max_cluster_diameter = 3


class Motorbiker:
    name = "motorbiker"
    color = (0, 255, 0)
    unified_idx = 7
    min_length, max_length = 1.2, 1.7
    min_width, max_width = 0.6, 1
    width, length, height = 0.8, 1.3, 1.1
    desired_l_w_ratio = 1.6
    m_max_cluster_diameter = 3


class Road:
    name = "road"
    cls = name
    color = (255, 243, 128)
    unified_idx = 8


class Sidewalk:
    name = "sidewalk"
    cls = name
    color = (244, 35, 232)
    unified_idx = 9


class Buildings:
    name = "buildings"
    cls = name
    color = (211,211,211)
    unified_idx = 10


class Wall:
    name = "wall"
    cls = name
    color = (222,184,135)
    unified_idx = 11


class Pole:
    name = "pole"
    color = (0, 0, 70)
    unified_idx = 12


class TrafficLight:
    name = "traffic-light"
    color = (0, 60, 100)
    unified_idx = 13


class TrafficSign:
    name = "traffic-sign"
    color = (0, 80, 100)
    unified_idx = 14


class Terrain:
    name = "terrain"
    cls = name
    color = (216, 0, 155)
    unified_idx = 15


class Sky:
    name = "sky"
    color = (70, 70, 70)
    unified_idx = 16


class Fence:
    name = "fence"
    cls = name
    color = (0, 0, 142)
    unified_idx = 17


class Train:
    name = "Train"
    color = (107, 142, 35)
    unified_idx = 18


class Vegetation:
    name = "vegetation"
    cls = name
    color = (0, 0, 230)
    unified_idx = 19


class Background:
    name = "background"
    color = (0, 0, 0)
    unified_idx = 0


class Unclassified:
    name = "unclassified"
    color = (0, 0, 0)
    unified_idx = -1
    min_length, max_length = 0,0
    min_width, max_width = 0,0
    width, length, height = 0,0,0
    desired_l_w_ratio = 0
    m_max_cluster_diameter = 0

class WiseClasses:
    def __init__(self):
        self.classes_map = {}
        self.skipped_classes = {"traffic light": True, "backpack": True, "stop sign": True, "handbag":True, "kite": True, "bird":True,
                                "bench": True, "parking meter": True, "horse": True, "fire hydrant":True, "chair": True, "umbrella": True,
                                "skateboard": True}

    def get_obj_by_name(self, obj_name, verbose=False):
        if obj_name in self.classes_map:
            return self.classes_map[obj_name]
        elif obj_name in self.skipped_classes:
            # print("WiseClasses: skipped class")
            return Unclassified()
        else:
            if verbose:
                print("class", obj_name, "not in map - Aborting!")
            return Unclassified()

    def get_color_by_idx(self, unified_idx):
        for cls_name, cls_obj in self.classes_map.items():
            if cls_obj.unified_idx == unified_idx:
                return cls_obj.color
        print("get_color_by_idx: No Class Specified", unified_idx)
        return np.array([0, 0, 0])

    def get_obj_by_idx(self, unified_idx):
        for cls_name, cls_obj in self.classes_map.items():
            if cls_obj.unified_idx == unified_idx:
                return cls_obj
        print("get_obj_by_idx: No class specified", unified_idx)
        return Background()


class WiseClassesObjectDetection(WiseClasses):
    def __init__(self):
        super().__init__()
        self.classes_map = {"vehicle": Car(), "utilityvehicle": Vehicle(), "car": Car(),
                            "vansuv": LargeVehicle(), "truck": LargeVehicle(), "trailer": LargeVehicle(), "bus": LargeVehicle(),
                            "motorbike": Motorcycle(), "motorcycle": Motorcycle(),
                            "cyclist": Cyclist(),
                            "bicycle": Bicycle(),
                            "motorbiker": Motorbiker(),
                            "pedestrian": Pedestrian(), "person": Pedestrian(),
                            "unclassified": Unclassified(),
                            "large_vehicle": LargeVehicle()}


class WiseClassesSegmentation(WiseClasses):
    def __init__(self):
        super().__init__()
        self.classes_map = {"vehicle": Car(), "utilityvehicle": Car(),
                            "vansuv": Car(), "car": Car(),
                            "utility-vehicle": Car(),
                            "truck": Truck(), "trailer": Truck(),
                            "bus": Bus(),
                            "cyclist": Rider(), "motorbiker": Rider(),
                            "bicycle": Bicycle(), "motorcycle": Motorcycle(),
                            "pedestrian": Pedestrian(), "person": Pedestrian(),
                            "road": Road(), "sidewalk": Sidewalk(), "buildings": Buildings(),
                            "rider": Rider(), "wall": Wall(), "pole": Pole(), "traffic-sign": TrafficSign(),
                            "traffic-light": TrafficLight(), "terrain": Terrain(),
                            "sky": Sky(), "vegetation": Vegetation(), "train": Train(), "fence": Fence(), "background": Background()}
