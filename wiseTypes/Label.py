from DataPipeline.DNNs.wrappers.exceptions import NotValidClass
from wiseTypes.Cluster import cmap_clusters
from wiseTypes.Types import MetaDataTypes
from wiseTypes.classes_dims import WiseClassesObjectDetection
import numpy as np
import pandas as pd
from wiseTypes.wise_types_utils import get_iou
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
wise_classes = WiseClassesObjectDetection()


class Label:
    def __init__(self):
        self.x1, self.y1, self.x2, self.y2 = None, None, None, None
        self.instance_id = ""
        self.meta_data = {}
        self.debug_data = {}
        self.write_data = {}
        self.cls = "undefined"
        self.associate_cluster = "undefined"
        self.score = 1
        self.annotate = ""
        self.color = (0, 0, 0)
        self.obj = None
        self.uuid = None
        self.trk_id = None
        return

    def set_cls(self, cls_obj):
        self.obj = cls_obj
        self.cls = cls_obj.name
        self.color = cls_obj.color

    def get_bev_bbox(self):
        return self.x1, self.y1, self.x2, self.y2

    def get_dict(self, add_meta=True):
        label_dict = {"bbox": self.get_coords(as_int=True), "cat": self.cls,
                      "score": float(self.score),
                      # "cx": int(center[0]), "cy": int(center[1]), "w": int(center[2]), "h": int(center[3])
                      }

        center = self.get_center(as_int=True)
        if center is not None:
            label_dict["cx"], label_dict["cy"], label_dict["w"], label_dict["h"] = center
        if add_meta:
            label_dict = {**label_dict, **self.meta_data}

        d = {**label_dict, **self.write_data}
        for k, v in d.items():
            if "float" in str(type(v)):
                d[k] = float(v)
            if "int" in str(type(v)):
                d[k] = int(v)
        return d

    def get_id(self):
        if not self.instance_id:
            return hash((self.x1, self.y1, self.x2, self.y2))
        return self.instance_id

    def get_coords(self, as_int=True):
        if (self.x1 is None) or (self.x2 is None) or (self.y1 is None) or (self.y2 is None):
            return None

        if as_int:
            return int(self.x1), int(self.y1), int(self.x2), int(self.y2)
        return self.x1, self.y1, self.x2, self.y2

    def get_score(self):
        return self.score

    def set_center(self, cx, cy, w, h):
        self.x1 = max(cx - w, 0)
        self.y1 = max(cy - h, 0)
        self.x2 = cx + w
        self.y2 = cy + h
        return True

    def get_center(self, as_int=True):
        if (self.x1 is None) or (self.x2 is None) or (self.y1 is None) or (self.y2 is None):
            return None
        cx = (self.x2 + self.x1) / 2
        cy = (self.y1 + self.y2) / 2
        w = (self.x2 - self.x1) / 2
        h = (self.y2 - self.y1) / 2
        ret = np.array([cx, cy, w, h])
        if as_int:
            ret = ret.astype(int)
        return ret

    def get_iou(self, other):
        return get_iou(a=[self.x1, self.y1, self.x2, self.y2], b=other.get_bev_bbox())

    @staticmethod
    def get_2d_iou(o1, o2):
        return get_iou(a=o1.get_bev_bbox(), b=o2.get_bev_bbox())

    @staticmethod
    def get_legend_patches(labels_lst):
        names_set = set()
        cls_set = set()
        patches = []
        for label in labels_lst:
            if label.cls in names_set:
                continue
            names_set.add(label.cls)
            cls_set.add(label)

        for label in sorted(list(cls_set), key=lambda x:x.cls):
            patches.append(mpatches.Patch(color=np.asarray(label.color)/255, label=label.cls))
        return patches


