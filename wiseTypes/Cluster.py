import numpy as np
import pandas as pd
from scipy.spatial import distance
import matplotlib.pyplot as plt
from matplotlib import gridspec, colors, patches
from sklearn.cluster import DBSCAN

from wiseTypes.classes_dims import Unclassified
from wiseTypes.wise_types_utils import get_iou


def get_cmap(num_colors, name='hsv'):
    '''Returns a function that maps each index in 0, 1, ..., n-1 to a distinct
    RGB color; the keyword argument name must be a standard mpl colormap name.'''
    return plt.cm.get_cmap(name, num_colors)

num_colors = 30
cmap_clusters = get_cmap(num_colors)


class ZeroPowerException(Exception):
    pass


class Cluster:
    def __init__(self, cluster_df, cluster_id="", power_key="power_im", force_leaf=False):
        self.valid = False
        self.cluster_df = cluster_df.reset_index(drop=True)

        # if force_leaf or "sub_cluster" not in self.cluster_df.keys():
        #     self.cluster_df["sub_cluster"] = -1
        #     self.sub_clusters = None
        # else:
        #     self.sub_clusters = {}
        #     for sub_cluster_idx in self.cluster_df["sub_cluster"].unique():
        #         self.sub_clusters[sub_cluster_idx] = Cluster(force_leaf=True, cluster_df=self.cluster_df[self.cluster_df["sub_cluster"] == sub_cluster_idx].reset_index(drop=True), cluster_id=sub_cluster_idx)

        if len(cluster_df) == 0:
            return
        self.power_key = power_key
        if self.power_key is None:
            self.weights = None
        else:
            self.weights = self.cluster_df[self.power_key] - self.cluster_df["noise"]
            self.cluster_df["total_power"] = self.weights
            if self.cluster_df["total_power"].sum() == 0:
                self.weights = None
            self.cluster_df = self.cluster_df.dropna(subset=['total_power'])
            if len(self.cluster_df) == 0:
                return

        self.tx = np.average(self.cluster_df["x"], weights=self.weights)
        self.ty = np.average(self.cluster_df["y"], weights=self.weights)
        self.tz = np.average(self.cluster_df["z"], weights=self.weights)
        self.cluster_id = cluster_id
        self.max_diameter = np.max(
            distance.cdist(self.cluster_df[["x", "y", "z"]], self.cluster_df[["x", "y", "z"]], 'euclidean'))
        self.associate_class = Unclassified()
        self.cls = self.associate_class.name

        self.valid = True
        # Cluster speed
        self.cluster_color = cmap_clusters(int(self.get_vel(as_speed=True)) % num_colors)[:3]

        if self.cluster_id:
            self.cluster_color = cmap_clusters(self.cluster_id % num_colors)[:3]
        else:
            self.cluster_color = (0, 0, 1)

    def is_valid(self):
        return self.valid

    def __len__(self):
        return len(self.cluster_df)

    @staticmethod
    def write_clusters(delta_clusters, path):
        dfs = []
        for k, v in delta_clusters.items():
            dfs.append(v.cluster_df)
        df = pd.concat(dfs)
        df.drop(df.filter(regex="Unname"), axis=1, inplace=True)
        df.reset_index(drop=True).to_csv(path)

    def get_bb_intersected_points(self, other):
        bb = other.get_bounding_box()
        ret_points = self.cluster_df[((self.cluster_df["x"] >= bb[0]) & (self.cluster_df["x"] <= bb[2])) & (
                    (self.cluster_df["y"] >= bb[1]) & (self.cluster_df["y"] <= bb[3]))]
        return Cluster(cluster_df=ret_points, cluster_id=self.cluster_id)

    def get_sub_clusters(self, eps=0.6, min_samples=3, force=False):
        if self.sub_clusters is not None and not force:
            return self.sub_clusters
        else:
            self.sub_clusters = {}
        coords_keys = ["x", "y"]
        db = DBSCAN(eps=eps, min_samples=min_samples)
        X = self.cluster_df[coords_keys]
        db_res = db.fit(X)
        labels = db_res.labels_
        core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
        core_samples_mask[db.core_sample_indices_] = True
        unique_labels = set(labels)
        cluster_idx = 0
        for _ in unique_labels:
            class_member_mask = (labels == _)
            cluster_df = self.cluster_df[class_member_mask & core_samples_mask]
            if len(cluster_df) == 0:
                continue
            self.cluster_df.loc[class_member_mask & core_samples_mask, "sub_cluster"] = cluster_idx
            self.sub_clusters[cluster_idx] = Cluster(cluster_df=cluster_df, cluster_id=cluster_idx)
            cluster_idx += 1
        return self.sub_clusters

    def get_best_iou(self, lst_clusters):
        ious = np.asarray([get_iou(self.get_bounding_box(), x.get_bounding_box()) for x in lst_clusters])
        return lst_clusters[ious.argmax()].cluster_id, ious.max()

    def get_iou(self, other):
        return get_iou(self.get_bounding_box(), other.get_bounding_box())

    def get_best_distance(self, lst_clusters, geometric=False):
        distances = np.asarray(
            [np.linalg.norm(self.get_3d_center(geometric=geometric) - x.get_3d_center(geometric=geometric)) for x in
             lst_clusters])
        return lst_clusters[distances.argmin()].cluster_id, distances.min()

    def get_best_cross_distance(self, lst_clusters):
        distances = np.asarray(
            [np.linalg.norm(self.get_bounding_box_center() - x.get_bounding_box_center()) for x in lst_clusters])
        return lst_clusters[distances.argmin()].cluster_id, distances.min()

    def get_score(self):
        if self.power_key is None:
            return 1
        return np.average(self.cluster_df[self.power_key] - self.cluster_df["noise"], weights=self.weights)

    def get_3d_center(self, geometric=False):
        if geometric:
            tx = np.average(self.cluster_df["x"])
            ty = np.average(self.cluster_df["y"])
            tz = np.average(self.cluster_df["z"])
            return np.asarray([tx, ty, tz])
        return np.asarray([self.tx, self.ty, self.tz])

    def get_id(self):
        return self.cluster_id

    def get_track_id(self):
        return self.cluster_df.loc[0, "trk_id"]

    def __getitem__(self, idx):
        if idx > 2:
            print("Can't get item")
            return None
        else:
            return self.get_3d_center()[idx]

    def get_pts(self):
        return self.cluster_df.copy()
        # return np.vstack((self.cluster_df["x"], self.cluster_df["y"], self.cluster_df["z"])).T

    def get_bounding_box(self):
        x_min = self.cluster_df["x"].min()
        x_max = self.cluster_df["x"].max()
        y_min = self.cluster_df["y"].min()
        y_max = self.cluster_df["y"].max()
        return np.asarray([x_min, y_min, x_max, y_max])

    def plot_bounding_cross(self, ax, color="r", line_width=3):
        x_min, y_min, x_max, y_max = self.get_bounding_box()
        w = (x_max - x_min)
        h = (y_max - y_min)
        ax.plot([x_min + w / 2, x_min + w / 2], [y_min, y_max], linewidth=line_width, c=color)
        ax.plot([x_min, x_max], [y_min + h / 2, y_min + h / 2], linewidth=line_width, c=color)

    def get_bounding_box_center(self):
        x_min, y_min, x_max, y_max = self.get_bounding_box()
        cx = (x_min + x_max) / 2
        cy = (y_min + y_max) / 2
        return np.asarray([cx, cy])

    def plot_bounding_box(self, ax, color="yellow", line_width=3, annotate=""):
        x_min, y_min, x_max, y_max = self.get_bounding_box()
        rect = patches.Rectangle((x_min, y_min), x_max - x_min, y_max - y_min, linewidth=line_width, edgecolor=color,
                                 facecolor='none')
        ax.add_patch(rect)

        if annotate:
            ax.annotate(str(annotate), (x_min-10, y_max),
                        color=self.cluster_color,
                        fontsize=20)
        return

    def get_vel(self, as_speed=False):
        try:
            x, y, z = self.cluster_df.loc[0, "vx"], self.cluster_df.loc[0, "vy"], self.cluster_df.loc[0, "vz"]
        except Exception as e:
            x, y, z = 0, 0, 0
        if as_speed:
            ret = np.linalg.norm(np.asarray([x, y, z]))
            return 0 if np.isnan(ret) else ret
        else:
            return x, y, z

    def is_static(self, vel_thresh=1):
        vel = self.get_vel()
        return np.linalg.norm(vel) <= vel_thresh

    def __repr__(self):
        return "Cluster id=" + str(self.cluster_id) + " center=" + str(self.get_3d_center())

    def plot(self, ax, color="yellow", scatter_size=3, quiver=True, annotate="", annotate_size=20):
        ax.scatter([self.tx], [self.ty], color=color, s=scatter_size * 2)
        if quiver and "vx" in self.cluster_df.keys():
            ax.quiver([self.tx], [self.ty], [self.get_vel()[0]], [self.get_vel()[1]], color=color)
        ax.scatter(self.cluster_df['x'], self.cluster_df['y'], color=self.cluster_color, s=scatter_size)
        cluster_center = self.get_3d_center()
        if annotate:
            ax.annotate("%s%s" % (str(self.cluster_id), annotate), (cluster_center[0], cluster_center[1]), color=self.cluster_color, fontsize=annotate_size)

    def plot_on_im(self, ax, im, s=2):
        norms = self.cluster_df["r"]
        cNorm = colors.Normalize(vmin=np.min(norms), vmax=np.max(norms))
        scat = ax.scatter(x=np.clip(np.round(self.cluster_df["x_pixel"]), 0, im.shape[1] - 1).astype(np.int),
                          y=np.clip(np.round(self.cluster_df["y_pixel"]), 0, im.shape[0] - 1).astype(np.int), s=s,
                          color=self.cluster_color)
