import sys

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QHBoxLayout, QMainWindow, QScrollArea
from PyQt5.QtGui import QIcon
from Calibrator.gui_classes import ParametersWidget, Params, MplCanvas, Data
from Calibrator.gui_classes import LIDAR_PC, RADAR_FFT, RECTIFY_IMG

class App(QMainWindow):
    def __init__(self):
        super().__init__()
        self.title = 'Wisense Calibrator'
        self.left = 10
        self.top = 10
        self.width = 1280
        self.height = 720
        self.main_widget = MainWidget(self)
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.setCentralWidget(self.main_widget)
        self.show()


class MainWidget(QWidget):
    def __init__(self, parent):
        super(QWidget, self).__init__(parent)
        self.parent = parent
        self.layout = QHBoxLayout(self)
        self.params = Params(self)
        self.data = Data(suffix=0)

        # self.display_widget = DisplayWidget(parent=self)
        self.display_widget = MplCanvas(parent=self)
        self.scroll_area = QScrollArea(parent=self)
        self.parameters_widget = ParametersWidget(parent=self.scroll_area, params=self.params, data=self.data)
        self.parameters_widget.setMaximumWidth(500)
        self.scroll_area.setMinimumWidth(550)
        self.scroll_area.setWidget(self.parameters_widget)
        self.scroll_area.setWidgetResizable(True)
        self.layout.addWidget(self.display_widget)
        self.layout.addWidget(self.scroll_area)
        self.setLayout(self.layout)
        # self.show()

        if sys.argv[1] == "d":
            # print("debug mode")
            # self.params.params_values["Top-View"].setCheckState(True)
            # self.params.params_values["Top-View"].setChecked(True)
            # self.params.params_values["Radar-PC"].setChecked(True)
            self.params.params_values[LIDAR_PC].setChecked(True)
            self.params.params_values[RECTIFY_IMG].setChecked(True)
            self.params.params_values[RADAR_FFT].setChecked(True)
            self.parameters_widget.load_config_from_path("/home/amper/AI/Harel/AI_Infrastructure/Calibrator/cali_200909")
            # self.params.params_values["Rng-Az"].setChecked(True)
            # self.params.params_values["Zero-Padding"].set_val(2)
            # self.display_widget.update_fig()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
