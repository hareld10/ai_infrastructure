import cv2
import os
import numpy as np
import json
import matplotlib.pyplot as plt
from matplotlib import colors, gridspec
from glob import glob
from Calibrator.projection import Camera, toRAzEl, toXYZ
from tqdm import tqdm
from DataPipeline.settings import Context




def db(array):
    return 10 * np.log10(array)


params = "/home/amper/workspace/wise_sdk/SDK/python/camera/cam60_calibration_b0.json"

if not os.path.exists(params):
    print(params)
else:
    print("Exists")

K, D = loadParams(params)
C = Camera(K=K, D=D)
THRESH = 25
base_data = "//home/amper/AI/Harel/AI_Infrastructure/Calibrator/data/from_faraday/"



for i in tqdm(np.random.permutation(glob(base_data + "/camera_data/camera_rgb/*.png"))):
    gs = gridspec.GridSpec(ncols=3, nrows=1)
    fig = plt.figure(figsize=(10, 10))
    im_ax = fig.add_subplot(gs[0, 0])
    distorted_im_ax = fig.add_subplot(gs[0, 1])
    pc_ax = fig.add_subplot(gs[0, 2], projection='3d')

    ts = os.path.split(i)[-1].split(".")[0].split("_")[-1]
    im = cv2.imread(base_data + "/camera_data/camera_rgb/camera_rgb_" + ts + ".png")[..., ::-1]
    ffta = np.load((base_data + "/fft_4d/fft_4d_" + ts + ".npy"))
    distorted_im_ax.imshow(im)
    im_ax.imshow(undistort(im))

    ffta = db(np.max(np.abs(ffta[..., 0] + ffta[..., 1]), 0))

    indices = np.argwhere(ffta > THRESH)

    radar_param = base_data + "../radarParam.npy"
    radar_param = np.load(radar_param, allow_pickle=True).item()

    cur_points = np.zeros((len(indices), 3), dtype=np.float32)  # Range Az El

    az_vec = np.rad2deg(np.arcsin(np.linspace(np.min(radar_param["az_vec"]), np.max(radar_param["az_vec"]), ffta.shape[2])))
    el_vec = np.rad2deg(np.arcsin(np.linspace(np.min(radar_param["el_vec"]), np.max(radar_param["el_vec"]), ffta.shape[1])))
    cur_points[:, 0] = radar_param["rng_vec"][indices[:, 0]]
    cur_points[:, 1] = az_vec[indices[:, 2]]
    cur_points[:, 2] = el_vec[indices[:, 1]]

    # Remove First 2 Meters
    cur_points = cur_points[cur_points[:, 0] > 2]

    radar_points_projected = C.Radar2Image(r=cur_points[:, 0], az=cur_points[:, 1], el=cur_points[:, 2], D=D)
    x, y, z = toXYZ(r=cur_points[:, 0], Az=cur_points[:, 1], El=cur_points[:, 2])
    pc_ax.scatter(x, y, z, c="blue")
    pc_ax.view_init(elev=90, azim=270)

    cNorm = colors.Normalize(vmin=0, vmax=80)
    norms = cur_points[:, 0]
    scat = im_ax.scatter(x=np.round(radar_points_projected[0, :]).astype(np.int),
                         y=np.round(radar_points_projected[1, :]).astype(np.int), s=1, c=norms,
                         cmap="jet",
                         norm=cNorm)

    plt.suptitle(str(ts) + " prjected_points " + str(radar_points_projected.shape[1]) + "\n" +
                 "Total Point CLoud Points " + str(cur_points.shape[0]))
    save_path = base_data + "/results/"
    os.makedirs(save_path, exist_ok=True)
    plt.savefig(save_path + str(ts) + " CFAR" + str(THRESH) + ".png")
    plt.show()
    break
    plt.close()
