import argparse
import json
import pathlib
import pickle
import sys
import time

module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, module_path)
module_path = str(pathlib.Path(__file__).parent.absolute())
sys.path.insert(0, module_path + "/../")
sys.path.insert(0, module_path + "/../DataPipeline/")
# from DataPipeline.DNNs.wrappers.efficientDetWrapper import EfficientDetWrapper
from DataPipeline.DNNs.wrappers.yoloWrapper import YoloWrapper
from DataPipeline.DNNs.wrappers.exceptions import NotValidClass
from Evaluation.classes import EvalResults, FrameResults
from Evaluation.EvalMetrices.show_bb import evaluate_frame_centers, evaluate_frame_ious
from wiseTypes.Label2D import Label2D
from matplotlib import gridspec, colors
from tqdm import tqdm
import warnings

warnings.filterwarnings('ignore')
import pandas as pd
import cv2
from sklearn.cluster import KMeans
import os
import numpy as np
from multiprocessing import Pool
import matplotlib.pyplot as plt
from collections import defaultdict
import matplotlib

plt.style.use('dark_background')
font = {'weight': 'bold',
        'size': 16}

from glob import glob
from Calibrator.projection import toRAzEl, Camera
from Calibrator.calibrator_utils import filter_fov_az, cut_by_range, read_label, PrintException, apply_6DOF, rectify, draw_label_on_image, get_mask_from_label, plot_pc_from_csv_to_ax, \
    process_lidar_to_image_on_ax, get_pc_radar_from_csv, plot_pc, fix_lidar_mat, update_map_indices, preprocess_lidar_to_image, get_pts_extractrd_from_pc, get_3d_ax, \
    build_rec_from_cluster_old2, build_bbox, save_label, get_L_fitting, plot_pc_2d
from DataPipeline.settings import Context
from OpenSource.AudiEngine import AudiEngine
from wiseTypes.classes_dims import WiseClassesObjectDetection

# python3 Calibrator/bb_overlay.py --force --pvrcnn --instances
# root@a227451a1850:/workspace/AI/Harel/AI_Infrastructure# python3 Calibrator/bb_overlay.py --force --eval 20 --plot 1 --centers-only --instances


# python3 Calibrator/bb_overlay.py --force --run_all  --plot 2 --no-save --start 1650 --instances --centers-only

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Date pipeline arguments')
    parser.add_argument('--run_all', action='store_true', default=False)
    parser.add_argument('--plot_only', action='store_true', default=False)
    parser.add_argument('--no-save', action='store_true', default=False)
    parser.add_argument('--rev', action='store_true', default=False)
    parser.add_argument('--force', action='store_true', default=False)
    parser.add_argument('--instances', action='store_true', default=False)
    parser.add_argument('--eval', default=-1, type=int)
    parser.add_argument('--centers-only', action='store_true', default=False)
    parser.add_argument('--processes', default=0, type=int)
    parser.add_argument('--plot', default=100, type=int)
    parser.add_argument('--start', default=0, type=int)
    args = parser.parse_args()

try:
    from DataPipeline.DNNs.OpenPCDet.tools.inference_harel import ModelRunner

    model_runner = ModelRunner()
    print("Loaded Model")
except Exception as e:
    model_runner = None


class Config(object):
    MAX_RANGE = 80
    MIN_RANGE = 2
    CORRIDOR_X = 0
    FOV_RADAR = 80
    FFT_MIN_POWER = 15
    DELTA_MIN_POWER = 30
    MIN_DOP_DELTA = 2
    MIN_DOP_4DFFT = MIN_DOP_DELTA
    CAMERA_FOV_AZ = 70
    CAMERA_FOV_EL = 40

    PROJECTION_POINT_SIZE = 3
    RADAR_3D_PLOT_POINT_SIZE = 9

    LIDAR_ELEVATION_LEVEL = -1.75
    LIDAR_MAX_RANGE = 200
    # params = "/home/amper/workspace/wise_sdk/SDK/python/camera/cam60_calibration_b0.json"
    # fileName = "//home/amper/AI/Harel/AI_Infrastructure/Calibrator/cali_200909"

    NUM_CLUSTERS = 5
    MIN_PINT_PER_CLUSTER = 2

    context = Context(args=None, index=0, create_dirs=False)
    K = context.K
    D = context.D
    camera_obj = Camera(K=context.K, D=context.D)

    # Eval
    iou_thresholds = [0.1, 0.3, 0.5]
    config_name = "base_config"
    wise_classes = WiseClassesObjectDetection()


class BBDisplay:
    def __init__(self, rect_paths=True, path=None):
        self.path = path
        if path is None:
            # self.path = "/media/amper/data_backup7/76mWF/200902_lidar_drive_highway_W92_M15_RF1/"
            # self.path = "/media/amper/data_backup7/76mWF/200903_lidar_drive_1_W92_M15_RF1/"
            self.path = "/media/amper/data_backup7/76mWF/200831_lidar_drive_1_urban_W92_M15_RF1/"
            # self.path = "/mnt/hdd/new_lidar_drives/drives/200902_lidar_drive_2_W92_M15_RF1/"
            # self.path = "/mnt/hdd/new_lidar_drives/drives/200901_lidar_drive_2_W92_M15_RF1/"
            # self.path = "/mnt/hdd/new_lidar_drives/drives/200903_lidar_drive_2_W92_M15_RF1/"
            # self.path = "/mnt/hdd/new_lidar_drives/200902_single_car_val_W92_M15_RF1/"

        self.context = Context(args=None, index=0, create_dirs=False, base_path=self.path, spatial_calibration_path="//home/amper/AI/Harel/AI_Infrastructure/Calibrator/cali_200909_pitch_fix")
        self.context.set_rect_paths(rect_paths)

        os.makedirs(self.context.camera_to_lidar, exist_ok=True)
        os.makedirs(self.context.camera_to_lidar_images, exist_ok=True)

        self.df = pd.read_csv(self.context.df_path, index_col=None)
        self.df = pd.read_csv(self.path + "/aux/sync_data_radar_lidar.csv", index_col=None)
        self.df["base_path"] = self.path

        self.classes_map = WiseClassesObjectDetection().classes_map

        self.pc_lidar = None
        self.radar_4dfft = None
        self.radar_cuda_pc = None
        self.camera_label_rect = None
        self.im = None
        self.cfar_results = None
        self.d3vd_label = None
        self.debug = False
        self.config = None

    def set_config(self, config):
        self.config = config

    def get_drive_name(self):
        return os.path.split(os.path.split(self.path)[0])[-1]

    def read_data_context(self, idx):
        rgb_path = self.context.camera_rgb + "/camera_rgb_" + str(self.df.loc[idx, 'timestampCamera']) + ".png"
        pc_path = self.context.lidar_points + "/lidar_pc_" + str(self.df.loc[idx, 'timestampLidar'])[:-4] + ".npz"
        lidar_seg_path = self.context.lidar_seg + "/lidar_seg_" + str(self.df.loc[idx, 'timestampLidar'])[:-4] + ".npz"

        radar_4dfft_path = self.context.cuda_4dfft + "/cuda_4dfft_" + str(self.df.loc[idx, "timestampRadar"]) + ".csv"
        radar_delta_pc_path = self.context.cuda_delta + "/cuda_delta_" + str(self.df.loc[idx, "timestampRadar"]) + ".csv"

        # camera_label = self.context.rectified_labels + "/camera_det_EfficientDetD7_" + str(self.df.loc[idx, "timestampCamera"]) + ".json"
        camera_label = self.context.camera_det_ensemble + "/camera_det_ensemble_" + str(self.df.loc[idx, "timestampCamera"]) + ".json"
        d3vd_path = self.context.lidar_3d + "lidar_3d_" + str(self.df.loc[idx, "timestampLidar"])[:-4] + ".json"
        try:
            self.d3vd_label = read_label(d3vd_path)
            self.im = cv2.cvtColor(cv2.imread(rgb_path), cv2.COLOR_BGR2RGB)
            self.pc_lidar = np.load(pc_path)['arr_0']
            self.lidar_seg = np.load(lidar_seg_path)['arr_0']

            mask = np.zeros((self.pc_lidar.shape[0], self.pc_lidar.shape[1] + 1), dtype=np.float32)
            mask[:, :3] = self.pc_lidar
            mask[:, -1] = self.lidar_seg
            self.pc_lidar = mask

            self.pc_lidar = self.pc_lidar[np.linalg.norm(self.pc_lidar, axis=-1) < self.config.LIDAR_MAX_RANGE]

            self.camera_label_rect = read_label(camera_label)
            for label_idx, cur_label in enumerate(self.camera_label_rect):
                self.camera_label_rect[label_idx]["lidar_label_center"] = 0
                self.camera_label_rect[label_idx]["lidar_label_max_iou"] = 0
                self.camera_label_rect[label_idx]["lidar_label_idx_to_plot"] = 0

            # self.radar_4dfft = pd.read_csv(radar_4dfft_path)
            # self.radar_4dfft = self.radar_4dfft.reset_index(drop=True)
            # self.radar_4dfft["total_power"] = db(
            #     np.abs(self.radar_4dfft["value_real"].values * self.radar_4dfft["value_real"].values + self.radar_4dfft["power_im"].values * self.radar_4dfft["power_im"].values))
            # self.radar_4dfft = self.radar_4dfft[(self.radar_4dfft["total_power"] - self.radar_4dfft["noise"]) > config.FFT_MIN_POWER]
            # self.radar_4dfft = self.radar_4dfft[np.abs(self.radar_4dfft["dop"]) > config.MIN_DOP_4DFFT]
            #
            # self.radar_cuda_pc = pd.read_csv(radar_delta_pc_path)
            # self.radar_cuda_pc = self.radar_cuda_pc.reset_index(drop=True)
            # self.radar_cuda_pc["total_power"] = db(
            #     np.abs(self.radar_cuda_pc["value_real"].values * self.radar_cuda_pc["value_real"].values + self.radar_cuda_pc["power_im"].values * self.radar_cuda_pc["power_im"].values))
            # self.radar_cuda_pc = self.radar_cuda_pc[(self.radar_cuda_pc["total_power"] - self.radar_cuda_pc["noise"]) > config.DELTA_MIN_POWER]
            # self.radar_cuda_pc = self.radar_cuda_pc[np.abs(self.radar_cuda_pc["dop"]) > config.MIN_DOP_DELTA]

            # vals = self.radar_4dfft["total_power"].values
            # print("4d", len(self.radar_4dfft), "Max power", max(vals), "Min", min(vals))
            # print("Delta", len(self.radar_cuda_pc), "Max power", max(vals), "Min", min(vals))
        except Exception as e:
            print(e)
            PrintException()
            return False
        return True

    def display_for_video(self, idx):
        while True:
            global_num_bbs = 1
            with open(self.config.fileName, "r") as fp:
                calibration_params = json.load(fp)
            if not self.read_data(idx):
                return
            gs = gridspec.GridSpec(ncols=2, nrows=4)
            fig = plt.figure(figsize=(20, 10))

            self.im = rectify(self.im, K=self.config.K, D=self.config.D)
            proj_im = self.im.copy()

            im_to_plot = draw_label_on_image(frame_data=self.camera_label_rect, im=self.im)
            label_mask = get_mask_from_label(frame_data=self.camera_label_rect, im=self.im)

            im_ax = fig.add_subplot(gs[0, 0])
            im_ax.imshow(im_to_plot)
            im_ax.axis(False)
            im_ax.set_title("4D-FFT cfar: " + str(self.config.FFT_MIN_POWER) + "db, doppler_min: " + str(self.config.MIN_DOP_4DFFT) + "m/s, dt: " + str(
                round((int(self.df.loc[idx, "timestampCamera"]) - int(self.df.loc[idx, "timestampRadar"])) / 10e6, 2)) + " ms" + ", colormap=range")
            #
            im_ax_2 = fig.add_subplot(gs[1, 0])
            im_ax_2.imshow(im_to_plot)
            im_ax_2.axis(False)
            im_ax_2.set_title("DELTA cfar: " + str(self.config.DELTA_MIN_POWER) + "db" + ", doppler_min: " + str(self.config.MIN_DOP_DELTA) + "m/s, dt: " + str(
                round((int(self.df.loc[idx, "timestampCamera"]) - int(self.df.loc[idx, "timestampRadar"])) / 10e6, 2)) + " ms" + ", colormap=range")

            im_ax_3 = fig.add_subplot(gs[2, 0])
            im_ax_3.axis(False)
            im_ax_3.imshow(im_to_plot)
            im_ax_3.set_title("LIDAR dt: " + str(round((int(self.df.loc[idx, "timestampCamera"]) - int(self.df.loc[idx, "timestampLidar"])) / 10e6, 2)) + " ms" + ", colormap=range")

            im_ax_4 = fig.add_subplot(gs[3, 0])
            im_ax_4.axis(False)

            # im_ax.imshow(self.im)
            ax1 = self.get_3d_ax(fig=fig, loc=gs[:, 1])

            """
            2D Plots
            """
            plot_pc_from_csv_to_ax(self.radar_4dfft.copy(), ax=im_ax, mask=label_mask, config=self.config)
            plot_pc_from_csv_to_ax(self.radar_cuda_pc, ax=im_ax_2, mask=label_mask, config=self.config)
            process_lidar_to_image_on_ax(pts=self.pc_lidar, im=self.im, config=self.config, ax=im_ax_3, calibration_params=calibration_params)
            """
            3D Plots
            """
            cur_points_4d = get_pc_radar_from_csv(self.radar_4dfft, config=self.config)
            sc1 = plot_pc(points=cur_points_4d, ax=ax1, config=self.config, s=self.config.RADAR_3D_PLOT_POINT_SIZE, c="red")
            cur_points_delta = get_pc_radar_from_csv(self.radar_cuda_pc, config=self.config)
            sc2 = plot_pc(points=cur_points_delta, ax=ax1, config=self.config, s=self.config.RADAR_3D_PLOT_POINT_SIZE, c="orange")
            lidar_to_display = fix_lidar_mat(self.pc_lidar, config=self.config, calibration_params=calibration_params)
            sc3 = plot_pc(points=lidar_to_display, ax=ax1, config=self.config, s=self.config.RADAR_3D_PLOT_POINT_SIZE // 3, c="lightskyblue", elev=False)

            ax1.legend([sc1, sc2, sc3], ["4dfft", "delta", "lidar"])

            bbs_indices_map = update_map_indices(lidar_label=self.d3vd_label, camera_label_rect=self.camera_label_rect, proj_im=proj_im,
                                                 config=self.config, calibration_params=calibration_params)

            im_ax_4.imshow(proj_im)
            title = "Sensors Projection\n"
            title += os.path.split(os.path.split(self.path)[0])[-1]
            title += " - " + str(self.df.loc[idx, "timestampRadar"]) + "\n"
            plt.suptitle(title)
            plt.tight_layout(pad=1)
            return str(self.df.loc[idx, "timestampRadar"])
            # plt.show()

    def get_clusterd_points(self, points_in_lidar_space):
        num_points = points_in_lidar_space.shape[0]
        if num_points > 450:
            self.config.NUM_CLUSTERS = 10
        elif num_points > 350:
            self.config.NUM_CLUSTERS = 9
        elif num_points > 250:
            self.config.NUM_CLUSTERS = 8
        elif num_points > 200:
            self.config.NUM_CLUSTERS = 7
        elif num_points < 100:
            self.config.NUM_CLUSTERS = 5
        elif num_points < 50:
            self.config.NUM_CLUSTERS = 4
        else:
            self.config.NUM_CLUSTERS = 6
        kmeans = KMeans(n_clusters=self.config.NUM_CLUSTERS, random_state=0).fit(points_in_lidar_space)
        preds = kmeans.update(points_in_lidar_space)
        unique, counts = np.unique(preds, return_counts=True)
        main_cluster_idx = unique[counts.argmax()]
        cur_points = points_in_lidar_space[preds == main_cluster_idx]

        def myfn(x):
            return np.linalg.norm(x - kmeans.cluster_centers_[main_cluster_idx], axis=-1)

        DISTANCE_FROM_CENTER = 4
        cur_points = cur_points[np.linalg.norm(cur_points[:, :3] - kmeans.cluster_centers_[main_cluster_idx], axis=-1) < DISTANCE_FROM_CENTER]
        if len(cur_points) > 100:
            predicate = myfn(cur_points)
            order = np.argsort(predicate)
            cur_points = cur_points[order][:int(len(cur_points) * 0.9)]

        return cur_points, np.mean(cur_points[:, :3], axis=0)

    def plot(self, im_to_plot, imgfov_pc_pixel, imgfov_pc_velo, lidar_centers_to_display, idx, instances=False, center_only=False):
        plt.close()
        gs = gridspec.GridSpec(ncols=2, nrows=2)
        fig = plt.figure(figsize=(30, 15))
        plt.tight_layout(pad=2)

        orig_im_ax = fig.add_subplot(gs[0, 0])
        orig_im_ax.axis(False)
        orig_im_ax.imshow(im_to_plot)
        im_ax = fig.add_subplot(gs[1, 0])
        im_ax.axis(False)
        ax_3d = get_3d_ax(fig=fig, loc=gs[:, 1:],  x_left=-45, x_right=45, ax3d=False)  # x_left=0, x_right=100, y_left=30, y_right=-30)

        process_lidar_to_image_on_ax(imgfov_pc_pixel=imgfov_pc_pixel, imgfov_pc_velo=imgfov_pc_velo, im=im_to_plot, config=self.config, ax=im_ax)
        lidar_to_display = fix_lidar_mat(self.pc_lidar, config=self.config, calibration_params=self.context.spatial_calibration_params)
        sc3 = plot_pc_2d(points=lidar_to_display, ax=ax_3d, config=self.config, s=self.config.RADAR_3D_PLOT_POINT_SIZE // 5, c="lightskyblue", elev=False, alpha=0.7)
        im_ax.imshow(im_to_plot)

        legend_flag = False
        cluster_center_legend_flag = False
        for p_idx, label in enumerate(self.camera_label_rect):
            if "3d_center_clustering" not in label:
                continue
            center_clusters = label["3d_center_clustering"]
            center_clusters = fix_lidar_mat(np.array([center_clusters]), config=self.config, calibration_params=self.context.spatial_calibration_params)
            plot_pc_2d(points=center_clusters, ax=ax_3d, config=self.config, s=self.config.RADAR_3D_PLOT_POINT_SIZE * 4, c="yellow", elev=False, alpha=1,
                    label="cluster-center" if not cluster_center_legend_flag else "")
            cluster_center_legend_flag = True

            if center_only: continue

            box_points = build_bbox(center=center_clusters, heading=label["3d_heading"], l=label["3d_dimensions"]["width"],
                                    w=label["3d_dimensions"]["length"],
                                    h=label["3d_dimensions"]["height"])
            ax_3d.plot(box_points[:, 0], box_points[:, 1], box_points[:, 2], color="orange", label="estimated-dimensions" if legend_flag == 0 else "")

            if "pvrcnn" in label:
                pred_boxes = label["pvrcnn"]["pred_boxes"].cpu().numpy()
                for p_box in pred_boxes:
                    print("p_box", p_box.shape)
                    if p_box.shape[0] == 0:
                        continue
                    p_box[:3] = fix_lidar_mat(np.array([p_box[:3]]), config=self.config, calibration_params=self.context.spatial_calibration_params)
                    box_points = build_bbox(center=p_box[:3], heading=p_box[6], l=p_box[4],
                                            w=p_box[3],
                                            h=p_box[5])

                    ax_3d.plot(box_points[:, 0], box_points[:, 1], box_points[:, 2], color="blue", label="pv-rcnn" if legend_flag == 0 else "")
            legend_flag = True

        ax_3d.legend(bbox_to_anchor=(0.5, -0.2), loc='lower center')
        plt.tight_layout(pad=2)
        plt.suptitle(drive + " | " + str(self.df.loc[idx, "timestampRadar"]) +"\n\n")
        plt.savefig(self.context.camera_to_lidar_images + "/camera_to_lidar_" + str(self.df.loc[idx, "timestampLidar"])[:-4] + ".png")

        plt.close()
        gs = gridspec.GridSpec(ncols=3, nrows=2)
        if instances:
            for label in self.camera_label_rect:
                if "3d_center_clustering" not in label:
                    continue
                p_idx = label["instance_idx"]
                x1, y1, x2, y2 = label["bbox"]
                cluster_center = label["3d_center_clustering"]
                pts_extracted_from_pc = get_pts_extractrd_from_pc(x1, y1, x2, y2, imgfov_pc_pixel, imgfov_pc_velo)

                ax_3d = get_3d_ax(fig=fig, loc=gs[:, 1], x_left=-45, x_right=45, ax3d=True)
                # ax_3d.cla()
                ax_3d.set_xlabel('X(m)', labelpad=1)
                ax_3d.set_ylabel('Y(m)', labelpad=1)
                ax_3d.set_zlabel('Z(m)', labelpad=1)

                # Only to plot current cluster
                points_in_lidar_space = pts_extracted_from_pc[:, 4:7]
                cur_points, _ = self.get_clusterd_points(points_in_lidar_space)

                plot_pc(points=cur_points[:, :3], ax=ax_3d, config=self.config, s=self.config.RADAR_3D_PLOT_POINT_SIZE, c="orange", elev=False, alpha=0.8, label="cluster-points")
                plot_pc(points=label["pvrcnn-input"] if "pvrcnn-input" in label else None, ax=ax_3d, config=self.config, s=self.config.RADAR_3D_PLOT_POINT_SIZE, c="cyan", elev=False, alpha=0.8,
                        label="pv-rcnn-input")
                plot_pc(points=np.array([np.mean(cur_points[:, :3], axis=0)]), ax=ax_3d, config=self.config, s=self.config.RADAR_3D_PLOT_POINT_SIZE / 4, c="red", elev=False, alpha=0.4)
                plot_pc(points=np.array([cluster_center]), ax=ax_3d, config=self.config, s=self.config.RADAR_3D_PLOT_POINT_SIZE * 4, c="yellow", elev=False, alpha=0.5)

                hist_ax = fig.add_subplot(gs[0, 2])
                hist_ax.cla()
                hist_ax.hist(cur_points[:, 0], density=True)
                hist_ax.set_title("x-axis")

                hist_ax = fig.add_subplot(gs[1, 2])
                hist_ax.cla()
                hist_ax.hist(cur_points[:, 1], density=True)
                hist_ax.set_title("y-axis")
                # plot_pc(points=pts_extracted_from_pc[:, :3], ax=ax_3d, config=config, s=config.RADAR_3D_PLOT_POINT_SIZE / 4, c="red", elev=False, alpha=0.4)
                # plot_pc(points=kmeans.cluster_centers_, ax=ax_3d, config=config, s=config.RADAR_3D_PLOT_POINT_SIZE * 4, c="pink", elev=False, alpha=1)

                # box_points = build_bbox(center=cluster_center, heading=label["3d_heading"], l=label["3d_dimensions"]["length"],
                #                         w=label["3d_dimensions"]["width"],
                #                         h=label["3d_dimensions"]["height"])
                if not center_only:
                    title = "heading: " + str(label["3d_heading"]) + ", l-w: " + str(label["3d_dimensions"]["length"]) + ", " + str(label["3d_dimensions"]["width"]) + "\n" \
                            + " #p " + str(cur_points.shape) + "\n" + str(label["text"])

                    title += "\n l-w-ratio: " + str(label["ret_dict"]["l_w_ratio"])
                    ax_3d.set_title(title)
                    # ax_3d.plot(box_points[:, 0], box_points[:, 1], box_points[:, 2])
                    ax_3d.quiver(cluster_center[0], cluster_center[1], cluster_center[2], np.cos(np.deg2rad(label["3d_heading"])), np.sin(np.deg2rad(label["3d_heading"])), 0, length=1, color="green",
                                 label="L-Fitting-Heading")
                    ax_3d.quiver(cluster_center[0], cluster_center[1], cluster_center[2], np.cos(np.deg2rad(label["ret_dict"]["curve_fit_heading"])),
                                 np.sin(np.deg2rad(label["ret_dict"]["curve_fit_heading"])), 0, length=1, color="yellow", label="Curve-Fit-Heading")
                    rect = label["ret_dict"]["rects"]
                    for r in rect:
                        r.plot(ax=ax_3d)

                if "pvrcnn" in label:
                    pred_boxes = label["pvrcnn"]["pred_boxes"].cpu().numpy()
                    for p_box in pred_boxes:
                        box_points = build_bbox(center=p_box[:3], heading=p_box[6], l=p_box[4],
                                                w=p_box[3],
                                                h=p_box[5])

                        ax_3d.plot(box_points[:, 0], box_points[:, 1], box_points[:, 2], color="blue", label="pvrcnn")

                # s_vec = label["ret_dict"]["start_point"]
                # e_vec = label["ret_dict"]["end_point"]
                # ax_3d.plot([s_vec[0], e_vec[0]], [s_vec[1], e_vec[1]], [s_vec[2], e_vec[2]])
                # proj_points = label["ret_dict"]["projected_points"]
                # plot_pc(points=proj_points, ax=ax_3d, config=config, s=config.RADAR_3D_PLOT_POINT_SIZE * 4, c="red", elev=False, alpha=0.5)

                # a, b, c = label["ret_dict"]["popt"]
                # points = np.array([c, b, label["3d_dimensions"]["height"]])
                # plot_pc(points=np.array([points]), ax=ax_3d, config=config, s=config.RADAR_3D_PLOT_POINT_SIZE * 4, c="yellow", elev=False, alpha=0.5)
                # self.abline(ax=ax_3d, a=a, b=b, c=c)
                # self.abline(ax=ax_3d, a=-(1/a), b=b, c=c)

                im_ax.set_ylim(int(y1), int(y2))
                im_ax.set_xlim(int(x1), int(x2))
                im_ax.invert_yaxis()
                ax_3d.legend()
                plt.savefig(self.context.camera_to_lidar_instances + "/camera_to_lidar_" + str(self.df.loc[idx, "timestampLidar"])[:-4] + "_" + str(p_idx) + ".png")

        return im_ax, orig_im_ax, ax_3d

    def get_points_in_box(self, x1, y1, x2, y2, imgfov_pc_pixel, ranges):
        pc_points_in_box = np.nan
        cut_factors = [2.2, 2.7, 3, 4, 1]
        for cut_factor in cut_factors:
            b_x1, b_x2 = x1 + (x2 - x1) // cut_factor, x2 - (x2 - x1) // cut_factor
            b_y1, b_y2 = y1 + (y2 - y1) // cut_factor, y2 - (y2 - y1) // cut_factor
            left = np.logical_and(imgfov_pc_pixel[0, :] > b_x1, imgfov_pc_pixel[0, :] < b_x2)
            right = np.logical_and(imgfov_pc_pixel[1, :] > b_y1, imgfov_pc_pixel[1, :] < b_y2)
            mask_points_of_bb = np.logical_and(left, right)
            rngs = ranges[mask_points_of_bb]
            if len(rngs) < 6 and cut_factor != cut_factors[-1]:
                continue
            pc_points_in_box = np.median(rngs)
            if not np.isnan(pc_points_in_box):
                break
        if np.isnan(pc_points_in_box):
            return
        return b_x1, b_x2, b_y1, b_y2, pc_points_in_box, mask_points_of_bb

    def get_labels_from_2d(self, label, imgfov_pc_pixel, imgfov_pc_velo, class_key="cat", im_to_plot=None):
        ranges = np.linalg.norm(imgfov_pc_velo[:, 4:7], axis=-1)
        labels_from_2d = []
        for cur_label in label:
            obj = cur_label[class_key]
            x1, y1, x2, y2 = cur_label['bbox']
            if obj in self.classes_map:
                color = self.classes_map[obj].color
            else:
                continue

            ret_val = self.get_points_in_box(x1, y1, x2, y2, imgfov_pc_pixel, ranges)
            if ret_val is None:
                continue
            b_x1, b_x2, b_y1, b_y2, pc_points_in_box, mask_points_of_bb = ret_val
            point = np.median(imgfov_pc_velo[mask_points_of_bb][:, :3], axis=0)  # Center
            point = np.append(point, np.median(imgfov_pc_velo[mask_points_of_bb][:, 4:7], axis=0))
            point = np.append(point, np.array([x1, y1, x2, y2]))
            labels_from_2d.append(np.array(point))

            if im_to_plot is not None:
                cv2.rectangle(im_to_plot, (int(b_x1), int(b_y1)), (int(b_x2), int(b_y2)), (0, 0, 255), 2)
                cv2.putText(im_to_plot, 'R {}'.format(str(round(pc_points_in_box, 2))),
                            (int((x1 + x2) // 2), int((y1 + y2) // 2)), cv2.FONT_HERSHEY_SIMPLEX, 0.7,
                            (255, 0, 0), 2)

        return np.array(labels_from_2d)

    def add_info_to_label(self, processed_label, p, pts_extracted_from_pc, p_idx, center_only, class_key="cat", predicted_score=1):
        obj = None
        if pts_extracted_from_pc.shape[0] < self.config.MIN_PINT_PER_CLUSTER: return

        x1, y1, x2, y2 = p[6:10]
        points_in_lidar_space = pts_extracted_from_pc[:, 4:7]
        cluster_points, cluster_center = self.get_clusterd_points(points_in_lidar_space)

        for label in processed_label:
            if label["bbox"] != [x1, y1, x2, y2]: continue
            obj = self.config.wise_classes.classes_map[label[class_key]]

        if obj is None:
            print("Error, couldn't match obj")
            return

        for label in processed_label:
            if label["bbox"] != [x1, y1, x2, y2]: continue
            # label["3d_center_of_mass"] = orig_x, orig_y, orig_z  # In Original Lidar Space!
            # label["3d_raz"] = toRAzEl(orig_y, orig_x, orig_z)  # In Original Lidar Space!
            # label["3d_center_img"] = center.tolist()  # In Image Space
            label["3d_center_clustering"] = cluster_center.tolist()
            label["3d_heading"] = 0  # In Image Space
            label["3d_dimensions"] = {"length": 0, "width": 0, "height": 0}
            label["prediction_score"] = predicted_score
            label["instance_idx"] = p_idx

            if center_only: continue
            ret_val = get_L_fitting(pts_extracted_from_pc=cluster_points, center=cluster_center, obj=obj)
            if ret_val[0]:  # L-Shape succeded
                print("L-shape succeed")
                cluster_center, i_length, i_width, i_height, heading = ret_val[1:]
            else: # Case Point R-CNN
                if model_runner is not None:
                    print("L-shape Failed - PV-RCNN")
                    label["pvrcnn-input"] = cluster_points
                    res = model_runner.process_single(cluster_points)[0]  # Batch
                    label["pvrcnn"] = res

                    pred_boxes = label["pvrcnn"]["pred_boxes"].cpu().numpy()
                    if len(pred_boxes) == 0:
                        print("PV-RCNN Couldn't Found instance")
                        cluster_center, i_length, i_width, i_height, heading, txt_meta, ret_dict = build_rec_from_cluster_old2(pts_extracted_from_pc=cluster_points, center=cluster_center, obj=obj)
                    else:
                        max_pbox = max(pred_boxes, key=lambda x:x[4]*x[3])
                        cluster_center = max_pbox[:3]
                        i_length, i_width, i_height = max_pbox[3], max_pbox[4], max_pbox[5]
                        heading = max_pbox[6]
                        print("PV-RCNN - finished", cluster_center, i_length, i_width, i_height)
                    # for p_box in pred_boxes:
                    #     box_points = build_bbox(center=p_box[:3], heading=p_box[6], l=p_box[4],
                    #                             w=p_box[3],
                    #                             h=p_box[5],
                    #                             alpha=0)
                    #
                    #     label["3d_heading"] = p_box[6]

                else:  # No-PV-RCNN model available
                    cluster_center, i_length, i_width, i_height, heading, txt_meta, ret_dict = build_rec_from_cluster_old2(pts_extracted_from_pc=cluster_points, center=cluster_center, obj=obj)

            label["3d_center_clustering"] = cluster_center.tolist()
            label["3d_heading"] = round(heading, 2)  # In Image Space
            label["3d_dimensions"] = {"length": i_length, "width": i_width, "height": i_height}
            # label["text"] = txt_meta

            # label["ret_dict"] = ret_dict
        return

    def range_measurement(self, idx, plot=0, center_only=False, save=False, force=False, plot_only=False, pv_rcnn=False):
        label_path = self.context.camera_to_lidar + "/camera_to_lidar_" + str(self.df.loc[idx, "timestampLidar"])[:-4] + ".json"
        if not force and os.path.exists(label_path) and not plot_only: return
        if plot_only and idx % plot != 0: return
        if not self.read_data_context(idx): return

        im_to_plot = draw_label_on_image(frame_data=self.camera_label_rect, im=self.im.copy(), classes_map=self.classes_map)

        imgfov_pc_pixel, imgfov_pc_velo = preprocess_lidar_to_image(pts=self.pc_lidar, im=self.im.copy(), config=self.config, calibration_params=self.context.spatial_calibration_params)

        labels_from_2d = self.get_labels_from_2d(label=self.camera_label_rect, imgfov_pc_pixel=imgfov_pc_pixel, imgfov_pc_velo=imgfov_pc_velo)
        if len(labels_from_2d) <= 0:
            save_label(label_path=label_path, camera_label_rect=self.camera_label_rect, save=save)
            return str(self.df.loc[idx, "timestampRadar"])

        lidar_centers_to_display = fix_lidar_mat(labels_from_2d, config=self.config, calibration_params=self.context.spatial_calibration_params, radar_lidar=True)

        # Iterate over points of center
        for p_idx, p in enumerate(lidar_centers_to_display):
            center = p[:3]
            x1, y1, x2, y2 = p[6:10]
            # orig_x, orig_y, orig_z = p[3:6]
            pts_extracted_from_pc = get_pts_extractrd_from_pc(x1, y1, x2, y2, imgfov_pc_pixel, imgfov_pc_velo)

            self.add_info_to_label(self.camera_label_rect, p=p, p_idx=p_idx, center_only=center_only, pts_extracted_from_pc=pts_extracted_from_pc)

        if plot and idx % plot == 0:
            self.plot(im_to_plot, imgfov_pc_pixel=imgfov_pc_pixel, imgfov_pc_velo=imgfov_pc_velo,
                      lidar_centers_to_display=lidar_centers_to_display, idx=idx, instances=args.instances, center_only=center_only)

        save_label(label_path=label_path, camera_label_rect=self.camera_label_rect, save=save)
        return str(self.df.loc[idx, "timestampRadar"])

    def project_distort(self, idx):
        if not self.read_data_context(idx):
            print("err in data")
            return

        while True:
            fileName = "/home/amper/AI/Harel/AI_Infrastructure/DataPipeline/configs/distorted_lidar_radar_201104"
            with open(fileName, "r") as fp:
                spatial_calibration_params = json.load(fp)

            gs = gridspec.GridSpec(ncols=1, nrows=1)
            fig = plt.figure(figsize=(20, 10))
            plt.tight_layout(pad=2)

            orig_im_ax = fig.add_subplot(gs[0, 0])
            orig_im_ax.axis(False)

            CAMERA_FOV_AZ = spatial_calibration_params["CAMERA_FOV_AZ"]
            CAMERA_FOV_EL = spatial_calibration_params["CAMERA_FOV_EL"]

            MAX_RANGE = 30
            MIN_RANGE = 3
            pts_velo = self.pc_lidar.copy()
            pts_velo[:, :3], rot = apply_6DOF(pts_velo[:, :3], yaw=spatial_calibration_params["yaw-lidar-camera"],
                                              roll=spatial_calibration_params["roll-lidar-camera"],
                                              pitch=spatial_calibration_params["pitch-lidar-camera"],
                                              tx=spatial_calibration_params["tx-lidar-camera"],
                                              ty=spatial_calibration_params["ty-lidar-camera"],
                                              tz=spatial_calibration_params["tz-lidar-camera"])

            pts_velo = cut_by_range(pts_velo, max_range=MAX_RANGE, min_range=MIN_RANGE)
            pts_velo, mask = filter_fov_az(pts_velo, az=CAMERA_FOV_AZ, x_idx=1, y_idx=0)
            r, az, el = toRAzEl(x=pts_velo[:, 1], y=pts_velo[:, 0], z=pts_velo[:, 2])

            az = 1824 - (1824 * ((az + (CAMERA_FOV_AZ // 2)) / CAMERA_FOV_AZ)).astype(np.int)
            el = 940 - (940 * ((el + (CAMERA_FOV_EL // 2)) / CAMERA_FOV_EL)).astype(np.int)
            norms = r

            cNorm = colors.Normalize(vmin=np.min(norms), vmax=np.max(norms))
            scat = orig_im_ax.scatter(x=np.clip(np.round(az), 0, self.im.shape[1] - 1).astype(np.int),
                                      y=np.clip(np.round(el), 0, self.im.shape[0] - 1).astype(np.int), s=self.config.PROJECTION_POINT_SIZE, c=norms,
                                      cmap="jet",
                                      norm=cNorm)

            cb = plt.colorbar(scat, ax=orig_im_ax, fraction=0.026, pad=0.08)
            orig_im_ax.imshow(self.im)
            plt.show()

    def audi_evaluation(self, audi_engine, exp_name, plot_every=5, center_only=False):
        print("Audi-Evaluation")

        cur_working_dir = module_path + "/eval_results/" + str(exp_name) + "/"
        os.makedirs(cur_working_dir, exist_ok=True)
        self.context.camera_to_lidar_instances = cur_working_dir + "/images/"
        os.makedirs(self.context.camera_to_lidar_instances, exist_ok=True)

        e_results = EvalResults(model_name="camera_to_lidar_v1.0", dataset_name=audi_engine.name, out_path=cur_working_dir, config=self.config)

        yolo_wrapper = YoloWrapper()
        # for audi_idx in indices:
        for audi_idx in tqdm(range(len(audi_engine.df))):
            plot = audi_idx % plot_every == 0
            im, label, lidar = audi_engine.get_image(audi_idx), audi_engine.get_3d_label(audi_idx), audi_engine.get_lidar(audi_idx)

            yolo_predictions = yolo_wrapper.infer_im(im=im)

            f_results = FrameResults()

            imgfov_pc_pixel, imgfov_pc_velo = audi_engine.process_lidar_on_image(lidar)
            labels_from_2d = self.get_labels_from_2d(label=label, imgfov_pc_pixel=imgfov_pc_pixel, imgfov_pc_velo=imgfov_pc_velo, class_key=audi_engine.class_key, im_to_plot=im)

            # Apply algorithm
            yolo_indices = []
            for p_idx, p in enumerate(labels_from_2d):
                x1, y1, x2, y2 = p[6:10]

                score = 1
                # success = False
                # score = 0
                # for yolo_idx, yolo_pred in enumerate(yolo_predictions):
                #     cur_iou = yolo_pred.get_iou(x1=x1, y1=y1, x2=x2, y2=y2)
                #     if cur_iou > 0.5:
                #         success = True
                #         score = yolo_pred.score
                #         print("Success IOU! - score:", score)
                #         yolo_indices.append(yolo_idx)
                #         break
                #
                # if not success:
                #     continue

                pts_extracted_from_pc = get_pts_extractrd_from_pc(x1, y1, x2, y2, imgfov_pc_pixel, imgfov_pc_velo)
                if pts_extracted_from_pc is None: continue
                self.add_info_to_label(label, p=p, p_idx=p_idx, center_only=center_only, pts_extracted_from_pc=pts_extracted_from_pc, class_key="class", predicted_score=score)

            # if not (plot and audi_idx % plot == 0): continue

            if plot:
                plt.close()
                matplotlib.rc('font', **font)
                gs = gridspec.GridSpec(ncols=2, nrows=2)
                fig = plt.figure(figsize=(30, 15))
                plt.tight_layout(pad=0)
                plt.suptitle("Camera to lidar evaluation - audi dataset")
                orig_im_ax = fig.add_subplot(gs[0, 0])
                orig_im_ax.axis(False)
                im_2d_labels = draw_label_on_image(label, im=im.copy(), classes_map=self.classes_map, class_key="class")
                orig_im_ax.imshow(im_2d_labels)

                im_ax = fig.add_subplot(gs[1, 0])
                im_ax.axis(False)
                im_to_plot = audi_engine.map_lidar_points_onto_image(im, lidar)

                im_ax.imshow(im_to_plot)
                ax_3d = get_3d_ax(fig=fig, loc=gs[:, 1], x_left=0, x_right=100, y_left=-30, y_right=30)
                sc3 = plot_pc(points=lidar["points"], ax=ax_3d, config=self.config, s=self.config.RADAR_3D_PLOT_POINT_SIZE // 6, c="lightskyblue", elev=False, alpha=0.5)

            legend_flag, legend_flag2 = False, False
            title = "Point Cloud\n"
            preds, labels = [], []

            # Add yolo
            # for yolo_idx, yolo_pred in enumerate(yolo_predictions):
            #     if yolo_idx in yolo_indices:
            #         continue
            #     preds.append(yolo_pred)

            for gt_idx, gt_label in enumerate(label):
                gt_center = [gt_label["tx"], gt_label["ty"], gt_label["tz"]]
                gt_center = np.array([gt_center])

                if plot: plot_pc(points=gt_center, ax=ax_3d, config=self.config, s=self.config.RADAR_3D_PLOT_POINT_SIZE*4, c="orange", elev=False, alpha=1, label="label" if legend_flag == 0 else "")

                heading_plot = gt_label["heading"]

                if 0 <= heading_plot <= 90:
                    heading_plot *= -1

                box_points = build_bbox(center=gt_center, heading=heading_plot, l=gt_label["sw"], w=gt_label["sl"], h=gt_label["sh"], alpha=0, mat=gt_label["rotation"])

                # title += str(gt_center) + ":" + str(gt_label["heading"]) + "\n"
                if plot: ax_3d.plot(box_points[:, 0], box_points[:, 1], box_points[:, 2], color="orange") # , label="gt_box" if legend_flag == 0 else "")
                legend_flag = True

                # l_value = np.array([gt_label["tx"], gt_label["ty"], gt_label["sl"], gt_label["sw"]])
                try:
                    l_value = Label2D(cls=gt_label["class"], score=1)
                except NotValidClass as e:
                    continue
                l_value.set_center(cx=gt_label["tx"], cy=gt_label["ty"], w=gt_label["sl"], h=gt_label["sw"])
                labels.append(l_value)
                # f_results.labels[gt_label["class"]].append(l_value)

                # Plot Prediction
                if "3d_center_clustering" not in gt_label: continue
                label_cluster = np.array([gt_label["3d_center_clustering"]])
                if plot: plot_pc(points=label_cluster, ax=ax_3d, config=self.config, s=self.config.RADAR_3D_PLOT_POINT_SIZE * 8, c="red", elev=False, alpha=1,
                                 label="pred-center" if legend_flag2 == 0 else "")

                heading_plot = gt_label["3d_heading"]
                if 0 <= heading_plot <= 90: heading_plot *= -1
                box_points = build_bbox(center=label_cluster, heading=gt_label["3d_heading"], l=gt_label["3d_dimensions"]["width"],
                                        w=gt_label["3d_dimensions"]["length"],
                                        h=gt_label["3d_dimensions"]["height"], alpha=0)
                # if plot: ax_3d.plot(box_points[:, 0], box_points[:, 1], box_points[:, 2], color="green", label="pred_box" if legend_flag2 == 0 else "")
                legend_flag2 = True

                prediction_score = gt_label["prediction_score"]
                #p_value = np.array([gt_label["3d_center_clustering"][0], gt_label["3d_center_clustering"][1], gt_label["3d_dimensions"]["length"], gt_label["3d_dimensions"]["width"]])
                p_label = Label2D(cls=gt_label["class"], score=prediction_score)
                p_label.set_center(cx=gt_label["3d_center_clustering"][0], cy=gt_label["3d_center_clustering"][1], w=gt_label["3d_dimensions"]["length"], h=gt_label["3d_dimensions"]["width"])
                preds.append(p_label)
                #f_results.preds[gt_label["class"]].append(p_value)

            f_results.set_labels(labels)
            f_results.set_prediction(preds)
            e_results.set_frame_results(frame_results=f_results)

            if plot:
                ax_3d.set_title(title)
                ax_3d.view_init(elev=90, azim=180)
                ax_3d.legend(loc='lower center') # loc='upper left')
                fig_path = self.context.camera_to_lidar_instances + "/camera_to_lidar_" + str(audi_idx) + ".png"
                plt.savefig(fig_path)
                plt.show()
        e_results.save_results()

    def od_3d_evaluation(self, data_engine, od_2d_model, evaluation_results, plot_every=5, center_only=False):
        for audi_idx in tqdm(range(len(data_engine))):
            plot = audi_idx % plot_every == 0
            im, label, lidar = data_engine.get_image(audi_idx), data_engine.get_3d_label(audi_idx), data_engine.get_lidar(audi_idx)

            od_2d_model_predictions = od_2d_model.infer_im(im=im)

            f_results = FrameResults()

            imgfov_pc_pixel, imgfov_pc_velo = data_engine.process_lidar_on_image(lidar)
            labels_from_2d = self.get_labels_from_2d(label=label, imgfov_pc_pixel=imgfov_pc_pixel, imgfov_pc_velo=imgfov_pc_velo, class_key=data_engine.class_key, im_to_plot=im)

            # Apply algorithm
            od_2d_indices = []
            for p_idx, p in enumerate(labels_from_2d):
                x1, y1, x2, y2 = p[6:10]

                score = 1
                success = False
                for od_2d_idx, od_2d_pred in enumerate(od_2d_model_predictions):
                    cur_iou = od_2d_pred.get_iou(x1=x1, y1=y1, x2=x2, y2=y2)
                    if cur_iou > 0.5:
                        success = True
                        score = od_2d_pred.score
                        od_2d_indices.append(od_2d_idx)
                        break

                if not success:
                    continue

                pts_extracted_from_pc = get_pts_extractrd_from_pc(x1, y1, x2, y2, imgfov_pc_pixel, imgfov_pc_velo)
                if pts_extracted_from_pc is None: continue
                self.add_info_to_label(label, p=p, p_idx=p_idx, center_only=center_only, pts_extracted_from_pc=pts_extracted_from_pc, class_key="class", predicted_score=score)

            if plot:
                plt.close()
                matplotlib.rc('font', **font)
                gs = gridspec.GridSpec(ncols=2, nrows=2)
                fig = plt.figure(figsize=(30, 15))
                plt.tight_layout(pad=0)
                plt.suptitle("Camera to lidar evaluation - audi dataset")
                orig_im_ax = fig.add_subplot(gs[0, 0])
                orig_im_ax.axis(False)
                im_2d_labels = draw_label_on_image(label, im=im.copy(), classes_map=self.classes_map, class_key="class")
                orig_im_ax.imshow(im_2d_labels)

                im_ax = fig.add_subplot(gs[1, 0])
                im_ax.axis(False)
                im_to_plot = data_engine.map_lidar_points_onto_image(im, lidar)

                im_ax.imshow(im_to_plot)
                ax_3d = get_3d_ax(fig=fig, loc=gs[:, 1], x_left=0, x_right=100, y_left=-30, y_right=30)
                sc3 = plot_pc(points=lidar["points"], ax=ax_3d, config=self.config, s=self.config.RADAR_3D_PLOT_POINT_SIZE // 6, c="lightskyblue", elev=False, alpha=0.5)

            legend_flag, legend_flag2 = False, False
            title = "Point Cloud\n"
            preds, labels = [], []

            # Add yolo
            for od_2d_idx, od_2d_pred in enumerate(od_2d_model_predictions):
                if od_2d_idx in od_2d_indices:
                    continue
                preds.append(od_2d_pred)

            for gt_idx, gt_label in enumerate(label):
                gt_center = [gt_label["tx"], gt_label["ty"], gt_label["tz"]]
                gt_center = np.array([gt_center])

                if plot: plot_pc(points=gt_center, ax=ax_3d, config=self.config, s=self.config.RADAR_3D_PLOT_POINT_SIZE*4, c="orange", elev=False, alpha=1, label="label" if legend_flag == 0 else "")

                heading_plot = gt_label["heading"]

                if 0 <= heading_plot <= 90:
                    heading_plot *= -1

                box_points = build_bbox(center=gt_center, heading=heading_plot, l=gt_label["sw"], w=gt_label["sl"], h=gt_label["sh"], alpha=0, mat=gt_label["rotation"])

                # title += str(gt_center) + ":" + str(gt_label["heading"]) + "\n"
                if plot: ax_3d.plot(box_points[:, 0], box_points[:, 1], box_points[:, 2], color="orange") # , label="gt_box" if legend_flag == 0 else "")
                legend_flag = True

                # l_value = np.array([gt_label["tx"], gt_label["ty"], gt_label["sl"], gt_label["sw"]])
                try:
                    l_value = Label2D(cls=gt_label["class"], score=1)
                except NotValidClass as e:
                    continue
                l_value.set_center(cx=gt_label["tx"], cy=gt_label["ty"], w=gt_label["sl"], h=gt_label["sw"])
                labels.append(l_value)
                # f_results.labels[gt_label["class"]].append(l_value)

                # Plot Prediction
                if "3d_center_clustering" not in gt_label: continue
                label_cluster = np.array([gt_label["3d_center_clustering"]])
                if plot: plot_pc(points=label_cluster, ax=ax_3d, config=self.config, s=self.config.RADAR_3D_PLOT_POINT_SIZE * 8, c="red", elev=False, alpha=1,
                                 label="pred-center" if legend_flag2 == 0 else "")

                heading_plot = gt_label["3d_heading"]
                if 0 <= heading_plot <= 90: heading_plot *= -1
                box_points = build_bbox(center=label_cluster, heading=gt_label["3d_heading"], l=gt_label["3d_dimensions"]["width"],
                                        w=gt_label["3d_dimensions"]["length"],
                                        h=gt_label["3d_dimensions"]["height"], alpha=0)
                # if plot: ax_3d.plot(box_points[:, 0], box_points[:, 1], box_points[:, 2], color="green", label="pred_box" if legend_flag2 == 0 else "")
                legend_flag2 = True

                prediction_score = gt_label["prediction_score"]
                #p_value = np.array([gt_label["3d_center_clustering"][0], gt_label["3d_center_clustering"][1], gt_label["3d_dimensions"]["length"], gt_label["3d_dimensions"]["width"]])
                p_label = Label2D(cls=gt_label["class"], score=prediction_score)
                p_label.set_center(cx=gt_label["3d_center_clustering"][0], cy=gt_label["3d_center_clustering"][1], w=gt_label["3d_dimensions"]["length"], h=gt_label["3d_dimensions"]["width"])
                preds.append(p_label)
                #f_results.preds[gt_label["class"]].append(p_value)

            f_results.set_labels(labels)
            f_results.set_prediction(preds)
            evaluation_results.set_frame_results(frame_results=f_results)

            if plot:
                ax_3d.set_title(title)
                ax_3d.view_init(elev=90, azim=180)
                ax_3d.legend(loc='lower center') # loc='upper left')
                fig_path = evaluation_results.images_out_path + "/camera_to_lidar_" + str(audi_idx) + ".png"
                plt.savefig(fig_path)
                plt.show()


def run_drive(_dest):
    _s = BBDisplay(path=_dest)
    # Check if do
    if not args.force:
        _count = len(glob(s.context.camera_to_lidar + "/*"))
        _len_data = len(s.df)
        if (_len_data - count) < _len_data * 0.05:
            print("check_if_do: Skipping - ", drive, _count, "len-df", _len_data)
            return
        else:
            print("check_if_do: Not-Skipping - ", drive, _count, "len-df", _len_data)

    for _i in tqdm(range(len(_s.df))):
        try:
            _s.range_measurement(idx=_i, plot=args.plot, save=True, force=args.force, plot_only=args.plot_only, center_only=args.centers_only)
        except Exception as e:
            PrintException()
            continue

_config = Config()

if __name__ == '__main__':
    if args.run_all:
        srcs = pd.read_csv("/DataPipeline/src/datasets_index.csv", index_col=False)
        if args.rev:
            print("reversing")
            srcs = srcs[::-1].reset_index(drop=True)

        src = [srcs.loc[src_idx]["drive"] for src_idx in range(len(srcs))]
        print(src)
        if not args.processes:
            for src_idx in range(len(srcs)):
                drive = srcs.loc[src_idx]["drive"]
                if pd.isna(drive):
                    continue
                if "highway" not in drive:
                    continue
                print("Drive: ", drive)
                dest = str(srcs.loc[src_idx]["dest"]) + "/" + drive + "/"
                dest = dest.replace("data_backup7/", "/media/amper/data_backup7/")
                dest = dest.replace("HDD/", "/mnt/hdd/")
                s = BBDisplay(path=dest)
                s.set_config(config=_config)

                # Check if do
                if not args.force:
                    count = len(glob(s.context.camera_to_lidar + "/*"))
                    len_data = len(s.df)
                    if (len_data - count) < len_data * 0.05:
                        print("check_if_do: Skipping - ", drive, count, "len-df", len_data)
                        continue
                    else:
                        print("check_if_do: Not-Skipping - ", drive, count, "len-df", len_data)

                for i in tqdm(range(len(s.df))):
                    if i < args.start: continue
                    try:
                        s.range_measurement(idx=i, plot=args.plot, save=not args.no_save, force=args.force, plot_only=args.plot_only, center_only=args.centers_only)
                    except Exception as e:
                        PrintException()
                        continue
        else:
            to_pool = []
            for src_idx in range(len(srcs)):
                drive = srcs.loc[src_idx]["drive"]
                if pd.isna(drive):
                    continue
                print("Drive: ", drive)
                dest = str(srcs.loc[src_idx]["dest"]) + "/" + drive + "/"
                dest = dest.replace("data_backup7/", "/media/amper/data_backup7/")
                dest = dest.replace("HDD/", "/mnt/hdd/")
                to_pool.append(dest)

            with Pool(args.processes) as p_pool:
                print(p_pool.map(run_drive, to_pool))

    else:
        args.instances = True

        s = BBDisplay()
        s.set_config(config=_config)

        # s.project_distort(idx=830)
        # s.range_measurement(idx=830, plot=1, force=True)
        # s.range_measurement(idx=2360, plot=1, force=True)
        # s.range_measurement(idx=1037, plot=1, force=True)
        # plt.show()

        s.context.camera_to_lidar_instances = module_path + "/images/instances/"
        os.makedirs(s.context.camera_to_lidar_instances, exist_ok=True)

        cur_audi_engine = AudiEngine(base_path="/media/amper/open_source5/audi/", val=True)
        if args.eval:
            if args.eval != -1:
                cur_audi_engine.df = cur_audi_engine.df.sample(args.eval).reset_index(drop=True)
            s.audi_evaluation(audi_engine=cur_audi_engine, exp_name=str(abs(args.eval)) + "_sampels", plot_every=args.plot, center_only=args.centers_only)

        # print("Full Phase")
        # cur_audi_engine = AudiEngine(base_path="/media/amper/open_source5/audi/")
        # cur_audi_engine.df = cur_audi_engine.df[460:464].reset_index(drop=True)
        # s.audi_evaluation(audi_engine=cur_audi_engine, exp_name="full_audi_run", plot_every=1, center_only=True)

        # s.context.camera_to_lidar_images = s.context.camera_to_lidar_instances
        # for idx in tqdm(np.random.randint(0, len(s.df), 50)):
        #     ts_radar = s.range_measurement(idx=idx, plot=1, force=True, pv_rcnn=args.pvrcnn, save=False)

        # for start in [1000, 2000, 3000]:
        #     args.instances = False
        #     VIDEO_DIR = "/home/amper/AI/Harel/AI_Infrastructure/Calibrator/images/" + s.get_drive_name() + "_" + str(start) + "/"
        #     if os.path.exists(VIDEO_DIR):
        #         shutil.rmtree(VIDEO_DIR)
        #     os.makedirs(VIDEO_DIR, exist_ok=True)
        #     for i in tqdm(range(start, start + 100)):
        #         # ts_radar = s.display_for_video(idx=i)
        #         ts_radar = s.range_measurement(idx=i, plot=1, force=True)
        #         if ts_radar:
        #             plt.savefig(VIDEO_DIR + ts_radar + "_" + str(i) + ".png")
        #             plt.close()
        #
        #     make_video_from_dir(src_dir=VIDEO_DIR, out_dir=VIDEO_DIR + "../", video_name=s.get_drive_name() + "_" + str(start), fr=3)
