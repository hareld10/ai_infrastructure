# cur_indices = np.zeros((peaks.shape[0], peaks.shape[1], 1), dtype=np.float32)  # Range Az El

# arr_of_indices[:, :, el_level] = ffta_val[peaks, el_level]
# From indices to real values
# cur_points[:, 0] = self.parent.data.radar_param["rng_vec"].min() + indices[:, 0] * self.parent.data.radar_param["rng_bin"]
# cur_points[:, 1] = np.rad2deg(np.arcsin(self.parent.data.radar_param["az_vec"].min())) + indices[:, 1] * np.rad2deg(np.arcsin(self.parent.data.radar_param["az_bin"]))
# cur_points[:, 2] = np.rad2deg(np.arcsin(self.parent.data.radar_param["el_vec"].min())) + el_level * np.rad2deg(np.arcsin(self.parent.data.radar_param["el_bin"]))

# cur_points[:, 0] = self.parent.data.radar_param["rng_vec"].min() + indices[:, 0] * self.parent.data.radar_param["rng_bin"]
# cur_points[:, 0] = self.parent.data.radar_param["range"][0] + indices[:, 0] * self.parent.data.bins["range"]
# cur_points[:, 1] = -40 + indices[:, 1] * (80/20)  # Az
# cur_points[:, 2] = -12 + el_level * (24 / 6)  # El

# Fix bin and minimum (zero_pad)
# cur_points[:, 0] = self.parent.data.radar_param["range"][0] + indices[:, 0] * self.parent.data.bins["range"]


# cur_points[:, 1] = np.rad2deg(np.arcsin(az_min)) + indices[:, 1] * np.rad2deg(np.arcsin(az_bin))
# cur_points[:, 2] = np.rad2deg(np.arcsin(el_min)) + el_level * np.rad2deg(np.arcsin(el_bin))


# Working
# cur_points[:, 0] = self.parent.data.radar_param["range"][0] + indices[:, 0] * self.parent.data.bins["range"]
# cur_points[:, 1] = np.rad2deg(np.arcsin(self.parent.data.radar_param["az"][0])) + indices[:, 1] * np.rad2deg(np.arcsin(self.parent.data.bins["az"]))
# cur_points[:, 2] = np.rad2deg(np.arcsin(self.parent.data.radar_param["el"][0])) + el_level * np.rad2deg(np.arcsin(self.parent.data.bins["el"]))

# arr_of_indices[:, 0] = self.parent.data.radar_param["range"][0] + arr_of_indices[:, 0] * self.parent.data.bins["range"]
# arr_of_indices[:, 1] = np.rad2deg(np.arcsin(self.az_min)) + arr_of_indices[:, 1] * np.rad2deg(np.arcsin(self.az_bin))
# arr_of_indices[:, 2] = np.rad2deg(np.arcsin(self.el_min)) + arr_of_indices[:, 2] * np.rad2deg(np.arcsin(self.el_bin))
# arr_of_indices =
# ret = np.max(arr_of_indices, axis=1, keepdims=True)


# locs = ax3.get_xticks()
# print("locs", ax3.get_xticks(), ax3.get_xticklabels(), [item.get_text() for item in ax3.get_xticklabels()])
# ax3.set_xticklabels([int(int(item)*self.az_bin) for item in ax3.get_xticks()])

# labels = [item.get_text() for item in ax3.get_xticklabels()]
# labels = [0] + list(np.linspace(-40, 40, len(labels)+1).astype(np.int))
# print("len(labels)", len(labels), labels)
# labels[1] = 'Testing'

# ax3.set_xticklabels(labels)
# print("len(", len(locs_x))

# ax3.set_xticklabels([1, 2,3,4,5,6,7,8])
# ax3.set_xticklabels(np.linspace(-self.min_az_fov, self.min_az_fov, 8).astype(np.int))
# ax3.set_yticklabels([1, 2, 3, 4, 5, 6, 7, 8])
# ax3.set_yticks(list(range(int(self.parent.params.params_values["range"].get_value()), 0, len(ax3.get_yticks()))))

# ax3.set_xticklabels(list(range(-self.min_az_fov, self.min_az_fov, len(ax3.get_xticks()))), minor=False)
# ax3.set_yticklabels(list(range(int(self.parent.params.params_values["range"].get_value()),0, len(ax3.get_yticks()))))


def get_4Dfft(self, s_ant, zero_pad=0):
    SRe = torch.from_numpy(s_ant.real).cuda(self.gpus_list[0])
    SIm = torch.from_numpy(s_ant.imag).cuda(self.gpus_list[0])

    s_t = torch.zeros(SRe.shape[1], SRe.shape[0], 6, 20, 2).cuda(self.gpus_list[0])

    s_t[:, :, self.ch_order[:, 0], self.ch_order[:, 1], 0] = SRe.permute(2, 0, 1)
    s_t[:, :, self.ch_order[:, 0], self.ch_order[:, 1], 1] = SIm.permute(2, 0, 1)

    if zero_pad:
        zero_pad = int(zero_pad)
        print("zero padding ", zero_pad)
        s_t = F.pad(s_t, (0, 0, 0, 20 * zero_pad - 20, 0, 6 * zero_pad * 2 - 6), "constant", 0)

    ffta = torch.fft(s_t, 2, normalized=True)  # torch.Size([48, 600, 6, 20, 2]) (dop,rng,el,az,real/imag)
    array = torch.zeros(ffta.shape[0] * 2, ffta.shape[1], ffta.shape[2], ffta.shape[3]).cuda(self.gpus_list[0])

    array[::2, :, :, :] = ffta[:, :, :, :, 0]
    array[1::2, :, :, :] = ffta[:, :, :, :, 1]

    axis = 3
    array = torch.roll(array, shifts=array.shape[axis] // 2, dims=axis)
    # min_idx = int(np.ceil(array.shape[axis] // 2 + self.min_az_fov / 180 * array.shape[axis]) - 1)
    # max_idx = int(np.floor(array.shape[axis] // 2 + self.max_az_fov / 180 * array.shape[axis]) + 1)
    # array = array[:, :, :, min_idx:max_idx]
    return array.cpu()




def fix_df_label_lidar(self, pts, calibration_params):
    def filter_fov_az_df(df, az):
        if az == 0:
            return df
        df = df[df.loc["cx"].values > 0]
        r, Az, El = toRAzEl(x=df.loc["cx"].values, y=df.loc["cy"].values, z=df.loc["cz"].values)
        df_mask = np.logical_not(np.logical_and(Az < (az // 2), (-az // 2) < Az))
        df = df[df_mask]
        return df

    pts_fixed, mask = filter_fov_az_df(pts, 80)

    pts_fixed = lidar_to_radar_coordinates(pts_fixed)
    pts_fixed = cut_by_range(pts_fixed, MAX_RANGE, MIN_RANGE)

    pts_fixed, rot = apply_6DOF(pts_fixed, yaw=calibration_params["yaw-radar-lidar"],
                                roll=calibration_params["roll-radar-lidar"],
                                pitch=calibration_params["pitch-radar-lidar"],
                                tx=calibration_params["tx-radar-lidar"],
                                ty=calibration_params["ty-radar-lidar"],
                                tz=calibration_params["tz-radar-lidar"])
    return pts_fixed, mask



 # print(pts_extracted_from_pc)
                # for theta in np.linspace(0, 360, 60):
                #     cur_point_center = np.array([-w / 2, -l / 2, 0])
                #     ps = []
                #     for multiplier in np.linspace(0, 1, 20):
                #         ps.append(cur_point_center + [w * multiplier, 0, 0])
                #         ps.append(cur_point_center + [0, l * multiplier, 0])
                #
                #     ps = np.array(ps)
                #     rot_mat = get_rot_mat(theta)
                #     ps_on_line = center + np.dot(rot_mat, ps.T).T
                #
                #     p1 = center + np.dot(rot_mat, [w / 2, -l / 2, h / 2])
                #     p2 = center + np.dot(rot_mat, [w / 2, l / 2, h / 2])
                #     p3 = center + np.dot(rot_mat, [-w / 2, l / 2, h / 2])
                #     p4 = center + np.dot(rot_mat, [-w / 2, -l / 2, h / 2])
                #
                #     rec_points = np.array([p1, p2, p3, p4])

                    # x_cond = np.logical_and(lidar_to_display[:, 0] >= np.min(rec_points[:, 0]), lidar_to_display[:, 0] <= np.max(rec_points[:, 0]))
                    # y_cond = np.logical_and(lidar_to_display[:, 1] >= np.min(rec_points[:, 1]), lidar_to_display[:, 1] <= np.max(rec_points[:, 1]))
                    # # z_cond = np.logical_and(lidar_to_display[:, 2] >= np.min(rec_points[:, 2]), lidar_to_display[:, 2] <= np.max(rec_points[:, 2]))
                    # z_cond = True

                    # cond = np.logical_and(np.logical_and(x_cond, y_cond), z_cond)
                    # if theta == 0:
                    #     pts_extracted_from_pc = lidar_to_display[cond][:, :3]
                    # print(pts_extracted_from_pc)
                    # np.save("/home/amper/AI/Harel/pts_extracted_from_pc_" +str(p_idx), pts_extracted_from_pc)
                    # if pts_extracted_from_pc.shape[0] == 0:
                    #     # continue
                    #     # print("No Points", theta)
                    #     continue
                    # else:
                    #     pass
                        # print("Some Points!", pts_extracted_from_pc.shape)
                    # lst_score = np.sum(np.linalg.lstsq(a=pts_extracted_from_pc.T, b=ps_on_line.T, rcond=0)[0])
                    # scores_results[theta] = (lst_score, ps_on_line)
                    # ax_3d.plot(ps_on_line[:, 0], ps_on_line[:, 1], ps_on_line[:, 2])

                # if len(scores_results)==0:
                #     print("No Points in Block")
                #     continue
                # val = min(scores_results.items(), key=lambda x: x[1][0])
                # ax_3d.plot(val[1][1][:, 0], val[1][1][:, 1], val[1][1][:, 2])
                # heading = val[0]
                # print(heading)

 """
Semantice_legend
"""
# process_lidar_to_image_on_ax(pts=self.pc_lidar, im=self.im, config=config, ax=lidar_semantic_ax, calibration_params=calibration_params, semantic=self.lidar_seg, cbar=False)

# handles = [
#     patches.Rectangle((0, 0), 1, 1, color=np.array(c)/255) for n, c in color_map.items()
# ]
# labels = [labels_map[n] for n, c in color_map.items()]
# lidar_semantic_ax.legend(handles, labels, bbox_to_anchor=(1.02, 1), loc='upper left', columnspacing=5)


# pts_extracted_from_pc = fix_lidar_mat(pts_extracted_from_pc, config=config, calibration_params=calibration_params, radar_lidar=radar_lidar_flag)
# plot_pc(points=pts_extracted_from_pc[:, :3], ax=ax_3d, config=config, s=config.RADAR_3D_PLOT_POINT_SIZE, c="red", elev=False, alpha=1)
# x_cond = np.logical_and(lidar_to_display[:, 0] >= np.min(rec_points[:, 0]), lidar_to_display[:, 0] <= np.max(rec_points[:, 0]))
# y_cond = np.logical_and(lidar_to_display[:, 1] >= np.min(rec_points[:, 1]), lidar_to_display[:, 1] <= np.max(rec_points[:, 1]))
# cond = np.logical_and(x_cond, y_cond)
# pts_extracted_from_pc = lidar_to_display[cond][:, :3]

# Plot
# for p_idx, p in enumerate(lidar_centers_to_display):
#     center_3d_image = p[:3]
#     x1, y1, x2, y2 = p[-7:-3]
#     pts_extracted_from_pc = get_pts_extractrd_from_pc(x1, y1, x2, y2, imgfov_pc_pixel, imgfov_pc_velo)
#     ax_3d.cla()
#     # pts_extracted_from_pc[:, :3] = pts_extracted_from_pc[:, 4:7]
#     pts_extracted_from_pc = fix_lidar_mat(pts_extracted_from_pc, config=config, calibration_params=self.context.spatial_calibration_params)
#     # plot_pc(points=np.array([center_clusters]), ax=ax_3d, config=config, s=config.RADAR_3D_PLOT_POINT_SIZE * 4, c="yellow", elev=False, alpha=1)
#     plot_pc(points=np.array([center_3d_image]), ax=ax_3d, config=config, s=config.RADAR_3D_PLOT_POINT_SIZE * 4, c="lightskyblue", elev=False, alpha=1)
#     plot_pc(points=pts_extracted_from_pc[:, :3], ax=ax_3d, config=config, s=config.RADAR_3D_PLOT_POINT_SIZE / 4, c="red", elev=False, alpha=0.3)
#     ps_single = self.build_rec_from_center(pts_extracted_from_pc=pts_extracted_from_pc, center=center_3d_image)
#     ax_3d.plot(ps_single[:, 0], ps_single[:, 1], ps_single[:, 2])
#     im_ax.set_ylim(int(y1), int(y2))
#     im_ax.set_xlim(int(x1), int(x2))
#     im_ax.invert_yaxis()
#     plt.savefig(self.context.camera_to_lidar_images + "/camera_to_lidar_" + str(self.df.loc[idx, "timestampRadar"]) + "_" + str(p_idx) + ".png")


result = []

# for tmp_p in pts_extracted_from_pc[:, :3]:
#     x = tmp_p
#
#     u = start_point
#     v = end_point
#
#     n = v - u
#     n /= np.linalg.norm(n, 2)
#
#     P = u + n * np.dot(x - u, n)
#     result.append(P)

# for tmp_p in pts_extracted_from_pc[:, :3]:
#     x = tmp_p
#
#     u1 = start_point
#     v1 = end_point
#
#     n1 = v1 - u1
#     n1 /= np.linalg.norm(n1, 2)
#
#     P = u1 + n1 * np.dot(x - u1, n1)
#     result.append(P)


# def func1(x, a, b, c):
#     # x[x < c] = np.inf
#     return a * (x - c) + b
#
# def func2(x, a, b, c):
#     # x[x < c] = np.inf
#     return (-1/a) * (x - c) + b
#
# def func(x, a, b, c):
#     fr = func1(x, a, b, c)
#     gr = func2(x, a, b, c)
#     hr = np.hstack([fr, gr])
#     return hr


# def read_data(self, idx):
#     rgb_path = str(self.df.loc[idx, 'base_path']) + "/camera_data/camera_rgb/camera_rgb_" + str(self.df.loc[idx, 'timestampCamera']) + ".png"
#     pc_path = str(self.df.loc[idx, 'base_path']) + "/lidar_data/lidar_pc/lidar_pc_" + str(self.df.loc[idx, 'timestampLidar'])[:-4] + ".npz"
#     lidar_seg_path = str(self.df.loc[idx, 'base_path']) + "/lidar_labels/lidar_seg/lidar_seg_" + str(self.df.loc[idx, 'timestampLidar'])[:-4] + ".npz"
#
#     radar_4dfft_path = str(self.df.loc[idx, 'base_path']) + "/radar_data/cuda_4dfft/cuda_4dfft_" + str(self.df.loc[idx, "timestampRadar"]) + ".csv"
#     radar_delta_pc_path = str(self.df.loc[idx, 'base_path']) + "/radar_data/cuda_delta/cuda_delta_" + str(self.df.loc[idx, "timestampRadar"]) + ".csv"
#
#     camera_label = str(self.df.loc[idx, 'base_path']) + "/camera_labels/rectified_labels/camera_det_EfficientDetD7_" + str(self.df.loc[idx, "timestampCamera"]) + ".json"
#     d3vd_path = str(self.df.loc[idx, 'base_path']) + "/lidar_labels/lidar_3d/lidar_3d_" + str(self.df.loc[idx, "timestampLidar"])[:-4] + ".json"
#     # print("timestampLidar", str(self.df.loc[idx, "timestampLidar"])[:-4])
#     try:
#         self.d3vd_label = read_label(d3vd_path)
#
#         self.im = cv2.cvtColor(cv2.imread(rgb_path), cv2.COLOR_BGR2RGB)
#         self.pc_lidar = np.load(pc_path)['arr_0']
#         self.lidar_seg = np.load(lidar_seg_path)['arr_0']
#
#         mask = np.zeros((self.pc_lidar.shape[0], self.pc_lidar.shape[1] + 1), dtype=np.float32)
#         mask[:, :3] = self.pc_lidar
#         mask[:, -1] = self.lidar_seg
#         self.pc_lidar = mask
#
#         self.pc_lidar = self.pc_lidar[np.linalg.norm(self.pc_lidar, axis=-1) < config.LIDAR_MAX_RANGE]
#
#         self.camera_label_rect = read_label(camera_label)
#         for label_idx, cur_label in enumerate(self.camera_label_rect):
#             self.camera_label_rect[label_idx]["lidar_label_center"] = 0
#             self.camera_label_rect[label_idx]["lidar_label_max_iou"] = 0
#             self.camera_label_rect[label_idx]["lidar_label_idx_to_plot"] = 0
#
#         self.radar_4dfft = pd.read_csv(radar_4dfft_path)
#         # print(self.radar_4dfft.keys())
#         self.radar_4dfft = self.radar_4dfft.reset_index(drop=True)
#         self.radar_4dfft["total_power"] = db(
#             np.abs(self.radar_4dfft["value_real"].values * self.radar_4dfft["value_real"].values + self.radar_4dfft["power_im"].values * self.radar_4dfft["power_im"].values))
#         # self.radar_4dfft["noise"] = db(self.radar_4dfft["noise"].values)
#         self.radar_4dfft = self.radar_4dfft[(self.radar_4dfft["total_power"] - self.radar_4dfft["noise"]) > config.FFT_MIN_POWER]
#         self.radar_4dfft = self.radar_4dfft[np.abs(self.radar_4dfft["dop"]) > config.MIN_DOP_4DFFT]
#
#         self.radar_cuda_pc = pd.read_csv(radar_delta_pc_path)
#         self.radar_cuda_pc = self.radar_cuda_pc.reset_index(drop=True)
#         self.radar_cuda_pc["total_power"] = db(
#             np.abs(self.radar_cuda_pc["value_real"].values * self.radar_cuda_pc["value_real"].values + self.radar_cuda_pc["power_im"].values * self.radar_cuda_pc["power_im"].values))
#         # self.radar_cuda_pc["noise"] = db(self.radar_cuda_pc["noise"].values)
#         self.radar_cuda_pc = self.radar_cuda_pc[(self.radar_cuda_pc["total_power"] - self.radar_cuda_pc["noise"]) > config.DELTA_MIN_POWER]
#         self.radar_cuda_pc = self.radar_cuda_pc[np.abs(self.radar_cuda_pc["dop"]) > config.MIN_DOP_DELTA]
#
#         # vals = self.radar_4dfft["total_power"].values
#         # print("4d", len(self.radar_4dfft), "Max power", max(vals), "Min", min(vals))
#         # print("Delta", len(self.radar_cuda_pc), "Max power", max(vals), "Min", min(vals))
#     except Exception as e:
#         PrintException()
#         return False
#     return True


# self.colors_map = {"car": (0, 0, 255), "truck": (255, 206, 250),
#                    "bus": (100, 149, 237),
#                    "pedestrian": (255, 165, 0), "person": (255, 165, 0),
#                    "motorcycle": (216, 0, 155), }

# self.colors_map = {"car": (0, 0, 255), "truck": (255, 206, 250),
#                    "bus": (100, 149, 237),
#                    "motorcycle": (216, 0, 155), }


# pad = 30
# if label["cat"] == "car" or label["cat"] == "vehicle":
#     # pad_y = int(self.im.shape[0]//10)
#     # pad_x = int(self.im.shape[1]//10)
#     cut_im = self.im[max(0, int(y1) - pad_y):min(int(y2) + pad_y, self.im.shape[0] - 1), max(0, int(x1) - pad_x):min(int(x2) + pad_x, self.im.shape[1] - 1)]
#     print(cut_im.shape)
#     cv2.imwrite(self.context.camera_to_lidar_instances + "/" + str(p_idx) + "_instance_cut_" + str(self.df.loc[idx, "timestampLidar"])[:-4] + ".png", cut_im[..., ::-1])


# imgfov_pc_pixel, imgfov_pc_velo, pts_velo, inds = preprocess_lidar_to_image(pts=self.pc_lidar, im=self.im, config=config, calibration_params=spatial_calibration_params)
# process_lidar_to_image_on_ax(pts_velo=pts_velo, imgfov_pc_pixel=imgfov_pc_pixel, imgfov_pc_velo=imgfov_pc_velo, inds=inds, im=self.im, config=config, ax=orig_im_ax)


# todo split by range
# print("try splitting by range")
# ranges = [(0, 30), (30, 2000)]
# pcs = []
# for rng_min, rng_max in ranges:
#     print("try", rng_min, rng_max)
#     WiseSDK.DSPConfiguration().dict()
#     dsp_configuration["ALGM"]["RNG_MIN"] = rng_min
#     dsp_configuration["ALGM"]["RNG_MAX"] = rng_max
#     WiseSDK.setDSPParameters(dsp_configuration, server_id)
#     time.sleep(20)
#     point_cloud = WiseSDK.processFrame(radar_frame, WiseSDK.DATA_TYPE_E.POINT_CLOUD_DATA_TYPE)
#     time.sleep(2)
#     point_cloud = WiseSDK.processFrame(radar_frame, WiseSDK.DATA_TYPE_E.POINT_CLOUD_DATA_TYPE)
#     PointCloud = self.orderData(point_cloud)
#     pcs.append(PointCloud)
#
# df = pd.DataFrame()
# for cur_p in pcs:
#     df = df.append(cur_p)
#
# frame_path = self.getRadarSavePath(point_cloud.getDataType(), str(dfData.loc[i, 'timestampRadar']), img_alg_type=img_alg_type)
# if img_alg_type == 2:
#     df = df.drop(columns=["cluster_id", "x_global", "y_global", "trk_id", "vx", "vy", "vz", "trk_extrapolation_flag"])
# df.to_csv(frame_path, index=False)


# if self.args.radar4d and self.args.radarPc:
#     sdk.apply_velocity_correction(self.df_data, img_alg_type=0)
#     sdk.apply_velocity_correction(self.df_data, img_alg_type=2)

# if self.args.radar or self.args.radarPc or self.args.radar4d:
#     do_flag = False
#
#     if self.args.radar and self.check_if_do(self.context.radar_sant_save_path):
#         do_flag = True
#
#     if self.args.radarPc and self.check_if_do(self.context.cuda_delta):
#         do_flag = True
#
#     if do_flag or self.args.force_pc:
#         IMG_ALG_TYPE = 0
#         # dsp(self.df_data, self.args.force, imaging_flag=self.args.radarPc, s_ant=self.args.radar, settings=self.context, img_alg_type=IMG_ALG_TYPE)
#         sdk.dsp(self.df_data, self.args.force, imaging_flag=self.args.radarPc, s_ant=self.args.radar, img_alg_type=IMG_ALG_TYPE)
#
#     if self.args.radar4d and self.check_if_do(self.context.cuda_4dfft) or self.args.force_4d:
#         print("4DFFT")
#         IMG_ALG_TYPE = 2
#         time.sleep(2)
#         WiseSDK_kill()
#         time.sleep(2)
#         WiseSDK_init()
#         # dsp(self.df_data, self.args.force, imaging_flag=True, s_ant=False, settings=self.context, img_alg_type=IMG_ALG_TYPE)
#
#         sdk.dsp(self.df_data, self.args.force, imaging_flag=True, s_ant=False, img_alg_type=IMG_ALG_TYPE)
#
#     # fix velocities


# def fix_key_to_unique(dfs_to_fix, keys):
#     # Iterate over each Key
#     for key in keys:
#         print("Processing Key = ", key)
#         total_unique = []
#         for pc in pcs:
#             if key not in pc.keys(): return pd.concat(dfs_to_fix, ignore_index=True)
#             unique_vals = get_unique_vals(pc[key])
#             print("Unique values", unique_vals)
#             total_unique.append(len(unique_vals))
#         # total_unique = np.cumsum(np.array(total_unique))
#         # print("total unique", total_unique)
#         new_indices = np.arange(1, np.sum(total_unique) + 1)
#         print("len new_indices", len(new_indices), new_indices.shape, new_indices)
#         for pc_idx, pc in enumerate(pcs[1:]):
#             cur_uniqe_vals = get_unique_vals(pc[key])
#             for un in cur_uniqe_vals:
#                 if un == -1:
#                     continue
#                 results = np.where(np.isclose(cur_uniqe_vals, un))[0]
#                 if len(results) > 1:
#                     print("Too many indices?!?!?!?")
#
#                 print("results", results)
#                 print("results[0]", results[0])
#                 print("new_indices[results[0]]", new_indices[results[0]])
#                 pc.loc[pc[key] == un, key] = new_indices[results[0]]
#
#     merged_pc_ = pd.concat(pcs, ignore_index=True)
#     for key in keys:  print(key, pd.unique(merged_pc_[key]))
#
#     return merged_pc_


# first_figure = {}
# second_figure = {"tp": True, "fp": True, "fn": True}
# for met in metrices:
#     if met not in second_figure:
#         first_figure[met] = True
#
# n_cols = 2
# n_rows = len(classes)
# plot_idx = 1
# width = 0.2
#
# legend_flag = True
# for cls_idx, cls in enumerate(sorted(classes)):
#     for g_idx, graph in enumerate([sorted(list(first_figure.keys()), reverse=True), sorted(list(second_figure.keys()))]):
#         x = np.arange(len(graph))
#         ax = plt.subplot(n_rows, n_cols, plot_idx)
#         num_maps = len(df_map)
#         if num_maps == 1:
#             margin = [0]
#         else:
#             margin = np.linspace(-width, width, len(df_map))
#
#         for model_idx, (model_name, df) in enumerate(df_map.items()):
#             if cls in list(df.keys()):
#                 rects1 = ax.bar(x - margin[model_idx], df.loc[graph, cls], width, label=model_name)
#
#         ax.set_ylabel('score' if g_idx == 0 else "count")
#         ax.set_title('Metrices Scores: ' + str(cls))
#         ax.set_xticks(x)
#         ax.set_xticklabels(graph)
#         if g_idx == 0:
#             ax.set_ylim(0, 1)
#             ax.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
#         plot_idx += 1
# plt.tight_layout(pad=3)
# plt.savefig(out_path + "/results.png")
# plt.close()

# def plot_tp_fp_fn(self):
#     plot_idx = 1
#     width = 0.2
#
#     legend_flag = True
#     for cls_idx, cls in enumerate(sorted(self.classes)):
#         plt.figure(figsize=(10, 10))
#         x = np.arange(len(graph))
#         num_maps = len(df_map)
#         if num_maps == 1:
#             margin = [0]
#         else:
#             margin = np.linspace(-width, width, len(df_map))
#
#         for model_idx, (model_name, df) in enumerate(df_map.items()):
#             if cls in list(df.keys()):
#                 rects1 = ax.bar(x - margin[model_idx], df.loc[graph, cls], width, label=model_name)
#
#         ax.set_ylabel('score' if g_idx == 0 else "count")
#         ax.set_title('Metrices Scores: ' + str(cls))
#         ax.set_xticks(x)
#         ax.set_xticklabels(graph)
#         ax.set_ylim(0, 1)
#         ax.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
#     plt.tight_layout(pad=3)
#     plt.savefig(self.out_path + "/tpfn_" + str(cls) + ".png")
#     plt.close()
#     return


# seg = engine.get_segmentation_label(idx=engine_idx)
# imgfov_pc_pixel, imgfov_pc_velo = engine.process_lidar_on_image(lidar, im)
#
# boolean_mask = seg.get_mask_of_points(imgfov_pc_pixel, cls=Road())
#
# imgfov_pc_pixel_road = imgfov_pc_pixel[:, boolean_mask]
# imgfov_pc_velo_road = imgfov_pc_velo[boolean_mask]

# imgfov_pc_pixel_road_not = imgfov_pc_pixel[:, ~boolean_mask]
# imgfov_pc_velo_road_not = imgfov_pc_velo[~boolean_mask]