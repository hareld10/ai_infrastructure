# module to compute radar params from camera image


import numpy as np
import pandas as pd
import os
import json
from pprint import pprint
# import data_analysis.vision.CameraCalibrator as c

HOME = os.path.expanduser('~')


class Camera():
    """Class to define a camera object.
    Will provide the camera matrix K, camera distortion D, and rotation R from camera to real (radar) coordinates.
    """

    def __init__(self, K, D, camera="60", tx=0, ty=-0.05, tz=0):
        self.cam = camera
        self.K = K
        self.D = D
        # pprint(self.K)
        # pprint(self.D)
        # self.K, self.D = c.loadParams(os.path.join(HOME,
        #                                            'workspace/wise_sdk/SDK/python/camera/cam' + camera + '_calibration_b0.json'))  # camera intrinsic matrix
        self.R = rotateCam()  # rotation matrix from camera to radar coordinates
        self.t = np.array(
            [[tx], [ty], [tz]])  # [x_t,y_t,z_t] in [m]; translation vector from radar to camera (camera coordinates)

    def set_t(self, tx, ty, tz):
        self.t = np.array(
            [[tx], [ty], [tz]])  # [x_t,y_t,z_t] in [m]; translation vector from radar to camera (camera coordinates)

    def set_rotation(self, yaw=0, pitch=0, roll=0):
        # print("Updating R", yaw, pitch, roll)
        self.R = rotateCam(yaw=yaw, pitch=pitch, roll=roll)
        return

    def Cart2Image(self, x, y, z, D=None):
        """Convert radar (r,az,el) to image pixel coordinates (u,v)
        """
        # print("x - ", x.shape, x.dtype, np.random.permutation(x)[:10])
        # ------ projection matrix from camera xyz into pixels u-v    ----------%
        n = 1 if np.isscalar(x) else x.shape[0]  # number of points
        X = np.array([x, y, z, np.ones(n)])
        # ------ rotation and translation matrix from radar to camera ----------%
        T = np.concatenate((self.R, self.t), axis=1)

        # -----  rotate translate and project point cloud from radar to camera system  ----%
        # first we rotate and translate
        Xc = np.dot(T, X)  # [m]
        # print("Xc ", Xc.shape,   np.random.permutation(Xc)[:10])
        # Then we normalize by distance (z_cam=y_rad)
        Xc2 = Xc / Xc[2, :]
        # print("Xc2 ", Xc2.shape,  np.random.permutation(Xc2)[:10])
        # ----- distortion correction
        if D is not None:
            D = D.squeeze()
            r2 = Xc2[0, :] ** 2 + Xc2[1, :] ** 2
            if D.size > 5:
                corr1 = (1 + D[0] * r2 + D[1] * r2 ** 2 + D[4] * r2 ** 3) / (
                            1 + D[5] * r2 + D[6] * r2 ** 2 + D[7] * r2 ** 3)
            if D.size == 5:
                corr1 = (1 + D[0] * r2 + D[1] * r2 ** 2 + D[4] * r2 ** 3)
            # print("correction for distortion...")
            Xc2[0, :] = Xc2[0, :] * corr1 + 2 * D[2] * Xc2[0, :] * Xc2[1, :] + D[3] * (r2 + 2 * Xc2[0, :] ** 2)
            Xc2[1, :] = Xc2[1, :] * corr1 + 2 * D[3] * Xc2[0, :] * Xc2[1, :] + D[2] * (r2 + 2 * Xc2[1, :] ** 2)

        # ----- then we multiply by camera matrix to transfer from camera to image coordinates
        Xc2 = np.nan_to_num(Xc2, nan=0, posinf=0, neginf=0)
        U = np.dot(self.K, Xc2).astype(int)
        return U[:2]  # third element is just 1 by definition

    def Radar2Image(self, r, az, el, D=None):
        """Convert radar (r,az,el) to image pixel coordinates (u,v)
        """
        r, az, el = [np.array(x) for x in (r, az, el)]  # make sure an np array
        x, y, z = toXYZ(r, az, el)
        # ------ projection matrix from camera xyz into pixels u-v    ----------%
        # R, RU, RV

        n = 1 if np.isscalar(x) else x.shape[0]  # number of points
        X = np.array([x, y, z, np.ones(n)])
        # ------ rotation and translation matrix from radar to camera ----------%
        T = np.concatenate((self.R, self.t), axis=1)

        # -----  rotate translate and project point cloud from radar to camera system  ----%
        # first we rotate and translate
        Xc = np.dot(T, X)  # [m]
        # Then we normalize by distance (z_cam=y_rad)
        Xc2 = Xc / Xc[2, :]

        # ----- distortion correction
        if D is not None:
            D = D.squeeze()
            r2 = Xc2[0, :] ** 2 + Xc2[1, :] ** 2
            if D.size > 5:
                corr1 = (1 + D[0] * r2 + D[1] * r2 ** 2 + D[4] * r2 ** 3) / (
                            1 + D[5] * r2 + D[6] * r2 ** 2 + D[7] * r2 ** 3)
            if D.size == 5:
                corr1 = (1 + D[0] * r2 + D[1] * r2 ** 2 + D[4] * r2 ** 3)
            # print("correction for distortion...")
            Xc2[0, :] = Xc2[0, :] * corr1 + 2 * D[2] * Xc2[0, :] * Xc2[1, :] + D[3] * (r2 + 2 * Xc2[0, :] ** 2)
            Xc2[1, :] = Xc2[1, :] * corr1 + 2 * D[3] * Xc2[0, :] * Xc2[1, :] + D[2] * (r2 + 2 * Xc2[1, :] ** 2)

        # ----- then we multiply by camera matrix to transfer from camera to image coordinates
        Xc2 = np.nan_to_num(Xc2, nan=0, posinf=0, neginf=0)
        U = np.dot(self.K, Xc2).astype(int)
        return U[:2]  # third element is just 1 by definition

    def Image2Radar(self, u, v, Dist):
        """Convert image (u,v) coordinates to radar (Range, Azimuth, Elevation) coordinates based on approximate distance.
        Dist should be a distance vector in [m] for each (u,v) coordinate.
        """
        if np.isscalar(u):
            U = np.array([u, v, 1])
        else:
            u, v, Dist = [np.array(x) for x in (u, v, Dist)]  # make sure an np array and not list
            U = np.array([u, v, np.ones(u.shape[0])])

        # --------using camera matrix K transform from image to (normalized) camera coordinates-------%
        Xc = np.linalg.inv(self.K).dot(U)
        # to real [m] units
        Xc_m = Xc * Dist

        # ---- rotate by R and translate by t from camera to radar coordinates -----------%
        Xr = np.linalg.inv(self.R).dot(Xc_m - self.t)

        # return Xr
        R, Az, El = toRAzEl(Xr[0, :], Xr[1, :], Xr[2, :])
        return R, Az, El

    def Image2Cart(self, u, v, Dist):
        """Convert image (u,v) coordinates to radar (Range, Azimuth, Elevation) coordinates based on approximate distance.
        Dist should be a distance vector in [m] for each (u,v) coordinate.
        """
        if np.isscalar(u):
            U = np.array([u, v, 1])
        else:
            u, v, Dist = [np.array(x) for x in (u, v, Dist)]  # make sure an np array and not list
            U = np.array([u, v, np.ones(u.shape[0])])

        # --------using camera matrix K transform from image to (normalized) camera coordinates-------%
        Xc = np.linalg.inv(self.K).dot(U)
        # to real [m] units
        Xc_m = Xc * Dist

        # ---- rotate by R and translate by t from camera to radar coordinates -----------%
        Xr = np.linalg.inv(self.R).dot(Xc_m - self.t)

        return Xr[0, :], Xr[1, :], Xr[2, :]

    def getCameraObjectsParams(self, imjfile, w=2):
        """extract Az/El/Range parameters for objects detected in camera images
        imjfile: json file of image (yolo) detected objects
        w: [m] typical width of car in real units
        """
        # read detection file
        detections = pd.read_json(imjfile)
        n = len(detections)
        if n == 0:
            return None

        fx = self.K[0, 0]
        u1, v1, u2, v2 = np.array(detections['bbox'].values.tolist()).T

        # measure distance to camera objects based on the triangle method
        w_all = (detections['category'] == 'car') * w \
                + (detections['category'] == 'bus') * w * 1.7 \
                + (detections['category'] == 'truck') * w * 1.6 \
                + (detections['category'] == 'person') * w * 0.31 \
                + (detections['category'] == 'motorbike') * w * 0.35 \
                + (detections['category'] == 'bicycle') * w * 0.31

        Dist = camera_distance(np.abs(u2 - u1), fx, W=w_all)
        Dist[Dist == 0] = np.nan

        # convert image coordinates to spherical (radar) coordinates
        R1, Az1, El1 = self.Image2Radar(u1, v1, Dist)
        R2, Az2, El2 = self.Image2Radar(u2, v2, Dist)
        err = lambda x: np.abs(x[0] - x[1]) / 2
        [dR, dAz, dEl] = [err(x) for x in ([R1, R2], [Az1, Az2], [El1, El2])]
        u = (u1 + u2) / 2
        v = (v1 + v2) / 2
        R, Az, El = self.Image2Radar(u, v, Dist)

        # convert image coordinates to cartesian coordinates
        x1, y1, z1 = toXYZ(R - 10, Az1, El1)
        x2, y2, z2 = toXYZ(R - 10, Az2, El1)
        x3, y3, z3 = toXYZ(R + 10, Az2, El2)
        x4, y4, z4 = toXYZ(R + 10, Az1, El2)

        # return as pandas DF
        values = pd.DataFrame(
            {'Range': R, 'dRange': dR, 'Az': Az, 'dAz': dAz, 'El': El, 'dEl': dEl, 'u': u, 'v': v, 'du': u2 - u1,
             'dv': v2 - v1, }, index=detections.index)  # make a dataframe
        values['xybox'] = np.array(
            [[x1, y1], [x2, y2], [x3, y3], [x4, y4]]).T.tolist()  # image bbox to radar-frame xy wedge
        detections = pd.concat([detections, values], axis=1)
        return detections


def getCameraObjectsParams3D(imjfile, model=None):
    """extract Az/El/Range parameters for objects detected in camera images
    imjfile: json file of image (yolo) detected objects
    w: [m] typical width of car in real units
    """
    with open(imjfile) as f:
        detections = json.load(f)
    detections = pd.DataFrame(detections)
    n = len(detections)
    if n == 0:
        return None
    u1, v1, u2, v2 = np.array(pd.DataFrame(detections)['bbox'].values.tolist()).T
    u = (u1 + u2) / 2
    v = (v1 + v2) / 2

    # convert image coordinates to spherical (radar) coordinates
    x, y, z = np.array(pd.DataFrame(detections)['loc'].values.tolist()).T
    dy, dx, dz = np.array(pd.DataFrame(detections)['dim'].values.tolist()).T
    R = rotateCam()
    Xr = (np.linalg.inv(R).dot(np.array([x, y, z])))
    dXr = np.abs((np.linalg.inv(R).dot(np.array([dx, dy, dz]))))
    R, Az, El = toRAzEl(Xr[0], Xr[1], Xr[2])
    R1, Az1, El1 = toRAzEl(Xr[0] - dXr[0] / 2, Xr[1] - dXr[1] / 2, Xr[2] - dXr[2] / 2)
    R2, Az2, El2 = toRAzEl(Xr[0] + dXr[0] / 2, Xr[1] + dXr[1] / 2, Xr[2] + dXr[2] / 2)
    # model correction
    if model is not None:
        model = np.poly1d(model.c)
        [R, R1, R2] = [model(x) for x in (R, R1, R2)]
    err = lambda x: np.abs(x[0] - x[1]) / 2
    [dR, dAz, dEl] = [err(x) for x in ([R1, R2], [Az1, Az2], [El1, El2])]

    # convert image coordinates to spherical (radar) coordinates

    # return as pandas DF
    values = pd.DataFrame(
        {'R': R, 'dR': dR, 'Az': Az, 'dAz': dAz, 'El': El, 'dEl': dEl, 'u': u, 'v': v})  # make a dataframe
    detections = pd.concat([detections, values], axis=1)
    return detections


##### radar/camera projection methods

def toXYZ(r, Az, El):
    """Spherical (range azimuth elevation) to cartesian (x,y,z) coordinates
    """
    u = np.sin(np.deg2rad(Az))  # u == cos(phi)sin(theta)
    v = np.sin(np.deg2rad(El))  # v == sin(phi)
    # following Kuti's definition
    x = r * u
    y = r * np.sqrt(1 - u ** 2 - v ** 2)
    z = r * v
    return (x, y, z)


def toRAzEl(x, y, z):
    """Cartesian (x,y,z) to spherical (range azimuth elevation) coordinates
    """
    r = np.sqrt(x ** 2 + y ** 2 + z ** 2)
    # Following Kuti's definition
    Az = np.rad2deg(np.arcsin(x / r))  # Az
    El = np.rad2deg(np.arcsin(z / r))  # phi
    return (r, Az, El)


def cart2spher(x, y, z):
    r = np.sqrt(x ** 2 + y ** 2 + z ** 2)
    theta = np.rad2deg(x / y)
    phi = np.rad2deg(np.arccos(np.sqrt(x ** 2 + y ** 2) / r))
    return r, theta, phi


def spher2cart(r, theta, phi):
    x = r * np.cos(np.deg2rad(phi)) * np.sin(np.deg2rad(theta))
    y = r * np.cos(np.deg2rad(phi)) * np.cos(np.deg2rad(theta))
    z = r * np.sin(np.deg2rad(phi))
    return x, y, z


def angles2Az(theta, phi):
    Az = np.rad2deg(np.arcsin(np.cos(np.deg2rad(phi)) * np.sin(np.deg2rad(theta))))
    El = phi
    return Az, El


def Az2angles(Az, El):
    phi = El
    theta = np.rad2deg(np.arcsin(np.sin(np.deg2rad(Az)) / np.cos(np.deg2rad(phi))))
    return theta, phi


def rotateCam(yaw=0, pitch=0, roll=0):
    """Radar to Camera rotation matrix
    """
    # ------ coordinate system rotation from radar to camera -----------
    # R_cord = np.array([ [0, -1,  0], # x_camera  =  -y_radar
    #                         [0,  0, -1], # y_camera  = -z_radar
    #                         [1,  0,  0] ])# z_camera  =  x_radar
    # Following Kuti's definition
    R_cord = np.array([[1, 0, 0],  # x_camera  =  x_radar
                       [0, 0, -1],  # y_camera  = -z_radar
                       [0, 1, 0]])  # z_camera  =  y_radar

    Rrot = rotate(Yaw_rot=yaw, Pitch_rot=pitch, Roll_rot=roll)
    R = np.dot(Rrot, R_cord)  # total rotation matrix
    return R


def rotate(Yaw_rot=0, Pitch_rot=0, Roll_rot=0):
    """Coordinate system rotation matrix using Euler angles
    """
    # ------ total rotation from radar to camera -----------
    theta_x = Yaw_rot;
    # We define the Yaw (elevation) rotation as *counter clockwise rotation*
    # (camera sustem) (unit matrix, no rotation)
    Rx = np.array([[1, 0, 0],
                   [0, np.cos(np.deg2rad(theta_x)), -np.sin(np.deg2rad(theta_x))],
                   [0, np.sin(np.deg2rad(theta_x)), np.cos(np.deg2rad(theta_x))]])
    theta_y = -Pitch_rot;
    # We define the Pitch (azimuth) rotation as *clockwise rotation*
    # (camera sustem) (unit matrix, no rotation)
    Ry = np.array([[np.cos(np.deg2rad(theta_y)), 0, -np.sin(np.deg2rad(theta_y))],
                   [0, 1, 0],
                   [np.sin(np.deg2rad(theta_y)), 0, np.cos(np.deg2rad(theta_y))]])
    theta_z = Roll_rot;  # (unit matrix, no rotation)
    Rz = np.array([[np.cos(np.deg2rad(theta_z)), -np.sin(np.deg2rad(theta_z)), 0],
                   [np.sin(np.deg2rad(theta_z)), np.cos(np.deg2rad(theta_z)), 0],
                   [0, 0, 1]])
    R = np.dot(np.dot(Rx, Ry), Rz)  # total rotation matrix
    return R


#############
def camera_distance(dx, f, W=2):
    """Compute distance to object based on real object width W [m].
    dx: object width in pixel units
    f: focal length in pixel units
    W: object with in real units [m]
    return distance in [m]
    """
    D = W * f / dx
    return D


##############
def FOV(cam, dim=[1824, 940], Dist=100):
    """Compute camera FOV (at distance Dist)"""
    # dim = np.array(unimg.shape[:2][::-1])
    camObj = Camera(cam)
    R, Az, El = camObj.Image2Radar([0, 1824], [0, 940], [Dist, Dist])

    az_fov = np.sum(np.abs(Az))
    el_fov = np.sum(np.abs(El))
    return az_fov, el_fov

##############

def FOV_obj(camObj, dim=[1824, 940], Dist=100):
    """Compute camera FOV (at distance Dist)"""
    # dim = np.array(unimg.shape[:2][::-1])
    R, Az, El = camObj.Image2Radar([0, 1824], [0, 940], [Dist, Dist])

    az_fov = np.sum(np.abs(Az))
    el_fov = np.sum(np.abs(El))
    return az_fov, el_fov