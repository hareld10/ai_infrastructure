import pickle

from PyQt5.QtWidgets import QWidget, QVBoxLayout, QFileDialog, QGroupBox, QFormLayout, QCheckBox, QDoubleSpinBox, \
    QPushButton, QGridLayout, QLabel, QHBoxLayout, QProxyStyle, QStyle
import cv2
import os
import numpy as np
import PyQt5
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from matplotlib import gridspec
import matplotlib.cm as cmx
from tqdm import tqdm
from Calibrator.calibrator_utils import project_to_image, PrintException, db
import json
import sys
# from scipy.constants import speed_of_light
import pandas as pd
import inspect
from pprint import pprint
import torch
import torch.nn.functional as F
from Calibrator.projection import Camera, toRAzEl, toXYZ
from Calibrator.calibrator_utils import detect_peaks, get_rotation_matrix, filter_points_out_fov, lidar_to_radar_coordinates

plt.style.use('dark_background')

MIN = 0
MAX = 0
NUM_BINS = 2

RADAR_PARAM = "radar_param_point_cloud.pkl"
# RADAR_PARAM = "radar_param.pkl"

POINT_SIZE = "point-size"

# CAMERA <-> LIDAR
TX_CAMERA_LIDAR = "tx-lidar-camera"
TY_CAMERA_LIDAR = "ty-lidar-camera"
TZ_CAMERA_LIDAR = "tz-lidar-camera"

YAW_CAMERA_LIDAR = "yaw-lidar-camera"
ROLL_CAMERA_LIDAR = "roll-lidar-camera"
PITCH_CAMERA_LIDAR = "pitch-lidar-camera"

ELEVATION_LIMIT = "elev-lim"

# CAMERA <-> RADAR
TZ_CAMERA_RADAR = "tz-camera-radar"
TY_CAMERA_RADAR = "ty-camera-radar"
TX_CAMERA_RADAR = "tx-camera-radar"

YAW_CAMERA_RADAR = "yaw-camera-radar"
ROLL_CAMERA_RADAR = "roll-camera-radar"
PITCH_CAMERA_RADAR = "pitch-camera-radar"

# LIDAR <-> RADAR
TZ_RADAR_LIDAR = "tz-radar-lidar"
TY_RADAR_LIDAR = "ty-radar-lidar"
TX_RADAR_LIDAR = "tx-radar-lidar"

YAW_RADAR_LIDAR = "yaw-radar-lidar"
ROLL_RADAR_LIDAR = "roll-radar-lidar"
PITCH_RADAR_LIDAR = "pitch-radar-lidar"

LIDAR_PC = "lidar-pc"
RADAR_DELTA = "radar-delta"
RADAR_FFT = "radar-FFT"

DELTA_MARKER = "x"
FFT_MARKER = "^"
LIDAR_MARKER = "o"

DCM_LIDAR = "DCM-lidar"
DF_IDX = "idx"
RECTIFY_IMG = "rectify-img"
CA_CFAR = "CA-CFAR"
AVG_FRAMES = "AVG_FRAMES"


class MplCanvas(FigureCanvasQTAgg):
    def __init__(self, parent=None, width=20, height=10, dpi=100):
        # self.width, self.height, self.dpi = width, height, dpi
        # self.fig = Figure(figsize=(self.width, self.height), dpi=self.dpi)
        self.fig = Figure(figsize=(width, height))
        super(MplCanvas, self).__init__(self.fig)
        self.axes = None
        self.updated_key = None
        self.calculated_ffta = None
        self.cfar_results = None
        self.mpl_connect("button_press_event", self.on_press)
        # self.axes = plt.gca()
        self.jet = plt.get_cmap('jet')
        self.autumn = plt.get_cmap('autumn')
        self.winter = plt.get_cmap('winter')
        self.cool = plt.get_cmap('cool')
        self.cb = None
        self.point_size = None
        self.cNorm = None
        self.max_range = 0
        self.refresh_fft = True
        self.pad_el, self.pad_az, self.az_bin, self.el_bin, self.min_az_ind, self.max_az_ind, self.min_el_ind, self.max_el_ind = None, None, None, None, None, None, None, None
        self.az_min, self.el_min = None, None
        self.radar_r_u_v = None
        self.parent = parent
        self.camera_radar = Camera(K=self.parent.params.get_K_matrix(), D=self.parent.params.get_D_matrix())
        self.camera_lidar = Camera(K=self.parent.params.get_K_matrix(), D=self.parent.params.get_D_matrix())
        self.history = {"theta": None}
        self.ch_order = np.array(
            [[0, 4], [1, 5], [0, 6], [1, 7], [0, 8], [1, 9], [0, 10], [1, 11], [0, 12], [1, 13], [0, 14], [1, 15],
             [0, 16], [1, 17], [0, 18],
             [1, 19], [2, 2], [3, 3], [2, 4], [3, 5], [2, 6], [3, 7], [2, 8], [3, 9], [2, 10], [3, 11], [2, 12],
             [3, 13], [2, 14], [3, 15],
             [2, 16], [3, 17], [4, 0], [5, 1], [4, 2], [5, 3], [4, 4], [5, 5], [4, 6], [5, 7], [4, 8], [5, 9], [4, 10],
             [5, 11], [4, 12], [5, 13],
             [4, 14], [5, 15]])

        fov_az = 110  # deg
        self.min_az_fov = -fov_az // 2
        self.max_az_fov = fov_az // 2

        fov_el = 40  # deg
        self.min_el_fov = -fov_el // 2
        self.max_el_fov = fov_el // 2

        self.gpus_list = [0]
        # self.update_fig()
        self.update_cfar_flag = False

    def undistort(self, im):
        K = self.parent.params.get_K_matrix()
        D = self.parent.params.get_D_matrix()
        dim = im.shape[:2][::-1]

        centerPrinicipalPoint = True
        new_K, roi = cv2.getOptimalNewCameraMatrix(K, D, dim, 0, dim, True)
        mapx, mapy = cv2.initUndistortRectifyMap(K, D, None, new_K, dim, cv2.CV_32FC1)
        undistorted_img = cv2.remap(im.copy(), mapx, mapy, cv2.INTER_LINEAR)
        return undistorted_img

    def project_velo_to_cam2(self, ):
        # swap axes =
        swap_axes = np.array(
            [[0, -1, 0, 0],
             [0, 0, -1, 0],
             [1, 0, 0, 0],
             [0, 0, 0, 1]])

        # P_velo2cam_ref = np.vstack(
        #     (self.parent.params.get_Tr_velo_to_cam().reshape(3, 4), np.array([0., 0., 0., 1.])))  # velo2ref_cam

        R_ref2rect = np.eye(4)

        # R0_rect = [""]
        # R0_rect = calib['R0_rect'].reshape(3, 3)  # ref_cam2rect
        # R_ref2rect[:3, :3] = R0_rect
        P_rect2cam2 = np.zeros((3, 4), dtype=np.float)
        P_rect2cam2[:, :3] = self.parent.params.get_K_matrix()
        # P_rect2cam2 = calib['P2'].reshape((3, 4))
        proj_mat = P_rect2cam2 @ R_ref2rect @ swap_axes  # @ P_velo2cam_ref
        return proj_mat

    def on_press(self, event):
        if not event.dblclick:
            return
        self.parent.params.params_values["image_w_x"].set_val(event.xdata)
        self.parent.params.params_values["image_h_y"].set_val(event.ydata)

    def get_pc(self, im):
        pts_velo = self.cut_by_range(self.parent.data.pc_lidar)
        # print("pts_velo", pts_velo.shape)
        elev_level = self.parent.params.params_values[ELEVATION_LIMIT].get_value()
        if elev_level:
            pts_velo = pts_velo[pts_velo[:, 2] > elev_level]

        pts_velo_lidar = pts_velo.copy()

        rot = get_rotation_matrix(theta_z=np.deg2rad(self.parent.params.params_values[YAW_RADAR_LIDAR].get_value()),
                                  theta_x=np.deg2rad(self.parent.params.params_values[PITCH_RADAR_LIDAR].get_value()),
                                  theta_y=np.deg2rad(self.parent.params.params_values[ROLL_RADAR_LIDAR].get_value()))
        # print("rot", rot.shape)
        camera_lidar_mat = np.zeros((3, 4), dtype=np.float32)
        camera_lidar_mat[:, :3] = rot
        camera_lidar_mat[:, 3] = np.array([self.parent.params.params_values[TX_RADAR_LIDAR].get_value(),
                                           self.parent.params.params_values[TY_RADAR_LIDAR].get_value(),
                                           self.parent.params.params_values[TZ_RADAR_LIDAR].get_value()])

        pts_velo_lidar = np.vstack((pts_velo_lidar.T, np.ones((1, pts_velo.shape[0]))))
        pts_velo_lidar = (camera_lidar_mat @ pts_velo_lidar).T
        pts_velo_lidar = self.filter_fov_az(pts_velo_lidar)

        rot = get_rotation_matrix(theta_z=np.deg2rad(self.parent.params.params_values[YAW_CAMERA_LIDAR].get_value()),
                                  theta_x=np.deg2rad(self.parent.params.params_values[PITCH_CAMERA_LIDAR].get_value()),
                                  theta_y=np.deg2rad(self.parent.params.params_values[ROLL_CAMERA_LIDAR].get_value()))
        # print("rot", rot.shape)
        camera_lidar_mat = np.zeros((3, 4), dtype=np.float32)
        camera_lidar_mat[:, :3] = rot
        camera_lidar_mat[:, 3] = np.array([self.parent.params.params_values[TX_CAMERA_LIDAR].get_value(),
                                           self.parent.params.params_values[TY_CAMERA_LIDAR].get_value(),
                                           self.parent.params.params_values[TZ_CAMERA_LIDAR].get_value()])

        pts_velo = np.vstack((pts_velo.T, np.ones((1, pts_velo.shape[0]))))
        pts_velo = (camera_lidar_mat @ pts_velo).T

        """
        Only if necesseary
        """
        # rotation_matrix = self.get_rotation_matrix()
        # pts_velo = np.matmul(rotation_matrix, pts_velo.T).T

        proj_velo2cam2 = self.project_velo_to_cam2()
        pts_2d = project_to_image(pts_velo.transpose(), proj_velo2cam2)

        imgfov_pc_pixel, imgfov_pc_velo, inds = filter_points_out_fov(im, pts_2d, pts_velo)

        # imgfov_pc_velo = pts_velo[inds, :]
        # imgfov_pc_velo = np.hstack((imgfov_pc_velo, np.ones((imgfov_pc_velo.shape[0], 1))))

        # imgfov_pc_cam2 = proj_velo2cam2 @ imgfov_pc_velo.transpose()

        return imgfov_pc_pixel, imgfov_pc_velo, pts_velo_lidar

    def reset(self):
        self.fig.clf()
        if self.cb != None:
            self.cb.remove()

    def get_nci(self, s_ant):
        nci = np.zeros([s_ant.shape[0], s_ant.shape[2]])
        for i in range(s_ant.shape[1]):
            nci += np.abs(s_ant[:, i, :])
        nci = np.fft.fftshift(nci, axes=(1,))
        cur_plot = self.db(nci).T
        h, w = cur_plot.shape[:2]

        ratio = max(int(w), int(h))
        cur_plot = cv2.resize(cur_plot, dsize=(ratio, ratio), interpolation=cv2.INTER_NEAREST)
        return cur_plot

    def apply_cfar(self, ffta):
        points = []
        total_num_of_points = 0
        noise_level = np.median(ffta)

        if not self.az_min:
            self.az_min = self.parent.data.radar_param["az"][0]
        if not self.el_min:
            self.el_min = self.parent.data.radar_param["el"][0]
        if not self.az_bin:
            self.az_bin = self.parent.data.bins["az"]
        if not self.el_bin:
            self.el_bin = self.parent.data.bins["el"]

        for el_level in range(ffta.shape[1]):
            ca_cfar_input = np.expand_dims(ffta[:, el_level, :], axis=0)
            peaks = detect_peaks(image=ca_cfar_input, peak_threshold=noise_level + self.parent.params.params_values["CA-CFAR"].get_value()).squeeze()
            indices = np.argwhere(peaks)
            cur_points = np.zeros((len(indices), 3), dtype=np.float32)  # Range Az El
            total_num_of_points += len(indices)

            cur_points[:, 0] = self.parent.data.radar_param["range"][0] + indices[:, 0] * self.parent.data.bins["range"]
            cur_points[:, 1] = np.rad2deg(np.arcsin(self.az_min)) + indices[:, 1] * np.rad2deg(np.arcsin(self.az_bin))
            cur_points[:, 2] = np.rad2deg(np.arcsin(self.el_min)) + el_level * np.rad2deg(np.arcsin(self.el_bin))

            points.append(cur_points)

        ret_points = np.zeros((total_num_of_points, 3), dtype=np.float32)

        start_idx, end_idx = 0, 0
        for p in points:
            start_idx = end_idx
            end_idx = start_idx + len(p)
            ret_points[start_idx:end_idx] = p

        return ret_points

    def get_image2d(self):
        if self.radar_r_u_v is None or self.updated_key == "Padding_Az" or self.updated_key == "Padding_El" or self.refresh_fft:
            # self.radar_r_u_v = self.radar_points(s_ant)
            self.radar_r_u_v = self.parent.data.pc_radar
            self.update_cfar_flag = True

        else:
            self.update_cfar_flag = False

        if self.updated_key == "CA-CFAR" or self.update_cfar_flag or self.cfar_results is None:
            cur_points = self.parent.data.pc_radar.copy()
            print("get_image2d len(cur_points)", len(cur_points))
            # cur_points = cur_points[cur_points["total_power"] > self.parent.params.params_values["CA-CFAR"].get_value()]

            self.cfar_results = np.zeros((len(cur_points), 3), dtype=np.float32)
            self.cfar_results[:, 0] = cur_points["r"].values
            self.cfar_results[:, 1] = np.rad2deg(np.arcsin(cur_points["u_sin"].values))
            self.cfar_results[:, 2] = np.rad2deg(np.arcsin(cur_points["v_sin"].values))

            # OLD WAY
            # self.cfar_results = self.apply_cfar(ffta=self.radar_r_u_v.copy())
            # if self.update_cfar_flag:
            #     theta = np.deg2rad(180)
            #     rotation_matrix = np.array([[1, 0, 0],[0, np.cos(theta), -np.sin(theta)],[0, np.sin(theta), np.cos(theta)]])
            #     self.cfar_results = np.matmul(rotation_matrix, self.cfar_results.T).T

            self.update_cfar_flag = False

        print("\n########### Radar Points Summary ###########")
        print("got peaks!", self.cfar_results.shape)
        if self.cfar_results.shape[0] > 0:
            print("range ", self.cfar_results[:, 0].min(), self.cfar_results[:, 0].max())
            print("az ", self.cfar_results[:, 1].min(), self.cfar_results[:, 1].max())
            print("el ", self.cfar_results[:, 2].min(), self.cfar_results[:, 2].max())
        else:
            print("No Peaks detected")
        print("#############################################\n")

        # self.cfar_results = self.cfar_results[self.cfar_results[:, 0] > 2]

        points = self.camera_radar.Radar2Image(r=self.cfar_results[:, 0], az=self.cfar_results[:, 1], el=self.cfar_results[:, 2], D=self.parent.params.get_D_matrix())
        return points, self.cfar_results

    def get_rng_az(self, s_ant):
        ffta = np.zeros([s_ant.shape[0] // 2, s_ant.shape[1], s_ant.shape[2], s_ant.shape[3]])
        for i in range(s_ant.shape[0] // 2):
            ffta[i, :, :, :] = np.sqrt(s_ant[2 * i, :, :, :] ** 2 + s_ant[2 * i + 1, :, :, :] ** 2)

        # self.parent.data.radar_param["range"][0] + indices[:, 0] * self.parent.data.bins["range"]

        slice_range = int(self.parent.params.params_values["range"].get_value() / self.parent.data.bins["range"])

        rng_az = np.max(np.max(ffta, axis=0), axis=1)[:slice_range, :]
        # print("get_rng_az: Slice - ", slice_range, rng_az.shape)
        rng_az = np.flipud(cv2.resize(rng_az, (300, 800)))
        # print("rng_az", rng_az.shape)
        return rng_az

    def cut_by_range(self, pts, index=3):
        x = pts[np.linalg.norm(pts[:, :index], axis=-1) < self.parent.params.params_values["range"].get_value()]
        x = x[np.linalg.norm(x[:, :index], axis=-1) > 4]
        return x
        # return pts[np.linalg.norm(pts[:, :index], axis=-1) < self.parent.params.params_values["range"].get_value()]

    def filter_fov_az(self, pts):
        az = self.parent.params.params_values["fov_az"].get_value()
        if az == 0:
            return pts
        # print("filter_fov_az", az)
        # pts = pts[pts[:, 0] > 0]
        r, Az, El = toRAzEl(x=pts[:, 0], y=pts[:, 1], z=pts[:, 2])
        # print("Az", Az.min(), Az.max())
        # mask = np.logical_and(Az < (az // 2), (-az // 2) < Az)
        mask = np.logical_and(Az < (az // 2), (-az // 2) > Az)
        # print("filter_fov_az mask", mask.shape, "pts", pts.shape)

        return pts[mask, :]

    def plot_pc(self, points, ax, c, cmap, ax_2=None, marker=None):
        # print("plot_pc - input", points.shape, self.parent.params.params_values[POINT_SIZE].get_value())
        # azim = self.parent.params.params_values["Top-View-Az"].get_value()
        # el = self.parent.params.params_values["Top-View-El"].get_value()

        azim = 90
        el = 90
        points = self.cut_by_range(pts=points)

        if c is None and points.shape[0] != 0:
            c = np.linalg.norm(points[:, :3], axis=-1)
            self.max_range = max(self.max_range, c.max())

        # print("self.parent.params.params_values[POINT_SIZE].get_value()", self.parent.params.params_values[POINT_SIZE].get_value())
        ax.scatter(points[:, 0], points[:, 1], points[:, 2],
                   c=c,
                   cmap=cmap,
                   s=self.parent.params.params_values[POINT_SIZE].get_value(),
                   norm=self.cNorm,
                   marker=marker
                   )
        labelpad = 10
        ax.set_xlabel('X(m)', labelpad=labelpad)
        ax.set_ylabel('Y(m)', labelpad=labelpad)
        ax.set_zlabel('Z(m)', labelpad=labelpad)
        # ax.set_title('Point Cloud ' + str(self.parent.data.ts))
        ax.view_init(elev=el, azim=azim + 180)

        return

    def plot_2d_points(self, points, ax, c):
        return

    def display_top_view(self):
        # Display Image
        ax1 = self.fig.add_subplot(2, 1, 2)
        im = self.get_im_rgb()
        ax1.imshow(im)
        ax1.set_axis_off()

        # Top View
        imgfov_pc_pixel, imgfov_pc_velo, pts_velo = self.get_pc(im)

        ax2 = self.fig.add_subplot(2, 1, 1, projection='3d')
        title = "#Points "

        norms = np.linalg.norm(pts_velo[:, :3], axis=-1)
        if self.parent.params.params_values["Radar-PC"].isChecked():
            norms = "red"

            radar_points = self.cut_by_range(self.parent.data.pc_radar, index=3)
            self.plot_pc(radar_points, ax=ax2, c="blue")
            title += "Radar (blue) " + str(radar_points.shape[0])

        if self.parent.params.params_values["Point Cloud"].isChecked():
            self.plot_pc(points=pts_velo, ax=ax2, c=norms)
            title += ", Lidar (red) " + str(len(pts_velo))

        ax2.set_title(title)
        return

    def get_im_rgb(self):
        im = self.parent.data.im
        if self.parent.params.params_values[RECTIFY_IMG].isChecked():
            im = self.undistort(im)

        # num_rows, num_cols = im.shape[:2]

        # self.camera_radar.set_t()
        # translation_matrix = np.float32([[1, 0, self.parent.params.params_values[TX_CAMERA_RADAR].get_value()],
        #                                  [0, 1, self.parent.params.params_values[TZ_CAMERA_RADAR].get_value()]])
        # im = cv2.warpAffine(im, translation_matrix, (num_cols, num_rows))

        # rot_mat = self.get_rotation_matrix(theta_z=np.deg2rad(self.parent.params.params_values[YAW_CAMERA].get_value()),
        #                                    theta_x=np.deg2rad(self.parent.params.params_values[PITCH_CAMERA].get_value()),
        #                                    theta_y=np.deg2rad(self.parent.params.params_values[ROLL_CAMERA].get_value()))

        # im = cv2.warpPerspective(im, rot_mat, dsize=(num_cols, num_rows))

        return im

    def radar_points(self, s_ant):
        self.pad_el = self.parent.params.params_values["Padding_El"].get_value()
        self.pad_az = self.parent.params.params_values["Padding_Az"].get_value()
        noise_level = 0

        def get_from_torch(_s_ant):
            SRe = torch.from_numpy(_s_ant.real)
            SIm = torch.from_numpy(_s_ant.imag)

            s_t = torch.zeros(SRe.shape[1], SRe.shape[0], 6, 20, 2)

            s_t[:, :, self.ch_order[:, 0], self.ch_order[:, 1], 0] = SRe.permute(2, 0, 1)
            s_t[:, :, self.ch_order[:, 0], self.ch_order[:, 1], 1] = SIm.permute(2, 0, 1)

            # print("self.ch_order.shape", self.ch_order.shape, s_t.shape)
            if self.pad_el or self.pad_az:
                # print("zero padding pad_az", self.pad_az, "pad_el", self.pad_el)
                s_t = F.pad(s_t, (0, 0, 0, 20 * int(self.pad_az) - 20, 0, 6 * int(self.pad_el) * 2 - 6), "constant", 0)
                # s_t = F.pad(s_t, (0,  6 * _zero_pad * 2 - 6, 20 * _zero_pad - 20, 0, 0), "constant", 0)
            # print("s_t", s_t.shape)
            ffta = torch.fft(s_t, 2, normalized=True)  # torch.Size([48, 600, 6, 20, 2]) (dop,rng,el,az,real/imag)

            # print("ffta after zero_pad", ffta.shape)
            array = torch.zeros(ffta.shape[0] * 2, ffta.shape[1], ffta.shape[2], ffta.shape[3])
            array[::2, :, :, :] = ffta[:, :, :, :, 0]
            array[1::2, :, :, :] = ffta[:, :, :, :, 1]

            axis = 3
            array = torch.roll(array, shifts=array.shape[axis] // 2, dims=3)
            axis = 2
            array = torch.roll(array, shifts=array.shape[axis] // 2, dims=axis)
            return array

        # def get_from_numpy(s_ant):
        #     SRe = s_ant.real
        #     SIm = s_ant.imag
        #
        #     s_t = np.zeros((SRe.shape[1], SRe.shape[0], 6, 20), dtype=np.float32)
        #     s_t[:, :, self.ch_order[:, 0], self.ch_order[:, 1]] = np.transpose(s_ant, (2, 0, 1))
        #
        #     print("self.ch_order.shape", self.ch_order.shape, s_t.shape)
        #     s_t = np.pad(array=s_t, pad_width=(6 * zero_pad * 2 - 6, 0, 20 * zero_pad - 20, 0), mode="constant", constant_values=0)
        #     ffta = np.fft.fft2(s_t, axes=(-2, -1), norm="ortho")  # torch.Size([48, 600, 6, 20, 2]) (dop,rng,el,az,real/imag)
        #     return ffta

        x = get_from_torch(_s_ant=s_ant)

        # hann_window = torch.hann_window(12, periodic=True, dtype=x.dtype)

        # 5.
        ffta = np.zeros([x.shape[0] // 2, x.shape[1], x.shape[2], x.shape[3]])
        for i in range(x.shape[0] // 2):
            ffta[i, :, :, :] = np.sqrt(x[2 * i, :, :, :] ** 2 + x[2 * i + 1, :, :, :] ** 2)

        ffta = self.db(ffta)

        # print("before shift", ffta.shape, ffta[0][0][0][0])
        # ffta = np.fft.fftshift(ffta, 2)
        # print("before shift", ffta.shape, ffta[0][0][0][0])
        self.calculated_ffta = ffta.copy()

        # 6. sum over dop
        # ffta = np.max(ffta, axis=0)
        num_dopplers = ffta.shape[0]
        ffta = np.sum(ffta, axis=0) / num_dopplers
        # ffta = ffta[(ffta.shape[0] // 2)]
        # ffta = ffta[0]
        # 7. CA_CFAR
        """
        Calculate Bins after padding
        """
        carrier_freq = 79000
        virtual_array_dx, virtual_array_dy = 0.22, 0.22
        non_zero = lambda v: 1 if not v else v
        n_azimuth, n_elevation = 20 * non_zero(self.pad_az), 6 * non_zero(self.pad_el)
        az_fov = np.sin(np.deg2rad(40))
        el_fov = np.sin(np.deg2rad(10))

        speed_of_light = 299792458
        lambda_eq = speed_of_light / (carrier_freq * 1e6)
        delta_x = np.abs(virtual_array_dx) * 0.01
        self.az_bin = (lambda_eq / delta_x) / n_azimuth  # self.parent.data.radar_param["az"][NUM_BINS]
        self.az_min = -1 * self.az_bin * (n_azimuth / 2)

        # min_az_ind = min(n_azimuth - 1, max(0, int(np.ceil(np.sin(np.deg2rad(-az_fov) - az_min) / az_bin)) - 1))
        # max_az_ind = min(n_azimuth - 1, max(0, int(np.floor(np.sin(np.deg2rad(az_fov) - az_min) / az_bin)) + 1))

        self.min_az_ind = max(0, int(np.floor(abs(self.az_min + az_fov) / self.az_bin)) - 1)
        self.max_az_ind = max(0, int(np.ceil(abs(-self.az_min + az_fov) / self.az_bin)) + 1)

        delta_y = np.abs(virtual_array_dy) * 0.01
        self.el_bin = (lambda_eq / delta_y) / n_elevation  # self.parent.data.radar_param["el"][NUM_BINS]
        self.el_min = -1 * self.el_bin * (n_elevation / 2)

        # min_el_ind = min(n_elevation - 1, max(0, int(np.ceil(np.sin(np.deg2rad(-el_fov) - el_min) / el_bin)) - 1))
        # max_el_ind = min(n_elevation - 1, max(0, int(np.floor(np.sin(np.deg2rad(el_fov) - el_min) / el_bin)) + 1))

        # self.min_el_ind = int(np.floor(abs(self.el_min + el_fov) / self.el_bin))
        # self.max_el_ind = int(np.ceil(abs(-self.el_min + el_fov) / self.el_bin))

        self.min_el_ind = max(0, int(np.floor(abs(self.el_min + el_fov) / self.el_bin)))
        self.max_el_ind = max(0, int(np.ceil(abs(-self.el_min + el_fov) / self.el_bin)) + 1)

        print("\n########### Clipping Az/El indices ###########")
        print("before: az_min", np.rad2deg(np.arcsin(self.az_min)), "el_min", np.rad2deg(np.arcsin(self.el_min)))

        self.az_min = self.az_min + self.min_az_ind * self.az_bin
        self.el_min = self.el_min + self.min_el_ind * self.el_bin

        print("after: az_min", np.rad2deg(np.arcsin(self.az_min)), "el_min", np.rad2deg(np.arcsin(self.el_min)))
        print("##############################################\n")

        print("az_bin", self.az_bin, "az_min", self.az_min, "el_bin", self.el_bin, "el_min", self.el_min)
        print("az_bin", np.rad2deg(np.arcsin(self.az_bin)), "az_min", np.rad2deg(np.arcsin(self.az_min)), "el_bin",
              np.rad2deg(np.arcsin(self.el_bin)), "el_min", np.rad2deg(np.arcsin(self.el_min)))
        print("min_az_ind", self.min_az_ind, "max_az_ind", self.max_az_ind, "min_el_ind", self.min_el_ind, "max_el_ind", self.max_el_ind)

        ffta = ffta[:, self.min_el_ind:self.max_el_ind, self.min_az_ind:self.max_az_ind]

        # def zero_unlocal_max(mat, min_rng, max_rng, min_az, max_az):
        #     mat[:min_rng]

        return ffta

    def get_radar_points_projected(self):
        radar_points = self.cut_by_range(self.parent.data.pc_radar)
        norms = np.linalg.norm(radar_points[:, :3], axis=-1)
        points = self.camera_radar.Cart2Image(x=radar_points[:, 0], y=radar_points[:, 1], z=radar_points[:, 2], D=self.parent.params.get_D_matrix())
        print("Projected radar points! ", points.shape)
        return points, norms, radar_points

    def get_cmap(self, radar, delta=None):
        if self.parent.params.params_values[LIDAR_PC].isChecked() and self.parent.params.params_values[RADAR_FFT].isChecked() and radar:
            return self.autumn
        elif self.parent.params.params_values[LIDAR_PC].isChecked() and self.parent.params.params_values[RADAR_FFT].isChecked() and not radar:
            return self.winter
        elif delta:
            return self.cool
        else:
            return self.jet

    def plot_image_to_radar(self, image_ax, radar_ax, ax3):
        w = self.parent.params.params_values["image_w_x"].get_value()
        h = self.parent.params.params_values["image_h_y"].get_value()

        image_ax.scatter([w], [h], s=20, c="purple")
        r, az, el = self.camera_radar.Image2Radar(np.array([w]), np.array([h]), Dist=self.parent.params.params_values["range"].get_value())
        r = self.max_range

        x, y, z = toXYZ(r, az, el)
        radar_ax.quiver([0], [0], [0], [x], [y], [z], arrow_length_ratio=0, color='purple')

        ax3.quiver([0], [0], [0], [x], [y], [z], arrow_length_ratio=0, color='purple')
        return

    def update_params(self, key):
        self.point_size = self.parent.params.params_values[POINT_SIZE].get_value()
        self.cNorm = colors.Normalize(vmin=0, vmax=self.parent.params.params_values["range"].get_value())
        self.updated_key = key
        self.max_range = 0
        self.camera_radar.set_t(tx=self.parent.params.params_values[TX_CAMERA_RADAR].get_value(),
                                ty=self.parent.params.params_values[TY_CAMERA_RADAR].get_value(),
                                tz=self.parent.params.params_values[TZ_CAMERA_RADAR].get_value())

        self.camera_radar.set_rotation(yaw=self.parent.params.params_values[PITCH_CAMERA_RADAR].get_value(),
                                       pitch=self.parent.params.params_values[YAW_CAMERA_RADAR].get_value(),
                                       roll=self.parent.params.params_values[ROLL_CAMERA_RADAR].get_value())
        return

    def update_fig(self, key=None):
        self.update_params(key)
        self.cb = None
        self.reset()
        # if self.parent.params.params_values["Top-View"].isChecked():
        #     self.display_top_view()
        #     self.draw()
        #     return

        gs = gridspec.GridSpec(ncols=4, nrows=2, height_ratios=[3, 2], width_ratios=[1, 4, 4, 1])
        # gs = gridspec.GridSpec(ncols=3, nrows=3, height_ratios=[2, 1, 1])

        cols = 1
        if self.parent.params.params_values["Rng-Az"].isChecked():
            ax1 = self.fig.add_subplot(2, 2, 4)
            cols = 2
        else:
            # ax1 = self.fig.add_subplot(2, 1, 1)
            ax1 = self.fig.add_subplot(gs[1, :])
            # ax1 = self.fig.add_subplot(gs[2, 1])
            # print(ax1)
        im = self.get_im_rgb()

        ax1.imshow(im)
        ax1.set_axis_off()

        # ax2 = self.fig.add_subplot(2, cols, 2, projection='3d')
        # ax2 = self.fig.add_subplot(gs[:3, :3],  projection='3d')
        self.fig.suptitle("Spatial Calibration")
        ax2 = self.fig.add_subplot(gs[0, :2], projection='3d')
        ax2.set_title("Top View")
        ax3 = self.fig.add_subplot(gs[0, 2:], projection='3d')
        ax3.set_title("Side View")
        ax1_title = ""

        if self.parent.params.params_values[LIDAR_PC].isChecked():
            imgfov_pc_pixel, imgfov_pc_velo, pts_velo = self.get_pc(im)
            norms = np.linalg.norm(imgfov_pc_velo[:, :3], axis=-1)
            self.plot_pc(points=lidar_to_radar_coordinates(pts_velo), ax=ax2, c=None, cmap=self.get_cmap(radar=False), marker=LIDAR_MARKER)

            # Second Plot
            self.plot_pc(points=lidar_to_radar_coordinates(pts_velo), ax=ax3, c=None, cmap=self.get_cmap(radar=False), marker=LIDAR_MARKER)

            scat = ax1.scatter(x=np.round(imgfov_pc_pixel[0, :]).astype(np.int),
                               y=np.round(imgfov_pc_pixel[1, :]).astype(np.int), s=self.point_size, c=norms,
                               cmap=self.get_cmap(radar=False),
                               norm=self.cNorm,
                               marker=LIDAR_MARKER)
            # ax1_title += "Lidar(Blue): " + str(imgfov_pc_pixel.shape) + " Points, "
            self.cb = plt.colorbar(scat, ax=ax1, fraction=0.026, pad=0.08)
            self.cb.set_label('Lidar', rotation=270, labelpad=8)

        if self.parent.params.params_values[RADAR_DELTA].isChecked():
            radar_points_projected, norms, delta_pc = self.get_radar_points_projected()
            self.plot_pc(delta_pc, ax=ax2, c=None, marker=DELTA_MARKER, cmap=self.cool)
            scat = ax1.scatter(x=np.round(radar_points_projected[0, :]).astype(np.int),
                               y=np.round(radar_points_projected[1, :]).astype(np.int),
                               s=self.point_size, c=norms,
                               cmap=self.cool,
                               norm=self.cNorm,
                               marker=DELTA_MARKER)
            self.cb = plt.colorbar(scat, ax=ax1, fraction=0.026, pad=0.04)

        if self.parent.params.params_values[RADAR_FFT].isChecked():
            radar_points_projected, radar_r_u_v = self.get_image2d()

            x, y, z = toXYZ(r=radar_r_u_v[:, 0], Az=radar_r_u_v[:, 1], El=radar_r_u_v[:, 2])
            pts_3d = np.stack((x, y, z), axis=-1)
            # radar_points_projected, pts_3d, inds = filter_points_out_fov(im, radar_points_projected, pts_3d=pts_3d)
            norms = radar_r_u_v[:, 0]
            self.plot_pc(pts_3d, ax=ax2, c=None, marker=FFT_MARKER, cmap=self.get_cmap(radar=True))

            # second Axe
            self.plot_pc(pts_3d, ax=ax3, c=None, marker=FFT_MARKER, cmap=self.get_cmap(radar=True))
            ax3.view_init(elev=0, azim=180)

            # print("to_show.shape", radar_points_projected.shape)
            scat = ax1.scatter(x=np.round(radar_points_projected[0, :]).astype(np.int),
                               y=np.round(radar_points_projected[1, :]).astype(np.int), s=self.point_size, c=norms,
                               cmap=self.get_cmap(radar=True),
                               norm=self.cNorm,
                               marker=FFT_MARKER)
            self.cb = plt.colorbar(scat, ax=ax1, fraction=0.026, pad=0.04)
            self.cb.set_label('Radar', rotation=270, labelpad=8)
            # ax1_title += "Radar(Red): " + str(radar_points_projected.shape) + " Points."

            if self.parent.params.params_values["Rng-Az"].isChecked():
                ax3 = self.fig.add_subplot(1, 2, 1)
                ax3.imshow(self.get_rng_az(s_ant=self.calculated_ffta))

        self.plot_image_to_radar(image_ax=ax1, radar_ax=ax2, ax3=ax3)
        ax1.set_title(str(ax1_title), pad=24)
        self.draw()
        self.parent.display_widget.refresh_fft = False
        return


class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


class ParametersWidget(QGroupBox):
    def __init__(self, parent, params, data):
        super(QWidget, self).__init__(parent)
        self.layout = QFormLayout()
        self.params = params
        self.q_edits = {}
        self.data = data

        self.reset_value = QPushButton('Reset', self)
        self.reset_value.clicked.connect(self.params.reset_camera_values)

        self.save_config_button = QPushButton('Save', self)
        self.save_config_button.clicked.connect(self.save_config)

        self.load_config_button = QPushButton('Load', self)
        self.load_config_button.clicked.connect(self.load_config)

        # Build Layout
        self.layout.addRow(self.save_config_button, self.load_config_button)
        self.layout.addRow(self.reset_value)

        self.select_im = SpinThresh(key=DF_IDX, params=self.params, single_step=1, default_value=0,
                                    callback=self.update_im)

        self.num_frames = SpinThresh(key=AVG_FRAMES, params=self.params, single_step=10, default_value=10, callback=self.update_im)

        self.layout.addRow(QLabel("Image:"), self.select_im)
        self.layout.addRow(QLabel("AVG_FRAMES:"), self.num_frames)

        # self.layout.addRow(QLabel("Lidar Params:"))

        for k, v in self.params.params_values.items():
            self.q_edits[k] = v
            self.layout.addRow(QLabel(k + ":"), self.q_edits[k])

            # if k == "k3":
            #     self.layout.addRow(QLabel("Radar Params:"))

        self.setLayout(self.layout)

    def update_im(self):
        suffix = int(self.select_im.get_value())
        # print("updating image, current suffix is ", suffix)
        self.data.update_data(suffix=suffix, num_frames=int(self.num_frames.get_value()))
        self.params.value_changed(key=DF_IDX)
        return

    def save_config(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getSaveFileName(self, "QFileDialog.getSaveFileName()", "",
                                                  "All Files (*);;Text Files (*.txt)", options=options)
        if fileName:
            print("Saving", fileName)
            with open(fileName, "w") as fp:
                d = self.params.get_dict()
                print("Saving", d)
                json.dump(d, fp, cls=NumpyEncoder)

    def load_config_from_path(self, fileName):
        print("Loading", fileName)
        with open(fileName, "r") as fp:
            d = json.load(fp)
            self.params.load(d)
        return

    def load_config(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(self, "QFileDialog.getOpenFileName()", "",
                                                  "All Files (*);;Python Files (*.py)", options=options)
        if fileName:
            self.load_config_from_path(fileName)


class CustomStyle(QProxyStyle):
    def styleHint(self, hint, option=None, widget=None, returnData=None):
        if hint == QStyle.SH_QDoubleSpinBox_KeyPressAutoRepeatRate:
            return 100 ** 100
        elif hint == QStyle.SH_QDoubleSpinBox_ClickAutoRepeatRate:
            return 100 ** 100
        elif hint == QStyle.SH_QDoubleSpinBox_ClickAutoRepeatThreshold:
            # You can use only this condition to avoid the auto-repeat,
            # but better safe than sorry ;-)
            return 100 ** 100
        else:
            return super().styleHint(hint, option, widget, returnData)


class SpinThresh(QWidget):
    def __init__(self, key, params, single_step=1.0, default_value=1.0, callback=None):
        super(SpinThresh, self).__init__()
        layout = QHBoxLayout()
        self.params = params
        # self.setStyle(CustomStyle())
        self.key = key
        self.sp = QDoubleSpinBox()
        self.sp.setRange(-2000, 2000)
        self.sp.setDecimals(5)
        self.sp.setSingleStep(single_step)
        self.def_val = default_value
        # self.sp.setValue(default_value)
        self.set_val(default_value)

        # layout.addWidget(QLabel(key))
        layout.addWidget(self.sp)
        if callback is None:
            self.sp.valueChanged.connect(self.value_changed)
        else:
            self.sp.valueChanged.connect(callback)
        self.setLayout(layout)

    def value_changed(self):
        self.params.set_param(self.key, self.sp.value())
        return

    def get_value(self):
        return self.sp.value()

    def set_val(self, val):
        # print("\n\n\n set_val \n\n\n\n")
        self.sp.setValue(val)

    def reset_default_value(self):
        self.sp.setValue(self.def_val)
        return

    def __repr__(self):
        return str(self.key) + " : " + str(self.sp.value())


class Data:
    def __init__(self, suffix=3):
        self.im = None
        os.makedirs("./cached/", exist_ok=True)
        self.pc_lidar = None
        self.pc_radar = None
        self.rd = None
        self.ts = ""
        self.radar_param = None
        self.suffix = suffix
        self.radar_ts = ""
        self.bins = {}

        # self.base = "/mnt/hdd/pipeline_test/lidar_ultra_static_W36_M8_RF0/"
        # self.base = "/mnt/hdd/pipeline_test/ultra_calib_new_W36_M8_RF0/"
        # self.base = "/mnt/hdd/pipeline_test/calib_lev_3_W36_M8_RF1/"
        # if len(sys.argv) > 2:
        #     self.base = sys.argv[2]

        self.base_paths = ["/mnt/hdd/pipeline_test/single_corner_front_W36_M8_RF1/",
                           "/mnt/hdd/pipeline_test/corner_right_W36_M8_RF1/",
                           "/mnt/hdd/pipeline_test/corner_left_W36_M8_RF1/",
                           ]

        # base_paths = ["/mnt/hdd/pipeline_test/all_corners_2_W36_M8_RF1/"]
        # self.base_paths = ["/mnt/hdd/pipeline_test/cornesr_left_right_W36_M8_RF1"]
        self.base_paths = ["/mnt/hdd/new_lidar_drives/200901_cali_front_W92_M15_RF1/",
                           "/mnt/hdd//new_lidar_drives//200902_lidar_cali_left_W92_M15_RF1/",
                           "/mnt/hdd/new_lidar_drives//200902_lidar_cali_right_W92_M15_RF1/",
                           # "/mnt/hdd//new_lidar_drives//200901_cali_left_v2_W92_M15_RF1/",
                           # "/mnt/hdd/pipeline_test//lidar_2_corner_20m_W36_M8_RF6/"
                           ]

        self.base = self.base_paths[0]
        self.df = pd.DataFrame()
        for b in self.base_paths:
            df = pd.read_csv(b + "/sample_df.csv", index_col=None)
            for i in range(10):
                df = df.sample(4).reset_index(drop=True)
                df["base_path"] = b

                rgb_path = str(df.loc[0, 'base_path']) + "/rgbCamera/rgbCamera_" + str(df.loc[0, 'timestampCamera']) + ".png"
                pc_path = str(df.loc[0, 'base_path']) + "/lidar_pc/lidar_pc_" + str(df.loc[0, 'timestampLidar'])[:-4] + ".npz"
                S_ANT_path = str(df.loc[0, 'base_path']) + "/S_ANT/S_ANT_" + str(df.loc[0, "timestampRadar"]) + ".npy"
                radar_pc_path = str(df.loc[0, 'base_path']) + "/pcRadar/pc_" + str(df.loc[0, "timestampRadar"]) + ".csv"

                paths = [rgb_path, pc_path, S_ANT_path, radar_pc_path]

                flag = False
                for p in paths:
                    if not os.path.exists(p):
                        flag = True
                        print("Path not exists", p)
                        break

                if flag:
                    print("couldn't find", b)
                    continue
                else:
                    self.df = self.df.append(df)
                    break
        self.df = self.df.reset_index(drop=True)
        print("len ", len(self.df))

        self.update_data = self.update_date_multiple

        self.update_data(0)
        return

    def inverse_db(self, arr):
        return (arr / 10) ** 10

    def get_rea(self, pc_radar):
        rdaz = np.zeros((self.radar_param["doppler"][2],
                         self.radar_param["range"][2],
                         self.radar_param["az"][2],
                         self.radar_param["el"][2]), dtype=np.complex)

        # doppler
        dop_indices = (((pc_radar["dop"].values - self.radar_param["doppler"][0]) / (np.abs(self.radar_param["doppler"][1] - self.radar_param["doppler"][0]))) * self.radar_param["doppler"][
            2] - 1).astype(np.int)
        range_indices = (((pc_radar["r"].values - self.radar_param["range"][0]) / (self.radar_param["range"][1] - self.radar_param["range"][0])) * self.radar_param["range"][2] - 1).astype(np.int)
        az_indices = (((pc_radar["u_sin"].values - self.radar_param["az"][0]) / (self.radar_param["az"][1] - self.radar_param["az"][0])) * self.radar_param["az"][2] - 1).astype(np.int)
        el_indices = (((pc_radar["v_sin"].values - self.radar_param["el"][0]) / (self.radar_param["el"][1] - self.radar_param["el"][0])) * self.radar_param["el"][2] - 1).astype(np.int)

        # value -> real
        # power -> img
        rdaz[dop_indices, range_indices, az_indices, el_indices] = self.inverse_db(pc_radar["value"].values) + 1j * self.inverse_db(pc_radar["power"].values)
        rea = np.max(db(np.abs(rdaz)), axis=0)
        rea = np.nan_to_num(rea, nan=0.0, neginf=0)
        rea = np.transpose(rea, (0, 2, 1))
        return rea

    def update_date_multiple(self, suffix=0, num_frames=10, mean=True):
        pats_idx = suffix % len(self.base_paths)

        self.df = pd.DataFrame()

        self.df = pd.read_csv(self.base_paths[pats_idx] + "/sample_df.csv", index_col=None)
        print("Loading", self.base_paths[pats_idx], "num_frames", num_frames, "len-sample.df", len(self.df))
        # self.df = self.df.sample(num_frames).reset_index(drop=True)
        self.df = self.df[:10].reset_index(drop=True)
        self.df["base_path"] = self.base_paths[pats_idx]
        self.im, self.pc_lidar, self.pc_radar, self.rd = None, None, None, None

        num_s_ants = 1
        self.pc_radar = pd.DataFrame()
        for idx in tqdm(range(1, len(self.df))):
            try:
                rgb_path = str(self.df.loc[idx, 'base_path']) + "/rgbCamera/rgbCamera_" + str(self.df.loc[idx, 'timestampCamera']) + ".png"
                pc_path = str(self.df.loc[idx, 'base_path']) + "/lidar_pc/lidar_pc_" + str(self.df.loc[idx, 'timestampLidar'])[:-4] + ".npz"
                S_ANT_path = str(self.df.loc[idx, 'base_path']) + "/S_ANT/S_ANT_" + str(self.df.loc[idx, "timestampRadar"]) + ".npy"
                radar_pc_path = str(self.df.loc[idx, 'base_path']) + "/pcRadar/pc_" + str(self.df.loc[idx, "timestampRadar"]) + ".csv"

                self.load_radar_param(idx)

                if self.im is None:
                    self.im = cv2.imread(rgb_path)[..., ::-1]
                if self.pc_lidar is None and os.path.exists(pc_path):
                    self.pc_lidar = np.load(pc_path)['arr_0']
                if os.path.exists(radar_pc_path):
                    pc_radar = pd.read_csv(radar_pc_path)
                    pc_radar = pc_radar[pc_radar["r"] > 2]
                    pc_radar = pc_radar.reset_index(drop=True)
                    pc_radar["total_power"] = db(np.abs(self.inverse_db(pc_radar["value"].values) + 1j * self.inverse_db(pc_radar["power"].values)))

                    # self.pc_radar = self.get_rea(pd.read_csv(radar_pc_path))
                    self.pc_radar = self.pc_radar.append(pc_radar.iloc[pc_radar["total_power"].idxmax()])
                    # print(len(self.pc_radar))

                self.ts = os.path.split(str(self.df.loc[idx, 'base_path']))[-1] + " " + str(self.df.loc[idx, "timestampRadar"])
                self.radar_ts = str(self.df.loc[idx, "timestampRadar"])

                # if os.path.exists(S_ANT_path):
                #     x = np.load(S_ANT_path)
                #     if self.rd is None:
                #         self.rd = x
                #     else:
                #         self.rd += x
                #         num_s_ants += 1

            except Exception as e:
                PrintException()
                # print("Error in update_date_multiple", e)
                exit()

        if mean:
            self.pc_radar = self.pc_radar.mean().to_frame().T

        # self.rd /= num_s_ants

    def load_radar_param(self, idx):
        with open(str(self.df.loc[idx, 'base_path']) + "/" + RADAR_PARAM, 'rb') as f:
            self.radar_param = pickle.load(f)

        keys = ["range", "az", "el", "doppler"]
        # min max bins
        for k in keys:
            self.bins[k] = (self.radar_param[k][1] - self.radar_param[k][0]) / self.radar_param[k][2]

        # print("bins", self.bins)

    def update_data(self, suffix):
        print("Calling update_data")
        self.suffix = suffix
        idx = suffix
        if idx == 0:
            idx += 1

        if idx >= len(self.df):
            idx = idx % len(self.df)

        rgb_path = str(self.df.loc[idx, 'base_path']) + "/rgbCamera/rgbCamera_" + str(self.df.loc[idx, 'timestampCamera']) + ".png"
        pc_path = str(self.df.loc[idx, 'base_path']) + "/lidar_pc/lidar_pc_" + str(self.df.loc[idx, 'timestampLidar'])[:-4] + ".npz"
        S_ANT_path = str(self.df.loc[idx, 'base_path']) + "/S_ANT/S_ANT_" + str(self.df.loc[idx, "timestampRadar"]) + ".npy"
        radar_pc_path = str(self.df.loc[idx, 'base_path']) + "/pcRadar/pc_" + str(self.df.loc[idx, "timestampRadar"]) + ".npy"

        self.im = cv2.imread(rgb_path)[..., ::-1]
        self.pc_lidar = np.load(pc_path)['arr_0']
        self.ts = os.path.split(str(self.df.loc[idx, 'base_path']))[-1] + " " + str(self.df.loc[idx, "timestampRadar"])
        self.radar_ts = str(self.df.loc[idx, "timestampRadar"])

        self.load_radar_param(idx)

        self.rd = np.load(S_ANT_path)
        self.pc_radar = np.load(radar_pc_path).T


class CustomCheckBox(QCheckBox):
    def __init__(self):
        super(CustomCheckBox, self).__init__()
        return

    def __repr__(self):
        return str(self.isChecked())

    def get_value(self):
        return self.isChecked()

    def set_val(self, val):
        if val:
            self.setChecked(True)
        else:
            self.setChecked(False)


class Matrix(QGridLayout):
    def __init__(self, params, rows=3, cols=4):
        super(Matrix, self).__init__()
        self.data = {}
        self.cols = cols
        self.rows = rows
        self.params = params

        self.default_values = np.array(
            [[0, -1, 0, 0],
             [0, 0, -1, 0],
             [1, 0, 0, 0]])

        self.default_values = np.array(
            [[1, 0, 0, 0],
             [0, 1, 0, 0],
             [0, 0, 1, 0]])

        for i in range(rows):
            for k in range(cols):
                self.data[(i, k)] = SpinThresh(key="trans_" + str(i) + "_" + str(k), params=self.params,
                                               single_step=0.01, default_value=self.default_values[i][k])
                self.addWidget(self.data[(i, k)], i, k)

    def get_matrix(self):
        m = np.zeros((self.rows, self.cols), dtype=np.float)
        for i in range(self.rows):
            for k in range(self.cols):
                val = self.data[(i, k)].get_value()
                m[i][k] = val
        return m

    def __repr__(self):
        return str(self.get_matrix())

    def get_value(self):
        return self.get_matrix()

    def set_val(self, val):
        m = np.asarray(val)
        for i in range(self.rows):
            for k in range(self.cols):
                self.data[(i, k)].set_val(m[i][k])


class Params:
    def __init__(self, parent):
        self.parent = parent
        self.camera_undistort = CustomCheckBox()
        self.camera_undistort.stateChanged.connect(self.value_changed)
        self.display_lidar_pc = CustomCheckBox()
        self.display_lidar_pc.stateChanged.connect(self.value_changed)
        self.display_lidar_top_view = CustomCheckBox()
        self.display_lidar_top_view.stateChanged.connect(self.value_changed)
        self.display_radar_pc = CustomCheckBox()
        self.display_radar_pc.stateChanged.connect(self.value_changed)
        self.display_radar_image2d = CustomCheckBox()
        self.display_radar_image2d.stateChanged.connect(self.value_changed)
        self.display_radar_rng_az = CustomCheckBox()
        self.display_radar_rng_az.stateChanged.connect(self.value_changed)

        self.translation_matrix = Matrix(rows=3, cols=4, params=self)

        self.params_values = {
            # Lidar Coeff
            LIDAR_PC: self.display_lidar_pc,
            # "Top-View": self.display_lidar_top_view,
            # "Top-View-Az": SpinThresh(key="Top-View-Az", params=self, single_step=4, default_value=90),
            # "Top-View-El": SpinThresh(key="Top-View-El", params=self, single_step=4, default_value=90),

            "range": SpinThresh(key="range", params=self, single_step=4, default_value=25),
            "fov_az": SpinThresh(key="fov_az", params=self, single_step=10, default_value=110),
            POINT_SIZE: SpinThresh(key=POINT_SIZE, params=self, single_step=1, default_value=3),

            "CAMERA-LIDAR": QLabel(""),
            ELEVATION_LIMIT: SpinThresh(key=ELEVATION_LIMIT, params=self, single_step=0.25, default_value=-1.25),
            YAW_CAMERA_LIDAR: SpinThresh(key=YAW_CAMERA_LIDAR, params=self, single_step=0.25, default_value=0),
            PITCH_CAMERA_LIDAR: SpinThresh(key=PITCH_CAMERA_LIDAR, params=self, single_step=0.25, default_value=0),
            ROLL_CAMERA_LIDAR: SpinThresh(key=ROLL_CAMERA_LIDAR, params=self, single_step=0.25, default_value=0),
            TX_CAMERA_LIDAR: SpinThresh(key=TX_CAMERA_LIDAR, params=self, single_step=0.1, default_value=0),
            TY_CAMERA_LIDAR: SpinThresh(key=TY_CAMERA_LIDAR, params=self, single_step=0.05, default_value=0),
            TZ_CAMERA_LIDAR: SpinThresh(key=TZ_CAMERA_LIDAR, params=self, single_step=0.1, default_value=0),

            "LIDAR-RADAR": QLabel(""),
            YAW_RADAR_LIDAR: SpinThresh(key=YAW_CAMERA_LIDAR, params=self, single_step=0.25, default_value=0),
            PITCH_RADAR_LIDAR: SpinThresh(key=PITCH_CAMERA_LIDAR, params=self, single_step=0.25, default_value=0),
            ROLL_RADAR_LIDAR: SpinThresh(key=ROLL_CAMERA_LIDAR, params=self, single_step=0.25, default_value=0),
            TX_RADAR_LIDAR: SpinThresh(key=TX_CAMERA_LIDAR, params=self, single_step=0.1, default_value=0),
            TY_RADAR_LIDAR: SpinThresh(key=TY_CAMERA_LIDAR, params=self, single_step=0.05, default_value=0),
            TZ_RADAR_LIDAR: SpinThresh(key=TZ_CAMERA_LIDAR, params=self, single_step=0.1, default_value=0),

            # DCM_LIDAR: self.translation_matrix,

            # Radar Params
            "RADAR-Settings": QLabel(""),
            RADAR_DELTA: self.display_radar_pc,
            RADAR_FFT: self.display_radar_image2d,
            "Rng-Az": self.display_radar_rng_az,
            "Padding_Az": SpinThresh(key="Padding_Az", params=self, single_step=1, default_value=0),
            "Padding_El": SpinThresh(key="Padding_El", params=self, single_step=1, default_value=0),
            CA_CFAR: SpinThresh(key=CA_CFAR, params=self, single_step=50, default_value=300),

            "CAMERA-RADAR": QLabel(""),

            TX_CAMERA_RADAR: SpinThresh(key=TX_CAMERA_RADAR, params=self, single_step=10, default_value=0),
            TY_CAMERA_RADAR: SpinThresh(key=TY_CAMERA_RADAR, params=self, single_step=10, default_value=0),
            TZ_CAMERA_RADAR: SpinThresh(key=TZ_CAMERA_RADAR, params=self, single_step=10, default_value=0),

            YAW_CAMERA_RADAR: SpinThresh(key=YAW_CAMERA_RADAR, params=self, single_step=1, default_value=0),
            PITCH_CAMERA_RADAR: SpinThresh(key=PITCH_CAMERA_RADAR, params=self, single_step=1, default_value=0),
            ROLL_CAMERA_RADAR: SpinThresh(key=ROLL_CAMERA_RADAR, params=self, single_step=1, default_value=0),

            "CAMERA-SETTINGS": QLabel(""),
            RECTIFY_IMG: self.camera_undistort,

            # Camera Matrix
            "fx": SpinThresh(key="fx", params=self, single_step=5, default_value=1262.4883422350001),
            "fy": SpinThresh(key="fy", params=self, single_step=5, default_value=1263.153839812814),
            "px": SpinThresh(key="px", params=self, single_step=5, default_value=911.5),
            "py": SpinThresh(key="py", params=self, single_step=5, default_value=469.5),

            # Distortion Coeff
            "k1": SpinThresh(key="k1", params=self, single_step=0.1,
                             default_value=-0.5538665737180782),
            "k2": SpinThresh(key="k2", params=self, single_step=0.1,
                             default_value=0.32106852808582886),
            "p1": SpinThresh(key="p1", params=self, single_step=0.01,
                             default_value=0),
            "p2": SpinThresh(key="p2", params=self, single_step=0.01,
                             default_value=0),
            "k3": SpinThresh(key="k3", params=self, single_step=0.01, default_value=-0.10558760320623743),

            "image_w_x": SpinThresh(key="image_w_x", params=self, single_step=10, default_value=932),
            "image_h_y": SpinThresh(key="image_h_y", params=self, single_step=10, default_value=500),

            # "k3": SpinThresh(key="k3", params=self, single_step=0.01, default_value=0),
        }

        return

    def set_param(self, key, val):
        # print("Param has been changed ", key, "-", val)
        self.value_changed(key)
        return

    def get_K_matrix(self):
        k = np.array([[self.params_values["fx"].get_value(), 0.0, self.params_values["px"].get_value()],
                      [0.0, self.params_values["fy"].get_value(), self.params_values["py"].get_value()],
                      [0.0, 0.0, 1.0]])
        return k

    def get_D_matrix(self):
        D = np.array([[self.params_values["k1"].get_value(),
                       self.params_values["k2"].get_value(),
                       self.params_values["p1"].get_value(),
                       self.params_values["p2"].get_value(),
                       self.params_values["k3"].get_value()
                       ]])
        return D

    def value_changed(self, key):
        # curframe = inspect.currentframe()
        # calframe = inspect.getouterframes(curframe, 2)
        # print('caller name:', calframe[1][3])
        # pprint(calframe)

        if key == DF_IDX:
            self.parent.display_widget.refresh_fft = True
            # self.params_values[CA_CFAR].set_val(0)
        self.parent.display_widget.update_fig(key)

    def get_Tr_velo_to_cam(self, ):
        return self.translation_matrix.get_matrix()

    def reset_camera_values(self):
        keys = ["fx", "fy", "px", "py", "k1", "k2", "p1", "p2"]
        for k in keys:
            self.params_values[k].reset_default_value()
        self.value_changed()

    def reset_pc_values(self):
        keys = ["theta", "range"]
        for k in keys:
            self.params_values[k].reset_default_value()
        self.value_changed()

    def get_dict(self):
        d = {}
        for k, v in self.params_values.items():
            if hasattr(v, "get_value"):
                print("Saving", k, v.get_value())
                d[k] = v.get_value()
        return d
        # return {k: v.get_value() for (k, v) in self.params_values.items()}

    def load(self, values_dict):
        for k, v in values_dict.items():
            self.params_values[k].set_val(v)
