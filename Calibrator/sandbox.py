import os
import glob
from pathlib import Path
from tqdm import tqdm
import pandas as pd
import numpy as np
from matplotlib import gridspec, colors
import matplotlib.pyplot as plt

pc = np.load("//mnt/hdd/new_lidar_drives/200901_cali_front_W92_M15_RF1/lidar_pc/lidar_pc_159894962257321.npz")["arr_0"]
df = pd.DataFrame()
df["x"] = pc[:, 0]
df["y"] = pc[:, 1]
df["z"] = pc[:, 2]
df["r"] = np.linalg.norm(pc, axis=1)
df = df[df["r"] <= 30]
df = df[df["x"] > 0]

fig = plt.figure(figsize=(20, 10))
gs = gridspec.GridSpec(ncols=1, nrows=1)
ax = fig.add_subplot(gs[0, 0], projection='3d')
ax.scatter(df["x"], df["y"], df["z"], s=1)

plt.show()