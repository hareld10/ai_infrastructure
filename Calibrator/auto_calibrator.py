import random
import sys
import threading

import matplotlib.pyplot as plt
from PyQt5 import QtCore, QtGui, QtWidgets
from matplotlib import gridspec
from tqdm import tqdm

from Calibrator.calibrator_utils import apply_6DOF, lidar_to_radar_coordinates, db, read_label, save_obj
from scipy import optimize
from scipy.optimize import minimize
import numpy as np
import glob
import os
import cv2
import pandas as pd
from pprint import pprint
from image_selector import Window

app = QtWidgets.QApplication(sys.argv)

LIDAR = "lidar"
RADAR = "radar"

AVG_FRAMES = 20

class AutoCalibrator:
    def __init__(self, radar_points, lidar_points):
        self.radar_points = radar_points
        self.lidar_points = lidar_points
        pass

    def cali_func(self, params):
        calibrated_pts = apply_6DOF(self.lidar_points, *params)[0]
        diffs = np.linalg.norm(self.radar_points - calibrated_pts, axis=1)
        return (diffs ** 2).mean()

    def calibrate(self):
        x0 = np.array([2.0, 0.0, 0.0, 0.4, -0.32, 0.03])
        res = minimize(self.cali_func, x0, method='nelder-mead',
                       options={'ftol': 0.000001, 'disp': True, "maxiter": 100000})
        print(res["x"].round(2))
        return res["x"].round(4)

    def compare_calibration(self, radar, lidar, calibrations, name="compare_calibrations.png"):
        calibrations = [np.zeros(6)] + [[2.0, 0.0, 0.0, 0.4, -0.32, 0.03]] + calibrations
        gs = gridspec.GridSpec(ncols=1, nrows=1)
        fig = plt.figure(figsize=(10, 10))
        ax = fig.add_subplot(gs[0, 0])
        ax.set_aspect('equal', 'box')
        scatter_size = 30
        radar.plot(ax=ax, x="x", y="y", c="z", colormap="jet", kind="scatter", s=scatter_size, colorbar=False)
        circle = plt.Circle((radar["x"], radar["y"]), 0.2, linewidth=3, color="blue", fill=False, label="radar")
        ax.add_patch(circle)
        colors = ["red", "orange", "green"]
        title = {0: "No calibration", 1: "spatial calibration 9.9.2020", 2: "Optimized method"}
        for idx, calibration in enumerate(calibrations):
            calibrated_pts = apply_6DOF(lidar, *calibration)[0]
            cali_pts = pd.DataFrame()
            cali_pts["x"] = calibrated_pts[:, 0]
            cali_pts["y"] = calibrated_pts[:, 1]
            cali_pts["z"] = calibrated_pts[:, 2]
            circle = plt.Circle((cali_pts["x"], cali_pts["y"]), 0.2, linewidth=3, color=colors[idx], fill=False,
                                label="lidar %s" % title[idx])
            ax.add_patch(circle)
            cali_pts.plot(ax=ax, x="x", y="y", c="z", colormap="jet", colorbar=False, kind="scatter", s=scatter_size)

        im = plt.gca().get_children()[0]
        cax = fig.add_axes([0.92, 0.1, 0.03, 0.8])
        fig.colorbar(im, cax=cax)
        ax.legend()
        plt.savefig("./images/auto_detect/%s" % name)


class PointExtractor:
    max_distance = 3

    def __init__(self, base_path, display=False):
        self.base_path = base_path[0]
        self.pc_radar = glob.glob("/%s/pcRadar/*.csv" % self.base_path)
        self.pc_lidar = glob.glob("/%s/lidar_pc/*.npz" % self.base_path)
        self.ims = glob.glob("/%s/rgbCamera/*.png" % self.base_path)
        self.im = cv2.imread(self.ims[0])[..., ::-1]
        self.patch_size = (5, 5)
        self.regions = base_path[1]
        self.pixels_locations = []

        pixels_locations_path = self.base_path + "/pixel_location.json"
        if os.path.exists(pixels_locations_path):
            self.pixels_locations = read_label(pixels_locations_path)
        else:
            self.pixels_locations = self.get_pixel_locations(image_path=self.ims[0])
            print(self.pixels_locations)
            save_obj(self.pixels_locations, path=pixels_locations_path)
        """
        drive display
        """
        if display:
            plt.close()
            r1, l1 = self.grab_pair()
            gs = gridspec.GridSpec(ncols=3, nrows=1)
            fig = plt.figure(figsize=(15, 5), dpi=200)
            ax = fig.add_subplot(gs[0, 0])
            ax.imshow(self.im)
            for pixel_location in self.pixels_locations:
                circle = plt.Circle((pixel_location[0], pixel_location[1]), 10, linewidth=1, color='r', fill=False)
                ax.add_patch(circle)
            ax.axis(False)

            ax = fig.add_subplot(gs[0, 1])
            ax.set_aspect('equal', 'box')
            ax.set_xlim(-20, 20)
            ax.set_ylim(5, 30)
            r1 = PointExtractor.apply_regions(r1, self.regions)
            scat = ax.scatter(x=r1["x"], y=r1["y"], c=r1["total_power"], cmap="jet")
            plt.colorbar(scat, ax=ax, orientation="horizontal")

            ax = fig.add_subplot(gs[0, 2])
            ax.set_xlim(-20, 20)
            ax.set_ylim(5, 30)
            ax.set_aspect('equal', 'box')
            l1 = PointExtractor.apply_regions(l1, self.regions)
            scat = ax.scatter(x=l1["x"], y=l1["y"], c=l1["total_power"], cmap="jet")
            plt.colorbar(scat, ax=ax, orientation="horizontal")

            plt.tight_layout()
            plt.savefig("./images/auto_detect/drive_display_%s.png" % os.path.split(self.base_path)[-1])
            plt.close()

    def get_exp_name(self):
        return os.path.split(self.base_path)[-1]

    def get_pixel_locations(self, image_path):

        window = Window()
        window.setGeometry(500, 300, 800, 600)
        window.show()
        window.loadImage(path=image_path)

        points = []
        window.set_postions(data=points)
        app.exec_()
        # sys.exit(app.exec_())
        return points

    def auto_detect_regions(self, data=None, fail_analysis=False):
        if data is None:
            radar, lidar = self.grab_pair()
        else:
            radar, lidar = data

        x_min, x_max = self.playground["x"][0], self.playground["x"][1]
        y_min, y_max = self.playground["y"][0], self.playground["y"][1]
        for x in np.linspace(x_min, x_max, np.abs(int((x_min - x_max) / self.patch_size[0]))):
            for y in np.linspace(y_min, y_max, np.abs(int((y_min - y_max) / self.patch_size[1]))):
                cur_region = {"x": (x, x + self.patch_size[0] * 1.2),
                              "y": (y, y + self.patch_size[1] * 1.2),
                              "z": (-10, 10)}
                res = PointExtractor.detect_peaks(dfs=[radar, lidar],
                                                  regions=[cur_region],
                                                  force_dispaly=True,
                                                  show=None if not fail_analysis else "./images/auto_detect/auto_region_detect_fail_%s_%s.png" % (
                                                      x, y))
                if len(res) >= 2:
                    self.regions.append(cur_region)
        return radar, lidar

    def grab_pair(self, radar_db=20, points_only=False):
        lidar = lidar_to_radar_coordinates(np.load(np.random.choice(self.pc_lidar, 1)[0])["arr_0"])
        lidar_df = pd.DataFrame()
        lidar_df["x"] = lidar[:, 0]
        lidar_df["y"] = lidar[:, 1]
        lidar_df["z"] = lidar[:, 2]
        lidar_df["total_power"] = lidar_df["z"]
        lidar_df = lidar_df[lidar_df["z"] >= -1.6].reset_index(drop=True)
        lidar_df = lidar_df[lidar_df["z"] <= 1].reset_index(drop=True)

        radar = pd.read_csv(np.random.choice(self.pc_radar, 1)[0])
        radar["total_power"] = db(np.abs(radar["value"].values ** 2 + 1j * radar["power"].values ** 2)) - db(
            radar["noise"])

        # radar = radar[radar["total_power"] >= radar_db].reset_index(drop=True)
        if len(radar) == 0 or len(lidar_df) == 0:
            print("Some empty data")
            exit()
        if points_only:
            return radar[["x", "y", "z"]], lidar_df[["x", "y", "z"]]
        return radar, lidar_df

    @staticmethod
    def apply_regions(df, regions):
        for region in regions:
            region = {"x": (region[0], region[1]),
                      "y": (region[2], region[3]),
                      "z": (region[4], region[5])}
            for k, v in region.items():
                df = df[(df[k] >= v[0]) & (df[k] <= v[1])].reset_index(drop=True)
        return df

    def detect_peaks(self, sensors, regions, peak="total_power", show=None, force_dispaly=False):
        if show:
            plt.close()
            gs = gridspec.GridSpec(ncols=2, nrows=1)
            fig = plt.figure(figsize=(20, 12))
        all_points = pd.DataFrame()
        for region in regions:
            region = {"x": (region[0], region[1]),
                      "y": (region[2], region[3]),
                      "z": (region[4], region[5])}
            region_points = pd.DataFrame()
            for idx, sensor in enumerate(sensors):
                avg_point = pd.DataFrame()
                # Avg measurments
                for avg_idx in range(AVG_FRAMES):
                    cur_df = self.grab_pair()[idx]
                    for k, v in region.items():  # Filter FOV
                        cur_df = cur_df[(cur_df[k] >= v[0]) & (cur_df[k] <= v[1])].reset_index(drop=True)
                    if len(cur_df) == 0:
                        continue
                    avg_point = avg_point.append(cur_df.iloc[cur_df["total_power"].argmax()])
                max_point = avg_point.mean()
                max_point.loc["idx"] = idx
                region_points = region_points.append(max_point[["x", "y", "z", "idx"]], ignore_index=True)
                if show:
                    ax = fig.add_subplot(gs[0, idx])
                    ax.set_aspect('equal', 'box')
                    circle = plt.Circle((max_point["x"], max_point["y"]), 1, linewidth=3, color='r', fill=False)
                    ax.add_patch(circle)
                    ax.set_title(str(max_point[["x", "y", "z"]].round(3)))
                    cur_df.plot(ax=ax, x="x", y="y", c=peak, colormap="jet", kind="scatter")

            # if len(region_points) >= 2:
            #     d = np.linalg.norm((region_points.iloc[0] - region_points.iloc[1])[["x", "y"]])
            #     if d <= 2:
            all_points = all_points.append(region_points)

        if len(all_points) > 1:
            d = np.linalg.norm((all_points.iloc[0] - all_points.iloc[1])[["x", "y"]])
            plt.suptitle("3D Distance between points=%.2fm" % d)

        def show_results():
            if show is not None:
                if type(show) == str:
                    plt.savefig(show)
                    plt.close()
                else:
                    plt.show()

        if force_dispaly:
            show_results()

        if len(all_points) > 1:
            d = np.linalg.norm((all_points.iloc[0] - all_points.iloc[1])[["x", "y"]])
            if d > PointExtractor.max_distance:
                print("Distance too high", d)
                return pd.DataFrame()
            if not force_dispaly:
                show_results()
            return all_points
        print("len all_points=1")
        return pd.DataFrame()

    def detect_peaks_from_image(self):

        return


class Evaluator:
    def __init__(self, base_path):
        self.base_path = base_path
        self.point_extractor = PointExtractor(base_path=base_path)

    def eval_lidar2radar(self, params):
        points = self.point_extractor.detect_peaks(sensors=[RADAR, LIDAR],
                                                   regions=self.point_extractor.regions,
                                                   show="./images/auto_detect/eval.png", force_dispaly=True)

        print("eval_lidar2radar: points.shape", points.shape)
        r_points = points[points["idx"] == 0][["x", "y", "z"]]
        l_points = points[points["idx"] == 1][["x", "y", "z"]]

        auto_calibrator = AutoCalibrator(radar_points=r_points, lidar_points=l_points)
        result = auto_calibrator.cali_func(params=params)

        auto_calibrator.compare_calibration(r_points, l_points, calibrations=[params],
                                            name="comp_cali_%s" % os.path.split(self.base_path[0])[-1])

        print(result)


def main():
    """
    Find points to Optimize
    """
    # configs
    num_frames_per_recording = 10

    load = 0
    points = pd.DataFrame()
    if not load:
        for record in tqdm(recordings):
            pc = PointExtractor(base_path=record, display=True)
            for i in range(num_frames_per_recording):
                tmp = pc.detect_peaks(sensors=[RADAR, LIDAR],
                                      regions=pc.regions, force_dispaly=True,
                                      show=None if i == 600 else "./images/auto_detect/%s_%s.png" % (
                                          os.path.split(record[0])[-1], i))

                tmp["exp"] = pc.get_exp_name()
                points = points.append(tmp)

    points = points.reset_index(drop=True)
    if load:
        points = pd.read_csv("./points.csv")
    else:
        points.to_csv("./points_%d.csv" % len(recordings))

    r_points = points[points["idx"] == 0][["x", "y", "z"]]
    l_points = points[points["idx"] == 1][["x", "y", "z"]]

    s = np.arange(l_points.shape[0])
    np.random.shuffle(s)

    r_points = r_points.iloc[s]
    l_points = l_points.iloc[s]

    ac = AutoCalibrator(radar_points=r_points, lidar_points=l_points)
    optimized_params = ac.calibrate()

    """
    Evaluate
    """
    for record in recordings:
        evaluator = Evaluator(base_path=record)
        evaluator.eval_lidar2radar(params=optimized_params)


recordings = [
    # ("/mnt/hdd/new_lidar_drives/200901_cali_left_W92_M15_RF1", [(-15, 5, 15, 25, -10, 10)]),
    # ("/workspace/HDD/new_lidar_drives/200903_corner_right_W92_M15_RF1", [(0, 15, 10, 20, -10, 10)]),
    ("/workspace/HDD/new_lidar_drives/200903_corner_front_W92_M15_RF1", [(-5, 5, 20, 25, -10, 10)]),
    # ("/workspace/HDD/new_lidar_drives/200902_lidar_Cali_right_W92_M15_RF1", [(0, 10, 16, 22, -10, 10)])
]

if __name__ == '__main__':
    main()
