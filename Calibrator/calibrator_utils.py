import glob
import json
import linecache
import math
import sys
import traceback
import os
import cv2
from matplotlib import gridspec, colors
# import mayavi.mlab as mlab
import numpy as np
from matplotlib import lines
from scipy.ndimage.filters import maximum_filter
from scipy.ndimage.morphology import generate_binary_structure, binary_erosion
from scipy import signal
from scipy.optimize import curve_fit
from scipy.spatial.distance import cdist
from tqdm import tqdm
import matplotlib.pyplot as plt
from Tools.LshapeFitting.rectangle_fitting import LShapeFitting
from mpl_toolkits.axes_grid1 import make_axes_locatable

color_map = {  # bgr
    0: [0, 0, 0],
    1: [0, 0, 255],
    10: [245, 150, 100],
    11: [245, 230, 100],
    13: [250, 80, 100],
    15: [150, 60, 30],
    16: [255, 0, 0],
    18: [180, 30, 80],
    20: [255, 0, 0],
    30: [30, 30, 255],
    31: [200, 40, 255],
    32: [90, 30, 150],
    40: [255, 0, 255],
    44: [255, 150, 255],
    48: [75, 0, 75],
    49: [75, 0, 175],
    50: [0, 200, 255],
    51: [50, 120, 255],
    52: [0, 150, 255],
    60: [170, 255, 150],
    70: [0, 175, 0],
    71: [0, 60, 135],
    72: [80, 240, 150],
    80: [150, 240, 255],
    81: [0, 0, 255],
    99: [255, 255, 50],
    252: [245, 150, 100],
    256: [255, 0, 0],
    253: [200, 40, 255],
    254: [30, 30, 255],
    255: [90, 30, 150],
    257: [250, 80, 100],
    258: [180, 30, 80],
    259: [255, 0, 0]}

labels_map = {
    0: "unlabeled",
    1: "outlier",
    10: "car",
    11: "bicycle",
    13: "bus",
    15: "motorcycle",
    16: "on-rails",
    18: "truck",
    20: "other-vehicle",
    30: "person",
    31: "bicyclist",
    32: "motorcyclist",
    40: "road",
    44: "parking",
    48: "sidewalk",
    49: "other-ground",
    50: "building",
    51: "fence",
    52: "other-structure",
    60: "lane-marking",
    70: "vegetation",
    71: "trunk",
    72: "terrain",
    80: "pole",
    81: "traffic-sign",
    99: "other-object",
    252: "moving-car",
    253: "moving-bicyclist",
    254: "moving-person",
    255: "moving-motorcyclist",
    256: "moving-on-rails",
    257: "moving-bus",
    258: "moving-truck",
    259: "moving-other-vehicle"}

lut = np.zeros((300, 3), dtype=np.float32)
for k, v in color_map.items():
    lut[k] = v

from Calibrator.projection import toRAzEl

lshapefitting = LShapeFitting()


def func(x, a, b):
    return a * x + b


class Box3D(object):
    """
    Represent a 3D box corresponding to data in label.txt
    """

    def __init__(self, label_file_line):
        data = label_file_line.split(' ')
        data[1:] = [float(x) for x in data[1:]]

        self.type = data[0]
        self.truncation = data[1]
        self.occlusion = int(data[2])  # 0=visible, 1=partly occluded, 2=fully occluded, 3=unknown
        self.alpha = data[3]  # object observation angle [-pi..pi]

        # extract 2d bounding box in 0-based coordinates
        self.xmin = data[4]  # left
        self.ymin = data[5]  # top
        self.xmax = data[6]  # right
        self.ymax = data[7]  # bottom
        self.box2d = np.array([self.xmin, self.ymin, self.xmax, self.ymax])

        # extract 3d bounding box information
        self.h = data[8]  # box height
        self.w = data[9]  # box width
        self.l = data[10]  # box length (in meters)
        self.t = (data[11], data[12], data[13])  # location (x,y,z) in camera coord.
        self.ry = data[14]  # yaw angle (around Y-axis in camera coordinates) [-pi..pi]

    def in_camera_coordinate(self, is_homogenous=False):
        # 3d bounding box dimensions
        l = self.l
        w = self.w
        h = self.h

        # 3D bounding box vertices [3, 8]
        x = [l / 2, l / 2, -l / 2, -l / 2, l / 2, l / 2, -l / 2, -l / 2]
        y = [0, 0, 0, 0, -h, -h, -h, -h]
        z = [w / 2, -w / 2, -w / 2, w / 2, w / 2, -w / 2, -w / 2, w / 2]
        box_coord = np.vstack([x, y, z])

        # Rotation
        R = roty(self.ry)  # [3, 3]
        points_3d = R @ box_coord

        # Translation
        points_3d[0, :] = points_3d[0, :] + self.t[0]
        points_3d[1, :] = points_3d[1, :] + self.t[1]
        points_3d[2, :] = points_3d[2, :] + self.t[2]

        if is_homogenous:
            points_3d = np.vstack((points_3d, np.ones(points_3d.shape[1])))

        return points_3d


# =========================================================
# Projections
# =========================================================
# def project_velo_to_cam2(calib):
#     P_velo2cam_ref = np.vstack((calib['Tr_velo_to_cam'].reshape(3, 4), np.array([0., 0., 0., 1.])))  # velo2ref_cam
#     R_ref2rect = np.eye(4)
#     R0_rect = calib['R0_rect'].reshape(3, 3)  # ref_cam2rect
#     R_ref2rect[:3, :3] = R0_rect
#     P_rect2cam2 = calib['P2'].reshape((3, 4))
#     proj_mat = P_rect2cam2 @ R_ref2rect @ P_velo2cam_ref
#     return proj_mat


def project_cam2_to_velo(calib):
    R_ref2rect = np.eye(4)
    R0_rect = calib['R0_rect'].reshape(3, 3)  # ref_cam2rect
    R_ref2rect[:3, :3] = R0_rect
    R_ref2rect_inv = np.linalg.inv(R_ref2rect)  # rect2ref_cam

    # inverse rigid transformation
    velo2cam_ref = np.vstack((calib['Tr_velo_to_cam'].reshape(3, 4), np.array([0., 0., 0., 1.])))  # velo2ref_cam
    P_cam_ref2velo = np.linalg.inv(velo2cam_ref)

    proj_mat = R_ref2rect_inv @ P_cam_ref2velo

    return proj_mat


def project_to_image(points, proj_mat):
    """
    Apply the perspective projection
    Args:
        pts_3d:     3D points in camera coordinate [3, npoints]
        proj_mat:   Projection matrix [3, 4]
    """
    num_pts = points.shape[1]

    # Change to homogenous coordinate
    points = np.vstack((points, np.ones((1, num_pts))))
    points = proj_mat @ points
    points[:2, :] /= points[2, :]
    return points[:2, :]


def project_camera_to_lidar(points, proj_mat):
    """
    Args:
        points:     3D points in camera coordinate [3, npoints]
        proj_mat:   Projection matrix [3, 4]

    Returns:
        points in lidar coordinate:     [3, npoints]
    """
    num_pts = points.shape[1]
    # Change to homogenous coordinate
    points = np.vstack((points, np.ones((1, num_pts))))
    points = proj_mat @ points
    return points[:3, :]


def map_box_to_image(box, proj_mat):
    """
    Projects 3D bounding box into the image plane.
    Args:
        box (Box3D)
        proj_mat: projection matrix
    """
    # box in camera coordinate
    points_3d = box.in_camera_coordinate()

    # project the 3d bounding box into the image plane
    points_2d = project_to_image(points_3d, proj_mat)

    return points_2d


def load_label(label_filename):
    lines = [line.rstrip() for line in open(label_filename)]
    # load as list of Object3D
    objects = [Box3D(line) for line in lines]
    return objects


def get_3d_ax(fig, loc, x_left=-30, x_right=30, y_left=0, y_right=80, ax3d=True):
    labelpad = 5
    if ax3d:
        ax1 = fig.add_subplot(loc, projection='3d')
        ax1.set_xlim3d(x_left, x_right)
        ax1.set_ylim3d(y_left, y_right)
        ax1.set_zlim(-15, 15)
        ax1.set_zlabel('Z[m]', labelpad=labelpad)
        ax1.w_zaxis.pane.fill = False
        ax1.set_zticks([])
        ax1.w_xaxis.pane.fill = False
        ax1.w_yaxis.pane.fill = False
    else:
        ax1 = fig.add_subplot(loc)
        ax1.set_xlim(x_left, x_right)
        ax1.set_ylim(y_left, y_right)

    ax1.grid(False)
    ax1.set_xlabel('X [m]\n', labelpad=labelpad)
    ax1.set_ylabel('Y [m]', labelpad=labelpad)
    return ax1


def get_pts_extractrd_from_pc(x1, y1, x2, y2, imgfov_pc_pixel, imgfov_pc_velo):
    left = np.logical_and(imgfov_pc_pixel[0, :] > x1, imgfov_pc_pixel[0, :] < x2)
    right = np.logical_and(imgfov_pc_pixel[1, :] > y1, imgfov_pc_pixel[1, :] < y2)
    mask_points_of_bb = np.logical_and(left, right)
    pts_extracted_from_pc = imgfov_pc_velo[mask_points_of_bb]
    return pts_extracted_from_pc


def save_obj(obj, path):
    with open(path, 'w') as fp:
        json.dump(obj, fp, indent=2)


def load_image(img_filename):
    return cv2.imread(img_filename)


def load_velo_scan(velo_filename):
    scan = np.fromfile(velo_filename, dtype=np.float32)
    scan = scan.reshape((-1, 4))
    return scan


def read_calib_file(filepath):
    """
    Read in a calibration file and parse into a dictionary.
    Ref: https://github.com/utiasSTARS/pykitti/blob/master/pykitti/utils.py
    """
    data = {}
    with open(filepath, 'r') as f:
        for line in f.readlines():
            line = line.rstrip()
            if len(line) == 0: continue
            key, value = line.split(':', 1)
            # The only non-float values in these files are dates, which
            # we don't care about anyway
            try:
                data[key] = np.array([float(x) for x in value.split()])
            except ValueError:
                pass

    return data


def roty(t):
    """
    Rotation about the y-axis.
    """
    c = np.cos(t)
    s = np.sin(t)
    return np.array([[c, 0, s],
                     [0, 1, 0],
                     [-s, 0, c]])


def draw_gt_boxes3d(gt_boxes3d, fig, color=(1, 1, 1)):
    """
    Draw 3D bounding boxes
    Args:
        gt_boxes3d: numpy array (3,8) for XYZs of the box corners
        fig: figure handler
        color: RGB value tuple in range (0,1), box line color
    """
    for k in range(0, 4):
        i, j = k, (k + 1) % 4
        mlab.plot3d([gt_boxes3d[0, i], gt_boxes3d[0, j]], [gt_boxes3d[1, i], gt_boxes3d[1, j]],
                    [gt_boxes3d[2, i], gt_boxes3d[2, j]], tube_radius=None, line_width=2, color=color, figure=fig)

        i, j = k + 4, (k + 1) % 4 + 4
        mlab.plot3d([gt_boxes3d[0, i], gt_boxes3d[0, j]], [gt_boxes3d[1, i], gt_boxes3d[1, j]],
                    [gt_boxes3d[2, i], gt_boxes3d[2, j]], tube_radius=None, line_width=2, color=color, figure=fig)

        i, j = k, k + 4
        mlab.plot3d([gt_boxes3d[0, i], gt_boxes3d[0, j]], [gt_boxes3d[1, i], gt_boxes3d[1, j]],
                    [gt_boxes3d[2, i], gt_boxes3d[2, j]], tube_radius=None, line_width=2, color=color, figure=fig)
    return fig


def draw_projected_box3d(image, qs, color=(255, 255, 255), thickness=1):
    qs = qs.astype(np.int32).transpose()
    for k in range(0, 4):
        # http://docs.enthought.com/mayavi/mayavi/auto/mlab_helper_functions.html
        i, j = k, (k + 1) % 4
        cv2.line(image, (qs[i, 0], qs[i, 1]), (qs[j, 0], qs[j, 1]), color, thickness, cv2.LINE_AA)

        i, j = k + 4, (k + 1) % 4 + 4
        cv2.line(image, (qs[i, 0], qs[i, 1]), (qs[j, 0], qs[j, 1]), color, thickness, cv2.LINE_AA)

        i, j = k, k + 4
        cv2.line(image, (qs[i, 0], qs[i, 1]), (qs[j, 0], qs[j, 1]), color, thickness, cv2.LINE_AA)

    return image


def draw_lidar(pc, color=None, fig=None, bgcolor=(0, 0, 0), pts_scale=1, pts_mode='point', pts_color=None):
    """
    Add lidar points
    Args:
        pc: point cloud xyz [npoints, 3]
        color:
        fig: fig handler
    Returns:

    """
    ''' Draw lidar points
    Args:
        pc: numpy array (n,3) of XYZ
        color: numpy array (n) of intensity or whatever
        fig: mayavi figure handler, if None create new one otherwise will use it
    Returns:
        fig: created or used fig
    '''
    if fig is None:
        fig = mlab.figure(figure=None, bgcolor=bgcolor, fgcolor=None, engine=None, size=(1600, 1000))
    if color is None:
        color = pc[:, 2]

    # add points
    mlab.points3d(pc[:, 0], pc[:, 1], pc[:, 2], color, color=pts_color, mode=pts_mode, colormap='gnuplot',
                  scale_factor=pts_scale, figure=fig)

    # # draw origin
    # mlab.points3d(0, 0, 0, color=(1, 1, 1), mode='sphere', scale_factor=0.2)

    # draw axis
    axes = np.array([
        [2., 0., 0., 0.],
        [0., 2., 0., 0.],
        [0., 0., 2., 0.],
    ], dtype=np.float64)
    mlab.plot3d([0, axes[0, 0]], [0, axes[0, 1]], [0, axes[0, 2]], color=(1, 0, 0), tube_radius=None, figure=fig)
    mlab.plot3d([0, axes[1, 0]], [0, axes[1, 1]], [0, axes[1, 2]], color=(0, 1, 0), tube_radius=None, figure=fig)
    mlab.plot3d([0, axes[2, 0]], [0, axes[2, 1]], [0, axes[2, 2]], color=(0, 0, 1), tube_radius=None, figure=fig)

    return fig


def rotate(origin, point, angle):
    """
    Rotate a point counterclockwise by a given angle around a given origin.

    The angle should be given in radians.
    """
    ox, oy = origin
    px, py = point

    qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
    qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
    return qx, qy


def loadParams(file):
    """Load saved camera calibration parameters from file"""
    with open(file) as json_file:
        params = json.load(json_file)
    K = np.array(params['K'])
    D = np.array(params['D'])
    return K, D


def filter_fov_az(pts, az, x_idx=0, y_idx=1):
    if az == 0:
        return pts
    pts = pts[pts[:, y_idx] > 0]
    r, Az, El = toRAzEl(x=pts[:, x_idx], y=pts[:, y_idx], z=pts[:, 2])
    mask = np.logical_and(Az < (az // 2), (-az // 2) < Az)
    return pts[mask, :], mask


def filter_fov_az_csv(pts, deg, key):
    if deg == 0:
        return pts
    mask = np.logical_and(np.rad2deg(np.arcsin(pts[key])) < (deg // 2), (-deg // 2) < np.rad2deg(np.arcsin(pts[key])))
    pts = pts[mask]
    return pts


def inverse_db(arr):
    print("inverse_db", max(arr))
    return 10 ** (arr / 10)


def db(array):
    return 10 * np.log10(array)


def PrintException():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    tb = traceback.format_exc()
    print(tb)
    print('EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj))


def read_label(path):
    with open(path, "r") as fp:
        frame_data = json.load(fp)
    return frame_data


def get_rot_mat(theta, alpha=-np.pi / 2):
    heading = np.deg2rad(theta)
    c = np.cos(heading + alpha)
    s = np.sin(heading + alpha)
    rot_mat = [[c, -s, 0], [s, c, 0], [0, 0, 1]]
    return rot_mat


def build_bbox(center, heading, alpha=-np.pi / 2, l=4, w=2, h=1.8, mat=None):
    if mat is None:
        rot_mat = get_rot_mat(heading, alpha=alpha)
    else:
        rot_mat = mat
    # theta = heading+alpha
    # rot_mat = get_rotation_matrix(theta_z=theta, theta_y=0, theta_x=0)
    p1 = center + np.dot(rot_mat, [w / 2, -l / 2, h / 2])
    p2 = center + np.dot(rot_mat, [w / 2, l / 2, h / 2])
    p3 = center + np.dot(rot_mat, [-w / 2, l / 2, h / 2])
    p4 = center + np.dot(rot_mat, [-w / 2, -l / 2, h / 2])
    return np.vstack((p1, p2, p3, p4, p1))


def build_rec_from_center(pts_extracted_from_pc, center, x_idx=0, y_idx=1):
    # Get Heading
    def func(x, a, b):
        return a * x + b

    x_data = pts_extracted_from_pc[:, x_idx]
    y_data = pts_extracted_from_pc[:, y_idx]
    popt, pcov = curve_fit(func, x_data, y_data)

    heading = np.rad2deg(np.arctan(popt[0]))  # + np.rad2deg(-np.pi / 2)
    return build_bbox(center=center, heading=heading)


def get_L_fitting(pts_extracted_from_pc, center, obj, x_idx=0, y_idx=1):
    x_data = pts_extracted_from_pc[:, x_idx]
    y_data = pts_extracted_from_pc[:, y_idx]
    # popt, pcov = curve_fit(func, x_data, y_data)
    # ret_dict["curve_fit_heading"] = np.rad2deg(np.arctan(popt[0]))
    # meta_txt += " var-x " + str(var_x) + ", var-y " + str(var_y) + "\n"
    # rects, idsets = lshapefitting.fitting_single(x_data, y_data)
    rects, idsets = lshapefitting.fitting(x_data, y_data)
    if len(rects) == 0:
        print("get_L_fitting: L-Fit Failed")
        return [False]
    # elif len(rects) > 0:
    #     print("get_L_fitting: More than one rectangle")

    distances = []
    for rec_idx, r in enumerate(rects):
        r.calc_rect_contour()
        c1 = np.array([rects[rec_idx].rect_c_x[1], rects[rec_idx].rect_c_y[1]])
        c2 = np.array([rects[rec_idx].rect_c_x[2], rects[rec_idx].rect_c_y[2]])
        c3 = np.array([rects[rec_idx].rect_c_x[3], rects[rec_idx].rect_c_y[3]])
        dx_c23 = rects[rec_idx].rect_c_x[2] - rects[rec_idx].rect_c_x[3]
        dy_c23 = rects[rec_idx].rect_c_y[2] - rects[rec_idx].rect_c_y[3]
        # holds dist and points
        dist1 = [float(cdist([c2], [c3])[0]), dx_c23, dy_c23]

        dx_c21 = rects[rec_idx].rect_c_x[2] - rects[rec_idx].rect_c_x[1]
        dy_c21 = rects[rec_idx].rect_c_y[2] - rects[rec_idx].rect_c_y[1]
        dist2 = [float(cdist([c2], [c1])[0]), dx_c21, dy_c21]

        distances.append((rec_idx, dist1, dist2))

    max_dist = max(distances, key=lambda x: x[1][0] * x[2][0])
    max_dist_idx = max_dist[0]
    dist1 = max_dist[1]
    dist2 = max_dist[2]

    if obj.min_width < dist1[0] < obj.max_width and obj.min_length < dist2[0] < obj.max_length:
        width = dist1[0]
        length = dist2[0]
        # heading = np.rad2deg(np.arctan(dist2[2] / dist2[1]))
    elif obj.min_width < dist2[0] < obj.max_width and obj.min_length < dist1[0] < obj.max_length:
        width = dist2[0]
        length = dist1[0]
    else:
        return [False]  # Not good L-fitting

    if dist1[0] < dist2[0]:
        heading = np.rad2deg(np.arctan(dist2[2] / dist2[1]))
    else:
        heading = np.rad2deg(np.arctan(dist1[2] / dist2[1]))

    center[:2] = rects[max_dist_idx].get_center()
    return [True, center, round(length, 2), round(width, 2), round(obj.height, 2), heading]


def build_rec_from_cluster_old2(pts_extracted_from_pc, center, obj, x_idx=0, y_idx=1):
    ret_dict = {}
    meta_txt = ""
    x_data = pts_extracted_from_pc[:, x_idx]
    y_data = pts_extracted_from_pc[:, y_idx]
    var_x = round(np.var(x_data), 2)
    var_y = round(np.var(y_data), 2)
    popt, pcov = curve_fit(func, x_data, y_data)
    ret_dict["curve_fit_heading"] = np.rad2deg(np.arctan(popt[0]))

    meta_txt += " var-x " + str(var_x) + ", var-y " + str(var_y) + "\n"

    # rects, idsets = lshapefitting.fitting_single(x_data, y_data)
    rects, idsets = lshapefitting.fitting(x_data, y_data)
    if len(rects) == 0:
        print("build_rec_from_cluster: L-Fit Failed")
        return

    for r in rects:
        r.calc_rect_contour()

    ret_dict["rects"] = rects

    c1 = np.array([rects[0].rect_c_x[1], rects[0].rect_c_y[1]])
    c2 = np.array([rects[0].rect_c_x[2], rects[0].rect_c_y[2]])
    c3 = np.array([rects[0].rect_c_x[3], rects[0].rect_c_y[3]])
    dx_c23 = rects[0].rect_c_x[2] - rects[0].rect_c_x[3]
    dy_c23 = rects[0].rect_c_y[2] - rects[0].rect_c_y[3]
    dist1 = [float(cdist([c2], [c3])[0]), dx_c23, dy_c23]

    dx_c21 = rects[0].rect_c_x[2] - rects[0].rect_c_x[1]
    dy_c21 = rects[0].rect_c_y[2] - rects[0].rect_c_y[1]
    dist2 = [float(cdist([c2], [c1])[0]), dx_c21, dy_c21]

    meta_txt += " rect dims dist1:" + str(round(dist1[0], 2)) + ", dist2:" + str(round(dist2[0], 2)) + "\n"
    if obj.min_width < dist1[0] < obj.max_width and obj.min_length < dist2[0] < obj.max_length:
        meta_txt += "Should be easy - all dims matched\n"
        width = dist1[0]
        length = dist2[0]
        # heading = np.rad2deg(np.arctan(dist2[2] / dist2[1]))
    elif obj.min_width < dist2[0] < obj.max_width and obj.min_length < dist1[0] < obj.max_length:
        meta_txt += "Should be easy - all dims matched\n"
        width = dist2[0]
        length = dist1[0]
    elif obj.min_width < dist1[0] < obj.max_width:
        meta_txt += "width match, length don't\n"
        width = dist1[0]
        length = obj.length
    elif obj.min_length < dist2[0] < obj.max_length:
        meta_txt += "length match, width don't\n"
        width = obj.width
        length = dist2[0]
    elif obj.min_width < dist2[0] < obj.max_width:
        meta_txt += "width match, length don't\n"
        width = dist2[0]
        length = obj.length
    elif obj.min_length < dist1[0] < obj.max_length:
        meta_txt += "length match, width don't\n"
        width = obj.width
        length = dist1[0]
    else:
        meta_txt += "Oh no!, no one is good enough\n"
        if np.random.randint(10) % 2 == 0:
            width = dist1[0]
            length = dist2[0]
        else:
            width = dist2[0]
            length = dist1[0]

    if dist1[0] < dist2[0]:
        heading = np.rad2deg(np.arctan(dy_c21 / dx_c21))
    else:
        heading = np.rad2deg(np.arctan(dy_c23 / dx_c23))
    l_w_ratio = round(length / (width + 0.000001), 2)

    # add 90 or not?
    dist_ratio = dist1[0] / dist2[0] + 0.000001
    meta_txt += "dist ration " + str(round(dist_ratio, 2)) + "\n"
    ret_dict["l_w_ratio"] = l_w_ratio

    if obj.min_width < width < obj.max_width and obj.min_length < length < obj.max_length:
        meta_txt += "All dims seems fine"
    elif 75 < np.abs(heading) < 105:
        meta_txt += "\n probably front/back l: " + str(round(length, 2)) + ", w: " + str(round(width, 2))
    else:
        if obj.min_width < width < obj.max_width:
            meta_txt += "width-seems-good, fixing length"
            length = obj.desired_l_w_ratio * width
        elif obj.min_length < length < obj.max_length:
            meta_txt += "length-seems-good, fixing width"
            width = length / obj.desired_l_w_ratio
        else:
            meta_txt += "dims are bad l: " + str(round(length, 2)) + ", w: " + str(round(width, 2))
            width = obj.width
            length = obj.length

    length = np.clip(length, obj.min_length, obj.max_length)
    width = np.clip(width, obj.min_width, obj.max_width)

    center[:2] = rects[0].get_center()
    # length, width = dist1[0], dist2[0]
    return center, round(length, 2), round(width, 2), round(obj.height, 2), heading, meta_txt, ret_dict


def save_label(label_path, camera_label_rect, save=False):
    if save:
        for k in camera_label_rect:
            k.pop('ret_dict', None)
            k.pop('instance_idx', None)
            k.pop('text', None)
        save_obj(obj=camera_label_rect, path=label_path)


def abline(ax, a, b, c):
    """Plot a line from slope and intercept"""
    axes = plt.gca()
    x_vals = np.array(axes.get_xlim())
    y_vals = b + a * (x_vals - c)
    ax.plot(x_vals, y_vals, '--')


def build_rec_from_cluster_old(pts_extracted_from_pc, center, x_idx=0, y_idx=1):
    # Get Heading
    def func(x, a, b):
        return a * x + b

    ret_dict = {}

    length, width, height = 4, 1.8, 1.8

    # dist_between_points = cdist(XA=pts_extracted_from_pc[:, :3], XB=pts_extracted_from_pc[:, :3])
    x_points = pts_extracted_from_pc[:, 0]
    var_x = round(np.var(x_points), 2)

    y_points = pts_extracted_from_pc[:, 1]
    var_y = round(np.var(y_points), 2)

    x_data = pts_extracted_from_pc[:, x_idx]
    y_data = pts_extracted_from_pc[:, y_idx]
    # x_data = np.hstack([x_data, x_data])
    # y_data = np.hstack([y_data, y_data])
    # heading = np.mean(np.rad2deg(np.arctan(popt[0])) + np.rad2deg(np.arctan(-1 / popt[0])))
    popt, pcov = curve_fit(func, x_data, y_data)
    heading = np.rad2deg(np.arctan(popt[0]))  # + np.rad2deg(-np.pi / 2)

    ret_dict["popt"] = popt
    # Edge cases

    meta_txt = " var-x " + str(var_x) + " var-y " + str(var_y) + "\n"
    if -10 < heading < 10:
        dist_between_points = cdist(XA=pts_extracted_from_pc[:, :3], XB=pts_extracted_from_pc[:, :3])
        max_range_between_points = np.max(dist_between_points)
        meta_txt += "max_range_between_points " + str(round(max_range_between_points, 2)) + "m,"
        if 1.5 < max_range_between_points < 2.2:
            width = max_range_between_points
            length = w_l_ratio * width
            meta_txt += " front/back "
            rot_mat = get_rot_mat(heading)
            # center = center + [length / 2, 0, 0]

        elif 3 < max_range_between_points < 4.5:
            length = max_range_between_points
            width = length / w_l_ratio
            if var_y > 0.4:
                heading = heading - 90
            meta_txt += " side"
            rot_mat = get_rot_mat(heading)
            # center = center + [width / 2, 0, 0]

    # Length
    end_point = center + np.array([np.cos(np.deg2rad(heading)), np.sin(np.deg2rad(heading)), 0]) * 3
    start_point = center + np.array([np.cos(np.deg2rad(heading)), np.sin(np.deg2rad(heading)), 0]) * (-3)
    ret_dict["start_point"] = start_point
    ret_dict["end_point"] = end_point

    u = start_point
    v = end_point
    n = v - u
    n /= np.linalg.norm(n, 2)
    left = np.dot(pts_extracted_from_pc[:, :3] - u, n)
    right = np.array([n] * pts_extracted_from_pc.shape[0])
    result = (left * right.T).T + u
    ret_dict["projected_points"] = result

    dist_between_points = cdist(XA=result, XB=result)
    max_range_between_points = np.max(dist_between_points)
    length = max_range_between_points

    # Width
    end_point = center + np.array([np.cos(np.deg2rad(heading + 90)), np.sin(np.deg2rad(heading + 90)), 0]) * 3
    start_point = center + np.array([np.cos(np.deg2rad(heading + 90)), np.sin(np.deg2rad(heading + 90)), 0]) * (-3)
    ret_dict["start_point"] = start_point
    ret_dict["end_point"] = end_point

    u = start_point
    v = end_point
    n = v - u
    n /= np.linalg.norm(n, 2)
    left = np.dot(pts_extracted_from_pc[:, :3] - u, n)
    right = np.array([n] * pts_extracted_from_pc.shape[0])
    result = (left * right.T).T + u
    ret_dict["projected_points"] = result

    dist_between_points = cdist(XA=result, XB=result)
    max_range_between_points = np.max(dist_between_points)
    width = max_range_between_points

    # ret_dict["project_points_on_heading"] = result

    # determine_center
    center[0] = np.mean((np.max(x_points), np.min(x_points)))
    center[1] = np.mean((np.max(y_points), np.min(y_points)))

    # ensure sanity
    length = np.clip(length, 2, 5.5)
    width = np.clip(width, 1.4, 2.5)

    # Fix Heading
    return center, round(length, 2), round(width, 2), round(height, 2), heading, meta_txt, ret_dict


def draw_label_on_image(frame_data, im, classes_map, class_key="cat"):
    for cur_label in frame_data:
        obj = cur_label[class_key]
        score = cur_label['score']
        x1, y1, x2, y2 = cur_label['bbox']
        # color = self.other_colors[cat_id]
        # if obj in classes_map:
        #     color = classes_map[obj].color
        # else:
        #     color = (0, 255, 0)

        color = (0, 0, 255)
        cv2.rectangle(im, (x1, y1), (x2, y2), color, 2)
        cv2.putText(im, '{}, {:.3f}'.format(obj, score),
                    (x1, y1 + 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                    color, 1)

    return im


def get_mask_from_label(frame_data, im):
    colors = {"car": (0, 0, 255), "truck": (135, 206, 250),
              "bus": (100, 149, 237),
              "pedestrian": (255, 165, 0), "person": (255, 165, 0), "bicycle": (170, 0, 255),
              "motorcycle": (216, 0, 155), "forbidden_pedestrian": (238, 130, 238)}

    mask = np.zeros(im.shape[:2], dtype=np.float32)
    vertices = []
    for cur_label in frame_data:
        obj = cur_label['cat']
        x1, y1, x2, y2 = cur_label['bbox']
        if obj not in colors:
            continue
        mask[y1:y2, x1:x2] = True
        vertices.append((x1, y1, x2, y2))
    return np.array(vertices)


def apply_corridor(pts, x):
    new_points = pts[np.logical_and(pts[:, 0] > -x, pts[:, 0] < x)]
    return new_points


def cut_by_range(pts, max_range, min_range, index=3):
    x = pts[np.linalg.norm(pts[:, :index], axis=-1) < max_range]
    x = x[np.linalg.norm(x[:, :index], axis=-1) > min_range]
    return x


def make_video_from_dir(src_dir, out_dir, video_name, fr=5):
    fourcc = cv2.VideoWriter_fourcc(*'MP4V')
    flag = False
    for i in tqdm(sorted(glob.glob(src_dir + "/*.png"))):
        cur = cv2.imread(i)
        if not flag:
            height, width = cur.shape[:2]
            video = cv2.VideoWriter(os.path.join(out_dir, str(video_name) + '.mp4'), fourcc, fr, (width, height))
            flag = True
        video.write(cur)
    video.release()
    return


def project_velo_to_cam2(K):
    swap_axes = np.array(
        [[0, -1, 0, 0],
         [0, 0, -1, 0],
         [1, 0, 0, 0],
         [0, 0, 0, 1]])
    R_ref2rect = np.eye(4)

    P_rect2cam2 = np.zeros((3, 4), dtype=np.float)
    P_rect2cam2[:, :3] = K
    proj_mat = P_rect2cam2 @ R_ref2rect @ swap_axes  # @ P_velo2cam_ref
    return proj_mat


def apply_6DOF(pts, yaw, pitch, roll, tx, ty, tz):
    rot = get_rotation_matrix(theta_z=np.deg2rad(yaw),
                              theta_x=np.deg2rad(pitch),
                              theta_y=np.deg2rad(roll))
    # print("rot", rot.shape)
    camera_lidar_mat = np.zeros((3, 4), dtype=np.float32)
    camera_lidar_mat[:, :3] = rot
    camera_lidar_mat[:, 3] = np.array([tx,
                                       ty,
                                       tz])

    pts_velo_lidar = np.vstack((pts.T, np.ones((1, pts.shape[0]))))
    return (camera_lidar_mat @ pts_velo_lidar).T, rot


def lidar_to_radar_coordinates(points):
    theta = np.deg2rad(90)
    rotation_matrix = np.array([[np.cos(theta), -np.sin(theta), 0],
                                [np.sin(theta), np.cos(theta), 0],
                                [0, 0, 1]])

    return np.matmul(rotation_matrix, points.T).T


def radar_to_lidar_coordinates(points):
    theta = np.deg2rad(-90)
    rotation_matrix = np.array([[np.cos(theta), -np.sin(theta), 0],
                                [np.sin(theta), np.cos(theta), 0],
                                [0, 0, 1]])

    return np.matmul(rotation_matrix, points.T).T

def get_orientation_fix_matrix(lidar_heading):
    return


def plot_lines(cur_ax):
    deg2check = 120
    d1 = (180 - deg2check) // 2
    d2 = 180 - d1

    l1 = lines.Line2D((0, 30 * np.cos(np.deg2rad(d1))), (0, 30 * np.sin(np.deg2rad(d1))), color='green', linewidth=2)
    l2 = lines.Line2D((0, 30 * np.cos(np.deg2rad(d2))), (0, 30 * np.sin(np.deg2rad(d2))), color='green', linewidth=2)
    cur_ax.add_line(l1)
    cur_ax.add_line(l2)
    deg2check = 80
    d1 = (180 - deg2check) // 2
    d2 = 180 - d1

    l1 = lines.Line2D((0, 30 * np.cos(np.deg2rad(d1))), (0, 30 * np.sin(np.deg2rad(d1))), color='red', linewidth=2)
    l2 = lines.Line2D((0, 30 * np.cos(np.deg2rad(d2))), (0, 30 * np.sin(np.deg2rad(d2))), color='red', linewidth=2)
    cur_ax.add_line(l1)
    cur_ax.add_line(l2)
    deg2check = 100
    d1 = (180 - deg2check) // 2
    d2 = 180 - d1
    l1 = lines.Line2D((0, 30 * np.cos(np.deg2rad(d1))), (0, 30 * np.sin(np.deg2rad(d1))), color='yellow', linewidth=2)
    l2 = lines.Line2D((0, 30 * np.cos(np.deg2rad(d2))), (0, 30 * np.sin(np.deg2rad(d2))), color='yellow', linewidth=2)
    cur_ax.add_line(l1)
    cur_ax.add_line(l2)


def rectify(img, K, D, fish=True, DIM=(1824, 940), balance=0):
    """Undistort camera image based on saved parameters.
    Accept image as image array (cv2 format; BGR)
    Returns undistorted image array (cv2 format).
    """
    # in case image is not cv2 format?
    # img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)

    D = D[0][:4]
    dim1 = img.shape[:2][::-1]  # dim1 is the dimension of input image to un-distort
    assert dim1[0] / dim1[1] == DIM[0] / DIM[1], "Image to undistort needs to have same aspect ratio as the ones used in calibration"
    scaled_K = K * dim1[0] / DIM[0]  # The values of K is to scale with image dimension.
    scaled_K[2][2] = 1.0  # Except that K[2][2] is always 1.0

    if fish is True:
        new_K = cv2.fisheye.estimateNewCameraMatrixForUndistortRectify(scaled_K, D, dim1, np.eye(3), balance=balance)
        new_K[0, 2] = K[0, 2]
        new_K[1, 2] = K[1, 2]  # manually set centerPrinicipalPoint=True
        map1, map2 = cv2.fisheye.initUndistortRectifyMap(scaled_K, D, np.eye(3), new_K, dim1, cv2.CV_32FC1)
    else:
        centerPrinicipalPoint = True
        new_K, roi = cv2.getOptimalNewCameraMatrix(scaled_K, D, dim1, balance, dim1, centerPrinicipalPoint)
        map1, map2 = cv2.initUndistortRectifyMap(scaled_K, D, None, new_K, dim1, cv2.CV_32FC1)

    # undistort
    undistorted_img = cv2.remap(img, map1, map2, interpolation=cv2.INTER_LINEAR, borderMode=cv2.BORDER_CONSTANT)
    return undistorted_img


def undistort(input_im, K, D):
    dim = input_im.shape[:2][::-1]
    new_K, roi = cv2.getOptimalNewCameraMatrix(K, D, dim, 0, dim, True)
    mapx, mapy = cv2.initUndistortRectifyMap(K, D, None, new_K, dim, cv2.CV_32FC1)
    undistorted_img = cv2.remap(input_im.copy(), mapx, mapy, cv2.INTER_LINEAR)
    print("UNDISTORTED SHAPE", undistorted_img.shape)
    return undistorted_img


def filter_points_out_fov(im, pts_2d, pts_3d):
    img_height, img_width = im.shape[:2]
    inds = np.where((pts_2d[0, :] < img_width) & (pts_2d[0, :] >= 0) &
                    (pts_2d[1, :] < img_height) & (pts_2d[1, :] >= 0) &
                    (pts_3d[:, 0] > 0)
                    )[0]

    # Filter out pixels points
    imgfov_pc_pixel = pts_2d[:, inds]

    imgfov_pc_velo = pts_3d[inds, :]
    imgfov_pc_velo = np.hstack((imgfov_pc_velo, np.ones((imgfov_pc_velo.shape[0], 1))))

    return imgfov_pc_pixel, imgfov_pc_velo, inds


def get_rotation_matrix(theta_z, theta_x, theta_y):
    z_theta = np.array([[np.cos(theta_z), -np.sin(theta_z), 0],
                        [np.sin(theta_z), np.cos(theta_z), 0],
                        [0, 0, 1]])

    x_theta = np.array([[1, 0, 0],
                        [0, np.cos(theta_x), -np.sin(theta_x)],
                        [0, np.sin(theta_x), np.cos(theta_x)]])

    y_theta = np.array([[np.cos(theta_y), 0, np.sin(theta_y)],
                        [0, 1, 0],
                        [-np.sin(theta_y), 0, np.cos(theta_y)]])

    rotation_matrix = z_theta @ x_theta @ y_theta
    return rotation_matrix


def detect_peaks(image, peak_threshold):
    """
    Takes an image and detect the peaks usingthe local maximum filter.
    Returns a boolean mask of the peaks (i.e. 1 when
    the pixel's value is the neighborhood maximum, 0 otherwise)
    """

    # Filter pixels below threshold
    image[image < peak_threshold] = 0

    # define an 8-connected neighborhood #
    neighborhood = generate_binary_structure(3, 3)

    # apply the local maximum filter; all pixel of maximal value
    # in their neighborhood are set to 1
    local_max = maximum_filter(image, footprint=neighborhood) == image
    # local_max is a mask that contains the peaks we are
    # looking for, but also the background.
    # In order to isolate the peaks we must remove the background from the mask.

    # we create the mask of the background
    background = (image == 0)

    # a little technicality: we must erode the background in order to
    # successfully subtract it form local_max, otherwise a line will
    # appear along the background border (artifact of the local maximum filter)
    eroded_background = binary_erosion(background, structure=neighborhood, border_value=1)

    # we obtain the final mask, containing only peaks,
    # by removing the background from the local_max mask (xor operation)
    detected_peaks = local_max ^ eroded_background

    return detected_peaks


def get_transformation_matrix(label):
    tx, ty, tz = label["tx"], label["ty"], label["tz"]
    c = math.cos(label["heading"])
    s = math.sin(label["heading"])

    return np.array([
        [c, -s, 0, tx],
        [s, c, 0, ty],
        [0, 0, 1, tz],
        [0, 0, 0, 1]])


def draw_3d_box(img, vehicle_to_image, label, colour=(255, 128, 128), draw_2d_bounding_box=True, retVertices=False):
    # Extract the box size
    sl, sh, sw = label["sl"], label["sh"], label["sw"]

    box_to_vehicle = get_transformation_matrix(label)

    box_to_image = np.matmul(vehicle_to_image, box_to_vehicle)

    # Loop through the 8 corners constituting the 3D box
    # and project them onto the image
    vertices = np.empty([2, 2, 2, 2])
    for k in [0, 1]:
        for l in [0, 1]:
            for m in [0, 1]:
                # 3D point in the box space
                v = np.array([(k - 0.5) * sl, (l - 0.5) * sw, (m - 0.5) * sh, 1.])

                # Project the point onto the image
                v = np.matmul(box_to_image, v)
                # If any of the corner is behind the camera, ignore this object.
                if v[2] < 0:
                    return img

                vertices[k, l, m, :] = [v[0] / v[2], v[1] / v[2]]

    vertices = vertices.astype(np.int32)

    draw_2d_bounding_box = False
    if draw_2d_bounding_box:
        # Compute the 2D bounding box and draw a rectangle
        x1 = np.amin(vertices[:, :, :, 0])
        x2 = np.amax(vertices[:, :, :, 0])
        y1 = np.amin(vertices[:, :, :, 1])
        y2 = np.amax(vertices[:, :, :, 1])

        x1 = min(max(0, x1), mask.shape[1])
        x2 = min(max(0, x2), mask.shape[1])
        y1 = min(max(0, y1), mask.shape[0])
        y2 = min(max(0, y2), mask.shape[0])

        if (x1 != x2 and y1 != y2):
            cv2.rectangle(mask, (x1, y1), (x2, y2), colour, thickness=1)
    else:
        # Draw the edges of the 3D bounding box
        for k in [0, 1]:
            for l in [0, 1]:
                for idx1, idx2 in [((0, k, l), (1, k, l)), ((k, 0, l), (k, 1, l)), ((k, l, 0), (k, l, 1))]:
                    cv2.line(img, tuple(vertices[idx1]), tuple(vertices[idx2]), colour, thickness=1)
        # Draw a cross on the front face to identify front & back.
        for idx1, idx2 in [((1, 0, 0), (1, 1, 1)), ((1, 1, 0), (1, 0, 1))]:
            cv2.line(img, tuple(vertices[idx1]), tuple(vertices[idx2]), colour, thickness=1)

    return img


def get_iou(a, b, epsilon=1e-5):
    """ Given two boxes `a` and `b` defined as a list of four numbers:
            [x1,y1,x2,y2]
        where:
            x1,y1 represent the upper left corner
            x2,y2 represent the lower right corner
        It returns the Intersect of Union score for these two boxes.

    Args:
        a:          (list of 4 numbers) [x1,y1,x2,y2]
        b:          (list of 4 numbers) [x1,y1,x2,y2]
        epsilon:    (float) Small value to prevent division by zero

    Returns:
        (float) The Intersect of Union score.
    """
    # COORDINATES OF THE INTERSECTION BOX
    x1 = max(a[0], b[0])
    y1 = max(a[1], b[1])
    x2 = min(a[2], b[2])
    y2 = min(a[3], b[3])

    # AREA OF OVERLAP - Area where the boxes intersect
    width = (x2 - x1)
    height = (y2 - y1)
    # handle case where there is NO overlap
    if (width < 0) or (height < 0):
        return 0.0
    area_overlap = width * height

    # COMBINED AREA
    area_a = (a[2] - a[0]) * (a[3] - a[1])
    area_b = (b[2] - b[0]) * (b[3] - b[1])
    area_combined = area_a + area_b - area_overlap

    # RATIO OF AREA OF OVERLAP OVER COMBINED AREA
    iou = area_overlap / (area_combined + epsilon)
    return iou


def plot_pc_from_csv_to_ax(pc_csv, ax, config, mask=None):
    pc_csv = filter_fov_az_csv(pts=pc_csv, deg=config.CAMERA_FOV_AZ, key="u_sin")
    pc_csv = filter_fov_az_csv(pts=pc_csv, deg=config.CAMERA_FOV_EL, key="v_sin")
    pc_csv = pc_csv[pc_csv["r"] > config.MIN_RANGE]

    cNorm = colors.Normalize(vmin=config.MIN_RANGE, vmax=config.MAX_RANGE)
    norms = pc_csv["r"].values

    radar_points_projected = config.camera_obj.Radar2Image(r=pc_csv["r"], az=np.rad2deg(np.arcsin(pc_csv["u_sin"])), el=np.rad2deg(np.arcsin(pc_csv["v_sin"])))

    radar_points_projected = radar_points_projected.T
    indices = np.array([])
    if mask is not None:
        for x1, y1, x2, y2 in mask:
            ll = np.array([x1, y1])
            ur = np.array([x2, y2])
            inidx = np.all(np.logical_and(ll <= radar_points_projected, radar_points_projected <= ur), axis=1)
            indices = np.append(indices, np.nonzero(inidx))
        indices = indices.astype(np.int)
        radar_points_projected = radar_points_projected[indices].T
        norms = norms[indices]

    scat = ax.scatter(x=np.round(radar_points_projected[0, :]).astype(np.int),
                      y=np.round(radar_points_projected[1, :]).astype(np.int), s=config.PROJECTION_POINT_SIZE, c=norms,
                      cmap="jet",
                      norm=cNorm)
    cb = plt.colorbar(scat, ax=ax, fraction=0.026, pad=0.08)
    return pc_csv


def get_pc_radar_from_csv(csv, config):
    cur_points = csv.copy()

    cfar_results = np.zeros((len(cur_points), 3), dtype=np.float32)
    cfar_results[:, 0] = cur_points["x"].values
    cfar_results[:, 1] = cur_points["y"].values
    cfar_results[:, 2] = cur_points["z"].values

    cfar_results = cut_by_range(cfar_results, config.MAX_RANGE, config.MIN_RANGE)
    return cfar_results


def fix_lidar_mat(pts, config, calibration_params, radar_lidar=True):
    if radar_lidar:
        pts[:, :3] = lidar_to_radar_coordinates(pts[:, :3])
        pts[:, :3], rot = apply_6DOF(pts[:, :3], yaw=calibration_params["yaw-radar-lidar"],
                                     roll=calibration_params["roll-radar-lidar"],
                                     pitch=calibration_params["pitch-radar-lidar"],
                                     tx=calibration_params["tx-radar-lidar"],
                                     ty=calibration_params["ty-radar-lidar"],
                                     tz=calibration_params["tz-radar-lidar"])

    pts, mask = filter_fov_az(pts, config.FOV_RADAR, x_idx=int(not radar_lidar), y_idx=int(radar_lidar))
    pts = cut_by_range(pts, config.MAX_RANGE, config.MIN_RANGE)
    return pts


def preprocess_lidar_to_image(pts, im, config, calibration_params, cut_by_elevation=0, ):
    pts_velo = np.zeros((pts.shape[0], pts.shape[1] + 3), dtype=np.float32)
    pts_velo[:, :pts.shape[1]] = pts.copy()
    pts_velo[:, -3:] = pts[:, :3].copy()
    # pts_velo holdds original points

    if cut_by_elevation:
        pts_velo = pts_velo[pts_velo[:, 2] < cut_by_elevation]

    pts_velo, mask = filter_fov_az(pts_velo, az=config.CAMERA_FOV_AZ, x_idx=1, y_idx=0)

    pts_velo[:, :3], rot = apply_6DOF(pts_velo[:, :3], yaw=calibration_params["yaw-lidar-camera"],
                                      roll=calibration_params["roll-lidar-camera"],
                                      pitch=calibration_params["pitch-lidar-camera"],
                                      tx=calibration_params["tx-lidar-camera"],
                                      ty=calibration_params["ty-lidar-camera"],
                                      tz=calibration_params["tz-lidar-camera"])

    # todo extract K from calibration params
    # print("K", config.K)
    proj_velo2cam2 = project_velo_to_cam2(K=config.K)
    pts_2d = project_to_image(pts_velo[:, :3].transpose(), proj_velo2cam2)
    imgfov_pc_pixel, imgfov_pc_velo, inds = filter_points_out_fov(im, pts_2d, pts_velo)
    return imgfov_pc_pixel, imgfov_pc_velo


def preprocess_lidar_to_image_simple(pts, im, calibration_params, K, cut_by_elevation=0, camera_fov_az=70):
    pts_velo = np.zeros((pts.shape[0], pts.shape[1] + 3), dtype=np.float32)
    pts_velo[:, :pts.shape[1]] = pts.copy()
    pts_velo[:, -3:] = pts[:, :3].copy()

    if cut_by_elevation:
        pts_velo = pts_velo[pts_velo[:, 2] < cut_by_elevation]

    pts_velo, mask = filter_fov_az(pts_velo, az=camera_fov_az, x_idx=1, y_idx=0)

    pts_velo[:, :3], rot = apply_6DOF(pts_velo[:, :3], yaw=calibration_params["yaw-lidar-camera"],
                                      roll=calibration_params["roll-lidar-camera"],
                                      pitch=calibration_params["pitch-lidar-camera"],
                                      tx=calibration_params["tx-lidar-camera"],
                                      ty=calibration_params["ty-lidar-camera"],
                                      tz=calibration_params["tz-lidar-camera"])

    proj_velo2cam2 = project_velo_to_cam2(K=K)
    pts_2d = project_to_image(pts_velo[:, :3].transpose(), proj_velo2cam2)
    imgfov_pc_pixel, imgfov_pc_velo, inds = filter_points_out_fov(im, pts_2d, pts_velo)
    return imgfov_pc_pixel, imgfov_pc_velo


def cart2spher(x, y, z):
    r = np.sqrt(x ** 2 + y ** 2 + z ** 2)
    theta = np.rad2deg(x / y)
    phi = np.rad2deg(np.arccos(np.sqrt(x ** 2 + y ** 2) / r))
    return r, theta, phi


def fix_center():
    # Fix Centers
    r, theta, phi = cart2spher(lidar_centers_to_display[:, 0], lidar_centers_to_display[:, 1], lidar_centers_to_display[:, 2])
    delta = np.sin(np.deg2rad(theta)) * 1.5
    lidar_centers_to_display[:, x_idx] = lidar_centers_to_display[:, x_idx] + delta
    lidar_centers_to_display[:, y_idx] = lidar_centers_to_display[:, y_idx] + 1.5


def process_lidar_to_image_on_ax(imgfov_pc_pixel, imgfov_pc_velo, im, ax, config, colormap="range", cmap="jet", cbar=True, semantic=None):
    """
    Expect pts (N, 3)
    """
    if imgfov_pc_pixel.shape[1] == 0:
        print("No Points")
        return

    # print("imgfov_pc_pixel.shape", imgfov_pc_pixel.shape)
    if ax is not None:
        if semantic is not None:
            norms = lut[imgfov_pc_velo[:, 4].astype(np.int)] / 255
            cNorm = colors.Normalize(vmin=np.min(norms), vmax=np.max(norms))
        elif colormap == "range":
            norms = np.linalg.norm(imgfov_pc_velo[:, :3], axis=-1)
            cNorm = colors.Normalize(vmin=np.min(norms), vmax=np.max(norms))
        elif colormap == "elevation":
            norms = imgfov_pc_velo[:, 2]
            cNorm = colors.Normalize(vmin=np.min(norms), vmax=np.max(norms))
        scat = ax.scatter(x=np.clip(np.round(imgfov_pc_pixel[0, :]), 0, im.shape[1] - 1).astype(np.int),
                          y=np.clip(np.round(imgfov_pc_pixel[1, :]), 0, im.shape[0] - 1).astype(np.int), s=config.PROJECTION_POINT_SIZE / 13, c=norms,
                          cmap=cmap,
                          norm=cNorm)
        if cbar:
            cb = plt.colorbar(scat, ax=ax, fraction=0.05, pad=0.08, orientation="horizontal")


def process_lidar_to_image_on_ax_simple(imgfov_pc_pixel, imgfov_pc_velo, im, ax, points_size, colormap="range", cmap="jet", cbar=True, semantic=None):
    """
    Expect pts (N, 3)
    """
    if imgfov_pc_pixel.shape[1] == 0:
        print("No Points")
        return

    # print("imgfov_pc_pixel.shape", imgfov_pc_pixel.shape)
    if ax is not None:
        if semantic is not None:
            norms = lut[imgfov_pc_velo[:, 4].astype(np.int)] / 255
            cNorm = colors.Normalize(vmin=np.min(norms), vmax=np.max(norms))
        elif colormap == "range":
            norms = np.linalg.norm(imgfov_pc_velo[:, :3], axis=-1)
            cNorm = colors.Normalize(vmin=np.min(norms), vmax=np.max(norms))
        elif colormap == "elevation":
            norms = imgfov_pc_velo[:, 2]
            cNorm = colors.Normalize(vmin=np.min(norms), vmax=np.max(norms))
        scat = ax.scatter(x=np.clip(np.round(imgfov_pc_pixel[0, :]), 0, im.shape[1] - 1).astype(np.int),
                          y=np.clip(np.round(imgfov_pc_pixel[1, :]), 0, im.shape[0] - 1).astype(np.int), s=points_size, c=norms,
                          cmap=cmap,
                          norm=cNorm)
        if cbar:
            cb = plt.colorbar(scat, ax=ax, fraction=0.026, pad=0.08)


def process_lidar_to_image_on_ax_df(display_df, im, ax, points_size, colormap="range",
                                    cmap="jet", cbar=True, semantic=None, specific_cNorm=None,
                                    orientation="vertical", pad=0.08, fraction=0.026, cbar_title=""):
    plot_df = display_df[display_df["x_pixel"].notna()].reset_index(drop=True)
    if len(plot_df) == 0:
        # print("No Points")
        return

    if ax is not None:
        x = np.clip(np.round(plot_df["x_pixel"]), 0, im.shape[1] - 1).astype(np.int)
        if semantic is not None:
            norms = lut[plot_df["cls"].astype(np.int)] / 255
            cNorm = colors.Normalize(vmin=np.min(norms), vmax=np.max(norms))
        elif colormap == "range":
            norms = plot_df["r"]
            cNorm = colors.Normalize(vmin=0, vmax=100)
        elif colormap == "elevation":
            norms = plot_df["z"]
            cNorm = colors.Normalize(vmin=np.min(norms), vmax=np.max(norms))
        elif colormap in display_df.keys():
            norms = plot_df[colormap]
            cNorm = colors.Normalize(vmin=np.min(norms), vmax=np.max(norms))
        elif specific_cNorm is not None:
            cNorm = specific_cNorm

        elif type(colormap) == tuple: # case color:
            scat = ax.scatter(x=x,
                              y=np.clip(np.round(plot_df["y_pixel"]), 0, im.shape[0] - 1).astype(np.int), s=points_size,
                              color=colormap)
            return

        scat = ax.scatter(x=x,
                          y=np.clip(np.round(plot_df["y_pixel"]), 0, im.shape[0] - 1).astype(np.int), s=points_size, c=norms,
                          cmap=cmap,
                          norm=cNorm)

        if cbar:
            # cb = plt.colorbar(scat, ax=ax, fraction=fraction, pad=pad, orientation=orientation)
            cb = plt.colorbar(scat, ax=ax, shrink=0.3, pad=pad, orientation=orientation)
            cb.set_label(cbar_title)

        return cNorm

def plot_pc(points, ax, c, config, s=0.2, marker=None, label="", elev=False, alpha=None, view_init_elev=90, view_init_az=270):
    if points is None:
        return
    if config.CORRIDOR_X:
        points = apply_corridor(points, config.CORRIDOR_X)

    if elev:
        points = points[points[:, 2] > -3]
        points = points[points[:, 2] < 3]
        norms = points[:, 2]
        cNorm = colors.Normalize(vmin=np.min(norms), vmax=np.max(norms))
        scat = ax.scatter(points[:, 0], points[:, 1], points[:, 2],
                          s=s,
                          c=norms,
                          cmap="jet",
                          norm=cNorm,
                          label=label)
    else:
        scat = ax.scatter(points[:, 0], points[:, 1], points[:, 2],
                          c=c,
                          s=s,
                          label=label, alpha=alpha)

    ax.view_init(elev=view_init_elev, azim=view_init_az)
    return scat

def plot_pc_2d(points, ax, c, config, s=0.2, marker=None, label="", elev=False, alpha=None, view_init_elev=90, view_init_az=270):
    if points is None:
        return
    if config.CORRIDOR_X:
        points = apply_corridor(points, config.CORRIDOR_X)

    if elev:
        points = points[points[:, 2] > -3]
        points = points[points[:, 2] < 3]
        norms = points[:, 2]
        cNorm = colors.Normalize(vmin=np.min(norms), vmax=np.max(norms))
        scat = ax.scatter(points[:, 0], points[:, 1],
                          s=s,
                          c=norms,
                          cmap="jet",
                          norm=cNorm,
                          label=label)
    else:
        scat = ax.scatter(points[:, 0], points[:, 1],
                          c=c,
                          s=s,
                          label=label, alpha=alpha)


    # ax.view_init(elev=view_init_elev, azim=view_init_az)
    return scat












def plot_pc_2d_df(lidar_df, ax, c, s=0.2, marker=None, label="", elev=False, alpha=None, view_init_elev=90, view_init_az=270):
    if lidar_df is None:
        return

    if elev:
        norms = lidar_df["z"]
        cNorm = colors.Normalize(vmin=np.min(norms), vmax=np.max(norms))
        scat = ax.scatter(lidar_df["x"], lidar_df["y"],
                          s=s,
                          c=norms,
                          cmap="jet",
                          norm=cNorm,
                          label=label)
    else:
        scat = ax.scatter(lidar_df["x"], lidar_df["y"],
                          c=c,
                          s=s,
                          label=label, alpha=alpha)

    # ax.view_init(elev=view_init_elev, azim=view_init_az)
    return scat
















def plot_pc_simple(points, ax, c, s=0.2, marker=None, label="", elev=False, alpha=None, view_init_elev=90, view_init_az=270):
    if points is None:
        return
    if elev:
        points = points[points[:, 2] > -3]
        points = points[points[:, 2] < 3]
        norms = points[:, 2]
        cNorm = colors.Normalize(vmin=np.min(norms), vmax=np.max(norms))
        scat = ax.scatter(points[:, 0], points[:, 1], points[:, 2],
                          s=s,
                          c=norms,
                          cmap="jet",
                          norm=cNorm,
                          label=label)
    else:
        scat = ax.scatter(points[:, 0], points[:, 1], points[:, 2],
                          c=c,
                          s=s,
                          label=label, alpha=alpha)

    ax.view_init(elev=view_init_elev, azim=view_init_az)
    return scat


def update_map_indices(lidar_label, camera_label_rect, proj_im, config, calibration_params):
    global_num_bbs = 1
    map_centers_idx = {}

    bb_points = np.zeros((len(lidar_label["pred_boxes"]), 9), dtype=np.float32)
    for idx_label in range(len(lidar_label["pred_boxes"])):
        bb_points[idx_label, :7] = lidar_label["pred_boxes"][idx_label]
        bb_points[idx_label, 7] = lidar_label["pred_labels"][idx_label]
        bb_points[idx_label, 8] = lidar_label["pred_scores"][idx_label]

    # points_to_plot = self.fix_lidar_mat(bb_points, calibration_params=calibration_params)
    # points_to_plot = points_to_plot[points_to_plot[:, 8] > 0]
    # points_to_plot = points_to_plot[points_to_plot[:, 7] == 1]
    # self.plot_pc(points_to_plot, ax=ax1, s=20, c="black")

    for head_idx, bb in enumerate(bb_points):
        cx, cy, cz = bb_points[head_idx][:3]
        dy, dx, dz = bb_points[head_idx][3:6]
        l, w, h = dy, dx, dz
        heading = bb[-1] + np.deg2rad(calibration_params["yaw-radar-lidar"])

        alpha = -np.pi / 2
        c = np.cos(heading + alpha)
        s = np.sin(heading + alpha)
        rot_mat = [[c, -s, 0], [s, c, 0], [0, 0, 1]]

        center = [cx, cy, cz]
        p1 = center + np.dot(rot_mat, [l / 2, w / 2, h / 2])
        p2 = center + np.dot(rot_mat, [l / 2, -w / 2, h / 2])
        p3 = center + np.dot(rot_mat, [-l / 2, -w / 2, h / 2])
        p4 = center + np.dot(rot_mat, [-l / 2, w / 2, h / 2])

        p5 = center + np.dot(rot_mat, [l / 2, w / 2, -h / 2])
        p6 = center + np.dot(rot_mat, [l / 2, -w / 2, - h / 2])
        p7 = center + np.dot(rot_mat, [-l / 2, -w / 2, -h / 2])
        p8 = center + np.dot(rot_mat, [-l / 2, w / 2, -h / 2])

        points = [p1, p2, p3, p4, p5, p6, p7, p8]
        points = np.stack(points, axis=0)

        if points.shape[0] == 0:
            continue

        pts_velo = points.copy()
        pts_velo, rot = apply_6DOF(pts_velo, yaw=calibration_params["yaw-lidar-camera"],
                                   roll=calibration_params["roll-lidar-camera"],
                                   pitch=calibration_params["pitch-lidar-camera"],
                                   tx=calibration_params["tx-lidar-camera"],
                                   ty=calibration_params["ty-lidar-camera"],
                                   tz=calibration_params["tz-lidar-camera"])

        proj_velo2cam2 = project_velo_to_cam2(K=config.K)
        pts_2d = project_to_image(pts_velo[:, :3].transpose(), proj_velo2cam2)
        imgfov_pc_pixel, imgfov_pc_velo, inds = filter_points_out_fov(proj_im, pts_2d, pts_velo)

        if imgfov_pc_pixel.shape[1] <= 1:
            continue
        x1 = np.amin(imgfov_pc_pixel[0, :])
        x2 = np.amax(imgfov_pc_pixel[0, :])
        y1 = np.amin(imgfov_pc_pixel[1, :])
        y2 = np.amax(imgfov_pc_pixel[1, :])

        x1 = min(max(0, x1), proj_im.shape[1])
        x2 = min(max(0, x2), proj_im.shape[1])
        y1 = min(max(0, y1), proj_im.shape[0])
        y2 = min(max(0, y2), proj_im.shape[0])

        # get max iou
        classes = {"car": True}
        for label_idx, cur_label in enumerate(camera_label_rect):
            obj = cur_label['cat']
            if obj not in classes:
                continue
            iou = get_iou(a=cur_label['bbox'], b=[x1, y1, x2, y2])
            if iou > cur_label["lidar_label_max_iou"]:
                cur_label["lidar_label_max_iou"] = iou
                cur_label["lidar_label_max_center"] = (cx, cy, cz)

        # process_lidar_to_image_on_ax(pts=points, im=im, ax=im_ax_4, config=config, calibration_params=calibration_params, cbar=False)
        # proj_im = cv2.rectangle(proj_im, (int(x1), int(y1)), (int(x2), int(y2)), (0, 255, 0), 10)

    # plot label idx
    color = (255, 0, 0)
    fontScale = 1
    font = cv2.FONT_HERSHEY_SIMPLEX
    thickness = 2

    for label_idx, cur_label in enumerate(camera_label_rect):
        if cur_label["lidar_label_max_iou"] > 0.1:
            # Match!
            map_centers_idx[cur_label["lidar_label_max_center"]] = global_num_bbs
            cur_label["lidar_label_idx_to_plot"] = global_num_bbs
            global_num_bbs += 1
    for label_idx, cur_label in enumerate(camera_label_rect):
        if cur_label["lidar_label_idx_to_plot"] == 0:
            cur_label["lidar_label_idx_to_plot"] = global_num_bbs
            global_num_bbs += 1

    for label_idx, cur_label in enumerate(camera_label_rect):
        x1, y1, x2, y2 = cur_label['bbox']
        proj_im = cv2.putText(proj_im, "idx:" + str(camera_label_rect[label_idx]["lidar_label_idx_to_plot"]), (x1, y1), font,
                              fontScale, color, thickness, cv2.LINE_AA)

    return map_centers_idx


def project(df, K, params):
    calibrated_pts = apply_6DOF(df[["orig_x", "orig_y", "orig_z"]], *params)[0]

    df[["orig_x", "orig_y", "orig_z"]] = calibrated_pts

    proj_velo2cam2 = project_velo_to_cam2(K=K)

    pts_2d = project_to_image(calibrated_pts[:, :3].transpose(), proj_velo2cam2)

    df["x_pixel"] = pts_2d[0, :]
    df["y_pixel"] = pts_2d[1, :]

    return df