import os

from DataPipeline.src.GetEngines import get_engines

srcs = ["/media/amper/data_backup6/250mWF_4/2_4_011121_drive_3_WF198_M32_F3/",
        "/media/amper/data_backup6/250mWF_raw/211101/2_4_011121_drive_3_WF198_M32_F3/radarRawData/",
        "/media/amper/data_backup11/250mWF_4/4_01_031121_drive_1_W198_M32/",
        "/media/amper/data_backup11/250mWF_raw/211103/4_01_031121_drive_1_W198_M32/radarRawData/"
        ]

dests = ["/extSSD3/250mWF_4/011121_drive_3_WF198_M32_F3/",
         "/extSSD3/250mWF_4/011121_drive_3_WF198_M32_F3/radar_data/radar_raw/",
         "/extSSD3/250mWF_4/031121_drive_1_W198_M32/",
         "/extSSD3/250mWF_4/031121_drive_1_W198_M32/radar_data/radar_raw/",
         ]

dest_prefix = "/media/amper/"
dests = [f"{dest_prefix}/{x}" for x in dests]

for src, dest in zip(srcs, dests):
    # dest_cmd = " faraday@192.168.1.189:%s" % dest
    # print("src=", src)
    # print("dest=", dest_cmd)

    # cmd_base = "//usr/bin/rsync -rahvtlzL --rsh=\"/usr/bin/sshpass -p wiseai2018 ssh -o StrictHostKeyChecking=no -l faraday\" "
    # final_cmd = cmd_base + src + dest_cmd
    # if "radarRawData" in src:
    #     drive_name = src.split("/")[-2]
    #     print("drive_name", drive_name)
    #     engine = get_engines(wf="198", df="sync_data_lr0_cr20_lc0_gr0.csv", rect=True, drive_str=drive_name)[0]
    #
    #
    #
    # else:
    cmd_base = "//usr/bin/rsync -rahvr "
    dest_cmd = dest
    final_cmd = cmd_base + " " + src + " " + dest_cmd

    print(final_cmd)
    os.system(final_cmd)
